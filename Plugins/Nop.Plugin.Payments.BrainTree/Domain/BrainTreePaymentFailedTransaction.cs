﻿using Nop.Core;
using System;

namespace Nop.Plugin.Payments.BrainTree.Domain
{
    /// <summary>
    /// Represents a failed BrainTree transaction
    /// </summary>
    public class BrainTreePaymentFailedTransaction : BaseEntity
    {
        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the BrainTree transacrion identifier
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// Gets or sets the masked card number
        /// </summary>
        public string MaskedNumber { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the entity is created
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }
    }
}