﻿using Nop.Data.Mapping;
using Nop.Plugin.Payments.BrainTree.Domain;

namespace Nop.Plugin.Payments.BrainTree.Data
{
    public class BrainTreePaymentFailedTransactionMap : NopEntityTypeConfiguration<BrainTreePaymentFailedTransaction>
    {
        public BrainTreePaymentFailedTransactionMap()
        {
            this.ToTable("BrainTreePayment_FailedTransaction");
            this.HasKey(x => x.Id);

            this.Property(x => x.TransactionId).IsRequired().HasMaxLength(20);
            this.Property(x => x.MaskedNumber).IsRequired().HasMaxLength(4);
        }
    }
}