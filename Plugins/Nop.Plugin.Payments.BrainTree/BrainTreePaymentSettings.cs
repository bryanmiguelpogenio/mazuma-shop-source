using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.BrainTree
{
    public class BrainTreePaymentSettings : ISettings
    {
        public bool UseSandBox { get; set; }
        public string MerchantId { get; set; }
        public string MerchantAccountId { get; set; }
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }
        public decimal AdditionalFee { get; set; }
        public bool AdditionalFeePercentage { get; set; }

        public bool Enable3dSecure { get; set; }
        public bool EnableKount { get; set; }

        public int FailedAttemptCount { get; set; }
    }
}