﻿namespace Nop.Plugin.Payments.BrainTree
{
    public static class BrainTreePaymentSessionNames
    {
        public const string PAYMENT_METHOD_NONCE = "payment_method_nonce";
        public const string DEVICE_DATA = "device_data";
    }
}