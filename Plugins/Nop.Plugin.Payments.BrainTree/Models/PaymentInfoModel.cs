﻿using Nop.Core.Domain.Common;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;

namespace Nop.Plugin.Payments.BrainTree.Models
{
    public class PaymentInfoModel : BaseNopModel
    {
        public string ClientToken { get; set; }
        public decimal Amount { get; set; }
        public bool Enable3dSecure { get; set; }
        public bool EnableKount { get; set; }

        public string MobilePhoneNumber { get; set; }
        public string Email { get; set; }
        public string ShippingMethod { get; set; }
        public Address BillingAddress { get; set; }

        #region These fields are temporarily used for labels

        [NopResourceDisplayName("Payment.CardholderName")]
        public string CardholderName { get; set; }

        [NopResourceDisplayName("Payment.CardNumber")]
        public string CardNumber { get; set; }

        [NopResourceDisplayName("Payment.ExpirationDate")]
        public string ExpireMonth { get; set; }

        [NopResourceDisplayName("Payment.ExpirationDate")]
        public string ExpireYear { get; set; }

        [NopResourceDisplayName("Payment.CardCode")]
        public string CardCode { get; set; }

        #endregion
    }
}