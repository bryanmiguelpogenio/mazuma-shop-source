﻿using Braintree;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.BrainTree.Models;
using Nop.Services.Discounts;
using Nop.Services.Orders;
using Nop.Web.Framework.Components;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Payments.BrainTree.Components
{
    [ViewComponent(Name = "PaymentBrainTree")]
    public class PaymentBrainTreeViewComponent : NopViewComponent
    {
        #region Fields

        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly BrainTreePaymentSettings _brainTreePaymentSettings;

        #endregion

        #region Ctor

        public PaymentBrainTreeViewComponent(IOrderTotalCalculationService orderTotalCalculationService,
            IStoreContext storeContext,
            IWorkContext workContext,
            BrainTreePaymentSettings brainTreePaymentSettings)
        {
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._brainTreePaymentSettings = brainTreePaymentSettings;
        }

        #endregion

        public IViewComponentResult Invoke()
        {
            //get settings
            var useSandBox = _brainTreePaymentSettings.UseSandBox;
            var merchantId = _brainTreePaymentSettings.MerchantId;
            var publicKey = _brainTreePaymentSettings.PublicKey;
            var privateKey = _brainTreePaymentSettings.PrivateKey;

            //new gateway
            var gateway = new BraintreeGateway
            {
                Environment = useSandBox ? Environment.SANDBOX : Environment.PRODUCTION,
                MerchantId = merchantId,
                PublicKey = publicKey,
                PrivateKey = privateKey
            };

            var clientTokenRequest = new ClientTokenRequest();

            if (!string.IsNullOrWhiteSpace(_brainTreePaymentSettings.MerchantAccountId))
                clientTokenRequest.MerchantAccountId = _brainTreePaymentSettings.MerchantAccountId;

            var model = new PaymentInfoModel
            {
                ClientToken = gateway.ClientToken.generate(clientTokenRequest),
                Enable3dSecure = _brainTreePaymentSettings.Enable3dSecure,
                EnableKount = _brainTreePaymentSettings.EnableKount,
                MobilePhoneNumber = "",
                Email = string.IsNullOrWhiteSpace(_workContext.CurrentCustomer.Email) ? _workContext.CurrentCustomer.BillingAddress.Email : _workContext.CurrentCustomer.Email,
                ShippingMethod = "",
                BillingAddress = _workContext.CurrentCustomer.BillingAddress
            };

            var cart = _workContext.CurrentCustomer.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id).ToList();

            if (!cart.Any())
                throw new NopException("Cart is empty");

            //order total (and applied discounts, gift cards, reward points)
            var orderTotal = _orderTotalCalculationService.GetShoppingCartTotal(cart, out decimal orderDiscountAmount, out List<DiscountForCaching> orderAppliedDiscounts, out List<AppliedGiftCard> appliedGiftCards, out int redeemedRewardPoints, out decimal redeemedRewardPointsAmount);
            if (!orderTotal.HasValue)
                throw new NopException("Order total couldn't be calculated");

            model.Amount = orderTotal.Value;

            return View("~/Plugins/Payments.BrainTree/Views/PaymentInfo.cshtml", model);
        }
    }
}