﻿using Microsoft.AspNetCore.Mvc.Controllers;
using Nop.Core.Domain.Payments;
using Nop.Services.Events;
using Nop.Services.Payments;
using Nop.Web.Framework.Events;
using Nop.Web.Framework.UI;

namespace Nop.Plugin.Payments.BrainTree.Services
{
    public class EventConsumer : IConsumer<PageRenderingEvent>
    {
        #region Fields

        private readonly IPaymentService _paymentService;
        private readonly PaymentSettings _paymentSettings;

        #endregion

        #region Ctor

        public EventConsumer(IPaymentService paymentService,
            PaymentSettings paymentSettings)
        {
            this._paymentService = paymentService;
            this._paymentSettings = paymentSettings;
        }

        #endregion

        #region Methods

        public void HandleEvent(PageRenderingEvent eventMessage)
        {
            if (eventMessage?.Helper?.ViewContext?.ActionDescriptor == null)
                return;

            //check whether the plugin is installed and is active
            var squarePaymentMethod = _paymentService.LoadPaymentMethodBySystemName("Payments.BrainTree");
            if (!(squarePaymentMethod?.PluginDescriptor?.Installed ?? false) || !squarePaymentMethod.IsPaymentMethodActive(_paymentSettings))
                return;

            //add js sсript to one page checkout
            if (eventMessage.Helper.ViewContext.ActionDescriptor is ControllerActionDescriptor actionDescriptor &&
                actionDescriptor.ControllerName == "Checkout" && (actionDescriptor.ActionName == "OnePageCheckout" || actionDescriptor.ActionName == "ThreeStepPaymentInfo"))
            {
                eventMessage.Helper.AddScriptParts(ResourceLocation.Footer, "https://js.braintreegateway.com/js/braintree-2.32.1.min.js", excludeFromBundle: true);
                eventMessage.Helper.AddScriptParts(ResourceLocation.Footer, "https://js.braintreegateway.com/web/3.37.0/js/client.min.js", excludeFromBundle: true);
                eventMessage.Helper.AddScriptParts(ResourceLocation.Footer, "https://js.braintreegateway.com/web/3.37.0/js/hosted-fields.min.js", excludeFromBundle: true);
                eventMessage.Helper.AddScriptParts(ResourceLocation.Footer, "https://js.braintreegateway.com/web/3.37.0/js/three-d-secure.min.js", excludeFromBundle: true);
                eventMessage.Helper.AddScriptParts(ResourceLocation.Footer, "https://js.braintreegateway.com/web/3.37.0/js/data-collector.min.js", excludeFromBundle: true);
            }
        }

        #endregion
    }
}