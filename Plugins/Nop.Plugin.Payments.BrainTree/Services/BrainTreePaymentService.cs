﻿using Braintree;
using Nop.Core.Data;
using Nop.Plugin.Payments.BrainTree.Domain;
using Nop.Services.Customers;
using Nop.Services.Payments;

namespace Nop.Plugin.Payments.BrainTree.Services
{
    public class BrainTreePaymentService : IBrainTreePaymentService
    {
        #region Constants

        /// <summary>
        /// nopCommerce partner code
        /// </summary>
        private const string BN_CODE = "nopCommerce_SP";

        #endregion

        #region Fields

        private readonly IRepository<BrainTreePaymentFailedTransaction> _brainTreePaymentFailedTransactionRepository;
        private readonly ICustomerService _customerService;
        private readonly BrainTreePaymentSettings _brainTreePaymentSettings;

        #endregion

        #region Ctor

        public BrainTreePaymentService(IRepository<BrainTreePaymentFailedTransaction> brainTreePaymentFailedTransactionRepository,
            ICustomerService customerService,
            BrainTreePaymentSettings brainTreePaymentSettings)
        {
            this._brainTreePaymentFailedTransactionRepository = brainTreePaymentFailedTransactionRepository;
            this._customerService = customerService;
            this._brainTreePaymentSettings = brainTreePaymentSettings;
        }

        #endregion

        #region Utilities

        protected virtual BraintreeGateway InitializeBraintreeGateway()
        {
            return new BraintreeGateway
            {
                Environment = _brainTreePaymentSettings.UseSandBox ? Environment.SANDBOX : Environment.PRODUCTION,
                MerchantId = _brainTreePaymentSettings.MerchantId,
                PublicKey = _brainTreePaymentSettings.PublicKey,
                PrivateKey = _brainTreePaymentSettings.PrivateKey
            };
        }

        #endregion

        #region Methods

        public virtual Result<Transaction> Sale(string paymentMethodNonce,
            ProcessPaymentRequest processPaymentRequest,
            string deviceData = null)
        {
            //get customer
            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);

            //get settings
            var merchantAccountId = _brainTreePaymentSettings.MerchantAccountId;

            var gateway = InitializeBraintreeGateway();

            //new transaction request
            var transactionRequest = new TransactionRequest
            {
                Amount = processPaymentRequest.OrderTotal,
                PaymentMethodNonce = paymentMethodNonce,
                Channel = BN_CODE,
                OrderId = processPaymentRequest.OrderGuid.ToString(),
                DeviceData = deviceData
            };

            if (!string.IsNullOrWhiteSpace(merchantAccountId))
                transactionRequest.MerchantAccountId = merchantAccountId;

            //address request
            var addressRequest = new AddressRequest
            {
                FirstName = customer.BillingAddress.FirstName,
                LastName = customer.BillingAddress.LastName,
                StreetAddress = customer.BillingAddress.Address1,
                PostalCode = customer.BillingAddress.ZipPostalCode
            };
            transactionRequest.BillingAddress = addressRequest;

            //transaction options request
            var transactionOptionsRequest = new TransactionOptionsRequest
            {
                SubmitForSettlement = true
            };
            transactionRequest.Options = transactionOptionsRequest;

            return gateway.Transaction.Sale(transactionRequest);
        }

        public virtual Result<Transaction> Refund(string transactionId, decimal? amount = null)
        {
            var gateway = InitializeBraintreeGateway();

            return amount.HasValue
                ? gateway.Transaction.Refund(transactionId, amount.Value)
                : gateway.Transaction.Refund(transactionId);
        }

        #endregion
    }
}