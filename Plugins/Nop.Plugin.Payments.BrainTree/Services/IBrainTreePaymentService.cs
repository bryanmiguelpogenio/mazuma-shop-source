﻿using Braintree;
using Nop.Services.Payments;

namespace Nop.Plugin.Payments.BrainTree.Services
{
    public interface IBrainTreePaymentService
    {
        Result<Transaction> Sale(string paymentMethodNonce, ProcessPaymentRequest processPaymentRequest, string deviceData = null);
        Result<Transaction> Refund(string transactionId, decimal? amount = null);
    }
}