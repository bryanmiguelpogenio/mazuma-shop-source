﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.BrainTree.Controllers;
using Nop.Services.Configuration;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Localization;
using System.Linq;
using Nop.Core.Http.Extensions;
using Nop.Plugin.Payments.BrainTree.Data;
using Nop.Plugin.Payments.BrainTree.Services;

namespace Nop.Plugin.Payments.BrainTree
{
    public class BrainTreePaymentProcessor : BasePlugin, IPaymentMethod
    {
        #region Fields

        //private readonly BrainTreePaymentObjectContext _objectContext;
        private readonly IBrainTreePaymentService _brainTreePaymentService;
        private readonly ISession _session;
        private readonly ISettingService _settingService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly BrainTreePaymentSettings _brainTreePaymentSettings;
        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;
        private readonly ILogger _logger;

        #endregion

        #region Ctor

        public BrainTreePaymentProcessor(//BrainTreePaymentObjectContext objectContext,
            IBrainTreePaymentService brainTreePaymentService,
            IHttpContextAccessor httpContextAccessor,
            ISettingService settingService,
            IOrderTotalCalculationService orderTotalCalculationService,
            BrainTreePaymentSettings brainTreePaymentSettings,
            ILocalizationService localizationService,
            IWebHelper webHelper,
            ILogger logger)
        {
            //this._objectContext = objectContext;
            this._brainTreePaymentService = brainTreePaymentService;
            this._session = httpContextAccessor.HttpContext.Session;
            this._settingService = settingService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._brainTreePaymentSettings = brainTreePaymentSettings;
            this._localizationService = localizationService;
            this._webHelper = webHelper;
            this._logger = logger;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var processPaymentResult = new ProcessPaymentResult();

            var paymentMethodNonce = _session.Get<string>(BrainTreePaymentSessionNames.PAYMENT_METHOD_NONCE);
            var deviceData = _session.Get<string>(BrainTreePaymentSessionNames.DEVICE_DATA);

            if (string.IsNullOrWhiteSpace(paymentMethodNonce))
            {
                processPaymentResult.AddError("Error processing payment. Please re-enter your payment details.");
                return processPaymentResult;
            }

            var result = _brainTreePaymentService.Sale(paymentMethodNonce, processPaymentRequest, deviceData);

            //result
            if (result.IsSuccess())
            {
                processPaymentResult.NewPaymentStatus = PaymentStatus.Paid;
                processPaymentResult.AuthorizationTransactionId = result.Target.Id;

                // clear nonce and device data
                _session.Remove(BrainTreePaymentSessionNames.PAYMENT_METHOD_NONCE);
                _session.Remove(BrainTreePaymentSessionNames.DEVICE_DATA);
            }
            else
            {
                processPaymentResult.AddError("Failed to process payment. Please try again.");
            }

            return processPaymentResult;
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            //nothing
        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            //you can put any logic here
            //for example, hide this payment method if all products in the cart are downloadable
            //or hide this payment method if current customer is from certain country
            return false;
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            var result = this.CalculateAdditionalFee(_orderTotalCalculationService, cart,
                _brainTreePaymentSettings.AdditionalFee, _brainTreePaymentSettings.AdditionalFeePercentage);
            return result;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            result.AddError("Capture method not supported");
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();

            try
            {
                string transactionId = refundPaymentRequest.Order.AuthorizationTransactionId;

                if (string.IsNullOrWhiteSpace(transactionId))
                {
                    result.AddError("No transaction ID recorded for this order.");
                    return result;
                }

                decimal? refundAmount = null;
                if (refundPaymentRequest.IsPartialRefund)
                    refundAmount = refundPaymentRequest.AmountToRefund;

                var transactionResult = _brainTreePaymentService.Refund(transactionId,  refundAmount);

                if (!transactionResult.IsSuccess())
                {
                    result.AddError(transactionResult.Message);
                }
                
                result.NewPaymentStatus = refundPaymentRequest.IsPartialRefund ? PaymentStatus.PartiallyRefunded : PaymentStatus.Refunded;
            }
            catch (Exception ex)
            {
                _logger.Error("Payment error", ex);

                result.AddError(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            result.AddError("Void not supported");
            return result;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            var result = new CancelRecurringPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            //it's not a redirection payment method. So we always return false
            return false;
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/PaymentBrainTree/Configure";
        }

        public IList<string> ValidatePaymentForm(IFormCollection form)
        {
            var warnings = new List<string>();

            var nonce = form["payment_method_nonce"].FirstOrDefault();

            if (string.IsNullOrWhiteSpace(nonce))
            {
                warnings.Add("Failed to tokenize data. Please try again.");
            }

            return warnings;
        }

        public ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {   
            _session.Set<string>(BrainTreePaymentSessionNames.PAYMENT_METHOD_NONCE, form["payment_method_nonce"].FirstOrDefault());

            var deviceData = form["device_data"].FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(deviceData))
                _session.Set<string>(BrainTreePaymentSessionNames.DEVICE_DATA, deviceData);

            return new ProcessPaymentRequest();
        }

        public void GetPublicViewComponent(out string viewComponentName)
        {
            viewComponentName = "PaymentBrainTree";
        }

        public Type GetControllerType()
        {
            return typeof(PaymentBrainTreeController);
        }

        public override void Install()
        {
            //settings
            var settings = new BrainTreePaymentSettings
            {
                UseSandBox = true,
                MerchantId = "",
                MerchantAccountId = "",
                PrivateKey = "",
                PublicKey = "",
                Enable3dSecure = true,
                EnableKount = true,
                FailedAttemptCount = 3
            };
            _settingService.SaveSetting(settings);

            //database objects
            //_objectContext.Install();

            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.UseSandbox", "Use Sandbox");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.UseSandbox.Hint", "Check to enable Sandbox (testing environment).");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.MerchantId", "Merchant ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.MerchantId.Hint", "Enter Merchant ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.MerchantAccountId", "Merchant Account ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.MerchantAccountId.Hint", "Enter Merchant Account ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.PublicKey", "Public Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.PublicKey.Hint", "Enter Public key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.PrivateKey", "Private Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.PrivateKey.Hint", "Enter Private key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.AdditionalFee", "Additional fee");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.AdditionalFee.Hint", "Enter additional fee to charge your customers.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.AdditionalFeePercentage", "Additional fee. Use percentage");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.AdditionalFeePercentage.Hint", "Determines whether to apply a percentage additional fee to the order total. If not enabled, a fixed value is used.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.PaymentMethodDescription", "Pay by credit / debit card");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.Enable3dSecure", "Enable 3D Secure");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.Enable3dSecure.Hint", "Check to enable 3D Secure");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.EnableKount", "Enable Kount");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTree.Fields.EnableKount.Hint", "Check to enable Kount");

            base.Install();
        }

        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<BrainTreePaymentSettings>();

            //database objects
            //_objectContext.Uninstall();

            //locales
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.UseSandbox");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.UseSandbox.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.MerchantId");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.MerchantId.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.MerchantAccountId");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.MerchantAccountId.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.PublicKey");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.PublicKey.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.PrivateKey");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.PrivateKey.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.AdditionalFee");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.AdditionalFee.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.AdditionalFeePercentage");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.AdditionalFeePercentage.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.PaymentMethodDescription");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.Enable3dSecure");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.Enable3dSecure.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.EnableKount");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTree.Fields.EnableKount.Hint");

            base.Uninstall();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get
            {
                return RecurringPaymentType.NotSupported;
            }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType
        {
            get
            {
                return PaymentMethodType.Standard;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a payment method description that will be displayed on checkout pages in the public store
        /// </summary>
        public string PaymentMethodDescription
        {
            get { return _localizationService.GetResource("Plugins.Payments.BrainTree.PaymentMethodDescription"); }
        }

        #endregion

    }
}
