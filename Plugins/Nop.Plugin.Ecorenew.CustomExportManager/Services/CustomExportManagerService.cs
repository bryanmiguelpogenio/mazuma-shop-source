﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;
using Nop.Core.Plugins;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.ExportImport;
using Nop.Services.ExportImport.Help;
using Nop.Services.Insurance;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Shipping.Date;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace Nop.Plugin.Ecorenew.CustomExportManager.Services
{
    public class CustomExportManagerService : ExportManager
    {
        #region Fields

        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly IPluginFinder _pluginFinder;
        private readonly CatalogSettings _catalogSettings;
        private readonly OrderSettings _orderSettings;
        private readonly ISettingService _settingService;
        private readonly CustomExportManagerSettings _customExportManagerSettings;

        #endregion

        #region Ctor

        public CustomExportManagerService(ICategoryService categoryService, 
            IManufacturerService manufacturerService, 
            ICustomerService customerService, 
            IProductAttributeService productAttributeService, 
            IProductAttributeParser productAttributeParser, 
            IPictureService pictureService, 
            INewsLetterSubscriptionService newsLetterSubscriptionService, 
            IStoreService storeService,
            IStoreContext storeContext,
            IWorkContext workContext, 
            ProductEditorSettings productEditorSettings, 
            IVendorService vendorService,
            IPluginFinder pluginFinder,
            IProductTemplateService productTemplateService, 
            IDateRangeService dateRangeService, 
            ITaxCategoryService taxCategoryService, 
            IMeasureService measureService, 
            CatalogSettings catalogSettings, 
            IGenericAttributeService genericAttributeService, 
            ICustomerAttributeFormatter customerAttributeFormatter, 
            OrderSettings orderSettings, 
            ISpecificationAttributeService specificationAttributeService,
            ISettingService settingService,
            CustomExportManagerSettings customExportManagerSettings) 
            : 
            base(categoryService, 
                manufacturerService, 
                customerService, 
                productAttributeService, 
                pictureService, 
                newsLetterSubscriptionService, 
                storeService, 
                workContext, 
                productEditorSettings, 
                vendorService, 
                productTemplateService, 
                dateRangeService, 
                taxCategoryService, 
                measureService, 
                catalogSettings, 
                genericAttributeService, 
                customerAttributeFormatter, 
                orderSettings, 
                specificationAttributeService)
        {
            this._productAttributeParser = productAttributeParser;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._pluginFinder = pluginFinder;
            this._catalogSettings = catalogSettings;
            this._orderSettings = orderSettings;
            this._settingService = settingService;
            this._customExportManagerSettings = customExportManagerSettings;
        }

        #endregion

        #region Utilities

        private byte[] ExportOrderToXlsxWithProducts(PropertyByName<Order>[] properties, IEnumerable<Order> itemsToExport)
        {
            var orderItemProperties = new[]
            {
                new PropertyByName<OrderItem>("Name", oi => oi.Product.Name),
                new PropertyByName<OrderItem>("Sku", oi => oi.Product.FormatSku(oi.AttributesXml, _productAttributeParser)),
                new PropertyByName<OrderItem>("PriceExclTax", oi => oi.UnitPriceExclTax),
                new PropertyByName<OrderItem>("PriceInclTax", oi => oi.UnitPriceInclTax),
                new PropertyByName<OrderItem>("Quantity", oi => oi.Quantity),
                new PropertyByName<OrderItem>("DiscountExclTax", oi => oi.DiscountAmountExclTax),
                new PropertyByName<OrderItem>("DiscountInclTax", oi => oi.DiscountAmountInclTax),
                new PropertyByName<OrderItem>("TotalExclTax", oi => oi.PriceExclTax),
                new PropertyByName<OrderItem>("TotalInclTax", oi => oi.PriceInclTax)
            };

            var orderItemsManager = new PropertyManager<OrderItem>(orderItemProperties);

            var giftCardProperties = new[]
            {
                new PropertyByName<GiftCardUsageHistory>("GiftCardId", gc => gc.GiftCard.Id),
                new PropertyByName<GiftCardUsageHistory>("GiftCardCouponCode", gc => gc.GiftCard.GiftCardCouponCode),
                new PropertyByName<GiftCardUsageHistory>("GiftCardAmount", gc => gc.GiftCard.Amount),
                new PropertyByName<GiftCardUsageHistory>("UsedValue", gc => gc.UsedValue),
            };

            var giftCardManager = new PropertyManager<GiftCardUsageHistory>(giftCardProperties);

            using (var stream = new MemoryStream())
            {
                // ok, we can run the real code of the sample now
                using (var xlPackage = new ExcelPackage(stream))
                {
                    // uncomment this line if you want the XML written out to the outputDir
                    //xlPackage.DebugMode = true; 

                    // get handles to the worksheets
                    var worksheet = xlPackage.Workbook.Worksheets.Add(typeof(Order).Name);
                    var fpWorksheet = xlPackage.Workbook.Worksheets.Add("DataForProductsFilters");
                    fpWorksheet.Hidden = eWorkSheetHidden.VeryHidden;

                    //create Headers and format them 
                    var manager = new PropertyManager<Order>(properties.Where(p => !p.Ignore));
                    manager.WriteCaption(worksheet, SetCaptionStyle);

                    var row = 2;
                    foreach (var order in itemsToExport)
                    {
                        manager.CurrentObject = order;
                        manager.WriteToXlsx(worksheet, row++, _catalogSettings.ExportImportUseDropdownlistsForAssociatedEntities);

                        //products
                        var orderItems = order.OrderItems.ToList();

                        //a vendor should have access only to his products
                        if (_workContext.CurrentVendor != null)
                            orderItems = orderItems.Where(p => p.Product.VendorId == _workContext.CurrentVendor.Id).ToList();

                        if (orderItems.Any())
                        {
                            orderItemsManager.WriteCaption(worksheet, SetCaptionStyle, row, 2);
                            worksheet.Row(row).OutlineLevel = 1;
                            worksheet.Row(row).Collapsed = true;

                            foreach (var orderItem in orderItems)
                            {
                                row++;
                                orderItemsManager.CurrentObject = orderItem;
                                orderItemsManager.WriteToXlsx(worksheet, row, _catalogSettings.ExportImportUseDropdownlistsForAssociatedEntities, 2, fpWorksheet);
                                worksheet.Row(row).OutlineLevel = 1;
                                worksheet.Row(row).Collapsed = true;
                            }
                        }

                        //gift cards
                        var giftCards = order.GiftCardUsageHistory.ToList();

                        if (giftCards.Any())
                        {
                            row++;
                            giftCardManager.WriteCaption(worksheet, SetCaptionStyle, row, 2);
                            worksheet.Row(row).OutlineLevel = 1;
                            worksheet.Row(row).Collapsed = true;

                            foreach (var giftCard in giftCards)
                            {
                                row++;
                                giftCardManager.CurrentObject = giftCard;
                                giftCardManager.WriteToXlsx(worksheet, row, _catalogSettings.ExportImportUseDropdownlistsForAssociatedEntities, 2, fpWorksheet);
                                worksheet.Row(row).OutlineLevel = 1;
                                worksheet.Row(row).Collapsed = true;
                            }
                        }

                        row++;
                    }

                    xlPackage.Save();
                }

                return stream.ToArray();
            }
        }

        private string Decrypt(string cipherText)
        {
            string EncryptionKey = _settingService.GetSettingByKey<string>("DataEncryptionKey");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Export order list to XML
        /// </summary>
        /// <param name="orders">Orders</param>
        /// <returns>Result in XML format</returns>
        public override string ExportOrdersToXml(IList<Order> orders)
        {
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(CustomExportManagerConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
                return base.ExportOrdersToXml(orders);

            // check if the setting for custom export manager is enabled
            if (!_customExportManagerSettings.CustomExportManagerEnabled)
                return base.ExportOrdersToXml(orders);

            //a vendor should have access only to part of order information
            var ignore = _workContext.CurrentVendor != null;

            var sb = new StringBuilder();
            var stringWriter = new StringWriter(sb);
            var xmlWriter = new XmlTextWriter(stringWriter);
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("Orders");
            xmlWriter.WriteAttributeString("Version", NopVersion.CurrentVersion);

            foreach (var order in orders)
            {
                xmlWriter.WriteStartElement("Order");

                xmlWriter.WriteString("OrderId", order.Id);
                xmlWriter.WriteString("OrderGuid", order.OrderGuid, ignore);
                xmlWriter.WriteString("StoreId", order.StoreId);
                xmlWriter.WriteString("CustomerId", order.CustomerId, ignore);
                xmlWriter.WriteString("OrderStatusId", order.OrderStatusId, ignore);
                xmlWriter.WriteString("PaymentStatusId", order.PaymentStatusId, ignore);
                xmlWriter.WriteString("ShippingStatusId", order.ShippingStatusId, ignore);
                xmlWriter.WriteString("CustomerLanguageId", order.CustomerLanguageId, ignore);
                xmlWriter.WriteString("CustomerTaxDisplayTypeId", order.CustomerTaxDisplayTypeId, ignore);
                xmlWriter.WriteString("CustomerIp", order.CustomerIp, ignore);
                xmlWriter.WriteString("OrderSubtotalInclTax", order.OrderSubtotalInclTax, ignore);
                xmlWriter.WriteString("OrderSubtotalExclTax", order.OrderSubtotalExclTax, ignore);
                xmlWriter.WriteString("OrderSubTotalDiscountInclTax", order.OrderSubTotalDiscountInclTax, ignore);
                xmlWriter.WriteString("OrderSubTotalDiscountExclTax", order.OrderSubTotalDiscountExclTax, ignore);
                xmlWriter.WriteString("OrderShippingInclTax", order.OrderShippingInclTax, ignore);
                xmlWriter.WriteString("OrderShippingExclTax", order.OrderShippingExclTax, ignore);
                xmlWriter.WriteString("PaymentMethodAdditionalFeeInclTax", order.PaymentMethodAdditionalFeeInclTax, ignore);
                xmlWriter.WriteString("PaymentMethodAdditionalFeeExclTax", order.PaymentMethodAdditionalFeeExclTax, ignore);
                xmlWriter.WriteString("TaxRates", order.TaxRates, ignore);
                xmlWriter.WriteString("OrderTax", order.OrderTax, ignore);
                xmlWriter.WriteString("OrderTotal", order.OrderTotal, ignore);
                xmlWriter.WriteString("RefundedAmount", order.RefundedAmount, ignore);
                xmlWriter.WriteString("OrderDiscount", order.OrderDiscount, ignore);
                xmlWriter.WriteString("CurrencyRate", order.CurrencyRate);
                xmlWriter.WriteString("CustomerCurrencyCode", order.CustomerCurrencyCode);
                xmlWriter.WriteString("AffiliateId", order.AffiliateId, ignore);
                xmlWriter.WriteString("AllowStoringCreditCardNumber", order.AllowStoringCreditCardNumber, ignore);
                xmlWriter.WriteString("CardType", order.CardType, ignore);
                xmlWriter.WriteString("CardName", order.CardName, ignore);
                xmlWriter.WriteString("CardNumber", order.CardNumber, ignore);
                xmlWriter.WriteString("MaskedCreditCardNumber", order.MaskedCreditCardNumber, ignore);
                xmlWriter.WriteString("CardCvv2", order.CardCvv2, ignore);
                xmlWriter.WriteString("CardExpirationMonth", order.CardExpirationMonth, ignore);
                xmlWriter.WriteString("CardExpirationYear", order.CardExpirationYear, ignore);
                xmlWriter.WriteString("PaymentMethodSystemName", order.PaymentMethodSystemName, ignore);
                xmlWriter.WriteString("AuthorizationTransactionId", order.AuthorizationTransactionId, ignore);
                xmlWriter.WriteString("AuthorizationTransactionCode", order.AuthorizationTransactionCode, ignore);
                xmlWriter.WriteString("AuthorizationTransactionResult", order.AuthorizationTransactionResult, ignore);
                xmlWriter.WriteString("CaptureTransactionId", order.CaptureTransactionId, ignore);
                xmlWriter.WriteString("CaptureTransactionResult", order.CaptureTransactionResult, ignore);
                xmlWriter.WriteString("SubscriptionTransactionId", order.SubscriptionTransactionId, ignore);
                xmlWriter.WriteString("PaidDateUtc", order.PaidDateUtc == null ? string.Empty : order.PaidDateUtc.Value.ToString(), ignore);
                xmlWriter.WriteString("ShippingMethod", order.ShippingMethod);
                xmlWriter.WriteString("ShippingRateComputationMethodSystemName", order.ShippingRateComputationMethodSystemName, ignore);
                xmlWriter.WriteString("CustomValuesXml", order.CustomValuesXml, ignore);
                xmlWriter.WriteString("VatNumber", order.VatNumber, ignore);
                xmlWriter.WriteString("Deleted", order.Deleted, ignore);
                xmlWriter.WriteString("CreatedOnUtc", order.CreatedOnUtc);

                if (_orderSettings.ExportWithProducts)
                {
                    //products
                    var orderItems = order.OrderItems;

                    //a vendor should have access only to his products
                    if (_workContext.CurrentVendor != null)
                        orderItems = orderItems.Where(oi => oi.Product.VendorId == _workContext.CurrentVendor.Id).ToList();

                    if (orderItems.Any())
                    {
                        xmlWriter.WriteStartElement("OrderItems");
                        foreach (var orderItem in orderItems)
                        {
                            xmlWriter.WriteStartElement("OrderItem");
                            xmlWriter.WriteString("Id", orderItem.Id);
                            xmlWriter.WriteString("OrderItemGuid", orderItem.OrderItemGuid);
                            xmlWriter.WriteString("Name", orderItem.Product.Name);
                            xmlWriter.WriteString("Sku", orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser));
                            xmlWriter.WriteString("PriceExclTax", orderItem.UnitPriceExclTax);
                            xmlWriter.WriteString("PriceInclTax", orderItem.UnitPriceInclTax);
                            xmlWriter.WriteString("Quantity", orderItem.Quantity);
                            xmlWriter.WriteString("DiscountExclTax", orderItem.DiscountAmountExclTax);
                            xmlWriter.WriteString("DiscountInclTax", orderItem.DiscountAmountInclTax);
                            xmlWriter.WriteString("TotalExclTax", orderItem.PriceExclTax);
                            xmlWriter.WriteString("TotalInclTax", orderItem.PriceInclTax);
                            xmlWriter.WriteEndElement();
                        }

                        xmlWriter.WriteEndElement();
                    }
                }

                //shipments
                var shipments = order.Shipments.OrderBy(x => x.CreatedOnUtc).ToList();
                if (shipments.Any())
                {
                    xmlWriter.WriteStartElement("Shipments");
                    foreach (var shipment in shipments)
                    {
                        xmlWriter.WriteStartElement("Shipment");
                        xmlWriter.WriteElementString("ShipmentId", null, shipment.Id.ToString());
                        xmlWriter.WriteElementString("TrackingNumber", null, shipment.TrackingNumber);
                        xmlWriter.WriteElementString("TotalWeight", null, shipment.TotalWeight?.ToString() ?? string.Empty);
                        xmlWriter.WriteElementString("ShippedDateUtc", null, shipment.ShippedDateUtc.HasValue ? shipment.ShippedDateUtc.ToString() : string.Empty);
                        xmlWriter.WriteElementString("DeliveryDateUtc", null, shipment.DeliveryDateUtc?.ToString() ?? string.Empty);
                        xmlWriter.WriteElementString("CreatedOnUtc", null, shipment.CreatedOnUtc.ToString());
                        xmlWriter.WriteEndElement();
                    }

                    xmlWriter.WriteEndElement();
                }

                var giftCardUsageHistories = order.GiftCardUsageHistory.OrderBy(x => x.CreatedOnUtc).ToList();
                if (giftCardUsageHistories.Any())
                {
                    xmlWriter.WriteStartElement("GiftCards");
                    foreach (var GiftCardUsageHistory in giftCardUsageHistories)
                    {
                        xmlWriter.WriteStartElement("GiftCard");
                        xmlWriter.WriteElementString("GiftCardId", null, GiftCardUsageHistory.GiftCard.Id.ToString());
                        xmlWriter.WriteElementString("GiftCardCouponCode", null, GiftCardUsageHistory.GiftCard.GiftCardCouponCode);
                        xmlWriter.WriteElementString("GiftCardAmount", null, GiftCardUsageHistory.GiftCard.Amount.ToString());
                        xmlWriter.WriteElementString("UsedValue", null, GiftCardUsageHistory.UsedValue.ToString());
                        xmlWriter.WriteElementString("CreatedOnUtc", null, GiftCardUsageHistory.GiftCard.CreatedOnUtc.ToString());
                        xmlWriter.WriteEndElement();
                    }

                    xmlWriter.WriteEndElement();
                }

                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
            return stringWriter.ToString();
        }

        /// <summary>
        /// Export orders to XLSX
        /// </summary>
        /// <param name="orders">Orders</param>
        public override byte[] ExportOrdersToXlsx(IList<Order> orders)
        {
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(CustomExportManagerConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
                return base.ExportOrdersToXlsx(orders);

            // check if the setting for custom export manager is enabled
            if (!_customExportManagerSettings.CustomExportManagerEnabled)
                return base.ExportOrdersToXlsx(orders);

            //a vendor should have access only to part of order information
            var ignore = _workContext.CurrentVendor != null;

            //property array
            var properties = new[]
            {
                new PropertyByName<Order>("OrderId", p => p.Id),
                new PropertyByName<Order>("StoreId", p => p.StoreId),
                new PropertyByName<Order>("OrderGuid", p => p.OrderGuid, ignore),
                new PropertyByName<Order>("CustomerId", p => p.CustomerId, ignore),
                new PropertyByName<Order>("OrderStatusId", p => p.OrderStatusId, ignore),
                new PropertyByName<Order>("PaymentStatusId", p => p.PaymentStatusId),
                new PropertyByName<Order>("ShippingStatusId", p => p.ShippingStatusId, ignore),
                new PropertyByName<Order>("OrderSubtotalInclTax", p => p.OrderSubtotalInclTax, ignore),
                new PropertyByName<Order>("OrderSubtotalExclTax", p => p.OrderSubtotalExclTax, ignore),
                new PropertyByName<Order>("OrderSubTotalDiscountInclTax", p => p.OrderSubTotalDiscountInclTax, ignore),
                new PropertyByName<Order>("OrderSubTotalDiscountExclTax", p => p.OrderSubTotalDiscountExclTax, ignore),
                new PropertyByName<Order>("OrderShippingInclTax", p => p.OrderShippingInclTax, ignore),
                new PropertyByName<Order>("OrderShippingExclTax", p => p.OrderShippingExclTax, ignore),
                new PropertyByName<Order>("PaymentMethodAdditionalFeeInclTax", p => p.PaymentMethodAdditionalFeeInclTax, ignore),
                new PropertyByName<Order>("PaymentMethodAdditionalFeeExclTax", p => p.PaymentMethodAdditionalFeeExclTax, ignore),
                new PropertyByName<Order>("TaxRates", p => p.TaxRates, ignore),
                new PropertyByName<Order>("OrderTax", p => p.OrderTax, ignore),
                new PropertyByName<Order>("OrderTotal", p => p.OrderTotal, ignore),
                new PropertyByName<Order>("RefundedAmount", p => p.RefundedAmount, ignore),
                new PropertyByName<Order>("OrderDiscount", p => p.OrderDiscount, ignore),
                new PropertyByName<Order>("CurrencyRate", p => p.CurrencyRate),
                new PropertyByName<Order>("CustomerCurrencyCode", p => p.CustomerCurrencyCode),
                new PropertyByName<Order>("AffiliateId", p => p.AffiliateId, ignore),
                new PropertyByName<Order>("PaymentMethodSystemName", p => p.PaymentMethodSystemName, ignore),
                new PropertyByName<Order>("ShippingPickUpInStore", p => p.PickUpInStore, ignore),
                new PropertyByName<Order>("ShippingMethod", p => p.ShippingMethod),
                new PropertyByName<Order>("ShippingRateComputationMethodSystemName", p => p.ShippingRateComputationMethodSystemName, ignore),
                new PropertyByName<Order>("CustomValuesXml", p => p.CustomValuesXml, ignore),
                new PropertyByName<Order>("VatNumber", p => p.VatNumber, ignore),
                new PropertyByName<Order>("CreatedOnUtc", p => p.CreatedOnUtc.ToOADate()),
                new PropertyByName<Order>("BillingFirstName", p => p.BillingAddress?.FirstName ?? string.Empty),
                new PropertyByName<Order>("BillingLastName", p => p.BillingAddress?.LastName ?? string.Empty),
                new PropertyByName<Order>("BillingEmail", p => p.BillingAddress?.Email ?? string.Empty),
                new PropertyByName<Order>("BillingCompany", p => p.BillingAddress?.Company ?? string.Empty),
                new PropertyByName<Order>("BillingCountry", p => p.BillingAddress?.Country?.Name ?? string.Empty),
                new PropertyByName<Order>("BillingStateProvince", p => p.BillingAddress?.StateProvince?.Name ?? string.Empty),
                new PropertyByName<Order>("BillingCity", p => p.BillingAddress?.City ?? string.Empty),
                new PropertyByName<Order>("BillingAddress1", p => p.BillingAddress?.Address1 ?? string.Empty),
                new PropertyByName<Order>("BillingAddress2", p => p.BillingAddress?.Address2 ?? string.Empty),
                new PropertyByName<Order>("BillingZipPostalCode", p => p.BillingAddress?.ZipPostalCode ?? string.Empty),
                new PropertyByName<Order>("BillingPhoneNumber", p => p.BillingAddress?.PhoneNumber ?? string.Empty),
                new PropertyByName<Order>("BillingFaxNumber", p => p.BillingAddress?.FaxNumber ?? string.Empty),
                new PropertyByName<Order>("ShippingFirstName", p => p.ShippingAddress?.FirstName ?? string.Empty),
                new PropertyByName<Order>("ShippingLastName", p => p.ShippingAddress?.LastName ?? string.Empty),
                new PropertyByName<Order>("ShippingEmail", p => p.ShippingAddress?.Email ?? string.Empty),
                new PropertyByName<Order>("ShippingCompany", p => p.ShippingAddress?.Company ?? string.Empty),
                new PropertyByName<Order>("ShippingCountry", p => p.ShippingAddress?.Country?.Name ?? string.Empty),
                new PropertyByName<Order>("ShippingStateProvince", p => p.ShippingAddress?.StateProvince?.Name ?? string.Empty),
                new PropertyByName<Order>("ShippingCity", p => p.ShippingAddress?.City ?? string.Empty),
                new PropertyByName<Order>("ShippingAddress1", p => p.ShippingAddress?.Address1 ?? string.Empty),
                new PropertyByName<Order>("ShippingAddress2", p => p.ShippingAddress?.Address2 ?? string.Empty),
                new PropertyByName<Order>("ShippingZipPostalCode", p => p.ShippingAddress?.ZipPostalCode ?? string.Empty),
                new PropertyByName<Order>("ShippingPhoneNumber", p => p.ShippingAddress?.PhoneNumber ?? string.Empty),
                new PropertyByName<Order>("ShippingFaxNumber", p => p.ShippingAddress?.FaxNumber ?? string.Empty)
            };

            return _orderSettings.ExportWithProducts ? ExportOrderToXlsxWithProducts(properties, orders) : ExportToXlsx(properties, orders);
        }

        public override string ExportDirectDebitDataToTxt(IList<Order> directDebitsOrder)
        {
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(CustomExportManagerConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
                return base.ExportDirectDebitDataToTxt(directDebitsOrder);

            // check if the setting for custom export manager is enabled
            if (!_customExportManagerSettings.CustomExportManagerEnabled)
                return base.ExportDirectDebitDataToTxt(directDebitsOrder);

            if (directDebitsOrder == null)
                throw new ArgumentNullException(nameof(directDebitsOrder));
            var _orderService = EngineContext.Current.Resolve<IOrderService>();
            var _insuranceDeviceService = EngineContext.Current.Resolve<IInsuranceDeviceService>();

            const string separator = ",";
            var sb = new StringBuilder();

            //new Info 
            sb.Append("Order ID");
            sb.Append(separator);
            sb.Append("Order total");
            sb.Append(separator);
            sb.Append("First name");
            sb.Append(separator);
            sb.Append("Last name");
            sb.Append(separator);
            sb.Append("E - mail");
            sb.Append(separator);
            sb.Append("Contact number");
            sb.Append(separator);
            sb.Append("Selected device type");
            sb.Append(separator);
            sb.Append("Selected device make");
            sb.Append(separator);
            sb.Append("Selected device model");
            sb.Append(separator);
            sb.Append("Selected device cover");
            sb.Append(separator);
            sb.Append("Address Line 1");
            sb.Append(separator);
            sb.Append("Address Line 2");
            sb.Append(separator);
            sb.Append("Address Line 3");
            sb.Append(separator);
            sb.Append("Postcode");
            sb.Append(separator);
            sb.Append("Created date");
            sb.Append(separator);
            sb.Append("Date of birth");
            sb.Append(separator);
            sb.Append("IMEI or serial number");
            sb.Append(separator);
            sb.Append("Mobile number");
            sb.Append(separator);
            sb.Append("Title");
            sb.Append(separator);
            sb.Append("Town");
            sb.Append(separator);
            sb.Append("Updated date");
            sb.Append(separator);
            sb.Append("Payment method");
            sb.Append(separator);
            sb.Append("Quantity");
            sb.Append(separator);
            sb.Append("Account name");
            sb.Append(separator);
            sb.Append("Account number");
            sb.Append(separator);
            sb.Append("Sort code");
            sb.Append(separator);
            sb.Append("Line item totals");
            sb.Append(separator);
            sb.Append("Cancellation date");
            sb.Append(separator);
            sb.Append("Excess Fee");
            sb.Append(separator);
            sb.Append("AccidentalDamage");
            sb.Append(separator);
            sb.Append("TheftCover");
            sb.Append(separator);
            sb.Append("LossCover");
            sb.Append(separator);
            sb.Append("Advice Type");
            sb.Append(Environment.NewLine); //new line

            foreach (var order in directDebitsOrder)
            {
                bool isUpload = false;
                var insuranceOrderItem = order.OrderItems.Where(x => x.UnitInsurancePrice > 0);

                var customer = order.Customer;
                var dd = order.DirectDebits.OrderByDescending(x => x.Id).FirstOrDefault();
                List<string> iMEIs = new List<string>();
                var orderNote = order.OrderNotes.Where(x => x.Note.Contains("IMEI"));
                foreach (var item in orderNote)
                {
                    var notes = item.Note.Split(';');
                    if (notes.Length > 0)
                    {
                        var imeis = notes[0].Split(':');
                        if (imeis.Length > 1)
                        {
                            iMEIs.Add(imeis[1]);
                        }

                    }
                }
                if (iMEIs.Count > 0)
                {
                    if (dd != null)
                    {
                        isUpload = true;
                        List<string> types = new List<string>();
                        List<string> makes = new List<string>();
                        List<string> models = new List<string>();
                        List<int> deviceCovers = new List<int>();
                        List<string> insurancePrices = new List<string>();
                        decimal insuranceOrderTotal = decimal.Zero;
                        int insuranceQuantity = 0;
                        foreach (var orderItem in insuranceOrderItem)
                        {
                            var insuranceDevice = _insuranceDeviceService.GetInsuranceDeviceByAttributesXml(orderItem.AttributesXml);
                            if (insuranceDevice != null)
                            {
                                insuranceOrderTotal += orderItem.UnitInsurancePrice * orderItem.Quantity;
                                insuranceQuantity += orderItem.Quantity;
                                for (var q = 0; q < orderItem.Quantity; q++)
                                {
                                    types.Add(insuranceDevice.Type);
                                    makes.Add(insuranceDevice.Make);
                                    models.Add(insuranceDevice.Model);
                                    deviceCovers.Add(insuranceDevice.Value);
                                    insurancePrices.Add(orderItem.UnitInsurancePrice.ToString("#.00"));
                                }
                            }
                            else
                            {
                                order.OrderNotes.Add(new OrderNote()
                                {
                                    CreatedOnUtc = DateTime.UtcNow,
                                    Note = $"Upload Insurance CSV file: Insurance Device not found, orderItem.Id={orderItem.Id}",
                                    DisplayToCustomer = false
                                });
                            }
                        }

                        sb.Append(order.CustomOrderNumber);
                        sb.Append(separator);

                        sb.Append(insuranceOrderTotal.ToString("#.00"));
                        sb.Append(separator);

                        //sb.Append(customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName));
                        sb.Append(order.ShippingAddress.FirstName);
                        sb.Append(separator);

                        //sb.Append(customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName));
                        sb.Append(order.ShippingAddress.LastName);
                        sb.Append(separator);

                        //sb.Append(customer.Email);
                        sb.Append(order.ShippingAddress.Email);
                        sb.Append(separator);

                        sb.Append(order.ShippingAddress.PhoneNumber);
                        sb.Append(separator);

                        sb.Append("\"" + string.Join(", ", types) + "\"");
                        sb.Append(separator);

                        sb.Append("\"" + string.Join(", ", makes) + "\"");
                        sb.Append(separator);

                        sb.Append("\"" + string.Join(", ", models) + "\"");
                        sb.Append(separator);

                        //??Selected device cover
                        sb.Append("\"" + string.Join(", ", deviceCovers) + "\"");
                        sb.Append(separator);


                        sb.Append(order.ShippingAddress?.Address1);
                        sb.Append(separator);

                        sb.Append(order.ShippingAddress?.Address2);
                        sb.Append(separator);

                        sb.Append(order.BillingAddress.Address1);
                        sb.Append(separator);

                        sb.Append(order.ShippingAddress.ZipPostalCode);
                        sb.Append(separator);


                        sb.Append(order.CreatedOnUtc.ToString("dd/MM/yyyy HH:mm"));
                        sb.Append(separator);

                        sb.Append(order.CreatedOnUtc.ToString("dd/MM/yyyy HH:mm"));
                        sb.Append(separator);

                        //IMEI or serial number
                        sb.Append("\"" + string.Join(", ", iMEIs) + "\"");
                        sb.Append(separator);

                        sb.Append(order.ShippingAddress.PhoneNumber);
                        sb.Append(separator);

                        sb.Append(customer.GetFullName());
                        sb.Append(separator);


                        sb.Append(order.ShippingAddress.City);
                        sb.Append(separator);

                        //?Updated date
                        sb.Append("");
                        sb.Append(separator);

                        sb.Append("Direct debit");
                        sb.Append(separator);

                        sb.Append(insuranceQuantity);
                        sb.Append(separator);

                        sb.Append(Decrypt(dd.BankAccountName.Replace(" ", "")));
                        sb.Append(separator);

                        sb.Append(Decrypt(dd.AccountNumber.Replace(" ", "")));
                        sb.Append(separator);

                        sb.Append(Decrypt(dd.SortCode.Replace("-", "").Replace(" ", "")));
                        sb.Append(separator);

                        sb.Append("\"" + string.Join(", ", insurancePrices) + "\"");
                        sb.Append(separator);

                        //?Cancellation date
                        sb.Append("");
                        sb.Append(separator);

                        //?Excess Fee
                        sb.Append("");
                        sb.Append(separator);

                        //AccidentalDamage
                        sb.Append("Yes");
                        sb.Append(separator);

                        //TheftCover
                        sb.Append("Yes");
                        sb.Append(separator);

                        //LossCover
                        sb.Append("Yes");
                        sb.Append(separator);

                        //Advice Type
                        sb.Append("NB");
                        sb.Append(separator);
                        sb.Append(Environment.NewLine); //new line
                    }

                }
                if (isUpload)
                {
                    order.UploadStatus = UploadStatus.Uploaded;
                }
                else
                {
                    order.UploadStatus = UploadStatus.Error;
                }
                order.OrderNotes.Add(new OrderNote()
                {
                    CreatedOnUtc = DateTime.UtcNow,
                    Note = "Upload Insurance CSV file" + (isUpload ? "Success" : "Failed"),
                    DisplayToCustomer = false
                });
                _orderService.UpdateOrder(order);
            }

            return sb.ToString();
        }

        #endregion
    }
}
