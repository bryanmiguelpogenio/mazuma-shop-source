﻿using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Ecorenew.CustomExportManager.Models
{
    public class ConfigurationModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.CustomExportManager.Fields.CustomExportManagerEnabled")]
        public bool CustomExportManagerEnabled { get; set; }
        public bool CustomExportManagerEnabled_OverrideForStore { get; set; }
    }
}
