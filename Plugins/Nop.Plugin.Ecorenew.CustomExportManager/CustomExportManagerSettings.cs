﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Ecorenew.CustomExportManager
{
    public class CustomExportManagerSettings : ISettings
    {
        /// <summary>
        /// Gets or sets the value indicating whether the custom export manager is enabled
        /// </summary>
        public bool CustomExportManagerEnabled { get; set; }
    }
}
