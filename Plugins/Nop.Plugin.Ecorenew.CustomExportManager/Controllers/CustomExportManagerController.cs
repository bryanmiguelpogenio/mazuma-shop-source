﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Ecorenew.CustomExportManager.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Ecorenew.CustomExportManager.Controllers
{
    public class CustomExportManagerController : BasePluginController
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public CustomExportManagerController(ILocalizationService localizationService,
            IPermissionService permissionService,
            ISettingService settingService,
            IStoreService storeService,
            IWorkContext workContext)
        {
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._settingService = settingService;
            this._storeService = storeService;
            this._workContext = workContext;
        }

        #endregion

        #region Methods

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMessageTemplates))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var customMessageTemplateSettings = _settingService.LoadSetting<CustomExportManagerSettings>(storeScope);

            var model = new ConfigurationModel
            {
                ActiveStoreScopeConfiguration = storeScope,
                CustomExportManagerEnabled = customMessageTemplateSettings.CustomExportManagerEnabled
            };

            if (storeScope > 0)
            {
                model.CustomExportManagerEnabled_OverrideForStore = _settingService.SettingExists(customMessageTemplateSettings, x => x.CustomExportManagerEnabled, storeScope);
            }

            return View("~/Plugins/Ecorenew.CustomExportManager/Views/Configure.cshtml", model);
        }

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMessageTemplates))
                return AccessDeniedView();

            if (!ModelState.IsValid)
                return Configure();

            // load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var customMessageTemplateSettings = _settingService.LoadSetting<CustomExportManagerSettings>(storeScope);

            customMessageTemplateSettings.CustomExportManagerEnabled = model.CustomExportManagerEnabled;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(customMessageTemplateSettings, x => x.CustomExportManagerEnabled, model.CustomExportManagerEnabled_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }
        #endregion
    }
}
