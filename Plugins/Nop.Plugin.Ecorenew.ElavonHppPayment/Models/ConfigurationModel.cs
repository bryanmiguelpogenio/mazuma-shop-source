﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.ElavonHppPayment.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        private IEnumerable<SelectListItem> _defaultCurrencySelectList;

        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Ecorenew.ElavonHppPayment.Fields.UseSandbox")]
        public bool UseSandbox { get; set; }
        public bool UseSandbox_OverrideForStore { get; set; }

        [NopResourceDisplayName("Ecorenew.ElavonHppPayment.Fields.Account")]
        public string Account { get; set; }
        public bool Account_OverrideForStore { get; set; }

        [NopResourceDisplayName("Ecorenew.ElavonHppPayment.Fields.MerchantId")]
        public string MerchantId { get; set; }
        public bool MerchantId_OverrideForStore { get; set; }

        [NopResourceDisplayName("Ecorenew.ElavonHppPayment.Fields.Secret")]
        public string Secret { get; set; }
        public bool Secret_OverrideForStore { get; set; }

        [NopResourceDisplayName("Ecorenew.ElavonHppPayment.Fields.AutoSettle")]
        public bool AutoSettle { get; set; }
        public bool AutoSettle_OverrideForStore { get; set; }

        [NopResourceDisplayName("Ecorenew.ElavonHppPayment.Fields.HppVersion")]
        public string HppVersion { get; set; }
        public bool HppVersion_OverrideForStore { get; set; }

        [NopResourceDisplayName("Ecorenew.ElavonHppPayment.Fields.HppLanguageCode")]
        public string HppLanguageCode { get; set; }
        public bool HppLanguageCode_OverrideForStore { get; set; }

        [NopResourceDisplayName("Ecorenew.ElavonHppPayment.Fields.DefaultCurrencyCode")]
        public string DefaultCurrencyCode { get; set; }
        public bool DefaultCurrencyCode_OverrideForStore { get; set; }
        public IEnumerable<SelectListItem> DefaultCurrencySelectList
        {
            get { return _defaultCurrencySelectList ?? (_defaultCurrencySelectList = new List<SelectListItem>()); }
            set { _defaultCurrencySelectList = value; }
        }

        [NopResourceDisplayName("Ecorenew.ElavonHppPayment.Fields.UseCustomerCurrency")]
        public bool UseCustomerCurrency { get; set; }
        public bool UseCustomerCurrency_OverrideForStore { get; set; }
    }
}
