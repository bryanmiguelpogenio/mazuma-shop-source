﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.ElavonHppPayment.Domain;

namespace Nop.Plugin.Ecorenew.ElavonHppPayment.Data
{
    public class EcorenewElavonHppTransactionResponseMap : NopEntityTypeConfiguration<EcorenewElavonHppTransactionResponse>
    {
        public EcorenewElavonHppTransactionResponseMap()
        {
            this.ToTable("Ecorenew_ElavonHppTransactionResponse");
            this.HasKey(x => x.Id);

            this.Property(x => x.ACCOUNT).IsOptional().HasMaxLength(30);
            this.Property(x => x.AMOUNT).IsOptional().HasMaxLength(11);
            this.Property(x => x.AUTHCODE).IsOptional().HasMaxLength(20);
            this.Property(x => x.AVSADDRESSRESPONSE).IsOptional().HasMaxLength(2);
            this.Property(x => x.AVSPOSTCODERESPONSE).IsOptional().HasMaxLength(2);
            this.Property(x => x.BATCHID).IsOptional().HasMaxLength(20);
            this.Property(x => x.BILLING_CO).IsOptional().HasMaxLength(3);
            this.Property(x => x.BILLING_CODE).IsOptional().HasMaxLength(10);
            this.Property(x => x.CARD_PAYMENT_BUTTON).IsOptional().HasMaxLength(25);
            this.Property(x => x.CAVV).IsOptional().HasMaxLength(50);
            this.Property(x => x.COMMENT1).IsOptional().HasMaxLength(255);
            this.Property(x => x.COMMENT2).IsOptional().HasMaxLength(255);
            this.Property(x => x.CVNRESULT).IsOptional().HasMaxLength(2);
            this.Property(x => x.ECI).IsOptional().HasMaxLength(10);
            this.Property(x => x.HPP_LANG).IsOptional().HasMaxLength(2);
            this.Property(x => x.MERCHANT_ID).IsOptional().HasMaxLength(50);
            this.Property(x => x.MERCHANT_RESPONSE_URL).IsOptional().HasMaxLength(255);
            this.Property(x => x.MESSAGE).IsOptional().HasMaxLength(255);
            this.Property(x => x.ORDER_ID).IsOptional().HasMaxLength(40);
            this.Property(x => x.PASREF).IsOptional().HasMaxLength(50);
            this.Property(x => x.RESULT).IsOptional().HasMaxLength(5);
            this.Property(x => x.SHA1HASH).IsOptional().HasMaxLength(40);
            this.Property(x => x.SHIPPING_CO).IsOptional().HasMaxLength(50);
            this.Property(x => x.SHIPPING_CODE).IsOptional().HasMaxLength(30);
            this.Property(x => x.SUPPLEMENTARY_DATA).IsOptional().HasMaxLength(255);
            this.Property(x => x.TIMESTAMP).IsOptional().HasMaxLength(14);
            this.Property(x => x.XID).IsOptional().HasMaxLength(50);
        }
    }
}
