﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework.Mvc.Routing;

namespace Nop.Plugin.Ecorenew.ElavonHppPayment
{
    public class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Plugin.Ecorenew.ElavonHppPayment.ProcessPayment", "ElavonHpp/ProcessPayment",
                new { controller = "EcorenewElavonHppPayment", action = "ElavonHpp" });
        }
        public int Priority
        {
            get
            {
                return -1;
            }
        }
    }
}