﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Ecorenew.ElavonHppPayment.Components
{
    [ViewComponent(Name = "EcorenewElavonHpp")]
    public class EcorenewElavonHppComponent : NopViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Plugins/Ecorenew.ElavonHppPayment/Views/EcorenewElavonHppPayment/PaymentInfo.cshtml");
        }
    }
}