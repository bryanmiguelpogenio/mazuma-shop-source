﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Logging;
using Nop.Plugin.Ecorenew.ElavonHppPayment.Domain;
using Nop.Plugin.Ecorenew.ElavonHppPayment.Models;
using Nop.Plugin.Ecorenew.ElavonHppPayment.Services;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Linq;

namespace Nop.Plugin.Ecorenew.ElavonHppPayment.Controllers
{
    public class EcorenewElavonHppPaymentController : BasePaymentController
    {
        #region Fields

        private readonly ICurrencyService _currencyService;
        private readonly IEcorenewElavonHppService _elavonHppService;
        private readonly ILocalizationService _localizationService;
        private readonly ILogger _logger;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public EcorenewElavonHppPaymentController(ICurrencyService currencyService,
            IEcorenewElavonHppService elavonHppService,
            ILocalizationService localizationService,
            ILogger logger,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            ISettingService settingService,
            IStoreContext storeContext,
            IStoreService storeService,
            IWorkContext workContext)
        {
            this._currencyService = currencyService;
            this._elavonHppService = elavonHppService;
            this._localizationService = localizationService;
            this._logger = logger;
            this._orderProcessingService = orderProcessingService;
            this._orderService = orderService;
            this._settingService = settingService;
            this._storeContext = storeContext;
            this._storeService = storeService;
            this._workContext = workContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the configuration page for the plugin
        /// </summary>
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public ActionResult Configure()
        {
            // load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var elavonHppPaymentSettings = _settingService.LoadSetting<EcorenewElavonHppPaymentSettings>(storeScope);

            var model = new ConfigurationModel
            {
                UseSandbox = elavonHppPaymentSettings.UseSandbox,
                Account = elavonHppPaymentSettings.Account,
                MerchantId = elavonHppPaymentSettings.MerchantId,
                Secret = elavonHppPaymentSettings.Secret,
                AutoSettle = elavonHppPaymentSettings.AutoSettle,
                HppVersion = elavonHppPaymentSettings.HppVersion,
                HppLanguageCode = elavonHppPaymentSettings.HppLanguageCode,
                DefaultCurrencyCode = elavonHppPaymentSettings.DefaultCurrencyCode,
                UseCustomerCurrency = elavonHppPaymentSettings.UseCustomerCurrency,

                DefaultCurrencySelectList = _currencyService.GetAllCurrencies(storeId: storeScope)
                .Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.CurrencyCode,
                    Selected = x.CurrencyCode == elavonHppPaymentSettings.DefaultCurrencyCode
                }).ToList(),

                ActiveStoreScopeConfiguration = storeScope
            };

            if (storeScope > 0)
            {
                model.UseSandbox_OverrideForStore = _settingService.SettingExists(elavonHppPaymentSettings, x => x.UseSandbox, storeScope);
                model.Account_OverrideForStore = _settingService.SettingExists(elavonHppPaymentSettings, x => x.Account, storeScope);
                model.MerchantId_OverrideForStore = _settingService.SettingExists(elavonHppPaymentSettings, x => x.MerchantId, storeScope);
                model.Secret_OverrideForStore = _settingService.SettingExists(elavonHppPaymentSettings, x => x.Secret, storeScope);
                model.AutoSettle_OverrideForStore = _settingService.SettingExists(elavonHppPaymentSettings, x => x.AutoSettle, storeScope);
                model.HppVersion_OverrideForStore = _settingService.SettingExists(elavonHppPaymentSettings, x => x.HppVersion, storeScope);
                model.HppLanguageCode_OverrideForStore = _settingService.SettingExists(elavonHppPaymentSettings, x => x.HppLanguageCode, storeScope);
                model.DefaultCurrencyCode_OverrideForStore = _settingService.SettingExists(elavonHppPaymentSettings, x => x.DefaultCurrencyCode, storeScope);
                model.UseCustomerCurrency_OverrideForStore = _settingService.SettingExists(elavonHppPaymentSettings, x => x.UseCustomerCurrency, storeScope);
            }

            return View("~/Plugins/Ecorenew.ElavonHppPayment/Views/EcorenewElavonHppPayment/Configure.cshtml", model);
        }

        /// <summary>
        /// Handles the post request for configure
        /// </summary>
        /// <param name="model">The configuration model</param>
        [HttpPost]
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public ActionResult Configure(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var elavonHppPaymentSettings = _settingService.LoadSetting<EcorenewElavonHppPaymentSettings>(storeScope);

            //save settings
            elavonHppPaymentSettings.UseSandbox = model.UseSandbox;
            elavonHppPaymentSettings.Account = model.Account;
            elavonHppPaymentSettings.MerchantId = model.MerchantId;
            elavonHppPaymentSettings.Secret = model.Secret;
            elavonHppPaymentSettings.AutoSettle = model.AutoSettle;
            elavonHppPaymentSettings.HppVersion = model.HppVersion;
            elavonHppPaymentSettings.HppLanguageCode = model.HppLanguageCode;
            elavonHppPaymentSettings.DefaultCurrencyCode = model.DefaultCurrencyCode;
            elavonHppPaymentSettings.UseCustomerCurrency = model.UseCustomerCurrency;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            if (model.UseSandbox_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(elavonHppPaymentSettings, x => x.UseSandbox, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(elavonHppPaymentSettings, x => x.UseSandbox, storeScope);

            if (model.Account_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(elavonHppPaymentSettings, x => x.Account, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(elavonHppPaymentSettings, x => x.Account, storeScope);

            if (model.MerchantId_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(elavonHppPaymentSettings, x => x.MerchantId, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(elavonHppPaymentSettings, x => x.MerchantId, storeScope);

            if (model.Secret_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(elavonHppPaymentSettings, x => x.Secret, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(elavonHppPaymentSettings, x => x.Secret, storeScope);

            if (model.AutoSettle_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(elavonHppPaymentSettings, x => x.AutoSettle, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(elavonHppPaymentSettings, x => x.AutoSettle, storeScope);

            if (model.HppVersion_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(elavonHppPaymentSettings, x => x.HppVersion, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(elavonHppPaymentSettings, x => x.HppVersion, storeScope);

            if (model.HppLanguageCode_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(elavonHppPaymentSettings, x => x.HppLanguageCode, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(elavonHppPaymentSettings, x => x.HppLanguageCode, storeScope);

            if (model.DefaultCurrencyCode_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(elavonHppPaymentSettings, x => x.DefaultCurrencyCode, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(elavonHppPaymentSettings, x => x.DefaultCurrencyCode, storeScope);

            if (model.UseCustomerCurrency_OverrideForStore || storeScope == 0)
                _settingService.SaveSetting(elavonHppPaymentSettings, x => x.UseCustomerCurrency, storeScope, false);
            else if (storeScope > 0)
                _settingService.DeleteSetting(elavonHppPaymentSettings, x => x.UseCustomerCurrency, storeScope);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        /// <summary>
        /// Elavon HPP payment page
        /// </summary>
        /// <param name="order">This should be the order GUID</param>
        public ActionResult ElavonHpp(string order)
        {
            var hppRequest = _elavonHppService.CreateNewHppTransactionRequest(
                order, 
                _storeContext.CurrentStore.Id, 
                Url.Action("ElavonHppResponse"), 
                out string hppRequestUrl);

            ViewBag.EcorenewElavonHppRequestUrl = hppRequestUrl;

            return View("~/Plugins/Ecorenew.ElavonHppPayment/Views/EcorenewElavonHppPayment/ElavonHpp.cshtml", hppRequest);
        }

        /// <summary>
        /// This will handle the Elavon HPP response.
        /// The page should redirect here after the HPP payment.
        /// This will only work on the domain registered to Elavon
        /// </summary>
        /// <param name="response">The response being thrown by Elavon</param>
        [HttpPost]
        public ActionResult ElavonHppResponse(EcorenewElavonHppTransactionResponse response)
        {
            bool errorOnPostPayment = false;

            if (response == null)
            {
                errorOnPostPayment = true;
                _logger.InsertLog(LogLevel.Error, "EcoRenew Elavon HPP Post Payment - Null Response", "Null Response. ORDER_ID value: " + response.ORDER_ID, _workContext.CurrentCustomer);
            }
            else
            {
                // If passed
                if (response.RESULT == "00")
                {
                    // Get order by GUID
                    Guid orderGuid;

                    if (Guid.TryParse(response.ORDER_ID, out orderGuid) == false)
                    {
                        errorOnPostPayment = true;
                        _logger.InsertLog(LogLevel.Error, "EcoRenew Elavon HPP Post Payment - Error on parsing order GUID", "Failed to parse the order GUID from Elavon HPP response. ORDER_ID value: " + response.ORDER_ID, _workContext.CurrentCustomer);
                    }
                    else
                    {
                        var order = _orderService.GetOrderByGuid(orderGuid);
                        if (order == null)
                        {
                            errorOnPostPayment = true;
                            _logger.InsertLog(LogLevel.Error, "EcoRenew Elavon HPP Post Payment - Unable to get order", "Failed to get the order by GUID. ORDER_ID value: " + response.ORDER_ID, _workContext.CurrentCustomer);
                        }
                        else
                        {
                            try
                            {
                                // Set order as paid
                                _orderProcessingService.MarkOrderAsPaid(order);
                            }
                            catch (Exception ex)
                            {
                                errorOnPostPayment = true;
                                _logger.InsertLog(LogLevel.Error, "EcoRenew Elavon HPP Post Payment - Failed to mark order as paid", "Exception message: " + ex.Message + ". ORDER_ID value: " + response.ORDER_ID, _workContext.CurrentCustomer);
                            }
                        }
                    }
                }

                try
                {
                    _elavonHppService.SaveElavonHppTransactionResponse(response);
                }
                catch (Exception ex)
                {
                    _logger.InsertLog(LogLevel.Warning, "EcoRenew Elavon HPP Post Payment - Error while saving response", "Exception message: " + ex.Message + ". ORDER_ID value: " + response.ORDER_ID, _workContext.CurrentCustomer);
                }
            }

            if (errorOnPostPayment)
            {
                return PartialView("~/Plugins/Ecorenew.ElavonHppPayment/Views/EcorenewElavonHppPayment/ElavonHppResponseError.cshtml");
            }
            else
            {
                return PartialView("~/Plugins/Ecorenew.ElavonHppPayment/Views/EcorenewElavonHppPayment/ElavonHppResponse.cshtml", response);
            }
        }

        #endregion
    }
}