﻿namespace Nop.Plugin.Ecorenew.ElavonHppPayment.Domain
{
    /// <summary>
    /// Represents the model for creating HPP transaction requests.
    /// </summary>
    /// <see cref="https://developer.ElavonHppPaymentgateway.com/#!/hpp/transaction-processing"/>
    public class EcorenewElavonHppTransactionRequest
    {
        /// <summary>
        /// (Mandatory) Supplied by Elavon Payment Gateway, also known as the Client ID.
        /// Note: This is not the merchant number supplied by your bank.
        /// </summary>
        public string MERCHANT_ID { get; set; }
        
        /// <summary>
        /// (Optional) The sub-account to use for this transaction.
        /// If not present, the default sub-account will be used.
        /// </summary>
        public string ACCOUNT { get; set; }
        
        /// <summary>
        /// (Mandatory) A unique alphanumeric id that’s used to identify the transaction.
        /// No spaces are allowed.
        /// Note: Equivalent to OrderGuid
        /// </summary>
        public string ORDER_ID { get; set; }
        
        /// <summary>
        /// (Mandatory) Total amount to authorise in the lowest unit of the currency – i.e. 100 euro would be entered as 10000.
        /// If there is no decimal in the currency (e.g. JPY Yen) then contact Elavon Payment Gateway. No decimal points are allowed.
        /// </summary>
        public string AMOUNT { get; set; }
        
        /// <summary>
        /// (Mandatory) A three-letter currency code (for example EUR, GBP).
        /// </summary>
        public string CURRENCY { get; set; }
        
        /// <summary>
        /// (Mandatory) The AutoSettle Flag allow merchants to specify whether they wish for the transaction to be added to
        /// the next settlement file ('1') or whether to just authorise the transaction and wait to actually
        /// deduct the funds from the customer ('0') until such a time that the transaction is settled.
        /// The card schemes (Visa, MasterCard etc.) have their own rules around how long transactions can wait to be settled.
        /// Please consult with your Acquiring Bank or contact a member of our support team for more information.
        /// </summary>
        public bool AUTO_SETTLE_FLAG { get; set; }
        
        /// <summary>
        /// (Optional) A free-form comment to describe the transaction.
        /// </summary>
        public string COMMENT1 { get; set; }
        
        /// <summary>
        /// (Optional) A free-form comment to describe the transaction.
        /// </summary>
        public string COMMENT2 { get; set; }
        
        /// <summary>
        /// (Optional) The postcode or ZIP of the shipping address.
        /// </summary>
        public string SHIPPING_CODE { get; set; }

        /// <summary>
        /// (Optional) The country of the shipping address.
        /// </summary>
        public string SHIPPING_CO { get; set; }
        
        /// <summary>
        /// (Optional) The postcode or ZIP of the billing address.
        /// Please see the AVS article on how to format this field correctly for address checking.
        /// </summary>
        /// <see cref="https://developer.ElavonHppPaymentgateway.com/#!/hpp/fraud-management/avs"/>
        public string BILLING_CODE { get; set; }
        
        /// <summary>
        /// (Optional) The country of the billing address.
        /// </summary>
        public string BILLING_CO { get; set; }
        
        /// <summary>
        /// (Optional) A unique ID for the customer. This will visible under the transaction in Reporting.
        /// Note: Equivalent to CustomerGuid
        /// </summary>
        public string CUST_NUM { get; set; }

        /// <summary>
        /// (Optional) A variable reference associated with this transaction. This will visible under the transaction in Reporting.
        /// </summary>
        public string VAR_REF { get; set; }

        /// <summary>
        /// (Optional) A product ID associated with this transaction. This will visible under the transaction in Reporting.
        /// Note: Equivalent to either Product ID, Product SKU or Product Attribute Combination SKU
        /// Recommended equivalent: Product Attribute Combination SKU
        /// </summary>
        public string PROD_ID { get; set; }

        /// <summary>
        /// (Optional) Used to set what language HPP is displayed in.
        /// If the field is not sent in, the default language set on your account will be displayed.
        /// This can be set by your account manager.
        /// </summary>
        public string HPP_LANG { get; set; }

        /// <summary>
        /// (Optional) Determines which look and feel of the HPP is displayed to the customer.
        /// Must be submitted as '2' to enable HPP Card Management, unless you are using Elavon's Lightbox solution.
        /// </summary>
        public string HPP_VERSION { get; set; }

        /// <summary>
        /// (Mandatory) Date and time of the transaction. Entered in the following format: YYYYMMDDHHMMSS.
        /// Must be within 24 hours of the current time.
        /// </summary>
        public string TIMESTAMP { get; set; }

        /// <summary>
        /// (Mandatory) A digital signature generated using the SHA-1 algorithm.
        /// </summary>
        public string SHA1HASH { get; set; }

        /// <summary>
        /// (Optional) Used to set which URL in your application the transaction response will be sent to.
        /// A fixed URL can also be added to your account by our support team
        /// </summary>
        public string MERCHANT_RESPONSE_URL { get; set; }

        /// <summary>
        /// (Optional) Used to set what text is displayed on the payment button for card transactions.
        /// If this field is not sent in, “Pay Now” is displayed on the button by default.
        /// </summary>
        public string CARD_PAYMENT_BUTTON { get; set; }

        /// <summary>
        /// (Optional) Anything else you send to Elavon Payment Gateway will be returned in the
        /// response (whatever other information you collected from the customer such as product or address/telephone numbers etc….)
        /// Note: These fields will not be returned in the post response for PayPal transactions.
        /// </summary>
        public string SUPPLEMENTARY_DATA { get; set; }
    }
}
