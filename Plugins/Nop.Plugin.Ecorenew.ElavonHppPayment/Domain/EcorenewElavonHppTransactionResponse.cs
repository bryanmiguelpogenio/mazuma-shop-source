﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.ElavonHppPayment.Domain
{
    /// <summary>
    /// Represents the model for creating HPP transaction response.
    /// </summary>
    /// <see cref="https://developer.ElavonHppPaymentgateway.com/#!/hpp/transaction-processing"/>
    public class EcorenewElavonHppTransactionResponse : BaseEntity
    {
        /// <summary>
        /// The outcome of the transaction.
        /// Will contain “00” if the transaction was a success or another value (depending on the error) if not.
        /// </summary>
        public string RESULT { get; set; }

        /// <summary>
        /// Will contain a valid authcode if the transaction was successful. Will be empty otherwise.
        /// </summary>
        public string AUTHCODE { get; set; }

        /// <summary>
        /// A text message that describes the result code.
        /// </summary>
        public string MESSAGE { get; set; }

        /// <summary>
        /// A unique payments reference assigned to your transaction.
        /// </summary>
        public string PASREF { get; set; }

        /// <summary>
        /// The result of the Address Verification Service (AVS) check on the post code of the customer's billing address.
        /// Available for UK cardholders only.
        /// </summary>
        public string AVSPOSTCODERESPONSE { get; set; }

        /// <summary>
        /// The result of the Address Verification Service (AVS) check on the first line of the billing address.
        /// Available for UK cardholders only.
        /// </summary>
        public string AVSADDRESSRESPONSE { get; set; }

        /// <summary>
        /// The result of the Card Verification check (if enabled).
        /// </summary>
        public string CVNRESULT { get; set; }

        /// <summary>
        /// The sub-account used in the transaction.
        /// </summary>
        public string ACCOUNT { get; set; }

        /// <summary>
        /// Also known as the Client ID; assigned by Elavon Payment Gateway.
        /// </summary>
        public string MERCHANT_ID { get; set; }

        /// <summary>
        /// The unique order ID for this transaction.
        /// </summary>
        public string ORDER_ID { get; set; }

        /// <summary>
        /// The timestamp of the transaction response sent from Elavon Payment Gateway.
        /// </summary>
        public string TIMESTAMP { get; set; }

        /// <summary>
        /// The amount that was authorised. Returned in the lowest unit of the currency.
        /// </summary>
        public string AMOUNT { get; set; }

        /// <summary>
        /// The text that was displayed in the payment button.
        /// </summary>
        public string CARD_PAYMENT_BUTTON { get; set; }

        /// <summary>
        /// The URL that HPP sent the transaction response to, if applicable.
        /// </summary>
        public string MERCHANT_RESPONSE_URL { get; set; }

        /// <summary>
        /// The language that was displayed in the HPP.
        /// </summary>
        public string HPP_LANG { get; set; }

        /// <summary>
        /// Postcode or ZIP of the customer's shipping address.
        /// </summary>
        public string SHIPPING_CODE { get; set; }

        /// <summary>
        /// Country code of the customer's shipping address.
        /// </summary>
        public string SHIPPING_CO { get; set; }

        /// <summary>
        /// Postcode or ZIP of the customer's billing address.
        /// </summary>
        public string BILLING_CODE { get; set; }

        /// <summary>
        /// Country code of the customer's billing address.
        /// </summary>
        public string BILLING_CO { get; set; }

        /// <summary>
        /// The comment sent in the transaction request.
        /// </summary>
        public string COMMENT1 { get; set; }

        /// <summary>
        /// The comment sent in the transaction request.
        /// </summary>
        public string COMMENT2 { get; set; }

        /// <summary>
        /// This is the Elavon Payment Gateway batch that this transaction will be in.
        /// (This is equal to “-1” if the transaction was sent in with the autosettle flag off.
        /// After you settle it (either manually or programmatically) the response to that transaction will contain the batch id.)
        /// </summary>
        public string BATCHID { get; set; }

        /// <summary>
        /// This is the ecommerce indicator (this will only be returned for 3DSecure transactions).
        /// Indicates which 3D Secure scenario the transaction falls under.
        /// </summary>
        public string ECI { get; set; }

        /// <summary>
        /// Cardholder Authentication Verification Value (this will only be returned for 3DSecure transactions).
        /// A unique identifier for the authentication.
        /// </summary>
        public string CAVV { get; set; }

        /// <summary>
        /// Exchange Identifier (this will only be returned for 3DSecure transactions).
        /// </summary>
        public string XID { get; set; }

        /// <summary>
        /// A SHA-1 digital signature created using the fields and your shared secret, used to check the integrity of the response
        /// </summary>
        public string SHA1HASH { get; set; }

        /// <summary>
        /// Anything else you sent to us in the request will be returned to you.
        /// Note: These fields will not be returned for PayPal transactions.
        /// </summary>
        public string SUPPLEMENTARY_DATA { get; set; }
    }
}
