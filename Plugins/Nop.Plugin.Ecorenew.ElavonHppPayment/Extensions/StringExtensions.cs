﻿using System;
using System.Linq;

namespace Nop.Plugin.Ecorenew.ElavonHppPayment.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Converts string to SHA1 hashed string
        /// </summary>
        /// <param name="str">The string to be hashed</param>
        public static string ToSha1HashString(this string str)
        {
            byte[] toHash = System.Text.Encoding.UTF8.GetBytes(str);

            byte[] hashedData;

            using (System.Security.Cryptography.SHA1 sha = System.Security.Cryptography.SHA1.Create())
            {
                hashedData = sha.ComputeHash(toHash);
            }

            return hashedData.ToHexString();
        }

        /// <summary>
        /// Extracts only digit characters from the string
        /// </summary>
        /// <param name="str">Input string</param>
        public static string ExtractNumber(this string str)
        {
            return new string(str.Where(c => Char.IsDigit(c)).ToArray());
        }
    }
}
