﻿using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Ecorenew.ElavonHppPayment.Data;
using Nop.Plugin.Ecorenew.ElavonHppPayment.Domain;
using Nop.Plugin.Ecorenew.ElavonHppPayment.Services;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Ecorenew.ElavonHppPayment
{
    /// <summary>
    /// Dependency Registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            // database contexts
            this.RegisterPluginDataContext<EcorenewElavonHppPaymentObjectContext>(builder, "nop_object_context_ecorenew_elavon_hpp_payment");

            // services
            builder.RegisterType<EcorenewElavonHppService>().As<IEcorenewElavonHppService>().InstancePerLifetimeScope();

            // repositories
            builder.RegisterType<EfRepository<EcorenewElavonHppTransactionResponse>>()
                .As<IRepository<EcorenewElavonHppTransactionResponse>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_ecorenew_elavon_hpp_payment"))
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}
