﻿using Nop.Core.Domain.Orders;
using Nop.Plugin.Ecorenew.ElavonHppPayment.Domain;

namespace Nop.Plugin.Ecorenew.ElavonHppPayment.Services
{
    public interface IEcorenewElavonHppService
    {
        /// <summary>
        /// Returns SHA1 hash for the HPP transaction request
        /// </summary>
        /// <param name="hppTransactionRequest">HPP transaction Request</param>
        /// <param name="secret">HPP shared secret</param>
        string GetSha1Hash(EcorenewElavonHppTransactionRequest hppTransactionRequest, string secret);

        /// <summary>
        /// Creates a new HPP transaction request based on the provided parameters
        /// </summary>
        /// <param name="orderGuid">Order GUID</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="responseUrl">The URL under the current store domain that will handle the response coming from the HPP</param>
        /// <param name="requestUrl">The URL for the request. This will differ based on the current setting.</param>
        EcorenewElavonHppTransactionRequest CreateNewHppTransactionRequest(string orderGuid, int storeId, string responseUrl, out string requestUrl);

        /// <summary>
        /// Save a record of the HPP transaction response
        /// </summary>
        /// <param name="response">The HPP transaction response to be recorded</param>
        void SaveElavonHppTransactionResponse(EcorenewElavonHppTransactionResponse response);
    }
}
