﻿using Nop.Core.Data;
using Nop.Plugin.Ecorenew.ElavonHppPayment.Domain;
using Nop.Plugin.Ecorenew.ElavonHppPayment.Extensions;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Stores;
using System;
using System.Text.RegularExpressions;

namespace Nop.Plugin.Ecorenew.ElavonHppPayment.Services
{
    public class EcorenewElavonHppService : IEcorenewElavonHppService
    {
        #region Fields

        private readonly ICurrencyService _currencyService;
        private readonly ILocalizationService _localizationService;
        private readonly IOrderService _orderService;
        private readonly IRepository<EcorenewElavonHppTransactionResponse> _elavonHppTransactionResponseRepository;
        private readonly ISettingService _settingService;
        private readonly IStoreService _storeService;

        #endregion

        #region Ctor

        public EcorenewElavonHppService(ICurrencyService currencyService,
            ILocalizationService _localizationService,
            IOrderService orderService,
            IRepository<EcorenewElavonHppTransactionResponse> elavonHppTransactionResponseRepository,
            ISettingService settingService,
            IStoreService storeService)
        {
            this._currencyService = currencyService;
            this._localizationService = _localizationService;
            this._orderService = orderService;
            this._elavonHppTransactionResponseRepository = elavonHppTransactionResponseRepository;
            this._settingService = settingService;
            this._storeService = storeService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns SHA1 hash for the HPP transaction request
        /// </summary>
        /// <param name="hppTransactionRequest">HPP transaction Request</param>
        /// <param name="secret">HPP shared secret</param>
        public string GetSha1Hash(EcorenewElavonHppTransactionRequest hppTransactionRequest, string secret)
        {
            // validations
            if (hppTransactionRequest == null)
            {
                throw new Exception("Invalid HPP Request.");
            }

            if (string.IsNullOrWhiteSpace(secret))
            {
                throw new ArgumentNullException("secret");
            }

            if (!Regex.IsMatch(hppTransactionRequest.TIMESTAMP, "[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]"))
            {
                throw new Exception("Invalid TIMESTAMP.");
            }

            if (string.IsNullOrWhiteSpace(hppTransactionRequest.MERCHANT_ID))
            {
                throw new Exception("Invalid MERCHANT_ID.");
            }

            if (string.IsNullOrWhiteSpace(hppTransactionRequest.ORDER_ID))
            {
                throw new Exception("Invalid ORDER_ID.");
            }

            if (string.IsNullOrWhiteSpace(hppTransactionRequest.CURRENCY))
            {
                throw new Exception("Invalid CURRENCY.");
            }

            // Using the sha-1 algorithm, hash a string made up of the request values
            // Format: timestamp.merchantid.orderid.amount.currency
            string hppBlueprint = string.Format("{0}.{1}.{2}.{3}.{4}", hppTransactionRequest.TIMESTAMP, hppTransactionRequest.MERCHANT_ID, hppTransactionRequest.ORDER_ID, hppTransactionRequest.AMOUNT, hppTransactionRequest.CURRENCY);
            string hppBlueprintHash = hppBlueprint.ToSha1HashString();

            return string.Format("{0}.{1}", hppBlueprintHash, secret).ToSha1HashString();
        }

        /// <summary>
        /// Creates a new HPP transaction request based on the provided parameters
        /// </summary>
        /// <param name="orderGuid">Order GUID</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="responseUrl">The URL under the store domain that will handle the response coming from the HPP</param>
        /// <param name="requestUrl">The URL for the request. This will differ based on the current setting.</param>
        public EcorenewElavonHppTransactionRequest CreateNewHppTransactionRequest(string orderGuid, int storeId, string responseUrl, out string requestUrl)
        {
            if (string.IsNullOrWhiteSpace(responseUrl))
                throw new ArgumentNullException("responseUrl");

            // Check if the order exists
            var order = _orderService.GetOrderByGuid(Guid.Parse(orderGuid));
            if (order == null)
                throw new ArgumentNullException("order");

            // Check if the store exists
            var store = _storeService.GetStoreById(storeId);
            if (store == null)
                throw new ArgumentNullException("storeId");

            var hppSettings = _settingService.LoadSetting<EcorenewElavonHppPaymentSettings>(storeId);

            if (hppSettings == null)
                throw new ArgumentNullException("hppSettings");

            requestUrl = hppSettings.UseSandbox ? "https://pay.sandbox.elavonpaymentgateway.com/pay" : "https://pay.elavonpaymentgateway.com/pay";

            // Get the total amount of the order. NOTE: Order total's currency is in primary currency.
            // To get the correct value, multiply the total to the currency rate
            decimal amountCustomerCurrency = order.OrderTotal * order.CurrencyRate;
            var customerCurrency = _currencyService.GetCurrencyByCode(order.CustomerCurrencyCode);

            var defaultHppCurrency = _currencyService.GetCurrencyByCode(hppSettings.DefaultCurrencyCode);
            if (defaultHppCurrency == null)
                throw new ArgumentNullException("defaultHppCurrency");

            decimal amountHppDefaultCurrency = _currencyService.ConvertCurrency(amountCustomerCurrency, customerCurrency, defaultHppCurrency);

            string amount = RoundingHelper.RoundPrice(hppSettings.UseCustomerCurrency ? amountCustomerCurrency : amountHppDefaultCurrency).ToString();
            var splitAmount = amount.Split('.');
            if(splitAmount.Length > 1)
            {
                amount = splitAmount[0] + splitAmount[1];
                
                // add trailing zeroes if not present
                for(int i = 0; i < (2 - splitAmount[1].Length); i++)
                {
                    amount = amount + "0";
                }
            }

            // Create new HPP transaction request
            var request = new EcorenewElavonHppTransactionRequest
            {
                ACCOUNT                 = hppSettings.Account,
                AMOUNT                  = amount,
                AUTO_SETTLE_FLAG        = hppSettings.AutoSettle,
                BILLING_CO              = order.BillingAddress.Country.TwoLetterIsoCode,
                BILLING_CODE            = order.BillingAddress.ZipPostalCode.ExtractNumber() + "|" + order.BillingAddress.Address1.ExtractNumber(),
                CARD_PAYMENT_BUTTON     = _localizationService.GetResource("Ecorenew.ElavonHppPayment.CardPaymentButton"),
                CURRENCY                = hppSettings.UseCustomerCurrency ? customerCurrency.CurrencyCode : defaultHppCurrency.CurrencyCode,
                CUST_NUM                = order.Customer.CustomerGuid.ToString(),
                HPP_LANG                = hppSettings.HppLanguageCode,
                HPP_VERSION             = hppSettings.HppVersion,
                MERCHANT_ID             = hppSettings.MerchantId,
                MERCHANT_RESPONSE_URL   = (store.SslEnabled ? store.SecureUrl : store.Url).TrimEnd('/') + "/" + responseUrl.TrimStart('/'),
                ORDER_ID                = order.OrderGuid.ToString(),
                SHIPPING_CO             = order.ShippingAddress.Country.TwoLetterIsoCode,
                SHIPPING_CODE           = order.ShippingAddress.ZipPostalCode,
                TIMESTAMP               = DateTime.UtcNow.ToString("yyyyMMddHHmmss")
            };

            request.CARD_PAYMENT_BUTTON = request.CARD_PAYMENT_BUTTON.ToLower() == "ecorenew.elavonhpppayment.cardpaymentbutton" ? "Pay" : request.CARD_PAYMENT_BUTTON;

            // Set SHA1HASH
            request.SHA1HASH = GetSha1Hash(request, hppSettings.Secret);

            return request;
        }

        /// <summary>
        /// Save a record of the HPP transaction response
        /// </summary>
        /// <param name="response">The HPP transaction response to be recorded</param>
        public void SaveElavonHppTransactionResponse(EcorenewElavonHppTransactionResponse response)
        {
            _elavonHppTransactionResponseRepository.Insert(response);
        }

        #endregion
    }
}