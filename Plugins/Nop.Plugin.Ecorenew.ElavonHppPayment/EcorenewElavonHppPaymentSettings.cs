﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Ecorenew.ElavonHppPayment
{
    public class EcorenewElavonHppPaymentSettings : ISettings
    {
        public bool UseSandbox { get; set; }
        public string Account { get; set; }
        public string MerchantId { get; set; }
        public string Secret { get; set; }
        public bool AutoSettle { get; set; }
        public string HppVersion { get; set; }
        public string HppLanguageCode { get; set; }
        public string DefaultCurrencyCode { get; set; }
        public bool UseCustomerCurrency { get; set; }
    }
}
