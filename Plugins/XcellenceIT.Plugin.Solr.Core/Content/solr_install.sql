--*************************************************************************
--*                                                                       *
--* nopAccelerate - Solr Integration Extension for nopCommerce            *
--* Copyright (c) Xcellence-IT. All Rights Reserved.                      *
--*                                                                       *
--*************************************************************************
--*                                                                       *
--* Email: info@nopaccelerate.com                                         *
--* Website: http://www.nopaccelerate.com                                 *
--*                                                                       *
--*************************************************************************
--*                                                                       *
--* This  software is furnished  under a license  and  may  be  used  and *
--* modified  only in  accordance with the terms of such license and with *
--* the  inclusion of the above  copyright notice.  This software or  any *
--* other copies thereof may not be provided or  otherwise made available *
--* to any  other  person.   No title to and ownership of the software is *
--* hereby transferred.                                                   *
--*                                                                       *
--* You may not reverse  engineer, decompile, defeat  license  encryption *
--* mechanisms  or  disassemble this software product or software product *
--* license.  Xcellence-IT may terminate this license if you don't comply *
--* with  any  of  the  terms and conditions set forth in  our  end  user *
--* license agreement (EULA).  In such event,  licensee  agrees to return *
--* licensor  or destroy  all copies of software  upon termination of the *
--* license.                                                              *
--*                                                                       *
--* Please see the  License file for the full End User License Agreement. *
--* The  complete license agreement is also available on  our  website at *
--* http://www.nopaccelerate.com/terms/                                   *
--*                                                                       *
--*************************************************************************

-- =============================================================================================
-- Author:		 XcellenceIT
-- Created By:	  Mayank Modi
-- Create date:   04-09-2015
-- Updated by:   Raju Paladiya
-- Updated date:  02-20-2018
-- Description:	
				  -- Creating all the tables/settings while installing the core plugin
				  -- Makes all products as pending in Incremental_Solr_Product 
-- =============================================================================================


-- Deleting [Incremental_Solr_Product] table if exists before creating because we don't need to keep track of records
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Incremental_Solr_Product'))
BEGIN
DROP TABLE [dbo].[Incremental_Solr_Product]
END

-- Creating [Incremental_Solr_Product] table
CREATE TABLE [dbo].[Incremental_Solr_Product] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[SolrStatus] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[InTime] [datetime] NULL,
	[StoreId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	PRIMARY KEY([Id])
);

-- Now Inserting products into [Incremental_Solr_Product] for solr indexing process
DECLARE @pId int
DECLARE @sId int

Truncate Table [dbo].[Incremental_Solr_Product]
	
Insert Into [dbo].[Incremental_Solr_Product] (ProductId, SolrStatus, IsDeleted, InTime, StoreId, LanguageId)
select distinct p.Id,1,0,GETDATE(),s.Id,l.Id from [dbo].[Store] s, [dbo].[Product] p, [dbo].[Language] as l
left join [dbo].[StoreMapping] sm on sm.EntityId = l.Id 
where (l.LimitedToStores=1 and s.Id in(sm.StoreId) and sm.EntityName='Language') or l.LimitedToStores=0 

DECLARE tempCursor CURSOR SCROLL FOR 
select p.Id,sm.StoreId from [dbo].[Product] as p
	   join [dbo].[StoreMapping] as sm on p.Id = sm.EntityId
	   join [dbo].[Store] as s on sm.StoreId=s.Id
	   where p.LimitedToStores=1 and sm.EntityName ='Product'
	
OPEN tempCursor 
FETCH FIRST FROM tempCursor
INTO @pId,@sId

WHILE @@fetch_status = 0   
 BEGIN    
    UPDATE [dbo].[Incremental_Solr_Product] SET IsDeleted=0
    WHERE ProductId=@pId AND StoreId=@sId
    
    FETCH NEXT FROM tempCursor
    INTO @pId, @sId;
 END
CLOSE tempCursor

UPDATE [dbo].[Incremental_Solr_Product]
Set IsDeleted=0
where ProductId in (Select p.Id from [dbo].[Product] as p where p.LimitedToStores=0)

UPDATE [dbo].[Incremental_Solr_Product]
Set IsDeleted=1
where ProductId in (Select p.Id from [dbo].[Product] as p where p.Published = 0 OR p.Deleted = 1 OR p.VisibleIndividually = 0)

DEALLOCATE tempCursor


-- Add SQL index to [Incremental_Solr_Product] table
CREATE NONCLUSTERED INDEX IX_Incremental_Solr_Product ON [dbo].[Incremental_Solr_Product] (ProductId,SolrStatus,IsDeleted)


-- Install Solr Schedule Task for [Incremental_Solr_Product] table solr indexing scheduler
IF NOT EXISTS ( SELECT * FROM [dbo].[ScheduleTask] WHERE Name = 'Solr IDI Process' )
INSERT Into [dbo].[ScheduleTask] (Name,Seconds,Type,Enabled,StopOnError,LastStartUtc,LastEndUtc,LastSuccessUtc) VALUES ('Solr IDI Process', 3600, 'XcellenceIT.Plugin.Solr.Core.Services.SolrScheduleTask', 1, 0, null, null, null)

-- Install Solr Schedule Task to write external file of popular products on solr Core
IF NOT EXISTS ( SELECT * FROM [dbo].[ScheduleTask] WHERE Name = 'UpdatePopularProducts' )
INSERT Into [dbo].[ScheduleTask] (Name,Seconds,Type,Enabled,StopOnError,LastStartUtc,LastEndUtc,LastSuccessUtc) VALUES ('UpdatePopularProducts', 86400, 'XcellenceIT.Plugin.Solr.Core.Services.PopularProductsTask', 0, 0, null, null, null)

-- Install Solr Schedule Task for [Incremental_Solr_Product] to add external products on solr Core
IF NOT EXISTS ( SELECT * FROM [dbo].[ScheduleTask] WHERE Name = 'InsertProductDataInIncrementalTable' )
INSERT Into [dbo].[ScheduleTask] (Name,Seconds,Type,Enabled,StopOnError,LastStartUtc,LastEndUtc,LastSuccessUtc) VALUES ('InsertProductDataInIncrementalTable', 86400, 'XcellenceIT.Plugin.Solr.Core.Services.InsertProductDataInIncrementalTableTask', 0, 0, null, null, null)

-- Insert Activity Log types
INSERT Into [dbo].[ActivityLogType] (SystemKeyword,Name,Enabled) 
VALUES ('SolrUtility','Start Indexing',1),
('AddSolrProduct','Add new product in solr',1),
('DeleteSolrProduct','Delete product from solr',1),
('AddSolrProductAPI','nopAccelerate API - Add new product',1),
('UpdateSolrProductAPI','nopAccelerate API - Update product',1),
('DeleteSolrProductAPI','nopAccelerate API - Delete product',1),
('SolrUtilityAPI','nopAccelerate API - Start Indexing',1)


-- Deleting [SolrLog] table if exists before creating because we don't need to keep track of log records
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SolrLog'))
BEGIN
DROP TABLE [dbo].[SolrLog]
END


-- Creating [SolrLog] table
CREATE TABLE [dbo].[SolrLog] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShortMessage] [nvarchar](max) NULL,
	[FullMessage] [nvarchar](max) NULL,
	[IpAddress] [nvarchar](200) NULL,
	[PageUrl] [nvarchar](max) NULL,
	[CreatedOnUtc] [datetime] NOT NULL,
	[StoreId] [int] NOT NULL,
	PRIMARY KEY([Id])
);


-- Check before creating [SolrCoreConfiguration] table
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SolrCoreConfiguration'))
BEGIN
CREATE TABLE [dbo].[SolrCoreConfiguration]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[CoreUrl] [nvarchar](500),
	PRIMARY KEY([Id])
);
END

-- Check before creating [nopAccelerateSearchTermLog] table
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'NopAccelerateSearchTermLog'))
BEGIN
CREATE TABLE [dbo].[NopAccelerateSearchTermLog]
(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SearchTerm] [nvarchar](450) NOT NULL,
	[CatID][int] NULL,
	[ManId][int] NULL,
    [StoreId][int] NOT NULL,
	[ResultCount][int] NOT NULL,
	[Clicks][int] NULL,
	[Latency][decimal](18,2) NOT NULL,
	[TermSearchTime][datetime] NOT NULL,
	PRIMARY KEY([Id])

);

-- Add SQL index to [NopAccelerateSearchTermLog] table
CREATE NONCLUSTERED INDEX IX_NopAccelerateSearchTermLog ON [dbo].[NopAccelerateSearchTermLog] (SearchTerm,CatID)

END



/****** Object:  StoredProcedure [dbo].[Solr_GetCategories]  ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Solr_GetCategories]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[Solr_GetCategories]
END


/****** Object:  StoredProcedure [dbo].[Solr_GetCategories]    Script Date: 4/25/2018 5:15:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:		 XcellenceIT
-- Create date:  09-02-2018
-- Description:	 Get Categories with language, store and ACL
-- Created by:   Raju Paladiya
-- =========================================================================

CREATE PROCEDURE [dbo].[Solr_GetCategories]
(
    @ShowHidden         BIT = 0,
    @Name               NVARCHAR(MAX) = NULL,
    @StoreId            INT = 0,
    @CustomerRoleIds	NVARCHAR(MAX) = NULL,  
	@LanguageId			INT = 0,
	@OnlyParentCategory	BIT = 0,
    @TotalRecords		INT = NULL OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON

	
    --filter by customer role IDs (access control list)
	SET @CustomerRoleIds = ISNULL(@CustomerRoleIds, '')
	CREATE TABLE #FilteredCustomerRoleIds
	(
		CustomerRoleId INT NOT NULL
	)
	INSERT INTO #FilteredCustomerRoleIds (CustomerRoleId)
	SELECT CAST(data AS INT) FROM [nop_splitstring_to_table](@CustomerRoleIds, ',')
	DECLARE @FilteredCustomerRoleIdsCount INT = (SELECT COUNT(1) FROM #FilteredCustomerRoleIds)

    --ordered categories
    CREATE TABLE #OrderedCategoryIds
	(
		[Id] int IDENTITY (1, 1) NOT NULL,
		[CategoryId] int NOT NULL,
		[CategoryName] nvarchar(MAX) NULL,
		[SlugURL] nvarchar(MAX) NULL
	)
    
    --get max length of DisplayOrder and Id columns (used for padding Order column)
    DECLARE @lengthId INT = (SELECT LEN(MAX(Id)) FROM [Category])
    DECLARE @lengthOrder INT = (SELECT LEN(MAX(DisplayOrder)) FROM [Category])
	if(@OnlyParentCategory = 0)
	BEGIN
		--get category tree
		;WITH [CategoryTree]
		AS (SELECT [Category].[Id] AS [Id], dbo.[nop_padright] ([Category].[DisplayOrder], '0', @lengthOrder) + '-' + dbo.[nop_padright] ([Category].[Id], '0', @lengthId) AS [Order]
			FROM [Category] WHERE [Category].[ParentCategoryId] = 0
			UNION ALL
			SELECT [Category].[Id] AS [Id], [CategoryTree].[Order] + '|' + dbo.[nop_padright] ([Category].[DisplayOrder], '0', @lengthOrder) + '-' + dbo.[nop_padright] ([Category].[Id], '0', @lengthId) AS [Order]
			FROM [Category]
			INNER JOIN [CategoryTree] ON [CategoryTree].[Id] = [Category].[ParentCategoryId])

		-- Insert records in table
		INSERT INTO #OrderedCategoryIds ([CategoryId], [CategoryName], [SlugURL] )
		SELECT cat.ID,cat.CATNAME,slug.SlugUrl FROM (
		select c.id,isnull(lp.LocaleValue,c.Name) CATNAME, c.Deleted, c.Published, c.LimitedToStores, SubjectToAcl from [Category] as c
		Left join LocalizedProperty LP 
		ON LP.LocaleKeyGroup='Category' and c.Id=lp.EntityId
		and LP.LanguageId=@LanguageId
		and Lp.LocaleKey ='Name'
		) cat,
		(SELECT U1T.EntityId,ISNULL(U2T.Slug,U1T.Slug) as SlugUrl FROM (
			select * from UrlRecord u1 WHERE u1.LanguageId = 0
		AND u1.EntityName='Category' and u1.IsActive = 1) U1T LEFT JOIN
		(select * from UrlRecord u2 WHERE u2.LanguageId =  @LanguageId
		AND u2.EntityName='Category' and u2.IsActive = 1) U2T
		ON U1T.EntityId = U2T.EntityId
		) slug 
		,[CategoryTree] 
		WHERE cat.Id = slug.EntityId
		and [CategoryTree].[Id] = cat.Id


		--filter results
		--WHERE [Category].[Deleted] = 0
		and  cat.[Deleted] = 0
		AND (@ShowHidden = 1 OR cat.[Published] = 1)
		AND (@Name IS NULL OR @Name = '' OR cat.CATNAME LIKE ('%' + @Name + '%'))
		AND (@ShowHidden = 1 OR @FilteredCustomerRoleIdsCount  = 0 OR cat.[SubjectToAcl] = 0
			OR EXISTS (SELECT 1 FROM #FilteredCustomerRoleIds [roles] WHERE [roles].[CustomerRoleId] IN
				(SELECT [acl].[CustomerRoleId] FROM [AclRecord] acl WITH (NOLOCK) WHERE [acl].[EntityId] = cat.[Id] AND [acl].[EntityName] = 'Category')
			)
		)
		AND (@StoreId = 0 OR cat.[LimitedToStores] = 0
			OR EXISTS (SELECT 1 FROM [StoreMapping] sm WITH (NOLOCK)
				WHERE [sm].[EntityId] = cat.[Id] AND [sm].[EntityName] = 'Category' AND [sm].[StoreId] = @StoreId
			)
		)
		ORDER BY ISNULL([CategoryTree].[Order], 1)
	END
	ELSE
	BEGIN
		;WITH [CategoryTree]
		AS (SELECT [Category].[Id] AS [Id], dbo.[nop_padright] ([Category].[DisplayOrder], '0', @lengthOrder) + '-' + dbo.[nop_padright] ([Category].[Id], '0', @lengthId) AS [Order]
			FROM [Category] WHERE [Category].[ParentCategoryId] = 0)

		-- Insert records in table
		INSERT INTO #OrderedCategoryIds ([CategoryId], [CategoryName], [SlugURL] )
		SELECT cat.ID,cat.CATNAME,slug.SlugUrl FROM (
		select c.id,isnull(lp.LocaleValue,c.Name) CATNAME, c.Deleted, c.Published, c.LimitedToStores, SubjectToAcl from [Category] as c
		Left join LocalizedProperty LP 
		ON LP.LocaleKeyGroup='Category' and c.Id=lp.EntityId
		and LP.LanguageId=@LanguageId
		and Lp.LocaleKey ='Name'
		where c.ParentCategoryId = 0
		) cat,
		(SELECT U1T.EntityId,ISNULL(U2T.Slug,U1T.Slug) as SlugUrl FROM (
			select * from UrlRecord u1 WHERE u1.LanguageId = 0 and u1.IsActive = 1
		AND u1.EntityName='Category') U1T LEFT JOIN
		(select * from UrlRecord u2 WHERE u2.LanguageId =  @LanguageId
		AND u2.EntityName='Category' and u2.IsActive = 1) U2T
		ON U1T.EntityId = U2T.EntityId
		) slug 
		,[CategoryTree] 
		WHERE cat.Id = slug.EntityId
		and [CategoryTree].[Id] = cat.Id
	

		--filter results
		--WHERE [Category].[Deleted] = 0
		and  cat.[Deleted] = 0
		AND (@ShowHidden = 1 OR cat.[Published] = 1)
		AND (@Name IS NULL OR @Name = '' OR cat.CATNAME LIKE ('%' + @Name + '%'))
		AND (@ShowHidden = 1 OR @FilteredCustomerRoleIdsCount  = 0 OR cat.[SubjectToAcl] = 0
			OR EXISTS (SELECT 1 FROM #FilteredCustomerRoleIds [roles] WHERE [roles].[CustomerRoleId] IN
				(SELECT [acl].[CustomerRoleId] FROM [AclRecord] acl WITH (NOLOCK) WHERE [acl].[EntityId] = cat.[Id] AND [acl].[EntityName] = 'Category')
			)
		)
		AND (@StoreId = 0 OR cat.[LimitedToStores] = 0
			OR EXISTS (SELECT 1 FROM [StoreMapping] sm WITH (NOLOCK)
				WHERE [sm].[EntityId] = cat.[Id] AND [sm].[EntityName] = 'Category' AND [sm].[StoreId] = @StoreId
			)
		)
		ORDER BY ISNULL([CategoryTree].[Order], 1)

	END

    --paging
    SELECT * FROM #OrderedCategoryIds AS [Result] -- INNER JOIN [Category] ON [Result].[CategoryId] = [Category].[Id]
   -- WHERE ([Result].[Id] > @PageSize * @PageIndex AND [Result].[Id] <= @PageSize * (@PageIndex + 1))
    ORDER BY [Result].[Id]

    DROP TABLE #FilteredCustomerRoleIds
    DROP TABLE #OrderedCategoryIds
END





/****** Object:  StoredProcedure [dbo].[Solr_GetManufacturer]    Script Date: 4/25/2018 5:13:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Solr_GetManufacturer]
(
    @ShowHidden         BIT = 0,
    @Name               NVARCHAR(MAX) = NULL,
    @StoreId            INT = 0,
    @CustomerRoleIds NVARCHAR(MAX) = NULL,  
 @LanguageId   INT = 0,
    @TotalRecords  INT = NULL OUTPUT
)
AS
BEGIN
 SET NOCOUNT ON

 
    --filter by customer role IDs (access control list)
 SET @CustomerRoleIds = ISNULL(@CustomerRoleIds, '')
 CREATE TABLE #FilteredCustomerRoleIds
 (
  CustomerRoleId INT NOT NULL
 )
 INSERT INTO #FilteredCustomerRoleIds (CustomerRoleId)
 SELECT CAST(data AS INT) FROM [nop_splitstring_to_table](@CustomerRoleIds, ',')
 DECLARE @FilteredCustomerRoleIdsCount INT = (SELECT COUNT(1) FROM #FilteredCustomerRoleIds)

    --ordered manufacturer
    CREATE TABLE #OrderedManufacturerIds
 (
  [Id] int IDENTITY (1, 1) NOT NULL,
  [ManufacturerId] int NOT NULL,
  [ManufacturerName] nvarchar(MAX) NULL,
  [SlugURL] nvarchar(MAX) NULL,
  [DisplayOrder] int NOT NULL
 )
    
    --get max length of DisplayOrder and Id columns (used for padding Order column)
    DECLARE @lengthId INT = (SELECT LEN(MAX(Id)) FROM [Manufacturer])
    DECLARE @lengthOrder INT = (SELECT LEN(MAX(DisplayOrder)) FROM [Manufacturer])

 -- fetch and insert record in table 
    INSERT INTO #OrderedManufacturerIds ([ManufacturerId], [ManufacturerName], [SlugURL], [DisplayOrder] )
    SELECT distinct man.ID,man.CATNAME,slug.SlugUrl,DisplayOrder FROM (
 select m.id,isnull(lp.LocaleValue,m.[Name]) CATNAME, m.Deleted, m.Published, m.LimitedToStores, m.SubjectToAcl, m.DisplayOrder from [Manufacturer] as m
 Left join LocalizedProperty LP 
 ON LP.LocaleKeyGroup='Manufacturer' and m.Id=lp.EntityId 
 and LP.LanguageId=@LanguageId and Lp.LocaleKey ='Name' 
 ) man,
 (SELECT U1T.EntityId,ISNULL(U2T.Slug,U1T.Slug) as SlugUrl FROM (
  select * from UrlRecord u1 WHERE u1.LanguageId = 0
 AND u1.EntityName='Manufacturer' and u1.IsActive = 1) U1T LEFT JOIN
 (select * from UrlRecord u2 WHERE u2.LanguageId =  @LanguageId
 AND u2.EntityName='Manufacturer' and u2.IsActive = 1) U2T
 ON U1T.EntityId = U2T.EntityId
 ) slug 
 
 WHERE man.Id = slug.EntityId

    --filter results   
    and  man.[Deleted] = 0
    AND (@ShowHidden = 1 OR man.[Published] = 1)
    AND (@Name IS NULL OR @Name = '' OR man.CATNAME LIKE ('%' + @Name + '%'))
    AND (@ShowHidden = 1 OR @FilteredCustomerRoleIdsCount  = 0 OR man.[SubjectToAcl] = 0
        OR EXISTS (SELECT 1 FROM #FilteredCustomerRoleIds [roles] WHERE [roles].[CustomerRoleId] IN
            (SELECT [acl].[CustomerRoleId] FROM [AclRecord] acl WITH (NOLOCK) WHERE [acl].[EntityId] = man.[Id] AND [acl].[EntityName] = 'Manufacturer')
        )
    )
    AND (@StoreId = 0 OR man.[LimitedToStores] = 0
        OR EXISTS (SELECT 1 FROM [StoreMapping] sm WITH (NOLOCK)
   WHERE [sm].[EntityId] = man.[Id] AND [sm].[EntityName] = 'Manufacturer' AND [sm].[StoreId] = @StoreId
  )
    )
    ORDER BY man.DisplayOrder

    --paging
    SELECT * FROM #OrderedManufacturerIds AS [Result] 
    ORDER BY [Result].[Id]

    DROP TABLE #FilteredCustomerRoleIds
    DROP TABLE #OrderedManufacturerIds
END
GO
