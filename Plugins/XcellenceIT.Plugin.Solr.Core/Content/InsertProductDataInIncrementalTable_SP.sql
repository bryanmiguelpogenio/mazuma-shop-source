--*************************************************************************
--*                                                                       *
--* nopAccelerate - Solr Integration Extension for nopCommerce            *
--* Copyright (c) Xcellence-IT. All Rights Reserved.                      *
--*                                                                       *
--*************************************************************************
--*                                                                       *
--* Email: info@nopaccelerate.com                                         *
--* Website: http://www.nopaccelerate.com                                 *
--*                                                                       *
--*************************************************************************
--*                                                                       *
--* This  software is furnished  under a license  and  may  be  used  and *
--* modified  only in  accordance with the terms of such license and with *
--* the  inclusion of the above  copyright notice.  This software or  any *
--* other copies thereof may not be provided or  otherwise made available *
--* to any  other  person.   No title to and ownership of the software is *
--* hereby transferred.                                                   *
--*                                                                       *
--* You may not reverse  engineer, decompile, defeat  license  encryption *
--* mechanisms  or  disassemble this software product or software product *
--* license.  Xcellence-IT may terminate this license if you don't comply *
--* with  any  of  the  terms and conditions set forth in  our  end  user *
--* license agreement (EULA).  In such event,  licensee  agrees to return *
--* licensor  or destroy  all copies of software  upon termination of the *
--* license.                                                              *
--*                                                                       *
--* Please see the  License file for the full End User License Agreement. *
--* The  complete license agreement is also available on  our  website at *
--* http://www.nopaccelerate.com/terms/				                      *
--*                                                                       *
--*************************************************************************

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertProductDataInIncrementalTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertProductDataInIncrementalTable]
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertProductDataInIncrementalTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertProductDataInIncrementalTable]
GO

/****** Object:  StoredProcedure [dbo].[InsertProductDataInIncrementalTable]    
Script Date: 10/10/2016 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:		 XcellenceIT
-- Create date:  08-12-2016
-- Description:	 Insert Product Data In Incremental Solr Product Table
-- Created by:   Kalpesh Boghara
-- Updated by:   Raju Paladiya
-- Updated date:  02-20-2018
-- =========================================================================
CREATE procedure [dbo].[InsertProductDataInIncrementalTable]
as
BEGIN
DECLARE @pId int
DECLARE @sId int

Truncate Table [dbo].[Incremental_Solr_Product]
	
Insert Into [dbo].[Incremental_Solr_Product] (ProductId, SolrStatus, IsDeleted, InTime, StoreId, LanguageId)
select distinct p.Id,1,0,GETDATE(),s.Id,l.Id from [dbo].[Store] s, [dbo].[Product] p, [dbo].[Language] as l
left join [dbo].[StoreMapping] sm on sm.EntityId = l.Id 
where (l.LimitedToStores=1 and s.Id in(sm.StoreId) and sm.EntityName='Language') or l.LimitedToStores=0 

DECLARE tempCursor CURSOR SCROLL FOR 
select p.Id,sm.StoreId from [dbo].[Product] as p
	   join [dbo].[StoreMapping] as sm on p.Id = sm.EntityId
	   join [dbo].[Store] as s on sm.StoreId=s.Id
	   where p.LimitedToStores=1 and sm.EntityName ='Product'
	
OPEN tempCursor 
FETCH FIRST FROM tempCursor
INTO @pId,@sId

WHILE @@fetch_status = 0   
 BEGIN    
    UPDATE [dbo].[Incremental_Solr_Product] SET IsDeleted=0
    WHERE ProductId=@pId AND StoreId=@sId
    
    FETCH NEXT FROM tempCursor
    INTO @pId, @sId;
 END
CLOSE tempCursor

UPDATE [dbo].[Incremental_Solr_Product]
Set IsDeleted=0
where ProductId in (Select p.Id from [dbo].[Product] as p where p.LimitedToStores=0)

UPDATE [dbo].[Incremental_Solr_Product]
Set IsDeleted=1
where ProductId in (Select p.Id from [dbo].[Product] as p where p.Published = 0 OR p.Deleted = 1 OR p.VisibleIndividually = 0)

DEALLOCATE tempCursor

END
