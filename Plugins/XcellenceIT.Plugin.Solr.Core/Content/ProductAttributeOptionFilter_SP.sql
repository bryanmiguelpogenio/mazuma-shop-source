--*************************************************************************
--*                                                                       *
--* nopAccelerate - Solr Integration Extension for nopCommerce            *
--* Copyright (c) Xcellence-IT. All Rights Reserved.                      *
--*                                                                       *
--*************************************************************************
--*                                                                       *
--* Email: info@nopaccelerate.com                                         *
--* Website: http://www.nopaccelerate.com                                 *
--*                                                                       *
--*************************************************************************
--*                                                                       *
--* This  software is furnished  under a license  and  may  be  used  and *
--* modified  only in  accordance with the terms of such license and with *
--* the  inclusion of the above  copyright notice.  This software or  any *
--* other copies thereof may not be provided or  otherwise made available *
--* to any  other  person.   No title to and ownership of the software is *
--* hereby transferred.                                                   *
--*                                                                       *
--* You may not reverse  engineer, decompile, defeat  license  encryption *
--* mechanisms  or  disassemble this software product or software product *
--* license.  Xcellence-IT may terminate this license if you don't comply *
--* with  any  of  the  terms and conditions set forth in  our  end  user *
--* license agreement (EULA).  In such event,  licensee  agrees to return *
--* licensor  or destroy  all copies of software  upon termination of the *
--* license.                                                              *
--*                                                                       *
--* Please see the  License file for the full End User License Agreement. *
--* The  complete license agreement is also available on  our  website at *
--* http://www.nopaccelerate.com/terms/				                      *
--*                                                                       *
--*************************************************************************

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductAttributeOptionFilter_ByCatId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ProductAttributeOptionFilter_ByCatId]
GO

/****** Object:  StoredProcedure [dbo].[ProductAttributeOptionFilter_ByCatId]    Script Date: 15-03-2016 AM 10:20:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =========================================================================
-- Author:		XcellenceIT
-- Create date: 15-03-2016
-- Description:	Store Procedure to get Product filters by category id 
-- Updated date: 07-03-2018
-- Updated By: Raju Paladiya
-- =========================================================================

CREATE PROCEDURE [dbo].[ProductAttributeOptionFilter_ByCatId]
(
	@AllCategoryId		varchar(max) ,
	@LanguageId			int = 0,
	@TotalRecords		int = null OUTPUT
)
AS
BEGIN
	
DECLARE @SP INT
DECLARE @CatId VARCHAR(100)

Create table #tempPA(Id int,Name varchar(max),DescriptionValue Varchar(max))
	WHILE PATINDEX('%' + ',' + '%', @AllCategoryId ) <> 0 
BEGIN
   SELECT  @SP = PATINDEX('%' + ',' + '%',@AllCategoryId)
   SELECT  @CatId = LEFT(@AllCategoryId , @SP - 1)
   SELECT  @AllCategoryId = STUFF(@AllCategoryId, 1, @SP, '')
  insert into #tempPA(Id,Name,DescriptionValue)

  (select pa.Id as IDd,
  isnull(lp.LocaleValue,pa.Name) PANAME,
  pa.Description 
  from Product_Category_Mapping as pcm

	join Product_ProductAttribute_Mapping as ppam on pcm.ProductId=ppam.ProductId
	join ProductAttribute as pa on ppam.ProductAttributeId =pa.Id
  
	Left join LocalizedProperty LP 
	ON LP.LocaleKeyGroup='ProductAttribute' 
	and LP.LanguageId=@LanguageId
	and pa.[Id] =LP.EntityId


  join Product as p on pcm.ProductId = p.Id
  where pcm.CategoryId=@CatId and p.Published=1 and p.Deleted=0
	)
END
	select DISTINCT Id,Name,DescriptionValue as 'Description'  from #tempPA
	drop table #tempPA
END

GO
