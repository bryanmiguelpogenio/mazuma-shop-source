--*************************************************************************
--*                                                                       *
--* nopAccelerate - Solr Integration Extension for nopCommerce            *
--* Copyright (c) Xcellence-IT. All Rights Reserved.                      *
--*                                                                       *
--*************************************************************************
--*                                                                       *
--* Email: info@nopaccelerate.com                                         *
--* Website: http://www.nopaccelerate.com                                 *
--*                                                                       *
--*************************************************************************
--*                                                                       *
--* This  software is furnished  under a license  and  may  be  used  and *
--* modified  only in  accordance with the terms of such license and with *
--* the  inclusion of the above  copyright notice.  This software or  any *
--* other copies thereof may not be provided or  otherwise made available *
--* to any  other  person.   No title to and ownership of the software is *
--* hereby transferred.                                                   *
--*                                                                       *
--* You may not reverse  engineer, decompile, defeat  license  encryption *
--* mechanisms  or  disassemble this software product or software product *
--* license.  Xcellence-IT may terminate this license if you don't comply *
--* with  any  of  the  terms and conditions set forth in  our  end  user *
--* license agreement (EULA).  In such event,  licensee  agrees to return *
--* licensor  or destroy  all copies of software  upon termination of the *
--* license.                                                              *
--*                                                                       *
--* Please see the  License file for the full End User License Agreement. *
--* The  complete license agreement is also available on  our  website at *
--* http://www.nopaccelerate.com/terms/				                      *
--*                                                                       *
--*************************************************************************

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpecificationAttributeOptionFilter_ByCatId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpecificationAttributeOptionFilter_ByCatId]
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpecificationAttrbuteOptionFilter_ByCatId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SpecificationAttrbuteOptionFilter_ByCatId]
GO

/****** Object:  StoredProcedure [dbo].[SpecificationAttributeOptionFilter_ByCatId]    Script Date: 04/29/2013 10:30:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:		XcellenceIT
-- Create date: 29-4-2013
-- Description:	Store Procedure to get specification filters by category id 
-- Update on:   07-03-2028
-- Update by:   Raju Paladiya
-- =========================================================================
CREATE  PROCEDURE [dbo].[SpecificationAttributeOptionFilter_ByCatId]
(
	@AllCategoryId		varchar(max) ,
	@LanguageId			int = 0,
	@TotalRecords		int = null OUTPUT
)
AS
BEGIN
	
DECLARE @SP INT
DECLARE @CatId VARCHAR(100)

Create table #temp(Id int,Name nvarchar(max),DisplayOrder int)
	WHILE PATINDEX('%' + ',' + '%', @AllCategoryId ) <> 0 
BEGIN
   SELECT  @SP = PATINDEX('%' + ',' + '%',@AllCategoryId)
   SELECT  @CatId = LEFT(@AllCategoryId , @SP - 1)
   SELECT  @AllCategoryId = STUFF(@AllCategoryId, 1, @SP, '')
  insert into #temp(Id,Name,DisplayOrder)
  (
	 select sa.Id, isnull(lp.LocaleValue,sa.Name) PANAME, sa.DisplayOrder from Product as p


	join Product_Category_Mapping as pc on pc.CategoryId= @CatId and pc.ProductId=p.Id
	join Product_SpecificationAttribute_Mapping as psa on p.Id=psa.ProductId
	join SpecificationAttributeOption as sao on psa.SpecificationAttributeOptionId=sao.Id
	join SpecificationAttribute as sa on sao.SpecificationAttributeId=sa.Id

	Left join LocalizedProperty LP 
	ON LP.LocaleKeyGroup='SpecificationAttribute' 
	and LP.LanguageId= @LanguageId
	and sa.[Id] =LP.EntityId

	)
END
	select DISTINCT * from #temp ORDER BY DisplayOrder DESC
	drop table #temp
END



GO


