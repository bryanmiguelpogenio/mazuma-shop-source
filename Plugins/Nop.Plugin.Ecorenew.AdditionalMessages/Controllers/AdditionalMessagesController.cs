﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Plugin.Ecorenew.AdditionalMessages.Domain;
using Nop.Plugin.Ecorenew.AdditionalMessages.Models;
using Nop.Plugin.Ecorenew.AdditionalMessages.Services;
using Nop.Services;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Extensions;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Linq;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Controllers
{
    public class AdditionalMessagesController : BasePluginController
    {
        #region Fields

        private readonly IAdditionalMessageTemplateService _additionalMessageTemplateService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly ISettingService _settingService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public AdditionalMessagesController(IAdditionalMessageTemplateService additionalMessageTemplateService,
            IEmailAccountService emailAccountService,
            ILanguageService languageService,
            ILocalizationService localizationService,
            IMessageTemplateService messageTemplateService,
            IStoreContext storeContext,
            IStoreService storeService,
            ISettingService settingService,
            IWorkContext workContext)
        {
            this._additionalMessageTemplateService = additionalMessageTemplateService;
            this._emailAccountService = emailAccountService;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._messageTemplateService = messageTemplateService;
            this._storeContext = storeContext;
            this._storeService = storeService;
            this._settingService = settingService;
            this._workContext = workContext;
        }

        #endregion

        #region Utilities

        protected virtual void PrepareAdditionalMessageTemplateModel(AdditionalMessageTemplateModel model)
        {
            model.AvailableEmailAccounts = _emailAccountService.GetAllEmailAccounts()
                .Select(x => x.ToModel())
                .ToList();
            model.AvailableEventTypes = AdditionalMessageTemplateEventType.OrderPaidEvent.ToSelectList().ToList();
        }

        #endregion

        #region Methods

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var settings = _settingService.LoadSetting<AdditionalMessagesPluginSettings>(storeScope);

            var model = new ConfigurationModel
            {
                ActiveStoreScopeConfiguration = storeScope,
                AdditionalMessagesEnabled = settings.AdditionalMessagesEnabled,
            };

            if (storeScope > 0)
            {
                model.AdditionalMessagesEnabled_OverrideForStore = _settingService.SettingExists(settings, x => x.AdditionalMessagesEnabled, storeScope);
            }

            return View("~/Plugins/Ecorenew.AdditionalMessages/Views/Configure.cshtml", model);
        }
        
        [HttpPost]
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var settings = _settingService.LoadSetting<AdditionalMessagesPluginSettings>(storeScope);

            //save settings
            settings.AdditionalMessagesEnabled = model.AdditionalMessagesEnabled;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(settings, x => x.AdditionalMessagesEnabled, model.AdditionalMessagesEnabled_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult List()
        {
            return View("~/Plugins/Ecorenew.AdditionalMessages/Views/List.cshtml");
        }

        [HttpPost]
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult List(DataSourceRequest command)
        {
            var additionalMessageTemplates = _additionalMessageTemplateService.GetAdditionalMessageTemplates(pageIndex: command.Page - 1, pageSize: command.PageSize, includeInactive: true);

            var gridModel = new DataSourceResult
            {
                Data = additionalMessageTemplates.Select(x =>
                {
                    return new
                    {
                        x.Id,
                        x.Name,
                        x.MessageTemplateName
                    };
                }),
                Total = additionalMessageTemplates.TotalCount
            };

            return Json(gridModel);
        }

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Create()
        {
            var model = new AdditionalMessageTemplateModel();

            PrepareAdditionalMessageTemplateModel(model);

            return View("~/Plugins/Ecorenew.AdditionalMessages/Views/Create.cshtml", model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Create(AdditionalMessageTemplateModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                var existingMessageTemplate = _messageTemplateService.GetMessageTemplateByName(model.MessageTemplateName, 0);
                if (existingMessageTemplate != null)
                {
                    ErrorNotification(_localizationService.GetResource("Plugins.Ecorenew.AdditionalMessages.MessageTemplateAlreadyExists"));
                }
                else
                {
                    // create new message template
                    _messageTemplateService.InsertMessageTemplate(new MessageTemplate
                    {
                        AttachedDownloadId = 0,
                        BccEmailAddresses = model.BccEmailAddresses,
                        Body = model.Body,
                        EmailAccountId = model.EmailAccountId,
                        IsActive = true,
                        LimitedToStores = false,
                        Name = model.MessageTemplateName,
                        Subject = model.Subject
                    });

                    var additionalMessageTemplate = new AdditionalMessageTemplate
                    {
                        CreatedByCustomerId = _workContext.CurrentCustomer.Id,
                        CreatedOnUtc = DateTime.UtcNow,
                        Enabled = model.Enabled,
                        EventTypeId = model.EventTypeId,
                        MessageTemplateName = model.MessageTemplateName,
                        Name = model.Name
                    };

                    _additionalMessageTemplateService.CreateAdditionalMessageTemplate(additionalMessageTemplate);

                    SuccessNotification(_localizationService.GetResource("Plugins.Ecorenew.AdditionalMessages.AdditionalMessageTemplateSaved"));

                    if (continueEditing)
                        return RedirectToAction("Edit", new { id = additionalMessageTemplate.Id });
                    else
                        return RedirectToAction("List");
                }
            }

            PrepareAdditionalMessageTemplateModel(model);

            return View("~/Plugins/Ecorenew.AdditionalMessages/Views/Create.cshtml", model);
        }

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Edit(int id)
        {
            var additionalMessageTemplate = _additionalMessageTemplateService.GetAdditionalMessageTemplateById(id);
            if (additionalMessageTemplate == null)
                return RedirectToAction("List");

            var messageTemplate = _messageTemplateService.GetMessageTemplateByName(additionalMessageTemplate.MessageTemplateName, _storeContext.CurrentStore.Id);
            if (messageTemplate == null)
                throw new Exception(_localizationService.GetResource("Plugins.Ecorenew.AdditionalMessages.MessageTemplateDoesNotExists"));
            
            var model = new AdditionalMessageTemplateModel
            {
                BccEmailAddresses = messageTemplate.BccEmailAddresses,
                Body = messageTemplate.Body,
                EmailAccountId = messageTemplate.EmailAccountId,
                Enabled = additionalMessageTemplate.Enabled,
                EventTypeId = additionalMessageTemplate.EventTypeId,
                MessageTemplateName = messageTemplate.Name,
                Subject = messageTemplate.Subject,
                Name = messageTemplate.Name
            };

            AddLocales(_languageService, model.Locales, (locale, languageId) =>
            {
                locale.BccEmailAddresses = messageTemplate.GetLocalized(x => x.BccEmailAddresses, languageId, false, false);
                locale.Body = messageTemplate.GetLocalized(x => x.Body, languageId, false, false);
                locale.EmailAccountId = messageTemplate.GetLocalized(x => x.EmailAccountId, languageId, false, false);
                locale.Subject = messageTemplate.GetLocalized(x => x.Subject, languageId, false, false);
            });

            PrepareAdditionalMessageTemplateModel(model);

            return View("~/Plugins/Ecorenew.AdditionalMessages/Views/Edit.cshtml", model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Edit(AdditionalMessageTemplateModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                var additionalMessageTemplate = _additionalMessageTemplateService.GetAdditionalMessageTemplateById(model.Id);
                if (additionalMessageTemplate == null)
                    return RedirectToAction("List");

                var messageTemplate = _messageTemplateService.GetMessageTemplateByName(additionalMessageTemplate.MessageTemplateName, _storeContext.CurrentStore.Id);
                if (messageTemplate == null)
                    throw new Exception(_localizationService.GetResource("Plugins.Ecorenew.AdditionalMessages.MessageTemplateDoesNotExists"));

                // update message template
                messageTemplate.BccEmailAddresses = model.BccEmailAddresses;
                messageTemplate.Body = model.Body;
                messageTemplate.EmailAccountId = model.EmailAccountId;
                messageTemplate.Name = model.MessageTemplateName;
                messageTemplate.Subject = model.Subject;

                _messageTemplateService.UpdateMessageTemplate(messageTemplate);

                additionalMessageTemplate.Enabled = model.Enabled;
                additionalMessageTemplate.EventTypeId = model.EventTypeId;
                additionalMessageTemplate.MessageTemplateName = model.MessageTemplateName;
                additionalMessageTemplate.Name = model.Name;

                _additionalMessageTemplateService.UpdateAdditionalMessageTemplate(additionalMessageTemplate);

                SuccessNotification(_localizationService.GetResource("Plugins.Ecorenew.AdditionalMessages.AdditionalMessageTemplateSaved"));

                if (continueEditing)
                    return RedirectToAction("Edit", additionalMessageTemplate.Id);
                else
                    return RedirectToAction("List");
            }

            PrepareAdditionalMessageTemplateModel(model);

            return View("~/Plugins/Ecorenew.AdditionalMessages/Views/Edit.cshtml", model);
        }

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Delete(int id)
        {
            var additionalMessageTemplate = _additionalMessageTemplateService.GetAdditionalMessageTemplateById(id);
            if (additionalMessageTemplate == null)
                return RedirectToAction("List");

            var messageTemplate = _messageTemplateService.GetMessageTemplateByName(additionalMessageTemplate.MessageTemplateName, _storeContext.CurrentStore.Id);
            if (messageTemplate == null)
                throw new Exception(_localizationService.GetResource("Plugins.Ecorenew.AdditionalMessages.MessageTemplateDoesNotExists"));

            _additionalMessageTemplateService.RemoveAdditionalMessageTemplate(additionalMessageTemplate);

            return RedirectToAction("List");
        }
        #endregion
    }
}
