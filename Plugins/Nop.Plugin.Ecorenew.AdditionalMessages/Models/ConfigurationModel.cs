﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.AdditionalMessages.Fields.AdditionalMessagesEnabled")]
        public bool AdditionalMessagesEnabled { get; set; }
        public bool AdditionalMessagesEnabled_OverrideForStore { get; set; }
    }
}
