﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Areas.Admin.Models.Messages;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Models
{
    public class AdditionalMessageTemplateModel : BaseNopEntityModel, ILocalizedModel<LocalizedMessageTemplateModel>, ILocalizedModel
    {
        public AdditionalMessageTemplateModel()
        {
            AvailableEventTypes = new List<SelectListItem>();
            Locales = new List<LocalizedMessageTemplateModel>();
            AvailableEmailAccounts = new List<EmailAccountModel>();
        }

        [NopResourceDisplayName("Plugins.Ecorenew.AdditionalMessages.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.AdditionalMessages.Fields.EventType")]
        public int EventTypeId { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.AdditionalMessages.Fields.Enabled")]
        public bool Enabled { get; set; }

        public IList<SelectListItem> AvailableEventTypes { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.AdditionalMessages.Fields.MessageTemplateName")]
        public string MessageTemplateName { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.BccEmailAddresses")]
        public string BccEmailAddresses { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.Subject")]
        public string Subject { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.Body")]
        public string Body { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.EmailAccount")]
        public int EmailAccountId { get; set; }

        public IList<LocalizedMessageTemplateModel> Locales { get; set; }

        public IList<EmailAccountModel> AvailableEmailAccounts { get; set; }
    }
}
