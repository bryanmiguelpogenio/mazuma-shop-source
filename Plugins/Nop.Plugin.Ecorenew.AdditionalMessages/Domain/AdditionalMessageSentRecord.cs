﻿using Nop.Core;
using System;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Domain
{
    public class AdditionalMessageSentRecord : BaseEntity
    {
        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the queued email identifier
        /// </summary>
        public int QueuedEmailId { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the entity has been created
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }
    }
}
