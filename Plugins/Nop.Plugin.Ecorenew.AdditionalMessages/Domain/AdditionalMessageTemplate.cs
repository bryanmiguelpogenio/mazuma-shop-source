﻿using Nop.Core;
using System;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Domain
{
    public class AdditionalMessageTemplate : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the additional message template it enabled
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the additional message template event type identifier
        /// This will determine when will the message be sent to the customer
        /// </summary>
        public int EventTypeId { get; set; }

        /// <summary>
        /// Gets or sets the message template name
        /// </summary>
        public string MessageTemplateName { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the customer who created the entity
        /// </summary>
        public int CreatedByCustomerId { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the entity is created
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the additional message template event type
        /// This will determine when will the message be sent to the customer
        /// </summary>
        public virtual AdditionalMessageTemplateEventType EventType
        {
            get { return (AdditionalMessageTemplateEventType)EventTypeId; }
            set { EventTypeId = (int)value; }
        }
    }
}
