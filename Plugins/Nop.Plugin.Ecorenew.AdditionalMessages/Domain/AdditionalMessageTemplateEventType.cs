﻿namespace Nop.Plugin.Ecorenew.AdditionalMessages.Domain
{
    public enum AdditionalMessageTemplateEventType
    {
        OrderPaidEvent = 1,
        OrderPlacedEvent = 2,
        OrderCancelledEvent = 3,
        OrderRefundedEvent = 4,
        CustomerRegisteredEvent = 5,
        ProductReviewApprovedEvent = 6,
        EmailSubscribedEvent = 7,
        EmailUnsubscribedEvent = 8,
        NewsCommentApprovedEvent = 9,
        ShipmentSentEvent = 10,
        ShipmentDeliveredEvent = 11
    }
}
