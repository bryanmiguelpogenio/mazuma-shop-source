﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.AdditionalMessages.Domain;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Data
{
    public class AdditionalMessageTemplateMap : NopEntityTypeConfiguration<AdditionalMessageTemplate>
    {
        public AdditionalMessageTemplateMap()
        {
            this.ToTable("AdditionalMessageTemplate");
            this.HasKey(x => x.Id);

            this.Property(x => x.MessageTemplateName).IsRequired().HasMaxLength(200);
            this.Ignore(x => x.EventType);
        }
    }
}
