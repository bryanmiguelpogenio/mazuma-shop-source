﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.AdditionalMessages.Domain;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Data
{
    public class AdditionalMessageSentRecordMap : NopEntityTypeConfiguration<AdditionalMessageSentRecord>
    {
        public AdditionalMessageSentRecordMap()
        {
            this.ToTable("AdditionalMessageSentRecord");
            this.HasKey(x => x.Id);

            this.Property(x => x.CustomerId).IsOptional();
        }
    }
}
