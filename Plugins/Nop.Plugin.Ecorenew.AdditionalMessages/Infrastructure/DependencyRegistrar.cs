﻿using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Ecorenew.AdditionalMessages.Data;
using Nop.Plugin.Ecorenew.AdditionalMessages.Domain;
using Nop.Plugin.Ecorenew.AdditionalMessages.Services;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Infrastructure
{
    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        #region Fields

        private const string DB_CONTEXT_NAME = "nop_object_context_ecorenew_additional_messages";

        #endregion

        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            // new services
            builder.RegisterType<CustomWorkflowMessageService>()
                .As<ICustomWorkflowMessageService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<AdditionalMessageTemplateService>()
                .As<IAdditionalMessageTemplateService>()
                .InstancePerLifetimeScope();

            //data context
            this.RegisterPluginDataContext<AdditionalMessagesObjectContext>(builder, DB_CONTEXT_NAME);

            //override required repository with our custom context
            builder.RegisterType<EfRepository<AdditionalMessageSentRecord>>()
                .As<IRepository<AdditionalMessageSentRecord>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(DB_CONTEXT_NAME))
                .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<AdditionalMessageTemplate>>()
                .As<IRepository<AdditionalMessageTemplate>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(DB_CONTEXT_NAME))
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 9; }
        }
    }
}
