﻿using Microsoft.AspNetCore.Routing;
using Nop.Core;
using Nop.Core.Plugins;
using Nop.Plugin.Ecorenew.AdditionalMessages.Data;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Web.Framework;
using Nop.Web.Framework.Menu;
using System.Linq;

namespace Nop.Plugin.Ecorenew.AdditionalMessages
{
    public class AdditionalMessagesPlugin : BasePlugin, IMiscPlugin, IAdminMenuPlugin
    {
        private readonly AdditionalMessagesObjectContext _objectContext;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;

        public AdditionalMessagesPlugin(AdditionalMessagesObjectContext objectContext, 
            ISettingService settingService,
            IWebHelper webHelper)
        {
            this._objectContext = objectContext;
            this._settingService = settingService;
            this._webHelper = webHelper;
        }
        public override void Install()
        {
            var settings = new AdditionalMessagesPluginSettings
            {
                AdditionalMessagesEnabled = false
            };
            _settingService.SaveSetting(settings);

            _objectContext.Install();

            // locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages.Fields.AdditionalMessagesEnabled", "Enabled");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages", "Additional Messages");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages.Fields.Name", "Name");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages.Fields.EventType", "Event Type");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages.Fields.Enabled", "Enabled");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages.Fields.MessageTemplateName", "Message Template Name");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages.BackToList", "Back to list");

            base.Install();
        }

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var mainNode = new SiteMapNode()
            {
                SystemName = "Ecorenew",
                Title = "EcoRenew Group",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
                IconClass = "fa-leaf"
            };

            var additionalMessagesNode = new SiteMapNode()
            {
                SystemName = "Ecorenew.AdditionalMessages",
                Title = "Additional Messages",
                ControllerName = "AdditionalMessages",
                ActionName = "List",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", AreaNames.Admin } },
                IconClass = "fa-genderless"
            };

            mainNode.ChildNodes.Add(additionalMessagesNode);

            var ecorenewNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Ecorenew");
            if (ecorenewNode != null)
                ecorenewNode.ChildNodes.Add(additionalMessagesNode);
            else
                rootNode.ChildNodes.Add(mainNode);
        }

        public override void Uninstall()
        {
            // settings
            _settingService.DeleteSetting<AdditionalMessagesPluginSettings>();

            _objectContext.Uninstall();

            // locales
            this.DeletePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages.Fields.AdditionalMessagesEnabled");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages.Fields.Name");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages.Fields.EventType");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages.Fields.Enabled");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages.Fields.MessageTemplateName");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.AdditionalMessages.BackToList");

            base.Uninstall();
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/AdditionalMessages/Configure";
        }
    }
}
