﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Plugin.Ecorenew.AdditionalMessages.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Services
{
    public class AdditionalMessageTemplateService : IAdditionalMessageTemplateService
    {
        #region Fields

        private readonly IRepository<AdditionalMessageTemplate> _additionalMessageTemplateRepository;

        #endregion

        #region Ctor

        public AdditionalMessageTemplateService(IRepository<AdditionalMessageTemplate> additionalMessageTemplateRepository)
        {
            this._additionalMessageTemplateRepository = additionalMessageTemplateRepository;
        }

        #endregion

        #region Methods

        public virtual IPagedList<AdditionalMessageTemplate> GetAdditionalMessageTemplates(int pageIndex = 0, int pageSize = Int32.MaxValue,
            IEnumerable<AdditionalMessageTemplateEventType> eventTypes = null,
            bool includeInactive = false)
        {
            var source = _additionalMessageTemplateRepository.Table;

            if (eventTypes != null && eventTypes.Any())
            {
                var eventTypeIds = eventTypes.Select(x => (int)x);
                source = source.Where(x => eventTypeIds.Contains(x.EventTypeId));
            }

            if (!includeInactive)
                source = source.Where(x => x.Enabled);

            source = source.OrderBy(x => x.Id);

            return new PagedList<AdditionalMessageTemplate>(source, pageIndex, pageSize);
        }

        public void CreateAdditionalMessageTemplate(AdditionalMessageTemplate additionalMessageTemplate)
        {
            _additionalMessageTemplateRepository.Insert(additionalMessageTemplate);
        }

        public AdditionalMessageTemplate GetAdditionalMessageTemplateById(int id)
        {
            return _additionalMessageTemplateRepository.GetById(id);
        }

        public void UpdateAdditionalMessageTemplate(AdditionalMessageTemplate additionalMessageTemplate)
        {
            _additionalMessageTemplateRepository.Update(additionalMessageTemplate);
        }

        public void RemoveAdditionalMessageTemplate(AdditionalMessageTemplate additionalMessageTemplate)
        {
            _additionalMessageTemplateRepository.Delete(additionalMessageTemplate);
        }
        #endregion
    }
}
