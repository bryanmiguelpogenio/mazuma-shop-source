﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.News;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Plugins;
using Nop.Plugin.Ecorenew.AdditionalMessages.Domain;
using Nop.Services.Events;
using Nop.Services.Logging;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Services
{
    public class EventConsumer : IConsumer<OrderPaidEvent>, 
        IConsumer<OrderPlacedEvent>, 
        IConsumer<OrderCancelledEvent>, 
        IConsumer<OrderRefundedEvent>,
        IConsumer<CustomerRegisteredEvent>,
        IConsumer<ProductReviewApprovedEvent>,
        IConsumer<EmailSubscribedEvent>,
        IConsumer<EmailUnsubscribedEvent>,
        IConsumer<NewsCommentApprovedEvent>,
        IConsumer<ShipmentSentEvent>,
        IConsumer<ShipmentDeliveredEvent>
    {
        #region Fields

        private readonly IAdditionalMessageTemplateService _additionalMessageTemplateService;
        private readonly ICustomWorkflowMessageService _customWorkflowMessageService;
        private readonly ILogger _logger;
        private readonly IPluginFinder _pluginFinder;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly AdditionalMessagesPluginSettings _additionalMessagesPluginSettings;

        #endregion

        #region Ctor

        public EventConsumer(IAdditionalMessageTemplateService additionalMessageTemplateService,
            ICustomWorkflowMessageService customWorkflowMessageService,
            ILogger logger,
            IPluginFinder pluginFinder,
            IStoreContext storeContext,
            IWorkContext workContext,
            AdditionalMessagesPluginSettings additionalMessagesPluginSettings)
        {
            this._additionalMessageTemplateService = additionalMessageTemplateService;
            this._customWorkflowMessageService = customWorkflowMessageService;
            this._logger = logger;
            this._pluginFinder = pluginFinder;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._additionalMessagesPluginSettings = additionalMessagesPluginSettings;
        }

        #endregion

        #region Utilities

        protected virtual bool IsPluginInstalled()
        {
            // check if the plugin is installed
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(AdditionalMessagesConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected virtual void SendAdditionalMessageTemplates(
            Customer customer = null,
            Order order = null,
            ProductReview productReview = null,
            NewsLetterSubscription newsLetterSubscription = null,
            NewsComment newsComment = null,
            Shipment shipment = null,
            IEnumerable<AdditionalMessageTemplateEventType> eventTypes = null)
        {

            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(AdditionalMessagesConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
                return;

            // check if the setting for custom export manager is enabled
            if (!_additionalMessagesPluginSettings.AdditionalMessagesEnabled)
                return;

            // get all messages with OrderPaidEvent event type
            var additionalMessageTemplates = _additionalMessageTemplateService.GetAdditionalMessageTemplates(eventTypes: eventTypes);

            foreach (var additionalMessageTemplate in additionalMessageTemplates)
            {
                _customWorkflowMessageService.SendAdditionalMessage(additionalMessageTemplate,
                    customer: customer,
                    order: order,
                    productReview: productReview,
                    newsLetterSubscription: newsLetterSubscription,
                    newsComment: newsComment,
                    shipment: shipment,
                    languageId: _workContext.WorkingLanguage.Id);
            }
        }

        #endregion

        #region Methods

        public void HandleEvent(OrderPaidEvent eventMessage)
        {
            if (IsPluginInstalled())
            {
                SendAdditionalMessageTemplates(
                    customer: eventMessage.Order.Customer,
                    order: eventMessage.Order,
                    eventTypes: new List<AdditionalMessageTemplateEventType> { AdditionalMessageTemplateEventType.OrderPaidEvent });
            }
        }

        public void HandleEvent(OrderPlacedEvent eventMessage)
        {
            if (IsPluginInstalled())
            {
                SendAdditionalMessageTemplates(
                    customer: eventMessage.Order.Customer,
                    order: eventMessage.Order,
                    eventTypes: new List<AdditionalMessageTemplateEventType> { AdditionalMessageTemplateEventType.OrderPlacedEvent });
            }
        }

        public void HandleEvent(OrderCancelledEvent eventMessage)
        {
            if (IsPluginInstalled())
            {
                SendAdditionalMessageTemplates(
                    customer: eventMessage.Order.Customer,
                    order: eventMessage.Order,
                    eventTypes: new List<AdditionalMessageTemplateEventType> { AdditionalMessageTemplateEventType.OrderCancelledEvent });
            }
        }

        public void HandleEvent(OrderRefundedEvent eventMessage)
        {
            if (IsPluginInstalled())
            {
                SendAdditionalMessageTemplates(
                    customer: eventMessage.Order.Customer,
                    order: eventMessage.Order,
                    eventTypes: new List<AdditionalMessageTemplateEventType> { AdditionalMessageTemplateEventType.OrderRefundedEvent });
            }
        }

        public void HandleEvent(CustomerRegisteredEvent eventMessage)
        {
            if (IsPluginInstalled())
            {
                SendAdditionalMessageTemplates(
                    customer: eventMessage.Customer,
                    eventTypes: new List<AdditionalMessageTemplateEventType> { AdditionalMessageTemplateEventType.CustomerRegisteredEvent });
            }
        }

        public void HandleEvent(ProductReviewApprovedEvent eventMessage)
        {
            if (IsPluginInstalled())
            {
                SendAdditionalMessageTemplates(
                    customer: eventMessage.ProductReview.Customer,
                    productReview: eventMessage.ProductReview,
                    eventTypes: new List<AdditionalMessageTemplateEventType> { AdditionalMessageTemplateEventType.ProductReviewApprovedEvent });
            }
        }

        public void HandleEvent(EmailSubscribedEvent eventMessage)
        {
            if (IsPluginInstalled())
            {
                SendAdditionalMessageTemplates(
                    newsLetterSubscription: eventMessage.Subscription,
                    eventTypes: new List<AdditionalMessageTemplateEventType> { AdditionalMessageTemplateEventType.EmailSubscribedEvent });
            }
        }

        public void HandleEvent(EmailUnsubscribedEvent eventMessage)
        {
            if (IsPluginInstalled())
            {
                SendAdditionalMessageTemplates(
                    newsLetterSubscription: eventMessage.Subscription,
                    eventTypes: new List<AdditionalMessageTemplateEventType> { AdditionalMessageTemplateEventType.EmailUnsubscribedEvent });
            }
        }

        public void HandleEvent(NewsCommentApprovedEvent eventMessage)
        {
            if (IsPluginInstalled())
            {
                SendAdditionalMessageTemplates(
                    customer: eventMessage.NewsComment.Customer,
                    newsComment: eventMessage.NewsComment,
                    eventTypes: new List<AdditionalMessageTemplateEventType> { AdditionalMessageTemplateEventType.NewsCommentApprovedEvent });
            }
        }

        public void HandleEvent(ShipmentSentEvent eventMessage)
        {
            if (IsPluginInstalled())
            {
                SendAdditionalMessageTemplates(
                    customer: eventMessage.Shipment.Order.Customer,
                    shipment: eventMessage.Shipment,
                    eventTypes: new List<AdditionalMessageTemplateEventType> { AdditionalMessageTemplateEventType.ShipmentSentEvent });
            }
        }

        public void HandleEvent(ShipmentDeliveredEvent eventMessage)
        {
            if (IsPluginInstalled())
            {
                SendAdditionalMessageTemplates(
                    customer: eventMessage.Shipment.Order.Customer,
                    shipment: eventMessage.Shipment,
                    eventTypes: new List<AdditionalMessageTemplateEventType> { AdditionalMessageTemplateEventType.ShipmentDeliveredEvent });
            }
        }

        #endregion
    }
}
