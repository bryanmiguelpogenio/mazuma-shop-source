﻿using Nop.Core;
using Nop.Plugin.Ecorenew.AdditionalMessages.Domain;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Services
{
    public interface IAdditionalMessageTemplateService
    {
        IPagedList<AdditionalMessageTemplate> GetAdditionalMessageTemplates(int pageIndex = 0, int pageSize = Int32.MaxValue,
            IEnumerable<AdditionalMessageTemplateEventType> eventTypes = null,
            bool includeInactive = false);

        void CreateAdditionalMessageTemplate(AdditionalMessageTemplate additionalMessageTemplate);

        void UpdateAdditionalMessageTemplate(AdditionalMessageTemplate additionalMessageTemplate);

        AdditionalMessageTemplate GetAdditionalMessageTemplateById(int id);

        void RemoveAdditionalMessageTemplate(AdditionalMessageTemplate additionalMessageTemplate);
    }
}