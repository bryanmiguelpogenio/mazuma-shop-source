﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.News;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Ecorenew.AdditionalMessages.Domain;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Services
{
    public interface ICustomWorkflowMessageService
    {
        int SendAdditionalMessage(AdditionalMessageTemplate additionalMessageTemplate, int languageId, 
            Customer customer = null, 
            Order order = null,
            ProductReview productReview = null,
            NewsLetterSubscription newsLetterSubscription = null,
            NewsComment newsComment = null,
            Shipment shipment = null);
    }
}