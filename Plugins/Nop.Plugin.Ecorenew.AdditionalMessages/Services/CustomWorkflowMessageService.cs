﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.News;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Ecorenew.AdditionalMessages.Domain;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.AdditionalMessages.Services
{
    public class CustomWorkflowMessageService : WorkflowMessageService, ICustomWorkflowMessageService
    {
        private readonly IRepository<AdditionalMessageSentRecord> _additionalMessageSentRecordRepository;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly IStoreContext _storeContext;
        private readonly IEventPublisher _eventPublisher;

        public CustomWorkflowMessageService(IRepository<AdditionalMessageSentRecord> additionalMessageSentRecordRepository,
            IMessageTemplateService messageTemplateService, 
            IQueuedEmailService queuedEmailService, 
            ILanguageService languageService, 
            ITokenizer tokenizer, 
            IEmailAccountService emailAccountService, 
            IMessageTokenProvider messageTokenProvider, 
            IStoreService storeService, 
            IStoreContext storeContext, 
            CommonSettings commonSettings, 
            EmailAccountSettings emailAccountSettings, 
            IEventPublisher eventPublisher) 
            : base(messageTemplateService, 
                  queuedEmailService, 
                  languageService, 
                  tokenizer, 
                  emailAccountService, 
                  messageTokenProvider, 
                  storeService, 
                  storeContext, 
                  commonSettings, 
                  emailAccountSettings, 
                  eventPublisher)
        {
            this._additionalMessageSentRecordRepository = additionalMessageSentRecordRepository;
            this._messageTokenProvider = messageTokenProvider;
            this._storeContext = storeContext;
            this._eventPublisher = eventPublisher;
        }

        public int SendAdditionalMessage(AdditionalMessageTemplate additionalMessageTemplate, int languageId,
            Customer customer = null,
            Order order = null,
            ProductReview productReview = null,
            NewsLetterSubscription newsLetterSubscription = null,
            NewsComment newsComment = null,
            Shipment shipment = null)
        {
            if (customer == null && newsLetterSubscription == null)
                throw new ArgumentNullException(nameof(customer));

            var store = _storeContext.CurrentStore;
            languageId = EnsureLanguageIsActive(languageId, store.Id);

            var messageTemplate = GetActiveMessageTemplate(additionalMessageTemplate.MessageTemplateName, store.Id);
            if (messageTemplate == null)
                return 0;

            //email account
            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

            //tokens
            var tokens = new List<Token>();

            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);

            if (customer != null)
                _messageTokenProvider.AddCustomerTokens(tokens, customer);
            if (order != null)
                _messageTokenProvider.AddOrderTokens(tokens, order, languageId);
            if (productReview != null)
                _messageTokenProvider.AddProductReviewTokens(tokens, productReview);
            if (newsLetterSubscription != null)
                _messageTokenProvider.AddNewsLetterSubscriptionTokens(tokens, newsLetterSubscription);
            if (newsComment != null)
                _messageTokenProvider.AddNewsCommentTokens(tokens, newsComment);
            if (shipment != null)
                _messageTokenProvider.AddShipmentTokens(tokens, shipment, languageId);

            //event notification
            _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

            //var toEmail = newsLetterSubscription != null ? newsLetterSubscription.Email : customer.Email;
            //var toName = newsLetterSubscription != null ? newsLetterSubscription.Email : customer.GetFullName();

            var toEmail = order.BillingAddress.Email;
            var toName = $"{order.BillingAddress.FirstName} {order.BillingAddress.LastName}";

            int queuedEmailId = SendNotification(messageTemplate, emailAccount, languageId, tokens, toEmail, toName);

            _additionalMessageSentRecordRepository.Insert(new AdditionalMessageSentRecord
            {
                CreatedOnUtc = DateTime.UtcNow,
                CustomerId = newsLetterSubscription != null ? null : (int?)customer.Id,
                QueuedEmailId = queuedEmailId,
                StoreId = store.Id
            });

            return queuedEmailId;
        }
    }
}
