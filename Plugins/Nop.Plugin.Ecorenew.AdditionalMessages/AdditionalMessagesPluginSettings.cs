﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Ecorenew.AdditionalMessages
{
    public class AdditionalMessagesPluginSettings : ISettings
    {
        public bool AdditionalMessagesEnabled { get; set; }
    }
}
