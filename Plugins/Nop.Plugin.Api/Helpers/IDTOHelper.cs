﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Stores;
using Nop.Plugin.Api.DTOs.Categories;
using Nop.Plugin.Api.DTOs.GiftCards;
using Nop.Plugin.Api.DTOs.Languages;
using Nop.Plugin.Api.DTOs.MazumaWiFi;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Plugin.Api.DTOs.ProductAttributes;
using Nop.Plugin.Api.DTOs.Products;
using Nop.Plugin.Api.DTOs.ReturnRequests;
using Nop.Plugin.Api.DTOs.ShoppingCarts;
using Nop.Plugin.Api.DTOs.Stores;
using Nop.Plugin.Mazuma.WiFiVoucher.Domain;

namespace Nop.Plugin.Api.Helpers
{
    public interface IDTOHelper
    {
        ProductDto PrepareProductDTO(Product product);
        CategoryDto PrepareCategoryDTO(Category category);
        OrderDto PrepareOrderDTO(Order order);
        ShoppingCartItemDto PrepareShoppingCartItemDTO(ShoppingCartItem shoppingCartItem);
        OrderItemDto PrepareOrderItemDTO(OrderItem orderItem);
        StoreDto PrepareStoreDTO(Store store);
        LanguageDto PrepateLanguageDto(Language language);
        ProductAttributeDto PrepareProductAttributeDTO(ProductAttribute productAttribute);

        #region Added by EcoRenew

        ReturnRequestDto PrepareReturnRequestDTO(ReturnRequest returnRequest);
        ReturnRequestReportDto PrepareReturnRequestReportDto(ReturnRequest returnRequest);

        MazumaWiFiVoucherDto PrepareMazumaWiFiVoucherDto(MazumaWiFiVoucher mazumaWiFiVoucher);

        GiftCardResponseDto PrepareGiftCardDto(GiftCard giftCard);
        #endregion
    }
}
