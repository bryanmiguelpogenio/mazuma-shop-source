﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.GiftCards
{
    public class GiftCardsRootObject : ISerializableObject
    {
        public GiftCardsRootObject()
        {
            GiftCards = new List<GiftCardResponseDto>();
        }

        [JsonProperty("gift_cards")]
        public IList<GiftCardResponseDto> GiftCards { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "gift_cards";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(GiftCardResponseDto);
        }
    }
}
