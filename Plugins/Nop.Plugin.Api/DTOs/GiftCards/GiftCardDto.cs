﻿using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Plugin.Api.Validators;
using System;

namespace Nop.Plugin.Api.DTOs.GiftCards
{
    [JsonObject(Title = "gift_card")]
    [Validator(typeof(GiftCardDtoValidator))]
    public class GiftCardDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("is_gift_card_activated")]
        public bool IsGiftCardActivated { get; set; }

        [JsonProperty("gift_card_coupon_code")]
        public string GiftCardCouponCode { get; set; }

        [JsonProperty("recipient_name")]
        public string RecipientName { get; set; }

        [JsonProperty("recipient_email")]
        public string RecipientEmail { get; set; }

        [JsonProperty("sender_name")]
        public string SenderName { get; set; }

        [JsonProperty("sender_email")]
        public string SenderEmail { get; set; }

        [JsonProperty("has_duration_period")]
        public bool HasDurationPeriod { get; set; }

        [JsonProperty("start_date")]
        public DateTime? StartDateUtc { get; set; }

        [JsonProperty("end_date")]
        public DateTime? EndDateUtc { get; set; }

        [JsonProperty("sell_trade_number")]
        public string SellTradeNumber { get; set; }
    }
}
