﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.GiftCards
{
    public class GiftCardsCountRootObject
    {
        [JsonProperty("count")]
        public int Count { get; set; }
    }
}
