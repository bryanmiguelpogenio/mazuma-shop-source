﻿using Newtonsoft.Json;
using Nop.Core.Domain.Orders;

namespace Nop.Plugin.Api.DTOs.Orders
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "order_order_status")]
    public class OrderOrderStatusDto
    {
        [JsonProperty("order_id")]
        public int OrderId { get; set; }

        [JsonProperty("order_status")]
        public string OrderStatus { get; set; }
    }
}