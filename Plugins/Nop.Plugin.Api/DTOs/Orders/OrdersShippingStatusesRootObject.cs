﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.Orders
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "orders_shipping_statuses_root")]
    public class OrdersShippingStatusesRootObject : ISerializableObject
    {
        public OrdersShippingStatusesRootObject()
        {
            OrdersShippingStatuses = new List<OrderShippingStatusDto>();
        }

        [JsonProperty("orders_shipping_statuses")]
        public IList<OrderShippingStatusDto> OrdersShippingStatuses { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "orders_shipping_statuses";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderShippingStatusDto);
        }
    }
}