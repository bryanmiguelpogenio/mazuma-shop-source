﻿using Newtonsoft.Json;
using System;

namespace Nop.Plugin.Api.DTOs.Orders
{
    [JsonObject(Title = "gift_card_usage_history")]
    public class GiftCardUsageHistoryDto
    {
        /// <summary>
        /// Gets or sets the identifier
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the gift card identifier
        /// </summary>
        [JsonProperty("gift_card_id")]
        public int GiftCardId { get; set; }

        /// <summary>
        /// Gets or sets the order identifier
        /// </summary>
        [JsonProperty("used_with_order_id")]
        public int UsedWithOrderId { get; set; }

        /// <summary>
        /// Gets or sets the used value (amount)
        /// </summary>
        [JsonProperty("used_value")]
        public decimal UsedValue { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        [JsonProperty("created_on_utc")]
        public DateTime? CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the gift card coupon code
        /// </summary>
        [JsonProperty("gift_card_coupon_code")]
        public string GiftCardCouponCode { get; set; }

        /// <summary>
        /// Gets or sets the gift card coupon code
        /// </summary>
        [JsonProperty("gift_card_recipient_name")]
        public string GiftCardRecipientName { get; set; }
    }
}
