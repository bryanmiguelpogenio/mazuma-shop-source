﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Orders
{
    [JsonObject(Title = "order_shipping_status")]
    public class OrderShippingStatusDto
    {
        [JsonProperty("order_id")]
        public int OrderId { get; set; }

        [JsonProperty("shipping_status")]
        public string ShippingStatus { get; set; }
    }
}