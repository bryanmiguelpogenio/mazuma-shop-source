﻿using Newtonsoft.Json;
using System;

namespace Nop.Plugin.Api.DTOs.Orders
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "synchronized_order")]
    public class SynchronizedOrderDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("synchronized_on_utc")]
        public DateTime SynchronizedOnUtc { get; set; }
    }
}