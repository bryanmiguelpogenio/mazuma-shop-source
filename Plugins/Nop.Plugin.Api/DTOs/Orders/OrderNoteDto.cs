﻿using Newtonsoft.Json;
using System;

namespace Nop.Plugin.Api.DTOs.Orders
{
    /// <remarks>
    /// Added by EcoRenew
    /// </remarks>
    [JsonObject(Title = "order_note")]
    public class OrderNoteDto
    {
        /// <summary>
        /// Gets or sets the id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the order identifier
        /// </summary>
        [JsonProperty("order_id")]
        public int OrderId { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("download_id")]
        public int? DownloadId { get; set; }

        [JsonProperty("display_to_customer")]
        public bool DisplayToCustomer { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        [JsonProperty("created_on_utc")]
        public DateTime? CreatedOnUtc { get; set; }
    }
}