﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.Orders
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "orders_order_statuses_root")]
    public class OrdersOrderStatusesRootObject : ISerializableObject
    {
        public OrdersOrderStatusesRootObject()
        {
            OrdersOrderStatuses = new List<OrderOrderStatusDto>();
        }

        [JsonProperty("orders_order_statuses")]
        public IList<OrderOrderStatusDto> OrdersOrderStatuses { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "orders_order_statuses";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderOrderStatusDto);
        }
    }
}