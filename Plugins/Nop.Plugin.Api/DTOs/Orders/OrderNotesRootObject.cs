﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.Orders
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "order_notes_root")]
    public class OrderNotesRootObject : ISerializableObject
    {
        public OrderNotesRootObject()
        {
            OrderNotes = new List<OrderNoteDto>();
        }

        [JsonProperty("order_notes")]
        public IList<OrderNoteDto> OrderNotes { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "order_notes";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderNoteDto);
        }
    }
}