﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.Orders
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "synchronized_orders_root")]
    public class SynchronizedOrdersRootObject : ISerializableObject
    {
        public SynchronizedOrdersRootObject()
        {
            SynchronizedOrders = new List<SynchronizedOrderDto>();
        }

        [JsonProperty("synchronized_orders")]
        public IList<SynchronizedOrderDto> SynchronizedOrders { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "synchronized_orders";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(SynchronizedOrderDto);
        }
    }
}