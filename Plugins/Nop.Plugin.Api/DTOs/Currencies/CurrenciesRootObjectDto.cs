﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.Currencies
{
    public class CurrenciesRootObjectDto : ISerializableObject
    {
        public CurrenciesRootObjectDto()
        {
            Currencies = new List<CurrencyDto>();
        }

        [JsonProperty("currencies")]
        public IList<CurrencyDto> Currencies { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "currencies";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(CurrencyDto);
        }
    }
}