﻿using Newtonsoft.Json;
using System;

namespace Nop.Plugin.Api.DTOs.Currencies
{
    [JsonObject(Title = "currency")]
    public class CurrencyDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("currency_code")]
        public string CurrencyCode { get; set; }

        [JsonProperty("rate")]
        public decimal Rate { get; set; }

        [JsonProperty("published")]
        public bool Published { get; set; }

        [JsonProperty("updated_on_utc")]
        public DateTime UpdatedOnUtc { get; set; }
    }
}