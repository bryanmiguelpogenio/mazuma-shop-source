﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.BrainTreePayPalCredit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.BrainTreePayPal
{
    public class OrderPayPalCreditFinancingDetailsRootObjectDto : ISerializableObject
    {
        public OrderPayPalCreditFinancingDetailsRootObjectDto()
        {
            FinancingDetails = new List<OrderPayPalCreditFinancingDetailsDto>();
        }

        [JsonProperty("braintree_paypal_financing_details")]
        public IList<OrderPayPalCreditFinancingDetailsDto> FinancingDetails { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "braintree_paypal_financing_details";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderPayPalCreditFinancingDetailsDto);
        }
    }
}
