﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.BrainTreePayPalCredit
{
    [JsonObject(Title="financing_details_item")]
    public class OrderPayPalCreditFinancingDetailsDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("order_id")]
        public int OrderId { get; set; }

        [JsonProperty("total_cost_value")]
        public string TotalCostValue { get; set; }

        [JsonProperty("total_cost_currency")]
        public string TotalCostCurrency { get; set; }

        [JsonProperty("term")]
        public int Term { get; set; }

        [JsonProperty("monthly_payment_value")]
        public string MonthlyPaymentValue { get; set; }

        [JsonProperty("monthly_payment_currency")]
        public string MonthlyPaymentCurrency { get; set; }

        [JsonProperty("total_interest_value")]
        public string TotalInterestValue { get; set; }

        [JsonProperty("total_interest_currency")]
        public string TotalInterestCurrency { get; set; }

        [JsonProperty("payer_acceptance")]
        public bool PayerAcceptance { get; set; }

        [JsonProperty("cart_amount_immutable")]
        public bool CartAmountImmutable { get; set; }
    }
}
