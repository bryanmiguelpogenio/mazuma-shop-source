﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.BrainTreePayPal
{
    public class OrderPayPalCreditFinancingDetailsCountRootObject
    {
        [JsonProperty("count")]
        public int Count { get; set; }
    }
}
