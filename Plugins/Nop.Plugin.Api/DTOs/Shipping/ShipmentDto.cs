﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.Shipping
{
    [JsonObject("shipment")]
    public class ShipmentDto
    {
        private ICollection<ShipmentItemDto> _shipmentItemDtos;

        /// <summary>
        /// Gets or sets the order identifier
        /// </summary>
        [JsonProperty("order_id")]
        public int OrderId { get; set; }

        /// <summary>
        /// Gets or sets the tracking number of this shipment
        /// </summary>
        [JsonProperty("tracking_number")]
        public string TrackingNumber { get; set; }

        /// <summary>
        /// Gets or sets the total weight of this shipment
        /// It's nullable for compatibility with the previous version of nopCommerce where was no such property
        /// </summary>
        [JsonProperty("total_weight")]
        public decimal? TotalWeight { get; set; }

        /// <summary>
        /// Gets or sets the shipped date and time
        /// </summary>
        [JsonProperty("shipped_date_utc")]
        public DateTime? ShippedDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the delivery date and time
        /// </summary>
        [JsonProperty("delivery_date_utc")]
        public DateTime? DeliveryDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the admin comment
        /// </summary>
        [JsonProperty("admin_comment")]
        public string AdminComment { get; set; }

        /// <summary>
        /// Gets or sets the entity creation date
        /// </summary>
        [JsonProperty("created_on_utc")]
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the shipment items
        /// </summary>
        [JsonProperty("shipment_items")]
        public virtual ICollection<ShipmentItemDto> ShipmentItems
        {
            get { return _shipmentItemDtos ?? (_shipmentItemDtos = new List<ShipmentItemDto>()); }
            protected set { _shipmentItemDtos = value; }
        }
    }
}