﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Shipping
{
    [JsonObject(Title = "shipment_item")]
    public class ShipmentItemDto
    {
        /// <summary>
        /// Gets or sets the shipment identifier
        /// </summary>
        [JsonProperty("shipment_id")]
        public int ShipmentId { get; set; }

        /// <summary>
        /// Gets or sets the order item identifier
        /// </summary>
        [JsonProperty("order_item_id")]
        public int OrderItemId { get; set; }

        /// <summary>
        /// Gets or sets the quantity
        /// </summary>
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the warehouse identifier
        /// </summary>
        [JsonProperty("warehouse_id")]
        public int WarehouseId { get; set; }
    }
}