﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.Shipping
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "shipment_delivery_dates_root")]
    public class ShipmentDeliveryDatesRootObject : ISerializableObject
    {
        public ShipmentDeliveryDatesRootObject()
        {
            ShipmentDeliveryDates = new List<ShipmentDeliveryDateDto>();
        }

        [JsonProperty("shipment_delivery_dates")]
        public IList<ShipmentDeliveryDateDto> ShipmentDeliveryDates { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "shipment_delivery_dates";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ShipmentDeliveryDateDto);
        }
    }
}