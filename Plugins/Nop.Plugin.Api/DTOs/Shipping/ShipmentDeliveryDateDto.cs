﻿using Newtonsoft.Json;
using System;

namespace Nop.Plugin.Api.DTOs.Shipping
{
    [JsonObject("shipment_delivery_date")]
    public class ShipmentDeliveryDateDto
    {
        /// <summary>
        /// Gets or sets the tracking number of this shipment
        /// </summary>
        [JsonProperty("tracking_number")]
        public string TrackingNumber { get; set; }

        /// <summary>
        /// Gets or sets the delivery date and time
        /// </summary>
        [JsonProperty("delivery_date_utc")]
        public DateTime? DeliveryDateUtc { get; set; }
    }
}