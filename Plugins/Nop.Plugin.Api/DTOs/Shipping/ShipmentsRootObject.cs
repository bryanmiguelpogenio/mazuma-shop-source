﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.Shipping
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "shipments_root")]
    public class ShipmentsRootObject : ISerializableObject
    {
        public ShipmentsRootObject()
        {
            Shipments = new List<ShipmentDto>();
        }

        [JsonProperty("shipments")]
        public IList<ShipmentDto> Shipments { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "shipments";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ShipmentDto);
        }
    }
}