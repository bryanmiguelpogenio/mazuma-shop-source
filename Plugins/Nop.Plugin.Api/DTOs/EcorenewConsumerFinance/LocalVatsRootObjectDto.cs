﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.EcorenewConsumerFinance
{
    public class LocalVatsRootObjectDto : ISerializableObject
    {
        public LocalVatsRootObjectDto()
        {
            LocalVats = new List<LocalVatDto>();
        }

        [JsonProperty("local_vats")]
        public IList<LocalVatDto> LocalVats { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "local_vats";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(LocalVatDto);
        }
    }
}