﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.EcorenewConsumerFinance
{
    [JsonObject("consumer_finance_order_item")]
    public class ConsumerFinanceOrderItemDto
    {
        /// <summary>
        /// Gets or set the id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the consumer finance signup identifier
        /// </summary>
        [JsonProperty("consumer_finance_signup_id")]
        public int ConsumerFinanceSignupId { get; set; }

        /// <summary>
        /// Gets or sets the shopping cart item identifier
        /// </summary>
        [JsonProperty("shopping_cart_item_id")]
        public int? ShoppingCartItemId { get; set; }

        /// <summary>
        /// Gets or sets the order item identifier
        /// </summary>
        [JsonProperty("order_item_id")]
        public int? OrderItemId { get; set; }

        /// <summary>
        /// Gets or sets the duration of agreement
        /// </summary>
        [JsonProperty("duration_of_agreement")]
        public int DurationOfAgreement { get; set; }

        /// <summary>
        /// Gets or sets the product identifier
        /// </summary>
        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the product attributes XML
        /// </summary>
        [JsonProperty("product_attributes_xml")]
        public string ProductAttributesXml { get; set; }

        /// <summary>
        /// Gets or sets the product attributes desctription
        /// </summary>
        [JsonProperty("product_attribute_description")]
        public string ProductAttributeDescription { get; set; }

        /// <summary>
        /// Gets or sets the SKU of the product
        /// </summary>
        [JsonProperty("product_sku")]
        public string ProductSku { get; set; }

        /// <summary>
        /// Gets or sets the currency code
        /// </summary>
        [JsonProperty("currency_code")]
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Gets or sets the product price (including TAX)
        /// </summary>
        [JsonProperty("product_price_incl_tax")]
        public decimal ProductPriceInclTax { get; set; }

        /// <summary>
        /// Gets or sets the quantity of the product
        /// </summary>
        [JsonProperty("product_quantity")]
        public int ProductQuantity { get; set; }

        /// <summary>
        /// Gets or sets the tax rate
        /// </summary>
        [JsonProperty("tax_rate")]
        public decimal TaxRate { get; set; }

        /// <summary>
        /// Gets or sets the initial payment amount
        /// </summary>
        [JsonProperty("initial_payment")]
        public decimal InitialPayment { get; set; }

        /// <summary>
        /// Gets or sets the monthly payment amount
        /// </summary>
        [JsonProperty("monthly_payment")]
        public decimal MonthlyPayment { get; set; }

        /// <summary>
        /// Gets or sets the documentation fee
        /// </summary>
        [JsonProperty("documentation_fee")]
        public decimal DocumentationFee { get; set; }

        /// <summary>
        /// Gets or sets the agreement rate
        /// </summary>
        [JsonProperty("agreement_rate")]
        public decimal AgreementRate { get; set; }

        /// <summary>
        /// Gets or sets the total credit / finance changes
        /// </summary>
        [JsonProperty("total_credit_or_finance_charges")]
        public decimal TotalCreditOrFinanceCharges { get; set; }

        /// <summary>
        /// Gets or sets the total payable
        /// </summary>
        [JsonProperty("total_payable")]
        public decimal TotalPayable { get; set; }

        /// <summary>
        /// Gets or sets the APR
        /// </summary>
        [JsonProperty("apr")]
        public decimal Apr { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the consumer finance CAAP record
        /// </summary>
        [JsonProperty("consumer_finance_caap_record_id")]
        public int? ConsumerFinanceCaapRecordId { get; set; }
    }
}