﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.EcorenewConsumerFinance
{
    [JsonObject(Title = "consumer_finance_signup")]
    public class ConsumerFinanceSignupDto
    {
        private ICollection<ConsumerFinanceOrderItemDto> _orderItemDtos;

        /// <summary>
        /// Gets or set the id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        [JsonProperty("customer_id")]
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the store identifier
        /// </summary>
        [JsonProperty("store_id")]
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the date and time the entity is created
        /// </summary>
        [JsonProperty("created_on_utc")]
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the order identifier
        /// </summary>
        [JsonProperty("order_id")]
        public int? OrderId { get; set; }

        /// <summary>
        /// Gets or sets the order items for financing
        /// </summary>
        [JsonProperty("order_items")]
        public virtual ICollection<ConsumerFinanceOrderItemDto> OrderItemDtos
        {
            get { return _orderItemDtos ?? (_orderItemDtos = new List<ConsumerFinanceOrderItemDto>()); }
            set { _orderItemDtos = value; }
        }
    }
}