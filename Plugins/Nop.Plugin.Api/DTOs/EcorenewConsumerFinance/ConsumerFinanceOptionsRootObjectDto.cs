﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.EcorenewConsumerFinance
{
    public class ConsumerFinanceOptionsRootObjectDto : ISerializableObject
    {
        public ConsumerFinanceOptionsRootObjectDto()
        {
            ConsumerFinanceOptions = new List<ConsumerFinanceOptionDto>();
        }

        [JsonProperty("consumer_finance_options")]
        public IList<ConsumerFinanceOptionDto> ConsumerFinanceOptions { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "consumer_finance_options";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ConsumerFinanceOptionDto);
        }
    }
}