﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.EcorenewConsumerFinance
{
    public class ConsumerFinanceOrderItemsRootObjectDto : ISerializableObject
    {
        public ConsumerFinanceOrderItemsRootObjectDto()
        {
            ConsumerFinanceOrderItems = new List<ConsumerFinanceOrderItemDto>();
        }

        [JsonProperty("consumer_finance_order_items")]
        public IList<ConsumerFinanceOrderItemDto> ConsumerFinanceOrderItems { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "consumer_finance_order_items";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ConsumerFinanceOrderItemDto);
        }
    }
}