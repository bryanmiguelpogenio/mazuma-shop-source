﻿using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Plugin.Api.Validators.EcorenewConsumerFinance;

namespace Nop.Plugin.Api.DTOs.EcorenewConsumerFinance
{
    [JsonObject(Title = "local_vat")]
    public class LocalVatDto
    {
        [JsonProperty("store_id")]
        public int StoreId { get; set; }

        [JsonProperty("percentage")]
        public decimal Percentage { get; set; }
    }
}