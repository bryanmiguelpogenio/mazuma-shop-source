﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.EcorenewConsumerFinance
{
    [JsonObject(Title = "consumer_finance_option")]
    public class ConsumerFinanceOptionDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("duration_in_months")]
        public int DurationInMonths { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("agreement_rate")]
        public decimal AgreementRate { get; set; }

        [JsonProperty("is_active")]
        public bool IsActive { get; set; }
    }
}