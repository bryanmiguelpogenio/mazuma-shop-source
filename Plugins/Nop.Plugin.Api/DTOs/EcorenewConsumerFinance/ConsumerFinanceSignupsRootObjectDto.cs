﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.EcorenewConsumerFinance
{
    public class ConsumerFinanceSignupsRootObjectDto : ISerializableObject
    {
        public ConsumerFinanceSignupsRootObjectDto()
        {
            ConsumerFinanceSignups = new List<ConsumerFinanceSignupDto>();
        }

        [JsonProperty("consumer_finance_signups")]
        public IList<ConsumerFinanceSignupDto> ConsumerFinanceSignups { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "consumer_finance_signups";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ConsumerFinanceSignupDto);
        }
    }
}