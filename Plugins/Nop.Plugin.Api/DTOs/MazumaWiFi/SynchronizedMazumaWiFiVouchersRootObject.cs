﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.MazumaWiFi
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "synchronized_mazuma_wifi_vouchers_root")]
    public class SynchronizedMazumaWiFiVouchersRootObject : ISerializableObject
    {
        public SynchronizedMazumaWiFiVouchersRootObject()
        {
            SynchronizedMazumaWiFiVouchers = new List<SynchronizedMazumaWiFiVoucherDto>();
        }

        [JsonProperty("synchronized_mazuma_wifi_vouchers")]
        public IList<SynchronizedMazumaWiFiVoucherDto> SynchronizedMazumaWiFiVouchers { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "synchronized_mazuma_wifi_vouchers";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(SynchronizedMazumaWiFiVoucherDto);
        }
    }
}
