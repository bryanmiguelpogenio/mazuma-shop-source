﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.MazumaWiFi
{
    public class MazumaWiFiVouchersRootObject : ISerializableObject
    {
        public MazumaWiFiVouchersRootObject()
        {
            MazumaWiFiVouchers = new List<MazumaWiFiVoucherDto>();
        }

        [JsonProperty("mazuma_wifi_vouchers")]
        public IList<MazumaWiFiVoucherDto> MazumaWiFiVouchers { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "mazuma_wifi_vouchers";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(MazumaWiFiVoucherDto);
        }
    }
}