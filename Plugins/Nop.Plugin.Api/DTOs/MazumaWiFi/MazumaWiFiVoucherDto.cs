﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.MazumaWiFi
{
    [JsonObject(Title = "mazuma_wifi_voucher")]
    public class MazumaWiFiVoucherDto
    {
        /// <summary>
        /// Gets or sets the identifier
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the voucher serial number
        /// </summary>
        [JsonProperty("voucher_serial_number")]
        public string VoucherSerialNumber { get; set; }

        /// <summary>
        /// Gets or sets the pin
        /// </summary>
        [JsonProperty("pin")]
        public string Pin { get; set; }

        /// <summary>
        /// Gets or sets the balance
        /// </summary>
        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        /// <summary>
        /// Gets or sets the status
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the order assigned to the voucher code
        /// </summary>
        [JsonProperty("order_id")]
        public int? OrderId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the batch where the voucher info is included
        /// </summary>
        [JsonProperty("mazuma_wifi_voucher_upload_batch_id")]
        public int MazumaWiFiVoucherUploadBatchId { get; set; }
    }
}