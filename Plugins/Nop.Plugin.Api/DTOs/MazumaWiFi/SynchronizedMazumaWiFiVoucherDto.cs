﻿using Newtonsoft.Json;
using System;

namespace Nop.Plugin.Api.DTOs.MazumaWiFi
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "synchronized_mazuma_wifi_voucher")]
    public class SynchronizedMazumaWiFiVoucherDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("synchronized_on_utc")]
        public DateTime SynchronizedOnUtc { get; set; }
    }
}