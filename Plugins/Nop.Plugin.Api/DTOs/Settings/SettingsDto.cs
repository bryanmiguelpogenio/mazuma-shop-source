﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Settings
{
    [JsonObject(Title = "settings")]
    public class SettingsDto
    {
        [JsonProperty("settings_type_name")]
        public string SettingsTypeName { get; set; }

        [JsonProperty("settings_json")]
        public string SettingsJson { get; set; }

        [JsonProperty("store_id")]
        public int StoreId { get; set; }
    }
}