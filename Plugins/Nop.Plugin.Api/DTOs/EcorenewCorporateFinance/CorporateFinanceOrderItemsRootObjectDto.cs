﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.EcorenewCorporateFinance
{
    public class CorporateFinanceOrderItemsRootObjectDto : ISerializableObject
    {
        public CorporateFinanceOrderItemsRootObjectDto()
        {
            CorporateFinanceOrderItems = new List<CorporateFinanceOrderItemDto>();
        }

        [JsonProperty("corporate_finance_order_items")]
        public IList<CorporateFinanceOrderItemDto> CorporateFinanceOrderItems { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "corporate_finance_order_items";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(CorporateFinanceOrderItemDto);
        }
    }
}