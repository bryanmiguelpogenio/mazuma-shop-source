﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.ReturnRequests
{
    public class ReturnRequestsReportTotalRootObject
    {
        [JsonProperty("total")]
        public int Total { get; set; }
    }
}