﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.ReturnRequests
{
    [JsonObject(Title = "return_request")]
    public class ReturnRequestReportDto
    {
        [JsonProperty("return_request_id")]
        public int ReturnRequestId { get; set; }

        [JsonProperty("store_id")]
        public int StoreId { get; set; }

        [JsonProperty("custom_number")]
        public string CustomNumber { get; set; }

        [JsonProperty("order_status_id")]
        public int OrderStatusId { get; set; }

        [JsonProperty("return_request_status_id")]
        public int ReturnRequestStatusId { get; set; }

        [JsonProperty("unit_price_include_tax")]
        public decimal UnitPriceIncludeTax { get; set; }

        [JsonProperty("refund_amount")]
        public decimal RefundAmount { get; set; }
    }
}