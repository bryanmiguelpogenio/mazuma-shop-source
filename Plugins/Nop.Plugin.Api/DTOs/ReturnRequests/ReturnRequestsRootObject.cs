﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.ReturnRequests
{
    /// <remarks>Added by EcoRenew</remarks>
    public class ReturnRequestsRootObject : ISerializableObject
    {
        public ReturnRequestsRootObject()
        {
            ReturnRequests = new List<ReturnRequestDto>();
        }

        [JsonProperty("return_requests")]
        public IList<ReturnRequestDto> ReturnRequests { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "return_requests";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ReturnRequestDto);
        }
    }
}