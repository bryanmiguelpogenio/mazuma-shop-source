﻿using Newtonsoft.Json;
using System;

namespace Nop.Plugin.Api.DTOs.ReturnRequests
{

    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "return_request")]
    public class ReturnRequestDto
    {
        /// <summary>
        /// Gets or sets the value indicating the return request id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Custom number of return request
        /// </summary>
        [JsonProperty("custom_number")]
        public string CustomNumber { get; set; }

        /// <summary>
        /// Gets or sets the store identifier
        /// </summary>
        [JsonProperty("store_id")]
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the order item identifier
        /// </summary>
        [JsonProperty("order_item_id")]
        public int OrderItemId { get; set; }

        /// <summary>
        /// Gets or sets the quantity
        /// </summary>
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the reason to return
        /// </summary>
        [JsonProperty("reason_for_return")]
        public string ReasonForReturn { get; set; }

        /// <summary>
        /// Gets or sets the requested action
        /// </summary>
        [JsonProperty("requested_action")]
        public string RequestedAction { get; set; }

        /// <summary>
        /// Gets or sets the customer comments
        /// </summary>
        [JsonProperty("customer_comments")]
        public string CustomerComments { get; set; }

        /// <summary>
        /// Gets or sets the staff notes
        /// </summary>
        [JsonProperty("staff_notes")]
        public string StaffNotes { get; set; }

        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        [JsonProperty("created_on_utc")]
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of entity update
        /// </summary>
        [JsonProperty("updated_on_utc")]
        public DateTime UpdatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the return status
        /// </summary>
        [JsonProperty("return_request_status")]
        public string ReturnRequestStatus { get; set; }
    }
}