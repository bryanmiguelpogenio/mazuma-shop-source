﻿using Newtonsoft.Json;
using System;

namespace Nop.Plugin.Api.DTOs.ReturnRequests
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "synchronized_return_request")]
    public class SynchronizedReturnRequestDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("synchronized_on_utc")]
        public DateTime SynchronizedOnUtc { get; set; }
    }
}