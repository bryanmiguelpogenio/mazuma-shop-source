﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.ReturnRequests
{
    public class ReturnRequestsReportRootObject : ISerializableObject
    {
        [JsonProperty("return_requests")]
        public List<ReturnRequestReportDto> ReturnRequests { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "return_requests";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ReturnRequestReportDto);
        }
    }
}