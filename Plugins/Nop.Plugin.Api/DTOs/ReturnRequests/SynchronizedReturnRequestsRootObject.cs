﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.ReturnRequests
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "synchronized_return_requests_root")]
    public class SynchronizedReturnRequestsRootObject : ISerializableObject
    {
        public SynchronizedReturnRequestsRootObject()
        {
            SynchronizedReturnRequests = new List<SynchronizedReturnRequestDto>();
        }

        [JsonProperty("synchronized_return_requests")]
        public IList<SynchronizedReturnRequestDto> SynchronizedReturnRequests { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "synchronized_return_requests";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(SynchronizedReturnRequestDto);
        }
    }
}