﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.ReturnRequests
{
    [JsonObject(Title = "return_request_return_request_status")]
    public class ReturnRequestReturnRequestStatusDto
    {
        [JsonProperty("return_request_id")]
        public int ReturnRequestId { get; set; }

        [JsonProperty("return_request_status")]
        public string ReturnRequestStatus { get; set; }
    }
}