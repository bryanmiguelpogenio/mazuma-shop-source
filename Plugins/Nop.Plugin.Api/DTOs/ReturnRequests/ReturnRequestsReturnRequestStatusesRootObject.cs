﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.ReturnRequests
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "return_requests_return_request_statuses_root")]
    public class ReturnRequestsReturnRequestStatusesRootObject : ISerializableObject
    {
        public ReturnRequestsReturnRequestStatusesRootObject()
        {
            ReturnRequestsReturnRequestStatuses = new List<ReturnRequestReturnRequestStatusDto>();
        }

        [JsonProperty("return_requests_return_request_statuses")]
        public IList<ReturnRequestReturnRequestStatusDto> ReturnRequestsReturnRequestStatuses { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "return_requests_return_request_statuses";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ReturnRequestReturnRequestStatusDto);
        }
    }
}