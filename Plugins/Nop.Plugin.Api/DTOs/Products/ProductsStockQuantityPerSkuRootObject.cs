﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.Products
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "products_stock_quantity_per_sku_root")]
    public class ProductsStockQuantityPerSkuRootObject : ISerializableObject
    {
        public ProductsStockQuantityPerSkuRootObject()
        {
            ProductsStockQuantityPerSku = new List<ProductStockQuantityPerSkuDto>();
        }

        [JsonProperty("products_stock_quantity_per_sku")]
        public IList<ProductStockQuantityPerSkuDto> ProductsStockQuantityPerSku { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "products_stock_quantity_per_sku";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ProductStockQuantityPerSkuDto);
        }
    }
}