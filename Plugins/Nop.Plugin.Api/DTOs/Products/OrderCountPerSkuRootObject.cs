﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.Products
{
    public class OrderCountPerSkuRootObject : ISerializableObject
    {
        public OrderCountPerSkuRootObject()
        {
            OrderCountPerSku = new Dictionary<string, int>();
        }

        [JsonProperty("order_count_per_sku")]
        public IDictionary<string, int> OrderCountPerSku { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "order_count_per_sku";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(Dictionary<string, int>);
        }
    }
}