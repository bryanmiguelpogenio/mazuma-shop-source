﻿using Newtonsoft.Json;
using System;

namespace Nop.Plugin.Api.DTOs.Products
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "product_stock_quantity_per_sku")]
    public class ProductStockQuantityPerSkuDto
    {
        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("stock_quantity")]
        public int StockQuantity { get; set; }

        [JsonProperty("synchronized_unassiged_order_count")]
        public int SynchronizedUnassigedOrderCount { get; set; }
    }
}