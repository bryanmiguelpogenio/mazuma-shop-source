﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Products
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "product_price_per_sku")]
    public class ProductPricePerSkuDto
    {
        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }
    }
}