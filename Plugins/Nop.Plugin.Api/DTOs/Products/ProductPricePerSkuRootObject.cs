﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.DTOs.Products
{
    /// <remarks>Added by EcoRenew</remarks>
    [JsonObject(Title = "product_prices_per_sku_root")]
    public class ProductPricePerSkuRootObject : ISerializableObject
    {
        public ProductPricePerSkuRootObject()
        {
            ProductPricesPerSku = new List<ProductPricePerSkuDto>();
        }

        [JsonProperty("product_prices_per_sku")]
        public IList<ProductPricePerSkuDto> ProductPricesPerSku { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "product_prices_per_sku";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ProductPricePerSkuDto);
        }
    }
}