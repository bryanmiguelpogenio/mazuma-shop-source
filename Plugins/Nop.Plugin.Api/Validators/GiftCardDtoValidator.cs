﻿using FluentValidation;
using Nop.Plugin.Api.DTOs.GiftCards;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.Validators
{
    public class GiftCardDtoValidator : AbstractValidator<GiftCardDto>
    {
        public GiftCardDtoValidator(string httpMethod, Dictionary<string, object> passedPropertyValuePairs)
        {
            if (string.IsNullOrEmpty(httpMethod) ||
                httpMethod.Equals("post", StringComparison.InvariantCultureIgnoreCase))
            {
                SetGiftCardRule();
            }
            else if (httpMethod.Equals("put", StringComparison.InvariantCultureIgnoreCase))
            {
                RuleFor(x => x.Id)
                    .NotNull()
                    .NotEmpty()
                    .Must(id => id > 0)
                    .WithMessage("Invalid Id");

                SetEditGiftCardRule();

                if (passedPropertyValuePairs.ContainsKey("amount"))
                    SetAmountRule();

                if (passedPropertyValuePairs.ContainsKey("has_duration_period") ||
                    passedPropertyValuePairs.ContainsKey("start_date") ||
                    passedPropertyValuePairs.ContainsKey("end_date"))
                    SetEditDurationDateRule();
            }
        }

        private void SetGiftCardRule()
        {
            RuleFor(x => x.Amount)
                .NotEmpty()
                .WithMessage("Amount is required.");

            RuleFor(x => x.GiftCardCouponCode)
                .NotEmpty()
                .WithMessage("Coupon Code is required.");

            RuleFor(x => x.RecipientEmail)
                .NotEmpty()
                .WithMessage("Recipient Email is required")
                .EmailAddress();

            RuleFor(x => x.RecipientName)
                .NotEmpty()
                .WithMessage("Recipient Name is required");

            RuleFor(x => x.StartDateUtc)
                .NotEmpty()
                .WithMessage("Start Date is required.")
                .When(x => x.HasDurationPeriod);

            RuleFor(x => x.EndDateUtc)
                .NotEmpty()
                .WithMessage("End Date is required")
                .GreaterThan(x => x.StartDateUtc)
                .WithMessage("End Date must be greater than Start Date.")
                .When(x => x.HasDurationPeriod);

            SetSenderEmail();
            SetTradeNumberRule();
        }

        private void SetEditGiftCardRule()
        {
            RuleFor(x => x.GiftCardCouponCode)
                 .Length(1, 250)
                 .WithMessage("Coupon Code is required.");

            RuleFor(x => x.RecipientEmail)
                .Length(1, 250)
                .WithMessage("Recipient Email is required")
                .EmailAddress();

            RuleFor(x => x.RecipientName)
                .Length(1, 250)
                .WithMessage("Recipient Name is required");

            SetSenderEmail();
            SetTradeNumberRule();
        }

        private void SetSenderEmail()
        {
            RuleFor(x => x.SenderEmail)
                .EmailAddress();
        }

        private void SetAmountRule()
        {
            RuleFor(x => x.Amount)
                .GreaterThan(0)
                .WithMessage("Amount should be greater than 0.");
        }

        private void SetTradeNumberRule()
        {
            RuleFor(x => x.SellTradeNumber)
                .Length(1, 250)
                .WithMessage("Sell Trade Number should not be empty.");
        }

        private void SetEditDurationDateRule()
        {
            RuleFor(x => x.StartDateUtc)
                .NotEmpty()
                .WithMessage("Start Date is required.");

            RuleFor(x => x.EndDateUtc)
                .NotEmpty()
                .WithMessage("End Date is required")
                .GreaterThan(x => x.StartDateUtc)
                .WithMessage("End Date must be greater than Start Date.");
        }
    }
}
