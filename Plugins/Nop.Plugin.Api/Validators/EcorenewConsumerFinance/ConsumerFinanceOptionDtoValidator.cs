﻿using FluentValidation;
using Nop.Plugin.Api.DTOs.EcorenewConsumerFinance;

namespace Nop.Plugin.Api.Validators.EcorenewConsumerFinance
{
    public class ConsumerFinanceOptionDtoValidator : AbstractValidator<ConsumerFinanceOptionDto>
    {
        public ConsumerFinanceOptionDtoValidator()
        {
            RuleFor(dto => dto.DurationInMonths)
                .GreaterThan(0)
                .WithMessage("invalid consumer finance option duration in months");

            RuleFor(dto => dto.Description)
                .NotNull()
                .NotEmpty()
                .WithMessage("invalid consumer finance option description");

            RuleFor(dto => dto.AgreementRate)
                .GreaterThan(0)
                .WithMessage("invalid consumer finance option agreement rate");
        }
    }
}