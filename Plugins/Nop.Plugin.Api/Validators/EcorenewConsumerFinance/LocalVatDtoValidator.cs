﻿using FluentValidation;
using Nop.Plugin.Api.DTOs.EcorenewConsumerFinance;
using Nop.Services.Directory;
using Nop.Services.Stores;

namespace Nop.Plugin.Api.Validators.EcorenewConsumerFinance
{
    public class LocalVatDtoValidator : AbstractValidator<LocalVatDto>
    {
        private readonly ICountryService _countryService;
        private readonly IStoreService _storeService;

        public LocalVatDtoValidator(ICountryService countryService,
            IStoreService storeService)
        {
            this._countryService = countryService;
            this._storeService = storeService;
        }

        public LocalVatDtoValidator()
        {
            RuleFor(dto => dto.Percentage)
                .GreaterThanOrEqualTo(0)
                .WithMessage("invalid local VAT percentage");
            
            RuleFor(dto => dto.StoreId)
                .Must(id => _storeService.GetStoreById(id) != null)
                .WithMessage("store not found");
        }
    }
}