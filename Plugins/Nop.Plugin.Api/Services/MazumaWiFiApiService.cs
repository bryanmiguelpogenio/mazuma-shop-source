﻿using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DataStructures;
using Nop.Plugin.Mazuma.WiFiVoucher.Domain;

namespace Nop.Plugin.Api.Services
{
    ///<remarks>Added by EcoRenew</remarks>
    public class MazumaWiFiApiService : IMazumaWiFiApiService
    {
        #region Fields

        private readonly IRepository<MazumaWiFiVoucher> _mazumaWiFiVoucherRepository;

        #endregion

        #region Ctor

        public MazumaWiFiApiService(IRepository<MazumaWiFiVoucher> mazumaWiFiVoucherRepository)
        {
            this._mazumaWiFiVoucherRepository = mazumaWiFiVoucherRepository;
        }

        #endregion

        #region Methods

        public virtual IList<MazumaWiFiVoucher> GetMazumaWiFiVouchers(int limit = Configurations.DefaultLimit,
            int page = Configurations.DefaultPageValue,
            bool withOrderOnly = false,
            bool unsynchronizedOnly = false)
        {
            var query = _mazumaWiFiVoucherRepository.TableNoTracking;

            if (withOrderOnly)
                query = query.Where(x => x.OrderId.HasValue);
            if (unsynchronizedOnly)
                query = query.Where(x => !x.SynchronizedOnUtc.HasValue);

            query = query.OrderBy(x => x.Id);

            return new ApiList<MazumaWiFiVoucher>(query, page - 1, limit);
        }

        public IEnumerable<MazumaWiFiVoucher> GetMazumaWiFiVouchersByIds(IEnumerable<int> ids)
        {
            return _mazumaWiFiVoucherRepository.Table.Where(x => ids.Contains(x.Id));
        }

        public void UpdateMazumaWiFiVouchers(IEnumerable<MazumaWiFiVoucher> vouchers)
        {
            _mazumaWiFiVoucherRepository.Update(vouchers);
        }

        #endregion
    }
}