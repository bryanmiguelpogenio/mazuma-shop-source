﻿using Nop.Core.Data;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DataStructures;
using Nop.Plugin.Payments.BrainTreePayPal.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Api.Services
{
    public class BrainTreePayPalApiService : IBrainTreePayPalApiService
    {
        #region Fields

        private readonly IRepository<OrderPayPalCreditFinancingDetails> _orderPayPalCreditFinancingDetailsRepository;

        #endregion

        #region Ctor

        public BrainTreePayPalApiService(IRepository<OrderPayPalCreditFinancingDetails> orderPayPalCreditFinancingDetailsRepository)
        {
            this._orderPayPalCreditFinancingDetailsRepository = orderPayPalCreditFinancingDetailsRepository;
        }

        #endregion

        #region Utilities

        private IQueryable<OrderPayPalCreditFinancingDetails> GetOrderPayPalCreditFinancingDetailsQuery(IList<int> ids = null, int? order_id = null)
        {
            var query = _orderPayPalCreditFinancingDetailsRepository.TableNoTracking;

            if (order_id != null)
            {
                query = query.Where(o => o.OrderId == order_id);
            }

            if (ids != null && ids.Count > 0)
            {
                query = query.Where(c => ids.Contains(c.Id));
            }

            query = query.OrderBy(o => o.Id);

            return query;
        }

        #endregion

        #region Methods

        public IList<OrderPayPalCreditFinancingDetails> GetAllOrderPayPalCreditFinancingDetails(IList<int> ids = null,
            int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
            int sinceId = Configurations.DefaultSinceId, int? order_id = null)
        {
            var query = GetOrderPayPalCreditFinancingDetailsQuery(ids, order_id);

            if (sinceId > 0)
            {
                query = query.Where(order => order.Id > sinceId);
            }

            return new ApiList<OrderPayPalCreditFinancingDetails>(query, page - 1, limit);
        }

        public int GetOrderPayPalCreditFinancingDetailsCount()
        {
            return GetOrderPayPalCreditFinancingDetailsQuery().Count();
        }

        #endregion
    }
}
