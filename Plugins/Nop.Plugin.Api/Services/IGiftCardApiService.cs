﻿using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Constants;
using System.Collections.Generic;

namespace Nop.Plugin.Api.Services
{
    public interface IGiftCardApiService
    {
        void DeleteGiftCard(GiftCard giftCard);

        IList<GiftCard> GetGiftCards(int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
            IList<int> ids = null, int sinceId = Configurations.DefaultSinceId);

        int GetGiftCardsCount(IList<int> ids = null, int sinceId = Configurations.DefaultSinceId);

        GiftCard GetGiftCardById(int giftCardId);

        void InsertGiftCard(GiftCard giftCard);

        void UpdateGiftCard(GiftCard giftCard);

        bool CouponCodeExists(string couponCode);

        bool CouponCodeExists(int id, string couponCode);
    }
}
