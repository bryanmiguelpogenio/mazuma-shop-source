﻿using Nop.Plugin.Api.Constants;
using Nop.Plugin.Mazuma.WiFiVoucher.Domain;
using System.Collections.Generic;

namespace Nop.Plugin.Api.Services
{
    /// <remarks>
    /// Added by EcoRenew
    /// </remarks>
    public interface IMazumaWiFiApiService
    {
        IList<MazumaWiFiVoucher> GetMazumaWiFiVouchers(int limit = Configurations.DefaultLimit,
            int page = Configurations.DefaultPageValue,
            bool withOrderOnly = false,
            bool unsynchronizedOnly = false);

        IEnumerable<MazumaWiFiVoucher> GetMazumaWiFiVouchersByIds(IEnumerable<int> ids);

        void UpdateMazumaWiFiVouchers(IEnumerable<MazumaWiFiVoucher> vouchers);
    }
}