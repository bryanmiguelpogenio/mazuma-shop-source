﻿using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Constants;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.Services
{
    /// <remarks>Added by EcoRenew</remarks>
    public interface IReturnRequestApiService
    {
        IList<ReturnRequest> GetReturnRequests(IList<int> ids = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                               int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
                               int sinceId = Configurations.DefaultSinceId, ReturnRequestStatus? status = null, 
                               int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false);

        void UpdateReturnRequests(IEnumerable<ReturnRequest> returnRequests);

        IList<ReturnRequest> GetSynchronizedIncompleteReturnRequests();
        int GetReturnRequestsReportTotal(int? storeId = null);
    }
}