﻿using Nop.Core.Domain.Shipping;
using System.Collections.Generic;

namespace Nop.Plugin.Api.Services
{
    /// <remarks>Added by EcoRenew</remarks>
    public interface IShipmentApiService
    {
        void InsertShipments(IEnumerable<Shipment> shipments);
        void UpdateShipment(Shipment shipment);
        Shipment GetShipmentById(int shipmentId);
        IEnumerable<Shipment> GetShipmentsByTrackingNumber(string trackingNumber);
    }
}