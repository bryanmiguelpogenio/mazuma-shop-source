﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DataStructures;
using Nop.Plugin.Api.DTOs.Products;
using Nop.Services.Catalog;
using Nop.Services.Events;
using Nop.Services.Stores;

namespace Nop.Plugin.Api.Services
{
    public class ProductApiService : IProductApiService
    {
        //Added by EcoRenew
        #region Constants

        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PRODUCTATTRIBUTES_PATTERN_KEY = "Nop.productattribute.";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PRODUCTATTRIBUTEMAPPINGS_PATTERN_KEY = "Nop.productattributemapping.";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PRODUCTATTRIBUTEVALUES_PATTERN_KEY = "Nop.productattributevalue.";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string PRODUCTATTRIBUTECOMBINATIONS_PATTERN_KEY = "Nop.productattributecombination.";

        #endregion

        private readonly IRepository<OrderItem> _orderItemRepository; // Added by EcoRenew
        private readonly IStoreMappingService _storeMappingService;
        private readonly IProductService _productService;
        private readonly IRepository<ProductAttributeCombination> _productAttributeCombinationRepository; //Added by EcoRenew
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<ProductCategory> _productCategoryMappingRepository;
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly ICacheManager _cacheManager; //Added by EcoRenew
        private readonly IEventPublisher _eventPublisher; //Added by EcoRenew
        private readonly IProductAttributeParser _productAttributeParser; //Added by EcoRenew

        public ProductApiService(IRepository<OrderItem> orderItemRepository, //Added by EcoRenew
            IRepository<ProductAttributeCombination> productAttributeCombinationRepository, //Added by EcoRenew
            IRepository<Product> productRepository,
            IRepository<ProductCategory> productCategoryMappingRepository,
            IRepository<Vendor> vendorRepository,
            ICacheManager cacheManager, //Added by EcoRenew
            IEventPublisher eventPublisher, //Added by EcoRenew
            IProductAttributeParser productAttributeParser, //Added by EcoRenew
            IStoreMappingService storeMappingService,
            IProductService productService)
        {
            _orderItemRepository = orderItemRepository;
            _productAttributeCombinationRepository = productAttributeCombinationRepository; //Added by EcoRenew
            _productRepository = productRepository;
            _productCategoryMappingRepository = productCategoryMappingRepository;
            _vendorRepository = vendorRepository;
            _cacheManager = cacheManager; //Added by EcoRenew
            this._eventPublisher = eventPublisher; //Added by EcoRenew
            this._productAttributeParser = productAttributeParser; //Added by EcoRenew
            _storeMappingService = storeMappingService;
            _productService = productService;
        }

        public IList<Product> GetProducts(IList<int> ids = null,
            DateTime? createdAtMin = null, DateTime? createdAtMax = null, DateTime? updatedAtMin = null, DateTime? updatedAtMax = null,
           int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue, int sinceId = Configurations.DefaultSinceId,
           int? categoryId = null, string vendorName = null, bool? publishedStatus = null)
        {
            var query = GetProductsQuery(createdAtMin, createdAtMax, updatedAtMin, updatedAtMax, vendorName, publishedStatus, ids, categoryId);

            if (sinceId > 0)
            {
                query = query.Where(c => c.Id > sinceId);
            }

            return new ApiList<Product>(query, page - 1, limit);
        }
        
        public int GetProductsCount(DateTime? createdAtMin = null, DateTime? createdAtMax = null, 
            DateTime? updatedAtMin = null, DateTime? updatedAtMax = null, bool? publishedStatus = null, string vendorName = null, 
            int? categoryId = null)
        {
            var query = GetProductsQuery(createdAtMin, createdAtMax, updatedAtMin, updatedAtMax, vendorName,
                                         publishedStatus, categoryId: categoryId);

            return query.ToList().Count(p => _storeMappingService.Authorize(p));
        }

        public Product GetProductById(int productId)
        {
            if (productId == 0)
                return null;

            return _productRepository.Table.FirstOrDefault(product => product.Id == productId && !product.Deleted);
        }

        public Product GetProductByIdNoTracking(int productId)
        {
            if (productId == 0)
                return null;

            return _productRepository.TableNoTracking.FirstOrDefault(product => product.Id == productId && !product.Deleted);
        }

        /// <remarks>Added by EcoRenew</remarks>
        public void SetProductsStockQuantityPerSku(List<ProductStockQuantityPerSkuDto> skuStockQuantityDictionary)
        {
            try
            {
                var skuList = skuStockQuantityDictionary.Select(x => x.Sku);

                var products = _productRepository.Table
                    .Where(p => !p.Deleted 
                        && p.Published 
                        && p.ManageInventoryMethodId == (int)ManageInventoryMethod.ManageStock
                        && skuList.Contains(p.Sku))
                    .ToList();

                var productAttributeCombinations = _productAttributeCombinationRepository.Table
                    .Where(pac => !pac.Product.Deleted
                        && pac.Product.Published
                        && pac.Product.ManageInventoryMethodId == (int)ManageInventoryMethod.ManageStockByAttributes
                        && skuList.Contains(pac.Sku))
                    .ToList();

                foreach (var p in products)
                {
                    int pStockQuantityBefore = p.StockQuantity;

                    p.StockQuantity = 0;

                    var matchingSkuData = skuStockQuantityDictionary.FirstOrDefault(x => x.Sku == p.Sku);

                    if (matchingSkuData != null)
                    {
                        // check for orders placed later than the last assigned order that has been synchronized with logistics
                        int orderCount = 0;
                        orderCount = matchingSkuData.SynchronizedUnassigedOrderCount + (_orderItemRepository.TableNoTracking
                            .Where(x => x.ProductId == p.Id
                                && !x.Order.SynchronizedOnUtc.HasValue
                                && x.Order.OrderStatusId == (int)OrderStatus.Processing)
                            .Sum(x => (int?)x.Quantity) ?? 0);

                        int computedStockQuantity = matchingSkuData.StockQuantity - orderCount;

                        p.StockQuantity = computedStockQuantity < 0 ? 0 : computedStockQuantity;
                    }

                    _productRepository.Update(p);

                    if (p.StockQuantity != pStockQuantityBefore)
                    {
                        int stockQuantityDifference = p.StockQuantity - pStockQuantityBefore;
                        _productService.AddStockQuantityHistoryEntry(p, stockQuantityDifference, p.StockQuantity, message: "Logistics sync");
                    }
                }

                foreach (var pac in productAttributeCombinations)
                {
                    int pacStockQuantityBefore = pac.StockQuantity;

                    pac.StockQuantity = 0;

                    if (!string.IsNullOrWhiteSpace(pac.Sku))
                    {
                        var matchingSkuData = skuStockQuantityDictionary.FirstOrDefault(x => x.Sku == pac.Sku);
                        if (matchingSkuData != null)
                        {
                            var skuAttributeXmls = _productAttributeCombinationRepository.TableNoTracking
                                .Where(x => x.Sku == pac.Sku)
                                .Select(x => x.AttributesXml)
                                .Distinct();

                            // check for orders placed later than the last assigned order that has been synchronized with logistics
                            int orderCount = 0;

                            //List<OrderItem> ordersForProduct = new List<OrderItem>();
                            orderCount = matchingSkuData.SynchronizedUnassigedOrderCount + (_orderItemRepository.TableNoTracking
                                .Where(x => x.ProductId == pac.ProductId
                                    && skuAttributeXmls.Contains(x.AttributesXml)
                                    && !x.Order.SynchronizedOnUtc.HasValue
                                    && x.Order.OrderStatusId == (int)OrderStatus.Processing)
                                .Sum(x => (int?)x.Quantity) ?? 0);
                                //.ToList();

                            //int orderCount = 0;
                            //orderCount = ordersForProduct.Where(oi => oi.Product.FormatSku(oi.AttributesXml, _productAttributeParser) == pac.Sku)
                            //    .Sum(x => (int?)x.Quantity) ?? 0;

                            int computedStockQuantity = matchingSkuData.StockQuantity - orderCount;

                            pac.StockQuantity = computedStockQuantity < 0 ? 0 : computedStockQuantity;
                        }
                    }

                    _productAttributeCombinationRepository.Update(pac);

                    if (pac.StockQuantity != pacStockQuantityBefore)
                    {
                        int stockQuantityDifference = pac.StockQuantity - pacStockQuantityBefore;
                        _productService.AddStockQuantityHistoryEntry(pac.Product, stockQuantityDifference, pac.StockQuantity, message: "Logistics sync", combinationId: pac.Id);
                    }
                }


                //var products = _productRepository.Table
                //    .Where(p => !p.Deleted && p.Published)
                //    .ToList();

                //foreach (var p in products)
                //{
                //    int pStockQuantityBefore = p.StockQuantity;

                //    p.StockQuantity = 0;

                //    if (p.ManageInventoryMethod == ManageInventoryMethod.ManageStock && !string.IsNullOrWhiteSpace(p.Sku))
                //    {
                //        var matchingSkuData = skuStockQuantityDictionary.FirstOrDefault(x => x.Sku == p.Sku);

                //        if (matchingSkuData != null)
                //        {
                //            // check for orders placed later than the last assigned order that has been synchronized with logistics
                //            int orderCount = 0;
                //            orderCount = _orderItemRepository.TableNoTracking
                //                .Where(x => x.ProductId == p.Id 
                //                    && (!matchingSkuData.LastOrderCreatedOnUtc.HasValue
                //                        || (matchingSkuData.LastOrderCreatedOnUtc.HasValue && x.Order.CreatedOnUtc > matchingSkuData.LastOrderCreatedOnUtc)
                //                        || !x.Order.SynchronizedOnUtc.HasValue
                //                    )
                //                    && x.Order.OrderStatusId == (int)OrderStatus.Processing)
                //                .Sum(x => (int?)x.Quantity) ?? 0;

                //            int computedStockQuantity = matchingSkuData.StockQuantity - orderCount;

                //            p.StockQuantity = computedStockQuantity < 0 ? 0 : computedStockQuantity;
                //        }
                //    }

                //    if (p.StockQuantity != pStockQuantityBefore)
                //    {
                //        int stockQuantityDifference = p.StockQuantity - pStockQuantityBefore;
                //        _productService.AddStockQuantityHistoryEntry(p, stockQuantityDifference, p.StockQuantity, message: "Logistics sync");
                //    }

                //    if (p.ManageInventoryMethod == ManageInventoryMethod.ManageStockByAttributes)
                //    {
                //        foreach (var pac in p.ProductAttributeCombinations)
                //        {
                //            int pacStockQuantityBefore = pac.StockQuantity;

                //            pac.StockQuantity = 0;

                //            if (!string.IsNullOrWhiteSpace(pac.Sku))
                //            {
                //                var matchingSkuData = skuStockQuantityDictionary.FirstOrDefault(x => x.Sku == pac.Sku);
                //                if (matchingSkuData != null)
                //                {
                //                    // check for orders placed later than the last assigned order that has been synchronized with logistics
                //                    List<OrderItem> ordersForProduct = new List<OrderItem>();
                //                    ordersForProduct = _orderItemRepository.TableNoTracking
                //                        .Where(x => x.ProductId == p.Id
                //                            && (!matchingSkuData.LastOrderCreatedOnUtc.HasValue
                //                                || (matchingSkuData.LastOrderCreatedOnUtc.HasValue && x.Order.CreatedOnUtc > matchingSkuData.LastOrderCreatedOnUtc)
                //                                || !x.Order.SynchronizedOnUtc.HasValue
                //                            )
                //                            && x.Order.OrderStatusId == (int)OrderStatus.Processing)
                //                        .ToList();

                //                    int orderCount = 0;
                //                    orderCount = ordersForProduct.Where(oi => oi.Product.FormatSku(oi.AttributesXml, _productAttributeParser) == pac.Sku)
                //                        .Sum(x => (int?)x.Quantity) ?? 0;

                //                    int computedStockQuantity = matchingSkuData.StockQuantity - orderCount;

                //                    pac.StockQuantity = computedStockQuantity < 0 ? 0 : computedStockQuantity;
                //                }
                //            }

                //            if (pac.StockQuantity != pacStockQuantityBefore)
                //            {
                //                int stockQuantityDifference = pac.StockQuantity - pacStockQuantityBefore;
                //                _productService.AddStockQuantityHistoryEntry(p, stockQuantityDifference, pac.StockQuantity, message: "Logistics sync", combinationId: pac.Id);
                //            }
                //        }
                //    }

                //    _productRepository.Update(p);
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <remarks>Added by EcoRenew</remarks>
        public IList<Product> GetProductsBySkus(IList<string> skus)
        {
            return _productRepository.Table.Where(x => skus.Contains(x.Sku)).ToList();
        }

        /// <remarks>Added by EcoRenew</remarks>
        public IList<ProductAttributeCombination> GetProductAttributeCombinationsBySkus(IList<string> skus)
        {
            return _productAttributeCombinationRepository.Table.Where(x => skus.Contains(x.Sku)).ToList();
        }

        /// <remarks>Added by EcoRenew</remarks>
        public void UpdateProductAttributeCombinations(IList<ProductAttributeCombination> productAttributeCombinations)
        {
            if (productAttributeCombinations == null)
                throw new ArgumentNullException(nameof(productAttributeCombinations));

            //update
            _productAttributeCombinationRepository.Update(productAttributeCombinations);

            //cache
            _cacheManager.RemoveByPattern(PRODUCTATTRIBUTES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTATTRIBUTEMAPPINGS_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTATTRIBUTEVALUES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(PRODUCTATTRIBUTECOMBINATIONS_PATTERN_KEY);

            //event notification
            foreach (var productAttributeCombination in productAttributeCombinations)
            {
                _eventPublisher.EntityUpdated(productAttributeCombination);
            }
        }

        /// <remarks>Added by EcoRenew</remarks>
        public IDictionary<string, decimal> GetProductPricePerSku()
        {
            return _productAttributeCombinationRepository.TableNoTracking
                .Where(x => x.Product.Published && !string.IsNullOrEmpty(x.Sku))
                .GroupBy(x => x.Sku)
                .Select(g => new
                {
                    Sku = g.Key,
                    Price = g.Max(x => (x.OverriddenPrice ?? x.Product.Price))
                })
                .ToDictionary(k => k.Sku, v => v.Price);
        }

        private IQueryable<Product> GetProductsQuery(DateTime? createdAtMin = null, DateTime? createdAtMax = null, 
            DateTime? updatedAtMin = null, DateTime? updatedAtMax = null, string vendorName = null, 
            bool? publishedStatus = null, IList<int> ids = null, int? categoryId = null)
            
        {
            var query = _productRepository.TableNoTracking;

            if (ids != null && ids.Count > 0)
            {
                query = query.Where(c => ids.Contains(c.Id));
            }

            if (publishedStatus != null)
            {
                query = query.Where(c => c.Published == publishedStatus.Value);
            }

            // always return products that are not deleted!!!
            query = query.Where(c => !c.Deleted);

            if (createdAtMin != null)
            {
                query = query.Where(c => c.CreatedOnUtc > createdAtMin.Value);
            }

            if (createdAtMax != null)
            {
                query = query.Where(c => c.CreatedOnUtc < createdAtMax.Value);
            }

            if (updatedAtMin != null)
            {
               query = query.Where(c => c.UpdatedOnUtc > updatedAtMin.Value);
            }

            if (updatedAtMax != null)
            {
                query = query.Where(c => c.UpdatedOnUtc < updatedAtMax.Value);
            }

            if (!string.IsNullOrEmpty(vendorName))
            {
                query = from vendor in _vendorRepository.TableNoTracking
                        join product in _productRepository.TableNoTracking on vendor.Id equals product.VendorId
                        where vendor.Name == vendorName && !vendor.Deleted && vendor.Active
                        select product;
            }

            //only distinct products (group by ID)
            query = from p in query
                    group p by p.Id
                        into pGroup
                    orderby pGroup.Key
                    select pGroup.FirstOrDefault();

            if (categoryId != null)
            {
                var categoryMappingsForProduct = from productCategoryMapping in _productCategoryMappingRepository.TableNoTracking
                                                 where productCategoryMapping.CategoryId == categoryId
                                                 select productCategoryMapping;

                query = from product in query
                        join productCategoryMapping in categoryMappingsForProduct on product.Id equals productCategoryMapping.ProductId
                        select product;
            }

            query = query.OrderBy(product => product.Id);

            return query;
        }
    }
}