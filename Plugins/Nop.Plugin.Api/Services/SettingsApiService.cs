﻿using System;
using System.ComponentModel;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Configuration;
using Nop.Services.Configuration;
using Nop.Services.Events;

namespace Nop.Plugin.Api.Services
{
    /// <remarks>Added by EcoRenew</remarks>
    public class SettingsApiService : SettingService, ISettingsApiService
    {
        public SettingsApiService(IStaticCacheManager cacheManager, 
            IEventPublisher eventPublisher, 
            IRepository<Setting> settingRepository) 
            : base(cacheManager, 
                  eventPublisher, 
                  settingRepository)
        {
        }

        public virtual void SaveSetting(object settings, Type settingsType, int storeId = 0)
        {
            /* We do not clear cache after each setting update.
                * This behavior can increase performance because cached settings will not be cleared 
                * and loaded from database after each update */
            foreach (var prop in settingsType.GetProperties())
            {
                // get properties we can read and write to
                if (!prop.CanRead || !prop.CanWrite)
                    continue;

                if (!TypeDescriptor.GetConverter(prop.PropertyType).CanConvertFrom(typeof(string)))
                    continue;

                var key = settingsType.Name + "." + prop.Name;
                //Duck typing is not supported in C#. That's why we're using dynamic type
                dynamic value = prop.GetValue(settings, null);
                if (value != null)
                    SetSetting(key, value, storeId, false);
                else
                    SetSetting(key, "", storeId, false);
            }

            //and now clear cache
            ClearCache();
        }
    }
}