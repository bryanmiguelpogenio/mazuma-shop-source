﻿using Nop.Plugin.Api.Constants;
using Nop.Plugin.Payments.BrainTreePayPal.Domain;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Api.Services
{
    public interface IBrainTreePayPalApiService
    {
        IList<OrderPayPalCreditFinancingDetails> GetAllOrderPayPalCreditFinancingDetails(IList<int> ids = null,
            int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
            int sinceId = Configurations.DefaultSinceId, int? order_id = null);

        int GetOrderPayPalCreditFinancingDetailsCount();
    }
}
