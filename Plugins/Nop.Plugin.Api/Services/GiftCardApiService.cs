﻿using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DataStructures;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Api.Services
{
    public class GiftCardApiService : IGiftCardApiService
    {
        #region Fields

        private readonly IRepository<GiftCard> _giftCardRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="giftCardRepository">Gift card context</param>
        /// <param name="eventPublisher">Event published</param>
        public GiftCardApiService(IRepository<GiftCard> giftCardRepository, IEventPublisher eventPublisher)
        {
            _giftCardRepository = giftCardRepository;
            _eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes a gift card
        /// </summary>
        /// <param name="giftCard">Gift card</param>
        public virtual void DeleteGiftCard(GiftCard giftCard)
        {
            if (giftCard == null)
                throw new ArgumentNullException(nameof(giftCard));

            _giftCardRepository.Delete(giftCard);

            //event notification
            _eventPublisher.EntityDeleted(giftCard);
        }

        private IQueryable<GiftCard> GetGiftCardsQuery(IList<int> ids = null, int sinceId = Configurations.DefaultSinceId)
        {
            var query = _giftCardRepository.TableNoTracking;

            if (ids != null && ids.Count > 0)
            {
                query = query.Where(c => ids.Contains(c.Id));
            }

            if (sinceId > 0)
            {
                query = query.Where(returnRequest => returnRequest.Id > sinceId);
            }

            query = query.OrderBy(o => o.Id);

            return query;
        }

        public IList<GiftCard> GetGiftCards(int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
            IList<int> ids = null, int sinceId = Configurations.DefaultSinceId)
        {
            var query = GetGiftCardsQuery(ids: ids, sinceId: sinceId);

            return new ApiList<GiftCard>(query, page - 1, limit);
        }

        public int GetGiftCardsCount(IList<int> ids = null, int sinceId = Configurations.DefaultSinceId)
        {
            return GetGiftCardsQuery(ids: ids, sinceId: sinceId).Count();
        }

        /// <summary>
        /// Gets a gift card
        /// </summary>
        /// <param name="giftCardId">Gift card identifier</param>
        /// <returns>Gift card entry</returns>
        public virtual GiftCard GetGiftCardById(int giftCardId)
        {
            if (giftCardId == 0)
                return null;

            return _giftCardRepository.GetById(giftCardId);
        }

        /// <summary>
        /// Inserts a gift card
        /// </summary>
        /// <param name="giftCard">Gift card</param>
        public virtual void InsertGiftCard(GiftCard giftCard)
        {
            if (giftCard == null)
                throw new ArgumentNullException(nameof(giftCard));

            _giftCardRepository.Insert(giftCard);

            //event notification
            _eventPublisher.EntityInserted(giftCard);
        }

        /// <summary>
        /// Updates the gift card
        /// </summary>
        /// <param name="giftCard">Gift card</param>
        public virtual void UpdateGiftCard(GiftCard giftCard)
        {
            if (giftCard == null)
                throw new ArgumentNullException(nameof(giftCard));

            _giftCardRepository.Update(giftCard);

            //event notification
            _eventPublisher.EntityUpdated(giftCard);
        }

        public virtual bool CouponCodeExists(string couponCode)
        {
            var giftCard = _giftCardRepository
                .TableNoTracking
                .FirstOrDefault(x => x.GiftCardCouponCode.ToLower() == couponCode.ToLower());

            return giftCard != null;
        }

        public virtual bool CouponCodeExists(int id, string couponCode)
        {
            var giftCard = _giftCardRepository
                .TableNoTracking
                .FirstOrDefault(x => x.GiftCardCouponCode.ToLower() == couponCode.ToLower()
                    && x.Id != id);

            return giftCard != null;
        }

        #endregion
    }
}
