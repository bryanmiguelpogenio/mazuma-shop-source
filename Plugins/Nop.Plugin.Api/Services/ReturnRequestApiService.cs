﻿using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DataStructures;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Api.Services
{
    /// <remarks>Added by EcoRenew</remarks>
    public class ReturnRequestApiService : IReturnRequestApiService
    {
        #region Fields

        private readonly IRepository<ReturnRequest> _returnRequestRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        public ReturnRequestApiService(IRepository<ReturnRequest> returnRequestRepository,
            IEventPublisher eventPublisher)
        {
            this._returnRequestRepository = returnRequestRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Utilities

        private IQueryable<ReturnRequest> GetReturnRequestsQuery(DateTime? createdAtMin = null, DateTime? createdAtMax = null, ReturnRequestStatus? status = null,
            IList<int> ids = null, int? customerId = null, int? storeId = null, bool unsynchronizedReturnRequestsOnly = false)
        {
            var query = _returnRequestRepository.Table;

            if (customerId != null)
            {
                query = query.Where(returnRequest => returnRequest.CustomerId == customerId);
            }

            if (ids != null && ids.Count > 0)
            {
                query = query.Where(c => ids.Contains(c.Id));
            }

            if (status != null)
            {
                query = query.Where(returnRequest => returnRequest.ReturnRequestStatusId == (int)status);
            }

            if (createdAtMin != null)
            {
                query = query.Where(returnRequest => returnRequest.CreatedOnUtc > createdAtMin.Value.ToUniversalTime());
            }

            if (createdAtMax != null)
            {
                query = query.Where(returnRequest => returnRequest.CreatedOnUtc < createdAtMax.Value.ToUniversalTime());
            }

            if (storeId != null)
            {
                query = query.Where(returnRequest => returnRequest.StoreId == storeId);
            }

            if (unsynchronizedReturnRequestsOnly)
            {
                query = query.Where(returnRequest => !returnRequest.SynchronizedOnUtc.HasValue);
            }

            query = query.OrderBy(returnRequest => returnRequest.Id);

            return query;
        }

        #endregion

        #region Methods

        public IList<ReturnRequest> GetReturnRequests(IList<int> ids = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
           int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue, int sinceId = Configurations.DefaultSinceId,
           ReturnRequestStatus? status = null, int? customerId = null, int? storeId = null, bool unsynchronizedReturnRequestsOnly = false)
        {
            var query = GetReturnRequestsQuery(createdAtMin, createdAtMax, status, ids, customerId, storeId, unsynchronizedReturnRequestsOnly);

            if (sinceId > 0)
            {
                query = query.Where(returnRequest => returnRequest.Id > sinceId);
            }

            return new ApiList<ReturnRequest>(query, page - 1, limit);
        }

        public IList<ReturnRequest> GetSynchronizedIncompleteReturnRequests()
        {
            return _returnRequestRepository.TableNoTracking
                .Where(r => r.SynchronizedOnUtc.HasValue
                    && (r.ReturnRequestStatusId == (int)ReturnRequestStatus.Pending || r.ReturnRequestStatusId == (int)ReturnRequestStatus.Received || r.ReturnRequestStatusId == (int)ReturnRequestStatus.ReturnAuthorized))
                .ToList();
        }

        public void UpdateReturnRequests(IEnumerable<ReturnRequest> returnRequests)
        {
            _returnRequestRepository.Update(returnRequests);

            foreach (var returnRequest in returnRequests)
                _eventPublisher.EntityUpdated(returnRequest);
        }

        public int GetReturnRequestsReportTotal(int? storeId = null)
        {
            var query = _returnRequestRepository.TableNoTracking;

            if (storeId != null)
            {
                query = query.Where(returnRequest => returnRequest.StoreId == storeId);
            }

            return query.Count();
        }
        #endregion
    }
}