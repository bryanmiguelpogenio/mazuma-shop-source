﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Api.Constants;

namespace Nop.Plugin.Api.Services
{
    public interface IOrderApiService
    {
        IList<Order> GetOrdersByCustomerId(int customerId);

        /// <remarks>
        /// Updated by EcoRenew
        /// Added: unsynchronizedOrdersOnly, paidOnly
        /// </remarks>
        IList<Order> GetOrders(IList<int> ids = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                               int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue, 
                               int sinceId = Configurations.DefaultSinceId, OrderStatus? status = null, PaymentStatus? paymentStatus = null, 
                               ShippingStatus? shippingStatus = null, int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false);

        Order GetOrderById(int orderId);

        /// <remarks>
        /// Updated by EcoRenew
        /// Added: unsynchronizedOrdersOnly, paidOnly
        /// </remarks>
        int GetOrdersCount(DateTime? createdAtMin = null, DateTime? createdAtMax = null, OrderStatus? status = null,
                           PaymentStatus? paymentStatus = null, ShippingStatus? shippingStatus = null,
                           int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false);

        /// <remarks>Added by EcoRenew</remarks>
        IDictionary<string, int> GetProductsOrderCountPerSku();

        /// <remarks>Added by EcoRenew</remarks>
        IList<Order> GetSynchronizedIncompleteOrders(IList<int> ids = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                               int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
                               int sinceId = Configurations.DefaultSinceId, OrderStatus? status = null, PaymentStatus? paymentStatus = null,
                               ShippingStatus? shippingStatus = null, int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false);

        /// <remarks>Added by EcoRenew</remarks>
        int GetSynchronizedIncompleteOrderCount(IList<int> ids = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                               int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
                               int sinceId = Configurations.DefaultSinceId, OrderStatus? status = null, PaymentStatus? paymentStatus = null,
                               ShippingStatus? shippingStatus = null, int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false);

        /// <remarks>Added by EcoRenew</remarks>
        IList<Order> GetSynchronizedUndeliveredOrders(IList<int> ids = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                               int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
                               int sinceId = Configurations.DefaultSinceId, OrderStatus? status = null, PaymentStatus? paymentStatus = null,
                               ShippingStatus? shippingStatus = null, int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false);

        /// <remarks>Added by EcoRenew</remarks>
        int GetSynchronizedUndeliveredOrderCount(IList<int> ids = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                               int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
                               int sinceId = Configurations.DefaultSinceId, OrderStatus? status = null, PaymentStatus? paymentStatus = null,
                               ShippingStatus? shippingStatus = null, int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false);

        /// <remarks>Added by EcoRenew</remarks>
        void UpdateOrders(IEnumerable<Order> orders);

        /// <remarks>Added by EcoRenew</remarks>
        void InsertOrderNotes(IEnumerable<OrderNote> orderNotes);
    }
}