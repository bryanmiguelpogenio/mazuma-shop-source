﻿using Nop.Services.Configuration;
using System;

namespace Nop.Plugin.Api.Services
{
    /// <remarks>Added by EcoRenew</remarks>
    public interface ISettingsApiService : ISettingService
    {
        void SaveSetting(object settings, Type settingsType, int storeId = 0);
    }
}