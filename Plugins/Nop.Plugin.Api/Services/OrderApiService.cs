﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DataStructures;
using Nop.Services.Catalog;
using Nop.Services.Events;

namespace Nop.Plugin.Api.Services
{
    public class OrderApiService : IOrderApiService
    {
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<OrderNote> _orderNoteRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly IProductAttributeParser _productAttributeParser;

        public OrderApiService(IRepository<Order> orderRepository,
            IRepository<OrderItem> orderItemRepository, // Added by EcoRenew
            IRepository<OrderNote> orderNoteRepository, // Added by EcoRenew
            IEventPublisher eventPublisher, // Added by EcoRenew
            IProductAttributeParser productAttributeParser) // Added by EcoRenew
        {
            _orderRepository = orderRepository;
            _orderItemRepository = orderItemRepository;
            this._orderNoteRepository = orderNoteRepository;
            _eventPublisher = eventPublisher;
            this._productAttributeParser = productAttributeParser;
        }

        public IList<Order> GetOrdersByCustomerId(int customerId)
        {
            var query = from order in _orderRepository.TableNoTracking
                        where order.CustomerId == customerId && !order.Deleted
                        orderby order.Id
                        select order;

            return new ApiList<Order>(query, 0, Configurations.MaxLimit);
        }

        /// <remarks>
        /// Updated by EcoRenew
        /// Added: unsynchronizedOrdersOnly, paidOnly
        /// </remarks>
        public IList<Order> GetOrders(IList<int> ids = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
           int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue, int sinceId = Configurations.DefaultSinceId, 
           OrderStatus? status = null, PaymentStatus? paymentStatus = null, ShippingStatus? shippingStatus = null, int? customerId = null, 
           int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false)
        {
            var query = GetOrdersQuery(createdAtMin, createdAtMax, status, paymentStatus, shippingStatus, ids, customerId, storeId, unsynchronizedOrdersOnly, paidOnly);

            if (sinceId > 0)
            {
                query = query.Where(order => order.Id > sinceId);
            }

            return new ApiList<Order>(query, page - 1, limit);
        }

        public Order GetOrderById(int orderId)
        {
            if (orderId <= 0)
                return null;

            return _orderRepository.Table.FirstOrDefault(order => order.Id == orderId && !order.Deleted);
        }

        /// <remarks>
        /// Updated by EcoRenew
        /// Added: unsynchronizedOrdersOnly, paidOnly
        /// </remarks>
        public int GetOrdersCount(DateTime? createdAtMin = null, DateTime? createdAtMax = null, OrderStatus? status = null,
                                 PaymentStatus? paymentStatus = null,
                                 ShippingStatus? shippingStatus = null,
                                 int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false)
        {
            var query = GetOrdersQuery(createdAtMin, createdAtMax, status, paymentStatus, shippingStatus, customerId: customerId, storeId: storeId, unsynchronizedOrdersOnly: unsynchronizedOrdersOnly, paidOnly: paidOnly);

            return query.Count();
        }

        /// <remarks>Added by EcoRenew</remarks>
        public void UpdateOrders(IEnumerable<Order> orders)
        {
            _orderRepository.Update(orders);

            foreach (var order in orders)
                _eventPublisher.EntityUpdated(order);
        }

        /// <remarks>Added by EcoRenew</remarks>
        public void InsertOrderNotes(IEnumerable<OrderNote> orderNotes)
        {
            _orderNoteRepository.Insert(orderNotes);
        }

        /// <remarks>Added by EcoRenew</remarks>
        public IDictionary<string, int> GetProductsOrderCountPerSku()
        {
            var orderItems = _orderItemRepository.TableNoTracking
                .Where(oi => !oi.Order.Deleted
                    && (oi.Order.OrderStatusId == (int)OrderStatus.Pending || oi.Order.OrderStatusId == (int)OrderStatus.Processing))
                .ToList();

            return orderItems.GroupBy(oi => oi.Product.FormatSku(oi.AttributesXml, _productAttributeParser) ?? "")
                .ToDictionary(g => g.Key, g => g.Sum(oi => oi.Quantity));
        }

        /// <remarks>Added by EcoRenew</remarks>
        public IList<Order> GetSynchronizedUndeliveredOrders(IList<int> ids = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                               int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
                               int sinceId = Configurations.DefaultSinceId, OrderStatus? status = null, PaymentStatus? paymentStatus = null,
                               ShippingStatus? shippingStatus = null, int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false)
        {
            var query = GetOrdersQuery(createdAtMin, createdAtMax, status, paymentStatus, shippingStatus, customerId: customerId, storeId: storeId, unsynchronizedOrdersOnly: unsynchronizedOrdersOnly, paidOnly: paidOnly);

            query = query.Where(o => o.SynchronizedOnUtc.HasValue
                    && (o.ShippingStatusId != (int)ShippingStatus.Delivered && o.ShippingStatusId != (int)ShippingStatus.ShippingNotRequired));

            return new ApiList<Order>(query, page - 1, limit);
        }

        /// <remarks>Added by EcoRenew</remarks>
        public int GetSynchronizedUndeliveredOrderCount(IList<int> ids = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                               int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
                               int sinceId = Configurations.DefaultSinceId, OrderStatus? status = null, PaymentStatus? paymentStatus = null,
                               ShippingStatus? shippingStatus = null, int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false)
        {
            var query = GetOrdersQuery(createdAtMin, createdAtMax, status, paymentStatus, shippingStatus, customerId: customerId, storeId: storeId, unsynchronizedOrdersOnly: unsynchronizedOrdersOnly, paidOnly: paidOnly);

            query = query.Where(o => o.SynchronizedOnUtc.HasValue
                    && (o.ShippingStatusId != (int)ShippingStatus.Delivered && o.ShippingStatusId != (int)ShippingStatus.ShippingNotRequired));

            return query.Count();
        }

        /// <remarks>Added by EcoRenew</remarks>
        public IList<Order> GetSynchronizedIncompleteOrders(IList<int> ids = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                               int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
                               int sinceId = Configurations.DefaultSinceId, OrderStatus? status = null, PaymentStatus? paymentStatus = null,
                               ShippingStatus? shippingStatus = null, int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false)
        {
            var query = GetOrdersQuery(createdAtMin, createdAtMax, status, paymentStatus, shippingStatus, customerId: customerId, storeId: storeId, unsynchronizedOrdersOnly: unsynchronizedOrdersOnly, paidOnly: paidOnly);

            query = query.Where(o => o.SynchronizedOnUtc.HasValue
                    && (o.OrderStatusId == (int)OrderStatus.Pending || o.OrderStatusId == (int)OrderStatus.Processing));

            return new ApiList<Order>(query, page - 1, limit);
        }

        /// <remarks>Added by EcoRenew</remarks>
        public int GetSynchronizedIncompleteOrderCount(IList<int> ids = null, DateTime? createdAtMin = null, DateTime? createdAtMax = null,
                               int limit = Configurations.DefaultLimit, int page = Configurations.DefaultPageValue,
                               int sinceId = Configurations.DefaultSinceId, OrderStatus? status = null, PaymentStatus? paymentStatus = null,
                               ShippingStatus? shippingStatus = null, int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false)
        {
            var query = GetOrdersQuery(createdAtMin, createdAtMax, status, paymentStatus, shippingStatus, customerId: customerId, storeId: storeId, unsynchronizedOrdersOnly: unsynchronizedOrdersOnly, paidOnly: paidOnly);

            query = query.Where(o => o.SynchronizedOnUtc.HasValue
                    && (o.OrderStatusId == (int)OrderStatus.Pending || o.OrderStatusId == (int)OrderStatus.Processing));

            return query.Count();
        }

        /// <remarks>
        /// Updated by EcoRenew
        /// Added: unsynchronizedOrdersOnly, paidOnly
        /// </remarks>
        private IQueryable<Order> GetOrdersQuery(DateTime? createdAtMin = null, DateTime? createdAtMax = null, OrderStatus? status = null,
            PaymentStatus? paymentStatus = null, ShippingStatus? shippingStatus = null, IList<int> ids = null, 
            int? customerId = null, int? storeId = null, bool unsynchronizedOrdersOnly = false, bool paidOnly = false)
        {
            var query = _orderRepository.TableNoTracking;

            if (customerId != null)
            {
                query = query.Where(order => order.CustomerId == customerId);
            }

            if (ids != null && ids.Count > 0)
            {
                query = query.Where(c => ids.Contains(c.Id));
            }
            
            if (status != null)
            {
                query = query.Where(order => order.OrderStatusId == (int)status);
            }
            
            if (paymentStatus != null)
            {
                query = query.Where(order => order.PaymentStatusId == (int)paymentStatus);
            }
            
            if (shippingStatus != null)
            {
                query = query.Where(order => order.ShippingStatusId == (int)shippingStatus);
            }

            query = query.Where(order => !order.Deleted);

            if (createdAtMin != null)
            {
                query = query.Where(order => order.CreatedOnUtc > createdAtMin.Value.ToUniversalTime());
            }

            if (createdAtMax != null)
            {
                query = query.Where(order => order.CreatedOnUtc < createdAtMax.Value.ToUniversalTime());
            }

            if (storeId != null)
            {
                query = query.Where(order => order.StoreId == storeId);
            }

            if (unsynchronizedOrdersOnly)
            {
                query = query.Where(order => !order.SynchronizedOnUtc.HasValue);
            }

            if (paidOnly)
            {
                query = query.Where(order => order.PaidDateUtc.HasValue);
            }

            query = query.OrderBy(order => order.Id);

            return query;
        }
    }
}