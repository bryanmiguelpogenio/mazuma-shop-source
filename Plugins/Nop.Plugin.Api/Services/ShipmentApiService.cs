﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.Shipping;
using Nop.Services.Events;

namespace Nop.Plugin.Api.Services
{
    /// <remarks>Added by EcoRenew</remarks>
    public class ShipmentApiService : IShipmentApiService
    {
        #region Fields

        private readonly IRepository<Shipment> _shipmentRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        public ShipmentApiService(IRepository<Shipment> shipmentRepository,
            IEventPublisher eventPublisher)
        {
            this._shipmentRepository = shipmentRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        public void InsertShipments(IEnumerable<Shipment> shipments)
        {
            if (shipments == null)
                throw new ArgumentNullException(nameof(shipments));

            _shipmentRepository.Insert(shipments);

            foreach (var shipment in shipments)
                _eventPublisher.EntityInserted(shipment);
        }

        public Shipment GetShipmentById(int shipmentId)
        {
            return _shipmentRepository.GetById(shipmentId);
        }

        public IEnumerable<Shipment> GetShipmentsByTrackingNumber(string trackingNumber)
        {
            return _shipmentRepository.Table.Where(x => x.TrackingNumber == trackingNumber);
        }

        public void UpdateShipment(Shipment shipment)
        {
            _shipmentRepository.Update(shipment);
        }
    }
}