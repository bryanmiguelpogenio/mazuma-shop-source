﻿using System;
using Newtonsoft.Json;
using Nop.Core.Domain.Orders;

namespace Nop.Plugin.Api.Models.ReturnRequestsParameters
{
    // JsonProperty is used only for swagger
    /// <remarks>Added by EcoRenew</remarks>
    public class BaseReturnRequestsParametersModel
    {
        public BaseReturnRequestsParametersModel()
        {
            CreatedAtMax = null;
            CreatedAtMin = null;
            Status = null;
            CustomerId = null;
        }

        /// <summary>
        /// Show return requests created after date (format: 2008-12-31 03:00)
        /// </summary>
        [JsonProperty("created_at_min")]
        public DateTime? CreatedAtMin { get; set; }

        /// <summary>
        /// Show return requests created before date(format: 2008-12-31 03:00)
        /// </summary>
        [JsonProperty("created_at_max")]
        public DateTime? CreatedAtMax { get; set; }

        [JsonProperty("status")]
        public ReturnRequestStatus? Status { get; set; }

        /// <summary>
        /// Show all the return requests for this customer
        /// </summary>
        [JsonProperty("customer_id")]
        public int? CustomerId { get; set; }
    }
}