﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.ReturnRequestsParameters
{
    using Microsoft.AspNetCore.Mvc;

    // JsonProperty is used only for swagger
    /// <remarks>Added by EcoRenew</remarks>
    [ModelBinder(typeof(ParametersModelBinder<ReturnRequestsParametersModel>))]
    public class ReturnRequestsParametersModel : BaseReturnRequestsParametersModel
    {
        public ReturnRequestsParametersModel()
        {
            Ids = null;
            Limit = Configurations.DefaultLimit;
            Page = Configurations.DefaultPageValue;
            SinceId = Configurations.DefaultSinceId;
            Fields = string.Empty;
            UnsynchronizedOnly = false;
        }

        /// <summary>
        /// A comma-separated list of return request ids
        /// </summary>
        [JsonProperty("ids")]
        public List<int> Ids { get; set; }

        /// <summary>
        /// Amount of results (default: 50) (maximum: 250)
        /// </summary>
        [JsonProperty("limit")]
        public int Limit { get; set; }

        /// <summary>
        /// Page to show (default: 1)
        /// </summary>
        [JsonProperty("page")]
        public int Page { get; set; }

        /// <summary>
        /// Restrict results to after the specified ID
        /// </summary>
        [JsonProperty("since_id")]
        public int SinceId { get; set; }

        /// <summary>
        /// comma-separated list of fields to include in the response
        /// </summary>
        [JsonProperty("fields")]
        public string Fields { get; set; }

        /// <summary>
        /// Set to true to get the unsynchronized return requests only
        /// </summary>
        [JsonProperty("unsynchronized_only")]
        public bool UnsynchronizedOnly { get; set; }
    }
}