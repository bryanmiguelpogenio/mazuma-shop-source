﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Nop.Plugin.Api.Models.EcorenewConsumerFinance
{
    public class ConsumerFinanceSignupsParametersModel
    {
        /// <summary>
        /// A comma-separated list of order ids
        /// </summary>
        [JsonProperty("order_ids")]
        public List<int> OrderIds { get; set; }
    }
}