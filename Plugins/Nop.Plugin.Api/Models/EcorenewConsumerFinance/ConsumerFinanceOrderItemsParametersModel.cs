﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Nop.Plugin.Api.Models.EcorenewConsumerFinance
{
    public class ConsumerFinanceOrderItemsParametersModel
    {
        /// <summary>
        /// A comma-separated list of order item ids
        /// </summary>
        [JsonProperty("order_item_ids")]
        public List<int> OrderItemIds { get; set; }
    }
}