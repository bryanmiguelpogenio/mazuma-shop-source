﻿using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;
using System.Collections.Generic;
using System.Web.Http.ModelBinding;

namespace Nop.Plugin.Api.Models.GiftCardParameters
{
    [ModelBinder(typeof(ParametersModelBinder<GiftCardParametersModel>))]
    public class GiftCardParametersModel
    {
        public GiftCardParametersModel()
        {
            Ids = null;
            Limit = Configurations.DefaultLimit;
            Page = Configurations.DefaultPageValue;
            SinceId = Configurations.DefaultSinceId;
            Fields = string.Empty;
        }

        [JsonProperty("ids")]
        public List<int> Ids { get; set; }

        [JsonProperty("limit")]
        public int Limit { get; set; }

        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("since_id")]
        public int SinceId { get; set; }

        [JsonProperty("fields")]
        public string Fields { get; set; }

    }
}
