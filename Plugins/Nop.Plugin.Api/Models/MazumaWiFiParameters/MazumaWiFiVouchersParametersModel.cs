﻿using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.MazumaWiFiParameters
{
    using Microsoft.AspNetCore.Mvc;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<MazumaWiFiVouchersParametersModel>))]
    public class MazumaWiFiVouchersParametersModel
    {
        public MazumaWiFiVouchersParametersModel()
        {
            Limit = Configurations.DefaultLimit;
            Page = Configurations.DefaultPageValue;
            WithOrderOnly = false;
            UnsynchronizedOnly = false;
        }

        /// <summary>
        /// Amount of results (default: 50) (maximum: 250)
        /// </summary>
        [JsonProperty("limit")]
        public int Limit { get; set; }

        /// <summary>
        /// Page to show (default: 1)
        /// </summary>
        [JsonProperty("page")]
        public int Page { get; set; }

        /// <summary>
        /// comma-separated list of fields to include in the response
        /// </summary>
        [JsonProperty("fields")]
        public string Fields { get; set; }
        
        [JsonProperty("with_order_only")]
        public bool WithOrderOnly { get; set; }

        [JsonProperty("unsynchronized_only")]
        public bool UnsynchronizedOnly { get; set; }
    }
}
