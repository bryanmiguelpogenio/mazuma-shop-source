﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Errors;
using Nop.Plugin.Api.DTOs.ReturnRequests;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.ReturnRequestsParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ReturnRequestsController : BaseApiController
    {
        #region Fields

        private readonly IDTOHelper _dtoHelper;
        private readonly IReturnRequestApiService _returnRequestApiService;
        private readonly IStoreContext _storeContext;

        #endregion

        #region Ctor

        public ReturnRequestsController(IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            IDTOHelper dTOHelper,
            IReturnRequestApiService returnRequestApiService,
            IStoreContext storeContext)
            : base(jsonFieldsSerializer,
                  aclService,
                  customerService,
                  storeMappingService,
                  storeService,
                  discountService,
                  customerActivityService,
                  localizationService,
                  pictureService)
        {
            this._dtoHelper = dTOHelper;
            this._returnRequestApiService = returnRequestApiService;
            this._storeContext = storeContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Receive a list of all Return Requests
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/return_requests")]
        [ProducesResponseType(typeof(ReturnRequestsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetReturnRequests(ReturnRequestsParametersModel parameters)
        {
            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
            }

            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid limit parameter");
            }

            var storeId = _storeContext.CurrentStore.Id;
            
            IList<ReturnRequest> returnRequests = _returnRequestApiService.GetReturnRequests(parameters.Ids, parameters.CreatedAtMin,
                parameters.CreatedAtMax,
                parameters.Limit, parameters.Page, parameters.SinceId,
                parameters.Status, parameters.CustomerId, storeId, parameters.UnsynchronizedOnly);

            IList<ReturnRequestDto> returnRequestsAsDtos = returnRequests.Select(x => _dtoHelper.PrepareReturnRequestDTO(x)).ToList();

            var returnRequestsRootObject = new ReturnRequestsRootObject()
            {
                ReturnRequests = returnRequestsAsDtos
            };

            var json = _jsonFieldsSerializer.Serialize(returnRequestsRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/return_requests/synchronize")]
        [ProducesResponseType(typeof(SynchronizedReturnRequestsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult SetReturnRequestsAsSynchronized([ModelBinder(typeof(JsonModelBinder<SynchronizedReturnRequestsRootObject>))] Delta<SynchronizedReturnRequestsRootObject> synchronizedReturnRequestsRootObjectDelta)
        {
            var returnRequests = _returnRequestApiService.GetReturnRequests(ids: synchronizedReturnRequestsRootObjectDelta.Dto.SynchronizedReturnRequests.Select(x => x.Id).ToArray()).ToList();

            returnRequests.ForEach(o =>
            {
                o.SynchronizedOnUtc = synchronizedReturnRequestsRootObjectDelta.Dto.SynchronizedReturnRequests.FirstOrDefault(x => x.Id == o.Id)?.SynchronizedOnUtc;
            });

            _returnRequestApiService.UpdateReturnRequests(returnRequests);

            var json = _jsonFieldsSerializer.Serialize(synchronizedReturnRequestsRootObjectDelta.Dto, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/return_requests/synchronized_incomplete")]
        [ProducesResponseType(typeof(ReturnRequestsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetSynchronizedIncompleteReturnRequests()
        {
            var returnRequestsRootObject = new ReturnRequestsRootObject()
            {
                ReturnRequests = _returnRequestApiService.GetSynchronizedIncompleteReturnRequests().Select(x => _dtoHelper.PrepareReturnRequestDTO(x)).ToList()
            };

            var json = _jsonFieldsSerializer.Serialize(returnRequestsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/return_requests/set_statuses")]
        [ProducesResponseType(typeof(ReturnRequestsReturnRequestStatusesRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult SetReturnRequestsStatuses([ModelBinder(typeof(JsonModelBinder<ReturnRequestsReturnRequestStatusesRootObject>))] Delta<ReturnRequestsReturnRequestStatusesRootObject> returnRequestsReturnRequestStatusesRootObject)
        {
            var returnRequests = _returnRequestApiService.GetReturnRequests(ids: returnRequestsReturnRequestStatusesRootObject.Dto.ReturnRequestsReturnRequestStatuses.Select(x => x.ReturnRequestId).ToArray()).ToList();

            returnRequests.ForEach(o =>
            {
                var dto = returnRequestsReturnRequestStatusesRootObject.Dto.ReturnRequestsReturnRequestStatuses.FirstOrDefault(x => x.ReturnRequestId == o.Id);

                if (!Enum.TryParse(dto.ReturnRequestStatus, out ReturnRequestStatus returnRequestStatus))
                    throw new Exception($"Unable to parse return request status for return request ID: {o.Id}. Return request status: {dto.ReturnRequestStatus}");

                o.ReturnRequestStatus = returnRequestStatus;
            });

            _returnRequestApiService.UpdateReturnRequests(returnRequests);

            var json = _jsonFieldsSerializer.Serialize(returnRequestsReturnRequestStatusesRootObject.Dto, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/return_requests/report")]
        [ProducesResponseType(typeof(ReturnRequestReportDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetReturnRequestsReport(ReturnRequestsParametersModel parameters)
        {
            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
            }

            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid limit parameter");
            }

            var storeId = _storeContext.CurrentStore.Id;

            var returnRequests = _returnRequestApiService.GetReturnRequests(page: parameters.Page, limit: parameters.Limit, storeId: storeId);
            var returnRequestsDtos = returnRequests.Select(x => _dtoHelper.PrepareReturnRequestReportDto(x)).ToList();

            var returnRequestsRootObject = new ReturnRequestsReportRootObject()
            {
                ReturnRequests = returnRequestsDtos
            };

            var json = _jsonFieldsSerializer.Serialize(returnRequestsRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/return_requests/report/total")]
        [ProducesResponseType(typeof(ReturnRequestsReportTotalRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetReturnRequestsReportCount()
        {
            var storeId = _storeContext.CurrentStore.Id;

            int total = _returnRequestApiService.GetReturnRequestsReportTotal(storeId: storeId);

            var returnRequestsTotal = new ReturnRequestsReportTotalRootObject()
            {
                Total = total
            };

            return Ok(returnRequestsTotal);
        }
        #endregion
    }
}