﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DTOs.MazumaWiFi;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.Services;
using Nop.Plugin.Mazuma.WiFiVoucher.Domain;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Stores;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Nop.Plugin.Api.Delta;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.JSON.Serializers;
    using Nop.Plugin.Api.ModelBinders;
    using Nop.Plugin.Api.Models.MazumaWiFiParameters;
    using Nop.Services.Media;

    /// <remarks>
    /// Added by EcoRenew
    /// </remarks>
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MazumaWiFiController : BaseApiController
    {
        #region Fields

        private readonly IMazumaWiFiApiService _mazumaWiFiApiService;
        private readonly IDTOHelper _dtoHelper;

        #endregion

        #region Ctor

        public MazumaWiFiController(IMazumaWiFiApiService mazumaWiFiApiService,
            IDTOHelper dtoHelper,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IProductService productService,
            IPictureService pictureService)
            : base(jsonFieldsSerializer, aclService, customerService, storeMappingService,
                 storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            this._mazumaWiFiApiService = mazumaWiFiApiService;
            this._dtoHelper = dtoHelper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Receive a list of all Mazuma WiFi Vouchers
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/mazuma_wifi_vouchers")]
        [ProducesResponseType(typeof(MazumaWiFiVouchersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetMazumaWiFiVouchers(MazumaWiFiVouchersParametersModel parameters)
        {
            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
            }

            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid limit parameter");
            }

            IList<MazumaWiFiVoucher> vouchers = _mazumaWiFiApiService.GetMazumaWiFiVouchers(parameters.Limit, parameters.Page,
                parameters.WithOrderOnly, parameters.UnsynchronizedOnly);

            IList<MazumaWiFiVoucherDto> vouchersAsDtos = vouchers.Select(x => _dtoHelper.PrepareMazumaWiFiVoucherDto(x)).ToList();

            var mazumaWiFiVouchersRootObject = new MazumaWiFiVouchersRootObject()
            {
                MazumaWiFiVouchers = vouchersAsDtos
            };

            var json = _jsonFieldsSerializer.Serialize(mazumaWiFiVouchersRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/mazuma_wifi_vouchers/synchronize")]
        [ProducesResponseType(typeof(SynchronizedMazumaWiFiVouchersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult SetMazumaWiFiVouchersAsSynchronized([ModelBinder(typeof(JsonModelBinder<SynchronizedMazumaWiFiVouchersRootObject>))] Delta<SynchronizedMazumaWiFiVouchersRootObject> mazumaWiFiVouchersSynchronizedDelta)
        {
            var mazumaWiFiVouchers = _mazumaWiFiApiService.GetMazumaWiFiVouchersByIds(mazumaWiFiVouchersSynchronizedDelta.Dto.SynchronizedMazumaWiFiVouchers.Select(x => x.Id).ToArray()).ToList();

            mazumaWiFiVouchers.ForEach(o =>
            {
                o.SynchronizedOnUtc = mazumaWiFiVouchersSynchronizedDelta.Dto.SynchronizedMazumaWiFiVouchers.FirstOrDefault(x => x.Id == o.Id).SynchronizedOnUtc;
            });

            _mazumaWiFiApiService.UpdateMazumaWiFiVouchers(mazumaWiFiVouchers);

            var json = _jsonFieldsSerializer.Serialize(mazumaWiFiVouchersSynchronizedDelta.Dto, string.Empty);

            return new RawJsonActionResult(json);
        }

        #endregion
    }
}