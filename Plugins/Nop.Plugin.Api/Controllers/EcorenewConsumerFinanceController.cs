﻿using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;
using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Tax;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.EcorenewConsumerFinance;
using Nop.Plugin.Api.DTOs.Errors;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Tax.FixedOrByCountryStateZip.Domain;
using Nop.Plugin.Tax.FixedOrByCountryStateZip.Services;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Tax;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Plugin.Api.Models.EcorenewConsumerFinance;
using Nop.Plugin.Api.MappingExtensions;
using System;

namespace Nop.Plugin.Api.Controllers
{
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class EcorenewConsumerFinanceController : BaseApiController
    {
        #region Fields

        private readonly IConsumerFinanceService _consumerFinanceService;
        private readonly ITaxCategoryService _taxCategoryService;
        private readonly ICountryService _countryService;
        private readonly ICountryStateZipService _taxRateService;

        #endregion

        #region Ctor

        public EcorenewConsumerFinanceController(IConsumerFinanceService consumerFinanceService,
            ITaxCategoryService taxCategoryService,
            ICountryService countryService,
            ICountryStateZipService taxRateService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPictureService pictureService)
            : base(jsonFieldsSerializer,
                  aclService,
                  customerService,
                  storeMappingService,
                  storeService,
                  discountService,
                  customerActivityService,
                  localizationService,
                  pictureService)
        {
            this._consumerFinanceService = consumerFinanceService;
            this._taxCategoryService = taxCategoryService;
            this._countryService = countryService;
            this._taxRateService = taxRateService;
        }

        #endregion

        #region Methods

        [HttpGet]
        [Route("/api/ecorenew_consumer_finance_options")]
        [ProducesResponseType(typeof(ConsumerFinanceOptionsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetAllConsumerFinanceOptions()
        {
            IList<ConsumerFinanceOptionDto> allConsumerFinanceOptions = _consumerFinanceService.GetConsumerFinanceOptions(includeInactive: true)
                .Select(o => new ConsumerFinanceOptionDto
                {
                    AgreementRate = o.AgreementRate,
                    Description = o.Description,
                    DurationInMonths = o.DurationInMonths,
                    Id = o.Id,
                    IsActive = o.IsActive
                })
                .ToList();

            var consumerFinanceOptionsRootObject = new ConsumerFinanceOptionsRootObjectDto()
            {
                ConsumerFinanceOptions = allConsumerFinanceOptions
            };

            var json = _jsonFieldsSerializer.Serialize(consumerFinanceOptionsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/ecorenew_consumer_finance_options/{id}")]
        [ProducesResponseType(typeof(ConsumerFinanceOptionDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetConsumerFinanceOptionById(int id, string fields = "")
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var consumerFinanceOption = _consumerFinanceService.GetConsumerFinanceOptionById(id);
            if (consumerFinanceOption == null)
                return Error(HttpStatusCode.NotFound, "consumerFinanceOption", "consumer finance option not found");

            var consumerFinanceOptionDto = new ConsumerFinanceOptionDto
            {
                AgreementRate = consumerFinanceOption.AgreementRate,
                Description = consumerFinanceOption.Description,
                DurationInMonths = consumerFinanceOption.DurationInMonths,
                Id = consumerFinanceOption.Id,
                IsActive = consumerFinanceOption.IsActive
            };

            var consumerFinanceOptionsRootObject = new ConsumerFinanceOptionsRootObjectDto();

            consumerFinanceOptionsRootObject.ConsumerFinanceOptions.Add(consumerFinanceOptionDto);

            var json = _jsonFieldsSerializer.Serialize(consumerFinanceOptionsRootObject, fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/ecorenew_consumer_finance_options")]
        [ProducesResponseType(typeof(ConsumerFinanceOptionsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult CreateConsumerFinanceOption([ModelBinder(typeof(JsonModelBinder<ConsumerFinanceOptionDto>))] Delta<ConsumerFinanceOptionDto> consumerFinanceOptionDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            //If the validation has passed the consumerFinanceOptionDelta object won't be null for sure so we don't need to check for this.

            // Inserting the new consumerFinanceOption
            var consumerFinanceOption = new ConsumerFinanceOption
            {
                AgreementRate = consumerFinanceOptionDelta.Dto.AgreementRate,
                Description = consumerFinanceOptionDelta.Dto.Description,
                DurationInMonths = consumerFinanceOptionDelta.Dto.DurationInMonths,
                IsActive = consumerFinanceOptionDelta.Dto.IsActive
            };

            consumerFinanceOptionDelta.Merge(consumerFinanceOption);

            _consumerFinanceService.InsertConsumerFinanceOption(consumerFinanceOption);

            // Preparing the result dto of the new consumerFinanceOption
            var newConsumerFinanceOptionDto = new ConsumerFinanceOptionDto
            {
                AgreementRate = consumerFinanceOption.AgreementRate,
                Description = consumerFinanceOption.Description,
                DurationInMonths = consumerFinanceOption.DurationInMonths,
                Id = consumerFinanceOption.Id,
                IsActive = consumerFinanceOption.IsActive
            };

            var consumerFinanceOptionsRootObject = new ConsumerFinanceOptionsRootObjectDto();

            consumerFinanceOptionsRootObject.ConsumerFinanceOptions.Add(newConsumerFinanceOptionDto);

            var json = _jsonFieldsSerializer.Serialize(consumerFinanceOptionsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPut]
        [Route("/api/ecorenew_consumer_finance_options/{id}")]
        [ProducesResponseType(typeof(ConsumerFinanceOptionsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        public IActionResult UpdateConsumerFinanceOption(
            [ModelBinder(typeof(JsonModelBinder<ConsumerFinanceOptionDto>))] Delta<ConsumerFinanceOptionDto> consumerFinanceOptionDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // We do not need to validate the consumerFinanceOption id, because this will happen in the model binder using the dto validator.

            var consumerFinanceOption = _consumerFinanceService.GetConsumerFinanceOptionById(consumerFinanceOptionDelta.Dto.Id);
            if (consumerFinanceOption == null)
                return Error(HttpStatusCode.NotFound, "consumerFinanceOption", "consumer finance option not found");

            consumerFinanceOptionDelta.Merge(consumerFinanceOption);

            _consumerFinanceService.UpdateConsumerFinanceOption(consumerFinanceOption);

            var consumerFinanceOptionDto = new ConsumerFinanceOptionDto
            {
                AgreementRate = consumerFinanceOption.AgreementRate,
                Description = consumerFinanceOption.Description,
                DurationInMonths = consumerFinanceOption.DurationInMonths,
                Id = consumerFinanceOption.Id,
                IsActive = consumerFinanceOption.IsActive
            };

            var consumerFinanceOptionsRootObject = new ConsumerFinanceOptionsRootObjectDto();

            consumerFinanceOptionsRootObject.ConsumerFinanceOptions.Add(consumerFinanceOptionDto);

            var json = _jsonFieldsSerializer.Serialize(consumerFinanceOptionsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/local_vat")]
        [ProducesResponseType(typeof(LocalVatsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult CreateOrUpdateLocalVatPercentage([ModelBinder(typeof(JsonModelBinder<LocalVatDto>))] Delta<LocalVatDto> localVatDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // Check if there is a local VAT category. Create if there isn't
            var localVatCategory = _taxCategoryService.GetAllTaxCategories().FirstOrDefault(tc => tc.Name.ToLower() == "local vat");
            if (localVatCategory == null)
            {
                localVatCategory = new TaxCategory
                {
                    Name = "Local VAT",
                    DisplayOrder = 0
                };
                _taxCategoryService.InsertTaxCategory(localVatCategory);
            }

            var countries = _countryService.GetAllCountries(showHidden: true);

            foreach (var country in countries)
            {
                // Get existing local VATs
                var localVats = _taxRateService.GetAllTaxRates()
                    .Where(t => t.CountryId == country.Id && t.StoreId == localVatDelta.Dto.StoreId && t.StateProvinceId == 0)
                    .ToList();

                if (localVats.Any())
                {
                    localVats.ForEach(lv =>
                    {
                        lv.Percentage = localVatDelta.Dto.Percentage;
                        _taxRateService.UpdateTaxRate(lv);
                    });
                }
                else
                {
                    var newLocalVat = new TaxRate
                    {
                        CountryId = country.Id,
                        Percentage = localVatDelta.Dto.Percentage,
                        StateProvinceId = 0,
                        StoreId = localVatDelta.Dto.StoreId,
                        TaxCategoryId = localVatCategory.Id
                    };

                    _taxRateService.InsertTaxRate(newLocalVat);
                }
            }

            var localVatsRootObject = new LocalVatsRootObjectDto
            {
                LocalVats = new List<LocalVatDto>
                {
                    localVatDelta.Dto
                }
            };

            var json = _jsonFieldsSerializer.Serialize(localVatsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/ecorenew_consumer_finance_signups")]
        [ProducesResponseType(typeof(ConsumerFinanceSignupsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetAllConsumerFinanceSignups(ConsumerFinanceSignupsParametersModel parameters)
        {
            IList<ConsumerFinanceSignupDto> allConsumerFinanceSignups = _consumerFinanceService.GetConsumerFinanceSignupsByOrderIds(parameters.OrderIds)
                .Select(x => x.ToDto())
                .ToList();

            var consumerFinanceSignupsRootObject = new ConsumerFinanceSignupsRootObjectDto()
            {
                ConsumerFinanceSignups = allConsumerFinanceSignups
            };

            var json = _jsonFieldsSerializer.Serialize(consumerFinanceSignupsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/ecorenew_consumer_finance_order_items")]
        [ProducesResponseType(typeof(ConsumerFinanceOrderItemsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetAllConsumerFinanceOrderItems(string orderItemIds)
        {
            IList<ConsumerFinanceOrderItemDto> allConsumerFinanceOrderItems = _consumerFinanceService.GetConsumerFinanceOrderItemsByOrderItemIds(orderItemIds.Split(',').Select(Int32.Parse))
                .Select(x => x.ToDto())
                .ToList();

            var consumerFinanceOrderItemsRootObject = new ConsumerFinanceOrderItemsRootObjectDto()
            {
                ConsumerFinanceOrderItems = allConsumerFinanceOrderItems
            };

            var json = _jsonFieldsSerializer.Serialize(consumerFinanceOrderItemsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        #endregion
    }
}