﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Errors;
using Nop.Plugin.Api.DTOs.Shipping;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Services;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    /// <remarks>Added by EcoRenew</remarks>
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ShippingController : BaseApiController
    {
        #region Fields

        private readonly IShipmentApiService _shipmentApiService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IOrderService _orderService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILogger _logger;

        #endregion

        #region Ctor

        public ShippingController(IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            IShipmentApiService shipmentApiService,
            IWorkflowMessageService workflowMessageService,
            IOrderService orderService,
            IEventPublisher eventPublisher,
            ILogger logger)
            : base(jsonFieldsSerializer,
                  aclService,
                  customerService,
                  storeMappingService,
                  storeService,
                  discountService,
                  customerActivityService,
                  localizationService,
                  pictureService)
        {
            this._shipmentApiService = shipmentApiService;
            this._workflowMessageService = workflowMessageService;
            this._orderService = orderService;
            this._eventPublisher = eventPublisher;
            this._logger = logger;
        }

        #endregion

        #region Utilities

        /// <remarks>Added by EcoRenew</remarks>
        protected virtual void AddOrderNote(Order order, string note)
        {
            order.OrderNotes.Add(new OrderNote
            {
                Note = note,
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });

            _orderService.UpdateOrder(order);
        }

        #endregion

        #region Methods

        /// <remarks>Added by EcoRenew</remarks>
        [HttpPost]
        [Route("/api/shipments")]
        [ProducesResponseType(typeof(ShipmentsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult CreateShipments([ModelBinder(typeof(JsonModelBinder<ShipmentsRootObject>))] Delta<ShipmentsRootObject> shipmentsRootObjectDelta)
        {
            var shipments = new List<Shipment>();
            
            shipmentsRootObjectDelta.Dto.Shipments
                .ToList()
                .ForEach(s =>
                {
                    var shipment = new Shipment
                    {
                        AdminComment = s.AdminComment,
                        CreatedOnUtc = s.CreatedOnUtc,
                        DeliveryDateUtc = s.DeliveryDateUtc,
                        OrderId = s.OrderId,
                        ShippedDateUtc = s.ShippedDateUtc,
                        TotalWeight = s.TotalWeight,
                        TrackingNumber = s.TrackingNumber
                    };

                    // Shipment items
                    s.ShipmentItems.ToList()
                        .ForEach(si =>
                        {
                            shipment.ShipmentItems.Add(new ShipmentItem
                            {
                                OrderItemId = si.OrderItemId,
                                Quantity = si.Quantity,
                                WarehouseId = si.WarehouseId
                            });
                        });

                    shipments.Add(shipment);
                });

            _shipmentApiService.InsertShipments(shipments);

            foreach (var shipment in shipments)
            {
                if (shipment.ShippedDateUtc.HasValue)
                {
                    var order = _orderService.GetOrderById(shipment.OrderId);
                    if (order == null)
                        throw new Exception("Order cannot be loaded");

                    //check whether we have more items to ship
                    if (order.HasItemsToAddToShipment() || order.HasItemsToShip())
                        order.ShippingStatusId = (int)ShippingStatus.PartiallyShipped;
                    else
                        order.ShippingStatusId = (int)ShippingStatus.Shipped;
                    _orderService.UpdateOrder(order);

                    //add a note
                    AddOrderNote(order, $"Shipment# {shipment.Id} has been sent");
                    
                    //notify customer
                    var queuedEmailId = _workflowMessageService.SendShipmentSentCustomerNotification(shipment, order.CustomerLanguageId);
                    if (queuedEmailId > 0)
                    {
                        AddOrderNote(order, $"\"Shipped\" email (to customer) has been queued. Queued email identifier: {queuedEmailId}.");
                    }

                    //event
                    _eventPublisher.PublishShipmentSent(shipment);
                }
            }

            var json = _jsonFieldsSerializer.Serialize(shipmentsRootObjectDelta.Dto, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <remarks>Added by EcoRenew</remarks>
        [HttpPost]
        [Route("/api/shipment_delivery_dates")]
        [ProducesResponseType(typeof(ShipmentDeliveryDatesRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult UpdateShipmentDeliveryDates([ModelBinder(typeof(JsonModelBinder<ShipmentDeliveryDatesRootObject>))] Delta<ShipmentDeliveryDatesRootObject> shipmentDeliveryDatesRootObjectDelta)
        {
            try
            {
                foreach (var shipmentDeliveryDate in shipmentDeliveryDatesRootObjectDelta.Dto.ShipmentDeliveryDates)
                {
                    // get shipment by tracking number
                    var shipments = _shipmentApiService.GetShipmentsByTrackingNumber(shipmentDeliveryDate.TrackingNumber)
                        .ToList();

                    foreach (var shipment in shipments)
                    {
                        if (shipment.ShippedDateUtc.HasValue && !shipment.DeliveryDateUtc.HasValue && shipmentDeliveryDate.DeliveryDateUtc.HasValue)
                        {
                            shipment.DeliveryDateUtc = shipmentDeliveryDate.DeliveryDateUtc;
                            _shipmentApiService.UpdateShipment(shipment);

                            var order = shipment.Order;
                            if (order == null)
                                throw new Exception("Order cannot be loaded");

                            if (!order.HasItemsToAddToShipment() && !order.HasItemsToShip() && !order.HasItemsToDeliver())
                            {
                                order.ShippingStatusId = (int)ShippingStatus.Delivered;
                                order.OrderStatusId = (int)OrderStatus.Complete;
                            }
                            _orderService.UpdateOrder(order);

                            //add a note
                            AddOrderNote(order, $"Shipment# {shipment.Id} has been delivered");

                            //send email notification
                            var queuedEmailId = _workflowMessageService.SendShipmentDeliveredCustomerNotification(shipment, order.CustomerLanguageId);
                            if (queuedEmailId > 0)
                            {
                                AddOrderNote(order, $"\"Delivered\" email (to customer) has been queued. Queued email identifier: {queuedEmailId}.");
                            }

                            //event
                            _eventPublisher.PublishShipmentDelivered(shipment);
                        }
                    }
                }

                var json = _jsonFieldsSerializer.Serialize(shipmentDeliveryDatesRootObjectDelta.Dto, string.Empty);

                return new RawJsonActionResult(json);
            }
            catch (Exception ex)
            {
                _logger.Error("API error (/api/shipment_delivery_dates)", ex);
                throw;
            }
        }

        #endregion
    }
}
