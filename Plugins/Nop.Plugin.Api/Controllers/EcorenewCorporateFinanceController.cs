﻿using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Tax;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.EcorenewCorporateFinance;
using Nop.Plugin.Api.DTOs.Errors;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Tax.FixedOrByCountryStateZip.Domain;
using Nop.Plugin.Tax.FixedOrByCountryStateZip.Services;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Tax;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Plugin.Api.Models.EcorenewCorporateFinance;
using Nop.Plugin.Api.MappingExtensions;
using System;

namespace Nop.Plugin.Api.Controllers
{
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class EcorenewCorporateFinanceController : BaseApiController
    {
        #region Fields

        private readonly ICorporateFinanceService _corporateFinanceService;

        #endregion

        #region Ctor

        public EcorenewCorporateFinanceController(ICorporateFinanceService corporateFinanceService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPictureService pictureService)
            : base(jsonFieldsSerializer,
                  aclService,
                  customerService,
                  storeMappingService,
                  storeService,
                  discountService,
                  customerActivityService,
                  localizationService,
                  pictureService)
        {
            this._corporateFinanceService = corporateFinanceService;
        }

        #endregion

        #region Methods

        [HttpGet]
        [Route("/api/ecorenew_corporate_finance_order_items")]
        [ProducesResponseType(typeof(CorporateFinanceOrderItemsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetAllCorporateFinanceOrderItems(string orderItemIds)
        {
            IList<CorporateFinanceOrderItemDto> allCorporateFinanceOrderItems = _corporateFinanceService.GetCorporateFinanceOrderItemsByOrderItemIds(orderItemIds.Split(',').Select(Int32.Parse))
                .Select(x => x.ToDto())
                .ToList();

            var corporateFinanceOrderItemsRootObject = new CorporateFinanceOrderItemsRootObjectDto()
            {
                CorporateFinanceOrderItems = allCorporateFinanceOrderItems
            };

            var json = _jsonFieldsSerializer.Serialize(corporateFinanceOrderItemsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        #endregion
    }
}