﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Directory;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Currencies;
using Nop.Plugin.Api.DTOs.Errors;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Plugin.Api.ModelBinders;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    // Added by EcoRenew Group
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CurrencyController : BaseApiController
    {
        #region Fields

        private readonly CurrencySettings _currencySettings;
        private readonly ICurrencyService _currencyService;

        #endregion

        #region Ctor

        public CurrencyController(CurrencySettings currencySettings,
            ICurrencyService currencyService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPictureService pictureService)
            : base(jsonFieldsSerializer,
                  aclService,
                  customerService,
                  storeMappingService,
                  storeService,
                  discountService,
                  customerActivityService,
                  localizationService,
                  pictureService)
        {
            this._currencySettings = currencySettings;
            this._currencyService = currencyService;
        }

        #endregion

        #region Methods

        [HttpGet]
        [Route("/api/primary_currency")]
        [ProducesResponseType(typeof(Currency), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetSettings()
        {
            var primaryCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);

            return Json(primaryCurrency);
        }

        [HttpGet]
        [Route("/api/currencies")]
        [ProducesResponseType(typeof(CurrenciesRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetAllCurrencies()
        {
            IList<CurrencyDto> allCurrencies = _currencyService.GetAllCurrencies(showHidden: true)
                .Select(o => new CurrencyDto
                {
                    CurrencyCode = o.CurrencyCode,
                    Id = o.Id,
                    Name = o.Name,
                    Published = o.Published,
                    Rate = o.Rate,
                    UpdatedOnUtc = o.UpdatedOnUtc
                })
                .ToList();

            var currenciesRootObject = new CurrenciesRootObjectDto()
            {
                Currencies = allCurrencies
            };

            var json = _jsonFieldsSerializer.Serialize(currenciesRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/currencies/{id}")]
        [ProducesResponseType(typeof(CurrencyDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCurrencyById(int id, string fields = "")
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var currency = _currencyService.GetCurrencyById(id);
            if (currency == null)
                return Error(HttpStatusCode.NotFound, "currency", "consumer finance option not found");

            var currencyDto = new CurrencyDto
            {
                CurrencyCode = currency.CurrencyCode,
                Id = currency.Id,
                Name = currency.Name,
                Published = currency.Published,
                Rate = currency.Rate,
                UpdatedOnUtc = currency.UpdatedOnUtc
            };

            var currenciesRootObject = new CurrenciesRootObjectDto();

            currenciesRootObject.Currencies.Add(currencyDto);

            var json = _jsonFieldsSerializer.Serialize(currenciesRootObject, fields);

            return new RawJsonActionResult(json);
        }

        [HttpPut]
        [Route("/api/currencies/{id}")]
        [ProducesResponseType(typeof(CurrenciesRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        public IActionResult UpdateCurrency(
            [ModelBinder(typeof(JsonModelBinder<CurrencyDto>))] Delta<CurrencyDto> currencyDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // We do not need to validate the currency id, because this will happen in the model binder using the dto validator.

            var currency = _currencyService.GetCurrencyById(currencyDelta.Dto.Id);
            if (currency == null)
                return Error(HttpStatusCode.NotFound, "currency", "consumer finance option not found");

            currencyDelta.Merge(currency);
            currency.UpdatedOnUtc = DateTime.UtcNow;

            _currencyService.UpdateCurrency(currency);

            var currencyDto = new CurrencyDto
            {
                CurrencyCode = currency.CurrencyCode,
                Id = currency.Id,
                Name = currency.Name,
                Published = currency.Published,
                Rate = currency.Rate,
                UpdatedOnUtc = currency.UpdatedOnUtc
            };

            var currenciesRootObject = new CurrenciesRootObjectDto();

            currenciesRootObject.Currencies.Add(currencyDto);

            var json = _jsonFieldsSerializer.Serialize(currenciesRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        #endregion
    }
}