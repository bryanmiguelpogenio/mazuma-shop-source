﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DTOs.BrainTreePayPal;
using Nop.Plugin.Api.DTOs.BrainTreePayPalCredit;
using Nop.Plugin.Api.DTOs.Errors;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Plugin.Api.Models.BrainTreePayPalParameters;
using Nop.Plugin.Api.Services;
using Nop.Plugin.Payments.BrainTreePayPal.Services;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;

namespace Nop.Plugin.Api.Controllers
{
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BrainTreePayPalController : BaseApiController
    {
        private readonly IBrainTreePayPalApiService _brainTreePayPalApiService;

        public BrainTreePayPalController(
            IBrainTreePayPalApiService brainTreePayPalApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPictureService pictureService) :
            base(jsonFieldsSerializer, aclService, customerService, storeMappingService, storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            this._brainTreePayPalApiService = brainTreePayPalApiService;
        }

        /// <summary>
        /// Receive a list of all PayPal Financing Details
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/braintree_paypal_financing_details")]
        [ProducesResponseType(typeof(OrderPayPalCreditFinancingDetailsDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetPayPalFinancingDetails(BrainTreePayPalFinancingDetailsParametersModel parameters)
        {
            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
            }

            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid limit parameter");
            }

            var financingDetailsDtos = _brainTreePayPalApiService.GetAllOrderPayPalCreditFinancingDetails(
                parameters.Ids, parameters.Limit, parameters.Page,
                parameters.SinceId, parameters.OrderId)
                .Select(x => new OrderPayPalCreditFinancingDetailsDto
                {
                    Id = x.Id,
                    OrderId = x.OrderId,
                    TotalCostValue = x.TotalCostValue,
                    TotalCostCurrency = x.TotalCostCurrency,
                    Term = x.Term,
                    MonthlyPaymentValue = x.MonthlyPaymentValue,
                    MonthlyPaymentCurrency = x.MonthlyPaymentCurrency,
                    TotalInterestValue = x.TotalInterestValue,
                    TotalInterestCurrency = x.TotalInterestCurrency,
                    PayerAcceptance = x.PayerAcceptance,
                    CartAmountImmutable = x.CartAmountImmutable
                })
                .ToList();

            var rootObject = new OrderPayPalCreditFinancingDetailsRootObjectDto()
            {
                FinancingDetails = financingDetailsDtos
            };

            var json = _jsonFieldsSerializer.Serialize(rootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// Receive a count of all PayPal Financing Details
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/braintree_paypal_financing_details/count")]
        [ProducesResponseType(typeof(OrderPayPalCreditFinancingDetailsCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetPayPalFinancingDetailsCount()
        {
            int payPalFinancingDetailsCount = _brainTreePayPalApiService.GetOrderPayPalCreditFinancingDetailsCount();

            var ordersCountRootObject = new OrderPayPalCreditFinancingDetailsCountRootObject()
            {
                Count = payPalFinancingDetailsCount
            };

            return Ok(ordersCountRootObject);
        }
    }
}
