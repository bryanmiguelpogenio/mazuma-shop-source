﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nop.Core.Configuration;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Errors;
using Nop.Plugin.Api.DTOs.Settings;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Services;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using System;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    /// <remarks>Added by EcoRenew</remarks>
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SettingsController : BaseApiController
    {
        private readonly ISettingsApiService _settingApiService;

        public SettingsController(ISettingsApiService settingApiService,
            IJsonFieldsSerializer jsonFieldsSerializer, 
            IAclService aclService, 
            ICustomerService customerService, 
            IStoreMappingService storeMappingService, 
            IStoreService storeService, 
            IDiscountService discountService, 
            ICustomerActivityService customerActivityService, 
            ILocalizationService localizationService, 
            IPictureService pictureService) 
            : base(jsonFieldsSerializer, 
                  aclService, 
                  customerService, 
                  storeMappingService, 
                  storeService, 
                  discountService, 
                  customerActivityService, 
                  localizationService, 
                  pictureService)
        {
            this._settingApiService = settingApiService;
        }

        /// <summary>
        /// Retrieve specific settings
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/settings")]
        [ProducesResponseType(typeof(ISettings), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetSettings(string settingsTypeName, int storeId = 0)
        {
            if (string.IsNullOrWhiteSpace(settingsTypeName))
            {
                return Error(HttpStatusCode.BadRequest, "settingsType", "invalid settings type");
            }

            var type = Type.GetType(settingsTypeName);

            if (type == null || type.IsAssignableFrom(typeof(ISettings)))
            {
                return Error(HttpStatusCode.BadRequest, "settingsType", "invalid settings type");
            }

            var settings = _settingApiService.LoadSetting(type, storeId);

            return Json(settings);
        }

        /// <summary>
        /// Update settings
        /// </summary>
        [HttpPut]
        [Route("/api/settings/update")]
        [ProducesResponseType(typeof(ISettings), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult UpdateSettings([ModelBinder(typeof(JsonModelBinder<SettingsDto>))] Delta<SettingsDto> settingsDelta)
        {
            if (string.IsNullOrWhiteSpace(settingsDelta.Dto.SettingsTypeName))
            {
                return Error(HttpStatusCode.BadRequest, "settingsType", "invalid settings type");
            }

            var type = Type.GetType(settingsDelta.Dto.SettingsTypeName);

            if (type == null || type.IsAssignableFrom(typeof(ISettings)))
            {
                return Error(HttpStatusCode.BadRequest, "settingsType", "invalid settings type");
            }

            var settings = JsonConvert.DeserializeObject(settingsDelta.Dto.SettingsJson, type);
            if (settings == null)
            {
                return Error(HttpStatusCode.BadRequest, "settingsJson", "invalid settings json");
            }

            _settingApiService.SaveSetting(settings, type, settingsDelta.Dto.StoreId);

            return Json(_settingApiService.LoadSetting(type, settingsDelta.Dto.StoreId));
        }
    }
}