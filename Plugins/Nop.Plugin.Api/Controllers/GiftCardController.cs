﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Errors;
using Nop.Plugin.Api.DTOs.GiftCards;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.GiftCardParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using System;
using System.Linq;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GiftCardController : BaseApiController
    {
        private readonly IGiftCardApiService _giftCardApiService;
        private readonly IDTOHelper _dtoHelper;

        public GiftCardController(IAclService aclService,
            ICustomerActivityService customerActivityService,
            ICustomerService customerService,
            IDiscountService discountService,
            IGiftCardApiService giftCardApiService,
            IDTOHelper dtoHelper,
            IJsonFieldsSerializer jsonFieldsSerializer,
            ILocalizationService localizationService,
            IPictureService pictureService,
            IStoreMappingService storeMappingService,
            IStoreService storeService) : base(jsonFieldsSerializer, aclService, customerService, storeMappingService, storeService, discountService, customerActivityService, localizationService, pictureService)
        {
            this._giftCardApiService = giftCardApiService;
            this._dtoHelper = dtoHelper;
        }

        /// <summary>
        /// Receive a list of all Gift Cards
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/gift_cards")]
        [ProducesResponseType(typeof(GiftCardsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetGiftCards(GiftCardParametersModel parameters)
        {
            if (parameters.Page < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
            }

            if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid limit parameter");
            }

            var giftCardsDto = _giftCardApiService.GetGiftCards(limit: parameters.Limit, page: parameters.Page,
                    ids: parameters.Ids, sinceId: parameters.SinceId)
                .ToList()
                .Select(x => _dtoHelper.PrepareGiftCardDto(x))
                .ToList();

            var giftCardRootObject = new GiftCardsRootObjectDto
            {
                GiftCards = giftCardsDto
            };

            var json = _jsonFieldsSerializer.Serialize(giftCardRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/gift_cards/count")]
        [ProducesResponseType(typeof(GiftCardsCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetGiftCardsCount()
        {
            int giftCardsCount = _giftCardApiService.GetGiftCardsCount();

            var ordersCountRootObject = new GiftCardsCountRootObject()
            {
                Count = giftCardsCount
            };

            return Ok(ordersCountRootObject);
        }

        [HttpGet]
        [Route("/api/gift_cards/{id}")]
        [ProducesResponseType(typeof(GiftCardsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetGiftCardById(int id, string fields = "")
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var giftCard = _giftCardApiService.GetGiftCardById(id);

            if (giftCard == null)
            {
                return Error(HttpStatusCode.NotFound, "gift_card", "not found");
            }

            var giftCardDto = _dtoHelper.PrepareGiftCardDto(giftCard);

            var giftCardRootObject = new GiftCardsRootObjectDto();

            giftCardRootObject.GiftCards.Add(giftCardDto);

            var json = _jsonFieldsSerializer.Serialize(giftCardRootObject, fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/gift_cards")]
        [ProducesResponseType(typeof(GiftCardsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateGiftCard([ModelBinder(typeof(JsonModelBinder<GiftCardDto>))] Delta<GiftCardDto> giftCardDelta)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

            if (_giftCardApiService.CouponCodeExists(giftCardDelta.Dto.GiftCardCouponCode))
            {
                return Error(HttpStatusCode.BadRequest, "gift_card_coupon_code", "Coupon code already exists.");
            }

            var giftCard = new GiftCard();
            giftCardDelta.Merge(giftCard);

            giftCard.CreatedOnUtc = DateTime.UtcNow;
            giftCard.HasDurationPeriod = giftCard.HasDurationPeriod == null ? false : giftCard.HasDurationPeriod.Value;

            if (!giftCard.HasDurationPeriod.Value)
            {
                giftCard.StartDateUtc = null;
                giftCard.EndDateUtc = null;
            }
            
            _giftCardApiService.InsertGiftCard(giftCard);

            _customerActivityService.InsertActivity("AddNewGiftCard",
                 _localizationService.GetResource("ActivityLog.AddNewGiftCard"), giftCard.GiftCardCouponCode);

            var giftCardRootObject = new GiftCardsRootObject();
            var giftCardDto = _dtoHelper.PrepareGiftCardDto(giftCard);

            giftCardRootObject.GiftCards.Add(giftCardDto);

            var json = _jsonFieldsSerializer.Serialize(giftCardRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPut]
        [Route("/api/gift_cards/{id}")]
        [ProducesResponseType(typeof(GiftCardsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult UpdateGiftCard([ModelBinder(typeof(JsonModelBinder<GiftCardDto>))] Delta<GiftCardDto> giftCardDelta)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var giftCard = _giftCardApiService.GetGiftCardById(giftCardDelta.Dto.Id);

            if (giftCard == null)
            {
                return Error(HttpStatusCode.NotFound, "gift_card", "not found");
            }

            if (giftCardDelta.Dto.GiftCardCouponCode != null
                && _giftCardApiService.CouponCodeExists(giftCard.Id, giftCardDelta.Dto.GiftCardCouponCode))
            {
                return Error(HttpStatusCode.BadRequest, "gift_card_coupon_code", "Coupon code already exists.");
            }

            giftCardDelta.Merge(giftCard);

            if (!giftCard.HasDurationPeriod.Value)
            {
                giftCard.StartDateUtc = null;
                giftCard.EndDateUtc = null;
            }

            _giftCardApiService.UpdateGiftCard(giftCard);

            _customerActivityService.InsertActivity("EditGiftCard",
                 _localizationService.GetResource("ActivityLog.EditGiftCard"), giftCard.GiftCardCouponCode);

            var giftCardRootObject = new GiftCardsRootObject();
            var giftCardDto = _dtoHelper.PrepareGiftCardDto(giftCard);

            giftCardRootObject.GiftCards.Add(giftCardDto);

            var json = _jsonFieldsSerializer.Serialize(giftCardRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        //[HttpDelete]
        //[Route("/api/gift_cards/{id}")]
        //[ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        //[ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        //[ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        //[GetRequestsErrorInterceptorActionFilter]
        //public IActionResult DeleteGiftCard(int id)
        //{
        //    if (id <= 0)
        //    {
        //        return Error(HttpStatusCode.BadRequest, "id", "invalid id");
        //    }

        //    var giftCard = _giftCardService.GetGiftCardById(id);

        //    if (giftCard == null)
        //    {
        //        return Error(HttpStatusCode.NotFound, "gift_card", "not found");
        //    }

        //    _giftCardService.DeleteGiftCard(giftCard);

        //    //activity log
        //    _customerActivityService.InsertActivity("DeleteGiftCard", _localizationService.GetResource("ActivityLog.DeleteGiftCard"), giftCard.GiftCardCouponCode);

        //    return new RawJsonActionResult("{}");
        //}

    }
}
