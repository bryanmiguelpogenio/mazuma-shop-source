﻿using Nop.Plugin.Api.AutoMapper;
using Nop.Plugin.Api.DTOs.MazumaWiFi;
using Nop.Plugin.Mazuma.WiFiVoucher.Domain;

namespace Nop.Plugin.Api.MappingExtensions
{
    public static class MazumaWiFiVoucherDtoMappings
    {
        public static MazumaWiFiVoucherDto ToDto(this MazumaWiFiVoucher mazumaWiFiVoucher)
        {
            return mazumaWiFiVoucher.MapTo<MazumaWiFiVoucher, MazumaWiFiVoucherDto>();
        }
    }
}
