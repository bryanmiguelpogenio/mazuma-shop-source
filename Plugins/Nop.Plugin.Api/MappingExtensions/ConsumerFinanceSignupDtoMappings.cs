﻿using Nop.Plugin.Api.AutoMapper;
using Nop.Plugin.Api.DTOs.EcorenewConsumerFinance;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;

namespace Nop.Plugin.Api.MappingExtensions
{
    public static class ConsumerFinanceSignupDtoMappings
    {
        public static ConsumerFinanceSignupDto ToDto(this ConsumerFinanceSignup consumerFinanceSignup)
        {
            return consumerFinanceSignup.MapTo<ConsumerFinanceSignup, ConsumerFinanceSignupDto>();
        }
    }
}