﻿using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.AutoMapper;
using Nop.Plugin.Api.DTOs.GiftCards;

namespace Nop.Plugin.Api.MappingExtensions
{
    public static class GiftCardDtoMappings
    {
        public static GiftCardResponseDto ToDto(this GiftCard giftCard)
        {
            return giftCard.MapTo<GiftCard, GiftCardResponseDto>();
        }
    }
}
