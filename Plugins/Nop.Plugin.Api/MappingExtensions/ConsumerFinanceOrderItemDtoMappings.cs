﻿using Nop.Plugin.Api.AutoMapper;
using Nop.Plugin.Api.DTOs.EcorenewConsumerFinance;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;

namespace Nop.Plugin.Api.MappingExtensions
{
    public static class ConsumerFinanceOrderItemDtoMappings
    {
        public static ConsumerFinanceOrderItemDto ToDto(this ConsumerFinanceOrderItem consumerFinanceOrderItem)
        {
            return consumerFinanceOrderItem.MapTo<ConsumerFinanceOrderItem, ConsumerFinanceOrderItemDto>();
        }
    }
}