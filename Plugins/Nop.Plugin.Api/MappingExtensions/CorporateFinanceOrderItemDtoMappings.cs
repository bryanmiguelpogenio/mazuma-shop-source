﻿using Nop.Plugin.Api.AutoMapper;
using Nop.Plugin.Api.DTOs.EcorenewCorporateFinance;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;

namespace Nop.Plugin.Api.MappingExtensions
{
    public static class CorporateFinanceOrderItemDtoMappings
    {
        public static CorporateFinanceOrderItemDto ToDto(this CorporateFinanceOrderItem corporateFinanceOrderItem)
        {
            return corporateFinanceOrderItem.MapTo<CorporateFinanceOrderItem, CorporateFinanceOrderItemDto>();
        }
    }
}