﻿using Nop.Core;
using System;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Domain
{
    public class DiscountCouponGeneratorMessageRecord : BaseEntity
    {
        public int StoreId { get; set; }

        public int DiscountCouponGeneratorMessageTypeId { get; set; }

        public string EmailAddress { get; set; }

        public int QueuedEmailId { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public virtual DiscountCouponGeneratorMessageType DiscountCouponGeneratorMessageType
        {
            get { return (DiscountCouponGeneratorMessageType)DiscountCouponGeneratorMessageTypeId; }
            set { DiscountCouponGeneratorMessageTypeId = (int)value; }
        }
    }
}
