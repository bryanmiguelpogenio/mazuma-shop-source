﻿namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Domain
{
    public enum DiscountCouponGeneratorMessageType
    {
        AbandonedCart = 1,
        Monthly = 2,
        Birthday = 3
    }
}