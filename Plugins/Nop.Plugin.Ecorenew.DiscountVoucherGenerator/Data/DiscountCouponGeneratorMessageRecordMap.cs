﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Domain;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Data
{
    public class DiscountCouponGeneratorMessageRecordMap : NopEntityTypeConfiguration<DiscountCouponGeneratorMessageRecord>
    {
        public DiscountCouponGeneratorMessageRecordMap()
        {
            this.ToTable("DiscountCouponGenerator_MessageRecord");
            this.HasKey(x => x.Id);

            this.Property(x => x.EmailAddress).HasMaxLength(255).IsRequired();

            this.Ignore(x => x.DiscountCouponGeneratorMessageType);
        }
    }
}
