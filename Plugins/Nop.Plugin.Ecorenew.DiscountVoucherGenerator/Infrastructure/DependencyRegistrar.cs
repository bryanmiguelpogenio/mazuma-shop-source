﻿using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Data;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Domain;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Services;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Helpers;
using Nop.Services.Messages;
using Nop.Web.Framework.Infrastructure;
using Ecorenew.MailChimpApi;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Infrastructure
{
    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        #region Fields

        private const string DB_CONTEXT_NAME = "nop_object_context_discount_coupon_generator";

        #endregion

        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            // new services
            builder.RegisterType<CustomMessageTokenProvider>()
                .As<ICustomMessageTokenProvider>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CustomWorkflowMessageService>()
                .As<ICustomWorkflowMessageService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CouponCodeGenerator>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CustomDiscountService>()
                .As<ICustomDiscountService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CustomProductAttributeFormatter>()
                .As<ICustomProductAttributeFormatter>()
                .InstancePerLifetimeScope();

            builder.RegisterType<MailChimpApiClient>()
                .InstancePerLifetimeScope();

            // overridden services
            builder.RegisterType<CustomWorkflowMessageService>()
                .As<IWorkflowMessageService>()
                .InstancePerLifetimeScope();

            //data context
            this.RegisterPluginDataContext<DiscountCouponGeneratorObjectContext>(builder, DB_CONTEXT_NAME);

            //override required repository with our custom context
            builder.RegisterType<EfRepository<DiscountCouponGeneratorMessageRecord>>()
                .As<IRepository<DiscountCouponGeneratorMessageRecord>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(DB_CONTEXT_NAME))
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 9; }
        }
    }
}
