﻿namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator
{
    public enum ConditionMetUnitOfMeasurement
    {
        Minutes = 0,
        Hours = 1,
        Days = 2
    }
}
