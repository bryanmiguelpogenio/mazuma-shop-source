﻿using Nop.Core.Domain.Discounts;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Services
{
    public interface ICustomDiscountService
    {
        Discount CreateRandomCouponOrderTotalDiscount(string discountName,
            bool usePercentage,
            decimal discountAmount,
            int randomCouponCodeLength = 6,
            string couponCodeFormat = null,
            int daysToExpire = 0,
            bool isCumulative = false,
            DiscountLimitationType discountLimitationType = DiscountLimitationType.NTimesOnly,
            int limitationTimes = 1,
            IEnumerable<int> limitToCustomerIds = null,
            decimal? spentAmountShouldBeOver = null);

        Discount CreateRandomCouponOrderTotalDiscount(string discountName,
            bool usePercentage,
            decimal discountAmount,
            int randomCouponCodeLength = 6,
            string couponCodeFormat = null,
            DateTime? startDateUtc = null,
            DateTime? endDateUtc = null,
            bool isCumulative = false,
            DiscountLimitationType discountLimitationType = DiscountLimitationType.NTimesOnly,
            int limitationTimes = 1,
            IEnumerable<int> limitToCustomerIds = null,
            decimal? spentAmountShouldBeOver = null);
    }
}