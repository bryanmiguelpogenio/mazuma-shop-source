﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Discounts;
using Nop.Services.Messages;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Services
{
    public interface ICustomWorkflowMessageService
    {
        int SendCustomerBirthdayMessage(string customerEmailAddress, string customerName, int languageId, Discount discountOver200, Discount discountOver1000);
        int SendAbandonedCartMessage(int storeId, Customer customer, int languageId);
        int SendCustomerMonthlyCouponMessage(string customerEmailAddress, string customerName, int languageId, Discount discountOver100, Discount discountOver200, Discount discountOver500);
    }
}
