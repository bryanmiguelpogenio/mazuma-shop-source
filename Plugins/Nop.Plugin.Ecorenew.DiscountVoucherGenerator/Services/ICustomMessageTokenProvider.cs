﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Discounts;
using Nop.Services.Messages;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Services
{
    public interface ICustomMessageTokenProvider
    {
        void AddDiscountCouponTokens(IList<Token> tokens, Discount discount, string tokenName = null);
        void AddShoppingCartItemsTokens(IList<Token> tokens, Customer customer, int storeId);
    }
}
