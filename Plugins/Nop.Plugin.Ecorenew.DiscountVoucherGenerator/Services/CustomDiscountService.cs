﻿using Nop.Core.Data;
using Nop.Core.Domain.Discounts;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Helpers;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Services
{
    public class CustomDiscountService : ICustomDiscountService
    {
        #region Fields

        private const string LIMIT_TO_CUSTOMER_DISCOUNT_RULE = "DiscountRequirement.MustBeAssignedToCustomer";
        private const string LIMIT_TO_CUSTOMER_DISCOUNT_RULE_SETTINGS_KEY = "DiscountRequirement.MustBeAssignedToCustomer-{0}";
        private const string SPENT_AMOUNT_SHOULD_BE_OVER_DISCOUNT_RULE = "DiscountRequirement.SpendAmountOver";
        private const string SPENT_AMOUNT_SHOULD_BE_OVER_DISCOUNT_RULE_SETTINGS_KEY = "DiscountRequirement.SpendAmountOver-{0}";

        private readonly IRepository<Discount> _discountRepository;
        private readonly CouponCodeGenerator _couponCodeGenerator;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor

        public CustomDiscountService(IRepository<Discount> discountRepository,
            CouponCodeGenerator couponCodeGenerator,
            ILocalizationService localizationService,
            ISettingService settingService)
        {
            this._discountRepository = discountRepository;
            this._couponCodeGenerator = couponCodeGenerator;
            this._localizationService = localizationService;
            this._settingService = settingService;
        }

        #endregion

        #region Utilities

        protected virtual bool IsDiscountCouponCodeValid(string couponCode)
        {
            return !_discountRepository.TableNoTracking.Any(x => x.CouponCode == couponCode);
        }

        #endregion

        #region Methods

        public virtual Discount CreateRandomCouponOrderTotalDiscount(string discountName,
            bool usePercentage,
            decimal discountAmount,
            int randomCouponCodeLength = 6,
            string couponCodeFormat = null,
            int daysToExpire = 0,
            bool isCumulative = false,
            DiscountLimitationType discountLimitationType = DiscountLimitationType.NTimesOnly,
            int limitationTimes = 1,
            IEnumerable<int> limitToCustomerIds = null,
            decimal? spentAmountShouldBeOver = null)
        {
            DateTime? startDateUtc = null;
            DateTime? endDateUtc = null;

            if (daysToExpire > 0)
            {
                startDateUtc = DateTime.UtcNow;
                endDateUtc = DateTime.UtcNow.AddDays(daysToExpire);
            }

            return CreateRandomCouponOrderTotalDiscount(discountName,
                usePercentage,
                discountAmount,
                randomCouponCodeLength,
                couponCodeFormat,
                startDateUtc,
                endDateUtc,
                isCumulative,
                discountLimitationType,
                limitationTimes,
                limitToCustomerIds,
                spentAmountShouldBeOver);
        }

        public virtual Discount CreateRandomCouponOrderTotalDiscount(string discountName,
            bool usePercentage,
            decimal discountAmount,
            int randomCouponCodeLength = 6,
            string couponCodeFormat = null,
            DateTime? startDateUtc = null,
            DateTime? endDateUtc = null,
            bool isCumulative = false,
            DiscountLimitationType discountLimitationType = DiscountLimitationType.NTimesOnly,
            int limitationTimes = 1,
            IEnumerable<int> limitToCustomerIds = null,
            decimal? spentAmountShouldBeOver = null)
        {
            var couponCode = "";
            bool discountCouponCodeValid = false;
            int generateCouponTries = 50;
            int currentGenerateCouponTryCount = 0;

            do
            {
                couponCode = _couponCodeGenerator.GenerateRandomCouponCode(randomCouponCodeLength, couponCodeFormat);
                discountCouponCodeValid = IsDiscountCouponCodeValid(couponCode);
            } while (!discountCouponCodeValid && currentGenerateCouponTryCount < generateCouponTries);

            if (!discountCouponCodeValid)
                throw new Exception($"Failed to generate random coupon code after {generateCouponTries} tries");

            // create and insert discount
            var discount = new Discount
            {
                CouponCode = couponCode,
                DiscountAmount = discountAmount,
                DiscountLimitation = DiscountLimitationType.NTimesOnly,
                DiscountPercentage = usePercentage ? discountAmount : 0,
                DiscountType = DiscountType.AssignedToOrderTotal,
                IsCumulative = isCumulative,
                LimitationTimes = 1,
                Name = discountName,
                RequiresCouponCode = true,
                ShowGeneralRibbon = false,
                UsePercentage = usePercentage
            };

            if (startDateUtc.HasValue)
                discount.StartDateUtc = startDateUtc.Value;
            if (endDateUtc.HasValue)
                discount.EndDateUtc = endDateUtc.Value;

            _discountRepository.Insert(discount);

            if (limitToCustomerIds != null && limitToCustomerIds.Any())
            {
                foreach (var customerId in limitToCustomerIds)
                {
                    //add default requirement group
                    var defaultRequirementGroup = new DiscountRequirement
                    {
                        IsGroup = true,
                        InteractionType = RequirementGroupInteractionType.And,
                        DiscountRequirementRuleSystemName = _localizationService.GetResource("Plugins.Ecorenew.DiscountDiscountCouponGenerator.Requirements.LimitToCustomerIds")
                    };

                    var discountRequirement = new DiscountRequirement
                    {
                        DiscountRequirementRuleSystemName = LIMIT_TO_CUSTOMER_DISCOUNT_RULE,
                        InteractionType = RequirementGroupInteractionType.Or
                    };

                    discount.DiscountRequirements.Add(defaultRequirementGroup);
                    discount.DiscountRequirements.Add(discountRequirement);
                    _discountRepository.Update(discount);

                    discountRequirement.ParentId = defaultRequirementGroup.Id;
                    _discountRepository.Update(discount);

                    //save restricted customer identifier
                    _settingService.SetSetting(string.Format(LIMIT_TO_CUSTOMER_DISCOUNT_RULE_SETTINGS_KEY, discountRequirement.Id), customerId);
                }
            }

            if (spentAmountShouldBeOver.HasValue)
            {
                //add default requirement group
                var defaultRequirementGroup = new DiscountRequirement
                {
                    IsGroup = true,
                    InteractionType = RequirementGroupInteractionType.And,
                    DiscountRequirementRuleSystemName = _localizationService.GetResource("Plugins.Ecorenew.DiscountDiscountCouponGenerator.Requirements.SpentAmountShouldBeOver")
                };

                var discountRequirement = new DiscountRequirement
                {
                    DiscountRequirementRuleSystemName = SPENT_AMOUNT_SHOULD_BE_OVER_DISCOUNT_RULE,
                    InteractionType = RequirementGroupInteractionType.Or
                };

                discount.DiscountRequirements.Add(defaultRequirementGroup);
                discount.DiscountRequirements.Add(discountRequirement);
                _discountRepository.Update(discount);

                discountRequirement.ParentId = defaultRequirementGroup.Id;
                _discountRepository.Update(discount);

                //save restricted customer identifier
                _settingService.SetSetting(string.Format(SPENT_AMOUNT_SHOULD_BE_OVER_DISCOUNT_RULE_SETTINGS_KEY, discountRequirement.Id), spentAmountShouldBeOver);
            }

            return discount;
        }

        #endregion
    }
}
