﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Configuration;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Messages;
using Nop.Core.Plugins;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Domain;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Helpers;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Services
{
    public class CustomWorkflowMessageService : WorkflowMessageService, ICustomWorkflowMessageService
    {
        #region Fields

        private readonly CouponCodeGenerator _couponCodeGenerator;
        private readonly IRepository<DiscountCouponGeneratorMessageRecord> _discountCouponGeneratorMessageRecordRepository;
        private readonly IRepository<DiscountRequirement> _discountRequirementRepository;
        private readonly IRepository<Setting> _settingRepository;
        private readonly ICustomDiscountService _customDiscountService;
        private readonly ICustomMessageTokenProvider _customMessageTokenProvider;
        private readonly IDiscountService _discountService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILocalizationService _localizationService;
        private readonly ILogger _logger;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly IPluginFinder _pluginFinder;
        private readonly IStoreService _storeService;
        private readonly IStoreContext _storeContext;

        private readonly DiscountCouponGeneratorSettings _discountCouponGeneratorSettings;

        #endregion

        #region Ctor

        public CustomWorkflowMessageService(CouponCodeGenerator couponCodeGenerator,
            IRepository<DiscountCouponGeneratorMessageRecord> discountCouponGeneratorMessageRecordRepository,
            IRepository<DiscountRequirement> discountRequirementRepository,
            IRepository<Setting> settingRepository,
            ICustomDiscountService customDiscountService,
            ICustomMessageTokenProvider customMessageTokenProvider,
            IDiscountService discountService,
            IEmailAccountService emailAccountService,
            IEventPublisher eventPublisher,
            ILanguageService languageService, 
            ILocalizationService localizationService,
            ILogger logger,
            IMessageTemplateService messageTemplateService,
            IMessageTokenProvider messageTokenProvider,
            IPluginFinder pluginFinder,
            IQueuedEmailService queuedEmailService,
            IStoreService storeService,
            IStoreContext storeContext,
            ITokenizer tokenizer,
            CommonSettings commonSettings,
            EmailAccountSettings emailAccountSettings,
            DiscountCouponGeneratorSettings discountCouponGeneratorSettings)
            : base(messageTemplateService,
                  queuedEmailService,
                  languageService,
                  tokenizer,
                  emailAccountService,
                  messageTokenProvider,
                  storeService,
                  storeContext,
                  commonSettings,
                  emailAccountSettings,
                  eventPublisher)
        {
            this._couponCodeGenerator = couponCodeGenerator;
            this._discountCouponGeneratorMessageRecordRepository = discountCouponGeneratorMessageRecordRepository;
            this._discountRequirementRepository = discountRequirementRepository;
            this._settingRepository = settingRepository;
            this._customDiscountService = customDiscountService;
            this._customMessageTokenProvider = customMessageTokenProvider;
            this._discountService = discountService;
            this._eventPublisher = eventPublisher;
            this._localizationService = localizationService;
            this._logger = logger;
            this._messageTemplateService = messageTemplateService;
            this._messageTokenProvider = messageTokenProvider;
            this._pluginFinder = pluginFinder;
            this._storeService = storeService;
            this._storeContext = storeContext;

            this._discountCouponGeneratorSettings = discountCouponGeneratorSettings;
        }

        #endregion

        #region Utilities

        protected virtual bool IsNewDiscountCouponCodeValid(string couponCode)
        {
            // discount coupon code should not be existing
            return !_discountService.GetAllDiscounts(couponCode: couponCode).Any();
        }

        #endregion

        #region Methods

        public override int SendCustomerWelcomeMessage(Customer customer, int languageId)
        {
            // check if the plugin is installed and enabled for the current store
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(DiscountCouponGeneratorConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
                return base.SendCustomerWelcomeMessage(customer, languageId);

            // check if the setting for customer registered event discount is enabled
            if (!_discountCouponGeneratorSettings.CustomerRegisteredEventDiscountEnabled)
                return base.SendCustomerWelcomeMessage(customer, languageId);

            // generate random discount code and check if existing
            string discountCode = "";
            int discountCodeGenerateTries = 0;
            bool discountCodeIsValid = false;

            do
            {
                discountCode = _couponCodeGenerator.GenerateRandomCouponCode(
                    couponCodeLength: _discountCouponGeneratorSettings.CustomerRegisteredEventRandomCodeLength,
                    format: _discountCouponGeneratorSettings.CustomerRegisteredEventFormat);

                discountCodeIsValid = IsNewDiscountCouponCodeValid(discountCode);

            } while (!discountCodeIsValid && discountCodeGenerateTries < 50);

            if (!discountCodeIsValid)
            {
                discountCode = "";
                _logger.Error("Unable to generate a valid discount coupon code.");
            }

            // create a new discount
            var discount = new Discount
            {
                CouponCode = discountCode,
                DiscountAmount = _discountCouponGeneratorSettings.CustomerRegisteredEventDiscountUsePercentage ? 0 : _discountCouponGeneratorSettings.CustomerRegisteredEventDiscountValue,
                DiscountPercentage = _discountCouponGeneratorSettings.CustomerRegisteredEventDiscountUsePercentage ? _discountCouponGeneratorSettings.CustomerRegisteredEventDiscountValue : 0,
                DiscountLimitation = DiscountLimitationType.NTimesOnly,
                DiscountType = DiscountType.AssignedToOrderTotal,
                IsCumulative = false,
                LimitationTimes = 1,
                Name = DiscountCouponGeneratorConstants.CustomerRegisteredEventCouponCodeName,
                ShowGeneralRibbon = false,
                RequiresCouponCode = true,
                UsePercentage = _discountCouponGeneratorSettings.CustomerRegisteredEventDiscountUsePercentage
            };

            if (_discountCouponGeneratorSettings.CustomerRegisteredEventDiscountDaysToExpire > 0)
            {
                discount.StartDateUtc = DateTime.UtcNow;
                discount.EndDateUtc = discount.StartDateUtc.Value.AddDays(_discountCouponGeneratorSettings.CustomerRegisteredEventDiscountDaysToExpire);
            }

            _discountService.InsertDiscount(discount);

            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            var store = _storeContext.CurrentStore;
            languageId = EnsureLanguageIsActive(languageId, store.Id);

            var messageTemplate = GetActiveMessageTemplate(MessageTemplateSystemNames.CustomerWelcomeMessage, store.Id);
            if (messageTemplate == null)
                return 0;

            //email account
            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

            //tokens
            var tokens = new List<Token>();
            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            _messageTokenProvider.AddCustomerTokens(tokens, customer);
            _customMessageTokenProvider.AddDiscountCouponTokens(tokens, discount);

            //event notification
            _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

            var toEmail = customer.Email;
            var toName = customer.GetFullName();

            return SendNotification(messageTemplate, emailAccount, languageId, tokens, toEmail, toName);
        }

        public virtual int SendCustomerMonthlyCouponMessage(string customerEmailAddress, string customerName, int languageId, Discount discountOver100, Discount discountOver200, Discount discountOver500)
        {

            // check if the plugin is installed and enabled for the current store
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(DiscountCouponGeneratorConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
            {
                _logger.Error("Plugin Discount Coupon Generator is not yet installed.");
                return 0;
            }

            // check if the setting for customer registered event discount is enabled
            if (!_discountCouponGeneratorSettings.MonthlyCouponEventDiscountEnabled)
                return 0;

            if (string.IsNullOrWhiteSpace(customerEmailAddress))
                throw new ArgumentNullException(nameof(customerEmailAddress));
            if (string.IsNullOrWhiteSpace(customerName))
                throw new ArgumentNullException(nameof(customerName));

            // store tokens inside a list
            var tokens = new List<Token>();

            _customMessageTokenProvider.AddDiscountCouponTokens(tokens, discountOver100, DiscountCouponGeneratorConstants.Over100MonthlyCouponEventDiscountTokenName);
            _customMessageTokenProvider.AddDiscountCouponTokens(tokens, discountOver200, DiscountCouponGeneratorConstants.Over200MonthlyCouponEventDiscountTokenName);
            _customMessageTokenProvider.AddDiscountCouponTokens(tokens, discountOver500, DiscountCouponGeneratorConstants.Over500MonthlyCouponEventDiscountTokenName);

            var store = _storeContext.CurrentStore;
            languageId = EnsureLanguageIsActive(languageId, store.Id);

            var messageTemplate = GetActiveMessageTemplate(DiscountCouponMessageTemplateSystemNames.MonthlyCouponMessage, store.Id);
            if (messageTemplate == null)
            {
                _logger.Error($"Message Template {DiscountCouponMessageTemplateSystemNames.MonthlyCouponMessage} was not found.");
                return 0;
            }

            //email account
            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

            //tokens
            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            tokens.Add(new Token("Customer.EmailAddress", customerEmailAddress));
            tokens.Add(new Token("Customer.Name", customerName));

            //event notification
            _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

            int queuedEmailId = SendNotification(messageTemplate, emailAccount, languageId, tokens, customerEmailAddress, customerName);

            // add discount coupon message record
            var discountCouponGeneratorMessageRecord = new DiscountCouponGeneratorMessageRecord
            {
                EmailAddress = customerEmailAddress,
                DiscountCouponGeneratorMessageType = DiscountCouponGeneratorMessageType.Monthly,
                QueuedEmailId = queuedEmailId,
                CreatedOnUtc = DateTime.UtcNow
            };

            _discountCouponGeneratorMessageRecordRepository.Insert(discountCouponGeneratorMessageRecord);

            return queuedEmailId;
        }

        public virtual int SendCustomerBirthdayMessage(string customerEmailAddress, string customerName, int languageId, Discount discountOver200, Discount discountOver1000)
        {
            // check if the plugin is installed and enabled for the current store
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(DiscountCouponGeneratorConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
            {
                _logger.Error("Plugin Discount Coupon Generator is not yet installed.");
                return 0;
            }

            // check if the setting for customer registered event discount is enabled
            if (!_discountCouponGeneratorSettings.CustomerBirthdayEventDiscountEnabled)
                return 0;
            
            if (string.IsNullOrWhiteSpace(customerEmailAddress))
                throw new ArgumentNullException(nameof(customerEmailAddress));
            if (string.IsNullOrWhiteSpace(customerName))
                throw new ArgumentNullException(nameof(customerName));

            var store = _storeContext.CurrentStore;
            languageId = EnsureLanguageIsActive(languageId, store.Id);

            var messageTemplate = GetActiveMessageTemplate(DiscountCouponMessageTemplateSystemNames.CustomerBirthdayMessage, store.Id);

            // store tokens inside a list
            var tokens = new List<Token>();

            _customMessageTokenProvider.AddDiscountCouponTokens(tokens, discountOver200, DiscountCouponGeneratorConstants.Over200CustomerBirthdayEventDiscountTokenName);
            _customMessageTokenProvider.AddDiscountCouponTokens(tokens, discountOver1000, DiscountCouponGeneratorConstants.Over1000CustomerBirthdayEventDiscountTokenName);

            messageTemplate = GetActiveMessageTemplate(DiscountCouponMessageTemplateSystemNames.CustomerBirthdayMessage, store.Id);
            if (messageTemplate == null)
                return 0;

            // email account
            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

            // tokens
            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            tokens.Add(new Token("Customer.EmailAddress", customerEmailAddress));
            tokens.Add(new Token("Customer.Name", customerName));

            // event notification
            _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

            int queuedEmailId = SendNotification(messageTemplate, emailAccount, languageId, tokens, customerEmailAddress, customerName);

            // add discount coupon message record
            var discountCouponGeneratorMessageRecord = new DiscountCouponGeneratorMessageRecord
            {
                EmailAddress = customerEmailAddress,
                DiscountCouponGeneratorMessageType = DiscountCouponGeneratorMessageType.Birthday,
                QueuedEmailId = queuedEmailId,
                CreatedOnUtc = DateTime.UtcNow
            };

            _discountCouponGeneratorMessageRecordRepository.Insert(discountCouponGeneratorMessageRecord);

            return queuedEmailId;
        }

        public virtual int SendAbandonedCartMessage(int storeId, Customer customer, int languageId)
        {
            // check if the plugin is installed and enabled for the current store
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(DiscountCouponGeneratorConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
            {
                _logger.Error("Plugin Discount Coupon Generator is not yet installed.");
                return 0;
            }

            // check if the setting for customer registered event discount is enabled
            if (!_discountCouponGeneratorSettings.AbandonedCartEventDiscountEnabled)
                return 0;

            var store = _storeService.GetStoreById(storeId);
            if (store == null)
                throw new Exception("Send abandoned cart message failed. Store with ID " + storeId + " not found");

            var messageTemplate = GetActiveMessageTemplate(DiscountCouponMessageTemplateSystemNames.AbandonedCartMessage, store.Id);

            var discount = _customDiscountService.CreateRandomCouponOrderTotalDiscount(
                _discountCouponGeneratorSettings.AbandonedCartEventDiscountName,
                _discountCouponGeneratorSettings.AbandonedCartEventDiscountUsePercentage,
                _discountCouponGeneratorSettings.AbandonedCartEventDiscountValue,
                _discountCouponGeneratorSettings.AbandonedCartEventRandomCodeLength,
                _discountCouponGeneratorSettings.AbandonedCartEventFormat,
                _discountCouponGeneratorSettings.AbandonedCartEventDiscountDaysToExpire,
                _discountCouponGeneratorSettings.AbandonedCartEventDiscountCumulative,
                DiscountLimitationType.NTimesOnly,
                1,
                _discountCouponGeneratorSettings.AbandonedCartEventDiscountShareable ? null : new int[] { customer.Id });

            if (customer == null)
                throw new ArgumentNullException(nameof(customer));
            
            if (messageTemplate == null)
                return 0;

            //email account
            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

            //tokens
            var tokens = new List<Token>();

            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            _messageTokenProvider.AddCustomerTokens(tokens, customer);
            _customMessageTokenProvider.AddDiscountCouponTokens(tokens, discount);
            _customMessageTokenProvider.AddShoppingCartItemsTokens(tokens, customer, store.Id);

            //event notification
            _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

            int queuedEmailId = SendNotification(messageTemplate, emailAccount, languageId, tokens, customer.Email, customer.GetFullName());

            // add discount coupon message record
            var discountCouponGeneratorMessageRecord = new DiscountCouponGeneratorMessageRecord
            {
                StoreId = store.Id,
                EmailAddress = customer.Email,
                DiscountCouponGeneratorMessageType = DiscountCouponGeneratorMessageType.AbandonedCart,
                QueuedEmailId = queuedEmailId,
                CreatedOnUtc = DateTime.UtcNow
            };

            _discountCouponGeneratorMessageRecordRepository.Insert(discountCouponGeneratorMessageRecord);

            return queuedEmailId;
        }

        #endregion
    }
}