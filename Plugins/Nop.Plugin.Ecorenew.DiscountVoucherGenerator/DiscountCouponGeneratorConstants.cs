﻿namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator
{
    public static class DiscountCouponGeneratorConstants
    {
        public const string PluginSystemName = "Ecorenew.DiscountCouponGenerator";
        public const string CustomerRegisteredEventCouponCodeName = "Customer Registered";

        public const string Over100MonthlyCouponEventCouponCodeName = "Monthly Coupon 100";
        public const string Over200MonthlyCouponEventCouponCodeName = "Monthly Coupon 200";
        public const string Over500MonthlyCouponEventCouponCodeName = "Monthly Coupon 300";
        public const string Over100MonthlyCouponEventDiscountTokenName = "Discount.Coupon.100";
        public const string Over200MonthlyCouponEventDiscountTokenName = "Discount.Coupon.200";
        public const string Over500MonthlyCouponEventDiscountTokenName = "Discount.Coupon.500";
        public const string MonthlyCouponEventScheduleTaskName = "Monthly Coupon Event (Discount Coupon)";
        public const string MonthlyCouponEventScheduleTask = "Nop.Plugin.Ecorenew.DiscountCouponGenerator.Tasks.MonthlyCouponGeneratorTask";

        public const string Over200CustomerBirthdayEventCouponCodeName = "Customer Birthday 200";
        public const string Over1000CustomerBirthdayEventCouponCodeName = "Customer Birthday 1000";
        public const string Over200CustomerBirthdayEventDiscountTokenName = "Discount.Coupon.200";
        public const string Over1000CustomerBirthdayEventDiscountTokenName = "Discount.Coupon.1000";
        public const string CustomerBirthdayEventScheduleTaskName = "Customer Birthday Event (Message Token)";
        public const string CustomerBirthdayEventScheduleTask = "Nop.Plugin.Ecorenew.DiscountCouponGenerator.Tasks.CustomerBirthdayMessageGeneratorTask";


        public const string AbandonedCartEventCouponCodeName = "Abandoned Cart Discount";
        public const string AbandonedCartEventScheduleTaskName = "Abandoned Cart Event (Discount Coupon)";
        public const string AbandonedCartEventScheduleTask = "Nop.Plugin.Ecorenew.DiscountCouponGenerator.Tasks.AbandonedCartMessageGeneratorTask";
    }
}