﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Models;
using Nop.Services;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using System.Linq;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Controllers
{
    public class DiscountCouponGeneratorController : BasePluginController
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public DiscountCouponGeneratorController(ILocalizationService localizationService,
            IPermissionService permissionService,
            ISettingService settingService,
            IStoreService storeService,
            IWorkContext workContext)
        {
            this._settingService = settingService;
            this._storeService = storeService;
            this._workContext = workContext;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
        }

        #endregion

        #region Methods

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDiscounts))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var discountCouponGeneratorSettings = _settingService.LoadSetting<DiscountCouponGeneratorSettings>(storeScope);

            var model = new ConfigurationModel
            {
                ActiveStoreScopeConfiguration = storeScope,
                MailChimpApiKey = discountCouponGeneratorSettings.MailChimpApiKey,

                CustomerRegisteredEventDiscountEnabled = discountCouponGeneratorSettings.CustomerRegisteredEventDiscountEnabled,
                CustomerRegisteredEventDiscountUsePercentage = discountCouponGeneratorSettings.CustomerRegisteredEventDiscountUsePercentage,
                CustomerRegisteredEventDiscountValue = discountCouponGeneratorSettings.CustomerRegisteredEventDiscountValue,
                CustomerRegisteredEventDiscountDaysToExpire = discountCouponGeneratorSettings.CustomerRegisteredEventDiscountDaysToExpire,
                CustomerRegisteredEventFormat = discountCouponGeneratorSettings.CustomerRegisteredEventFormat,
                CustomerRegisteredEventRandomCodeLength = discountCouponGeneratorSettings.CustomerRegisteredEventRandomCodeLength,

                MonthlyCouponEventDiscountEnabled = discountCouponGeneratorSettings.MonthlyCouponEventDiscountEnabled,
                MonthlyCouponEventDiscountUsePercentage = discountCouponGeneratorSettings.MonthlyCouponEventDiscountUsePercentage,
                Over100MonthlyCouponEventDiscountValue = discountCouponGeneratorSettings.Over100MonthlyCouponEventDiscountValue,
                Over200MonthlyCouponEventDiscountValue = discountCouponGeneratorSettings.Over200MonthlyCouponEventDiscountValue,
                Over500MonthlyCouponEventDiscountValue = discountCouponGeneratorSettings.Over500MonthlyCouponEventDiscountValue,
                MonthlyCouponEventDiscountDaysToExpire = discountCouponGeneratorSettings.MonthlyCouponEventDiscountDaysToExpire,
                MonthlyCouponEventFormat = discountCouponGeneratorSettings.MonthlyCouponEventFormat,
                MonthlyCouponEventRandomCodeLength = discountCouponGeneratorSettings.MonthlyCouponEventRandomCodeLength,
                MonthlyCouponEventUseMailChimpList = discountCouponGeneratorSettings.MonthlyCouponEventUseMailChimpList,
                MonthlyCouponEventMailChimpListIds = discountCouponGeneratorSettings.MonthlyCouponEventMailChimpListIds,
                MonthlyCouponEventDiscountCumulative = discountCouponGeneratorSettings.MonthlyCouponEventDiscountCumulative,

                CustomerBirthdayEventDiscountEnabled = discountCouponGeneratorSettings.CustomerBirthdayEventDiscountEnabled,
                CustomerBirthdayEventDiscountUsePercentage = discountCouponGeneratorSettings.CustomerBirthdayEventDiscountUsePercentage,
                Over200CustomerBirthdayEventDiscountValue = discountCouponGeneratorSettings.Over200CustomerBirthdayEventDiscountValue,
                Over1000CustomerBirthdayEventDiscountValue = discountCouponGeneratorSettings.Over1000CustomerBirthdayEventDiscountValue,
                CustomerBirthdayEventDiscountDaysToExpire = discountCouponGeneratorSettings.CustomerBirthdayEventDiscountDaysToExpire,
                CustomerBirthdayEventFormat = discountCouponGeneratorSettings.CustomerBirthdayEventFormat,
                CustomerBirthdayEventRandomCodeLength = discountCouponGeneratorSettings.CustomerBirthdayEventRandomCodeLength,
                CustomerBirthdayEventUseMailChimpList = discountCouponGeneratorSettings.CustomerBirthdayEventUseMailChimpList,
                CustomerBirthdayEventMailChimpListIds = discountCouponGeneratorSettings.MonthlyCouponEventMailChimpListIds,
                CustomerBirthdayEventDiscountCumulative = discountCouponGeneratorSettings.CustomerBirthdayEventDiscountCumulative,

                AbandonedCartEventDiscountEnabled = discountCouponGeneratorSettings.AbandonedCartEventDiscountEnabled,
                AbandonedCartEventDiscountName = discountCouponGeneratorSettings.AbandonedCartEventDiscountName,
                AbandonedCartEventDiscountUsePercentage = discountCouponGeneratorSettings.AbandonedCartEventDiscountUsePercentage,
                AbandonedCartEventDiscountValue = discountCouponGeneratorSettings.AbandonedCartEventDiscountValue,
                AbandonedCartEventDiscountDaysToExpire = discountCouponGeneratorSettings.AbandonedCartEventDiscountDaysToExpire,
                AbandonedCartEventFormat = discountCouponGeneratorSettings.AbandonedCartEventFormat,
                AbandonedCartEventRandomCodeLength = discountCouponGeneratorSettings.AbandonedCartEventRandomCodeLength,
                AbandonedCartEventConditionMetEarlierThan = discountCouponGeneratorSettings.AbandonedCartEventConditionMetEarlierThan,
                AbandonedCartEventConditionMetUomEarlierThan = discountCouponGeneratorSettings.AbandonedCartEventConditionMetUomEarlierThan,
                AbandonedCartEventConditionMetLaterThan = discountCouponGeneratorSettings.AbandonedCartEventConditionMetLaterThan,
                AbandonedCartEventConditionMetUomLaterThan = discountCouponGeneratorSettings.AbandonedCartEventConditionMetUomLaterThan,
                AbandonedCartEventDiscountCumulative = discountCouponGeneratorSettings.AbandonedCartEventDiscountCumulative,
                AbandonedCartEventDiscountShareable = discountCouponGeneratorSettings.AbandonedCartEventDiscountShareable
            };

            if (storeScope > 0)
            {
                model.MailChimpApiKey_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.MailChimpApiKey, storeScope);

                model.CustomerRegisteredEventDiscountEnabled_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerRegisteredEventDiscountEnabled, storeScope);
                model.CustomerRegisteredEventDiscountUsePercentage_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerRegisteredEventDiscountUsePercentage, storeScope);
                model.CustomerRegisteredEventDiscountValue_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerRegisteredEventDiscountValue, storeScope);
                model.CustomerRegisteredEventDiscountDaysToExpire_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerRegisteredEventDiscountDaysToExpire, storeScope);
                model.CustomerRegisteredEventFormat_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerRegisteredEventFormat, storeScope);
                model.CustomerRegisteredEventRandomCodeLength_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerRegisteredEventRandomCodeLength, storeScope);

                model.MonthlyCouponEventDiscountEnabled_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.MonthlyCouponEventDiscountEnabled, storeScope);
                model.MonthlyCouponEventDiscountUsePercentage_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.MonthlyCouponEventDiscountUsePercentage, storeScope);
                model.Over100MonthlyCouponEventDiscountValue_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.Over100MonthlyCouponEventDiscountValue, storeScope);
                model.Over200MonthlyCouponEventDiscountValue_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.Over200MonthlyCouponEventDiscountValue, storeScope);
                model.Over500MonthlyCouponEventDiscountValue_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.Over500MonthlyCouponEventDiscountValue, storeScope);
                model.MonthlyCouponEventDiscountDaysToExpire_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.MonthlyCouponEventDiscountDaysToExpire, storeScope);
                model.MonthlyCouponEventFormat_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.MonthlyCouponEventFormat, storeScope);
                model.MonthlyCouponEventRandomCodeLength_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.MonthlyCouponEventRandomCodeLength, storeScope);
                model.MonthlyCouponEventRandomCodeLength_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.MonthlyCouponEventMailChimpListIds, storeScope);
                model.MonthlyCouponEventUseMailChimpList_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.MonthlyCouponEventUseMailChimpList, storeScope);
                model.MonthlyCouponEventDiscountCumulative_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.MonthlyCouponEventDiscountCumulative, storeScope);

                model.CustomerBirthdayEventDiscountEnabled_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventDiscountEnabled, storeScope);
                model.CustomerBirthdayEventDiscountUsePercentage_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventDiscountUsePercentage, storeScope);
                model.Over200CustomerBirthdayEventDiscountValue_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.Over200CustomerBirthdayEventDiscountValue, storeScope);
                model.CustomerBirthdayEventDiscountDaysToExpire_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventDiscountDaysToExpire, storeScope);
                model.CustomerBirthdayEventFormat_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventFormat, storeScope);
                model.CustomerBirthdayEventRandomCodeLength_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventRandomCodeLength, storeScope);
                model.CustomerBirthdayEventUseMailChimpList_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventUseMailChimpList, storeScope);
                model.CustomerBirthdayEventMailChimpListIds_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.MonthlyCouponEventMailChimpListIds, storeScope);
                model.CustomerBirthdayEventDiscountCumulative_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventDiscountCumulative, storeScope);

                model.AbandonedCartEventDiscountEnabled_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountEnabled, storeScope);
                model.AbandonedCartEventDiscountName_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountName, storeScope);
                model.AbandonedCartEventDiscountUsePercentage_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountUsePercentage, storeScope);
                model.AbandonedCartEventDiscountValue_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountValue, storeScope);
                model.AbandonedCartEventDiscountDaysToExpire_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountDaysToExpire, storeScope);
                model.AbandonedCartEventFormat_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventFormat, storeScope);
                model.AbandonedCartEventRandomCodeLength_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventRandomCodeLength, storeScope);
                model.AbandonedCartEventConditionMetEarlierThan_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventConditionMetEarlierThan, storeScope);
                model.AbandonedCartEventConditionMetUomEarlierThan_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventConditionMetUomEarlierThan, storeScope);
                model.AbandonedCartEventConditionMetLaterThan_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventConditionMetLaterThan, storeScope);
                model.AbandonedCartEventConditionMetUomLaterThan_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventConditionMetUomLaterThan, storeScope);
                model.AbandonedCartEventDiscountCumulative_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountCumulative, storeScope);
                model.AbandonedCartEventDiscountShareable_OverrideForStore = _settingService.SettingExists(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountShareable, storeScope);
            }

            model.AbandonedCartEventConditionMetUomEarlierThanList = ConditionMetUnitOfMeasurement.Days.ToSelectList(false).ToList();
            foreach (var item in model.AbandonedCartEventConditionMetUomEarlierThanList.Where(x => x.Value == model.AbandonedCartEventConditionMetUomEarlierThan.ToString()))
                item.Selected = true;

            model.AbandonedCartEventConditionMetUomLaterThanList = ConditionMetUnitOfMeasurement.Days.ToSelectList(false).ToList();
            foreach (var item in model.AbandonedCartEventConditionMetUomLaterThanList.Where(x => x.Value == model.AbandonedCartEventConditionMetUomLaterThan.ToString()))
                item.Selected = true;

            return View("~/Plugins/Ecorenew.DiscountCouponGenerator/Views/Configure.cshtml", model);
        }

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var discountCouponGeneratorSettings = _settingService.LoadSetting<DiscountCouponGeneratorSettings>(storeScope);

            //save settings
            discountCouponGeneratorSettings.MailChimpApiKey = model.MailChimpApiKey;

            discountCouponGeneratorSettings.CustomerRegisteredEventDiscountEnabled = model.CustomerRegisteredEventDiscountEnabled;
            discountCouponGeneratorSettings.CustomerRegisteredEventDiscountUsePercentage = model.CustomerRegisteredEventDiscountUsePercentage;
            discountCouponGeneratorSettings.CustomerRegisteredEventDiscountValue = model.CustomerRegisteredEventDiscountValue;
            discountCouponGeneratorSettings.CustomerRegisteredEventDiscountDaysToExpire = model.CustomerRegisteredEventDiscountDaysToExpire;
            discountCouponGeneratorSettings.CustomerRegisteredEventFormat = model.CustomerRegisteredEventFormat;
            discountCouponGeneratorSettings.CustomerRegisteredEventRandomCodeLength = model.CustomerRegisteredEventRandomCodeLength;

            discountCouponGeneratorSettings.MonthlyCouponEventDiscountEnabled = model.MonthlyCouponEventDiscountEnabled;
            discountCouponGeneratorSettings.MonthlyCouponEventDiscountUsePercentage = model.MonthlyCouponEventDiscountUsePercentage;
            discountCouponGeneratorSettings.Over100MonthlyCouponEventDiscountValue = model.Over100MonthlyCouponEventDiscountValue;
            discountCouponGeneratorSettings.Over200MonthlyCouponEventDiscountValue = model.Over200MonthlyCouponEventDiscountValue;
            discountCouponGeneratorSettings.Over500MonthlyCouponEventDiscountValue = model.Over500MonthlyCouponEventDiscountValue;
            discountCouponGeneratorSettings.MonthlyCouponEventDiscountDaysToExpire = model.MonthlyCouponEventDiscountDaysToExpire;
            discountCouponGeneratorSettings.MonthlyCouponEventFormat = model.MonthlyCouponEventFormat;
            discountCouponGeneratorSettings.MonthlyCouponEventRandomCodeLength = model.MonthlyCouponEventRandomCodeLength;
            discountCouponGeneratorSettings.MonthlyCouponEventUseMailChimpList = model.MonthlyCouponEventUseMailChimpList;
            discountCouponGeneratorSettings.MonthlyCouponEventMailChimpListIds = model.MonthlyCouponEventMailChimpListIds;
            discountCouponGeneratorSettings.MonthlyCouponEventDiscountCumulative = model.MonthlyCouponEventDiscountCumulative;

            discountCouponGeneratorSettings.CustomerBirthdayEventDiscountEnabled = model.CustomerBirthdayEventDiscountEnabled;
            discountCouponGeneratorSettings.CustomerBirthdayEventDiscountUsePercentage = model.CustomerBirthdayEventDiscountUsePercentage;
            discountCouponGeneratorSettings.Over200CustomerBirthdayEventDiscountValue = model.Over200CustomerBirthdayEventDiscountValue;
            discountCouponGeneratorSettings.CustomerBirthdayEventDiscountDaysToExpire = model.CustomerBirthdayEventDiscountDaysToExpire;
            discountCouponGeneratorSettings.CustomerBirthdayEventFormat = model.CustomerBirthdayEventFormat;
            discountCouponGeneratorSettings.CustomerBirthdayEventRandomCodeLength = model.CustomerBirthdayEventRandomCodeLength;
            discountCouponGeneratorSettings.CustomerBirthdayEventUseMailChimpList = model.CustomerBirthdayEventUseMailChimpList;
            discountCouponGeneratorSettings.CustomerBirthdayEventMailChimpListIds = model.CustomerBirthdayEventMailChimpListIds;
            discountCouponGeneratorSettings.CustomerBirthdayEventDiscountCumulative = model.CustomerBirthdayEventDiscountCumulative;

            discountCouponGeneratorSettings.AbandonedCartEventDiscountEnabled = model.AbandonedCartEventDiscountEnabled;
            discountCouponGeneratorSettings.AbandonedCartEventDiscountName = model.AbandonedCartEventDiscountName;
            discountCouponGeneratorSettings.AbandonedCartEventDiscountUsePercentage = model.AbandonedCartEventDiscountUsePercentage;
            discountCouponGeneratorSettings.AbandonedCartEventDiscountValue = model.AbandonedCartEventDiscountValue;
            discountCouponGeneratorSettings.AbandonedCartEventDiscountDaysToExpire = model.AbandonedCartEventDiscountDaysToExpire;
            discountCouponGeneratorSettings.AbandonedCartEventFormat = model.AbandonedCartEventFormat;
            discountCouponGeneratorSettings.AbandonedCartEventRandomCodeLength = model.AbandonedCartEventRandomCodeLength;

            discountCouponGeneratorSettings.AbandonedCartEventConditionMetEarlierThan = model.AbandonedCartEventConditionMetEarlierThan;
            discountCouponGeneratorSettings.AbandonedCartEventConditionMetUomEarlierThan = model.AbandonedCartEventConditionMetUomEarlierThan;
            discountCouponGeneratorSettings.AbandonedCartEventConditionMetLaterThan = model.AbandonedCartEventConditionMetLaterThan;
            discountCouponGeneratorSettings.AbandonedCartEventConditionMetUomLaterThan = model.AbandonedCartEventConditionMetUomLaterThan;
            discountCouponGeneratorSettings.AbandonedCartEventDiscountCumulative = model.AbandonedCartEventDiscountCumulative;
            discountCouponGeneratorSettings.AbandonedCartEventDiscountShareable = model.AbandonedCartEventDiscountShareable;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.MailChimpApiKey, model.MailChimpApiKey_OverrideForStore, storeScope, false);

            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerRegisteredEventDiscountEnabled, model.CustomerRegisteredEventDiscountEnabled_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerRegisteredEventDiscountUsePercentage, model.CustomerRegisteredEventDiscountUsePercentage_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerRegisteredEventDiscountValue, model.CustomerRegisteredEventDiscountValue_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerRegisteredEventDiscountDaysToExpire, model.CustomerRegisteredEventDiscountDaysToExpire_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerRegisteredEventFormat, model.CustomerRegisteredEventFormat_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerRegisteredEventRandomCodeLength, model.CustomerRegisteredEventRandomCodeLength_OverrideForStore, storeScope, false);

            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.MonthlyCouponEventDiscountEnabled, model.MonthlyCouponEventDiscountEnabled_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.MonthlyCouponEventDiscountUsePercentage, model.MonthlyCouponEventDiscountUsePercentage_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.Over100MonthlyCouponEventDiscountValue, model.Over100MonthlyCouponEventDiscountValue_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.Over200MonthlyCouponEventDiscountValue, model.Over200MonthlyCouponEventDiscountValue_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.Over500MonthlyCouponEventDiscountValue, model.Over500MonthlyCouponEventDiscountValue_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.MonthlyCouponEventDiscountDaysToExpire, model.MonthlyCouponEventDiscountDaysToExpire_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.MonthlyCouponEventFormat, model.MonthlyCouponEventFormat_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.MonthlyCouponEventRandomCodeLength, model.MonthlyCouponEventRandomCodeLength_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.MonthlyCouponEventUseMailChimpList, model.MonthlyCouponEventUseMailChimpList_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.MonthlyCouponEventMailChimpListIds, model.MonthlyCouponEventMailChimpListIds_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.MonthlyCouponEventDiscountCumulative, model.MonthlyCouponEventDiscountCumulative_OverrideForStore, storeScope, false);

            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventDiscountEnabled, model.CustomerBirthdayEventDiscountEnabled_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventDiscountUsePercentage, model.CustomerBirthdayEventDiscountUsePercentage_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.Over200CustomerBirthdayEventDiscountValue, model.Over200CustomerBirthdayEventDiscountValue_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.Over1000CustomerBirthdayEventDiscountValue, model.Over1000CustomerBirthdayEventDiscountValue_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventDiscountDaysToExpire, model.CustomerBirthdayEventDiscountDaysToExpire_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventFormat, model.CustomerBirthdayEventFormat_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventRandomCodeLength, model.CustomerBirthdayEventRandomCodeLength_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventUseMailChimpList, model.CustomerBirthdayEventUseMailChimpList_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventMailChimpListIds, model.CustomerBirthdayEventMailChimpListIds_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.CustomerBirthdayEventDiscountCumulative, model.CustomerBirthdayEventDiscountCumulative_OverrideForStore, storeScope, false);

            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountEnabled, model.AbandonedCartEventDiscountEnabled_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountName, model.AbandonedCartEventDiscountName_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountUsePercentage, model.AbandonedCartEventDiscountUsePercentage_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountValue, model.AbandonedCartEventDiscountValue_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountDaysToExpire, model.AbandonedCartEventDiscountDaysToExpire_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventFormat, model.AbandonedCartEventFormat_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventRandomCodeLength, model.AbandonedCartEventRandomCodeLength_OverrideForStore, storeScope, false);

            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventConditionMetEarlierThan, model.AbandonedCartEventConditionMetEarlierThan_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventConditionMetUomEarlierThan, model.AbandonedCartEventConditionMetUomEarlierThan_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventConditionMetLaterThan, model.AbandonedCartEventConditionMetLaterThan_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventConditionMetUomLaterThan, model.AbandonedCartEventConditionMetUomLaterThan_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountCumulative, model.AbandonedCartEventDiscountCumulative_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(discountCouponGeneratorSettings, x => x.AbandonedCartEventDiscountShareable, model.AbandonedCartEventDiscountShareable_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            model.AbandonedCartEventConditionMetUomEarlierThanList = ConditionMetUnitOfMeasurement.Days.ToSelectList(false).ToList();
            foreach (var item in model.AbandonedCartEventConditionMetUomEarlierThanList.Where(x => x.Value == model.AbandonedCartEventConditionMetUomEarlierThan.ToString()))
                item.Selected = true;

            model.AbandonedCartEventConditionMetUomLaterThanList = ConditionMetUnitOfMeasurement.Days.ToSelectList(false).ToList();
            foreach (var item in model.AbandonedCartEventConditionMetUomLaterThanList.Where(x => x.Value == model.AbandonedCartEventConditionMetUomLaterThan.ToString()))
                item.Selected = true;

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        #endregion
    }
}
