﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Models
{
    public class ConfigurationModel
    {
        public ConfigurationModel()
        {
            this.AbandonedCartEventConditionMetUomLaterThanList = new List<SelectListItem>();
            this.AbandonedCartEventConditionMetUomEarlierThanList = new List<SelectListItem>();
        }

        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MailChimpApiKey")]
        public string MailChimpApiKey { get; set; }
        public bool MailChimpApiKey_OverrideForStore { get; set; }

        #region Customer Registered Event

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventDiscountEnabled")]
        public bool CustomerRegisteredEventDiscountEnabled { get; set; }
        public bool CustomerRegisteredEventDiscountEnabled_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventDiscountUsePercentage")]
        public bool CustomerRegisteredEventDiscountUsePercentage { get; set; }
        public bool CustomerRegisteredEventDiscountUsePercentage_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventDiscountValue")]
        public decimal CustomerRegisteredEventDiscountValue { get; set; }
        public bool CustomerRegisteredEventDiscountValue_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventDiscountDaysToExpire")]
        public int CustomerRegisteredEventDiscountDaysToExpire { get; set; }
        public bool CustomerRegisteredEventDiscountDaysToExpire_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventFormat")]
        public string CustomerRegisteredEventFormat { get; set; }
        public bool CustomerRegisteredEventFormat_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventRandomCodeLength")]
        public int CustomerRegisteredEventRandomCodeLength { get; set; }
        public bool CustomerRegisteredEventRandomCodeLength_OverrideForStore { get; set; }

        #endregion

        #region Monthly Coupon Event

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountEnabled")]
        public bool MonthlyCouponEventDiscountEnabled { get; set; }
        public bool MonthlyCouponEventDiscountEnabled_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountUsePercentage")]
        public bool MonthlyCouponEventDiscountUsePercentage { get; set; }
        public bool MonthlyCouponEventDiscountUsePercentage_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over100MonthlyCouponEventDiscountValue")]
        public decimal Over100MonthlyCouponEventDiscountValue { get; set; }
        public bool Over100MonthlyCouponEventDiscountValue_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over200MonthlyCouponEventDiscountValue")]
        public decimal Over200MonthlyCouponEventDiscountValue { get; set; }
        public bool Over200MonthlyCouponEventDiscountValue_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over500MonthlyCouponEventDiscountValue")]
        public decimal Over500MonthlyCouponEventDiscountValue { get; set; }
        public bool Over500MonthlyCouponEventDiscountValue_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountDaysToExpire")]
        public int MonthlyCouponEventDiscountDaysToExpire { get; set; }
        public bool MonthlyCouponEventDiscountDaysToExpire_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventFormat")]
        public string MonthlyCouponEventFormat { get; set; }
        public bool MonthlyCouponEventFormat_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventRandomCodeLength")]
        public int MonthlyCouponEventRandomCodeLength { get; set; }
        public bool MonthlyCouponEventRandomCodeLength_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventUseMailChimpList")]
        public bool MonthlyCouponEventUseMailChimpList { get; set; }
        public bool MonthlyCouponEventUseMailChimpList_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventMailChimpListIds")]
        public string MonthlyCouponEventMailChimpListIds { get; set; }
        public bool MonthlyCouponEventMailChimpListIds_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountCumulative")]
        public bool MonthlyCouponEventDiscountCumulative { get; set; }
        public bool MonthlyCouponEventDiscountCumulative_OverrideForStore { get; set; }

        #endregion

        #region Customer Birthday Event

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountEnabled")]
        public bool CustomerBirthdayEventDiscountEnabled { get; set; }
        public bool CustomerBirthdayEventDiscountEnabled_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountUsePercentage")]
        public bool CustomerBirthdayEventDiscountUsePercentage { get; set; }
        public bool CustomerBirthdayEventDiscountUsePercentage_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over200CustomerBirthdayEventDiscountValue")]
        public decimal Over200CustomerBirthdayEventDiscountValue { get; set; }
        public bool Over200CustomerBirthdayEventDiscountValue_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over1000CustomerBirthdayEventDiscountValue")]
        public decimal Over1000CustomerBirthdayEventDiscountValue { get; set; }
        public bool Over1000CustomerBirthdayEventDiscountValue_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountDaysToExpire")]
        public int CustomerBirthdayEventDiscountDaysToExpire { get; set; }
        public bool CustomerBirthdayEventDiscountDaysToExpire_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventFormat")]
        public string CustomerBirthdayEventFormat { get; set; }
        public bool CustomerBirthdayEventFormat_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventRandomCodeLength")]
        public int CustomerBirthdayEventRandomCodeLength { get; set; }
        public bool CustomerBirthdayEventRandomCodeLength_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventUseMailChimpList")]
        public bool CustomerBirthdayEventUseMailChimpList { get; set; }
        public bool CustomerBirthdayEventUseMailChimpList_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventMailChimpListIDs")]
        public string CustomerBirthdayEventMailChimpListIds { get; set; }
        public bool CustomerBirthdayEventMailChimpListIds_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountCumulative")]
        public bool CustomerBirthdayEventDiscountCumulative { get; set; }
        public bool CustomerBirthdayEventDiscountCumulative_OverrideForStore { get; set; }

        #endregion

        #region Abandoned Cart Event

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountEnabled")]
        public bool AbandonedCartEventDiscountEnabled { get; set; }
        public bool AbandonedCartEventDiscountEnabled_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountName")]
        public string AbandonedCartEventDiscountName { get; set; }
        public bool AbandonedCartEventDiscountName_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountUsePercentage")]
        public bool AbandonedCartEventDiscountUsePercentage { get; set; }
        public bool AbandonedCartEventDiscountUsePercentage_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountValue")]
        public decimal AbandonedCartEventDiscountValue { get; set; }
        public bool AbandonedCartEventDiscountValue_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountDaysToExpire")]
        public int AbandonedCartEventDiscountDaysToExpire { get; set; }
        public bool AbandonedCartEventDiscountDaysToExpire_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventFormat")]
        public string AbandonedCartEventFormat { get; set; }
        public bool AbandonedCartEventFormat_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventRandomCodeLength")]
        public int AbandonedCartEventRandomCodeLength { get; set; }
        public bool AbandonedCartEventRandomCodeLength_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventConditionMetLaterThan")]
        public int AbandonedCartEventConditionMetLaterThan { get; set; }
        public bool AbandonedCartEventConditionMetLaterThan_OverrideForStore { get; set; }

        public ConditionMetUnitOfMeasurement AbandonedCartEventConditionMetUomLaterThan { get; set; }
        public bool AbandonedCartEventConditionMetUomLaterThan_OverrideForStore { get; set; }
        public IList<SelectListItem> AbandonedCartEventConditionMetUomLaterThanList { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventConditionMetEarlierThan")]
        public int AbandonedCartEventConditionMetEarlierThan { get; set; }
        public bool AbandonedCartEventConditionMetEarlierThan_OverrideForStore { get; set; }

        public ConditionMetUnitOfMeasurement AbandonedCartEventConditionMetUomEarlierThan { get; set; }
        public bool AbandonedCartEventConditionMetUomEarlierThan_OverrideForStore { get; set; }
        public IList<SelectListItem> AbandonedCartEventConditionMetUomEarlierThanList { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountCumulative")]
        public bool AbandonedCartEventDiscountCumulative { get; set; }
        public bool AbandonedCartEventDiscountCumulative_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountShareable")]
        public bool AbandonedCartEventDiscountShareable { get; set; }
        public bool AbandonedCartEventDiscountShareable_OverrideForStore { get; set; }

        #endregion
    }
}