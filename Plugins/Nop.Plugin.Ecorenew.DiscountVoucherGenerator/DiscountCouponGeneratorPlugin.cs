﻿using Nop.Core;
using Nop.Core.Domain.Tasks;
using Nop.Core.Plugins;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Data;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Tasks;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Core.Domain.Messages;
using System.Linq;
using Nop.Web.Framework.Menu;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator
{
    public class DiscountCouponGeneratorPlugin : BasePlugin, IMiscPlugin, IAdminMenuPlugin
    {
        #region Fields

        private readonly IEmailAccountService _emailAccountService;
        private readonly ILogger _logger;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IScheduleTaskService _scheduleTaskService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;

        private readonly DiscountCouponGeneratorObjectContext _objectContext;

        #endregion

        #region Ctor

        public DiscountCouponGeneratorPlugin(IEmailAccountService emailAccountService,
            ILogger logger,
            IMessageTemplateService messageTemplateService,
            IScheduleTaskService scheduleTaskService, 
            ISettingService settingService,
            IStoreContext storeContext,
            IWebHelper webHelper,
            IWorkContext workContext,
            DiscountCouponGeneratorObjectContext objectContext)
        {
            this._emailAccountService = emailAccountService;
            this._logger = logger;
            this._messageTemplateService = messageTemplateService;
            this._scheduleTaskService = scheduleTaskService;
            this._settingService = settingService;
            this._storeContext = storeContext;
            this._webHelper = webHelper;
            this._objectContext = objectContext;
            this._workContext = workContext;
        }

        #endregion

        #region Methods

        public override void Install()
        {
            // settings
            var settings = new DiscountCouponGeneratorSettings
            {
                CustomerRegisteredEventDiscountEnabled = true,
                CustomerRegisteredEventDiscountUsePercentage = false,
                CustomerRegisteredEventDiscountValue = 10,
                CustomerRegisteredEventDiscountDaysToExpire = 30,
                CustomerRegisteredEventFormat = "",
                CustomerRegisteredEventRandomCodeLength = 12,

                MonthlyCouponEventDiscountEnabled = false,
                MonthlyCouponEventDiscountUsePercentage = false,
                Over100MonthlyCouponEventDiscountValue = 10,
                Over200MonthlyCouponEventDiscountValue = 20,
                Over500MonthlyCouponEventDiscountValue = 50,
                MonthlyCouponEventDiscountDaysToExpire = 30,
                MonthlyCouponEventFormat = "",
                MonthlyCouponEventRandomCodeLength = 12,
                MonthlyCouponEventUseMailChimpList = false,
                MonthlyCouponEventMailChimpListIds = "",
                MonthlyCouponEventDiscountCumulative = false,

                CustomerBirthdayEventDiscountEnabled = false,
                CustomerBirthdayEventDiscountUsePercentage = false,
                Over200CustomerBirthdayEventDiscountValue = 20,
                Over1000CustomerBirthdayEventDiscountValue = 100,
                CustomerBirthdayEventDiscountDaysToExpire = 30,
                CustomerBirthdayEventFormat = "",
                CustomerBirthdayEventRandomCodeLength = 12,
                CustomerBirthdayEventUseMailChimpList = false,
                CustomerBirthdayEventMailChimpListIds = "",
                CustomerBirthdayEventDiscountCumulative = false,

                AbandonedCartEventDiscountEnabled = false,
                AbandonedCartEventDiscountName = "Abandoned Cart Coupon",
                AbandonedCartEventDiscountUsePercentage = false,
                AbandonedCartEventDiscountValue = 10,
                AbandonedCartEventDiscountDaysToExpire = 2,
                AbandonedCartEventFormat = "",
                AbandonedCartEventRandomCodeLength = 12,
                AbandonedCartEventDiscountCumulative = false,
                AbandonedCartEventDiscountShareable = false,
                AbandonedCartEventConditionMetEarlierThan = 1,
                AbandonedCartEventConditionMetUomEarlierThan = ConditionMetUnitOfMeasurement.Days,
                AbandonedCartEventConditionMetLaterThan = 2,
                AbandonedCartEventConditionMetUomLaterThan = ConditionMetUnitOfMeasurement.Days
            };
            _settingService.SaveSetting(settings);

            _objectContext.Install();

            // locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MailChimpApiKey", "MailChimp API Key");

            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Registered.Event.Title", "Registered Event");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Monthly.Event.Title", "Monthly Event");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Birthday.Event.Title", "Birthday Event");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.AbandonedCart.Event.Title", "Abandoned Cart Event");

            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventDiscountEnabled", "Enable Event");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventDiscountUsePercentage", "Use Percentage");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventDiscountValue", "Discount Value");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventDiscountDaysToExpire", "Days to expire");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventFormat", "Format");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventRandomCodeLength", "Random Code Length");

            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountEnabled", "Enable Event");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountName", "Discount Name");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountUsePercentage", "Use Percentage");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over100MonthlyCouponEventDiscountValue", "Over 100 Discount Value");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over200MonthlyCouponEventDiscountValue", "Over 200 Discount Value");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over500MonthlyCouponEventDiscountValue", "Over 500 Discount Value");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountDaysToExpire", "Days to expire");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventFormat", "Format");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventRandomCodeLength", "Random Code Length");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventUseMailChimpList", "Use MailChimp Lists");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventMailChimpListIds", "Mailchimp List IDs");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountCumulative", "Is the Discount cumulative?");

            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountEnabled", "Enable Event");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountName", "Discount Name");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountUsePercentage", "Use Percentage");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over200CustomerBirthdayEventDiscountValue", "Over 200 Discount Value");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over1000CustomerBirthdayEventDiscountValue", "Over 1000 Discount Value");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountDaysToExpire", "Days to expire");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventFormat", "Format");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventRandomCodeLength", "Random Code Length");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventUseMailChimpList", "Use MailChimp Lists");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventMailChimpListIds", "Mailchimp List IDs");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountCumulative", "Is the Discount cumulative?");

            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountEnabled", "Enable Event");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountName", "Discount Name");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountUsePercentage", "Use Percentage");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountValue", "Discount Value");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountDaysToExpire", "Days to expire");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventFormat", "Format");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventRandomCodeLength", "Random Code Length");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventConditionMetLaterThan", "Condition Met Later Than");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventConditionMetEarlierThan", "Condition Met Earlier Than");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountCumulative", "Is the Discount cumulative?");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountShareable", "Is the Discount shareable?");

            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.AbandonedCartTemplate.Row", "Enter text here");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.AbandonedCartTemplate.Table", "Enter text here");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.AbandonedCartTemplate.Paragraph", "Enter text here");

            // install monthly coupon event schedule task
            if (_scheduleTaskService.GetTaskByType(DiscountCouponGeneratorConstants.MonthlyCouponEventScheduleTask) == null)
            {
                _scheduleTaskService.InsertTask(new ScheduleTask
                {
                    Enabled = true,
                    Seconds = 28800, // 8 hours?
                    Name = DiscountCouponGeneratorConstants.MonthlyCouponEventScheduleTaskName,
                    Type = DiscountCouponGeneratorConstants.MonthlyCouponEventScheduleTask,
                });
            }

            // install customer birthday event schedule task
            if (_scheduleTaskService.GetTaskByType(DiscountCouponGeneratorConstants.CustomerBirthdayEventScheduleTask) == null)
            {
                _scheduleTaskService.InsertTask(new ScheduleTask
                {
                    Enabled = true,
                    Seconds = 10800, // 3 hours?
                    Name = DiscountCouponGeneratorConstants.CustomerBirthdayEventScheduleTaskName,
                    Type = DiscountCouponGeneratorConstants.CustomerBirthdayEventScheduleTask,
                });
            }

            // install abandoned cart event schedule task
            if (_scheduleTaskService.GetTaskByType(DiscountCouponGeneratorConstants.AbandonedCartEventScheduleTask) == null)
            {
                _scheduleTaskService.InsertTask(new ScheduleTask
                {
                    Enabled = true,
                    Seconds = 1800, // 30 mins?
                    Name = DiscountCouponGeneratorConstants.AbandonedCartEventScheduleTaskName,
                    Type = DiscountCouponGeneratorConstants.AbandonedCartEventScheduleTask,
                });
            }

            var emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            if (emailAccount != null)
            {
                _messageTemplateService.InsertMessageTemplate(new MessageTemplate
                {
                    EmailAccountId = emailAccount.Id,
                    IsActive = true,
                    Name = DiscountCouponMessageTemplateSystemNames.AbandonedCartMessage,
                    Subject = DiscountCouponMessageTemplateSystemNames.AbandonedCartMessage,
                    Body = "This is a test",
                    LimitedToStores = false
                });

                _messageTemplateService.InsertMessageTemplate(new MessageTemplate
                {
                    EmailAccountId = emailAccount.Id,
                    IsActive = true,
                    Name = DiscountCouponMessageTemplateSystemNames.CustomerBirthdayMessage,
                    Subject = DiscountCouponMessageTemplateSystemNames.CustomerBirthdayMessage,
                    Body = "This is a test",
                    LimitedToStores = false
                });

                _messageTemplateService.InsertMessageTemplate(new MessageTemplate
                {
                    EmailAccountId = emailAccount.Id,
                    IsActive = true,
                    Name = DiscountCouponMessageTemplateSystemNames.MonthlyCouponMessage,
                    Subject = DiscountCouponMessageTemplateSystemNames.MonthlyCouponMessage,
                    Body = "This is a test",
                    LimitedToStores = false
                });
            }

            base.Install();
        }

        public override void Uninstall()
        {
            // settings
            _settingService.DeleteSetting<DiscountCouponGeneratorSettings>();

            _objectContext.Uninstall();

            // locales
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MailChimpApiKey");

            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Registered.Event.Title");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Monthly.Event.Title");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Birthday.Event.Title");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.AbandonedCart.Event.Title");

            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventDiscountEnabled");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventDiscountUsePercentage");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventDiscountValue");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventDiscountDaysToExpire");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventFormat");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerRegisteredEventRandomCodeLength");

            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountEnabled");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountName");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountUsePercentage");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over100MonthlyCouponEventDiscountValue");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over200MonthlyCouponEventDiscountValue");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over500MonthlyCouponEventDiscountValue");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountDaysToExpire");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventFormat");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventRandomCodeLength");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventUseMailChimpList");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventMailChimpListIds");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.MonthlyCouponEventDiscountCumulative");

            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountEnabled");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountName");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountUsePercentage");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over200CustomerBirthdayEventDiscountValue");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.Over1000CustomerBirthdayEventDiscountValue");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountDaysToExpire");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventFormat");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventRandomCodeLength");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventUseMailChimpList");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventMailChimpListIDs");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.CustomerBirthdayEventDiscountCumulative");

            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountEnabled");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountName");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountUsePercentage");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountValue");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountDaysToExpire");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventFormat");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventRandomCodeLength");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventConditionMetLaterThan");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventConditionMetEarlierThan");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountCumulative");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.Fields.AbandonedCartEventDiscountShareable");

            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.AbandonedCartTemplate.Row");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.AbandonedCartTemplate.Table");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.DiscountCouponCodeGenerator.AbandonedCartTemplate.Paragraph");

            // remove scheduled task
            var task = _scheduleTaskService.GetTaskByType(DiscountCouponGeneratorConstants.MonthlyCouponEventScheduleTask);

            if (task != null)
                _scheduleTaskService.DeleteTask(task);

            task = _scheduleTaskService.GetTaskByType(DiscountCouponGeneratorConstants.CustomerBirthdayEventScheduleTask);

            if (task != null)
                _scheduleTaskService.DeleteTask(task);

            task = _scheduleTaskService.GetTaskByType(DiscountCouponGeneratorConstants.AbandonedCartEventScheduleTask);

            if (task != null)
                _scheduleTaskService.DeleteTask(task);

            //var abandonedCartTemplate = _messageTemplateService.GetMessageTemplateByName(DiscountCouponMessageTemplateSystemNames.AbandonedCartMessage, 0);
            //if (abandonedCartTemplate != null)
            //    _messageTemplateService.DeleteMessageTemplate(abandonedCartTemplate);

            //var birthdayTemplate = _messageTemplateService.GetMessageTemplateByName(DiscountCouponMessageTemplateSystemNames.CustomerBirthdayMessage, 0);
            //if (birthdayTemplate != null)
            //    _messageTemplateService.DeleteMessageTemplate(birthdayTemplate);

            //var monthlyTemplate = _messageTemplateService.GetMessageTemplateByName(DiscountCouponMessageTemplateSystemNames.MonthlyCouponMessage, 0);
            //if (monthlyTemplate != null)
            //    _messageTemplateService.DeleteMessageTemplate(monthlyTemplate);

            base.Uninstall();
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/DiscountCouponGenerator/Configure";
        }

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var mainNode = new SiteMapNode()
            {
                SystemName = "Ecorenew",
                Title = "EcoRenew Group",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
                IconClass = "fa-leaf"
            };

            var discountCouponCodeGeneratorNode = new SiteMapNode()
            {
                SystemName = "Ecorenew.DiscountCouponGenerator",
                Title = "Discount Coupon Generator",
                Visible = true,
                IconClass = "fa-genderless",
                RouteValues = new RouteValueDictionary() { { "area", AreaNames.Admin } },
                ControllerName = "DiscountCouponGenerator",
                ActionName = "Configure"
            };

            mainNode.ChildNodes.Add(discountCouponCodeGeneratorNode);

            var ecorenewNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Ecorenew");
            if (ecorenewNode != null)
                ecorenewNode.ChildNodes.Add(discountCouponCodeGeneratorNode);
            else
                rootNode.ChildNodes.Add(mainNode);
        }

        #endregion
    }
}
