﻿using System;
using System.Linq;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Helpers
{
    public class CouponCodeGenerator
    {
        public virtual string GenerateRandomCouponCode(int couponCodeLength = 6, string format = null)
        {
            if (couponCodeLength < 6)
                couponCodeLength = 6;

            Random random = new Random();

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string couponCode = new string(Enumerable.Repeat(chars, couponCodeLength)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            if (!string.IsNullOrWhiteSpace(format))
                couponCode = string.Format(format, couponCode);

            return couponCode;
        }
    }
}