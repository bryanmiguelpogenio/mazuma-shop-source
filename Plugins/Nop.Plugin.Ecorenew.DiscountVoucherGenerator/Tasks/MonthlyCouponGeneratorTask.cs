﻿using Ecorenew.MailChimpApi;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Messages;
using Nop.Core.Plugins;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Domain;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Helpers;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Services;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Tasks;
using System;
using System.Linq;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Tasks
{
    public class MonthlyCouponGeneratorTask : IScheduleTask
    {
        #region Fields

        private readonly IRepository<DiscountCouponGeneratorMessageRecord> _discountCouponGeneratorMessageRecordRepository;
        private readonly IRepository<DiscountRequirement> _discountRequirementRepository;
        private readonly IRepository<NewsLetterSubscription> _newsletterSubscriptionRepository;
        private readonly CouponCodeGenerator _couponCodeGenerator;
        private readonly ICustomDiscountService _customDiscountService;
        private readonly ICustomerService _customerService;
        private readonly ICustomWorkflowMessageService _customWorkflowMessageService;
        private readonly ICustomMessageTokenProvider _discountCouponGeneratorMessageTokenProvider;
        private readonly IDiscountService _discountService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILocalizationService _localizationService;
        private readonly ILogger _logger;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IPluginFinder _pluginFinder;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly MailChimpApiClient _mailChimpApiClient;
        private readonly DiscountCouponGeneratorSettings _discountCouponGeneratorSettings;

        #endregion

        #region Ctor
        public MonthlyCouponGeneratorTask(IRepository<DiscountCouponGeneratorMessageRecord> discountCouponGeneratorMessageRecordRepository,
            IRepository<DiscountRequirement> discountRequirementRepository,
            IRepository<NewsLetterSubscription> newsletterSubscriptionRepository,
            CouponCodeGenerator couponCodeGenerator,
            ICustomDiscountService customDiscountService,
            ICustomerService customerService,
            ICustomWorkflowMessageService customWorkflowMessageService, 
            ICustomMessageTokenProvider discountCouponGeneratorMessageTokenProvider,
            IDiscountService discountService,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            ILogger logger,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            IPluginFinder pluginFinder,
            IStoreContext storeContext,
            IWorkContext workContext,
            MailChimpApiClient mailChimpApiClient,
            DiscountCouponGeneratorSettings discountCouponGeneratorSettings)
        {
            this._discountCouponGeneratorMessageRecordRepository = discountCouponGeneratorMessageRecordRepository;
            this._discountRequirementRepository = discountRequirementRepository;
            this._newsletterSubscriptionRepository = newsletterSubscriptionRepository;
            this._couponCodeGenerator = couponCodeGenerator;
            this._customDiscountService = customDiscountService;
            this._customerService = customerService;
            this._customWorkflowMessageService = customWorkflowMessageService;
            this._discountCouponGeneratorMessageTokenProvider = discountCouponGeneratorMessageTokenProvider;
            this._discountService = discountService;
            this._eventPublisher = eventPublisher;
            this._localizationService = localizationService;
            this._logger = logger;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._pluginFinder = pluginFinder;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._discountCouponGeneratorSettings = discountCouponGeneratorSettings;

            this._mailChimpApiClient = mailChimpApiClient;
            this._mailChimpApiClient.Initialize(_discountCouponGeneratorSettings.MailChimpApiKey);
        }
        #endregion

        #region Method
        public void Execute()
        {
            // check if the plugin is installed and enabled for the current store
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(DiscountCouponGeneratorConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
            {
                _logger.Error("Plugin Discount Coupon Generator is not yet installed.");
                return;
            }

            // check if the setting for monthly coupon event discount is enabled
            if (!_discountCouponGeneratorSettings.MonthlyCouponEventDiscountEnabled)
                return;

            var utcNow = DateTime.UtcNow;

            // check if we already have sent messages for today
            if (_discountCouponGeneratorMessageRecordRepository
                .TableNoTracking
                .Any(x => x.DiscountCouponGeneratorMessageTypeId == (int)DiscountCouponGeneratorMessageType.Monthly
                    && x.CreatedOnUtc.Month == utcNow.Month))
            {
                // no need to send any if we already did
                return;
            }

            var discountOver100 = _customDiscountService.CreateRandomCouponOrderTotalDiscount(
                DiscountCouponGeneratorConstants.Over100MonthlyCouponEventCouponCodeName,
                _discountCouponGeneratorSettings.MonthlyCouponEventDiscountUsePercentage,
                _discountCouponGeneratorSettings.Over100MonthlyCouponEventDiscountValue,
                _discountCouponGeneratorSettings.MonthlyCouponEventRandomCodeLength,
                _discountCouponGeneratorSettings.MonthlyCouponEventFormat,
                _discountCouponGeneratorSettings.MonthlyCouponEventDiscountDaysToExpire,
                _discountCouponGeneratorSettings.MonthlyCouponEventDiscountCumulative,
                DiscountLimitationType.NTimesPerCustomer,
                1,
                null,
                100);

            var discountOver200 = _customDiscountService.CreateRandomCouponOrderTotalDiscount(
                DiscountCouponGeneratorConstants.Over200MonthlyCouponEventCouponCodeName,
                _discountCouponGeneratorSettings.MonthlyCouponEventDiscountUsePercentage,
                _discountCouponGeneratorSettings.Over200MonthlyCouponEventDiscountValue,
                _discountCouponGeneratorSettings.MonthlyCouponEventRandomCodeLength,
                _discountCouponGeneratorSettings.MonthlyCouponEventFormat,
                _discountCouponGeneratorSettings.MonthlyCouponEventDiscountDaysToExpire,
                _discountCouponGeneratorSettings.MonthlyCouponEventDiscountCumulative,
                DiscountLimitationType.NTimesPerCustomer,
                1,
                null,
                200);

            var discountOver500 = _customDiscountService.CreateRandomCouponOrderTotalDiscount(
                DiscountCouponGeneratorConstants.Over500MonthlyCouponEventCouponCodeName,
                _discountCouponGeneratorSettings.MonthlyCouponEventDiscountUsePercentage,
                _discountCouponGeneratorSettings.Over500MonthlyCouponEventDiscountValue,
                _discountCouponGeneratorSettings.MonthlyCouponEventRandomCodeLength,
                _discountCouponGeneratorSettings.MonthlyCouponEventFormat,
                _discountCouponGeneratorSettings.MonthlyCouponEventDiscountDaysToExpire,
                _discountCouponGeneratorSettings.MonthlyCouponEventDiscountCumulative,
                DiscountLimitationType.NTimesPerCustomer,
                1,
                null,
                500);

            if (_discountCouponGeneratorSettings.MonthlyCouponEventUseMailChimpList && !string.IsNullOrWhiteSpace(_discountCouponGeneratorSettings.MailChimpApiKey))
            {
                foreach (var listId in _discountCouponGeneratorSettings.MonthlyCouponEventMailChimpListIds.Split(','))
                {
                    // get total count members first
                    var totalSubscribers = _mailChimpApiClient.GetSubscribedListMembers(listId.Trim(), 1).TotalItems;
                    if (totalSubscribers > 0)
                    {
                        int pageSize = 1000;
                        int pages = totalSubscribers / pageSize;
                        if (totalSubscribers % pageSize > 0)
                            pages++;

                        for (int i = 0; i < pages; i++)
                        {
                            var list = _mailChimpApiClient.GetSubscribedListMembers(
                                listId.Trim(),
                                pageSize,
                                i * pageSize,
                                new string[]
                                {
                                    "members.email_address",
                                    "members.merge_fields.fname",
                                    "members.merge_fields.lname"
                                });


                            // send messages to each
                            foreach (var member in list.Members)
                            {
                                // check if we already have just sent an email to the address
                                if (_discountCouponGeneratorMessageRecordRepository
                                        .TableNoTracking
                                        .Any(x => x.DiscountCouponGeneratorMessageTypeId == (int)DiscountCouponGeneratorMessageType.Monthly
                                            && x.CreatedOnUtc.Month == utcNow.Month
                                            && x.EmailAddress == member.EmailAddress))
                                {
                                    continue;
                                }

                                _customWorkflowMessageService.SendCustomerMonthlyCouponMessage(
                                    member.EmailAddress,
                                    $"{member.MergeFields.FirstName} {member.MergeFields.LastName}",
                                    _workContext.WorkingLanguage.Id,
                                    discountOver100,
                                    discountOver200,
                                    discountOver500);
                            }
                        }
                    }
                }
            }
            else
            {
                var pageSize = 1000;
                var subscribers = _newsLetterSubscriptionService.GetAllNewsLetterSubscriptions(isActive: true, pageSize: pageSize);

                for (int i = 0; i < subscribers.TotalPages; i++)
                {
                    foreach (var subscriber in subscribers)
                    {
                        // check if we already have just sent an email to the address
                        if (_discountCouponGeneratorMessageRecordRepository
                                .TableNoTracking
                                .Any(x => x.DiscountCouponGeneratorMessageTypeId == (int)DiscountCouponGeneratorMessageType.Monthly
                                    && x.CreatedOnUtc.Month == utcNow.Month
                                    && x.EmailAddress == subscriber.Email))
                        {
                            continue;
                        }

                        _customWorkflowMessageService.SendCustomerMonthlyCouponMessage(
                            subscriber.Email,
                            subscriber.Email,
                            _workContext.WorkingLanguage.Id,
                            discountOver100,
                            discountOver200,
                            discountOver500);
                    }

                    subscribers = _newsLetterSubscriptionService.GetAllNewsLetterSubscriptions(
                        isActive: true,
                        pageSize: pageSize,
                        pageIndex: i + 1);
                }
            }

            _logger.Information("Monthly Coupon Event Schedule Task executed successfully.");
        }

        #endregion
    }
}
