﻿using Nop.Core.Plugins;
using Nop.Services.Discounts;
using Nop.Services.Tasks;
using Nop.Services.Logging;
using Nop.Core;
using Nop.Services.Events;
using System.Linq;
using Nop.Services.Customers;
using System;
using Nop.Core.Data;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Domain;
using Nop.Plugin.Ecorenew.DiscountCouponGenerator.Services;
using Nop.Services.Helpers;
using Nop.Core.Domain.Discounts;
using Ecorenew.MailChimpApi;
using System.Data.Entity;
using Nop.Core.Domain.Messages;

namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator.Tasks
{
    class CustomerBirthdayMessageGeneratorTask : IScheduleTask
    {
        #region Fields
        
        private readonly IRepository<DiscountCouponGeneratorMessageRecord> _discountCouponGeneratorMessageRecordRepository;
        private readonly IRepository<NewsLetterSubscription> _newsletterSubscriptionRepository;
        private readonly ICustomDiscountService _customDiscountService;
        private readonly ICustomerService _customerService;
        private readonly ICustomWorkflowMessageService _customWorkflowMessageService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IDiscountService _discountService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILogger _logger;
        private readonly IPluginFinder _pluginFinder;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly MailChimpApiClient _mailChimpApiClient;
        private readonly DiscountCouponGeneratorSettings _discountCouponGeneratorSettings;

        #endregion

        #region Ctor
        public CustomerBirthdayMessageGeneratorTask(IRepository<DiscountCouponGeneratorMessageRecord> discountCouponGeneratorMessageRecordRepository,
            IRepository<NewsLetterSubscription> newsletterSubscriptionRepository,
            ICustomDiscountService customDiscountService,
            ICustomerService customerService,
            ICustomWorkflowMessageService customWorkflowMessageService,
            IDateTimeHelper dateTimeHelper,
            IDiscountService discountService,
            IEventPublisher eventPublisher,
            ILogger logger,
            IPluginFinder pluginFinder,
            IStoreContext storeContext,
            IWorkContext workContext,
            MailChimpApiClient mailChimpApiClient,
            DiscountCouponGeneratorSettings discountCouponGeneratorSettings)
        {
            this._discountCouponGeneratorMessageRecordRepository = discountCouponGeneratorMessageRecordRepository;
            this._newsletterSubscriptionRepository = newsletterSubscriptionRepository;
            this._customDiscountService = customDiscountService;
            this._customerService = customerService;
            this._customWorkflowMessageService = customWorkflowMessageService;
            this._dateTimeHelper = dateTimeHelper;
            this._discountService = discountService;
            this._eventPublisher = eventPublisher;
            this._logger = logger;
            this._pluginFinder = pluginFinder;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._discountCouponGeneratorSettings = discountCouponGeneratorSettings;

            this._mailChimpApiClient = mailChimpApiClient;
            this._mailChimpApiClient.Initialize(_discountCouponGeneratorSettings.MailChimpApiKey);
        }
        #endregion

        #region Utilities

        protected virtual Discount CreateBirthdayDiscount(decimal priceOver, string discountName)
        {
            return _customDiscountService.CreateRandomCouponOrderTotalDiscount(
                discountName,
                _discountCouponGeneratorSettings.CustomerBirthdayEventDiscountUsePercentage,
                _discountCouponGeneratorSettings.Over200CustomerBirthdayEventDiscountValue,
                _discountCouponGeneratorSettings.CustomerBirthdayEventRandomCodeLength,
                _discountCouponGeneratorSettings.CustomerBirthdayEventFormat,
                _discountCouponGeneratorSettings.CustomerBirthdayEventDiscountDaysToExpire,
                _discountCouponGeneratorSettings.CustomerBirthdayEventDiscountCumulative,
                DiscountLimitationType.NTimesPerCustomer,
                1,
                null,
                priceOver);
        }

        #endregion

        #region Method

        public void Execute()
        {
            // check if the plugin is installed and enabled for the current store
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(DiscountCouponGeneratorConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
            {
                _logger.Error("Plugin Discount Coupon Generator is not yet installed.");
                return;
            }

            // check if the setting for customer birthday message event discount is enabled
            if (!_discountCouponGeneratorSettings.CustomerBirthdayEventDiscountEnabled)
                return;
            
            // check if we already have sent messages for today
            if (_discountCouponGeneratorMessageRecordRepository
                .TableNoTracking
                .Any(x => x.DiscountCouponGeneratorMessageTypeId == (int)DiscountCouponGeneratorMessageType.Birthday
                    && DbFunctions.TruncateTime(x.CreatedOnUtc) == DbFunctions.TruncateTime(DateTime.UtcNow)))
            {
                // no need to send any if we already did
                return;
            }

            var utcNow = DateTime.UtcNow;

            if (_discountCouponGeneratorSettings.CustomerBirthdayEventUseMailChimpList && !string.IsNullOrWhiteSpace(_discountCouponGeneratorSettings.MailChimpApiKey))
            {
                var discountOver200 = CreateBirthdayDiscount(200, DiscountCouponGeneratorConstants.Over200CustomerBirthdayEventCouponCodeName);
                var discountOver1000 = CreateBirthdayDiscount(1000, DiscountCouponGeneratorConstants.Over1000CustomerBirthdayEventCouponCodeName);

                foreach (var listId in _discountCouponGeneratorSettings.CustomerBirthdayEventMailChimpListIds.Split(','))
                {
                    // get total count members first
                    var totalSubscribers = _mailChimpApiClient.GetSubscribedListMembers(listId.Trim(), 1).TotalItems;
                    if (totalSubscribers > 0)
                    {
                        int pageSize = 1000;
                        int pages = totalSubscribers / pageSize;
                        if (totalSubscribers % pageSize > 0)
                            pages++;

                        for (int i = 0; i < pages; i++)
                        {
                            var list = _mailChimpApiClient.GetSubscribedListMembers(
                                listId.Trim(),
                                pageSize,
                                i * pageSize,
                                new string[]
                                {
                                    "members.email_address",
                                    "members.merge_fields.fname",
                                    "members.merge_fields.lname",
                                    "members.merge_fields.birthday"
                                });

                            // get all with those with birthdays today
                            var withBirthdaysToday = list.Members
                                .Where(x => x.MergeFields.Birthday != null
                                    && x.MergeFields.Birthday == utcNow.ToString("MM/dd"));

                            // send messages to each
                            foreach (var withBirthdayToday in withBirthdaysToday)
                            {
                                // check if we already have just sent an email to the address
                                if (_discountCouponGeneratorMessageRecordRepository
                                        .TableNoTracking
                                        .Any(x => x.DiscountCouponGeneratorMessageTypeId == (int)DiscountCouponGeneratorMessageType.Birthday
                                            && DbFunctions.TruncateTime(x.CreatedOnUtc) == DbFunctions.TruncateTime(DateTime.UtcNow)
                                            && x.EmailAddress == withBirthdayToday.EmailAddress))
                                {
                                    continue;
                                }

                                _customWorkflowMessageService.SendCustomerBirthdayMessage(
                                    withBirthdayToday.EmailAddress,
                                    $"{withBirthdayToday.MergeFields.FirstName} {withBirthdayToday.MergeFields.LastName}",
                                    _workContext.WorkingLanguage.Id,
                                    discountOver200,
                                    discountOver1000);
                            }
                        }
                    }
                }
            }
            else
            {
                var pageSize = 1000;
                var customers = _customerService.GetAllCustomers(dayOfBirth: utcNow.Day, monthOfBirth: utcNow.Month,
                    pageSize: pageSize);

                if (customers.TotalCount > 0)
                {
                    var discountOver200 = CreateBirthdayDiscount(200, DiscountCouponGeneratorConstants.Over200CustomerBirthdayEventCouponCodeName);
                    var discountOver1000 = CreateBirthdayDiscount(1000, DiscountCouponGeneratorConstants.Over1000CustomerBirthdayEventCouponCodeName);

                    for (int i = 0; i < customers.TotalPages; i++)
                    {
                        var customerEmailAddresses = customers.Select(x => x.Email).ToList();
                        var subscribedEmailAddresses = _newsletterSubscriptionRepository.TableNoTracking
                            .Where(x => customerEmailAddresses.Contains(x.Email)
                                && x.Active)
                            .Select(x => x.Email)
                            .ToList();

                        var subscribedCustomers = customers.Where(x => subscribedEmailAddresses.Contains(x.Email));

                        foreach (var subscribedCustomer in subscribedCustomers)
                        {
                            // check if we already have just sent an email to the address
                            if (_discountCouponGeneratorMessageRecordRepository
                                    .TableNoTracking
                                    .Any(x => x.DiscountCouponGeneratorMessageTypeId == (int)DiscountCouponGeneratorMessageType.Birthday
                                        && DbFunctions.TruncateTime(x.CreatedOnUtc) == DbFunctions.TruncateTime(DateTime.UtcNow)
                                        && x.EmailAddress == subscribedCustomer.Email))
                            {
                                continue;
                            }

                            _customWorkflowMessageService.SendCustomerBirthdayMessage(subscribedCustomer.Email,
                                subscribedCustomer.GetFullName(),
                                _workContext.WorkingLanguage.Id,
                                discountOver200,
                                discountOver1000);
                        }

                        customers = _customerService.GetAllCustomers(dayOfBirth: utcNow.Day, monthOfBirth: utcNow.Month,
                            pageIndex: i + 1, pageSize: pageSize);
                    }
                }
            }

            //log information about the successful customer birthday message event
            _logger.Information("Customer Birthday Event Schedule Task executed successfully.");
        }

        #endregion
    }
}
