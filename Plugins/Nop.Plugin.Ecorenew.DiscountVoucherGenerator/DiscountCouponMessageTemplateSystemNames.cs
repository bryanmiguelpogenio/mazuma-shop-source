﻿namespace Nop.Plugin.Ecorenew.DiscountCouponGenerator
{
    public static partial class DiscountCouponMessageTemplateSystemNames
    {
        #region Customer
        
        public const string CustomerBirthdayMessage = "DiscountCouponGenerator.CustomerBirthday";
        public const string MonthlyCouponMessage = "DiscountCouponGenerator.Monthly";
        public const string AbandonedCartMessage = "DiscountCouponGenerator.AbandonedCart";

        #endregion
    }
}
