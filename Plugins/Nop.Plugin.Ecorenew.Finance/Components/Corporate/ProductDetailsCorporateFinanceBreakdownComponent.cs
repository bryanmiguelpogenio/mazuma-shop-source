﻿using Microsoft.AspNetCore.Mvc;

namespace Nop.Plugin.Ecorenew.Finance.Components.Corporate
{
    [ViewComponent(Name = "ProductDetailsCorporateFinanceBreakdown")]
    public class ProductDetailsCorporateFinanceBreakdownComponent : BaseCorporateFinanceViewComponent
    {
        public IViewComponentResult Invoke(int additionalData)
        {
            return View(additionalData);
        }
    }
}