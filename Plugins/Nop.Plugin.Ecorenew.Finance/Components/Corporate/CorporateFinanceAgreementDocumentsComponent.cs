﻿using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Microsoft.AspNetCore.Mvc;

namespace Nop.Plugin.Ecorenew.Finance.Components.Corporate
{
    [ViewComponent(Name = "CorporateFinanceAgreementDocuments")]
    public class CorporateFinanceAgreementDocumentsComponent : BaseCorporateFinanceViewComponent
    {
        #region Fields

        private readonly ICorporateFinanceService _corporateFinanceService;

        #endregion

        #region Ctor

        public CorporateFinanceAgreementDocumentsComponent(ICorporateFinanceService corporateFinanceService)
        {
            this._corporateFinanceService = corporateFinanceService;
        }

        #endregion

        #region Methods

        // additionalData = OrderId
        public IViewComponentResult Invoke(int additionalData)
        {
            var url = _corporateFinanceService.GetOrderCorporateFinanceAgreementDocumentsLink(additionalData);

            return View(model: url ?? "");
        }

        #endregion
    }
}