﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Ecorenew.Finance.Models.Corporate.Components;

namespace Nop.Plugin.Ecorenew.Finance.Components.Corporate
{
    [ViewComponent(Name = "CorporateFinanceBreakdownPopup")]
    public class CorporateFinanceBreakdownPopupComponent : BaseCorporateFinanceViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var model = new CorporateFinanceBreakdownPopupComponentModel();
            
            return View(model);
        }
    }
}