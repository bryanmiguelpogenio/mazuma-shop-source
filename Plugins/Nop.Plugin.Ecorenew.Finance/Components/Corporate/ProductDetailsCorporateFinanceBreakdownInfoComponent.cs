﻿using Microsoft.AspNetCore.Mvc;

namespace Nop.Plugin.Ecorenew.Finance.Components.Corporate
{
    [ViewComponent(Name = "ProductDetailsCorporateFinanceBreakdownInfo")]
    public class ProductDetailsCorporateFinanceBreakdownInfoComponent : BaseCorporateFinanceViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}