﻿using Ecorenew.Common.DocuSign.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Plugin.Ecorenew.Finance.Models.Corporate.Components;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using System.Linq;

namespace Nop.Plugin.Ecorenew.Finance.Components.Corporate
{
    [ViewComponent(Name = "CorporatePaymentTermSelector")]
    public class CorporatePaymentTermSelectorComponent : BaseCorporateFinanceViewComponent
    {
        private readonly ICorporateFinanceService _corporateFinanceService;
        private readonly IDocuSignService _docuSignService;
        private readonly IStoreContext _storeContext;

        public CorporatePaymentTermSelectorComponent(ICorporateFinanceService corporateFinanceService,
            IDocuSignService docuSignService,
            IStoreContext storeContext)
        {
            this._corporateFinanceService = corporateFinanceService;
            this._docuSignService = docuSignService;
            this._storeContext = storeContext;
        }

        public IViewComponentResult Invoke()
        {
            var model = new CorporatePaymentModeSelectorComponentModel();

            // TODO localization

            var signup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();
            if (signup == null)
            {
                model.ReadOnly = false;
                model.ShowSignupLink = true;
            }
            else
            {
                if (signup.SignupCompletedOnUtc.HasValue)
                {
                    if (signup.IsApproved.HasValue && !signup.IsApproved.Value)
                    {
                        model.ReadOnly = false;
                        model.ShowSignupLink = true;
                    }
                    else
                    {
                        if (signup.OrderId.HasValue)
                        {
                            model.ReadOnly = false;
                            model.ShowSignupLink = true;
                        }
                        else if (_corporateFinanceService.HasPendingDocuSignEnvelope())
                        {
                            model.ReadOnly = true;
                            model.ShowSignupLink = false;

                            var directorDetails = _corporateFinanceService.GetSignupDirectorDetails(signup);

                            model.DocuSignToken = _docuSignService.RequestRecipientToken(
                                signup.DocuSignEnvelopeId,
                                signup.CustomerId.ToString(),
                                string.Join(" ", directorDetails.FirstName, directorDetails.LastName),
#if DEBUG
                            "nplusterio@ecoasiatech.com",
#else
                            directorDetails.EmailAddress,
#endif
                            _storeContext.CurrentStore.Id);
                        }
                        else
                        {
                            model.ReadOnly = true;
                        }
                    }
                }
                else
                {
                    model.ReadOnly = false;
                    model.ShowSignupLink = true;
                }
            }

            var existingFinanceOptionMappings = _corporateFinanceService.GetCustomerShoppingCartItemCorporateFinanceOptions();

            if (!existingFinanceOptionMappings.Any())
            {
                model.SelectedPaymentModeId = (int)CorporatePaymentMode.PayNow;
                model.ShowSignupLink = false;
                model.DocuSignToken = "";
            }
            else
            {
                model.SelectedPaymentModeId = (int)CorporatePaymentMode.PayMonthly;
                model.SelectedCorporateFinanceOptionId = existingFinanceOptionMappings.First().CorporateFinanceOptionId;
            }

            model.PaymentModes.Add(new SelectListItem
            {
                Text = "Pay now",
                Value = ((int)CorporatePaymentMode.PayNow).ToString(),
                Selected = model.SelectedPaymentModeId == (int)CorporatePaymentMode.PayNow
            });
            model.PaymentModes.Add(new SelectListItem
            {
                Text = "Pay monthly",
                Value = ((int)CorporatePaymentMode.PayMonthly).ToString(),
                Selected = model.SelectedPaymentModeId == (int)CorporatePaymentMode.PayMonthly
            });

            _corporateFinanceService.GetCorporateFinanceOptions()
                .ToList()
                .ForEach(o => model.CorporateFinanceOptions.Add(new SelectListItem
                {
                    Text = o.Description,
                    Value = o.Id.ToString(),
                    Selected = model.SelectedCorporateFinanceOptionId == o.Id
                }));

            return View(model);
        }
    }
}