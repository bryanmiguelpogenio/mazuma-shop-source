﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Ecorenew.Finance.Components
{
    [ViewComponent(Name = "EcorenewFinanceEmpty")]
    public class EcorenewFinanceEmptyComponent : NopViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Plugins/Ecorenew.Finance/Views/Components/EcorenewFinanceEmpty/Default.cshtml");
        }
    }
}