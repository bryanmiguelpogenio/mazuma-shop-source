﻿using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Microsoft.AspNetCore.Mvc;

namespace Nop.Plugin.Ecorenew.Finance.Components.Consumer
{
    [ViewComponent(Name = "ConsumerFinanceAgreementDocuments")]
    public class ConsumerFinanceAgreementDocumentsComponent : BaseConsumerFinanceViewComponent
    {
        #region Fields

        private readonly IConsumerFinanceService _consumerFinanceService;

        #endregion

        #region Ctor

        public ConsumerFinanceAgreementDocumentsComponent(IConsumerFinanceService consumerFinanceService)
        {
            this._consumerFinanceService = consumerFinanceService;
        }

        #endregion

        #region Methods

        // additionalData = OrderId
        public IViewComponentResult Invoke(int additionalData)
        {
            var url = _consumerFinanceService.GetOrderConsumerFinanceAgreementDocumentsLink(additionalData);

            return View(model: url ?? "");
        }

        #endregion
    }
}