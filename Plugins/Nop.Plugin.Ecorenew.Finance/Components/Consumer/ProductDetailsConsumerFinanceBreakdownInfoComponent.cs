﻿using Microsoft.AspNetCore.Mvc;

namespace Nop.Plugin.Ecorenew.Finance.Components.Consumer
{
    [ViewComponent(Name = "ProductDetailsConsumerFinanceBreakdownInfo")]
    public class ProductDetailsConsumerFinanceBreakdownInfoComponent : BaseConsumerFinanceViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}