﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Ecorenew.Finance.Models.Consumer.Components;

namespace Nop.Plugin.Ecorenew.Finance.Components.Consumer
{
    [ViewComponent(Name = "ConsumerFinanceBreakdownPopup")]
    public class ConsumerFinanceBreakdownPopupComponent : BaseConsumerFinanceViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var model = new ConsumerFinanceBreakdownPopupComponentModel();
            
            return View(model);
        }
    }
}