﻿using Microsoft.AspNetCore.Mvc;

namespace Nop.Plugin.Ecorenew.Finance.Components.Consumer
{
    [ViewComponent(Name = "ProductDetailsConsumerFinanceBreakdown")]
    public class ProductDetailsConsumerFinanceBreakdownComponent : BaseConsumerFinanceViewComponent
    {
        public IViewComponentResult Invoke(int additionalData)
        {
            return View(additionalData);
        }
    }
}