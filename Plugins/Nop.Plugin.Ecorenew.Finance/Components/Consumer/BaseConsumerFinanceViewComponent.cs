﻿using Microsoft.AspNetCore.Mvc.ViewComponents;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Web.Framework.Components;
using Nop.Web.Framework.Themes;
using System;

namespace Nop.Plugin.Ecorenew.Finance.Components.Consumer
{
    public abstract class BaseConsumerFinanceViewComponent : NopViewComponent
    {
        private const string PLUGIN_URL = "~/Plugins/Ecorenew.Finance";

        /// <summary>
        /// Returns a result which will render the partial view with name <paramref name="viewName"/>.
        /// </summary>
        /// <param name="viewName">The name of the partial view to render.</param>
        /// <param name="model">The model object for the view.</param>
        /// <returns>A <see cref="ViewViewComponentResult"/>.</returns>
        public new ViewViewComponentResult View<TModel>(string viewName, TModel model)
        {
            if (viewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return base.View(viewName, model);
            }
            else
            {
                viewName = viewName.Replace(".cshtml", "").TrimStart('/');

                var viewComponentName = ViewComponentContext.ViewComponentDescriptor.FullName;
                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/Components/{viewComponentName}/{viewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return base.View(themeViewName, model);

                return base.View($"{PLUGIN_URL}/Views/Consumer/Components/{viewComponentName}/{viewName}.cshtml", model);
            }
        }

        /// <summary>
        /// Returns a result which will render the partial view
        /// </summary>
        /// <param name="model">The model object for the view.</param>
        /// <returns>A <see cref="ViewViewComponentResult"/>.</returns>
        public new ViewViewComponentResult View<TModel>(TModel model)
        {
            var viewComponentName = ViewComponentContext.ViewComponentDescriptor.FullName;
            var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

            var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/Components/{viewComponentName}/Default.cshtml";

            if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                return base.View(themeViewName, model);

            return base.View($"{PLUGIN_URL}/Views/Consumer/Components/{viewComponentName}/Default.cshtml", model);
        }

        /// <summary>
        ///  Returns a result which will render the partial view with name viewName
        /// </summary>
        /// <param name="viewName">The name of the partial view to render.</param>
        /// <returns>A <see cref="ViewViewComponentResult"/>.</returns>
        public new ViewViewComponentResult View(string viewName)
        {
            if (viewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return base.View(viewName);
            }
            else
            {
                viewName = viewName.Replace(".cshtml", "").TrimStart('/');

                var viewComponentName = ViewComponentContext.ViewComponentDescriptor.FullName;
                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/Components/{viewComponentName}/{viewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return base.View(themeViewName);

                return base.View($"{PLUGIN_URL}/Views/Consumer/Components/{viewComponentName}/{viewName}.cshtml");
            }
        }

        /// <summary>
        /// Returns a result which will render the partial view
        /// </summary>
        /// <returns>A <see cref="ViewViewComponentResult"/>.</returns>
        public new ViewViewComponentResult View()
        {
            var viewComponentName = ViewComponentContext.ViewComponentDescriptor.FullName;
            var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

            var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/Components/{viewComponentName}/Default.cshtml";

            if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                return base.View(themeViewName);

            return base.View($"{PLUGIN_URL}/Views/Consumer/Components/{viewComponentName}/Default.cshtml");
        }
    }
}