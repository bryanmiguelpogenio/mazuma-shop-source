﻿using Ecorenew.Common.DocuSign.Services;
using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Plugin.Ecorenew.Finance.Models.Consumer.Components;
using System.Linq;

namespace Nop.Plugin.Ecorenew.Finance.Components.Consumer
{
    [ViewComponent(Name = "ConsumerPaymentTermSelector")]
    public class ConsumerPaymentTermSelectorComponent : BaseConsumerFinanceViewComponent
    {
        private readonly IConsumerFinanceService _consumerFinanceService;
        private readonly IDocuSignService _docuSignService;
        private readonly IStoreContext _storeContext;

        public ConsumerPaymentTermSelectorComponent(IConsumerFinanceService consumerFinanceService,
            IDocuSignService docuSignService,
            IStoreContext storeContext)
        {
            this._consumerFinanceService = consumerFinanceService;
            this._docuSignService = docuSignService;
            this._storeContext = storeContext;
        }

        public IViewComponentResult Invoke()
        {
            var model = new ConsumerPaymentModeSelectorComponentModel();

            // TODO localization

            var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
            if (signup == null)
            {
                model.ReadOnly = false;
                model.ShowSignupLink = true;
            }
            else
            {
                if (signup.SignupCompletedOnUtc.HasValue && signup.AllChecksPassed)
                {
                    if (signup.OrderId.HasValue)
                    {
                        model.ReadOnly = false;
                        model.ShowSignupLink = true;
                    }
                    else if (_consumerFinanceService.HasPendingDocuSignEnvelope())
                    {
                        model.ReadOnly = true;
                        model.ShowSignupLink = false;

                        var personalInformation = _consumerFinanceService.GetSignupPersonalInformation(signup);

                        model.DocuSignToken = _docuSignService.RequestRecipientToken(
                            signup.DocuSignEnvelopeId,
                            signup.CustomerId.ToString(),
                            string.Join(" ", personalInformation.FirstName, personalInformation.LastName),
                            personalInformation.Email,
                            _storeContext.CurrentStore.Id);
                    }
                    else
                    {
                        model.ReadOnly = true;
                    }
                }
                else
                {
                    model.ReadOnly = false;
                    model.ShowSignupLink = true;
                }
            }

            var existingFinanceOptionMappings = _consumerFinanceService.GetCustomerShoppingCartItemConsumerFinanceOptions();

            if (!existingFinanceOptionMappings.Any())
            {
                model.SelectedPaymentModeId = (int)ConsumerPaymentMode.PayNow;
                model.ShowSignupLink = false;
                model.DocuSignToken = "";
            }
            else
            {
                model.SelectedPaymentModeId = (int)ConsumerPaymentMode.PayMonthly;
                model.SelectedConsumerFinanceOptionId = existingFinanceOptionMappings.First().ConsumerFinanceOptionId;
            }

            model.PaymentModes.Add(new SelectListItem
            {
                Text = "Pay now",
                Value = ((int)ConsumerPaymentMode.PayNow).ToString(),
                Selected = model.SelectedPaymentModeId == (int)ConsumerPaymentMode.PayNow
            });
            model.PaymentModes.Add(new SelectListItem
            {
                Text = "Pay monthly",
                Value = ((int)ConsumerPaymentMode.PayMonthly).ToString(),
                Selected = model.SelectedPaymentModeId == (int)ConsumerPaymentMode.PayMonthly
            });

            _consumerFinanceService.GetConsumerFinanceOptions()
                .ToList()
                .ForEach(o => model.ConsumerFinanceOptions.Add(new SelectListItem
                {
                    Text = o.Description,
                    Value = o.Id.ToString(),
                    Selected = model.SelectedConsumerFinanceOptionId == o.Id
                }));

            return View(model);
        }
    }
}