﻿namespace Nop.Plugin.Ecorenew.Finance.Areas.Admin.Models.EcorenewFinanceAdmin
{
    public class CorporateFinanceSignupOrderedItemModel
    {
        public string ProductSku { get; set; }

        public string ProductName { get; set; }

        public string PaymentTerm { get; set; }

        public int Quantity { get; set; }

        public string PaymentPerUnit { get; set; }

        public string PaymentSubTotal { get; set; }

        public string TotalPayable { get; set; }
    }
}