﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;

namespace Nop.Plugin.Ecorenew.Finance.Areas.Admin.Models.EcorenewFinanceAdmin
{
    public class SettingsModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        public int? FinanceTypeId { get; set; }
        [NopResourceDisplayName("Ecorenew.Finance.Admin.Settings.FinanceType")]
        public SelectList FinanceTypeValues { get; set; }
        public bool FinanceTypeId_OverrideForStore { get; set; }
    }
}