﻿using Nop.Core.Domain.Customers;
using Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup;
using Nop.Web.Framework.Mvc.Models;
using System;

namespace Nop.Plugin.Ecorenew.Finance.Areas.Admin.Models.EcorenewFinanceAdmin
{
    public class ViewUpdateCorporateFinanceSignupModel : BaseNopEntityModel
    {
        public BasicDetailsModel BasicDetails { get; set; }

        public DirectorDetailsModel DirectorDetails { get; set; }

        public BankModel BankDetails { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool? IsApproved { get; set; }

        public Customer ApprovalConductedByCustomer { get; set; }

        public DateTime? DateOfApproval { get; set; }
    }
}