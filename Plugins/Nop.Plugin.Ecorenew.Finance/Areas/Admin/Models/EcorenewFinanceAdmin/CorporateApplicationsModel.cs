﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Areas.Admin.Models.EcorenewFinanceAdmin
{
    public class CorporateApplicationsModel : BaseNopModel
    {
        public CorporateApplicationsModel()
        {
            this.AvailableApprovalStatuses = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Ecorenew.Finance.Admin.Corporate.Applications.ApprovalStatus")]
        public int[] SearchApprovalStatuses { get; set; }
        public IList<SelectListItem> AvailableApprovalStatuses { get; set; }
    }
}