﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Plugin.Ecorenew.Finance.Areas.Admin.Models.EcorenewFinanceAdmin;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using Nop.Plugin.Ecorenew.Finance.Extensions;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Nop.Services;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Stores;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Linq;

namespace Nop.Plugin.Ecorenew.Finance.Areas.Admin.Controllers
{
    public class EcorenewFinanceAdminController : BaseEcorenewFinanceAdminController
    {
        #region Fields

        private readonly ICorporateFinanceService _corporateFinanceService;
        private readonly ICountryService _countryService;
        private readonly ICurrencyService _currencyService;
        private readonly ICustomerService _customerService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IProductService _productService;
        private readonly ISettingService _settingService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public EcorenewFinanceAdminController(ICorporateFinanceService corporateFinanceService,
            ICountryService countryService,
            ICurrencyService currencyService,
            ICustomerService customerService,
            IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService,
            IPriceFormatter priceFormatter,
            IProductService productService,
            ISettingService settingService,
            IStateProvinceService stateProvinceService,
            IStoreService storeService,
            IWorkContext workContext)
        {
            this._corporateFinanceService = corporateFinanceService;
            this._countryService = countryService;
            this._currencyService = currencyService;
            this._customerService = customerService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._priceFormatter = priceFormatter;
            this._productService = productService;
            this._settingService = settingService;
            this._stateProvinceService = stateProvinceService;
            this._storeService = storeService;
            this._workContext = workContext;
        }

        #endregion

        #region Methods

        public IActionResult Settings()
        {
            // Load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var financeSettings = _settingService.LoadSetting<FinanceSettings>(storeScope);

            var model = new SettingsModel
            {
                ActiveStoreScopeConfiguration = storeScope,
                FinanceTypeId = (int)financeSettings.FinanceType,
                FinanceTypeValues = financeSettings.FinanceType.ToSelectList()
            };

            if (storeScope > 0)
            {
                model.FinanceTypeId_OverrideForStore = _settingService.SettingExists(financeSettings, x => x.FinanceType, storeScope);
            }

            return View(model);
        }

        [HttpPost]
        [AdminAntiForgery]
        public IActionResult Settings(SettingsModel model)
        {
            if (!ModelState.IsValid)
                return Settings();

            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var financeSettings = _settingService.LoadSetting<FinanceSettings>(storeScope);

            //save settings
            financeSettings.FinanceType = (FinanceType)model.FinanceTypeId;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */

            _settingService.SaveSettingOverridablePerStore(financeSettings, x => x.FinanceType, model.FinanceTypeId_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Settings();
        }

        public IActionResult CorporateApplications()
        {
            var model = new CorporateApplicationsModel();

            var approvalStatuses = CorporateFinanceSignupStatus.Unknown.ToSelectList();

            foreach (var approvalStatus in approvalStatuses)
            {
                model.AvailableApprovalStatuses.Add(
                    new SelectListItem
                    {
                        Text = approvalStatus.Text,
                        Value = approvalStatus.Value,
                        Selected = approvalStatus.Value == ((int)CorporateFinanceSignupStatus.Unknown).ToString()
                    });
            }

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult CorporateApplicationList(DataSourceRequest command, CorporateApplicationsModel model)
        {
            var approvalStatuses = model.SearchApprovalStatuses?.ToList();

            //load orders
            var signupRecords = _corporateFinanceService.GetCorporateFinanceSignupList(approvalStatuses: approvalStatuses,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = signupRecords.Select(x =>
                {
                    return new CorporateFinanceSignupListItem
                    {
                        Id = x.Id,
                        StoreName = x.StoreName ?? "Unknown",
                        ApprovalStatus = x.ApprovalStatus,
                        CompanyName = x.CompanyName,
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOn, DateTimeKind.Utc)
                    };
                }),
                Total = signupRecords.TotalCount
            };

            return Json(gridModel);
        }

        public virtual IActionResult ViewUpdateCorporateFinanceSignup(int id)
        {
            var signup = _corporateFinanceService.GetCorporateFinanceSignupById(id);

            if (signup == null || !signup.SignupCompletedOnUtc.HasValue)
                return CorporateApplications();

            var model = new ViewUpdateCorporateFinanceSignupModel
            {
                Id = signup.Id,
                BankDetails = _corporateFinanceService.GetSignupBankDetails(signup).ToModel(),
                BasicDetails = _corporateFinanceService.GetSignupBasicDetails(signup).ToModel(),
                CreatedOn = _dateTimeHelper.ConvertToUserTime(signup.CreatedOnUtc, DateTimeKind.Utc),
                DateOfApproval = signup.DateOfApprovalUtc.HasValue ? (DateTime?)_dateTimeHelper.ConvertToUserTime(signup.DateOfApprovalUtc.Value, DateTimeKind.Utc) : null,
                DirectorDetails = _corporateFinanceService.GetSignupDirectorDetails(signup).ToModel(),
                IsApproved = signup.IsApproved,
                ApprovalConductedByCustomer = (signup.IsApproved.HasValue && signup.ApprovalProcessConductedByCustomerId.HasValue)
                    ? _customerService.GetCustomerById(signup.ApprovalProcessConductedByCustomerId.Value)
                    : null
            };

            // Select lists
            #region Company Address

            var companyAddressCountry = _countryService.GetCountryById(model.BasicDetails.CompanyAddress.CountryId.Value);
            model.BasicDetails.CompanyAddress.CountrySelectList.Add(new SelectListItem
            {
                Text = companyAddressCountry.Name,
                Value = companyAddressCountry.Id.ToString()
            });
            var companyAddressStateProvince = _stateProvinceService.GetStateProvinceById(model.BasicDetails.CompanyAddress.StateProvinceId.Value);
            model.BasicDetails.CompanyAddress.StateProvinceSelectList.Add(new SelectListItem
            {
                Text = companyAddressStateProvince.Name,
                Value = companyAddressStateProvince.Id.ToString()
            });

            #endregion

            #region Trading Address

            if (!model.BasicDetails.TradingAddressSameAsCompanyAddress)
            {
                var tradingAddressCountry = _countryService.GetCountryById(model.BasicDetails.TradingAddress.CountryId.Value);
                model.BasicDetails.TradingAddress.CountrySelectList.Add(new SelectListItem
                {
                    Text = tradingAddressCountry.Name,
                    Value = tradingAddressCountry.Id.ToString()
                });
                var tradingAddressStateProvince = _stateProvinceService.GetStateProvinceById(model.BasicDetails.TradingAddress.StateProvinceId.Value);
                model.BasicDetails.TradingAddress.StateProvinceSelectList.Add(new SelectListItem
                {
                    Text = tradingAddressStateProvince.Name,
                    Value = tradingAddressStateProvince.Id.ToString()
                });
            }

            #endregion

            #region Director Details - Current Address

            var currentAddressCountry = _countryService.GetCountryById(model.DirectorDetails.CurrentAddress.CountryId.Value);
            model.DirectorDetails.CurrentAddress.CountrySelectList.Add(new SelectListItem
            {
                Text = currentAddressCountry.Name,
                Value = currentAddressCountry.Id.ToString()
            });
            var currentAddressStateProvince = _stateProvinceService.GetStateProvinceById(model.DirectorDetails.CurrentAddress.StateProvinceId.Value);
            model.DirectorDetails.CurrentAddress.StateProvinceSelectList.Add(new SelectListItem
            {
                Text = currentAddressStateProvince.Name,
                Value = currentAddressStateProvince.Id.ToString()
            });

            #endregion

            #region Director Details - Previous Address

            if (model.DirectorDetails.PreviousAddress.CountryId.HasValue && model.DirectorDetails.PreviousAddress.CountryId.Value > 0)
            {
                var previousAddressCountry = _countryService.GetCountryById(model.DirectorDetails.PreviousAddress.CountryId.Value);
                model.DirectorDetails.PreviousAddress.CountrySelectList.Add(new SelectListItem
                {
                    Text = previousAddressCountry.Name,
                    Value = previousAddressCountry.Id.ToString()
                });
            }

            if (model.DirectorDetails.PreviousAddress.StateProvinceId.HasValue && model.DirectorDetails.PreviousAddress.StateProvinceId.Value > 0)
            {
                var previousAddressStateProvince = _stateProvinceService.GetStateProvinceById(model.DirectorDetails.PreviousAddress.StateProvinceId.Value);
                model.DirectorDetails.PreviousAddress.StateProvinceSelectList.Add(new SelectListItem
                {
                    Text = previousAddressStateProvince.Name,
                    Value = previousAddressStateProvince.Id.ToString()
                });
            }

            #endregion

            #region Others

            _corporateFinanceService.GetAllMaritalStatuses().ToList()
                .ForEach(ms => model.DirectorDetails.MaritalStatusSelectList.Add(new SelectListItem
                {
                    Text = ms.Name,
                    Value = ms.Id.ToString()
                }));

            _corporateFinanceService.GetAllNumberOfDependents().ToList()
                .ForEach(nd => model.DirectorDetails.NumberOfDependentsSelectList.Add(new SelectListItem
                {
                    Text = nd.Name,
                    Value = nd.Id.ToString()
                }));

            _corporateFinanceService.GetAllHomeOwnerStatuses().ToList()
                .ForEach(ms => model.DirectorDetails.HomeOwnerStatusSelectList.Add(new SelectListItem
                {
                    Text = ms.Name,
                    Value = ms.Id.ToString()
                }));

            #endregion

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("approve", "isApproved")]
        public virtual IActionResult ViewUpdateCorporateFinanceSignup(ViewUpdateCorporateFinanceSignupModel model, bool isApproved)
        {
            _corporateFinanceService.SetCorporateFinanceSignupApprovalStatus(model.Id, isApproved ? CorporateFinanceSignupStatus.Approved : CorporateFinanceSignupStatus.Disapproved);

            return RedirectToAction("CorporateApplications");
        }

        [HttpPost]
        public virtual IActionResult CorporateFinanceSignupOrderedItems(DataSourceRequest command, ViewUpdateCorporateFinanceSignupModel model)
        {
            //load orders
            var signup = _corporateFinanceService.GetCorporateFinanceSignupById(model.Id);
            var orderedItems = new PagedList<CorporateFinanceOrderItem>(signup.OrderItems.ToList(), command.Page - 1, command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = orderedItems.Select(x =>
                {
                    var customerCurrency = _currencyService.GetCurrencyByCode(x.CurrencyCode);
                    var gbp = _currencyService.GetCurrencyByCode("GBP");
                    var monthlyPaymentGbp = _currencyService.ConvertCurrency(x.MonthlyPayment, customerCurrency, gbp);
                    var totalPayableGbp = _currencyService.ConvertCurrency(x.TotalPayable, customerCurrency, gbp);

                    return new CorporateFinanceSignupOrderedItemModel
                    {
                        ProductSku = x.ProductSku,
                        ProductName = _productService.GetProductById(x.ProductId)?.Name,
                        PaymentTerm = $"{x.DurationOfAgreement} Months",
                        Quantity = x.ProductQuantity,
                        PaymentPerUnit = _priceFormatter.FormatPrice(monthlyPaymentGbp, true, gbp),
                        PaymentSubTotal = _priceFormatter.FormatPrice(monthlyPaymentGbp * x.ProductQuantity, true, gbp),
                        TotalPayable = _priceFormatter.FormatPrice(totalPayableGbp * x.ProductQuantity, true, gbp)
                    };
                }),
                Total = orderedItems.TotalCount
            };

            return Json(gridModel);
        }

        #endregion
    }
}