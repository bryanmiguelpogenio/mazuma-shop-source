﻿using Autofac;
using Ecorenew.Common.CallCredit.Domain;
using Ecorenew.Common.CallCredit.Services;
using Ecorenew.Common.DocuSign.Services;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;
using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Plugin.Ecorenew.Finance.Controllers;
using Nop.Plugin.Ecorenew.Finance.Data;
using Nop.Plugin.Ecorenew.Finance.Extensions;
using Nop.Plugin.Ecorenew.Finance.Factories;
using Nop.Plugin.Ecorenew.Finance.Services;
using Nop.Services.Catalog;
using Nop.Services.Orders;
using Nop.Services.Tax;
using Nop.Web.Controllers;
using Nop.Web.Factories;
using Nop.Web.Framework.Infrastructure;
using System;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using Nop.Services.Common;

namespace Nop.Plugin.Ecorenew.Finance.Infrastructure
{
    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            #region Controllers, Factories & Services

            // NOP OVERRIDDEN CONTROLLERS, FACTORIES & SERVICES
            builder.RegisterService<EcorenewFinanceShoppingCartController, ShoppingCartController>();

            builder.RegisterService<FinanceCheckoutModelFactory, ICheckoutModelFactory>();
            builder.RegisterService<FinanceOrderModelFactory, IOrderModelFactory>();
            builder.RegisterService<FinanceProductModelFactory, IProductModelFactory>();
            builder.RegisterService<FinanceShoppingCartModelFactory, IShoppingCartModelFactory>();

            //builder.RegisterService<FinanceOrderProcessingService, IOrderProcessingService>();
            builder.RegisterService<FinancePdfService, IPdfService>();
            builder.RegisterService<FinancePriceCalculationService, IPriceCalculationService>();
            builder.RegisterService<FinanceShoppingCartService, IShoppingCartService>();
            builder.RegisterService<FinanceTaxService, ITaxService>();

            // COMMON SERVICES
            builder.RegisterService<CallReportService, ICallReportService>();
            builder.RegisterService<CallValidateService, ICallValidateService>();
            builder.RegisterService<DocuSignService, IDocuSignService>();

            // CONSUMER
            builder.RegisterService<ConsumerFinanceService, IConsumerFinanceService>();
            builder.RegisterService<ConsumerFinanceCalculationService, IConsumerFinanceCalculationService>();

            // CORPORATE
            builder.RegisterService<CorporateFinanceService, ICorporateFinanceService>();
            builder.RegisterService<CorporateFinanceCalculationService, ICorporateFinanceCalculationService>();
            builder.RegisterService<CorporateFinanceTokenProvider, ICorporateFinanceTokenProvider>();
            builder.RegisterService<CorporateFinanceWorkflowMessageService, ICorporateFinanceWorkflowMessageService>();

            #endregion

            // Data context
            const string CONTEXTNAME = "nop_object_context_ecorenew_finance";
            this.RegisterPluginDataContext<FinanceObjectContext>(builder, CONTEXTNAME);

            #region Repositories

            // Override required repository with our custom context
            // CONSUMER
            builder.RegisterRepository<AnnualGrossIncome>(CONTEXTNAME);
            builder.RegisterRepository<CallReportResult>(CONTEXTNAME);
            builder.RegisterRepository<CallValidateResult>(CONTEXTNAME);
            builder.RegisterRepository<ConsumerFinanceOption>(CONTEXTNAME);
            builder.RegisterRepository<ConsumerFinanceOrderItem>(CONTEXTNAME);
            builder.RegisterRepository<ConsumerFinanceSignup>(CONTEXTNAME);
            builder.RegisterRepository<EmploymentStatus>(CONTEXTNAME);
            builder.RegisterRepository<ExpenditureCategory>(CONTEXTNAME);
            builder.RegisterRepository<ExpenditureRange>(CONTEXTNAME);
            builder.RegisterRepository<ExpenditureCategoryRange>(CONTEXTNAME);
            builder.RegisterRepository<HomeOwnerStatus>(CONTEXTNAME);
            builder.RegisterRepository<MaritalStatus>(CONTEXTNAME);
            builder.RegisterRepository<NumberOfDependents>(CONTEXTNAME);
            builder.RegisterRepository<ShoppingCartItemConsumerFinanceOption>(CONTEXTNAME);

            // CORPORATE
            builder.RegisterRepository<CorporateFinanceOption>(CONTEXTNAME);
            builder.RegisterRepository<CorporateFinanceOrderItem>(CONTEXTNAME);
            builder.RegisterRepository<CorporateFinanceSignup>(CONTEXTNAME);
            builder.RegisterRepository<ShoppingCartItemCorporateFinanceOption>(CONTEXTNAME);

            builder.RegisterRepository<SkuCorporateFinanceRv>(CONTEXTNAME);

            #endregion
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return Int32.MaxValue - 1; }
        }
    }
}