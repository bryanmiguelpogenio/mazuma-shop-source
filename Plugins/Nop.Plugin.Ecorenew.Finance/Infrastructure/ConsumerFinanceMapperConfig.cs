﻿using AutoMapper;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;
using Nop.Core.Infrastructure.Mapper;
using Nop.Plugin.Ecorenew.Finance.Extensions;
using Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup;
using System;
using System.Linq;
using Nop.Plugin.Ecorenew.Finance.Models.Common;

namespace Nop.Plugin.Ecorenew.Finance.Infrastructure
{
    public class ConsumerFinanceMapperConfig : Profile, IMapperProfile
    {
        public ConsumerFinanceMapperConfig()
        {
            CreateMap<ConsumerFinancePersonalInformation, PersonalInformationModel>()
                .ForMember(dest => dest.ConfirmEmail, mo => mo.MapFrom(src => src.Email))
                .ForMember(dest => dest.DateOfBirthDay, mo => mo.MapFrom(src => src.DateOfBirth.Day))
                .ForMember(dest => dest.DateOfBirthMonth, mo => mo.MapFrom(src => src.DateOfBirth.Month))
                .ForMember(dest => dest.DateOfBirthYear, mo => mo.MapFrom(src => src.DateOfBirth.Year))
                .ForMember(dest => dest.Email, mo => mo.MapFrom(src => src.Email))
                .ForMember(dest => dest.FirstName, mo => mo.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, mo => mo.MapFrom(src => src.LastName))
                .ForMember(dest => dest.MaritalStatusId, mo => mo.MapFrom(src => src.MaritalStatusId))
                .ForMember(dest => dest.MiddleName, mo => mo.MapFrom(src => src.MiddleName))
                .ForMember(dest => dest.MobilePhoneNumber, mo => mo.MapFrom(src => src.MobilePhoneNumber))
                .ForMember(dest => dest.NumberOfDependentsId, mo => mo.MapFrom(src => src.NumberOfDependentsId))
                .ForMember(dest => dest.Title, mo => mo.MapFrom(src => src.Title));
            CreateMap<PersonalInformationModel, ConsumerFinancePersonalInformation>()
                .ForMember(dest => dest.DateOfBirth, mo => mo.MapFrom(src => new DateTime(src.DateOfBirthYear.Value, src.DateOfBirthMonth.Value, src.DateOfBirthDay.Value)))
                .ForMember(dest => dest.Email, mo => mo.MapFrom(src => src.Email))
                .ForMember(dest => dest.FirstName, mo => mo.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, mo => mo.MapFrom(src => src.LastName))
                .ForMember(dest => dest.MaritalStatusId, mo => mo.MapFrom(src => src.MaritalStatusId.Value))
                .ForMember(dest => dest.MiddleName, mo => mo.MapFrom(src => src.MiddleName))
                .ForMember(dest => dest.MobilePhoneNumber, mo => mo.MapFrom(src => src.MobilePhoneNumber))
                .ForMember(dest => dest.NumberOfDependentsId, mo => mo.MapFrom(src => src.NumberOfDependentsId))
                .ForMember(dest => dest.Password, mo => mo.MapFrom(src => src.Password))
                .ForMember(dest => dest.Title, mo => mo.MapFrom(src => src.Title));

            CreateMap<Address, AddressModel>()
                .ForMember(dest => dest.Address1, mo => mo.MapFrom(src => src.Address1))
                .ForMember(dest => dest.Address2, mo => mo.MapFrom(src => src.Address2))
                .ForMember(dest => dest.BuildingNumber, mo => mo.MapFrom(src => src.BuildingNumber))
                .ForMember(dest => dest.CityTown, mo => mo.MapFrom(src => src.CityTown))
                .ForMember(dest => dest.CountryId, mo => mo.MapFrom(src => src.CountryId))
                .ForMember(dest => dest.LengthOfStayMonths, mo => mo.MapFrom(src => src.LengthOfStayMonths))
                .ForMember(dest => dest.LengthOfStayYears, mo => mo.MapFrom(src => src.LengthOfStayYears))
                .ForMember(dest => dest.Postcode, mo => mo.MapFrom(src => src.Postcode))
                .ForMember(dest => dest.StateProvinceId, mo => mo.MapFrom(src => src.StateProvinceId));
            CreateMap<AddressModel, Address>()
                .ForMember(dest => dest.Address1, mo => mo.MapFrom(src => src.Address1))
                .ForMember(dest => dest.Address2, mo => mo.MapFrom(src => src.Address2))
                .ForMember(dest => dest.BuildingNumber, mo => mo.MapFrom(src => src.BuildingNumber))
                .ForMember(dest => dest.CityTown, mo => mo.MapFrom(src => src.CityTown))
                .ForMember(dest => dest.CountryId, mo => mo.MapFrom(src => src.CountryId))
                .ForMember(dest => dest.LengthOfStayMonths, mo => mo.MapFrom(src => src.LengthOfStayMonths))
                .ForMember(dest => dest.LengthOfStayYears, mo => mo.MapFrom(src => src.LengthOfStayYears))
                .ForMember(dest => dest.Postcode, mo => mo.MapFrom(src => src.Postcode))
                .ForMember(dest => dest.StateProvinceId, mo => mo.MapFrom(src => src.StateProvinceId));

            CreateMap<ConsumerFinanceHomeAddress, HomeAddressModel>()
                .ForMember(dest => dest.HomeOwnerStatusId, mo => mo.MapFrom(src => src.HomeOwnerStatusId))
                .ForMember(dest => dest.IsUkResident, mo => mo.MapFrom(src => src.IsUkResident))
                .AfterMap((e, m) =>
                {
                    m.Current = e.CurrentAddress.ToModel();
                    m.Previous = e.PreviousAddress.ToModel();
                });
            CreateMap<HomeAddressModel, ConsumerFinanceHomeAddress>()
                .ForMember(dest => dest.HomeOwnerStatusId, mo => mo.MapFrom(src => src.HomeOwnerStatusId))
                .ForMember(dest => dest.IsUkResident, mo => mo.MapFrom(src => src.IsUkResident))
                .AfterMap((e, m) =>
                {
                    m.CurrentAddress = e.Current.ToEntity();
                    m.PreviousAddress = e.Previous.ToEntity();
                });

            CreateMap<ConsumerFinanceEmployment, EmploymentModel>()
                .ForMember(dest => dest.AnnualGrossIncomeId, mo => mo.MapFrom(src => src.AnnualGrossIncomeId))
                .ForMember(dest => dest.EmployedEmployerName, mo => mo.MapFrom(src => src.EmployedEmployerName))
                .ForMember(dest => dest.EmployedJobTitle, mo => mo.MapFrom(src => src.EmployedJobTitle))
                .ForMember(dest => dest.EmployedMonthsEmployed, mo => mo.MapFrom(src => src.EmployedMonthsEmployed))
                .ForMember(dest => dest.EmployedPostCode, mo => mo.MapFrom(src => src.EmployedPostCode))
                .ForMember(dest => dest.EmployedYearsEmployed, mo => mo.MapFrom(src => src.EmployedYearsEmployed))
                .ForMember(dest => dest.EmploymentStatusId, mo => mo.MapFrom(src => src.EmploymentStatusId))
                .ForMember(dest => dest.OtherMonthsLength, mo => mo.MapFrom(src => src.OtherMonthsLength))
                .ForMember(dest => dest.OtherYearsLength, mo => mo.MapFrom(src => src.OtherYearsLength))
                .ForMember(dest => dest.SelfEmployedMonthsLength, mo => mo.MapFrom(src => src.SelfEmployedMonthsLength))
                .ForMember(dest => dest.SelfEmployedNatureOfBusiness, mo => mo.MapFrom(src => src.SelfEmployedNatureOfBusiness))
                .ForMember(dest => dest.SelfEmployedPostCode, mo => mo.MapFrom(src => src.SelfEmployedPostCode))
                .ForMember(dest => dest.SelfEmployedTradingAs, mo => mo.MapFrom(src => src.SelfEmployedTradingAs))
                .ForMember(dest => dest.SelfEmployedYearsLength, mo => mo.MapFrom(src => src.SelfEmployedYearsLength));
            CreateMap<EmploymentModel, ConsumerFinanceEmployment>()
                .ForMember(dest => dest.AnnualGrossIncomeId, mo => mo.MapFrom(src => src.AnnualGrossIncomeId))
                .ForMember(dest => dest.EmployedEmployerName, mo => mo.MapFrom(src => src.EmployedEmployerName))
                .ForMember(dest => dest.EmployedJobTitle, mo => mo.MapFrom(src => src.EmployedJobTitle))
                .ForMember(dest => dest.EmployedMonthsEmployed, mo => mo.MapFrom(src => src.EmployedMonthsEmployed))
                .ForMember(dest => dest.EmployedPostCode, mo => mo.MapFrom(src => src.EmployedPostCode))
                .ForMember(dest => dest.EmployedYearsEmployed, mo => mo.MapFrom(src => src.EmployedYearsEmployed))
                .ForMember(dest => dest.EmploymentStatusId, mo => mo.MapFrom(src => src.EmploymentStatusId))
                .ForMember(dest => dest.OtherMonthsLength, mo => mo.MapFrom(src => src.OtherMonthsLength))
                .ForMember(dest => dest.OtherYearsLength, mo => mo.MapFrom(src => src.OtherYearsLength))
                .ForMember(dest => dest.SelfEmployedMonthsLength, mo => mo.MapFrom(src => src.SelfEmployedMonthsLength))
                .ForMember(dest => dest.SelfEmployedNatureOfBusiness, mo => mo.MapFrom(src => src.SelfEmployedNatureOfBusiness))
                .ForMember(dest => dest.SelfEmployedPostCode, mo => mo.MapFrom(src => src.SelfEmployedPostCode))
                .ForMember(dest => dest.SelfEmployedTradingAs, mo => mo.MapFrom(src => src.SelfEmployedTradingAs))
                .ForMember(dest => dest.SelfEmployedYearsLength, mo => mo.MapFrom(src => src.SelfEmployedYearsLength));

            CreateMap<ConsumerFinanceExpenditureRecord, IncomeAndExpenditureModel.ExpenditureCategoryRangeModel>()
                .ForMember(dest => dest.ExpenditureCategoryId, mo => mo.MapFrom(src => src.ExpenditureCategoryId))
                .ForMember(dest => dest.ExpenditureRangeId, mo => mo.MapFrom(src => src.ExpenditureRangeId))
                .ForMember(dest => dest.SpecificValue, mo => mo.MapFrom(src => src.ConsumerExpenditureSpecificValueInGbp))
                .ForMember(dest => dest.SpecificValueEnabled, mo => mo.MapFrom(src => src.ConsumerExpenditureSpecificValueInGbp.HasValue));
            CreateMap<IncomeAndExpenditureModel.ExpenditureCategoryRangeModel, ConsumerFinanceExpenditureRecord>()
                .ForMember(dest => dest.ConsumerExpenditureSpecificValueInGbp, mo => mo.MapFrom(src => src.SpecificValue))
                .ForMember(dest => dest.ExpenditureCategoryId, mo => mo.MapFrom(src => src.ExpenditureCategoryId))
                .ForMember(dest => dest.ExpenditureRangeId, mo => mo.MapFrom(src => src.ExpenditureRangeId));

            CreateMap<ConsumerFinanceIncomeAndExpenditure, IncomeAndExpenditureModel>()
                .ForMember(dest => dest.MonthlyIncomeAmountAfterTax, mo => mo.MapFrom(src => src.MonthlyIncomeAmountAfterTax))
                .AfterMap((e, m) =>
                {
                    if (e.ConsumerExpenditures != null)
                        m.ExpenditureCategoryRangeList = e.ConsumerExpenditures.Select(x => x.ToModel()).ToList();
                });
            CreateMap<IncomeAndExpenditureModel, ConsumerFinanceIncomeAndExpenditure>()
                .ForMember(dest => dest.MonthlyIncomeAmountAfterTax, mo => mo.MapFrom(src => src.MonthlyIncomeAmountAfterTax))
                .AfterMap((m, e) =>
                {
                    if (m.ExpenditureCategoryRangeList != null)
                        e.ConsumerExpenditures = m.ExpenditureCategoryRangeList.Select(x => x.ToEntity()).ToList();
                });

            CreateMap<ConsumerFinanceBank, BankModel>()
                .ForMember(dest => dest.AccountAgeMonths, mo => mo.MapFrom(src => src.AccountAgeMonths))
                .ForMember(dest => dest.AccountAgeYears, mo => mo.MapFrom(src => src.AccountAgeYears))
                .ForMember(dest => dest.AccountName, mo => mo.MapFrom(src => src.AccountName))
                .ForMember(dest => dest.AccountNumber, mo => mo.MapFrom(src => src.AccountNumber))
                .ForMember(dest => dest.SortCode, mo => mo.MapFrom(src => src.SortCode));
            CreateMap<BankModel, ConsumerFinanceBank>()
                .ForMember(dest => dest.AccountAgeMonths, mo => mo.MapFrom(src => src.AccountAgeMonths))
                .ForMember(dest => dest.AccountAgeYears, mo => mo.MapFrom(src => src.AccountAgeYears))
                .ForMember(dest => dest.AccountName, mo => mo.MapFrom(src => src.AccountName))
                .ForMember(dest => dest.AccountNumber, mo => mo.MapFrom(src => src.AccountNumber))
                .ForMember(dest => dest.SortCode, mo => mo.MapFrom(src => src.SortCode));
        }

        public int Order => 0;
    }
}