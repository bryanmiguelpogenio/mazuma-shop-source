﻿using AutoMapper;
using Nop.Core.Infrastructure.Mapper;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using Nop.Plugin.Ecorenew.Finance.Extensions;
using Nop.Plugin.Ecorenew.Finance.Models.Common;
using Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup;
using System;

namespace Nop.Plugin.Ecorenew.Finance.Infrastructure
{
    public class CorporateFinanceMapperConfig : Profile, IMapperProfile
    {
        public CorporateFinanceMapperConfig()
        {
            CreateMap<CorporateFinanceBasicDetails, BasicDetailsModel>()
                .ForMember(dest => dest.CompanyName, mo => mo.MapFrom(src => src.CompanyName))
                .ForMember(dest => dest.TradingAs, mo => mo.MapFrom(src => src.TradingName))
                .ForMember(dest => dest.TradingAddressSameAsCompanyAddress, mo => mo.MapFrom(src => src.TradingAddressSameAsCompanyAddress))
                .ForMember(dest => dest.CompanyRegistrationNumber, mo => mo.MapFrom(src => src.CompanyRegistrationNumber))
                .ForMember(dest => dest.VatNumber, mo => mo.MapFrom(src => src.VatNumber))
                .ForMember(dest => dest.IndustrySector, mo => mo.MapFrom(src => src.IndustrySector))
                .ForMember(dest => dest.DateEstablishedDay, mo => mo.MapFrom(src => src.DateEstablished.Day))
                .ForMember(dest => dest.DateEstablishedMonth, mo => mo.MapFrom(src => src.DateEstablished.Month))
                .ForMember(dest => dest.DateEstablishedYear, mo => mo.MapFrom(src => src.DateEstablished.Year))
                .AfterMap((e, m) =>
                {
                    m.CompanyAddress = e.CompanyAddress.ToModel();
                    m.TradingAddress = e.TradingAddress.ToModel();
                });
            CreateMap<BasicDetailsModel, CorporateFinanceBasicDetails>()
                .ForMember(dest => dest.DateEstablished, mo => mo.MapFrom(src => new DateTime(src.DateEstablishedYear.Value, src.DateEstablishedMonth.Value, src.DateEstablishedDay.Value)))
                .ForMember(dest => dest.CompanyName, mo => mo.MapFrom(src => src.CompanyName))
                .ForMember(dest => dest.TradingName, mo => mo.MapFrom(src => src.TradingAs))
                .ForMember(dest => dest.TradingAddressSameAsCompanyAddress, mo => mo.MapFrom(src => src.TradingAddressSameAsCompanyAddress))
                .ForMember(dest => dest.CompanyRegistrationNumber, mo => mo.MapFrom(src => src.CompanyRegistrationNumber))
                .ForMember(dest => dest.VatNumber, mo => mo.MapFrom(src => src.VatNumber))
                .ForMember(dest => dest.IndustrySector, mo => mo.MapFrom(src => src.IndustrySector))
                .AfterMap((m, e) =>
                {
                    e.CompanyAddress = m.CompanyAddress.ToEntity();
                    e.TradingAddress = m.TradingAddress.ToEntity();
                });

            CreateMap<Address, AddressModel>()
                .ForMember(dest => dest.Address1, mo => mo.MapFrom(src => src.Address1))
                .ForMember(dest => dest.Address2, mo => mo.MapFrom(src => src.Address2))
                .ForMember(dest => dest.BuildingNumber, mo => mo.MapFrom(src => src.BuildingNumber))
                .ForMember(dest => dest.CityTown, mo => mo.MapFrom(src => src.CityTown))
                .ForMember(dest => dest.CountryId, mo => mo.MapFrom(src => src.CountryId))
                .ForMember(dest => dest.LengthOfStayMonths, mo => mo.MapFrom(src => src.LengthOfStayMonths))
                .ForMember(dest => dest.LengthOfStayYears, mo => mo.MapFrom(src => src.LengthOfStayYears))
                .ForMember(dest => dest.Postcode, mo => mo.MapFrom(src => src.Postcode))
                .ForMember(dest => dest.StateProvinceId, mo => mo.MapFrom(src => src.StateProvinceId));
            CreateMap<AddressModel, Address>()
                .ForMember(dest => dest.Address1, mo => mo.MapFrom(src => src.Address1))
                .ForMember(dest => dest.Address2, mo => mo.MapFrom(src => src.Address2))
                .ForMember(dest => dest.BuildingNumber, mo => mo.MapFrom(src => src.BuildingNumber))
                .ForMember(dest => dest.CityTown, mo => mo.MapFrom(src => src.CityTown))
                .ForMember(dest => dest.CountryId, mo => mo.MapFrom(src => src.CountryId))
                .ForMember(dest => dest.LengthOfStayMonths, mo => mo.MapFrom(src => src.LengthOfStayMonths))
                .ForMember(dest => dest.LengthOfStayYears, mo => mo.MapFrom(src => src.LengthOfStayYears))
                .ForMember(dest => dest.Postcode, mo => mo.MapFrom(src => src.Postcode))
                .ForMember(dest => dest.StateProvinceId, mo => mo.MapFrom(src => src.StateProvinceId));

            CreateMap<CorporateFinanceDirectorDetails, DirectorDetailsModel>()
                .ForMember(dest => dest.ConfirmEmail, mo => mo.MapFrom(src => src.EmailAddress))
                .ForMember(dest => dest.DateOfBirthDay, mo => mo.MapFrom(src => src.DateOfBirth.Day))
                .ForMember(dest => dest.DateOfBirthMonth, mo => mo.MapFrom(src => src.DateOfBirth.Month))
                .ForMember(dest => dest.DateOfBirthYear, mo => mo.MapFrom(src => src.DateOfBirth.Year))
                .ForMember(dest => dest.Email, mo => mo.MapFrom(src => src.EmailAddress))
                .ForMember(dest => dest.FirstName, mo => mo.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, mo => mo.MapFrom(src => src.LastName))
                .ForMember(dest => dest.MaritalStatusId, mo => mo.MapFrom(src => src.MaritalStatusId))
                .ForMember(dest => dest.MiddleName, mo => mo.MapFrom(src => src.MiddleName))
                .ForMember(dest => dest.MobilePhoneNumber, mo => mo.MapFrom(src => src.MobileNumber))
                .ForMember(dest => dest.NumberOfDependentsId, mo => mo.MapFrom(src => src.NumberOfDependentsId))
                .ForMember(dest => dest.Title, mo => mo.MapFrom(src => src.Title))
                .ForMember(dest => dest.HomeOwnerStatusId, mo => mo.MapFrom(src => src.HomeOwnerStatusId))
                .ForMember(dest => dest.IsUkResident, mo => mo.MapFrom(src => src.IsUkResident))
                .AfterMap((e, m) =>
                {
                    m.CurrentAddress = e.CurrentAddress.ToModel();
                    m.PreviousAddress = e.PreviousAddress.ToModel();
                });
            CreateMap<DirectorDetailsModel, CorporateFinanceDirectorDetails>()
                .ForMember(dest => dest.DateOfBirth, mo => mo.MapFrom(src => new DateTime(src.DateOfBirthYear.Value, src.DateOfBirthMonth.Value, src.DateOfBirthDay.Value)))
                .ForMember(dest => dest.EmailAddress, mo => mo.MapFrom(src => src.Email))
                .ForMember(dest => dest.FirstName, mo => mo.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, mo => mo.MapFrom(src => src.LastName))
                .ForMember(dest => dest.MaritalStatusId, mo => mo.MapFrom(src => src.MaritalStatusId.Value))
                .ForMember(dest => dest.MiddleName, mo => mo.MapFrom(src => src.MiddleName))
                .ForMember(dest => dest.MobileNumber, mo => mo.MapFrom(src => src.MobilePhoneNumber))
                .ForMember(dest => dest.NumberOfDependentsId, mo => mo.MapFrom(src => src.NumberOfDependentsId))
                .ForMember(dest => dest.Password, mo => mo.MapFrom(src => src.Password))
                .ForMember(dest => dest.Title, mo => mo.MapFrom(src => src.Title))
                .ForMember(dest => dest.HomeOwnerStatusId, mo => mo.MapFrom(src => src.HomeOwnerStatusId))
                .ForMember(dest => dest.IsUkResident, mo => mo.MapFrom(src => src.IsUkResident))
                .AfterMap((m, e) =>
                {
                    e.CurrentAddress = m.CurrentAddress.ToEntity();
                    e.PreviousAddress = m.PreviousAddress.ToEntity();
                });

            CreateMap<CorporateFinanceBankDetails, BankModel>()
                .ForMember(dest => dest.AccountAgeMonths, mo => mo.MapFrom(src => src.AccountAgeMonths))
                .ForMember(dest => dest.AccountAgeYears, mo => mo.MapFrom(src => src.AccountAgeYears))
                .ForMember(dest => dest.AccountName, mo => mo.MapFrom(src => src.AccountName))
                .ForMember(dest => dest.CompanyName, mo => mo.MapFrom(src => src.CompanyName))
                .ForMember(dest => dest.TradingName, mo => mo.MapFrom(src => src.TradingName))
                .ForMember(dest => dest.AccountNumber, mo => mo.MapFrom(src => src.AccountNumber))
                .ForMember(dest => dest.SortCode, mo => mo.MapFrom(src => src.SortCode));
            CreateMap<BankModel, CorporateFinanceBankDetails>()
                .ForMember(dest => dest.AccountAgeMonths, mo => mo.MapFrom(src => src.AccountAgeMonths))
                .ForMember(dest => dest.AccountAgeYears, mo => mo.MapFrom(src => src.AccountAgeYears))
                .ForMember(dest => dest.AccountName, mo => mo.MapFrom(src => src.AccountName))
                .ForMember(dest => dest.CompanyName, mo => mo.MapFrom(src => src.CompanyName))
                .ForMember(dest => dest.TradingName, mo => mo.MapFrom(src => src.TradingName))
                .ForMember(dest => dest.AccountNumber, mo => mo.MapFrom(src => src.AccountNumber))
                .ForMember(dest => dest.SortCode, mo => mo.MapFrom(src => src.SortCode));
        }

        public int Order => 0;
    }
}