﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Shipping.Date;
using Nop.Services.Stores;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;

namespace Nop.Plugin.Ecorenew.Finance.Services
{
    public class FinanceShoppingCartService : ShoppingCartService
    {
        private readonly IStoreContext _storeContext;
        private readonly IConsumerFinanceService _consumerFinanceService;
        private readonly ICorporateFinanceService _corporateFinanceService;
        private readonly FinanceSettings _financeSettings;

        #region Ctor

        public FinanceShoppingCartService(IRepository<ShoppingCartItem> sciRepository, 
            IWorkContext workContext, 
            IStoreContext storeContext, 
            ICurrencyService currencyService, 
            IProductService productService, 
            ILocalizationService localizationService, 
            IProductAttributeParser productAttributeParser, 
            ICheckoutAttributeService checkoutAttributeService, 
            ICheckoutAttributeParser checkoutAttributeParser, 
            IPriceFormatter priceFormatter, 
            ICustomerService customerService, 
            OrderSettings orderSettings, 
            ShoppingCartSettings shoppingCartSettings, 
            IEventPublisher eventPublisher, 
            IPermissionService permissionService, 
            IAclService aclService, 
            IDateRangeService dateRangeService, 
            IStoreMappingService storeMappingService, 
            IGenericAttributeService genericAttributeService, 
            IProductAttributeService productAttributeService, 
            IDateTimeHelper dateTimeHelper,
            IConsumerFinanceService consumerFinanceService,
            ICorporateFinanceService corporateFinanceService,
            FinanceSettings financeSettings) 
            : base(sciRepository, 
                  workContext, 
                  storeContext, 
                  currencyService, 
                  productService, 
                  localizationService, 
                  productAttributeParser, 
                  checkoutAttributeService, 
                  checkoutAttributeParser, 
                  priceFormatter, 
                  customerService, 
                  orderSettings, 
                  shoppingCartSettings, 
                  eventPublisher, 
                  permissionService, 
                  aclService, 
                  dateRangeService, 
                  storeMappingService, 
                  genericAttributeService, 
                  productAttributeService, 
                  dateTimeHelper)
        {
            this._storeContext = storeContext;
            this._consumerFinanceService = consumerFinanceService;
            this._corporateFinanceService = corporateFinanceService;
            this._financeSettings = financeSettings;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add a product to shopping cart
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <param name="product">Product</param>
        /// <param name="shoppingCartType">Shopping cart type</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="attributesXml">Attributes in XML format</param>
        /// <param name="customerEnteredPrice">The price enter by a customer</param>
        /// <param name="rentalStartDate">Rental start date</param>
        /// <param name="rentalEndDate">Rental end date</param>
        /// <param name="quantity">Quantity</param>
        /// <param name="automaticallyAddRequiredProductsIfEnabled">Automatically add required products if enabled</param>
        /// <returns>Warnings</returns>
        public override IList<string> AddToCart(Customer customer, Product product, ShoppingCartType shoppingCartType, int storeId, string attributesXml = null, decimal customerEnteredPrice = 0, DateTime? rentalStartDate = null, DateTime? rentalEndDate = null, int quantity = 1, bool automaticallyAddRequiredProductsIfEnabled = true)
        {
            if (FinancePlugin.IsInstalledAndEnabled(storeId))
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        if (!_consumerFinanceService.CustomerCanUpdateShoppingCart())
                            return new List<string> { "You are not allowed to update your shopping cart. Please complete your previous order first." }; // TODO localization and add additional info on error message
                        break;
                    case FinanceType.Corporate:
                        if (!_corporateFinanceService.CustomerCanUpdateShoppingCart())
                            return new List<string> { "You are not allowed to update your shopping cart. Please complete your previous order first." }; // TODO localization and add additional info on error message
                        break;
                }
            }

            return base.AddToCart(customer, product, shoppingCartType, storeId, attributesXml, customerEnteredPrice, rentalStartDate, rentalEndDate, quantity, automaticallyAddRequiredProductsIfEnabled);
        }

        /// <summary>
        /// Updates the shopping cart item
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <param name="shoppingCartItemId">Shopping cart item identifier</param>
        /// <param name="attributesXml">Attributes in XML format</param>
        /// <param name="customerEnteredPrice">New customer entered price</param>
        /// <param name="rentalStartDate">Rental start date</param>
        /// <param name="rentalEndDate">Rental end date</param>
        /// <param name="quantity">New shopping cart item quantity</param>
        /// <param name="resetCheckoutData">A value indicating whether to reset checkout data</param>
        /// <returns>Warnings</returns>
        public override IList<string> UpdateShoppingCartItem(Customer customer, int shoppingCartItemId, string attributesXml, decimal customerEnteredPrice, DateTime? rentalStartDate = null, DateTime? rentalEndDate = null, int quantity = 1, bool resetCheckoutData = true)
        {
            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        if (!_consumerFinanceService.CustomerCanUpdateShoppingCart())
                            return new List<string> { "You are not allowed to update your shopping cart. Please complete your previous order first." }; // TODO localization and add additional info on error message
                        break;
                    case FinanceType.Corporate:
                        if (!_corporateFinanceService.CustomerCanUpdateShoppingCart())
                            return new List<string> { "You are not allowed to update your shopping cart. Please complete your previous order first." }; // TODO localization and add additional info on error message
                        break;
                }
            }

            return base.UpdateShoppingCartItem(customer, shoppingCartItemId, attributesXml, customerEnteredPrice, rentalStartDate, rentalEndDate, quantity, resetCheckoutData);
        }

        /// <summary>
        /// Delete shopping cart item
        /// </summary>
        /// <param name="shoppingCartItem">Shopping cart item</param>
        /// <param name="resetCheckoutData">A value indicating whether to reset checkout data</param>
        /// <param name="ensureOnlyActiveCheckoutAttributes">A value indicating whether to ensure that only active checkout attributes are attached to the current customer</param>
        public override void DeleteShoppingCartItem(ShoppingCartItem shoppingCartItem, bool resetCheckoutData = true, bool ensureOnlyActiveCheckoutAttributes = false)
        {
            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        if (!_consumerFinanceService.CustomerCanUpdateShoppingCart())
                            throw new NopException("You are not allowed to update your shopping cart. Please complete your previous order first."); // TODO localization and add additional info on error message
                        break;
                    case FinanceType.Corporate:
                        if (!_corporateFinanceService.CustomerCanUpdateShoppingCart())
                            throw new NopException("You are not allowed to update your shopping cart. Please complete your previous order first."); // TODO localization and add additional info on error message
                        break;
                }
            }

            base.DeleteShoppingCartItem(shoppingCartItem, resetCheckoutData, ensureOnlyActiveCheckoutAttributes);
        }

        /// <summary>
        /// Migrate shopping cart
        /// </summary>
        /// <param name="fromCustomer">From customer</param>
        /// <param name="toCustomer">To customer</param>
        /// <param name="includeCouponCodes">A value indicating whether to coupon codes (discount and gift card) should be also re-applied</param>
        public override void MigrateShoppingCart(Customer fromCustomer, Customer toCustomer, bool includeCouponCodes)
        {
            if (FinancePlugin.IsInstalledAndEnabled())
            {
                // only migrate if toCustomer doesn't have a shopping cart item from that store
                if (!toCustomer.ShoppingCartItems.Any(s => s.ShoppingCartType == ShoppingCartType.ShoppingCart && s.StoreId == _storeContext.CurrentStore.Id))
                    base.MigrateShoppingCart(fromCustomer, toCustomer, includeCouponCodes);
            }
            else
            {
                base.MigrateShoppingCart(fromCustomer, toCustomer, includeCouponCodes);
            }
        }

        #endregion
    }
}