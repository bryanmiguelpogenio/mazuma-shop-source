﻿using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Services.Affiliates;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Nop.Core.Domain.Bank;
using Nop.Core.Data;
using Nop.Services.Insurance;

namespace Nop.Plugin.Ecorenew.Finance.Services
{
    public class FinanceOrderProcessingService : OrderProcessingService
    {
        private readonly IInsuranceDeviceService _insuranceDeviceService;
        private readonly IOrderService _orderService;
        private readonly IWebHelper _webHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IProductService _productService;
        private readonly IPaymentService _paymentService;
        private readonly ILogger _logger;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IShippingService _shippingService;
        private readonly ITaxService _taxService;
        private readonly ICustomerService _customerService;
        private readonly IEncryptionService _encryptionService;
        private readonly IRewardPointService _rewardPointService;
        private readonly IConsumerFinanceService _consumerFinanceService;
        private readonly ICorporateFinanceService _corporateFinanceService;
        private readonly FinanceSettings _financeSettings;
        private readonly ICustomNumberFormatter _customNumberFormatter;
        private readonly IRepository<TransferRecord> _transferRecordRepository;

        #region Ctor

        public FinanceOrderProcessingService(IInsuranceDeviceService insuranceDeviceService,
            IOrderService orderService, 
            IWebHelper webHelper, 
            ILocalizationService localizationService, 
            ILanguageService languageService, 
            IProductService productService, 
            IPaymentService paymentService, 
            ILogger logger, 
            IOrderTotalCalculationService orderTotalCalculationService, 
            IPriceCalculationService priceCalculationService, 
            IPriceFormatter priceFormatter, 
            IProductAttributeParser productAttributeParser, 
            IProductAttributeFormatter productAttributeFormatter, 
            IGiftCardService giftCardService, 
            IShoppingCartService shoppingCartService, 
            ICheckoutAttributeFormatter checkoutAttributeFormatter, 
            IShippingService shippingService, 
            IShipmentService shipmentService, 
            ITaxService taxService, 
            ICustomerService customerService, 
            IDiscountService discountService, 
            IEncryptionService encryptionService, 
            IWorkContext workContext, 
            IWorkflowMessageService workflowMessageService, 
            IVendorService vendorService, 
            ICustomerActivityService customerActivityService, 
            ICurrencyService currencyService, 
            IAffiliateService affiliateService, 
            IEventPublisher eventPublisher, 
            IPdfService pdfService, 
            IRewardPointService rewardPointService, 
            IGenericAttributeService genericAttributeService, 
            ICountryService countryService, 
            IStateProvinceService stateProvinceService, 
            IConsumerFinanceService consumerFinanceService,
            ICorporateFinanceService corporateFinanceService,
            ShippingSettings shippingSettings, 
            PaymentSettings paymentSettings, 
            RewardPointsSettings rewardPointsSettings, 
            OrderSettings orderSettings, 
            TaxSettings taxSettings, 
            LocalizationSettings localizationSettings, 
            CurrencySettings currencySettings,
            FinanceSettings financeSettings,
            ICustomNumberFormatter customNumberFormatter,
            IRepository<TransferRecord> transferRecordRepository) 
            : base(orderService, 
                  webHelper, 
                  localizationService, 
                  languageService, 
                  productService, 
                  paymentService, 
                  logger, 
                  orderTotalCalculationService, 
                  priceCalculationService, 
                  priceFormatter, 
                  productAttributeParser, 
                  productAttributeFormatter, 
                  giftCardService, 
                  shoppingCartService, 
                  checkoutAttributeFormatter, 
                  shippingService, 
                  shipmentService, 
                  taxService, 
                  customerService, 
                  discountService, 
                  encryptionService, 
                  workContext, 
                  workflowMessageService, 
                  vendorService, 
                  customerActivityService, 
                  currencyService, 
                  affiliateService, 
                  eventPublisher, 
                  pdfService, 
                  rewardPointService, 
                  genericAttributeService, 
                  countryService, 
                  stateProvinceService, 
                  shippingSettings, 
                  paymentSettings, 
                  rewardPointsSettings, 
                  orderSettings, 
                  taxSettings, 
                  localizationSettings, 
                  currencySettings, 
                  customNumberFormatter,
                  transferRecordRepository)
        {
            this._insuranceDeviceService = insuranceDeviceService;
            this._orderService = orderService;
            this._webHelper = webHelper;
            this._localizationService = localizationService;
            this._productService = productService;
            this._paymentService = paymentService;
            this._logger = logger;
            this._priceCalculationService = priceCalculationService;
            this._productAttributeFormatter = productAttributeFormatter;
            this._shoppingCartService = shoppingCartService;
            this._shippingService = shippingService;
            this._taxService = taxService;
            this._customerService = customerService;
            this._encryptionService = encryptionService;
            this._rewardPointService = rewardPointService;
            this._consumerFinanceService = consumerFinanceService;
            this._corporateFinanceService = corporateFinanceService;
            this._financeSettings = financeSettings;
            this._customNumberFormatter = customNumberFormatter;
            this._transferRecordRepository = transferRecordRepository;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Move shopping cart items to order items
        /// </summary>
        /// <param name="details">Place order container</param>
        /// <param name="order">Order</param>
        protected override void MoveShoppingCartItemsToOrderItems(PlaceOrderContainer details, Order order, IList<string> insuranceShoppingCartIds = null)
        {
            if (!FinancePlugin.IsInstalledAndEnabled())
            {
                base.MoveShoppingCartItemsToOrderItems(details, order, insuranceShoppingCartIds);
                return;
            }

            foreach (var sc in details.Cart)
            {
                //prices
                var scUnitPrice = _priceCalculationService.GetUnitPrice(sc);
                var scSubTotal = _priceCalculationService.GetSubTotal(sc, true, out decimal discountAmount,
                    out List<DiscountForCaching> scDiscounts, out int? _);
                var scUnitPriceInclTax =
                    _taxService.GetProductPrice(sc.Product, scUnitPrice, true, details.Customer, out decimal _);
                var scUnitPriceExclTax =
                    _taxService.GetProductPrice(sc.Product, scUnitPrice, false, details.Customer, out _);
                var scSubTotalInclTax =
                    _taxService.GetProductPrice(sc.Product, scSubTotal, true, details.Customer, out _);
                var scSubTotalExclTax =
                    _taxService.GetProductPrice(sc.Product, scSubTotal, false, details.Customer, out _);
                var discountAmountInclTax =
                    _taxService.GetProductPrice(sc.Product, discountAmount, true, details.Customer, out _);
                var discountAmountExclTax =
                    _taxService.GetProductPrice(sc.Product, discountAmount, false, details.Customer, out _);
                foreach (var disc in scDiscounts)
                    if (!details.AppliedDiscounts.ContainsDiscount(disc))
                        details.AppliedDiscounts.Add(disc);

                //attributes
                var attributeDescription =
                    _productAttributeFormatter.FormatAttributes(sc.Product, sc.AttributesXml, details.Customer);

                var itemWeight = _shippingService.GetShoppingCartItemWeight(sc);

                //get unitInsurancePrice
                decimal unitInsurancePrice = 0;
                if (insuranceShoppingCartIds != null && insuranceShoppingCartIds.Any(x => x == sc.Id.ToString()))
                {
                    var insuranceDevice = _insuranceDeviceService.GetInsuranceDeviceByAttributesXml(sc.AttributesXml);

                    if (insuranceDevice != null)
                    {
                        _logger.InsertLog(Core.Domain.Logging.LogLevel.Information,
                            "Order Processing: Insurance Value",
                            $"Insurance device value is {insuranceDevice.Value}");

                        var insuranceProduct = _productService.GetInsuranceProductByProduct(sc.Product, insuranceDevice.Value);
                        if (insuranceProduct != null)
                            unitInsurancePrice = insuranceProduct.Price;
                    }
                }

                //save order item
                var orderItem = new OrderItem
                {
                    OrderItemGuid = Guid.NewGuid(),
                    Order = order,
                    ProductId = sc.ProductId,
                    UnitPriceInclTax = scUnitPriceInclTax,
                    UnitPriceExclTax = scUnitPriceExclTax,
                    PriceInclTax = scSubTotalInclTax,
                    PriceExclTax = scSubTotalExclTax,
                    OriginalProductCost = _priceCalculationService.GetProductCost(sc.Product, sc.AttributesXml),
                    AttributeDescription = attributeDescription,
                    AttributesXml = sc.AttributesXml,
                    Quantity = sc.Quantity,
                    DiscountAmountInclTax = discountAmountInclTax,
                    DiscountAmountExclTax = discountAmountExclTax,
                    DownloadCount = 0,
                    IsDownloadActivated = false,
                    LicenseDownloadId = 0,
                    ItemWeight = itemWeight,
                    RentalStartDateUtc = sc.RentalStartDateUtc,
                    RentalEndDateUtc = sc.RentalEndDateUtc,
                    UnitInsurancePrice = unitInsurancePrice
                };
                order.OrderItems.Add(orderItem);
                _orderService.UpdateOrder(order);

                //gift cards
                AddGiftCards(sc.Product, sc.AttributesXml, sc.Quantity, orderItem, scUnitPriceExclTax);

                //inventory
                _productService.AdjustInventory(sc.Product, -sc.Quantity, sc.AttributesXml,
                    string.Format(_localizationService.GetResource("Admin.StockQuantityHistory.Messages.PlaceOrder"),
                        order.Id));
            }

            //clear shopping cart
            details.Cart.ToList().ForEach(sci => _shoppingCartService.DeleteShoppingCartItem(sci, false));
        }

        protected override Order SaveOrderDetails(ProcessPaymentRequest processPaymentRequest, ProcessPaymentResult processPaymentResult, PlaceOrderContainer details)
        {
            if (!FinancePlugin.IsInstalledAndEnabled())
            {
                return base.SaveOrderDetails(processPaymentRequest, processPaymentResult, details);
            }

            var order = new Order
            {
                StoreId = processPaymentRequest.StoreId,
                OrderGuid = processPaymentRequest.OrderGuid,
                CustomerId = details.Customer.Id,
                CustomerLanguageId = details.CustomerLanguage.Id,
                CustomerTaxDisplayType = details.CustomerTaxDisplayType,
                CustomerIp = _webHelper.GetCurrentIpAddress(),
                OrderSubtotalInclTax = details.OrderSubTotalInclTax,
                OrderSubtotalExclTax = details.OrderSubTotalExclTax,
                OrderSubTotalDiscountInclTax = details.OrderSubTotalDiscountInclTax,
                OrderSubTotalDiscountExclTax = details.OrderSubTotalDiscountExclTax,
                OrderShippingInclTax = details.OrderShippingTotalInclTax,
                OrderShippingExclTax = details.OrderShippingTotalExclTax,
                PaymentMethodAdditionalFeeInclTax = details.PaymentAdditionalFeeInclTax,
                PaymentMethodAdditionalFeeExclTax = details.PaymentAdditionalFeeExclTax,
                TaxRates = details.TaxRates,
                OrderTax = details.OrderTaxTotal,
                OrderTotal = details.OrderTotal,
                RefundedAmount = decimal.Zero,
                OrderDiscount = details.OrderDiscountAmount,
                CheckoutAttributeDescription = details.CheckoutAttributeDescription,
                CheckoutAttributesXml = details.CheckoutAttributesXml,
                CustomerCurrencyCode = details.CustomerCurrencyCode,
                CurrencyRate = details.CustomerCurrencyRate,
                AffiliateId = details.AffiliateId,
                OrderStatus = OrderStatus.Pending,
                AllowStoringCreditCardNumber = processPaymentResult.AllowStoringCreditCardNumber,
                CardType = processPaymentResult.AllowStoringCreditCardNumber ? _encryptionService.EncryptText(processPaymentRequest.CreditCardType) : string.Empty,
                CardName = processPaymentResult.AllowStoringCreditCardNumber ? _encryptionService.EncryptText(processPaymentRequest.CreditCardName) : string.Empty,
                CardNumber = processPaymentResult.AllowStoringCreditCardNumber ? _encryptionService.EncryptText(processPaymentRequest.CreditCardNumber) : string.Empty,
                MaskedCreditCardNumber = _encryptionService.EncryptText(_paymentService.GetMaskedCreditCardNumber(processPaymentRequest.CreditCardNumber)),
                CardCvv2 = processPaymentResult.AllowStoringCreditCardNumber ? _encryptionService.EncryptText(processPaymentRequest.CreditCardCvv2) : string.Empty,
                CardExpirationMonth = processPaymentResult.AllowStoringCreditCardNumber ? _encryptionService.EncryptText(processPaymentRequest.CreditCardExpireMonth.ToString()) : string.Empty,
                CardExpirationYear = processPaymentResult.AllowStoringCreditCardNumber ? _encryptionService.EncryptText(processPaymentRequest.CreditCardExpireYear.ToString()) : string.Empty,
                PaymentMethodSystemName = processPaymentRequest.PaymentMethodSystemName,
                AuthorizationTransactionId = processPaymentResult.AuthorizationTransactionId,
                AuthorizationTransactionCode = processPaymentResult.AuthorizationTransactionCode,
                AuthorizationTransactionResult = processPaymentResult.AuthorizationTransactionResult,
                CaptureTransactionId = processPaymentResult.CaptureTransactionId,
                CaptureTransactionResult = processPaymentResult.CaptureTransactionResult,
                SubscriptionTransactionId = processPaymentResult.SubscriptionTransactionId,
                PaymentStatus = processPaymentResult.NewPaymentStatus,
                PaidDateUtc = null,
                BillingAddress = details.BillingAddress,
                ShippingAddress = details.ShippingAddress,
                ShippingStatus = details.ShippingStatus,
                ShippingMethod = details.ShippingMethodName,
                PickUpInStore = details.PickUpInStore,
                PickupAddress = details.PickupAddress,
                ShippingRateComputationMethodSystemName = details.ShippingRateComputationMethodSystemName,
                CustomValuesXml = processPaymentRequest.SerializeCustomValues(),
                VatNumber = details.VatNumber,
                CreatedOnUtc = DateTime.UtcNow,
                CustomOrderNumber = string.Empty
            };

            _orderService.InsertOrder(order);

            //generate and set custom order number
            order.CustomOrderNumber = _customNumberFormatter.GenerateOrderCustomNumber(order);
            _orderService.UpdateOrder(order);

            //reward points history
            if (details.RedeemedRewardPointsAmount > decimal.Zero)
            {
                _rewardPointService.AddRewardPointsHistoryEntry(details.Customer, -details.RedeemedRewardPoints, order.StoreId,
                    string.Format(_localizationService.GetResource("RewardPoints.Message.RedeemedForOrder", order.CustomerLanguageId), order.CustomOrderNumber),
                    order, details.RedeemedRewardPointsAmount);
                _customerService.UpdateCustomer(details.Customer);
            }

            switch (_financeSettings.FinanceType)
            {
                case FinanceType.Consumer:
                    var consumerFinanceSignup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                    if (consumerFinanceSignup != null && consumerFinanceSignup.OrderItems.ToList().Any(oi => details.Cart.Any(ci => ci.Id == oi.ShoppingCartItemId)))
                    {
                        _consumerFinanceService.UpdateConsumerFinanceSignupOrderId(consumerFinanceSignup.Id, order.Id);

                        order.EcorenewFinanceType = EcorenewFinanceType.Consumer;
                        order.OrderNotes.Add(new OrderNote
                        {
                            CreatedOnUtc = DateTime.UtcNow,
                            DisplayToCustomer = true,
                            Note = $"Months to pay: {consumerFinanceSignup.OrderItems.First().DurationOfAgreement}"
                        });

                        _orderService.UpdateOrder(order);
                    }
                    break;
                case FinanceType.Corporate:
                    var corporateFinanceSignup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();
                    if (corporateFinanceSignup != null && corporateFinanceSignup.OrderItems.ToList().Any(oi => details.Cart.Any(ci => ci.Id == oi.ShoppingCartItemId)))
                    {
                        _corporateFinanceService.UpdateCorporateFinanceSignupOrderId(corporateFinanceSignup.Id, order.Id);

                        order.EcorenewFinanceType = EcorenewFinanceType.Corporate;
                        order.OrderNotes.Add(new OrderNote
                        {
                            CreatedOnUtc = DateTime.UtcNow,
                            DisplayToCustomer = true,
                            Note = $"Months to pay: {corporateFinanceSignup.OrderItems.First().DurationOfAgreement}"
                        });

                        _orderService.UpdateOrder(order);
                    }
                    break;
            }

            return order;
        }

        #endregion
    }
}