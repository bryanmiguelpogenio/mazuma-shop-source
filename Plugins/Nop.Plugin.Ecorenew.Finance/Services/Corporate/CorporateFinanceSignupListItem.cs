﻿using System;

namespace Nop.Plugin.Ecorenew.Finance.Services.Corporate
{
    public class CorporateFinanceSignupListItem
    {
        public int Id { get; set; }

        public string ApprovalStatus { get; set; }

        public string CompanyName { get; set; }

        public string StoreName { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}