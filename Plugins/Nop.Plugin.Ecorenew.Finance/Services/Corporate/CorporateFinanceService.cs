﻿using Ecorenew.Common.DocuSign.Services;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using Nop.Services.Authentication;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Stores;
using Spire.Doc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DocuSignWeb = Ecorenew.Common.DocuSignWeb;

namespace Nop.Plugin.Ecorenew.Finance.Services.Corporate
{
    /// <summary>
    /// Corporate finance service
    /// </summary>
    public class CorporateFinanceService : ICorporateFinanceService
    {
        #region Fields

        private readonly IRepository<CorporateFinanceOption> _corporateFinanceOptionRepository;
        private readonly IRepository<CorporateFinanceOrderItem> _corporateFinanceOrderItemRepository;
        private readonly IRepository<CorporateFinanceSignup> _corporateFinanceSignupRepository;
        private readonly IRepository<HomeOwnerStatus> _homeOwnerStatusRepository;
        private readonly IRepository<MaritalStatus> _maritalStatusRepository;
        private readonly IRepository<NumberOfDependents> _numberOfDependentsRepository;
        private readonly IRepository<ShoppingCartItem> _shoppingCartItemRepository;
        private readonly IRepository<ShoppingCartItemCorporateFinanceOption> _shoppingCartItemCorporateFinanceOptionRepository;
        private readonly IAddressService _addressService;
        private readonly IAuthenticationService _authenticationService;
        private readonly ICorporateFinanceCalculationService _corporateFinanceCalculationService;
        private readonly ICorporateFinanceWorkflowMessageService _corporateFinanceWorkflowService;
        private readonly ICurrencyService _currencyService;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly ICustomerService _customerService;
        private readonly IDocuSignService _docuSignService;
        private readonly IEncryptionService _encryptionService;
        private readonly IEventPublisher _eventPublisher;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILogger _logger;
        private readonly IOrderService _orderService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IProductService _productService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly CorporateFinanceSettings _corporateFinanceSettings;
        private readonly CustomerSettings _customerSettings;
        private readonly LocalizationSettings _localizationSettings;

        #endregion

        #region Ctor

        public CorporateFinanceService(IRepository<CorporateFinanceOption> corporateFinanceOptionRepository,
            IRepository<CorporateFinanceOrderItem> corporateFinanceOrderItemRepository,
            IRepository<CorporateFinanceSignup> corporateFinanceSignupRepository,
            IRepository<HomeOwnerStatus> homeOwnerStatusRepository,
            IRepository<MaritalStatus> maritalStatusRepository,
            IRepository<NumberOfDependents> numberOfDependentsRepository,
            IRepository<ShoppingCartItem> shoppingCartItemRepository,
            IRepository<ShoppingCartItemCorporateFinanceOption> shoppingCartItemCorporateFinanceOptionRepository,
            IAddressService addressService,
            IAuthenticationService authenticationService,
            ICorporateFinanceCalculationService corporateFinanceCalculationService,
            ICorporateFinanceWorkflowMessageService corporateFinanceWorkflowService,
            ICurrencyService currencyService,
            ICustomerRegistrationService customerRegistrationService,
            ICustomerService customerService,
            IDocuSignService docuSignService,
            IEncryptionService encryptionService,
            IEventPublisher eventPublisher,
            IGenericAttributeService genericAttributeService,
            ILogger logger,
            IOrderService orderService,
            IPriceFormatter priceFormatter,
            IProductAttributeParser productAttributeParser,
            IProductService productService,
            IStoreContext storeContext,
            IStoreService storeService,
            IWebHelper webHelper,
            IWorkContext workContext,
            IWorkflowMessageService workflowMessageService,
            CorporateFinanceSettings corporateFinanceSettings,
            CustomerSettings customerSettings,
            LocalizationSettings localizationSettings)
        {
            this._corporateFinanceOptionRepository = corporateFinanceOptionRepository;
            this._corporateFinanceOrderItemRepository = corporateFinanceOrderItemRepository;
            this._corporateFinanceSignupRepository = corporateFinanceSignupRepository;
            this._homeOwnerStatusRepository = homeOwnerStatusRepository;
            this._maritalStatusRepository = maritalStatusRepository;
            this._numberOfDependentsRepository = numberOfDependentsRepository;
            this._shoppingCartItemRepository = shoppingCartItemRepository;
            this._shoppingCartItemCorporateFinanceOptionRepository = shoppingCartItemCorporateFinanceOptionRepository;
            this._addressService = addressService;
            this._authenticationService = authenticationService;
            this._corporateFinanceCalculationService = corporateFinanceCalculationService;
            this._corporateFinanceWorkflowService = corporateFinanceWorkflowService;
            this._currencyService = currencyService;
            this._customerRegistrationService = customerRegistrationService;
            this._customerService = customerService;
            this._docuSignService = docuSignService;
            this._encryptionService = encryptionService;
            this._eventPublisher = eventPublisher;
            this._genericAttributeService = genericAttributeService;
            this._logger = logger;
            this._orderService = orderService;
            this._priceFormatter = priceFormatter;
            this._productAttributeParser = productAttributeParser;
            this._productService = productService;
            this._storeContext = storeContext;
            this._storeService = storeService;
            this._webHelper = webHelper;
            this._workContext = workContext;
            this._workflowMessageService = workflowMessageService;
            this._corporateFinanceSettings = corporateFinanceSettings;
            this._customerSettings = customerSettings;
            this._localizationSettings = localizationSettings;
        }

        #endregion

        #region Utilities

        protected virtual CustomerRegistrationResult RegisterCustomer(
            Customer customer,
            CorporateFinanceBasicDetails basicDetails,
            CorporateFinanceDirectorDetails directorDetails)
        {
            if (string.IsNullOrWhiteSpace(directorDetails.Password))
            {
                _logger.Error("Null or white space password received.");
                throw new NopException("Unexpected error occurred.");
            }

            bool isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;

            var registrationRequest = new CustomerRegistrationRequest(customer,
                directorDetails.EmailAddress,
                directorDetails.EmailAddress,
                directorDetails.Password,
                _customerSettings.DefaultPasswordFormat,
                _storeContext.CurrentStore.Id,
                isApproved);

            var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);

            if (registrationResult.Success)
            {
                //form fields
                if (_customerSettings.GenderEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, directorDetails.Title.Trim().ToLower() == "mr" ? "M" : "F");

                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, directorDetails.FirstName.Trim());
                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, directorDetails.LastName.Trim());

                if (_customerSettings.DateOfBirthEnabled)
                {
                    DateTime? dateOfBirth = directorDetails.DateOfBirth;
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, dateOfBirth);
                }
                if (_customerSettings.CompanyEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, basicDetails.CompanyName.Trim());

                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, (directorDetails.CurrentAddress.BuildingNumber + " " + directorDetails.CurrentAddress.Address1).Trim());

                if (!string.IsNullOrWhiteSpace(directorDetails.CurrentAddress.Address2))
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, directorDetails.CurrentAddress.Address2.Trim());

                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, directorDetails.CurrentAddress.Postcode.Trim());
                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, directorDetails.CurrentAddress.CityTown.Trim());
                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, directorDetails.CurrentAddress.CountryId);

                if (_customerSettings.PhoneEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, (directorDetails.MobileNumber ?? "").Trim());

                // Insert default address
                var defaultAddress = new Nop.Core.Domain.Common.Address
                {
                    FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                    LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
                    Email = customer.Email,
                    Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
                    CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) > 0 ?
                        (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) : null,
                    StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) > 0 ?
                        (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) : null,
                    City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City),
                    Address1 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress),
                    Address2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2),
                    ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
                    PhoneNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                    FaxNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax),
                    CreatedOnUtc = customer.CreatedOnUtc
                };
                if (this._addressService.IsAddressValid(defaultAddress))
                {
                    // Some validation
                    if (defaultAddress.CountryId == 0)
                        defaultAddress.CountryId = null;
                    if (defaultAddress.StateProvinceId == 0)
                        defaultAddress.StateProvinceId = null;

                    // Set default address
                    customer.Addresses.Add(defaultAddress);
                    customer.BillingAddress = defaultAddress;
                    customer.ShippingAddress = defaultAddress;
                    _customerService.UpdateCustomer(customer);
                }

                // Notifications
                if (_customerSettings.NotifyNewCustomerRegistration)
                    _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer, _localizationSettings.DefaultAdminLanguageId);

                // Raise event       
                _eventPublisher.Publish(new CustomerRegisteredEvent(customer));

                // Login customer now
                _authenticationService.SignIn(customer, true);

                switch (_customerSettings.UserRegistrationType)
                {
                    case UserRegistrationType.EmailValidation:
                        {
                            // Email validation message
                            _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.AccountActivationToken, Guid.NewGuid().ToString());
                            _workflowMessageService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);

                            break;
                        }
                    case UserRegistrationType.Standard:
                        {
                            // Send customer welcome message
                            _workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

                            break;
                        }
                }
            }

            return registrationResult;
        }

        protected virtual DocuSignWeb.EnvelopeStatus SendCorporateFinanceAgreementDocuments(CorporateFinanceSignup signup)
        {
            // Get the template
            Document agreementDoc = new Document(CommonHelper.MapPath(_corporateFinanceSettings.FinanceDocumentTemplatesPath + "CorporateFinanceAgreement.docx"));

            var directorDetails = GetSignupDirectorDetails(signup);

            var row = agreementDoc.Sections[0].Tables[0].Rows[0];

            var customerName = row.Cells[1].Paragraphs[0];
            customerName.Text = $"{directorDetails.FirstName} {directorDetails.LastName}";

            var customerNumber = row.Cells[3].Paragraphs[0];
            customerNumber.Text = _customerService.GetCustomerById(signup.CustomerId).Id.ToString().PadLeft(8, '0');

            var agreementNumber = row.Cells[5].Paragraphs[0];
            agreementNumber.Text = signup.Id.ToString().PadLeft(8, '0');

            // Send to customer through DocuSign
            using (MemoryStream docStream = new MemoryStream())
            {
                agreementDoc.SaveToStream(docStream, FileFormat.PDF);

                var docuSignDoc = _docuSignService.CreateDocuSignDocument("Subscription Agreement", "1", docStream.ToArray());

                var recipients = new List<DocuSignWeb.Recipient>();

                var signer = _docuSignService.CreateDocuSignRecipient(
#if DEBUG
                        "nplusterio@ecoasiatech.com",
#else
                        directorDetails.EmailAddress,
#endif
                        String.Join(" ", directorDetails.FirstName, directorDetails.LastName),
                    signup.CustomerId.ToString(),
                    DocuSignWeb.RecipientTypeCode.Signer);

                recipients.Add(signer);

                var tabs = new List<DocuSignWeb.Tab>();

                var signTab = _docuSignService.CreateDocuSignTab(
                    "1",
                    signer.ID,
                    DocuSignWeb.TabTypeCode.SignHere,
                    3, 470, 760, true);

                tabs.Add(signTab);

                var emailBlurb = "";
                var envelopeStatus = _docuSignService.SendEnvelopeEmbedded(
                    $"{_storeContext.CurrentStore.Name} - Subscription Agreement", // TODO localization
                    emailBlurb,
                    recipients,
                    new List<DocuSignWeb.Document> { docuSignDoc },
                    tabs,
                    signup.CustomerId.ToString());

                return envelopeStatus;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Maps the customer's shopping cart items to their selected finance option
        /// </summary>
        /// <param name="corporateFinanceOptionId">Corporate finance option identifier</param>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        public virtual void MapCustomerShoppingCartItemsToFinanceOption(int corporateFinanceOptionId, int customerId = 0, int storeId = 0)
        {
            var corporateFinanceOption = _corporateFinanceOptionRepository.GetById(corporateFinanceOptionId);
            if (corporateFinanceOption == null)
                throw new NopException("Invalid corporate finance option.");

            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            storeId = storeId > 0 ? storeId : _storeContext.CurrentStore.Id;

            var shoppingCartItemIds = customer.ShoppingCartItems
                .Where(s => s.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart && s.StoreId == storeId)
                .Select(s => s.Id)
                .ToList();

            // Delete existing shopping cart item mappings
            var shoppingCartItemFinanceOptions = _shoppingCartItemCorporateFinanceOptionRepository.Table
                .Where(s => shoppingCartItemIds.Contains(s.ShoppingCartItemId));

            _shoppingCartItemCorporateFinanceOptionRepository.Delete(shoppingCartItemFinanceOptions);

            // Create new shopping cart item mappings based on the provided corporate finance option identifier
            _shoppingCartItemCorporateFinanceOptionRepository.Insert(
                shoppingCartItemIds.Select(s => new ShoppingCartItemCorporateFinanceOption
                {
                    CorporateFinanceOptionId = corporateFinanceOptionId,
                    ShoppingCartItemId = s
                }));
        }

        /// <summary>
        /// Clear the customer's shopping cart item finance option mappings
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        public virtual void ClearCustomerShoppingCartItemFinanceOptions(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            storeId = storeId > 0 ? storeId : _storeContext.CurrentStore.Id;

            var shoppingCartItemIds = customer.ShoppingCartItems
                .Where(s => s.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart && s.StoreId == storeId)
                .Select(s => s.Id)
                .ToList();

            // Delete existing shopping cart item mappings
            var shoppingCartItemFinanceOptions = _shoppingCartItemCorporateFinanceOptionRepository.Table
                .Where(s => shoppingCartItemIds.Contains(s.ShoppingCartItemId));

            _shoppingCartItemCorporateFinanceOptionRepository.Delete(shoppingCartItemFinanceOptions);
        }

        /// <summary>
        /// Gets the customer's shopping cart finance option mappings
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identiifer</param>
        public virtual IEnumerable<ShoppingCartItemCorporateFinanceOption> GetCustomerShoppingCartItemCorporateFinanceOptions(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            storeId = storeId > 0 ? storeId : _storeContext.CurrentStore.Id;

            var shoppingCartItemIds = customer.ShoppingCartItems
                .Where(s => s.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart && s.StoreId == storeId)
                .Select(s => s.Id)
                .ToList();

            return _shoppingCartItemCorporateFinanceOptionRepository.Table
                .Where(s => shoppingCartItemIds.Contains(s.ShoppingCartItemId));
        }

        /// <summary>
        /// Creates a new shopping cart item finance option mapping
        /// </summary>
        /// <param name="shoppingCartItemId">Shopping cart item identifier</param>
        /// <param name="corporateFinanceOptionId">Corporate finance option identifier</param>
        public virtual void CreateShoppingCartItemFinanceOption(int shoppingCartItemId, int corporateFinanceOptionId)
        {
            // Check if shopping cart item exists
            var shoppingCartItem = _shoppingCartItemRepository.GetById(shoppingCartItemId);
            if (shoppingCartItem == null)
                throw new NopException("Shopping cart item not found.");

            // Check if shopping cart item finance option mapping already exists
            if (_shoppingCartItemCorporateFinanceOptionRepository.Table.Any(s => s.ShoppingCartItemId == shoppingCartItemId))
                return;

            // Check if corporate finance option exists
            var corporateFinanceOption = _corporateFinanceOptionRepository.GetById(corporateFinanceOptionId);
            if (corporateFinanceOption == null)
                throw new NopException("Corporate finance option not found.");

            // Insert new record
            _shoppingCartItemCorporateFinanceOptionRepository.Insert(new ShoppingCartItemCorporateFinanceOption
            {
                CorporateFinanceOptionId = corporateFinanceOptionId,
                ShoppingCartItemId = shoppingCartItemId
            });
        }

        /// <summary>
        /// Deletes a shopping cart item finance option mapping
        /// </summary>
        /// <param name="shoppingCartItemId">Shopping cart item identifier</param>
        public void DeleteShoppingCartItemFinanceOption(int shoppingCartItemId)
        {
            var items = _shoppingCartItemCorporateFinanceOptionRepository.Table.Where(s => s.ShoppingCartItemId == shoppingCartItemId);
            _shoppingCartItemCorporateFinanceOptionRepository.Delete(items);
        }

        /// <summary>
        /// Determines if the customer can signup for finance
        /// </summary>
        /// <param name="notAllowedMessage">Message if not allowed for finance signup</param>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        public virtual bool CustomerCanSignup(out string notAllowedMessage, int customerId = 0)
        {
            var signup = GetLatestCorporateSignupOfCustomer(customerId);

            if (signup != null && signup.SignupCompletedOnUtc.HasValue)
            {
                if (!signup.IsApproved.HasValue)
                {
                    notAllowedMessage = $"You still have a finance application that needs approval.";
                    return false;
                }

                // Check if the customer can signup again.
                if (!signup.IsApproved.Value)
                {
                    int daysPassed = (DateTime.UtcNow - signup.SignupCompletedOnUtc.Value).Days;
                    if (daysPassed < 90)
                    {
                        notAllowedMessage = $"Your previous finance application failed. You can apply for finance after {(90 - daysPassed)} day/s.";
                        return false;
                    }

                    // else, allowed
                }
                else
                {
                    // Check if signup has order and verify if the order is paid.
                    if (!signup.OrderId.HasValue)
                    {
                        notAllowedMessage = "Please complete your previous order first.";
                        return false;
                    }

                    var order = _orderService.GetOrderById(signup.OrderId.Value);
                    if (!order.PaidDateUtc.HasValue)
                    {
                        notAllowedMessage = "You still haven't paid your previous finance order.";
                        return false;
                    }

                    int daysPassed = (DateTime.UtcNow - order.PaidDateUtc.Value).Days;
                    if (daysPassed < 7)
                    {
                        notAllowedMessage = $"You are not allowed to apply for finance for a while. ({(7 - daysPassed).ToString()} day/s left)";
                        return false;
                    }
                }
            }

            notAllowedMessage = null;
            return true;
        }

        /// <summary>
        /// Returns the latest corporate finance signup record of a customer.
        /// </summary>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        /// <param name="storeId">Store identifier. Will use the current store if not provided</param>
        public virtual CorporateFinanceSignup GetLatestCorporateSignupOfCustomer(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            if (customer == null)
                throw new NopException("Customer not found.");

            var store = storeId > 0 ? _storeService.GetStoreById(storeId) : _storeContext.CurrentStore;
            if (store == null)
                throw new NopException("Store not found.");

            return _corporateFinanceSignupRepository.Table
                .Where(s => s.CustomerId == customer.Id && s.StoreId == store.Id)
                .OrderByDescending(s => s.Id)
                .FirstOrDefault();
        }

        /// <summary>
        /// Creates a new corporate finance signup record for a customer
        /// </summary>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        /// <param name="storeId">Store identifier. Will use the current store if not provided</param>
        public virtual CorporateFinanceSignup CreateNewCorporateFinanceSignup(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            if (customer == null)
                throw new NopException("Customer not found.");

            var store = storeId > 0 ? _storeService.GetStoreById(storeId) : _storeContext.CurrentStore;
            if (store == null)
                throw new NopException("Store not found.");

            var signup = new CorporateFinanceSignup
            {
                CreatedOnUtc = DateTime.UtcNow,
                CustomerId = customer.Id,
                StoreId = store.Id,
                UpdatedOnUtc = DateTime.UtcNow
            };

            _corporateFinanceSignupRepository.Insert(signup);

            return signup;
        }

        /// <summary>
        /// Returns all marital statuses
        /// </summary>
        public virtual IEnumerable<MaritalStatus> GetAllMaritalStatuses()
        {
            return _maritalStatusRepository.Table;
        }

        /// <summary>
        /// Returns all number of dependents
        /// </summary>
        public virtual IEnumerable<NumberOfDependents> GetAllNumberOfDependents()
        {
            return _numberOfDependentsRepository.Table;
        }

        /// <summary>
        /// Returns all home owner statuses
        /// </summary>
        public virtual IEnumerable<HomeOwnerStatus> GetAllHomeOwnerStatuses()
        {
            return _homeOwnerStatusRepository.Table;
        }

        /// <summary>
        /// Returns corporate finance options
        /// </summary>
        /// <param name="includeInactive">Include inactive corporate finance options?</param>
        public virtual IEnumerable<CorporateFinanceOption> GetCorporateFinanceOptions(bool includeInactive = false)
        {
            var options = _corporateFinanceOptionRepository.Table;

            if (!includeInactive)
                options = options.Where(o => o.IsActive);

            return options.OrderBy(o => o.DurationInMonths);
        }

        /// <summary>
        /// Gets a corporate finance option
        /// </summary>
        /// <param name="id">Corporate finance option identifier</param>
        public virtual CorporateFinanceOption GetCorporateFinanceOptionById(int id)
        {
            if (id <= 0)
                return null;

            return _corporateFinanceOptionRepository.GetById(id);
        }

        /// <summary>
        /// Inserts a new corporate finance option
        /// </summary>
        /// <param name="corporateFinanceOption">Corporate finance option</param>
        public virtual void InsertCorporateFinanceOption(CorporateFinanceOption corporateFinanceOption)
        {
            _corporateFinanceOptionRepository.Insert(corporateFinanceOption);
        }

        /// <summary>
        /// Updates a corporate finance option
        /// </summary>
        /// <param name="corporateFinanceOption">Corporate finance option</param>
        public virtual void UpdateCorporateFinanceOption(CorporateFinanceOption corporateFinanceOption)
        {
            _corporateFinanceOptionRepository.Update(corporateFinanceOption);
        }

        /// <summary>
        /// Gets the corporate finance basic details of the signup record
        /// </summary>
        /// <param name="corporateFinanceSignup">Corporate finance signup</param>
        public virtual CorporateFinanceBasicDetails GetSignupBasicDetails(CorporateFinanceSignup corporateFinanceSignup)
        {
            if (corporateFinanceSignup == null)
                throw new ArgumentNullException(nameof(corporateFinanceSignup));

            if (string.IsNullOrWhiteSpace(corporateFinanceSignup.CorporateFinanceBasicDetails))
                return null;

            var basicDetailsText = _encryptionService.DecryptText(corporateFinanceSignup.CorporateFinanceBasicDetails);
            return JsonConvert.DeserializeObject<CorporateFinanceBasicDetails>(basicDetailsText);
        }

        /// <summary>
        /// Sets the corporate finance basic details of the signup record
        /// </summary>
        /// <param name="corporateFinanceSignupId">Corporate finance signup identifier</param>
        /// <param name="basicDetails">Basic details</param>
        public virtual void SetSignupBasicDetails(int corporateFinanceSignupId, CorporateFinanceBasicDetails basicDetails)
        {
            if (basicDetails == null)
                throw new ArgumentNullException(nameof(basicDetails));

            CorporateFinanceSignup signup;

            if (corporateFinanceSignupId <= 0 || (signup = _corporateFinanceSignupRepository.GetById(corporateFinanceSignupId)) == null)
                throw new NopException("Corporate finance signup record not found.");

            string basicDetailsText = JsonConvert.SerializeObject(basicDetails);
            signup.CorporateFinanceBasicDetails = _encryptionService.EncryptText(basicDetailsText);

            _corporateFinanceSignupRepository.Update(signup);
        }

        /// <summary>
        /// Gets the corporate finance director details of the signup record
        /// </summary>
        /// <param name="corporateFinanceSignup">Corporate finance signup</param>
        public virtual CorporateFinanceDirectorDetails GetSignupDirectorDetails(CorporateFinanceSignup corporateFinanceSignup)
        {
            if (corporateFinanceSignup == null)
                throw new ArgumentNullException(nameof(corporateFinanceSignup));

            if (string.IsNullOrWhiteSpace(corporateFinanceSignup.CorporateFinanceDirectorDetails))
                return null;

            var directorDetailsText = _encryptionService.DecryptText(corporateFinanceSignup.CorporateFinanceDirectorDetails);
            return JsonConvert.DeserializeObject<CorporateFinanceDirectorDetails>(directorDetailsText);
        }

        /// <summary>
        /// Sets the corporate finance director details of the signup record
        /// </summary>
        /// <param name="corporateFinanceSignupId">Corporate finance signup identifier</param>
        /// <param name="directorDetails">Basic details</param>
        public virtual void SetSignupDirectorDetails(int corporateFinanceSignupId, CorporateFinanceDirectorDetails directorDetails)
        {
            if (directorDetails == null)
                throw new ArgumentNullException(nameof(directorDetails));

            CorporateFinanceSignup signup;

            if (corporateFinanceSignupId <= 0 || (signup = _corporateFinanceSignupRepository.GetById(corporateFinanceSignupId)) == null)
                throw new NopException("Corporate finance signup record not found.");

            string directorDetailsText = JsonConvert.SerializeObject(directorDetails);
            signup.CorporateFinanceDirectorDetails = _encryptionService.EncryptText(directorDetailsText);

            _corporateFinanceSignupRepository.Update(signup);
        }

        /// <summary>
        /// Gets the corporate finance bank of the signup record
        /// </summary>
        /// <param name="corporateFinanceSignup">Corporate finance signup</param>
        public virtual CorporateFinanceBankDetails GetSignupBankDetails(CorporateFinanceSignup corporateFinanceSignup)
        {
            if (corporateFinanceSignup == null)
                throw new ArgumentNullException(nameof(corporateFinanceSignup));

            if (string.IsNullOrWhiteSpace(corporateFinanceSignup.CorporateFinanceBankDetails))
                return null;

            var bankText = _encryptionService.DecryptText(corporateFinanceSignup.CorporateFinanceBankDetails);
            return JsonConvert.DeserializeObject<CorporateFinanceBankDetails>(bankText);
        }

        /// <summary>
        /// Sets the corporate finance bank of the signup record
        /// </summary>
        /// <param name="corporateFinanceSignupId">Corporate finance signup identifier</param>
        /// <param name="bank">Basic details</param>
        public virtual void SetSignupBankDetails(int corporateFinanceSignupId, CorporateFinanceBankDetails bank)
        {
            if (bank == null)
                throw new ArgumentNullException(nameof(bank));

            CorporateFinanceSignup signup;

            if (corporateFinanceSignupId <= 0 || (signup = _corporateFinanceSignupRepository.GetById(corporateFinanceSignupId)) == null)
                throw new NopException("Corporate finance signup record not found.");

            string bankText = JsonConvert.SerializeObject(bank);
            signup.CorporateFinanceBankDetails = _encryptionService.EncryptText(bankText);

            _corporateFinanceSignupRepository.Update(signup);
        }

        /// <summary>
        /// Complete the corporate finance signup.
        /// This will perform all the necessary processes for validating the corporate's affordability, identity, etc.
        /// This will also create a new DocuSign envelope to be signed by the corporate once all the checks have been marked as passed
        /// </summary>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        /// <param name="storeId">Store identifier. Will use the current store if not provided</param>
        public virtual CorporateFinanceSignup CompleteCorporateFinanceSignup(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            if (customer == null)
                throw new NopException("Customer not found.");

            var store = storeId > 0 ? _storeService.GetStoreById(storeId) : _storeContext.CurrentStore;
            if (store == null)
                throw new NopException("Store not found.");

            // Check if the customer has items on the shopping cart.
            ProductCorporateFinanceValues tempProductCorporateFinanceValues;

            var shoppingCartItems = customer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart
                    && x.StoreId == _storeContext.CurrentStore.Id)
                .ToList()
                .Join(_shoppingCartItemCorporateFinanceOptionRepository.Table,
                    sci => sci.Id,
                    scf => scf.ShoppingCartItemId,
                    (sci, scf) => new
                    {
                        ShoppingCartItem = sci,
                        CorporateFinanceOption = scf.CorporateFinanceOption,
                        CorporateFinanceValues = tempProductCorporateFinanceValues = _corporateFinanceCalculationService.ComputeProductCorporateFinanceValues(sci.Product, sci.AttributesXml),
                        SelectedTermBreakdown = tempProductCorporateFinanceValues.TermsBreakdown.Single(t => t.DurationInMonths == scf.CorporateFinanceOption.DurationInMonths)
                    })
                .ToList();

            if (!shoppingCartItems.Any())
                throw new NopException("You do not have any product for financing placed on your shopping cart.");

            var signup = GetLatestCorporateSignupOfCustomer(customer.Id, store.Id);

            #region Form Completion Checks

            // Check if the current customer is allowed for corporate finance signup
            if (!CustomerCanSignup(out string notAllowedForSignupMessage, customer.Id))
                throw new NopException(notAllowedForSignupMessage);

            // Check if the latest signup has been completed.
            // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
            if (signup.SignupCompletedOnUtc.HasValue)
                throw new NopException("You already have finished signing up for financing.");

            // Check if basic details is okay
            var basicDetails = GetSignupBasicDetails(signup);
            if (basicDetails == null)
                throw new NopException("Please provide your basic details first.");

            // Check if director details is okay
            var directorDetails = GetSignupDirectorDetails(signup);
            if (directorDetails == null)
                throw new NopException("Please provide your director details first.");

            // Check if bank is okay
            var bank = GetSignupBankDetails(signup);
            if (bank == null)
                throw new NopException("Please provide your bank information first");

            #endregion

            _corporateFinanceOrderItemRepository.Delete(signup.OrderItems.ToList());
            signup = _corporateFinanceSignupRepository.GetById(signup.Id);

            // Register customer if not yet registered
            var existingCustomer = _customerService.GetCustomerByEmail(directorDetails.EmailAddress);
            if (existingCustomer == null)
            {
                var registrationResult = RegisterCustomer(customer, basicDetails, directorDetails);
                if (registrationResult.Success)
                {
                    // Clear the customer provided password
                    directorDetails.Password = null;
                    string basicDetailsText = JsonConvert.SerializeObject(basicDetails);
                    signup.CorporateFinanceBasicDetails = _encryptionService.EncryptText(basicDetailsText);
                }
            }

            // Save items
            shoppingCartItems.ForEach(x =>
            {
                signup.OrderItems.Add(new CorporateFinanceOrderItem
                {
                    CorporateFinanceSignupId = signup.Id,
                    CurrencyCode = x.CorporateFinanceValues.CurrencyCode,
                    DurationOfAgreement = x.SelectedTermBreakdown.DurationInMonths,
                    MonthlyPayment = x.SelectedTermBreakdown.MonthlyPayment,
                    NetRv = x.CorporateFinanceValues.NetRv,
                    ProcessingCost = x.CorporateFinanceValues.ProcessingCost,
                    ProductAttributeDescription = x.CorporateFinanceValues.ProductAttributeDescription,
                    ProductAttributesXml = x.ShoppingCartItem.AttributesXml,
                    ProductId = x.ShoppingCartItem.ProductId,
                    ProductPriceInclTax = x.CorporateFinanceValues.Price,
                    ProductQuantity = x.ShoppingCartItem.Quantity,
                    Rv = x.CorporateFinanceValues.Rv,
                    ShoppingCartItemId = x.ShoppingCartItem.Id,
                    ProductSku = x.CorporateFinanceValues.ProductSku,
                    TaxRate = x.CorporateFinanceValues.TaxRate,
                    InterestRate = x.CorporateFinanceValues.InterestRate,
                    InterestCharges = x.SelectedTermBreakdown.InterestCharges,
                    TotalPayable = x.SelectedTermBreakdown.TotalPayable
                });
            });

            signup.SignupCompletedOnUtc = DateTime.UtcNow;

            // Apply changes to signup record
            _corporateFinanceSignupRepository.Update(signup);

            return signup;
        }

        /// <summary>
        /// Checks if the customer is allowed to update the shopping cart
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        public virtual bool CustomerCanUpdateShoppingCart(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            storeId = storeId > 0 ? storeId : _storeContext.CurrentStore.Id;

            var shoppingCartItemIds = customer.ShoppingCartItems
                .Where(s => s.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart && s.StoreId == storeId)
                .Select(s => s.Id)
                .ToList();

            // Check if one of the shopping cart items exists on the corporate finance order item repository
            bool shoppingCartItemExists = _corporateFinanceOrderItemRepository.Table.Any(oi => oi.ShoppingCartItemId.HasValue && shoppingCartItemIds.Contains(oi.ShoppingCartItemId.Value));

            // If there are records, it means that the corporate has completed and passed 
            // the signup process and still needs to finish the following processes.
            return !shoppingCartItemExists;
        }

        /// <summary>
        /// Updates the Order item identifier associated with the corporate finance signup
        /// </summary>
        /// <param name="shoppingCartId">Shopping cart identifier</param>
        /// <param name="orderItemId">Order item identifier</param>
        public virtual void UpdateCorporateFinanceOrderItemOrderItemId(int shoppingCartId, int orderItemId)
        {
            var items = _corporateFinanceOrderItemRepository.Table.Where(i => i.ShoppingCartItemId == shoppingCartId).ToList();
            items.ForEach(s =>
            {
                s.OrderItemId = orderItemId;
                s.ShoppingCartItemId = null;
            });

            _corporateFinanceOrderItemRepository.Update(items);
        }

        /// <summary>
        /// Updates the order identifier associated with the corporate finance signup
        /// </summary>
        /// <param name="corporateFinanceSignupId">Corporate finance signup identifier</param>
        /// <param name="orderId">Order identifier</param>
        public virtual void UpdateCorporateFinanceSignupOrderId(int corporateFinanceSignupId, int orderId)
        {
            var signup = _corporateFinanceSignupRepository.GetById(corporateFinanceSignupId);
            if (signup == null)
                throw new NopException("Signup record not found.");

            signup.OrderId = orderId;
            _corporateFinanceSignupRepository.Update(signup);
        }

        /// <summary>
        /// Gets the corporate finance signup associated with the order
        /// </summary>
        /// <param name="orderId">Order identifier</param>
        public virtual CorporateFinanceSignup GetCorporateFinanceSignupByOrderId(int orderId)
        {
            return _corporateFinanceSignupRepository.Table.FirstOrDefault(s => s.OrderId == orderId);
        }

        /// <summary>
        /// Gets a corporate finance order item by order item identifier
        /// </summary>
        /// <param name="orderItemId">Order item identifier</param>
        public virtual CorporateFinanceOrderItem GetCorporateFinanceOrderItemByOrderItemId(int orderItemId)
        {
            return _corporateFinanceOrderItemRepository.Table.FirstOrDefault(oi => oi.OrderItemId == orderItemId);
        }

        /// <summary>
        /// Gets a corporate finance order items by order item identifiers
        /// </summary>
        /// <param name="orderItemIds">Order item identifiers</param>
        public virtual IEnumerable<CorporateFinanceOrderItem> GetCorporateFinanceOrderItemsByOrderItemIds(IEnumerable<int> orderItemIds)
        {
            return _corporateFinanceOrderItemRepository.Table.Where(x => x.OrderItemId.HasValue && orderItemIds.Contains(x.OrderItemId.Value));
        }

        /// <summary>
        /// Gets the list of corporate finance signup
        /// </summary>
        /// <param name="approvalStatuses">Approval status filter</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        public virtual IPagedList<CorporateFinanceSignupListItem> GetCorporateFinanceSignupList(IEnumerable<int> approvalStatuses,
            int pageIndex = 0,
            int pageSize = Int32.MaxValue)
        {
            // TODO do not encrypt the company details.

            var signupRecords = _corporateFinanceSignupRepository.Table.Where(x => x.SignupCompletedOnUtc.HasValue);

            if (approvalStatuses.Any())
            {
                bool containsUnknown = approvalStatuses.Contains((int)CorporateFinanceSignupStatus.Unknown);
                bool containsApproved = approvalStatuses.Contains((int)CorporateFinanceSignupStatus.Approved);
                bool containsDisapproved = approvalStatuses.Contains((int)CorporateFinanceSignupStatus.Disapproved);

                signupRecords = signupRecords.Where(x => (containsUnknown && !x.IsApproved.HasValue)
                    || (containsApproved && (x.IsApproved.HasValue && x.IsApproved.Value))
                    || (containsDisapproved && (x.IsApproved.HasValue && !x.IsApproved.Value)));
            }

            signupRecords = signupRecords.OrderBy(x => x.IsApproved)
                        .ThenByDescending(x => x.Id);

            var signupRecordsPagedList = new PagedList<CorporateFinanceSignup>(signupRecords, pageIndex, pageSize);

            // Get all stores so EF will not search from the database each time
            var stores = _storeService.GetAllStores();

            // Convert to CorporateFinanceSignupListItem
            var source = signupRecordsPagedList.Select(x => new CorporateFinanceSignupListItem
            {
                Id = x.Id,
                ApprovalStatus = x.IsApproved.HasValue
                    ? (x.IsApproved.Value ? CommonHelper.ConvertEnum(CorporateFinanceSignupStatus.Approved.ToString()) : CommonHelper.ConvertEnum(CorporateFinanceSignupStatus.Disapproved.ToString()))
                    : CommonHelper.ConvertEnum(CorporateFinanceSignupStatus.Unknown.ToString()),
                CompanyName = GetSignupBasicDetails(x).CompanyName,
                CreatedOn = x.CreatedOnUtc,
                StoreName = stores.FirstOrDefault(s => s.Id == x.StoreId)?.Name
            });

            return new PagedList<CorporateFinanceSignupListItem>(source, pageIndex, pageSize, signupRecordsPagedList.TotalCount);
        }

        /// <summary>
        /// Returns a corporate finance signup record
        /// </summary>
        /// <param name="id">Corporate finance signup identifier</param>
        public virtual CorporateFinanceSignup GetCorporateFinanceSignupById(int id)
        {
            return _corporateFinanceSignupRepository.GetById(id);
        }

        /// <summary>
        /// Sets the approval status of the corporate finance signup
        /// </summary>
        /// <param name="corporateFinanceSignupId">Corporate finance signup identifier</param>
        /// <param name="status">Corporate finance signup status</param>
        public virtual void SetCorporateFinanceSignupApprovalStatus(int corporateFinanceSignupId, CorporateFinanceSignupStatus status)
        {
            var signup = GetCorporateFinanceSignupById(corporateFinanceSignupId);

            signup.IsApproved = status == CorporateFinanceSignupStatus.Unknown 
                ? null 
                : (bool?)(status == CorporateFinanceSignupStatus.Approved ? true : false);

            signup.ApprovalProcessConductedByCustomerId = _workContext.CurrentCustomer.Id;
            signup.DateOfApprovalUtc = DateTime.UtcNow;

            if (signup.IsApproved.HasValue && signup.IsApproved.Value)
            {
                var envelopeStatus = SendCorporateFinanceAgreementDocuments(signup);
                signup.UpdatedOnUtc = DateTime.UtcNow;
                signup.DocuSignEnvelopeId = envelopeStatus.EnvelopeID;

                _corporateFinanceSignupRepository.Update(signup);

                _corporateFinanceWorkflowService.SendCorporateFinanceApprovedMessage(signup, GetSignupDirectorDetails(signup), _workContext.WorkingLanguage.Id);
            }
            else
            {
                _corporateFinanceSignupRepository.Update(signup);
            }
        }

        /// <summary>
        /// Check if the customer has a pending DocuSign Envelope to be signed
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        public virtual bool HasPendingDocuSignEnvelope(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            storeId = storeId > 0 ? storeId : _storeContext.CurrentStore.Id;

            // Check if current customer has item in cart
            if (!customer.ShoppingCartItems.Any(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart && x.StoreId == storeId))
                return false;

            // Check if customer is already done with finance application (Get latest corporate signup record)
            var signup = GetLatestCorporateSignupOfCustomer(customerId);
            if (signup == null)
            {
                return false;
            }
            else
            {
                // Check if has envelope
                if (!string.IsNullOrWhiteSpace(signup.DocuSignEnvelopeId))
                {
                    var docuSignStatus = _docuSignService.RequestEnvelopeStatus(signup.DocuSignEnvelopeId).Status;

                    // Check if pending, cancelled or completed
                    if (docuSignStatus == DocuSignWeb.EnvelopeStatusCode.Voided || docuSignStatus == DocuSignWeb.EnvelopeStatusCode.Completed || docuSignStatus == DocuSignWeb.EnvelopeStatusCode.Declined || docuSignStatus == DocuSignWeb.EnvelopeStatusCode.Deleted)
                        return false;
                    else
                        return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Returns the link to view the corporate finance agreement documents
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public virtual string GetOrderCorporateFinanceAgreementDocumentsLink(int orderId)
        {
            var signup = _corporateFinanceSignupRepository.Table.OrderByDescending(s => s.Id).FirstOrDefault(s => s.OrderId == orderId);
            if (signup == null || string.IsNullOrEmpty(signup.DocuSignEnvelopeId))
                return null;

            var directorDetails = GetSignupDirectorDetails(signup);
            if (directorDetails == null)
                return null;

            return _docuSignService.RequestRecipientToken(
                signup.DocuSignEnvelopeId,
                signup.CustomerId.ToString(),
                string.Join(" ", directorDetails.FirstName, directorDetails.LastName),
#if DEBUG
                "nplusterio@ecoasiatech.com",
#else
                directorDetails.EmailAddress,
#endif
                _storeContext.CurrentStore.Id);
        }

        #endregion
    }
}