﻿using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.Ecorenew.Finance.Services.Corporate
{
    /// <summary>
    /// Corporate finance calculation service interface
    /// </summary>
    public interface ICorporateFinanceCalculationService
    {
        /// <summary>
        /// Computes the finance prices and terms breakdown for a specific product
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="productAttributesXml">Product attributes in XML format</param>
        /// <param name="currencyCode">Currency code to be used. Leave as null to use the current working currency</param>
        ProductCorporateFinanceValues ComputeProductCorporateFinanceValues(Product product, string productAttributesXml, string currencyCode = null);
    }
}