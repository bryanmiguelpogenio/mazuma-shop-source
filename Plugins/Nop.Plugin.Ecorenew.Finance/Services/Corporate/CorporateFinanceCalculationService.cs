﻿using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Tax;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;

namespace Nop.Plugin.Ecorenew.Finance.Services.Corporate
{
    /// <summary>
    /// Corporate finance calculation service
    /// </summary>
    public class CorporateFinanceCalculationService : ICorporateFinanceCalculationService
    {
        #region Fields

        private readonly IRepository<CorporateFinanceOption> _corporateFinanceOptionRepository;
        private readonly IRepository<SkuCorporateFinanceRv> _tempSkuCorporateFinanceRvRepository;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ITaxService _taxService;
        private readonly IWorkContext _workContext;
        private readonly CorporateFinanceSettings _corporateFinanceSettings;

        #endregion

        #region Ctor

        public CorporateFinanceCalculationService(IRepository<CorporateFinanceOption> corporateFinanceOptionRepository,
            IRepository<SkuCorporateFinanceRv> tempSkuCorporateFinanceRvRepository,
            ICurrencyService currencyService,
            IPriceCalculationService priceCalculationService,
            IProductAttributeFormatter productAttributeFormatter,
            IProductAttributeParser productAttributeParser,
            ITaxService taxService,
            IWorkContext workContext,
            CorporateFinanceSettings corporateFinanceSettings)
        {
            this._corporateFinanceOptionRepository = corporateFinanceOptionRepository;
            this._tempSkuCorporateFinanceRvRepository = tempSkuCorporateFinanceRvRepository;
            this._currencyService = currencyService;
            this._priceCalculationService = priceCalculationService;
            this._productAttributeFormatter = productAttributeFormatter;
            this._productAttributeParser = productAttributeParser;
            this._taxService = taxService;
            this._workContext = workContext;
            this._corporateFinanceSettings = corporateFinanceSettings;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Computes the finance prices and terms breakdown for a specific product
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="productAttributesXml">Product attributes in XML format</param>
        /// <param name="currencyCode">Currency code to be used. Leave as null to use the current working currency</param>
        public virtual ProductCorporateFinanceValues ComputeProductCorporateFinanceValues(Product product, string productAttributesXml, string currencyCode = null)
        {
            var result = new ProductCorporateFinanceValues
            {
                //Apr = _corporateFinanceSettings.AprPercentage,
                ProductAttributeDescription = _productAttributeFormatter.FormatAttributes(product, productAttributesXml),
                ProductAttributesXml = productAttributesXml,
                ProductId = product.Id,
                ProductSku = product.FormatSku(productAttributesXml, _productAttributeParser)
            };

            var gbp = _currencyService.GetCurrencyByCode("GBP");

            // Get working currency
            var currencyUsed = currencyCode == null ? _workContext.WorkingCurrency : _currencyService.GetCurrencyByCode(currencyCode);
            result.CurrencyCode = currencyUsed.CurrencyCode;

            decimal sellingPriceInclTaxBase = _taxService.GetProductPrice(product, _priceCalculationService.GetUnitPrice(product, _workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, 1, productAttributesXml, decimal.Zero, null, null, false, out decimal discountAmount, out List<DiscountForCaching> discountsForCaching), out decimal taxRate);
            decimal sellingPriceInclTax = _currencyService.ConvertFromPrimaryStoreCurrency(sellingPriceInclTaxBase, currencyUsed);

            result.Price = sellingPriceInclTax;
            result.TaxRate = taxRate;
            result.InterestRate = _corporateFinanceSettings.InterestRate;
            result.ProcessingCost = _currencyService.ConvertCurrency(_corporateFinanceSettings.ProcessingCostInGbp, gbp, currencyUsed);

            // RV
            var tempSkuRv = _tempSkuCorporateFinanceRvRepository.Table.FirstOrDefault(x => x.Sku == result.ProductSku);

            result.Rv = tempSkuRv != null ? _currencyService.ConvertCurrency(tempSkuRv.RvInGbp, gbp, currencyUsed) : 0;
            result.NetRv = result.Rv - result.ProcessingCost;
            
            result.TermsBreakdown = new List<CorporateFinanceTermBreakdown>();

            _corporateFinanceOptionRepository.Table
                .Where(o => o.IsActive)
                .OrderBy(o => o.DurationInMonths)
                .ToList()
                .ForEach(o =>
                {
                    var breakdown = new CorporateFinanceTermBreakdown
                    {
                        DurationInMonths = o.DurationInMonths,
                        MonthlyPayment = RoundingHelper.RoundPrice((decimal)Financial.Pmt((double)(result.InterestRate/100) / 12, o.DurationInMonths, (double)result.Price * -1, (double)result.NetRv, DueDate.BegOfPeriod))
                    };

                    breakdown.InterestCharges = (breakdown.MonthlyPayment - ((result.Price - result.NetRv) / breakdown.DurationInMonths)) * breakdown.DurationInMonths;
                    breakdown.TotalPayable = breakdown.MonthlyPayment * breakdown.DurationInMonths;

                    result.TermsBreakdown.Add(breakdown);
                });

            return result;
        }

        #endregion
    }
}