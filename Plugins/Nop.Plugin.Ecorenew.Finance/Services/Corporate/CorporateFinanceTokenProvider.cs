﻿using Ecorenew.Common.DocuSign.Services;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using Nop.Services.Messages;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Services.Corporate
{
    public class CorporateFinanceTokenProvider : ICorporateFinanceTokenProvider
    {
        #region Fields

        private readonly IDocuSignService _docuSignService;

        #endregion

        #region Ctor

        public CorporateFinanceTokenProvider(IDocuSignService docuSignService)
        {
            this._docuSignService = docuSignService;
        }

        #endregion

        #region Methods

        public virtual void AddCorporateFinanceSignupTokens(IList<Token> tokens, CorporateFinanceSignup signup, CorporateFinanceDirectorDetails directorDetails, int storeId)
        {
            var docuSignLink = _docuSignService.RequestRecipientToken(
                    signup.DocuSignEnvelopeId,
                    signup.CustomerId.ToString(),
                    string.Join(" ", directorDetails.FirstName, directorDetails.LastName),
#if DEBUG
                        "nplusterio@ecoasiatech.com",
#else
                        directorDetails.EmailAddress,
#endif
                    storeId);

            tokens.Add(new Token("Signup.AgreementLink", docuSignLink));
        }

        #endregion
    }
}