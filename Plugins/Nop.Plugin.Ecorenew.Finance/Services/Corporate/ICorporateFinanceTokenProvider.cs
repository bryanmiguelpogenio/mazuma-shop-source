﻿using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using Nop.Services.Messages;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Services.Corporate
{
    public interface ICorporateFinanceTokenProvider
    {
        void AddCorporateFinanceSignupTokens(IList<Token> tokens, CorporateFinanceSignup signup, CorporateFinanceDirectorDetails directorDetails, int storeId);
    }
}