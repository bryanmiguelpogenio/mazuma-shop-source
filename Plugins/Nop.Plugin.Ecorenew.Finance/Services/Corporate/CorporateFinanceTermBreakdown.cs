﻿namespace Nop.Plugin.Ecorenew.Finance.Services.Corporate
{
    /// <summary>
    /// Represents a breakdown of prices and other details of a corporate finance term
    /// </summary>
    public class CorporateFinanceTermBreakdown
    {
        public int DurationInMonths { get; set; }

        public decimal MonthlyPayment { get; set; }
        public decimal InterestCharges { get; set; }
        public decimal TotalPayable { get; set; }
    }
}