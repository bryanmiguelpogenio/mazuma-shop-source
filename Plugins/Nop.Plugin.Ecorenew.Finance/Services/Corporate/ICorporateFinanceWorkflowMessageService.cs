﻿using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;

namespace Nop.Plugin.Ecorenew.Finance.Services.Corporate
{
    public interface ICorporateFinanceWorkflowMessageService
    {
        int SendCorporateFinanceApprovedMessage(CorporateFinanceSignup signup, CorporateFinanceDirectorDetails directorDetails, int languageId);
    }
}