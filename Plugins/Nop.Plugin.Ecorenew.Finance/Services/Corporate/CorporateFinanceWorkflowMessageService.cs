﻿using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Messages;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Services.Corporate
{
    public class CorporateFinanceWorkflowMessageService : WorkflowMessageService, ICorporateFinanceWorkflowMessageService
    {
        #region Fields

        private readonly ICorporateFinanceTokenProvider _corporateFinanceTokenProvider;
        private readonly ICustomerService _customerService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILanguageService _languageService;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly IStoreContext _storeContext;

        #endregion

        #region Ctor

        public CorporateFinanceWorkflowMessageService(ICorporateFinanceTokenProvider corporateFinanceTokenProvider,
            ICustomerService customerService,
            IEmailAccountService emailAccountService,
            IEventPublisher eventPublisher,
            ILanguageService languageService,
            IMessageTemplateService messageTemplateService,
            IMessageTokenProvider messageTokenProvider,
            IQueuedEmailService queuedEmailService,
            IStoreContext storeContext,
            IStoreService storeService,
            ITokenizer tokenizer,
            CommonSettings commonSettings,
            EmailAccountSettings emailAccountSettings)
            : base (messageTemplateService,
                  queuedEmailService,
                  languageService,
                  tokenizer,
                  emailAccountService,
                  messageTokenProvider,
                  storeService,
                  storeContext,
                  commonSettings,
                  emailAccountSettings,
                  eventPublisher)
        {
            this._corporateFinanceTokenProvider = corporateFinanceTokenProvider;
            this._customerService = customerService;
            this._eventPublisher = eventPublisher;
            this._languageService = languageService;
            this._messageTokenProvider = messageTokenProvider;
            this._storeContext = storeContext;
        }

        #endregion

        #region Methods

        public virtual int SendCorporateFinanceApprovedMessage(CorporateFinanceSignup signup, CorporateFinanceDirectorDetails directorDetails, int languageId)
        {
            if (signup == null)
                throw new ArgumentNullException(nameof(signup));

            var store = _storeContext.CurrentStore;
            languageId = EnsureLanguageIsActive(languageId, store.Id);

            var customer = _customerService.GetCustomerById(signup.CustomerId);

            var messageTemplate = GetActiveMessageTemplate(CorporateMessageTemplateSystemNames.CorporateSignupApporvedCustomerNotification, store.Id);
            if (messageTemplate == null)
                return 0;

            //email account
            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

            //tokens
            var tokens = new List<Token>();
            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            _messageTokenProvider.AddCustomerTokens(tokens, customer);
            _corporateFinanceTokenProvider.AddCorporateFinanceSignupTokens(tokens, signup, directorDetails, store.Id);

            //event notification
            _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

#if DEBUG
            var toEmail = "nplusterio@ecoasiatech.com";
#else
            var toEmail = directorDetails.EmailAddress;
#endif
            var toName = string.Join(" ", directorDetails.FirstName, directorDetails.LastName);

            return SendNotification(messageTemplate, emailAccount, languageId, tokens, toEmail, toName);
        }

        #endregion
    }
}