﻿using Nop.Core;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Services.Corporate
{
    /// <summary>
    /// Corporate finance service interface
    /// </summary>
    public interface ICorporateFinanceService
    {
        /// <summary>
        /// Maps the customer's shopping cart items to their selected finance option
        /// </summary>
        /// <param name="corporateFinanceOptionId">Corporate finance option identifier</param>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        void MapCustomerShoppingCartItemsToFinanceOption(int corporateFinanceOptionId, int customerId = 0, int storeId = 0);

        /// <summary>
        /// Clear the customer's shopping cart item finance option mappings
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        void ClearCustomerShoppingCartItemFinanceOptions(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Gets the customer's shopping cart finance option mappings
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identiifer</param>
        IEnumerable<ShoppingCartItemCorporateFinanceOption> GetCustomerShoppingCartItemCorporateFinanceOptions(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Creates a new shopping cart item finance option mapping
        /// </summary>
        /// <param name="shoppingCartItemId">Shopping cart item identifier</param>
        /// <param name="corporateFinanceOptionId">Corporate finance option identifier</param>
        void CreateShoppingCartItemFinanceOption(int shoppingCartItemId, int corporateFinanceOptionId);

        /// <summary>
        /// Deletes a shopping cart item finance option mapping
        /// </summary>
        /// <param name="shoppingCartItemId">Shopping cart item identifier</param>
        void DeleteShoppingCartItemFinanceOption(int shoppingCartItemId);

        /// <summary>
        /// Determines if the customer can signup for finance
        /// </summary>
        /// <param name="notAllowedMessage">Message if not allowed for finance signup</param>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        bool CustomerCanSignup(out string notAllowedMessage, int customerId = 0);

        /// <summary>
        /// Returns the latest corporate finance signup record of a customer.
        /// </summary>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        /// <param name="storeId">Store identifier. Will use the current store if not provided</param>
        CorporateFinanceSignup GetLatestCorporateSignupOfCustomer(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Creates a new corporate finance signup record for a customer
        /// </summary>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        /// <param name="storeId">Store identifier. Will use the current store if not provided</param>
        CorporateFinanceSignup CreateNewCorporateFinanceSignup(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Returns all marital statuses
        /// </summary>
        IEnumerable<MaritalStatus> GetAllMaritalStatuses();

        /// <summary>
        /// Returns all number of dependents
        /// </summary>
        IEnumerable<NumberOfDependents> GetAllNumberOfDependents();

        /// <summary>
        /// Returns all home owner statuses
        /// </summary>
        IEnumerable<HomeOwnerStatus> GetAllHomeOwnerStatuses();

        /// <summary>
        /// Returns corporate finance options
        /// </summary>
        /// <param name="includeInactive">Include inactive corporate finance options?</param>
        IEnumerable<CorporateFinanceOption> GetCorporateFinanceOptions(bool includeInactive = false);

        /// <summary>
        /// Gets a corporate finance option
        /// </summary>
        /// <param name="id">Corporate finance option identifier</param>
        CorporateFinanceOption GetCorporateFinanceOptionById(int id);

        /// <summary>
        /// Inserts a new corporate finance option
        /// </summary>
        /// <param name="corporateFinanceOption">Corporate finance option</param>
        void InsertCorporateFinanceOption(CorporateFinanceOption corporateFinanceOption);

        /// <summary>
        /// Updates a corporate finance option
        /// </summary>
        /// <param name="corporateFinanceOption">Corporate finance option</param>
        void UpdateCorporateFinanceOption(CorporateFinanceOption corporateFinanceOption);

        /// <summary>
        /// Gets the corporate finance basic details of the signup record
        /// </summary>
        /// <param name="corporateFinanceSignup">Corporate finance signup</param>
        CorporateFinanceBasicDetails GetSignupBasicDetails(CorporateFinanceSignup corporateFinanceSignup);

        /// <summary>
        /// Sets the corporate finance basic details of the signup record
        /// </summary>
        /// <param name="corporateFinanceSignupId">Corporate finance signup identifier</param>
        /// <param name="basicDetails">Personal information</param>
        void SetSignupBasicDetails(int corporateFinanceSignupId, CorporateFinanceBasicDetails basicDetails);

        /// <summary>
        /// Gets the corporate finance director details of the signup record
        /// </summary>
        /// <param name="corporateFinanceSignup">Corporate finance signup</param>
        CorporateFinanceDirectorDetails GetSignupDirectorDetails(CorporateFinanceSignup corporateFinanceSignup);

        /// <summary>
        /// Sets the corporate finance director details of the signup record
        /// </summary>
        /// <param name="corporateFinanceSignupId">Corporate finance signup identifier</param>
        /// <param name="basicDetails">Personal information</param>
        void SetSignupDirectorDetails(int corporateFinanceSignupId, CorporateFinanceDirectorDetails basicDetails);

        /// <summary>
        /// Gets the corporate finance bank of the signup record
        /// </summary>
        /// <param name="corporateFinanceSignup">Corporate finance signup</param>
        CorporateFinanceBankDetails GetSignupBankDetails(CorporateFinanceSignup corporateFinanceSignup);

        /// <summary>
        /// Sets the corporate finance bank of the signup record
        /// </summary>
        /// <param name="corporateFinanceSignupId">Corporate finance signup identifier</param>
        /// <param name="basicDetails">Personal information</param>
        void SetSignupBankDetails(int corporateFinanceSignupId, CorporateFinanceBankDetails basicDetails);

        /// <summary>
        /// Complete the corporate finance signup.
        /// This will perform all the necessary processes for validating the corporate's affordability, identity, etc.
        /// This will also create a new DocuSign envelope to be signed by the corporate once all the checks have been marked as passed
        /// </summary>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        /// <param name="storeId">Store identifier. Will use the current store if not provided</param>
        CorporateFinanceSignup CompleteCorporateFinanceSignup(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Checks if the customer is allowed to update the shopping cart
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        bool CustomerCanUpdateShoppingCart(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Updates the Order item identifier associated with the corporate finance signup
        /// </summary>
        /// <param name="shoppingCartId">Shopping cart identifier</param>
        /// <param name="orderItemId">Order item identifier</param>
        void UpdateCorporateFinanceOrderItemOrderItemId(int shoppingCartId, int orderItemId);

        /// <summary>
        /// Updates the order identifier associated with the corporate finance signup
        /// </summary>
        /// <param name="corporateFinanceSignupId">Corporate finance signup identifier</param>
        /// <param name="orderId">Order identifier</param>
        void UpdateCorporateFinanceSignupOrderId(int corporateFinanceSignupId, int orderId);

        /// <summary>
        /// Gets the corporate finance signup associated with the order
        /// </summary>
        /// <param name="orderId">Order identifier</param>
        CorporateFinanceSignup GetCorporateFinanceSignupByOrderId(int orderId);

        /// <summary>
        /// Gets a corporate finance order item by order item identifier
        /// </summary>
        /// <param name="orderItemId">Order item identifier</param>
        CorporateFinanceOrderItem GetCorporateFinanceOrderItemByOrderItemId(int orderItemId);

        /// <summary>
        /// Gets a corporate finance order items by order item identifiers
        /// </summary>
        /// <param name="orderItemIds">Order item identifiers</param>
        IEnumerable<CorporateFinanceOrderItem> GetCorporateFinanceOrderItemsByOrderItemIds(IEnumerable<int> orderItemIds);

        /// <summary>
        /// Gets the list of corporate finance signup
        /// </summary>
        /// <param name="approvalStatuses">Approval status filter</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        IPagedList<CorporateFinanceSignupListItem> GetCorporateFinanceSignupList(IEnumerable<int> approvalStatuses,
            int pageIndex = 0,
            int pageSize = Int32.MaxValue);

        /// <summary>
        /// Returns a corporate finance signup record
        /// </summary>
        /// <param name="id">Corporate finance signup identifier</param>
        CorporateFinanceSignup GetCorporateFinanceSignupById(int id);
        
        /// <summary>
        /// Sets the approval status of the corporate finance signup
        /// </summary>
        /// <param name="corporateFinanceSignupId">Corporate finance signup identifier</param>
        /// <param name="status">Corporate finance signup status</param>
        void SetCorporateFinanceSignupApprovalStatus(int corporateFinanceSignupId, CorporateFinanceSignupStatus status);

        /// <summary>
        /// Check if the customer has a pending DocuSign Envelope to be signed
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        bool HasPendingDocuSignEnvelope(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Returns the link to view the corporate finance agreement documents
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        string GetOrderCorporateFinanceAgreementDocumentsLink(int orderId);
    }
}