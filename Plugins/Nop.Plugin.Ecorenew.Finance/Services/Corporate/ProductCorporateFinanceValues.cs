﻿using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Services.Corporate
{
    /// <summary>
    /// Represents the computed values (eg. monthly payment per term, deposit etc) for a product.
    /// Normally will be based on the product SKU
    /// </summary>
    [Serializable]
    public class ProductCorporateFinanceValues
    {
        public ProductCorporateFinanceValues()
        {
            this.TermsBreakdown = new List<CorporateFinanceTermBreakdown>();
        }

        public string ProductSku { get; set; }
        public int ProductId { get; set; }
        public string ProductAttributesXml { get; set; }
        public string ProductAttributeDescription { get; set; }

        public string CurrencyCode { get; set; }
        public decimal Price { get; set; }
        public decimal TaxRate { get; set; }
        public decimal InterestRate { get; set; }
        public decimal Rv { get; set; }
        public decimal ProcessingCost { get; set; }
        public decimal NetRv { get; set; }

        public List<CorporateFinanceTermBreakdown> TermsBreakdown { get; set; }
    }
}