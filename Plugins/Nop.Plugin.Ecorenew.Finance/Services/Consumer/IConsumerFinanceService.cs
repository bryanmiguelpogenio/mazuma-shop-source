﻿using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Services.Consumer
{
    /// <summary>
    /// Consumer finance service interface
    /// </summary>
    public interface IConsumerFinanceService
    {
        /// <summary>
        /// Maps the customer's shopping cart items to their selected finance option
        /// </summary>
        /// <param name="consumerFinanceOptionId">Consumer finance option identifier</param>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        void MapCustomerShoppingCartItemsToFinanceOption(int consumerFinanceOptionId, int customerId = 0, int storeId = 0);

        /// <summary>
        /// Clear the customer's shopping cart item finance option mappings
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        void ClearCustomerShoppingCartItemFinanceOptions(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Gets the customer's shopping cart finance option mappings
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identiifer</param>
        IEnumerable<ShoppingCartItemConsumerFinanceOption> GetCustomerShoppingCartItemConsumerFinanceOptions(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Creates a new shopping cart item finance option mapping
        /// </summary>
        /// <param name="shoppingCartItemId">Shopping cart item identifier</param>
        /// <param name="consumerFinanceOptionId">Consumer finance option identifier</param>
        void CreateShoppingCartItemFinanceOption(int shoppingCartItemId, int consumerFinanceOptionId);

        /// <summary>
        /// Deletes a shopping cart item finance option mapping
        /// </summary>
        /// <param name="shoppingCartItemId">Shopping cart item identifier</param>
        void DeleteShoppingCartItemFinanceOption(int shoppingCartItemId);

        /// <summary>
        /// Determines if the customer can signup for finance
        /// </summary>
        /// <param name="notAllowedMessage">Message if not allowed for finance signup</param>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        bool CustomerCanSignup(out string notAllowedMessage, int customerId = 0);

        /// <summary>
        /// Returns the latest consumer finance signup record of a customer.
        /// </summary>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        /// <param name="storeId">Store identifier. Will use the current store if not provided</param>
        ConsumerFinanceSignup GetLatestConsumerSignupOfCustomer(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Creates a new consumer finance signup record for a customer
        /// </summary>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        /// <param name="storeId">Store identifier. Will use the current store if not provided</param>
        ConsumerFinanceSignup CreateNewConsumerFinanceSignup(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Returns all marital statuses
        /// </summary>
        IEnumerable<MaritalStatus> GetAllMaritalStatuses();

        /// <summary>
        /// Returns all number of dependents
        /// </summary>
        IEnumerable<NumberOfDependents> GetAllNumberOfDependents();

        /// <summary>
        /// Returns all home owner statuses
        /// </summary>
        IEnumerable<HomeOwnerStatus> GetAllHomeOwnerStatuses();

        /// <summary>
        /// Returns all employment statuses
        /// </summary>
        IEnumerable<EmploymentStatus> GetAllEmploymentStatuses();

        /// <summary>
        /// Returns all annual gross income
        /// </summary>
        IEnumerable<AnnualGrossIncome> GetAllAnnualGrossIncome();

        /// <summary>
        /// Returns all expenditure categories
        /// </summary>
        IEnumerable<ExpenditureCategory> GetAllConsumerExpenditureCategories();

        /// <summary>
        /// Returns expenditure category ranges
        /// </summary>
        /// <param name="expenditureCategoryId">Expenditure category filter</param>
        IEnumerable<ExpenditureCategoryRange> GetExpenditureCategoryRanges(int expenditureCategoryId = 0);

        /// <summary>
        /// Returns consumer finance options
        /// </summary>
        /// <param name="includeInactive">Include inactive consumer finance options?</param>
        IEnumerable<ConsumerFinanceOption> GetConsumerFinanceOptions(bool includeInactive = false);

        /// <summary>
        /// Gets a consumer finance option
        /// </summary>
        /// <param name="id">Consumer finance option identifier</param>
        ConsumerFinanceOption GetConsumerFinanceOptionById(int id);

        /// <summary>
        /// Inserts a new consumer finance option
        /// </summary>
        /// <param name="consumerFinanceOption">Consumer finance option</param>
        void InsertConsumerFinanceOption(ConsumerFinanceOption consumerFinanceOption);

        /// <summary>
        /// Updates a consumer finance option
        /// </summary>
        /// <param name="consumerFinanceOption">Consumer finance option</param>
        void UpdateConsumerFinanceOption(ConsumerFinanceOption consumerFinanceOption);

        /// <summary>
        /// Returns an expenditure range
        /// </summary>
        /// <param name="expenditureRangeId">Expenditure range identifier</param>
        ExpenditureRange GetConsumerExpenditureRangeById(int expenditureRangeId);

        /// <summary>
        /// Gets the consumer finance personal information of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignup">Consumer finance signup</param>
        ConsumerFinancePersonalInformation GetSignupPersonalInformation(ConsumerFinanceSignup consumerFinanceSignup);

        /// <summary>
        /// Sets the consumer finance personal information of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignupId">Consumer finance signup identifier</param>
        /// <param name="personalInformation">Personal information</param>
        void SetSignupPersonalInformation(int consumerFinanceSignupId, ConsumerFinancePersonalInformation personalInformation);

        /// <summary>
        /// Gets the consumer finance home address of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignup">Consumer finance signup</param>
        ConsumerFinanceHomeAddress GetSignupHomeAddress(ConsumerFinanceSignup consumerFinanceSignup);

        /// <summary>
        /// Sets the consumer finance home address of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignupId">Consumer finance signup identifier</param>
        /// <param name="personalInformation">Personal information</param>
        void SetSignupHomeAddress(int consumerFinanceSignupId, ConsumerFinanceHomeAddress personalInformation);

        /// <summary>
        /// Gets the consumer finance employment of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignup">Consumer finance signup</param>
        ConsumerFinanceEmployment GetSignupEmployment(ConsumerFinanceSignup consumerFinanceSignup);

        /// <summary>
        /// Sets the consumer finance employment of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignupId">Consumer finance signup identifier</param>
        /// <param name="personalInformation">Personal information</param>
        void SetSignupEmployment(int consumerFinanceSignupId, ConsumerFinanceEmployment personalInformation);

        /// <summary>
        /// Gets the consumer finance income and expenditure of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignup">Consumer finance signup</param>
        ConsumerFinanceIncomeAndExpenditure GetSignupIncomeAndExpenditure(ConsumerFinanceSignup consumerFinanceSignup);

        /// <summary>
        /// Sets the consumer finance income and expenditure of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignupId">Consumer finance signup identifier</param>
        /// <param name="personalInformation">Personal information</param>
        void SetSignupIncomeAndExpenditure(int consumerFinanceSignupId, ConsumerFinanceIncomeAndExpenditure personalInformation);

        /// <summary>
        /// Gets the consumer finance bank of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignup">Consumer finance signup</param>
        ConsumerFinanceBank GetSignupBank(ConsumerFinanceSignup consumerFinanceSignup);

        /// <summary>
        /// Sets the consumer finance bank of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignupId">Consumer finance signup identifier</param>
        /// <param name="personalInformation">Personal information</param>
        void SetSignupBank(int consumerFinanceSignupId, ConsumerFinanceBank personalInformation);

        /// <summary>
        /// Complete the consumer finance signup.
        /// This will perform all the necessary processes for validating the consumer's affordability, identity, etc.
        /// This will also create a new DocuSign envelope to be signed by the consumer once all the checks have been marked as passed
        /// </summary>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        ConsumerFinanceSignup CompleteConsumerFinanceSignup(int customerId = 0);

        /// <summary>
        /// Checks if the customer is allowed to update the shopping cart
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        bool CustomerCanUpdateShoppingCart(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Updates the Order item identifier associated with the consumer finance signup
        /// </summary>
        /// <param name="shoppingCartId">Shopping cart identifier</param>
        /// <param name="orderItemId">Order item identifier</param>
        void UpdateConsumerFinanceOrderItemOrderItemId(int shoppingCartId, int orderItemId);

        /// <summary>
        /// Updates the order identifier associated with the consumer finance signup
        /// </summary>
        /// <param name="consumerFinanceSignupId">Consumer finance signup identifier</param>
        /// <param name="orderId">Order identifier</param>
        void UpdateConsumerFinanceSignupOrderId(int consumerFinanceSignupId, int orderId);

        /// <summary>
        /// Gets the consumer finance signup associated with the order
        /// </summary>
        /// <param name="orderId">Order identifier</param>
        ConsumerFinanceSignup GetConsumerFinanceSignupByOrderId(int orderId);

        /// <summary>
        /// Gets the consumer finance signups associated with the provided orders
        /// </summary>
        /// <param name="orderIds">Order identifiers</param>
        IEnumerable<ConsumerFinanceSignup> GetConsumerFinanceSignupsByOrderIds(IEnumerable<int> orderIds);

        /// <summary>
        /// Gets a consumer finance order item by order item identifier
        /// </summary>
        /// <param name="orderItemId">Order item identifier</param>
        ConsumerFinanceOrderItem GetConsumerFinanceOrderItemByOrderItemId(int orderItemId);

        /// <summary>
        /// Gets a consumer finance order items by order item identifiers
        /// </summary>
        /// <param name="orderItemIds">Order item identifiers</param>
        IEnumerable<ConsumerFinanceOrderItem> GetConsumerFinanceOrderItemsByOrderItemIds(IEnumerable<int> orderItemIds);

        /// <summary>
        /// Check if the customer has a pending DocuSign Envelope to be signed
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        bool HasPendingDocuSignEnvelope(int customerId = 0, int storeId = 0);

        /// <summary>
        /// Returns the link to view the consumer finance agreement documents
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        string GetOrderConsumerFinanceAgreementDocumentsLink(int orderId);
    }
}