﻿using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Services.Consumer
{
    /// <summary>
    /// Represents the computed values (eg. monthly payment per term, deposit etc) for a product.
    /// Normally will be based on the product SKU
    /// </summary>
    [Serializable]
    public class ProductConsumerFinanceValues
    {
        public ProductConsumerFinanceValues()
        {
            this.TermsBreakdown = new List<ConsumerFinanceTermBreakdown>();
        }

        public string ProductSku { get; set; }
        public int ProductId { get; set; }
        public string ProductAttributesXml { get; set; }
        public string ProductAttributeDescription { get; set; }

        public string CurrencyCode { get; set; }
        public decimal Price { get; set; }
        public decimal InitialPayment { get; set; }
        public decimal DocumentationFee { get; set; }
        public decimal TaxRate { get; set; }
        public decimal Apr { get; set; }
        public decimal OptionToPurchaseFee { get; set; }

        public List<ConsumerFinanceTermBreakdown> TermsBreakdown { get; set; }
    }
}