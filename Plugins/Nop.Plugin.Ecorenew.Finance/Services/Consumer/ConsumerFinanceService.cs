﻿using Callcredit.CallReport7;
using CallCredit.CallValidateApi.RequestEntities;
using DocuSignWeb = Ecorenew.Common.DocuSignWeb;
using Ecorenew.Common.CallCredit.Domain;
using Ecorenew.Common.CallCredit.Services;
using Ecorenew.Common.DocuSign.Services;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Ecorenew.Common.Utilities;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Services.Authentication;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Nop.Services.Stores;

namespace Nop.Plugin.Ecorenew.Finance.Services.Consumer
{
    /// <summary>
    /// Consumer finance service
    /// </summary>
    public class ConsumerFinanceService : IConsumerFinanceService
    {
        #region Fields

        private readonly IRepository<AnnualGrossIncome> _annualGrossIncomeRepository;
        private readonly IRepository<ConsumerFinanceOption> _consumerFinanceOptionRepository;
        private readonly IRepository<ConsumerFinanceOrderItem> _consumerFinanceOrderItemRepository;
        private readonly IRepository<ConsumerFinanceSignup> _consumerFinanceSignupRepository;
        private readonly IRepository<EmploymentStatus> _employmentStatusRepository;
        private readonly IRepository<ExpenditureCategory> _expenditureCategoryRepository;
        private readonly IRepository<ExpenditureRange> _expenditureRangeRepository;
        private readonly IRepository<ExpenditureCategoryRange> _expenditureCategoryRangeRepository;
        private readonly IRepository<HomeOwnerStatus> _homeOwnerStatusRepository;
        private readonly IRepository<MaritalStatus> _maritalStatusRepository;
        private readonly IRepository<NumberOfDependents> _numberOfDependentsRepository;
        private readonly IRepository<ShoppingCartItem> _shoppingCartItemRepository;
        private readonly IRepository<ShoppingCartItemConsumerFinanceOption> _shoppingCartItemConsumerFinanceOptionRepository;
        private readonly IAddressService _addressService;
        private readonly IAuthenticationService _authenticationService;
        private readonly ICallReportService _callReportService;
        private readonly ICallValidateService _callValidateService;
        private readonly IConsumerFinanceCalculationService _consumerFinanceCalculationService;
        private readonly ICurrencyService _currencyService;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly ICustomerService _customerService;
        private readonly IDocuSignService _docuSignService;
        private readonly IEncryptionService _encryptionService;
        private readonly IEventPublisher _eventPublisher;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILogger _logger;
        private readonly IOrderService _orderService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IProductService _productService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly ConsumerFinanceSettings _consumerFinanceSettings;
        private readonly CustomerSettings _customerSettings;
        private readonly LocalizationSettings _localizationSettings;

        #endregion

        #region Ctor

        public ConsumerFinanceService(IRepository<AnnualGrossIncome> annualGrossIncomeRepository,
            IRepository<ConsumerFinanceOption> consumerFinanceOptionRepository,
            IRepository<ConsumerFinanceOrderItem> consumerFinanceOrderItemRepository,
            IRepository<ConsumerFinanceSignup> consumerFinanceSignupRepository,
            IRepository<EmploymentStatus> employmentStatusRepository,
            IRepository<ExpenditureCategory> expenditureCategoryRepository,
            IRepository<ExpenditureRange> expenditureRangeRepository,
            IRepository<ExpenditureCategoryRange> expenditureCategoryRangeRepository,
            IRepository<HomeOwnerStatus> homeOwnerStatusRepository,
            IRepository<MaritalStatus> maritalStatusRepository,
            IRepository<NumberOfDependents> numberOfDependentsRepository,
            IRepository<ShoppingCartItem> shoppingCartItemRepository,
            IRepository<ShoppingCartItemConsumerFinanceOption> shoppingCartItemConsumerFinanceOptionRepository,
            IAddressService addressService,
            IAuthenticationService authenticationService,
            ICallReportService callReportService,
            ICallValidateService callValidateService,
            IConsumerFinanceCalculationService consumerFinanceCalculationService,
            ICurrencyService currencyService,
            ICustomerRegistrationService customerRegistrationService,
            ICustomerService customerService,
            IDocuSignService docuSignService,
            IEncryptionService encryptionService,
            IEventPublisher eventPublisher,
            IGenericAttributeService genericAttributeService,
            ILogger logger,
            IOrderService orderService,
            IPriceFormatter priceFormatter,
            IProductAttributeParser productAttributeParser,
            IProductService productService,
            IStoreContext storeContext,
            IStoreService storeService,
            IWebHelper webHelper,
            IWorkContext workContext,
            IWorkflowMessageService workflowMessageService,
            ConsumerFinanceSettings consumerFinanceSettings,
            CustomerSettings customerSettings,
            LocalizationSettings localizationSettings)
        {
            this._annualGrossIncomeRepository = annualGrossIncomeRepository;
            this._consumerFinanceOptionRepository = consumerFinanceOptionRepository;
            this._consumerFinanceOrderItemRepository = consumerFinanceOrderItemRepository;
            this._consumerFinanceSignupRepository = consumerFinanceSignupRepository;
            this._employmentStatusRepository = employmentStatusRepository;
            this._expenditureCategoryRepository = expenditureCategoryRepository;
            this._expenditureRangeRepository = expenditureRangeRepository;
            this._expenditureCategoryRangeRepository = expenditureCategoryRangeRepository;
            this._homeOwnerStatusRepository = homeOwnerStatusRepository;
            this._maritalStatusRepository = maritalStatusRepository;
            this._numberOfDependentsRepository = numberOfDependentsRepository;
            this._shoppingCartItemRepository = shoppingCartItemRepository;
            this._shoppingCartItemConsumerFinanceOptionRepository = shoppingCartItemConsumerFinanceOptionRepository;
            this._addressService = addressService;
            this._authenticationService = authenticationService;
            this._callReportService = callReportService;
            this._callValidateService = callValidateService;
            this._consumerFinanceCalculationService = consumerFinanceCalculationService;
            this._currencyService = currencyService;
            this._customerRegistrationService = customerRegistrationService;
            this._customerService = customerService;
            this._docuSignService = docuSignService;
            this._encryptionService = encryptionService;
            this._eventPublisher = eventPublisher;
            this._genericAttributeService = genericAttributeService;
            this._logger = logger;
            this._orderService = orderService;
            this._priceFormatter = priceFormatter;
            this._productAttributeParser = productAttributeParser;
            this._productService = productService;
            this._storeContext = storeContext;
            this._storeService = storeService;
            this._webHelper = webHelper;
            this._workContext = workContext;
            this._workflowMessageService = workflowMessageService;
            this._consumerFinanceSettings = consumerFinanceSettings;
            this._customerSettings = customerSettings;
            this._localizationSettings = localizationSettings;
        }

        #endregion

        #region Utilities

        protected virtual CT_SearchDefinition InitializeCallReportSearchDefinition(
            ConsumerFinancePersonalInformation personalInformation, 
            ConsumerFinanceHomeAddress homeAddress)
        {
            var srequest = new CT_searchrequest
            {
                purpose = "CA",
                score = Convert.ToByte(true),
                scoreSpecified = true,
                transient = 0,
                transientSpecified = true,
                schemaversion = "7.1",
                datasets = 511
            };

            var searchDef = new CT_SearchDefinition
            {
                creditrequest = srequest
            };

            var apiApplicant = new CT_searchapplicant
            {
                dob = personalInformation.DateOfBirth,
                dobSpecified = true,
                hho = Convert.ToByte(false),
                hhoSpecified = true,
                tpoptout = Convert.ToByte(true),
                tpoptoutSpecified = true
            };

            srequest.applicant = new CT_searchapplicant[] { apiApplicant };

            var apiName = new CT_inputname
            {
                title = personalInformation.Title,
                forename = personalInformation.FirstName,
                othernames = personalInformation.MiddleName,
                surname = personalInformation.LastName
            };

            apiApplicant.name = new CT_inputname[] { apiName };

            var apiInputCurrentAddress = new CT_inputaddress
            {
                buildingno = homeAddress.CurrentAddress.BuildingNumber,
                postcode = homeAddress.CurrentAddress.Postcode
            };

            CT_inputaddress apiInputPreviousAddress = null;
            if (!string.IsNullOrWhiteSpace(homeAddress.PreviousAddress.BuildingNumber) && !string.IsNullOrWhiteSpace(homeAddress.PreviousAddress.Postcode))
            {
                apiInputPreviousAddress = new CT_inputaddress
                {
                    buildingno = homeAddress.PreviousAddress.BuildingNumber,
                    postcode = homeAddress.PreviousAddress.Postcode
                };
            }

            if (apiInputPreviousAddress == null)
                apiApplicant.address = new CT_inputaddress[] { apiInputCurrentAddress };
            else
                apiApplicant.address = new CT_inputaddress[] { apiInputCurrentAddress, apiInputPreviousAddress };

            // Add any supplied demographics
            // NOTE: the 'type' values indicated below (e.g. inputEmail.type) can be obtained
            // from the reference data supplied by Callcredit. For an example of how to download
            // this reference data, see the sample 'LookupData' project
            var apiInputDemographics = new CT_applicantdemographics
            {
                contact = new CT_applicantdemographicsContact
                {
                    email = new CT_demographicsemail[]
                    {
                        new CT_demographicsemail
                        {
                            address = personalInformation.Email,
                            type = "03" // Home Email - Main ISP
                        }
                    }
                    //telephone = new CT_demographicstelephone[]
                    //{
                    //    new CT_demographicstelephone
                    //    {
                    //        number = personalInformation.HomePhoneNumber,
                    //        std = personalInformation.HomePhoneStd,
                    //        type = "13" // Own Home - 1st Line
                    //    },
                    //    new CT_demographicstelephone
                    //    {
                    //        number = personalInformation.MobilePhoneNumber,
                    //        std = personalInformation.MobilePhoneStd,
                    //        type = "15" // Own Mobile
                    //    }
                    //}
                }
            };

            apiApplicant.applicantdemographics = apiInputDemographics;

            return searchDef;
        }

        protected virtual CustomerRegistrationResult RegisterCustomer(
            Customer customer, 
            ConsumerFinancePersonalInformation personalInformation, 
            ConsumerFinanceHomeAddress homeAddress, 
            ConsumerFinanceEmployment employment)
        {
            //string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            //StringBuilder res = new StringBuilder();
            //Random rnd = new Random();
            //int length = 6;
            //while (0 < length--)
            //{
            //    res.Append(valid[rnd.Next(valid.Length)]);
            //}
            //string password = res.ToString();

            if (string.IsNullOrWhiteSpace(personalInformation.Password))
            {
                _logger.Error("Null or white space password received.");
                throw new NopException("Unexpected error occurred.");
            }

            bool isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;

            var registrationRequest = new CustomerRegistrationRequest(customer,
                personalInformation.Email,
                personalInformation.Email,
                personalInformation.Password,
                _customerSettings.DefaultPasswordFormat,
                _storeContext.CurrentStore.Id,
                isApproved);

            var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);

            if (registrationResult.Success)
            {
                //form fields
                if (_customerSettings.GenderEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, personalInformation.Title.Trim().ToLower() == "mr" ? "M" : "F");

                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, personalInformation.FirstName.Trim());
                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, personalInformation.LastName.Trim());

                if (_customerSettings.DateOfBirthEnabled)
                {
                    DateTime? dateOfBirth = personalInformation.DateOfBirth;
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, dateOfBirth);
                }
                if (_customerSettings.CompanyEnabled && employment.EmploymentStatusId == 1)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, employment.EmployedEmployerName.Trim());

                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, (homeAddress.CurrentAddress.BuildingNumber + " " + homeAddress.CurrentAddress.Address1).Trim());

                if (!string.IsNullOrWhiteSpace(homeAddress.CurrentAddress.Address2))
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, homeAddress.CurrentAddress.Address2.Trim());

                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, homeAddress.CurrentAddress.Postcode.Trim());
                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, homeAddress.CurrentAddress.CityTown.Trim());
                _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, homeAddress.CurrentAddress.CountryId);

                if (_customerSettings.PhoneEnabled)
                    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, (personalInformation.MobilePhoneNumber ?? "").Trim());

                // Insert default address
                var defaultAddress = new Nop.Core.Domain.Common.Address
                {
                    FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                    LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
                    Email = customer.Email,
                    Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
                    CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) > 0 ?
                        (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) : null,
                    StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) > 0 ?
                        (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) : null,
                    City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City),
                    Address1 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress),
                    Address2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2),
                    ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
                    PhoneNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                    FaxNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax),
                    CreatedOnUtc = customer.CreatedOnUtc
                };
                if (this._addressService.IsAddressValid(defaultAddress))
                {
                    // Some validation
                    if (defaultAddress.CountryId == 0)
                        defaultAddress.CountryId = null;
                    if (defaultAddress.StateProvinceId == 0)
                        defaultAddress.StateProvinceId = null;
                    
                    // Set default address
                    customer.Addresses.Add(defaultAddress);
                    customer.BillingAddress = defaultAddress;
                    customer.ShippingAddress = defaultAddress;
                    _customerService.UpdateCustomer(customer);
                }

                // Notifications
                if (_customerSettings.NotifyNewCustomerRegistration)
                    _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer, _localizationSettings.DefaultAdminLanguageId);

                // Raise event       
                _eventPublisher.Publish(new CustomerRegisteredEvent(customer));

                // Login customer now
                _authenticationService.SignIn(customer, true);

                switch (_customerSettings.UserRegistrationType)
                {
                    case UserRegistrationType.EmailValidation:
                        {
                            // Email validation message

                            // TODO: Notify customer about temporary password

                            _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.AccountActivationToken, Guid.NewGuid().ToString());
                            _workflowMessageService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);

                            break;
                        }
                    case UserRegistrationType.Standard:
                        {
                            // Send customer welcome message

                            // TODO: Notify customer about temporary password

                            _workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

                            break;
                        }
                }
            }

            return registrationResult;
        }

        /// <summary>
        /// Prepares the agreement documents and send them to the customer to be signed
        /// </summary>
        /// <param name="signup">Consumer finance signup</param>
        /// <returns>DocuSign Envelope Status</returns>
        protected virtual DocuSignWeb.EnvelopeStatus PrepareAndSendAgreementDocuments(ConsumerFinanceSignup signup)
        {
            var preContractCreditInfoDocuments = new List<byte[]>();
            var hirePurchaseAgreementDocuments = new List<byte[]>();

            foreach (var orderItem in signup.OrderItems)
            {
                for (int i = 0; i < orderItem.ProductQuantity; i++)
                {
                    preContractCreditInfoDocuments.Add(PreparePreContractCreditInfoDocument(signup, orderItem));
                    hirePurchaseAgreementDocuments.Add(PrepareHirePurchaseAgreementDocument(signup, orderItem));
                }
            }

            var personalInformation = GetSignupPersonalInformation(signup);

            // Create the recipient(s)
            var recipients = new List<DocuSignWeb.Recipient>();

            // TODO: Need a solution to handle this when an error occured
            var signer = _docuSignService.CreateDocuSignRecipient(
                personalInformation.Email, 
                String.Join(" ", personalInformation.FirstName, personalInformation.LastName), 
                signup.CustomerId.ToString(), 
                DocuSignWeb.RecipientTypeCode.Signer);

            recipients.Add(signer);

            // Create the document(s)
            var documents = new List<DocuSignWeb.Document>();

            var tabs = new List<DocuSignWeb.Tab>();

            int hpaId = 1;
            int pcciId = 2;

            for (int i = 0; i < preContractCreditInfoDocuments.Count; i++)
            {
                // Pre-Contract Credit Information
                var pcci = _docuSignService.CreateDocuSignDocument(
                    "Pre-Contract Credit Information", // TODO localization
                    pcciId.ToString(), 
                    preContractCreditInfoDocuments[i]);

                documents.Add(pcci);

                // pcci tab
                var pcciTab = _docuSignService.CreateDocuSignTab(
                    pcciId.ToString(), 
                    signer.ID,
                    DocuSignWeb.TabTypeCode.Custom, 
                    3, 528, 40, true,
                    DocuSignWeb.CustomTabType.Radio);
                tabs.Add(pcciTab);

                // Hire Purchase Agreement
                var hpa = _docuSignService.CreateDocuSignDocument(
                    signup.Id.ToString().PadLeft(8, '0'), 
                    hpaId.ToString(), 
                    hirePurchaseAgreementDocuments[i]);

                documents.Add(hpa);

                var listItems = "Yes;No";

                // Yes or No - Services Information
                var listTab1 = _docuSignService.CreateDocuSignTab(
                    hpaId.ToString(), 
                    signer.ID,
                    DocuSignWeb.TabTypeCode.Custom, 
                    3, 240, 148, true,
                    DocuSignWeb.CustomTabType.List);
                listTab1.CustomTabListItems = listItems;
                listTab1.TabLabel = "Services information";
                tabs.Add(listTab1);

                // Yes or No - Pass your details
                var listTab2 = _docuSignService.CreateDocuSignTab(
                    hpaId.ToString(), 
                    signer.ID,
                    DocuSignWeb.TabTypeCode.Custom, 
                    3, 240, 200, true,
                    DocuSignWeb.CustomTabType.List);
                listTab2.CustomTabListItems = listItems;
                listTab2.TabLabel = "Pass your details";
                tabs.Add(listTab2);

                // Yes or No - Contact by telephone
                var listTab3 = _docuSignService.CreateDocuSignTab(
                    hpaId.ToString(), 
                    signer.ID,
                    DocuSignWeb.TabTypeCode.Custom, 
                    3, 440, 220, true,
                    DocuSignWeb.CustomTabType.List);
                listTab3.CustomTabListItems = listItems;
                listTab3.TabLabel = "Contact by telephone";
                tabs.Add(listTab3);

                // Yes or No - Contact by email
                var listTab4 = _docuSignService.CreateDocuSignTab(
                    hpaId.ToString(), 
                    signer.ID,
                    DocuSignWeb.TabTypeCode.Custom, 
                    3, 440, 240, true,
                    DocuSignWeb.CustomTabType.List);
                listTab4.CustomTabListItems = listItems;
                listTab4.TabLabel = "Contact by email";
                tabs.Add(listTab4);

                var instructionsToBankSignatureTab = _docuSignService.CreateDocuSignTab(
                    hpaId.ToString(), 
                    signer.ID,
                    DocuSignWeb.TabTypeCode.SignHere, 
                    4, 371, 290, true);
                tabs.Add(instructionsToBankSignatureTab);

                var instructionsDateSignedTab = _docuSignService.CreateDocuSignTab(
                    hpaId.ToString(), 
                    signer.ID,
                    DocuSignWeb.TabTypeCode.DateSigned, 
                    4, 371, 380, true);
                tabs.Add(instructionsDateSignedTab);

                // Create a new signature tab
                var signTab = _docuSignService.CreateDocuSignTab(
                    hpaId.ToString(), 
                    signer.ID,
                    DocuSignWeb.TabTypeCode.SignHere, 
                    5, 200, 75, true);
                tabs.Add(signTab);

                var dateSignedTab = _docuSignService.CreateDocuSignTab(
                    hpaId.ToString(), 
                    signer.ID,
                    DocuSignWeb.TabTypeCode.DateSigned, 
                    5, 190, 168, true);
                tabs.Add(dateSignedTab);

                var dateSigned2Tab = _docuSignService.CreateDocuSignTab(
                    hpaId.ToString(), 
                    signer.ID,
                    DocuSignWeb.TabTypeCode.DateSigned, 
                    5, 190, 235, true);
                tabs.Add(dateSigned2Tab);

                hpaId += 2;
                pcciId += 2;
            }

            var emailBlurb = "";
            var envelopeStatus = _docuSignService.SendEnvelopeEmbedded(
                $"{_storeContext.CurrentStore.Name} - Finance Agreement", // TODO localization
                emailBlurb, 
                recipients, 
                documents, 
                tabs, 
                signup.CustomerId.ToString());

            return envelopeStatus;
        }

        // TODO Verify if this method is safe to use concurrently
        protected virtual byte[] PreparePreContractCreditInfoDocument(ConsumerFinanceSignup signup, ConsumerFinanceOrderItem orderItem)
        {
            byte[] bytes;

            using (var stream = new MemoryStream())
            {
                string templatePath = CommonHelper.MapPath(_consumerFinanceSettings.FinanceDocumentTemplatesPath + "PreContractCreditInfoTemplate.pdf");

                // Open the reader
                using (var reader = new PdfReader(templatePath))
                {
                    var pageSize = reader.GetPageSizeWithRotation(1);

                    using (var doc = new Document(pageSize))
                    {
                        using (var writer = PdfWriter.GetInstance(doc, stream))
                        {
                            doc.Open();

                            var baseFont = BaseFont.CreateFont(
                                CommonHelper.MapPath(_consumerFinanceSettings.FinanceDocumentTemplatesPath + "Fonts/Calibri.ttf"),
                                BaseFont.IDENTITY_H,
                                BaseFont.EMBEDDED);

                            var importedPage = writer.GetImportedPage(reader, 1);
                            var importedPage2 = writer.GetImportedPage(reader, 2);
                            var importedPage3 = writer.GetImportedPage(reader, 3);

                            var personalInformation = GetSignupPersonalInformation(signup);
                            var homeAddress = GetSignupHomeAddress(signup);

                            string customerTitle = personalInformation.Title ?? "";
                            string customerName = string.Join(" ", personalInformation.FirstName, personalInformation.LastName) ?? "";

                            var currentHomeAddress = homeAddress.CurrentAddress;
                            var addessLines = new List<string>
                            {
                                currentHomeAddress.Address1,
                                currentHomeAddress.Address2
                            };

                            string buildingNumber = (string.IsNullOrWhiteSpace(currentHomeAddress.BuildingNumber) ? "" : currentHomeAddress.BuildingNumber + " ");
                            string customerAddress = buildingNumber + string.Join(", ", addessLines.Where(al => !string.IsNullOrWhiteSpace(al)));
                            string customerTelephoneNumber = personalInformation.MobilePhoneNumber;

                            var orderItemCurrency = _currencyService.GetCurrencyByCode(orderItem.CurrencyCode);
                            var language = _workContext.WorkingLanguage;

                            string numberOfPayments = orderItem.DurationOfAgreement.ToString();
                            string cashPrice = _priceFormatter.FormatPrice(orderItem.ProductPriceInclTax, true, orderItemCurrency, language, true);
                            string totalAmountOfCredit = _priceFormatter.FormatPrice(orderItem.TotalCreditOrFinanceCharges, true, orderItemCurrency, language, true);
                            string totalAmountPayable = _priceFormatter.FormatPrice(orderItem.TotalPayable, true, orderItemCurrency, language, true);
                            string monthlyPayment = _priceFormatter.FormatPrice(orderItem.MonthlyPayment, true, orderItemCurrency, language, true);
                            string initialPayment = _priceFormatter.FormatPrice(orderItem.InitialPayment, true, orderItemCurrency, language, true);
                            string optionToPurchaseFee = _priceFormatter.FormatPrice(_currencyService.ConvertCurrency(_consumerFinanceSettings.OptionToPurchaseFeeGbp, _currencyService.GetCurrencyByCode("GBP"), orderItemCurrency), true, orderItemCurrency, language, true);

                            var selectedProductAttributeValues = new List<string>();
                            var productAttributeMappings = _productAttributeParser.ParseProductAttributeMappings(orderItem.ProductAttributesXml).ToList();
                            productAttributeMappings.ForEach(pa => selectedProductAttributeValues.AddRange(_productAttributeParser.ParseValues(orderItem.ProductAttributesXml, pa.Id)));

                            string descriptionOfGoods = $"{_productService.GetProductById(orderItem.ProductId).Name} {string.Join(" ", selectedProductAttributeValues)}. We would not be able to supply the serial number at this point.";

                            string supplier = _consumerFinanceSettings.SupplierCompanyAddress;
                            string rateOfInterestPerAnnum = $"{_consumerFinanceSettings.PercentageRateOfInterestPerAnnum}%";
                            string annualPercentageRate = $"{orderItem.Apr}%";
                            string limitToTermination = _priceFormatter.FormatPrice(RoundingHelper.RoundPrice(orderItem.TotalPayable / 2), true, orderItemCurrency, language, true);
                            string repossession = _priceFormatter.FormatPrice(RoundingHelper.RoundPrice(orderItem.TotalPayable / 3), true, orderItemCurrency, language, true);

                            PdfContentByte cb = writer.DirectContent;

                            #region Page 1

                            cb.BeginText();
                            cb.SetFontAndSize(baseFont, 10);

                            // credit
                            cb.SetTextMatrix(209, 596);
                            cb.ShowText(totalAmountOfCredit);

                            // number of payment
                            cb.SetTextMatrix(209, 486.5f);
                            cb.ShowText(numberOfPayments);

                            // total amount payable
                            cb.SetTextMatrix(209, 455);
                            cb.ShowText(totalAmountPayable);

                            // repayment installments
                            cb.SetTextMatrix(209, 413.5f);
                            cb.ShowText(numberOfPayments + " repayments each of " + monthlyPayment);

                            // description of goods
                            cb.SetTextMatrix(209, 351.5f);
                            cb.ShowText(descriptionOfGoods);

                            // cash price
                            cb.SetTextMatrix(209, 333);
                            cb.ShowText(cashPrice);

                            // deposit
                            cb.SetTextMatrix(209, 315);
                            cb.ShowText(initialPayment);

                            // option to purchase fee
                            cb.SetTextMatrix(209, 297.5f);
                            cb.ShowText(optionToPurchaseFee);

                            // supplier
                            float supplierY = 268;

                            foreach (var sentence in supplier.LimitString(85))
                            {
                                cb.SetTextMatrix(209, supplierY);
                                cb.ShowText(sentence);
                                supplierY -= 10;
                            }

                            // rate of interest per annum
                            cb.SetTextMatrix(295, 221.5f);
                            cb.ShowText(rateOfInterestPerAnnum);

                            // annual percentage rate
                            cb.SetTextMatrix(209, 190);
                            cb.ShowText(annualPercentageRate);

                            cb.EndText();
                            cb.AddTemplate(importedPage, 0, 0);

                            #endregion

                            #region Page 2

                            doc.NewPage();
                            cb.BeginText();
                            cb.SetFontAndSize(baseFont, 10);

                            // limit to termination
                            cb.SetTextMatrix(69, 293.5f);
                            cb.ShowText(limitToTermination);

                            // annual percentage rate
                            cb.SetTextMatrix(63, 238.5f);
                            cb.ShowText(repossession);

                            cb.EndText();
                            cb.AddTemplate(importedPage2, 0, 0);

                            #endregion

                            #region Page 3

                            doc.NewPage();
                            cb.AddTemplate(importedPage3, 0, 0);

                            #endregion

                            doc.Close();

                            bytes = stream.ToArray();

                            return bytes;
                        }
                    }
                }
            }
        }

        // TODO Verify if this method is safe to use concurrently
        protected virtual byte[] PrepareHirePurchaseAgreementDocument(ConsumerFinanceSignup signup, ConsumerFinanceOrderItem orderItem)
        {
            var callValidateResult = _callValidateService.GetCallValidateResultResults(signup.CallValidateResultId.Value);

            byte[] bytes;

            using (var stream = new MemoryStream())
            {
                string template = CommonHelper.MapPath(_consumerFinanceSettings.FinanceDocumentTemplatesPath + "HirePurchaseAgreementTemplate.pdf");

                // Open the reader
                using (var reader = new PdfReader(template))
                {
                    var size = reader.GetPageSizeWithRotation(1);
                    using (var doc = new Document(size))
                    {
                        // Open the writer
                        using (var writer = PdfWriter.GetInstance(doc, stream))
                        {
                            doc.Open();

                            var baseFont = BaseFont.CreateFont(
                                CommonHelper.MapPath(_consumerFinanceSettings.FinanceDocumentTemplatesPath + "Fonts/Calibri.ttf"),
                                BaseFont.IDENTITY_H,
                                BaseFont.EMBEDDED);

                            var importedPage = writer.GetImportedPage(reader, 1);
                            var importedPage2 = writer.GetImportedPage(reader, 2);
                            var importedPage3 = writer.GetImportedPage(reader, 3);
                            var importedPage4 = writer.GetImportedPage(reader, 4);
                            var importedPage5 = writer.GetImportedPage(reader, 5);
                            var importedPage6 = writer.GetImportedPage(reader, 6);

                            var personalInformation = GetSignupPersonalInformation(signup);
                            var homeAddress = GetSignupHomeAddress(signup);
                            var bank = GetSignupBank(signup);

                            string customerTitle = personalInformation.Title ?? "";
                            string customerName = string.Join(" ", personalInformation.FirstName, personalInformation.LastName) ?? "";

                            var currentHomeAddress = homeAddress.CurrentAddress;
                            var addessLines = new List<string>
                            {
                                currentHomeAddress.Address1,
                                currentHomeAddress.Address2
                            };

                            string buildingNumber = (string.IsNullOrWhiteSpace(currentHomeAddress.BuildingNumber) ? "" : currentHomeAddress.BuildingNumber + " ");
                            string customerAddress = buildingNumber + string.Join(", ", addessLines.Where(al => !string.IsNullOrWhiteSpace(al)));
                            string customerTelephoneNumber = personalInformation.MobilePhoneNumber;

                            var orderItemCurrency = _currencyService.GetCurrencyByCode(orderItem.CurrencyCode);
                            var language = _workContext.WorkingLanguage;

                            string numberOfPayments = orderItem.DurationOfAgreement.ToString();
                            string cashPrice = _priceFormatter.FormatPrice(orderItem.ProductPriceInclTax, true, orderItemCurrency, language, true);
                            string totalAmountOfCredit = _priceFormatter.FormatPrice(orderItem.TotalCreditOrFinanceCharges, true, orderItemCurrency, language, true);
                            string totalAmountPayable = _priceFormatter.FormatPrice(orderItem.TotalPayable, true, orderItemCurrency, language, true);
                            string monthlyPayment = _priceFormatter.FormatPrice(orderItem.MonthlyPayment, true, orderItemCurrency, language, true);
                            string initialPayment = _priceFormatter.FormatPrice(orderItem.InitialPayment, true, orderItemCurrency, language, true);
                            string optionToPurchaseFee = _priceFormatter.FormatPrice(_currencyService.ConvertCurrency(_consumerFinanceSettings.OptionToPurchaseFeeGbp, _currencyService.GetCurrencyByCode("GBP"), orderItemCurrency), true, orderItemCurrency, language, true);

                            var selectedProductAttributeValues = new List<string>();
                            var productAttributeMappings = _productAttributeParser.ParseProductAttributeMappings(orderItem.ProductAttributesXml).ToList();
                            productAttributeMappings.ForEach(pa => selectedProductAttributeValues.AddRange(_productAttributeParser.ParseValues(orderItem.ProductAttributesXml, pa.Id)));

                            string descriptionOfGoods = $"{_productService.GetProductById(orderItem.ProductId).Name} {string.Join(" ", selectedProductAttributeValues)}. We would not be able to supply the serial number at this point.";

                            string supplier = _consumerFinanceSettings.SupplierCompanyAddress;
                            string rateOfInterestPerAnnum = $"{_consumerFinanceSettings.PercentageRateOfInterestPerAnnum}%";
                            string annualPercentageRate = $"{orderItem.Apr}%";
                            string limitToTermination = _priceFormatter.FormatPrice(RoundingHelper.RoundPrice(orderItem.TotalPayable / 2), true, orderItemCurrency, language, true);
                            string repossession = _priceFormatter.FormatPrice(RoundingHelper.RoundPrice(orderItem.TotalPayable / 3), true, orderItemCurrency, language, true);

                            string toTheManager = "";
                            string bankName = "";
                            string bankAddress = "";
                            string bankPostcode = "";
                            string nameOfAccountHolder = bank.AccountName;
                            string bankAccountNumber = bank.AccountNumber;
                            string bankSortCode = "";

                            // do not throw exception for each
                            // TODO: Log Exceptions
                            try { toTheManager = callValidateResult.Result.Displays.BankcheckStandard.Major_Location_Name; } catch { }
                            try { bankAddress = string.Format("{0} {1} {2} {3}", callValidateResult.Result.Displays.BankcheckStandard.Address_line_1, callValidateResult.Result.Displays.BankcheckStandard.Address_line_2, callValidateResult.Result.Displays.BankcheckStandard.Address_line_3, callValidateResult.Result.Displays.BankcheckStandard.Address_line_4); }
                            catch { }
                            try { bankPostcode = callValidateResult.Result.Displays.BankcheckStandard.Post_Code_major_part + " " + callValidateResult.Result.Displays.BankcheckStandard.Post_Code_minor_part; }
                            catch { }
                            try { bankSortCode = callValidateResult.Result.Displays.BankcheckStandard.sortcode; }
                            catch { }

                            toTheManager = toTheManager ?? "";
                            bankAddress = bankAddress ?? "";
                            bankPostcode = bankPostcode ?? "";
                            bankSortCode = bankSortCode ?? "";

                            PdfContentByte cb = writer.DirectContent;

                            #region Page 1

                            int x = 30;

                            cb.BeginText();
                            cb.SetFontAndSize(baseFont, 10);

                            // customer name
                            cb.SetTextMatrix(144, 616 - x);
                            cb.ShowText(customerName);

                            // customer address
                            cb.SetTextMatrix(144, 594 - x);
                            cb.ShowText(customerAddress);

                            // customer telephone number
                            cb.SetTextMatrix(144, 572 - x);
                            cb.ShowText(customerTelephoneNumber);

                            // total amount of credit
                            cb.SetTextMatrix(209, 379 - x);
                            cb.ShowText(totalAmountOfCredit);

                            // term of agreement
                            cb.SetTextMatrix(209, 307 - x);
                            cb.ShowText(numberOfPayments + " months");

                            // total amount payable
                            cb.SetTextMatrix(209, 263 - x);
                            cb.ShowText(totalAmountPayable);

                            // number of payments
                            cb.SetTextMatrix(209, 221f - x);
                            cb.ShowText(numberOfPayments);

                            // monthly payment
                            cb.SetTextMatrix(305, 221f - x);
                            cb.ShowText(monthlyPayment);

                            // description of goods
                            cb.SetTextMatrix(209, 159.5f - x);
                            cb.ShowText(descriptionOfGoods);

                            // cash price
                            cb.SetTextMatrix(209, 141.5f - x);
                            cb.ShowText(cashPrice);

                            // deposit
                            cb.SetTextMatrix(209, 123f - x);
                            cb.ShowText(initialPayment);

                            // option to purchase fee...
                            cb.SetTextMatrix(209, 105f - x);
                            cb.ShowText(optionToPurchaseFee);

                            // supplier
                            cb.SetTextMatrix(209, 70.5f - x);

                            float supplierY = 84.5f - x;

                            string newSupplier = supplier + " (the 'Supplier')";

                            foreach (var sentence in newSupplier.LimitString(85))
                            {
                                supplierY -= 10;
                                cb.SetTextMatrix(209, supplierY);
                                cb.ShowText(sentence);
                            }

                            cb.EndText();
                            cb.AddTemplate(importedPage, 0, 0);

                            #endregion

                            #region Page 2

                            doc.NewPage();
                            cb.BeginText();
                            cb.SetFontAndSize(baseFont, 10);

                            // rate of interest per annum
                            cb.SetTextMatrix(289, 783.5f);
                            cb.ShowText(rateOfInterestPerAnnum);

                            // annual percentage rate
                            cb.SetTextMatrix(209, 752);
                            cb.ShowText(annualPercentageRate);

                            // termination part
                            cb.SetTextMatrix(70, 125);
                            cb.ShowText(limitToTermination);

                            // repossession part
                            cb.SetTextMatrix(118, 70);
                            cb.ShowText(repossession);

                            cb.EndText();
                            cb.AddTemplate(importedPage2, 0, 0);

                            #endregion

                            #region Page 3

                            doc.NewPage();
                            cb.AddTemplate(importedPage3, 0, 0);

                            #endregion

                            #region Page 4

                            doc.NewPage();
                            cb.BeginText();
                            cb.SetFontAndSize(baseFont, 10);

                            // to the manager
                            cb.SetTextMatrix(130, 656);
                            cb.ShowText(toTheManager);

                            // bank name
                            cb.SetTextMatrix(50, 641);
                            cb.ShowText(bankName);

                            // bank address
                            cb.SetTextMatrix(100, 626);
                            cb.ShowText(bankAddress.LimitString(49)[0]);

                            if (bankAddress.LimitString(49).Count > 1)
                            {
                                string ba = bankAddress.Replace(bankAddress.LimitString(49)[0], "");
                                var ba2 = ba.LimitString(60);

                                for (int i = 0; i < ba2.Count; i++)
                                {
                                    cb.SetTextMatrix(50, 611 - (i * 15));
                                    cb.ShowText(ba2[i]);
                                }
                            }

                            // bank postcode
                            cb.SetTextMatrix(200, 595);
                            cb.ShowText(bankPostcode);

                            // bank account holder
                            cb.SetTextMatrix(50, 558);
                            cb.ShowText(nameOfAccountHolder);

                            // bank account number
                            var an = bankAccountNumber.ToCharArray();

                            for (int i = 0; i < an.Length; i++)
                            {
                                cb.SetTextMatrix(54 + (i * 31), 507);
                                cb.ShowText(an[i].ToString());
                            }

                            // bank sort code
                            var sc = bankSortCode.ToCharArray();

                            for (int i = 0; i < sc.Length; i++)
                            {
                                cb.SetTextMatrix(54 + (i * 31), 469);
                                cb.ShowText(sc[i].ToString());
                            }

                            var referenceNumber = signup.Id.ToString().PadLeft(8, '0');
                            var referenceNumberCharArray = referenceNumber.ToCharArray();

                            for (int i = 0; i < referenceNumberCharArray.Length; i++)
                            {
                                cb.SetTextMatrix(325 + (i * 31), 625);
                                cb.ShowText(referenceNumberCharArray[i].ToString());
                            }

                            cb.EndText();
                            cb.AddTemplate(importedPage4, 0, 0);

                            #endregion

                            #region Page 5

                            doc.NewPage();
                            cb.BeginText();
                            cb.SetFontAndSize(baseFont, 10);

                            // customer name and title
                            cb.SetTextMatrix(190, 687);
                            cb.ShowText(customerTitle + " " + customerName);

                            // supplier address
                            cb.SetFontAndSize(baseFont, 9);
                            float supplierAddressY = 199.4f - x;

                            string supplierPlus = supplier + ".Following the Expiry Date we shall no longer be required to finance credit for the Goods.";

                            var firstSentence = supplierPlus.LimitString(55)[0];

                            supplierAddressY -= 10;
                            cb.SetTextMatrix(363, supplierAddressY);
                            cb.ShowText(firstSentence);

                            supplierPlus = supplierPlus.Replace(firstSentence, "");

                            foreach (var sentence in supplierPlus.LimitString(62))
                            {
                                supplierAddressY -= 10;

                                cb.SetTextMatrix(340, supplierAddressY);
                                cb.ShowText(sentence);
                            }

                            cb.EndText();
                            cb.AddTemplate(importedPage5, 0, 0);

                            #endregion

                            #region Page 6

                            doc.NewPage();
                            cb.AddTemplate(importedPage6, 0, 0);

                            #endregion

                            doc.Close();

                            bytes = stream.ToArray();

                            return bytes;
                        }
                    }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Maps the customer's shopping cart items to their selected finance option
        /// </summary>
        /// <param name="consumerFinanceOptionId">Consumer finance option identifier</param>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        public virtual void MapCustomerShoppingCartItemsToFinanceOption(int consumerFinanceOptionId, int customerId = 0, int storeId = 0)
        {
            var consumerFinanceOption = _consumerFinanceOptionRepository.GetById(consumerFinanceOptionId);
            if (consumerFinanceOption == null)
                throw new NopException("Invalid consumer finance option.");

            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            storeId = storeId > 0 ? storeId : _storeContext.CurrentStore.Id;

            var shoppingCartItemIds = customer.ShoppingCartItems
                .Where(s => s.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart && s.StoreId == storeId)
                .Select(s => s.Id)
                .ToList();

            // Delete existing shopping cart item mappings
            var shoppingCartItemFinanceOptions = _shoppingCartItemConsumerFinanceOptionRepository.Table
                .Where(s => shoppingCartItemIds.Contains(s.ShoppingCartItemId));

            _shoppingCartItemConsumerFinanceOptionRepository.Delete(shoppingCartItemFinanceOptions);

            // Create new shopping cart item mappings based on the provided consumer finance option identifier
            _shoppingCartItemConsumerFinanceOptionRepository.Insert(
                shoppingCartItemIds.Select(s => new ShoppingCartItemConsumerFinanceOption
                {
                    ConsumerFinanceOptionId = consumerFinanceOptionId,
                    ShoppingCartItemId = s
                }));
        }

        /// <summary>
        /// Clear the customer's shopping cart item finance option mappings
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        public virtual void ClearCustomerShoppingCartItemFinanceOptions(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            storeId = storeId > 0 ? storeId : _storeContext.CurrentStore.Id;
            
            var shoppingCartItemIds = customer.ShoppingCartItems
                .Where(s => s.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart && s.StoreId == storeId)
                .Select(s => s.Id)
                .ToList();

            // Delete existing shopping cart item mappings
            var shoppingCartItemFinanceOptions = _shoppingCartItemConsumerFinanceOptionRepository.Table
                .Where(s => shoppingCartItemIds.Contains(s.ShoppingCartItemId));

            _shoppingCartItemConsumerFinanceOptionRepository.Delete(shoppingCartItemFinanceOptions);
        }

        /// <summary>
        /// Gets the customer's shopping cart finance option mappings
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identiifer</param>
        public virtual IEnumerable<ShoppingCartItemConsumerFinanceOption> GetCustomerShoppingCartItemConsumerFinanceOptions(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            storeId = storeId > 0 ? storeId : _storeContext.CurrentStore.Id;
            
            var shoppingCartItemIds = customer.ShoppingCartItems
                .Where(s => s.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart && s.StoreId == storeId)
                .Select(s => s.Id)
                .ToList();

            return _shoppingCartItemConsumerFinanceOptionRepository.Table
                .Where(s => shoppingCartItemIds.Contains(s.ShoppingCartItemId));
        }

        /// <summary>
        /// Creates a new shopping cart item finance option mapping
        /// </summary>
        /// <param name="shoppingCartItemId">Shopping cart item identifier</param>
        /// <param name="consumerFinanceOptionId">Consumer finance option identifier</param>
        public virtual void CreateShoppingCartItemFinanceOption(int shoppingCartItemId, int consumerFinanceOptionId)
        {
            // Check if shopping cart item exists
            var shoppingCartItem = _shoppingCartItemRepository.GetById(shoppingCartItemId);
            if (shoppingCartItem == null)
                throw new NopException("Shopping cart item not found.");

            // Check if shopping cart item finance option mapping already exists
            if (_shoppingCartItemConsumerFinanceOptionRepository.Table.Any(s => s.ShoppingCartItemId == shoppingCartItemId))
                return;

            // Check if consumer finance option exists
            var consumerFinanceOption = _consumerFinanceOptionRepository.GetById(consumerFinanceOptionId);
            if (consumerFinanceOption == null)
                throw new NopException("Consumer finance option not found.");

            // Insert new record
            _shoppingCartItemConsumerFinanceOptionRepository.Insert(new ShoppingCartItemConsumerFinanceOption
            {
                ConsumerFinanceOptionId = consumerFinanceOptionId,
                ShoppingCartItemId = shoppingCartItemId
            });
        }

        /// <summary>
        /// Deletes a shopping cart item finance option mapping
        /// </summary>
        /// <param name="shoppingCartItemId">Shopping cart item identifier</param>
        public void DeleteShoppingCartItemFinanceOption(int shoppingCartItemId)
        {
            var items = _shoppingCartItemConsumerFinanceOptionRepository.Table.Where(s => s.ShoppingCartItemId == shoppingCartItemId);
            _shoppingCartItemConsumerFinanceOptionRepository.Delete(items);
        }

        /// <summary>
        /// Determines if the customer can signup for finance
        /// </summary>
        /// <param name="notAllowedMessage">Message if not allowed for finance signup</param>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        public virtual bool CustomerCanSignup(out string notAllowedMessage, int customerId = 0)
        {
            var signup = GetLatestConsumerSignupOfCustomer(customerId);

            if (signup != null && signup.SignupCompletedOnUtc.HasValue)
            {
                // Check if the customer can signup again.
                if (!signup.AllChecksPassed)
                {
                    int daysPassed = (DateTime.UtcNow - signup.SignupCompletedOnUtc.Value).Days;
                    if (daysPassed < 90)
                    {
                        notAllowedMessage = $"Your previous finance application failed. You can apply for finance after {(90 - daysPassed)} day/s.";
                        return false;
                    }

                    // else, allowed
                }
                else
                {
                    // Check if signup has order and verify if the order is paid.
                    if (!signup.OrderId.HasValue)
                    {
                        notAllowedMessage = "Please complete your previous order first.";
                        return false;
                    }

                    var order = _orderService.GetOrderById(signup.OrderId.Value);
                    if (!order.PaidDateUtc.HasValue)
                    {
                        notAllowedMessage = "You still haven't paid your previous finance order.";
                        return false;
                    }

                    int daysPassed = (DateTime.UtcNow - order.PaidDateUtc.Value).Days;
                    if (daysPassed < 7)
                    {
                        notAllowedMessage = $"You are not allowed to apply for finance for a while. ({(7 - daysPassed).ToString()} day/s left)";
                        return false;
                    }
                }
            }

            notAllowedMessage = null;
            return true;
        }

        /// <summary>
        /// Returns the latest consumer finance signup record of a customer.
        /// </summary>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        /// <param name="storeId">Store identifier. Will use the current store if not provided</param>
        public virtual ConsumerFinanceSignup GetLatestConsumerSignupOfCustomer(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            if (customer == null)
                throw new NopException("Customer not found.");

            var store = storeId > 0 ? _storeService.GetStoreById(storeId) : _storeContext.CurrentStore;
            if (store == null)
                throw new NopException("Store not found.");

            return _consumerFinanceSignupRepository.Table
                .Where(s => s.CustomerId == customer.Id && s.StoreId == store.Id)
                .OrderByDescending(s => s.Id)
                .FirstOrDefault();
        }

        /// <summary>
        /// Creates a new consumer finance signup record for a customer
        /// </summary>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        /// <param name="storeId">Store identifier. Will use the current store if not provided</param>
        public virtual ConsumerFinanceSignup CreateNewConsumerFinanceSignup(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            if (customer == null)
                throw new NopException("Customer not found.");

            var store = storeId > 0 ? _storeService.GetStoreById(storeId) : _storeContext.CurrentStore;
            if (store == null)
                throw new NopException("Store not found.");

            var signup = new ConsumerFinanceSignup
            {
                CreatedOnUtc = DateTime.UtcNow,
                CustomerId = customer.Id,
                StoreId = store.Id
            };

            _consumerFinanceSignupRepository.Insert(signup);

            return signup;
        }

        /// <summary>
        /// Returns all marital statuses
        /// </summary>
        public virtual IEnumerable<MaritalStatus> GetAllMaritalStatuses()
        {
            return _maritalStatusRepository.Table;
        }

        /// <summary>
        /// Returns all number of dependents
        /// </summary>
        public virtual IEnumerable<NumberOfDependents> GetAllNumberOfDependents()
        {
            return _numberOfDependentsRepository.Table;
        }

        /// <summary>
        /// Returns all home owner statuses
        /// </summary>
        public virtual IEnumerable<HomeOwnerStatus> GetAllHomeOwnerStatuses()
        {
            return _homeOwnerStatusRepository.Table;
        }

        /// <summary>
        /// Returns all employment statuses
        /// </summary>
        public virtual IEnumerable<EmploymentStatus> GetAllEmploymentStatuses()
        {
            return _employmentStatusRepository.Table;
        }

        /// <summary>
        /// Returns all annual gross income
        /// </summary>
        public virtual IEnumerable<AnnualGrossIncome> GetAllAnnualGrossIncome()
        {
            return _annualGrossIncomeRepository.Table;
        }

        /// <summary>
        /// Returns all expenditure categories
        /// </summary>
        public virtual IEnumerable<ExpenditureCategory> GetAllConsumerExpenditureCategories()
        {
            return _expenditureCategoryRepository.Table;
        }

        /// <summary>
        /// Returns expenditure category ranges
        /// </summary>
        /// <param name="expenditureCategoryId">Expenditure category filter</param>
        public virtual IEnumerable<ExpenditureCategoryRange> GetExpenditureCategoryRanges(int expenditureCategoryId = 0)
        {
            var selectionItems = _expenditureCategoryRangeRepository.Table;

            if (expenditureCategoryId > 0)
                selectionItems = selectionItems.Where(x => x.ExpenditureCategoryId == expenditureCategoryId);

            return selectionItems;
        }

        /// <summary>
        /// Returns consumer finance options
        /// </summary>
        /// <param name="includeInactive">Include inactive consumer finance options?</param>
        public virtual IEnumerable<ConsumerFinanceOption> GetConsumerFinanceOptions(bool includeInactive = false)
        {
            var options = _consumerFinanceOptionRepository.Table;

            if (!includeInactive)
                options = options.Where(o => o.IsActive);

            return options.OrderBy(o => o.DurationInMonths);
        }

        /// <summary>
        /// Gets a consumer finance option
        /// </summary>
        /// <param name="id">Consumer finance option identifier</param>
        public virtual ConsumerFinanceOption GetConsumerFinanceOptionById(int id)
        {
            if (id <= 0)
                return null;

            return _consumerFinanceOptionRepository.GetById(id);
        }

        /// <summary>
        /// Inserts a new consumer finance option
        /// </summary>
        /// <param name="consumerFinanceOption">Consumer finance option</param>
        public virtual void InsertConsumerFinanceOption(ConsumerFinanceOption consumerFinanceOption)
        {
            _consumerFinanceOptionRepository.Insert(consumerFinanceOption);
        }

        /// <summary>
        /// Updates a consumer finance option
        /// </summary>
        /// <param name="consumerFinanceOption">Consumer finance option</param>
        public virtual void UpdateConsumerFinanceOption(ConsumerFinanceOption consumerFinanceOption)
        {
            _consumerFinanceOptionRepository.Update(consumerFinanceOption);
        }

        /// <summary>
        /// Returns an expenditure range
        /// </summary>
        /// <param name="expenditureRangeId">Expenditure range identifier</param>
        public virtual ExpenditureRange GetConsumerExpenditureRangeById(int expenditureRangeId)
        {
            return _expenditureRangeRepository.GetById(expenditureRangeId);
        }

        /// <summary>
        /// Gets the consumer finance personal information of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignup">Consumer finance signup</param>
        public virtual ConsumerFinancePersonalInformation GetSignupPersonalInformation(ConsumerFinanceSignup consumerFinanceSignup)
        {
            if (consumerFinanceSignup == null)
                throw new ArgumentNullException(nameof(consumerFinanceSignup));

            if (string.IsNullOrWhiteSpace(consumerFinanceSignup.ConsumerFinancePersonalInformation))
                return null;

            var personalInformationText = _encryptionService.DecryptText(consumerFinanceSignup.ConsumerFinancePersonalInformation);
            return JsonConvert.DeserializeObject<ConsumerFinancePersonalInformation>(personalInformationText);
        }

        /// <summary>
        /// Sets the consumer finance personal information of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignupId">Consumer finance signup identifier</param>
        /// <param name="personalInformation">Personal information</param>
        public virtual void SetSignupPersonalInformation(int consumerFinanceSignupId, ConsumerFinancePersonalInformation personalInformation)
        {
            if (personalInformation == null)
                throw new ArgumentNullException(nameof(personalInformation));

            ConsumerFinanceSignup signup;

            if (consumerFinanceSignupId <= 0 || (signup = _consumerFinanceSignupRepository.GetById(consumerFinanceSignupId)) == null)
                throw new NopException("Consumer finance signup record not found.");

            string personalInformationText = JsonConvert.SerializeObject(personalInformation);
            signup.ConsumerFinancePersonalInformation = _encryptionService.EncryptText(personalInformationText);

            _consumerFinanceSignupRepository.Update(signup);
        }

        /// <summary>
        /// Gets the consumer finance home address of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignup">Consumer finance signup</param>
        public virtual ConsumerFinanceHomeAddress GetSignupHomeAddress(ConsumerFinanceSignup consumerFinanceSignup)
        {
            if (consumerFinanceSignup == null)
                throw new ArgumentNullException(nameof(consumerFinanceSignup));

            if (string.IsNullOrWhiteSpace(consumerFinanceSignup.ConsumerFinanceHomeAddress))
                return null;

            var homeAddressText = _encryptionService.DecryptText(consumerFinanceSignup.ConsumerFinanceHomeAddress);
            return JsonConvert.DeserializeObject<ConsumerFinanceHomeAddress>(homeAddressText);
        }

        /// <summary>
        /// Sets the consumer finance home address of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignupId">Consumer finance signup identifier</param>
        /// <param name="homeAddress">Personal information</param>
        public virtual void SetSignupHomeAddress(int consumerFinanceSignupId, ConsumerFinanceHomeAddress homeAddress)
        {
            if (homeAddress == null)
                throw new ArgumentNullException(nameof(homeAddress));

            ConsumerFinanceSignup signup;

            if (consumerFinanceSignupId <= 0 || (signup = _consumerFinanceSignupRepository.GetById(consumerFinanceSignupId)) == null)
                throw new NopException("Consumer finance signup record not found.");

            string homeAddressText = JsonConvert.SerializeObject(homeAddress);
            signup.ConsumerFinanceHomeAddress = _encryptionService.EncryptText(homeAddressText);

            _consumerFinanceSignupRepository.Update(signup);
        }

        /// <summary>
        /// Gets the consumer finance employment of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignup">Consumer finance signup</param>
        public virtual ConsumerFinanceEmployment GetSignupEmployment(ConsumerFinanceSignup consumerFinanceSignup)
        {
            if (consumerFinanceSignup == null)
                throw new ArgumentNullException(nameof(consumerFinanceSignup));

            if (string.IsNullOrWhiteSpace(consumerFinanceSignup.ConsumerFinanceEmployment))
                return null;

            var employmentText = _encryptionService.DecryptText(consumerFinanceSignup.ConsumerFinanceEmployment);
            return JsonConvert.DeserializeObject<ConsumerFinanceEmployment>(employmentText);
        }

        /// <summary>
        /// Sets the consumer finance employment of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignupId">Consumer finance signup identifier</param>
        /// <param name="employment">Personal information</param>
        public virtual void SetSignupEmployment(int consumerFinanceSignupId, ConsumerFinanceEmployment employment)
        {
            if (employment == null)
                throw new ArgumentNullException(nameof(employment));

            ConsumerFinanceSignup signup;

            if (consumerFinanceSignupId <= 0 || (signup = _consumerFinanceSignupRepository.GetById(consumerFinanceSignupId)) == null)
                throw new NopException("Consumer finance signup record not found.");

            string employmentText = JsonConvert.SerializeObject(employment);
            signup.ConsumerFinanceEmployment = _encryptionService.EncryptText(employmentText);

            _consumerFinanceSignupRepository.Update(signup);
        }

        /// <summary>
        /// Gets the consumer finance income and expenditure of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignup">Consumer finance signup</param>
        public virtual ConsumerFinanceIncomeAndExpenditure GetSignupIncomeAndExpenditure(ConsumerFinanceSignup consumerFinanceSignup)
        {
            if (consumerFinanceSignup == null)
                throw new ArgumentNullException(nameof(consumerFinanceSignup));

            if (string.IsNullOrWhiteSpace(consumerFinanceSignup.ConsumerFinanceIncomeAndExpenditure))
                return null;

            var incomeAndExpenditureText = _encryptionService.DecryptText(consumerFinanceSignup.ConsumerFinanceIncomeAndExpenditure);
            return JsonConvert.DeserializeObject<ConsumerFinanceIncomeAndExpenditure>(incomeAndExpenditureText);
        }

        /// <summary>
        /// Sets the consumer finance income and expenditure of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignupId">Consumer finance signup identifier</param>
        /// <param name="incomeAndExpenditure">Personal information</param>
        public virtual void SetSignupIncomeAndExpenditure(int consumerFinanceSignupId, ConsumerFinanceIncomeAndExpenditure incomeAndExpenditure)
        {
            if (incomeAndExpenditure == null)
                throw new ArgumentNullException(nameof(incomeAndExpenditure));

            ConsumerFinanceSignup signup;

            if (consumerFinanceSignupId <= 0 || (signup = _consumerFinanceSignupRepository.GetById(consumerFinanceSignupId)) == null)
                throw new NopException("Consumer finance signup record not found.");

            string incomeAndExpenditureText = JsonConvert.SerializeObject(incomeAndExpenditure);
            signup.ConsumerFinanceIncomeAndExpenditure = _encryptionService.EncryptText(incomeAndExpenditureText);

            _consumerFinanceSignupRepository.Update(signup);
        }

        /// <summary>
        /// Gets the consumer finance bank of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignup">Consumer finance signup</param>
        public virtual ConsumerFinanceBank GetSignupBank(ConsumerFinanceSignup consumerFinanceSignup)
        {
            if (consumerFinanceSignup == null)
                throw new ArgumentNullException(nameof(consumerFinanceSignup));

            if (string.IsNullOrWhiteSpace(consumerFinanceSignup.ConsumerFinanceBank))
                return null;

            var bankText = _encryptionService.DecryptText(consumerFinanceSignup.ConsumerFinanceBank);
            return JsonConvert.DeserializeObject<ConsumerFinanceBank>(bankText);
        }

        /// <summary>
        /// Sets the consumer finance bank of the signup record
        /// </summary>
        /// <param name="consumerFinanceSignupId">Consumer finance signup identifier</param>
        /// <param name="bank">Personal information</param>
        public virtual void SetSignupBank(int consumerFinanceSignupId, ConsumerFinanceBank bank)
        {
            if (bank == null)
                throw new ArgumentNullException(nameof(bank));

            ConsumerFinanceSignup signup;

            if (consumerFinanceSignupId <= 0 || (signup = _consumerFinanceSignupRepository.GetById(consumerFinanceSignupId)) == null)
                throw new NopException("Consumer finance signup record not found.");

            string bankText = JsonConvert.SerializeObject(bank);
            signup.ConsumerFinanceBank = _encryptionService.EncryptText(bankText);

            _consumerFinanceSignupRepository.Update(signup);
        }

        /// <summary>
        /// Complete the consumer finance signup.
        /// This will perform all the necessary processes for validating the consumer's affordability, identity, etc.
        /// This will also create a new DocuSign envelope to be signed by the consumer once all the checks have been marked as passed
        /// </summary>
        /// <param name="customerId">Customer identifier. Will use the current customer if not provided</param>
        public virtual ConsumerFinanceSignup CompleteConsumerFinanceSignup(int customerId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            if (customer == null)
                throw new NopException("Customer not found.");

            var signup = GetLatestConsumerSignupOfCustomer(customer.Id);

            #region Form Completion Checks

            // Check if the current customer is allowed for consumer finance signup
            if (!CustomerCanSignup(out string notAllowedForSignupMessage, customer.Id))
                throw new NopException(notAllowedForSignupMessage);

            // Check if the latest signup has been completed.
            // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
            if (signup.SignupCompletedOnUtc.HasValue)
                throw new NopException("You already have finished signing up for financing.");

            // Check if personal information is okay
            var personalInformation = GetSignupPersonalInformation(signup);
            if (personalInformation == null)
                throw new NopException("Please provide your personal information first.");

            // Check if home address is okay
            var homeAddress = GetSignupHomeAddress(signup);
            if (homeAddress == null)
                throw new NopException("Please provide your home address information first.");

            // Check if employment is okay
            var employment = GetSignupEmployment(signup);
            if (employment == null)
                throw new NopException("Please provide your employment information first.");

            // Check if income and expenditure is okay
            var incomeAndExpenditure = GetSignupIncomeAndExpenditure(signup);
            if (incomeAndExpenditure == null)
                throw new NopException("Please provide your income and expenditure information first.");

            // Check if bank is okay
            var bank = GetSignupBank(signup);
            if (bank == null)
                throw new NopException("Please provide your bank information first");

            #endregion

            #region Affordability Check

            // Check if the customer has items on the shopping cart.
            ProductConsumerFinanceValues tempProductConsumerFinanceValues;

            var shoppingCartItems = customer.ShoppingCartItems
                .Where(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart
                    && x.StoreId == _storeContext.CurrentStore.Id)
                .ToList()
                .Join(_shoppingCartItemConsumerFinanceOptionRepository.Table,
                    sci => sci.Id,
                    scf => scf.ShoppingCartItemId,
                    (sci, scf) => new
                    {
                        ShoppingCartItem = sci,
                        ConsumerFinanceOption = scf.ConsumerFinanceOption,
                        ConsumerFinanceValues = tempProductConsumerFinanceValues = _consumerFinanceCalculationService.ComputeProductConsumerFinanceValues(sci.Product, sci.AttributesXml),
                        SelectedTermBreakdown = tempProductConsumerFinanceValues.TermsBreakdown.Single(t => t.DurationInMonths == scf.ConsumerFinanceOption.DurationInMonths)
                    })
                .ToList();

            if (!shoppingCartItems.Any())
                throw new NopException("You do not have any product for financing placed on your shopping cart.");

            // Compute total monthly payment
            decimal monthlyPayment = shoppingCartItems.Sum(x => x.SelectedTermBreakdown.MonthlyPayment);

            // get the sum of the midpoint values of the selected expenditure ranges selected by the consumer
            decimal totalExpenses = incomeAndExpenditure.ConsumerExpenditures.Sum(x => x.ConsumerExpenditureSpecificValueInGbp ?? GetConsumerExpenditureRangeById(x.ExpenditureRangeId).MidPointValue);

            decimal monthlyIncomeForComputation = incomeAndExpenditure.MonthlyIncomeAmountAfterTax > 5000 ? 5000 : incomeAndExpenditure.MonthlyIncomeAmountAfterTax;

            decimal availableIncome = monthlyIncomeForComputation - totalExpenses;

            /* the affordability check will be marked as passed if the available income is
                * less than or equal to the total monthly payment multiplied by 4 */
            bool affordabilityCheckPassed = (monthlyPayment * 4 <= availableIncome);

            #endregion

            #region CallValidate

            bool? callValidatePassed = null;
            CallValidateResult callValidateResult = null;

            callValidatePassed = true;
            callValidateResult = new CallValidateResult();

            if (affordabilityCheckPassed)
            {
                var annualGrossIncome = _annualGrossIncomeRepository.GetById(employment.AnnualGrossIncomeId).ValueInGbp;

                var data = new CallCredit.CallValidateApi.RequestEntities.Data
                {
                    Bankinformation = new BankInformation
                    {
                        Bankaccountnumber = bank.AccountNumber.Replace("-", ""),
                        Banksortcode = bank.SortCode.Replace("-", "")
                    },
                    IncomeDetails = new IncomeDetails
                    {
                        annualgrossincome = annualGrossIncome.ToString()
                    },
                    Personalinformation = new PersonalInformation
                    {
                        AddressDetails = new AddressDetails
                        {
                            Address1 = homeAddress.CurrentAddress.Address1,
                            Address2 = homeAddress.CurrentAddress.Address2,
                            Address3 = homeAddress.CurrentAddress.Address3,
                            Buildingnumber = homeAddress.CurrentAddress.BuildingNumber,
                            Postcode = homeAddress.CurrentAddress.Postcode,
                            Previousaddress1 = homeAddress.PreviousAddress.Address1,
                            Previousaddress2 = homeAddress.PreviousAddress.Address2,
                            Previousaddress3 = homeAddress.PreviousAddress.Address3,
                            Previousbuildingnumber = homeAddress.PreviousAddress.BuildingNumber,
                            Previouspostcode = homeAddress.PreviousAddress.Postcode,
                            Previoustown = homeAddress.PreviousAddress.CityTown,
                            Town = homeAddress.CurrentAddress.CityTown
                        },
                        IndividualDetails = new IndividualDetails
                        {
                            Addresslessthan12months = (homeAddress.CurrentAddress.LengthOfStayMonths > 12 || homeAddress.CurrentAddress.LengthOfStayYears > 0) ? "yes" : "no",
                            Dateofbirth = personalInformation.DateOfBirth.ToString("dd-MM-yyyy"),
                            Emailaddress = personalInformation.Email,
                            Firstname = personalInformation.FirstName,
                            IPAddress = _webHelper.GetCurrentIpAddress(),
                            Othernames = personalInformation.MiddleName,
                            Phonenumber = (personalInformation.MobilePhoneNumber ?? "").Replace("+", "").Replace(" ", ""),
                            Surname = personalInformation.LastName,
                            Title = personalInformation.Title
                        }
                    }
                };

                callValidateResult = _callValidateService.PerformCallValidateChecks(
                    _consumerFinanceSettings.CallValidateCompany,
                    _consumerFinanceSettings.CallValidateUsername,
                    _consumerFinanceSettings.CallValidatePassword,
                    _consumerFinanceSettings.CallValidateApplication,
                    data,
                    _consumerFinanceSettings.CallValidateTestApiUrl,
                    _consumerFinanceSettings.CallValidateLiveApiUrl,
                    _consumerFinanceSettings.UseCallValidateLiveApi,
                    customer.Id);

                var callValidateResults = _callValidateService.GetCallValidateResultResults(callValidateResult);

                if (callValidateResults.Errors.Error.Count > 0 || callValidateResults.Errors.FatalError.Count > 0)
                {
                    callValidatePassed = false;
                }
                else if (callValidateResults.Result.Displays.OtherChecks.IdentityResult != "Pass" || callValidateResults.Result.Displays.BankcheckEnhanced.Result != "Pass")
                {
                    callValidatePassed = false;
                }
                else
                {
                    callValidatePassed = true;
                }
            }

            #endregion

            #region CallReport

            bool? callReportPassed = null;
            CallReportResult callReportResult = null;

            if (callValidatePassed.HasValue && callValidatePassed.Value)
            {
                var callReportSearchDefinition = InitializeCallReportSearchDefinition(personalInformation, homeAddress);

                var searchResult = _callReportService.CallReportSearch(
                    _consumerFinanceSettings.CallReportCompany,
                    _consumerFinanceSettings.CallReportUsername,
                    _consumerFinanceSettings.CallReportPassword,
                    callReportSearchDefinition,
                    _consumerFinanceSettings.CallReportTestApiUrl,
                    _consumerFinanceSettings.CallReportLiveApiUrl,
                    _consumerFinanceSettings.UseCallReportLiveApi);

                // Save call report result
                callReportResult = _callReportService.SaveCallReportResult(searchResult, customer.Id);

                // If no credit report, process picklists
                bool withPicklist = false;
                if (searchResult.creditreport == null)
                {
                    // check if a picklist has been generated
                    var picklistResult = _callReportService.ProcessPicklist(searchResult.picklist);

                    if ((picklistResult.NameMatchSelectionList?.Any() ?? false) || (picklistResult.AddressMatchSelectionList?.Any() ?? false))
                    {
                        withPicklist = true;
                    }
                }

                var applicant = searchResult.creditreport.applicant[0];

                if (!withPicklist)
                {
                    if (applicant.reporttype == "0" || applicant.creditscores == null || applicant.creditscores.Length == 0 || applicant.creditscores[0].score.Value <= _consumerFinanceSettings.CallReportPassingScore)
                    {
                        callReportPassed = false;
                    }
                    else
                    {
                        callReportPassed = true;
                    }
                }
            }

            //// TODO remove this
            //callReportPassed = true;

            #endregion

            _consumerFinanceOrderItemRepository.Delete(signup.OrderItems.ToList());
            signup = _consumerFinanceSignupRepository.GetById(signup.Id);

            // Register customer if not yet registered
            var existingCustomer = _customerService.GetCustomerByEmail(personalInformation.Email);
            if (existingCustomer == null)
            {
                var registrationResult = RegisterCustomer(customer, personalInformation, homeAddress, employment);
                if (registrationResult.Success)
                {
                    // Clear the customer provided password
                    personalInformation.Password = null;
                    string personalInformationText = JsonConvert.SerializeObject(personalInformation);
                    signup.ConsumerFinancePersonalInformation = _encryptionService.EncryptText(personalInformationText);
                }
            }

            // Save items
            shoppingCartItems.ForEach(x =>
            {
                signup.OrderItems.Add(new ConsumerFinanceOrderItem
                {
                    Apr = x.ConsumerFinanceValues.Apr,
                    ConsumerFinanceSignupId = signup.Id,
                    CurrencyCode = x.ConsumerFinanceValues.CurrencyCode,
                    DocumentationFee = x.ConsumerFinanceValues.DocumentationFee,
                    DurationOfAgreement = x.SelectedTermBreakdown.DurationInMonths,
                    AgreementRate = x.SelectedTermBreakdown.AgreementRate,
                    InitialPayment = x.ConsumerFinanceValues.InitialPayment,
                    MonthlyPayment = x.SelectedTermBreakdown.MonthlyPayment,
                    ProductAttributeDescription = x.ConsumerFinanceValues.ProductAttributeDescription,
                    ProductAttributesXml = x.ShoppingCartItem.AttributesXml,
                    ProductId = x.ShoppingCartItem.ProductId,
                    ProductPriceInclTax = x.ConsumerFinanceValues.Price,
                    ProductQuantity = x.ShoppingCartItem.Quantity,
                    ShoppingCartItemId = x.ShoppingCartItem.Id,
                    ProductSku = x.ConsumerFinanceValues.ProductSku,
                    TaxRate = x.ConsumerFinanceValues.TaxRate,
                    TotalCreditOrFinanceCharges = x.SelectedTermBreakdown.TotalCreditOrFinanceCharges,
                    TotalPayable = x.SelectedTermBreakdown.TotalPayable
                });
            });

            // Apply changes to signup record
            signup.AffordabilityCheckPassed = affordabilityCheckPassed;

            if (affordabilityCheckPassed)
            {
                signup.CallValidatePassed = callValidatePassed;
                signup.CallValidateResultId = callValidateResult.Id;

                if (callValidatePassed.Value)
                {
                    signup.CallReportPassed = callReportPassed;
                    signup.CallReportResultId = callReportResult.Id; //TODO Uncomment this
                }
            }

            _consumerFinanceSignupRepository.Update(signup);

            if (signup.AffordabilityCheckPassed.Value
                && signup.CallValidatePassed.HasValue && signup.CallValidatePassed.Value
                && !signup.CallReportPassed.HasValue)
            {
                // This means that the consumer has a picklist
                return signup;
            }

            if (signup.AllChecksPassed)
            {
                // Process documents
                signup.DocuSignEnvelopeId = PrepareAndSendAgreementDocuments(signup).EnvelopeID;
            }

            if (!signup.AffordabilityCheckPassed.Value ||
                (signup.AffordabilityCheckPassed.Value && signup.CallValidatePassed.HasValue && !signup.CallValidatePassed.Value) ||
                (signup.AffordabilityCheckPassed.Value && signup.CallValidatePassed.HasValue && signup.CallReportPassed.HasValue))
                signup.SignupCompletedOnUtc = DateTime.UtcNow;

            _consumerFinanceSignupRepository.Update(signup);

            return signup;
        }

        /// <summary>
        /// Checks if the customer is allowed to update the shopping cart
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        public virtual bool CustomerCanUpdateShoppingCart(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            storeId = storeId > 0 ? storeId : _storeContext.CurrentStore.Id;

            var shoppingCartItemIds = customer.ShoppingCartItems
                .Where(s => s.ShoppingCartTypeId == (int)ShoppingCartType.ShoppingCart && s.StoreId == storeId)
                .Select(s => s.Id)
                .ToList();

            // Check if one of the shopping cart items exists on the consumer finance order item repository
            bool shoppingCartItemExists = _consumerFinanceOrderItemRepository.Table.Any(oi => oi.ShoppingCartItemId.HasValue && shoppingCartItemIds.Contains(oi.ShoppingCartItemId.Value));

            // If there are records, it means that the consumer has completed and passed 
            // the signup process and still needs to finish the following processes.
            return !shoppingCartItemExists;
        }

        /// <summary>
        /// Updates the Order item identifier associated with the consumer finance signup
        /// </summary>
        /// <param name="shoppingCartId">Shopping cart identifier</param>
        /// <param name="orderItemId">Order item identifier</param>
        public virtual void UpdateConsumerFinanceOrderItemOrderItemId(int shoppingCartId, int orderItemId)
        {
            var items = _consumerFinanceOrderItemRepository.Table.Where(i => i.ShoppingCartItemId == shoppingCartId).ToList();
            items.ForEach(s =>
            {
                s.OrderItemId = orderItemId;
                s.ShoppingCartItemId = null;
            });

            _consumerFinanceOrderItemRepository.Update(items);
        }

        /// <summary>
        /// Updates the order identifier associated with the consumer finance signup
        /// </summary>
        /// <param name="consumerFinanceSignupId">Consumer finance signup identifier</param>
        /// <param name="orderId">Order identifier</param>
        public virtual void UpdateConsumerFinanceSignupOrderId(int consumerFinanceSignupId, int orderId)
        {
            var signup = _consumerFinanceSignupRepository.GetById(consumerFinanceSignupId);
            if (signup == null)
                throw new NopException("Signup record not found.");

            signup.OrderId = orderId;
            _consumerFinanceSignupRepository.Update(signup);
        }

        /// <summary>
        /// Gets the consumer finance signup associated with the order
        /// </summary>
        /// <param name="orderId">Order identifier</param>
        public virtual ConsumerFinanceSignup GetConsumerFinanceSignupByOrderId(int orderId)
        {
            return _consumerFinanceSignupRepository.Table.FirstOrDefault(s => s.OrderId == orderId);
        }

        /// <summary>
        /// Gets a consumer finance order items by order item identifiers
        /// </summary>
        /// <param name="orderItemIds">Order item identifiers</param>
        public virtual IEnumerable<ConsumerFinanceOrderItem> GetConsumerFinanceOrderItemsByOrderItemIds(IEnumerable<int> orderItemIds)
        {
            return _consumerFinanceOrderItemRepository.Table.Where(x => x.OrderItemId.HasValue && orderItemIds.Contains(x.OrderItemId.Value));
        }

        /// <summary>
        /// Gets the consumer finance signups associated with the provided orders
        /// </summary>
        /// <param name="orderIds">Order identifiers</param>
        public virtual IEnumerable<ConsumerFinanceSignup> GetConsumerFinanceSignupsByOrderIds(IEnumerable<int> orderIds)
        {
            return _consumerFinanceSignupRepository.Table.Where(x => x.OrderId.HasValue && orderIds.Contains(x.OrderId.Value));
        }

        /// <summary>
        /// Gets a consumer finance order item by order item identifier
        /// </summary>
        /// <param name="orderItemId">Order item identifier</param>
        public virtual ConsumerFinanceOrderItem GetConsumerFinanceOrderItemByOrderItemId(int orderItemId)
        {
            return _consumerFinanceOrderItemRepository.Table.FirstOrDefault(oi => oi.OrderItemId == orderItemId);
        }

        /// <summary>
        /// Check if the customer has a pending DocuSign Envelope to be signed
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier</param>
        public virtual bool HasPendingDocuSignEnvelope(int customerId = 0, int storeId = 0)
        {
            var customer = customerId > 0 ? _customerService.GetCustomerById(customerId) : _workContext.CurrentCustomer;
            storeId = storeId > 0 ? storeId : _storeContext.CurrentStore.Id;

            // Check if current customer has item in cart
            if (!customer.ShoppingCartItems.Any(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart && x.StoreId == storeId))
                return false;

            // Check if customer is already done with finance application (Get latest consumer signup record)
            var signup = GetLatestConsumerSignupOfCustomer(customerId);
            if (signup == null)
            {
                return false;
            }
            else
            {
                // Check if has envelope
                if (!string.IsNullOrWhiteSpace(signup.DocuSignEnvelopeId))
                {
                    var docuSignStatus = _docuSignService.RequestEnvelopeStatus(signup.DocuSignEnvelopeId).Status;

                    // Check if pending, cancelled or completed
                    if (docuSignStatus == DocuSignWeb.EnvelopeStatusCode.Voided || docuSignStatus == DocuSignWeb.EnvelopeStatusCode.Completed || docuSignStatus == DocuSignWeb.EnvelopeStatusCode.Declined || docuSignStatus == DocuSignWeb.EnvelopeStatusCode.Deleted)
                        return false;
                    else
                        return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Returns the link to view the consumer finance agreement documents
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public virtual string GetOrderConsumerFinanceAgreementDocumentsLink(int orderId)
        {
            var signup = _consumerFinanceSignupRepository.Table.OrderByDescending(s => s.Id).FirstOrDefault(s => s.OrderId == orderId);
            if (signup == null || string.IsNullOrEmpty(signup.DocuSignEnvelopeId))
                return null;

            var personalInformation = GetSignupPersonalInformation(signup);
            if (personalInformation == null)
                return null;

            return _docuSignService.RequestRecipientToken(
                signup.DocuSignEnvelopeId, 
                signup.CustomerId.ToString(), 
                string.Join(" ", personalInformation.FirstName, personalInformation.LastName), 
                personalInformation.Email, 
                _storeContext.CurrentStore.Id);
        }

        #endregion
    }
}