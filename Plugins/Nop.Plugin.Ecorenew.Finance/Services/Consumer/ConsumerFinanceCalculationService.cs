﻿using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Tax;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Ecorenew.Finance.Services.Consumer
{
    /// <summary>
    /// Consumer finance calculation service
    /// </summary>
    public class ConsumerFinanceCalculationService : IConsumerFinanceCalculationService
    {
        #region Fields

        private readonly IRepository<ConsumerFinanceOption> _consumerFinanceOptionRepository;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ITaxService _taxService;
        private readonly IWorkContext _workContext;
        private readonly ConsumerFinanceSettings _consumerFinanceSettings;

        #endregion

        #region Ctor

        public ConsumerFinanceCalculationService(IRepository<ConsumerFinanceOption> consumerFinanceOptionRepository,
            ICurrencyService currencyService,
            IPriceCalculationService priceCalculationService,
            IProductAttributeFormatter productAttributeFormatter,
            IProductAttributeParser productAttributeParser,
            ITaxService taxService,
            IWorkContext workContext,
            ConsumerFinanceSettings consumerFinanceSettings)
        {
            this._consumerFinanceOptionRepository = consumerFinanceOptionRepository;
            this._currencyService = currencyService;
            this._priceCalculationService = priceCalculationService;
            this._productAttributeFormatter = productAttributeFormatter;
            this._productAttributeParser = productAttributeParser;
            this._taxService = taxService;
            this._workContext = workContext;
            this._consumerFinanceSettings = consumerFinanceSettings;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Computes the finance prices and terms breakdown for a specific product
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="productAttributesXml">Product attributes in XML format</param>
        /// <param name="currencyCode">Currency code to be used. Leave as null to use the current working currency</param>
        public virtual ProductConsumerFinanceValues ComputeProductConsumerFinanceValues(Product product, string productAttributesXml, string currencyCode = null)
        {
            var result = new ProductConsumerFinanceValues
            {
                Apr = _consumerFinanceSettings.AprPercentage,
                ProductAttributeDescription = _productAttributeFormatter.FormatAttributes(product, productAttributesXml),
                ProductAttributesXml = productAttributesXml,
                ProductId = product.Id,
                ProductSku = product.FormatSku(productAttributesXml, _productAttributeParser)
            };

            var gbp = _currencyService.GetCurrencyByCode("GBP");

            // Get working currency
            var currencyUsed = currencyCode == null ? _workContext.WorkingCurrency : _currencyService.GetCurrencyByCode(currencyCode);
            result.CurrencyCode = currencyUsed.CurrencyCode;

            decimal sellingPriceInclTaxBase = _taxService.GetProductPrice(product, _priceCalculationService.GetUnitPrice(product, _workContext.CurrentCustomer, ShoppingCartType.ShoppingCart, 1, productAttributesXml, decimal.Zero, null, null, false, out decimal discountAmount, out List<DiscountForCaching> discountsForCaching), out decimal taxRate);
            decimal sellingPriceInclTax = _currencyService.ConvertFromPrimaryStoreCurrency(sellingPriceInclTaxBase, currencyUsed);

            result.TaxRate = taxRate;
            result.Price = sellingPriceInclTax; //+ result.DocumentationFee; Documentation fee should already be included from the ITaxService.GetProductPrice method
            result.DocumentationFee = _currencyService.ConvertCurrency(_consumerFinanceSettings.DocumentationFeeGbp, gbp, currencyUsed);
            result.OptionToPurchaseFee = _currencyService.ConvertCurrency(_consumerFinanceSettings.OptionToPurchaseFeeGbp, gbp, currencyUsed);

            // Get initial payment
            decimal priceInGbp = RoundingHelper.RoundPrice(_currencyService.ConvertCurrency(result.Price, currencyUsed, gbp));
            decimal initalPaymentGbp =
                priceInGbp >= 900 ? _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp900 : (
                priceInGbp >= 600 ? _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp600 : (
                priceInGbp >= 400 ? _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp400 : (
                priceInGbp >= 350 ? _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp350 : (
                priceInGbp >= 300 ? _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp300 : (
                priceInGbp >= 250 ? _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp250 : (
                priceInGbp >= 200 ? _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp200 : (
                priceInGbp >= 150 ? _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp150 :
                _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp0)))))));

            result.InitialPayment = _currencyService.ConvertCurrency(initalPaymentGbp, gbp, currencyUsed);

            result.TermsBreakdown = new List<ConsumerFinanceTermBreakdown>();

            _consumerFinanceOptionRepository.Table
                .Where(o => o.IsActive)
                .OrderBy(o => o.DurationInMonths)
                .ToList()
                .ForEach(o =>
                {
                    var breakdown = new ConsumerFinanceTermBreakdown
                    {
                        AgreementRate = o.AgreementRate,
                        DurationInMonths = o.DurationInMonths,
                        MonthlyPayment = RoundingHelper.RoundPrice(((result.Price - result.InitialPayment) * o.AgreementRate) / 1000)
                    };

                    breakdown.TotalPayable = result.InitialPayment + (breakdown.MonthlyPayment * o.DurationInMonths);
                    breakdown.TotalCreditOrFinanceCharges = breakdown.TotalPayable - result.Price;

                    result.TermsBreakdown.Add(breakdown);
                });

            return result;
        }

        #endregion
    }
}