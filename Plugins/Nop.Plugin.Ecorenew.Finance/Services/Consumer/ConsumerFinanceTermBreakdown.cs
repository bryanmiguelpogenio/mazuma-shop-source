﻿namespace Nop.Plugin.Ecorenew.Finance.Services.Consumer
{
    /// <summary>
    /// Represents a breakdown of prices and other details of a consumer finance term
    /// </summary>
    public class ConsumerFinanceTermBreakdown
    {
        public int DurationInMonths { get; set; }

        public decimal AgreementRate { get; set; }
        public decimal MonthlyPayment { get; set; }
        public decimal TotalCreditOrFinanceCharges { get; set; }
        public decimal TotalPayable { get; set; }
    }
}