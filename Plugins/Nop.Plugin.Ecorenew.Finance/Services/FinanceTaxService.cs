﻿using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Core.Plugins;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.Logging;
using Nop.Services.Tax;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;

namespace Nop.Plugin.Ecorenew.Finance.Services
{
    /// <summary>
    /// Custom implementation of TaxService for consumer financing
    /// </summary>
    public class FinanceTaxService : TaxService
    {
        #region Fields

        private readonly ICurrencyService _currencyService;
        private readonly ConsumerFinanceSettings _consumerFinanceSettings;
        private readonly FinanceSettings _financeSettings;

        #endregion

        #region Ctor

        public FinanceTaxService(IAddressService addressService,
            ICountryService countryService,
            ICurrencyService currencyService,
            IGeoLookupService geoLookupService,
            ILogger logger,
            IPluginFinder pluginFinder,
            IStateProvinceService stateProvinceService,
            IStoreContext storeContext,
            IWebHelper webHelper,
            IWorkContext workContext,
            AddressSettings addressSettings,
            ConsumerFinanceSettings consumerFinanceSettings,
            CustomerSettings customerSettings,
            ShippingSettings shippingSettings,
            TaxSettings taxSettings,
            FinanceSettings financeSettings)
            : base(addressService,
            workContext,
            storeContext,
            taxSettings,
            pluginFinder,
            geoLookupService,
            countryService,
            stateProvinceService,
            logger,
            webHelper,
            customerSettings,
            shippingSettings,
            addressSettings)
        {
            this._currencyService = currencyService;
            this._consumerFinanceSettings = consumerFinanceSettings;
            this._financeSettings = financeSettings;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets price
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="taxCategoryId">Tax category identifier</param>
        /// <param name="price">Price</param>
        /// <param name="includingTax">A value indicating whether calculated price should include tax</param>
        /// <param name="customer">Customer</param>
        /// <param name="priceIncludesTax">A value indicating whether price already includes tax</param>
        /// <param name="taxRate">Tax rate</param>
        /// <returns>Price</returns>
        public override decimal GetProductPrice(Product product, int taxCategoryId,
            decimal price, bool includingTax, Customer customer,
            bool priceIncludesTax, out decimal taxRate)
        {
            decimal productPrice = base.GetProductPrice(product, taxCategoryId, price, includingTax, customer, priceIncludesTax, out taxRate);

            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        // Convert DocumentationFee to primary store currency, then add it to the product price (TAX included or not)
                        var documentationFee = _currencyService.ConvertToPrimaryStoreCurrency(_consumerFinanceSettings.DocumentationFeeGbp, _currencyService.GetCurrencyByCode("GBP"));
                        productPrice += documentationFee;
                        break;
                    case FinanceType.Corporate:
                        break;
                }
            }

            return productPrice;
        }

        #endregion
    }
}