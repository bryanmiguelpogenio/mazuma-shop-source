﻿using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Tax;
using System.Collections.Generic;
using System.Linq;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;

namespace Nop.Plugin.Ecorenew.Finance.Services
{
    public class FinancePriceCalculationService : PriceCalculationService
    {
        #region Fields

        private readonly ICurrencyService _currencyService;
        private readonly ITaxService _taxService;
        private readonly CurrencySettings _currencySettings;
        private readonly FinanceSettings _financeSettings;

        #endregion

        #region Ctor

        public FinancePriceCalculationService(IWorkContext workContext,
            IStoreContext storeContext,
            IDiscountService discountService,
            ICategoryService categoryService,
            ICurrencyService currencyService,
            IManufacturerService manufacturerService,
            IProductAttributeParser productAttributeParser,
            IProductService productService,
            IStaticCacheManager cacheManager,
            ITaxService taxService,
            ShoppingCartSettings shoppingCartSettings,
            CatalogSettings catalogSettings,
            CurrencySettings currencySettings,
            FinanceSettings financeSettings)
            : base (workContext,
            storeContext,
            discountService,
            categoryService,
            manufacturerService,
            productAttributeParser,
            productService,
            cacheManager,
            shoppingCartSettings,
            catalogSettings)
        {
            this._currencyService = currencyService;
            this._taxService = taxService;
            this._currencySettings = currencySettings;
            this._financeSettings = financeSettings;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the shopping cart unit price (one item)
        /// </summary>
        /// <param name="shoppingCartItem">The shopping cart item</param>
        /// <param name="includeDiscounts">A value indicating whether include discounts or not for price computation</param>
        /// <returns>Shopping cart unit price (one item)</returns>
        public override decimal GetUnitPrice(ShoppingCartItem shoppingCartItem, bool includeDiscounts = true)
        {
            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        var consumerFinanceService = EngineContext.Current.Resolve<IConsumerFinanceService>();
                        var shoppingCartItemConsumerFinanceOptions = consumerFinanceService.GetCustomerShoppingCartItemConsumerFinanceOptions();

                        if (shoppingCartItemConsumerFinanceOptions.Any(s => s.ShoppingCartItemId == shoppingCartItem.Id))
                        {
                            var consumerFinanceCalculationService = EngineContext.Current.Resolve<IConsumerFinanceCalculationService>();

                            var financeValues = consumerFinanceCalculationService.ComputeProductConsumerFinanceValues(
                                shoppingCartItem.Product,
                                shoppingCartItem.AttributesXml,
                                _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode);

                            _taxService.GetProductPrice(shoppingCartItem.Product, financeValues.InitialPayment, out decimal taxRate);

                            return financeValues.InitialPayment / (1 + (taxRate / 100));
                        }
                        break;
                    case FinanceType.Corporate:
                        var corporateFinanceService = EngineContext.Current.Resolve<ICorporateFinanceService>();
                        var shoppingCartItemCorporateFinanceOptions = corporateFinanceService.GetCustomerShoppingCartItemCorporateFinanceOptions();

                        var option = shoppingCartItemCorporateFinanceOptions.SingleOrDefault(s => s.ShoppingCartItemId == shoppingCartItem.Id);

                        if (option != null)
                        {
                            var corporateFinanceCalculationService = EngineContext.Current.Resolve<ICorporateFinanceCalculationService>();

                            var financeValues = corporateFinanceCalculationService.ComputeProductCorporateFinanceValues(
                                shoppingCartItem.Product,
                                shoppingCartItem.AttributesXml,
                                _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode);
                            
                            return financeValues.TermsBreakdown.Single(x => x.DurationInMonths == option.CorporateFinanceOption.DurationInMonths).MonthlyPayment;
                        }
                        break;
                }
            }
 
            return base.GetUnitPrice(shoppingCartItem, includeDiscounts);
        }

        /// <summary>
        /// Gets the shopping cart unit price (one item)
        /// </summary>
        /// <param name="shoppingCartItem">The shopping cart item</param>
        /// <param name="includeDiscounts">A value indicating whether include discounts or not for price computation</param>
        /// <param name="discountAmount">Applied discount amount</param>
        /// <param name="appliedDiscounts">Applied discounts</param>
        /// <returns>Shopping cart unit price (one item)</returns>
        public override decimal GetUnitPrice(ShoppingCartItem shoppingCartItem, bool includeDiscounts, out decimal discountAmount, out List<DiscountForCaching> appliedDiscounts)
        {
            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        var consumerFinanceService = EngineContext.Current.Resolve<IConsumerFinanceService>();
                        var shoppingCartItemConsumerFinanceOptions = consumerFinanceService.GetCustomerShoppingCartItemConsumerFinanceOptions();

                        if (shoppingCartItemConsumerFinanceOptions.Any(s => s.ShoppingCartItemId == shoppingCartItem.Id))
                        {
                            var consumerFinanceCalculationService = EngineContext.Current.Resolve<IConsumerFinanceCalculationService>();

                            var financeValues = consumerFinanceCalculationService.ComputeProductConsumerFinanceValues(
                                shoppingCartItem.Product,
                                shoppingCartItem.AttributesXml,
                                _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode);

                            discountAmount = 0;
                            appliedDiscounts = new List<DiscountForCaching>();

                            _taxService.GetProductPrice(shoppingCartItem.Product, financeValues.InitialPayment, out decimal taxRate);

                            return financeValues.InitialPayment / (1 + (taxRate / 100));
                        }
                        break;
                    case FinanceType.Corporate:
                        var corporateFinanceService = EngineContext.Current.Resolve<ICorporateFinanceService>();
                        var shoppingCartItemCorporateFinanceOptions = corporateFinanceService.GetCustomerShoppingCartItemCorporateFinanceOptions();

                        var option = shoppingCartItemCorporateFinanceOptions.SingleOrDefault(s => s.ShoppingCartItemId == shoppingCartItem.Id);

                        if (option != null)
                        {
                            var corporateFinanceCalculationService = EngineContext.Current.Resolve<ICorporateFinanceCalculationService>();

                            var financeValues = corporateFinanceCalculationService.ComputeProductCorporateFinanceValues(
                                shoppingCartItem.Product,
                                shoppingCartItem.AttributesXml,
                                _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode);

                            discountAmount = 0;
                            appliedDiscounts = new List<DiscountForCaching>();
                            
                            return financeValues.TermsBreakdown.Single(x => x.DurationInMonths == option.CorporateFinanceOption.DurationInMonths).MonthlyPayment;
                        }
                        break;
                }
            }

            return base.GetUnitPrice(shoppingCartItem, includeDiscounts, out discountAmount, out appliedDiscounts);
        }

        #endregion
    }
}