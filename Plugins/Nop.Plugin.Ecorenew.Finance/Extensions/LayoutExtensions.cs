﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Web.Framework.Themes;
using Nop.Web.Framework.UI;
using System;

namespace Nop.Plugin.Ecorenew.Finance.Extensions
{
    public static class LayoutExtensions
    {
        private const string PLUGIN_URL = "~/Plugins/Ecorenew.Finance";

        public static void AddFinanceCssFileParts(this IHtmlHelper html, string src, string debugSrc = "", bool excludeFromBundle = false)
        {
            if (src.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                html.AddCssFileParts(src, debugSrc, excludeFromBundle);
            }
            else
            {
                src = src.Replace(".css", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themedSrc = $"{PLUGIN_URL}/Themes/{themeName}/Content/css/{src}.css";

                if (System.IO.File.Exists(CommonHelper.MapPath(themedSrc)))
                    html.AddCssFileParts(themedSrc, debugSrc, excludeFromBundle);
                else
                    html.AddCssFileParts($"{PLUGIN_URL}/Content/css/{src}.css", debugSrc, excludeFromBundle);
            }
        }

        public static void AddFinanceCssFileParts(this IHtmlHelper html, ResourceLocation location, string src, string debugSrc = "", bool excludeFromBundle = false)
        {
            if (src.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                html.AddCssFileParts(location, src, debugSrc, excludeFromBundle);
            }
            else
            {
                src = src.Replace(".css", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themedSrc = $"{PLUGIN_URL}/Themes/{themeName}/Content/css/{src}.css";

                if (System.IO.File.Exists(CommonHelper.MapPath(themedSrc)))
                    html.AddCssFileParts(location, themedSrc, debugSrc, excludeFromBundle);
                else
                    html.AddCssFileParts(location, $"{PLUGIN_URL}/Content/css/{src}.css", debugSrc, excludeFromBundle);
            }
        }

        public static void AddFinanceScriptParts(this IHtmlHelper html, ResourceLocation location,
            string src, string debugSrc = "", bool excludeFromBundle = false, bool isAsync = false)
        {
            if (src.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                html.AddScriptParts(location, src, debugSrc, excludeFromBundle, isAsync);
            }
            else
            {
                src = src.Replace(".js", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themedSrc = $"{PLUGIN_URL}/Themes/{themeName}/Scripts/{src}.js";

                if (System.IO.File.Exists(CommonHelper.MapPath(themedSrc)))
                    html.AddScriptParts(location, themedSrc, debugSrc, excludeFromBundle, isAsync);
                else
                    html.AddScriptParts(location, $"{PLUGIN_URL}/Scripts/{src}.js", debugSrc, excludeFromBundle, isAsync);
            }
        }

        public static string FinanceUrl(this IHtmlHelper html, string url)
        {
            var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

            var themedUrl = $"{PLUGIN_URL}/Themes/{themeName}/{url}";

            if (System.IO.File.Exists(CommonHelper.MapPath(themedUrl)))
                return themedUrl;
            else
                return $"{PLUGIN_URL}/{url}";
        }
    }
}