﻿using Autofac;
using Autofac.Core;
using Nop.Core;
using Nop.Core.Data;
using Nop.Data;

namespace Nop.Plugin.Ecorenew.Finance.Extensions
{
    public static class DependencyExtensions
    {
        public static void RegisterRepository<T>(this ContainerBuilder builder, string contextName) where T : BaseEntity
        {
            builder.RegisterType<EfRepository<T>>()
                .As<IRepository<T>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(contextName))
                .InstancePerLifetimeScope();
        }

        public static void RegisterService<TDerived, TBase>(this ContainerBuilder builder)
        {
            builder.RegisterType<TDerived>()
                .As<TBase>()
                .InstancePerLifetimeScope();
        }
    }
}