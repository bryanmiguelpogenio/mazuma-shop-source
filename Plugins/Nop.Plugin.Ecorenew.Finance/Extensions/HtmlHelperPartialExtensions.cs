﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Web.Framework.Themes;
using System;
using System.Threading.Tasks;

namespace Nop.Plugin.Ecorenew.Finance.Extensions
{
    public static class HtmlHelperPartialExtensions
    {
        private const string PLUGIN_URL = "~/Plugins/Ecorenew.Finance";

        public static IHtmlContent FinanceAdminPartial(this IHtmlHelper htmlHelper, string partialViewName)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return htmlHelper.Partial(partialViewName);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                return htmlHelper.Partial($"{PLUGIN_URL}/Areas/Admin/Views/{partialViewName}.cshtml");
            }
        }

        public static IHtmlContent FinanceAdminPartial(this IHtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return htmlHelper.Partial(partialViewName, viewData);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");
                
                return htmlHelper.Partial($"{PLUGIN_URL}/Areas/Admin/Views/{partialViewName}.cshtml", viewData);
            }
        }

        public static IHtmlContent FinanceAdminPartial(this IHtmlHelper htmlHelper, string partialViewName, object model)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return htmlHelper.Partial(partialViewName, model);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");
                
                return htmlHelper.Partial($"{PLUGIN_URL}/Areas/Admin/Views/{partialViewName}.cshtml", model);
            }
        }

        public async static Task<IHtmlContent> FinanceAdminPartialAsync(this IHtmlHelper htmlHelper, string partialViewName)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return await htmlHelper.PartialAsync(partialViewName);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");
                
                return await htmlHelper.PartialAsync($"{PLUGIN_URL}/Areas/Admin/Views/{partialViewName}.cshtml");
            }
        }

        public static async Task<IHtmlContent> FinanceAdminPartialAsync(this IHtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return await htmlHelper.PartialAsync(partialViewName, viewData);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");
                
                return await htmlHelper.PartialAsync($"{PLUGIN_URL}/Areas/Admin/Views/{partialViewName}.cshtml", viewData);
            }
        }

        public static async Task<IHtmlContent> FinanceAdminPartialAsync(this IHtmlHelper htmlHelper, string partialViewName, object model)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return await htmlHelper.PartialAsync(partialViewName, model);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");
                
                return await htmlHelper.PartialAsync($"{PLUGIN_URL}/Areas/Admin/Views/{partialViewName}.cshtml", model);
            }
        }

        #region Consumer

        public static IHtmlContent ConsumerFinancePartial(this IHtmlHelper htmlHelper, string partialViewName)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return htmlHelper.Partial(partialViewName);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/{partialViewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return htmlHelper.Partial(themeViewName);

                return htmlHelper.Partial($"{PLUGIN_URL}/Views/Consumer/{partialViewName}.cshtml");
            }
        }

        public static IHtmlContent ConsumerFinancePartial(this IHtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return htmlHelper.Partial(partialViewName, viewData);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/{partialViewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return htmlHelper.Partial(themeViewName, viewData);

                return htmlHelper.Partial($"{PLUGIN_URL}/Views/Consumer/{partialViewName}.cshtml", viewData);
            }
        }

        public static IHtmlContent ConsumerFinancePartial(this IHtmlHelper htmlHelper, string partialViewName, object model)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return htmlHelper.Partial(partialViewName, model);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/{partialViewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return htmlHelper.Partial(themeViewName, model);

                return htmlHelper.Partial($"{PLUGIN_URL}/Views/Consumer/{partialViewName}.cshtml", model);
            }
        }

        public async static Task<IHtmlContent> ConsumerFinancePartialAsync(this IHtmlHelper htmlHelper, string partialViewName)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return await htmlHelper.PartialAsync(partialViewName);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/{partialViewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return await htmlHelper.PartialAsync(themeViewName);

                return await htmlHelper.PartialAsync($"{PLUGIN_URL}/Views/Consumer/{partialViewName}.cshtml");
            }
        }

        public static async Task<IHtmlContent> ConsumerFinancePartialAsync(this IHtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return await htmlHelper.PartialAsync(partialViewName, viewData);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/{partialViewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return await htmlHelper.PartialAsync(themeViewName, viewData);

                return await htmlHelper.PartialAsync($"{PLUGIN_URL}/Views/Consumer/{partialViewName}.cshtml", viewData);
            }
        }

        public static async Task<IHtmlContent> ConsumerFinancePartialAsync(this IHtmlHelper htmlHelper, string partialViewName, object model)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return await htmlHelper.PartialAsync(partialViewName, model);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/{partialViewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return await htmlHelper.PartialAsync(themeViewName, model);

                return await htmlHelper.PartialAsync($"{PLUGIN_URL}/Views/Consumer/{partialViewName}.cshtml", model);
            }
        }

        #endregion

        #region Corporate

        public static IHtmlContent CorporateFinancePartial(this IHtmlHelper htmlHelper, string partialViewName)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return htmlHelper.Partial(partialViewName);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Corporate/{partialViewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return htmlHelper.Partial(themeViewName);

                return htmlHelper.Partial($"{PLUGIN_URL}/Views/Corporate/{partialViewName}.cshtml");
            }
        }

        public static IHtmlContent CorporateFinancePartial(this IHtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return htmlHelper.Partial(partialViewName, viewData);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Corporate/{partialViewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return htmlHelper.Partial(themeViewName, viewData);

                return htmlHelper.Partial($"{PLUGIN_URL}/Views/Corporate/{partialViewName}.cshtml", viewData);
            }
        }

        public static IHtmlContent CorporateFinancePartial(this IHtmlHelper htmlHelper, string partialViewName, object model)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return htmlHelper.Partial(partialViewName, model);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Corporate/{partialViewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return htmlHelper.Partial(themeViewName, model);

                return htmlHelper.Partial($"{PLUGIN_URL}/Views/Corporate/{partialViewName}.cshtml", model);
            }
        }

        public async static Task<IHtmlContent> CorporateFinancePartialAsync(this IHtmlHelper htmlHelper, string partialViewName)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return await htmlHelper.PartialAsync(partialViewName);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Corporate/{partialViewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return await htmlHelper.PartialAsync(themeViewName);

                return await htmlHelper.PartialAsync($"{PLUGIN_URL}/Views/Corporate/{partialViewName}.cshtml");
            }
        }

        public static async Task<IHtmlContent> CorporateFinancePartialAsync(this IHtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return await htmlHelper.PartialAsync(partialViewName, viewData);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Corporate/{partialViewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return await htmlHelper.PartialAsync(themeViewName, viewData);

                return await htmlHelper.PartialAsync($"{PLUGIN_URL}/Views/Corporate/{partialViewName}.cshtml", viewData);
            }
        }

        public static async Task<IHtmlContent> CorporateFinancePartialAsync(this IHtmlHelper htmlHelper, string partialViewName, object model)
        {
            if (partialViewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return await htmlHelper.PartialAsync(partialViewName, model);
            }
            else
            {
                partialViewName = partialViewName.Replace(".cshtml", "");

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Corporate/{partialViewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return await htmlHelper.PartialAsync(themeViewName, model);

                return await htmlHelper.PartialAsync($"{PLUGIN_URL}/Views/Corporate/{partialViewName}.cshtml", model);
            }
        }

        #endregion
    }
}