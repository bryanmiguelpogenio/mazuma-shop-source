﻿using Nop.Core.Infrastructure.Mapper;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using Nop.Plugin.Ecorenew.Finance.Models.Common;
using Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup;
using Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup;
using ConsumerSignupModels = Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup;
using CorporateSignupModels = Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup;

namespace Nop.Plugin.Ecorenew.Finance.Extensions
{
    public static class MappingExtensions
    {
        public static TDestination MapTo<TSource, TDestination>(this TSource source)
        {
            return AutoMapperConfiguration.Mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination)
        {
            return AutoMapperConfiguration.Mapper.Map(source, destination);
        }

        #region Common

        #region Address

        public static AddressModel ToModel(this Address entity)
        {
            return entity.MapTo<Address, AddressModel>();
        }

        public static Address ToEntity(this AddressModel model)
        {
            return model.MapTo<AddressModel, Address>();
        }

        public static Address ToEntity(this AddressModel model, Address destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #endregion

        #region Consumer

        #region PersonalInformation

        public static PersonalInformationModel ToModel(this ConsumerFinancePersonalInformation entity)
        {
            return entity.MapTo<ConsumerFinancePersonalInformation, PersonalInformationModel>();
        }

        public static ConsumerFinancePersonalInformation ToEntity(this PersonalInformationModel model)
        {
            return model.MapTo<PersonalInformationModel, ConsumerFinancePersonalInformation>();
        }

        public static ConsumerFinancePersonalInformation ToEntity(this PersonalInformationModel model, ConsumerFinancePersonalInformation destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Home Address

        public static HomeAddressModel ToModel(this ConsumerFinanceHomeAddress entity)
        {
            return entity.MapTo<ConsumerFinanceHomeAddress, HomeAddressModel>();
        }

        public static ConsumerFinanceHomeAddress ToEntity(this HomeAddressModel model)
        {
            return model.MapTo<HomeAddressModel, ConsumerFinanceHomeAddress>();
        }

        public static ConsumerFinanceHomeAddress ToEntity(this HomeAddressModel model, ConsumerFinanceHomeAddress destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Employment

        public static EmploymentModel ToModel(this ConsumerFinanceEmployment entity)
        {
            return entity.MapTo<ConsumerFinanceEmployment, EmploymentModel>();
        }

        public static ConsumerFinanceEmployment ToEntity(this EmploymentModel model)
        {
            return model.MapTo<EmploymentModel, ConsumerFinanceEmployment>();
        }

        public static ConsumerFinanceEmployment ToEntity(this EmploymentModel model, ConsumerFinanceEmployment destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Expenditure Selection Item

        public static IncomeAndExpenditureModel.ExpenditureCategoryRangeModel ToModel(this ConsumerFinanceExpenditureRecord entity)
        {
            return entity.MapTo<ConsumerFinanceExpenditureRecord, IncomeAndExpenditureModel.ExpenditureCategoryRangeModel>();
        }

        public static ConsumerFinanceExpenditureRecord ToEntity(this IncomeAndExpenditureModel.ExpenditureCategoryRangeModel model)
        {
            return model.MapTo<IncomeAndExpenditureModel.ExpenditureCategoryRangeModel, ConsumerFinanceExpenditureRecord>();
        }

        public static ConsumerFinanceExpenditureRecord ToEntity(this IncomeAndExpenditureModel.ExpenditureCategoryRangeModel model, ConsumerFinanceExpenditureRecord destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Income And Expenditure

        public static IncomeAndExpenditureModel ToModel(this ConsumerFinanceIncomeAndExpenditure entity)
        {
            return entity.MapTo<ConsumerFinanceIncomeAndExpenditure, IncomeAndExpenditureModel>();
        }

        public static IncomeAndExpenditureModel ToModel(this ConsumerFinanceIncomeAndExpenditure entity, IncomeAndExpenditureModel destination)
        {
            return entity.MapTo(destination);
        }

        public static ConsumerFinanceIncomeAndExpenditure ToEntity(this IncomeAndExpenditureModel model)
        {
            return model.MapTo<IncomeAndExpenditureModel, ConsumerFinanceIncomeAndExpenditure>();
        }

        public static ConsumerFinanceIncomeAndExpenditure ToEntity(this IncomeAndExpenditureModel model, ConsumerFinanceIncomeAndExpenditure destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Bank

        public static ConsumerSignupModels.BankModel ToModel(this ConsumerFinanceBank entity)
        {
            return entity.MapTo<ConsumerFinanceBank, ConsumerSignupModels.BankModel>();
        }

        public static ConsumerFinanceBank ToEntity(this ConsumerSignupModels.BankModel model)
        {
            return model.MapTo<ConsumerSignupModels.BankModel, ConsumerFinanceBank>();
        }

        public static ConsumerFinanceBank ToEntity(this ConsumerSignupModels.BankModel model, ConsumerFinanceBank destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #endregion

        #region Corporate

        #region BasicDetails

        public static BasicDetailsModel ToModel(this CorporateFinanceBasicDetails entity)
        {
            return entity.MapTo<CorporateFinanceBasicDetails, BasicDetailsModel>();
        }

        public static CorporateFinanceBasicDetails ToEntity(this BasicDetailsModel model)
        {
            return model.MapTo<BasicDetailsModel, CorporateFinanceBasicDetails>();
        }

        public static CorporateFinanceBasicDetails ToEntity(this BasicDetailsModel model, CorporateFinanceBasicDetails destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Director Details

        public static DirectorDetailsModel ToModel(this CorporateFinanceDirectorDetails entity)
        {
            return entity.MapTo<CorporateFinanceDirectorDetails, DirectorDetailsModel>();
        }

        public static CorporateFinanceDirectorDetails ToEntity(this DirectorDetailsModel model)
        {
            return model.MapTo<DirectorDetailsModel, CorporateFinanceDirectorDetails>();
        }

        public static CorporateFinanceDirectorDetails ToEntity(this DirectorDetailsModel model, CorporateFinanceDirectorDetails destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #region Bank

        public static CorporateSignupModels.BankModel ToModel(this CorporateFinanceBankDetails entity)
        {
            return entity.MapTo<CorporateFinanceBankDetails, CorporateSignupModels.BankModel>();
        }

        public static CorporateFinanceBankDetails ToEntity(this CorporateSignupModels.BankModel model)
        {
            return model.MapTo<CorporateSignupModels.BankModel, CorporateFinanceBankDetails>();
        }

        public static CorporateFinanceBankDetails ToEntity(this CorporateSignupModels.BankModel model, CorporateFinanceBankDetails destination)
        {
            return model.MapTo(destination);
        }

        #endregion

        #endregion
    }
}