﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents an available range for selection for the expenditure category
    /// </summary>
    public class ExpenditureCategoryRange : BaseEntity
    {
        /// <summary>
        /// Gets or sets the identifier of the expenditure category
        /// </summary>
        public int ExpenditureCategoryId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the expenditure range
        /// </summary>
        public int ExpenditureRangeId { get; set; }

        /// <summary>
        /// Gets or sets the expenditure category
        /// </summary>
        public virtual ExpenditureCategory ExpenditureCategory { get; set; }

        /// <summary>
        /// Gets or sets the expenditure range
        /// </summary>
        public virtual ExpenditureRange ExpenditureRange { get; set; }
    }
}