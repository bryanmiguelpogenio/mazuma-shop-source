﻿using System;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents the employment details of the consumer finance signup record
    /// </summary>
    [Serializable]
    public class ConsumerFinanceEmployment
    {
        /// <summary>
        /// Gets or sets the employment status identifier
        /// </summary>
        public int EmploymentStatusId { get; set; }

        /// <summary>
        /// Gets or sets the annual gross income identifier
        /// </summary>
        public int AnnualGrossIncomeId { get; set; }

        /// <summary>
        /// Gets or sets the employer name if employed
        /// </summary>
        public string EmployedEmployerName { get; set; }

        /// <summary>
        /// Gets or sets the postcode if employed
        /// </summary>
        public string EmployedPostCode { get; set; }

        /// <summary>
        /// Gets or sets the number of years employed
        /// </summary>
        public int? EmployedYearsEmployed { get; set; }

        /// <summary>
        /// Gets or set the number of months employed
        /// </summary>
        public int? EmployedMonthsEmployed { get; set; }

        /// <summary>
        /// Gets or sets the job title if employed
        /// </summary>
        public string EmployedJobTitle { get; set; }

        /// <summary>
        /// Gets or sets the self employed trading as
        /// </summary>
        public string SelfEmployedTradingAs { get; set; }

        /// <summary>
        /// Gets or sets the nature of business if self employed
        /// </summary>
        public string SelfEmployedNatureOfBusiness { get; set; }

        /// <summary>
        /// Gets or sets the self employed years
        /// </summary>
        public int? SelfEmployedYearsLength { get; set; }

        /// <summary>
        /// Gets or sets the self employed months
        /// </summary>
        public int? SelfEmployedMonthsLength { get; set; }

        /// <summary>
        /// Gets or sets the post code if self employed
        /// </summary>
        public string SelfEmployedPostCode { get; set; }

        /// <summary>
        /// Gets or sets the years if other employment status
        /// </summary>
        public int? OtherYearsLength { get; set; }

        /// <summary>
        /// Gets or sets the months if other employment status
        /// </summary>
        public int? OtherMonthsLength { get; set; }
    }
}