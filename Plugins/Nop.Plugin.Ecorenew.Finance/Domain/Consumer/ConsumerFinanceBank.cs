﻿using System;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represets the bank details of the consumer finance signup record
    /// </summary>
    [Serializable]
    public class ConsumerFinanceBank
    {
        /// <summary>
        /// Gets or sets the sortcode
        /// </summary>
        public string SortCode { get; set; }

        /// <summary>
        /// Gets or sets the account number
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// Gets or sets the account holder's name
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// Gets or sets how many years does the user had the account
        /// </summary>
        public int AccountAgeYears { get; set; }

        /// <summary>
        /// Gets or sets how many months does the user had the account
        /// </summary>
        public int AccountAgeMonths { get; set; }
    }
}