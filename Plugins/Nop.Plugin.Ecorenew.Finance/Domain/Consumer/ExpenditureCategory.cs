﻿using Nop.Core;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents an expenditure category
    /// </summary>
    public class ExpenditureCategory : BaseEntity
    {
        /// <summary>
        /// Gets or sets the locale string resource name
        /// </summary>
        public string LocaleStringResourceName { get; set; }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the entity is active
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the display order number
        /// </summary>
        public int DisplayOrderNumber { get; set; }

        /// <summary>
        /// Gets or sets the available expenditure ranges
        /// </summary>
        public virtual ICollection<ExpenditureCategoryRange> AvailableRanges { get; set; }
    }
}