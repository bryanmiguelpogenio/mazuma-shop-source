﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents an annual gross income
    /// </summary>
    public class AnnualGrossIncome : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value in pounds
        /// </summary>
        public decimal ValueInGbp { get; set; }
    }
}