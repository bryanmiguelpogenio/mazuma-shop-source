﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents a finance option
    /// </summary>
    public class ConsumerFinanceOption : BaseEntity
    {
        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the duration in months
        /// </summary>
        public int DurationInMonths { get; set; }

        /// <summary>
        /// Gets or sets the agreement rate
        /// </summary>
        public decimal AgreementRate { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the consumer finance option is active
        /// </summary>
        public bool IsActive { get; set; }
    }
}