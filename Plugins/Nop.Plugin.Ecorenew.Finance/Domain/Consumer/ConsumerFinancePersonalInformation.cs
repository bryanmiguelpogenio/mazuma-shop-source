﻿using System;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents the personal information of the consumer finance signup record
    /// </summary>
    [Serializable]
    public class ConsumerFinancePersonalInformation
    {
        /// <summary>
        /// Gets or sets the title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the middle name
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the marital status identifier
        /// </summary>
        public int MaritalStatusId { get; set; }

        /// <summary>
        /// Gets or sets the number of dependants identifier
        /// </summary>
        public int NumberOfDependentsId { get; set; }

        /// <summary>
        /// Gets or sets the date of birth
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the mobile phone number
        /// </summary>
        public string MobilePhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the password
        /// </summary>
        public string Password { get; set; }
    }
}