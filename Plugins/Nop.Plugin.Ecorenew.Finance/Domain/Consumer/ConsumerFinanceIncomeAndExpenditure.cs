﻿using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents the income and expenditure record of a consumer finance signup record
    /// </summary>
    [Serializable]
    public class ConsumerFinanceIncomeAndExpenditure
    {
        /// <summary>
        /// Gets or sets the monthly income amount after tax
        /// </summary>
        public decimal MonthlyIncomeAmountAfterTax { get; set; }

        /// <summary>
        /// Gets or sets the consumer expenditures
        /// </summary>
        public List<ConsumerFinanceExpenditureRecord> ConsumerExpenditures { get; set; }
    }
}