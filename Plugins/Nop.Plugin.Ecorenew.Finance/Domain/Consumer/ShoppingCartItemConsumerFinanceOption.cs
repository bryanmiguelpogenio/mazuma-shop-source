﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents the consumer finance option selected for the shopping cart item
    /// </summary>
    public class ShoppingCartItemConsumerFinanceOption : BaseEntity
    {
        /// <summary>
        /// Gets or sets the shopping cart item identifier
        /// </summary>
        public int ShoppingCartItemId { get; set; }

        /// <summary>
        /// Gets or sets the consumer finance option identifier
        /// </summary>
        public int ConsumerFinanceOptionId { get; set; }

        /// <summary>
        /// Gets or sets the consumer finance option
        /// </summary>
        public virtual ConsumerFinanceOption ConsumerFinanceOption { get; set; }
    }
}