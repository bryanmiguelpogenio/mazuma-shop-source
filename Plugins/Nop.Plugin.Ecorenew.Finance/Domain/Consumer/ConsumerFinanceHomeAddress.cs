﻿using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using System;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents the home address of the consumer finance signup record
    /// </summary>
    [Serializable]
    public class ConsumerFinanceHomeAddress
    {
        /// <summary>
        /// Gets or sets the home owner status identifier
        /// </summary>
        public int HomeOwnerStatusId { get; set; }

        /// <summary>
        /// Gets or sets the value whether the consumer's residence is in UK
        /// </summary>
        public bool IsUkResident { get; set; }

        /// <summary>
        /// Gets or sets the current address details
        /// </summary>
        public Address CurrentAddress { get; set; }

        /// <summary>
        /// Gets or sets the previous address details
        /// </summary>
        public Address PreviousAddress { get; set; }
    }
}