﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents an employment status
    /// </summary>
    public class EmploymentStatus : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }
    }
}