﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents an order item on financing
    /// </summary>
    public class ConsumerFinanceOrderItem : BaseEntity
    {
        /// <summary>
        /// Gets or sets the consumer finance signup identifier
        /// </summary>
        public int ConsumerFinanceSignupId { get; set; }

        /// <summary>
        /// Gets or sets the shopping cart item identifier
        /// </summary>
        public int? ShoppingCartItemId { get; set; }

        /// <summary>
        /// Gets or sets the order item identifier
        /// </summary>
        public int? OrderItemId { get; set; }

        /// <summary>
        /// Gets or sets the duration of agreement
        /// </summary>
        public int DurationOfAgreement { get; set; }

        /// <summary>
        /// Gets or sets the product identifier
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the product attributes XML
        /// </summary>
        public string ProductAttributesXml { get; set; }

        /// <summary>
        /// Gets or sets the product attributes desctription
        /// </summary>
        public string ProductAttributeDescription { get; set; }

        /// <summary>
        /// Gets or sets the SKU of the product
        /// </summary>
        public string ProductSku { get; set; }

        /// <summary>
        /// Gets or sets the currency code
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Gets or sets the product price (including TAX)
        /// </summary>
        public decimal ProductPriceInclTax { get; set; }

        /// <summary>
        /// Gets or sets the quantity of the product
        /// </summary>
        public int ProductQuantity { get; set; }

        /// <summary>
        /// Gets or sets the tax rate
        /// </summary>
        public decimal TaxRate { get; set; }

        /// <summary>
        /// Gets or sets the initial payment amount
        /// </summary>
        public decimal InitialPayment { get; set; }

        /// <summary>
        /// Gets or sets the monthly payment amount
        /// </summary>
        public decimal MonthlyPayment { get; set; }

        /// <summary>
        /// Gets or sets the documentation fee
        /// </summary>
        public decimal DocumentationFee { get; set; }

        /// <summary>
        /// Gets or sets the agreement rate
        /// </summary>
        public decimal AgreementRate { get; set; }

        /// <summary>
        /// Gets or sets the total credit / finance changes
        /// </summary>
        public decimal TotalCreditOrFinanceCharges { get; set; }

        /// <summary>
        /// Gets or sets the total payable
        /// </summary>
        public decimal TotalPayable { get; set; }

        /// <summary>
        /// Gets or sets the APR
        /// </summary>
        public decimal Apr { get; set; }

        // TOOD: Date start of payment

        /// <summary>
        /// Gets or sets the identifier of the consumer finance CAAP record
        /// </summary>
        public int? ConsumerFinanceCaapRecordId { get; set; }

        /// <summary>
        /// Gets or sets the consumer finance signup
        /// </summary>
        public virtual ConsumerFinanceSignup ConsumerFinanceSignup { get; set; }

        /// <summary>
        /// Gets or sets the consumer finance CAAP record
        /// </summary>
        public virtual ConsumerFinanceCaapRecord ConsumerFinanceCaapRecord { get; set; }
    }
}