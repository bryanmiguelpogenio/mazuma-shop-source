﻿using System;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents an expenditure record of the consumer finance signup record
    /// </summary>
    [Serializable]
    public class ConsumerFinanceExpenditureRecord
    {
        /// <summary>
        /// Gets or sets the expentiture category identifier
        /// </summary>
        public int ExpenditureCategoryId { get; set; }

        /// <summary>
        /// Gets or sets the expenditure range identifier
        /// </summary>
        public int ExpenditureRangeId { get; set; }

        /// <summary>
        /// Gets or sets the specific value for ranges with greater than the max value available for selection
        /// </summary>
        public decimal? ConsumerExpenditureSpecificValueInGbp { get; set; }
    }
}