﻿using Nop.Core;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents a CAAP record
    /// </summary>
    public class ConsumerFinanceCaapRecord : BaseEntity
    {
        private ICollection<ConsumerFinanceOrderItem> _orderItems;

        /// <summary>
        /// Gets or sets the date and time when the entity has been created
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the records of items associated with the CAAP record
        /// </summary>
        public virtual ICollection<ConsumerFinanceOrderItem> OrderItems
        {
            get { return _orderItems ?? (_orderItems = new List<ConsumerFinanceOrderItem>()); }
            protected set { _orderItems = value; }
        }
    }
}