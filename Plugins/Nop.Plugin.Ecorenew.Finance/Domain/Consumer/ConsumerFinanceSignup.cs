﻿using Nop.Core;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents a consumer finance signup record
    /// </summary>
    public class ConsumerFinanceSignup : BaseEntity
    {
        private ICollection<ConsumerFinanceOrderItem> _orderItems;

        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the store identifier
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the date and time the entity is created
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the IP address of the consumer
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets or sets the consumer's personal information (Encrypted)
        /// </summary>
        public string ConsumerFinancePersonalInformation { get; set; }

        /// <summary>
        /// Gets or sets the consumer's home address (Encrypted)
        /// </summary>
        public string ConsumerFinanceHomeAddress { get; set; }

        /// <summary>
        /// Gets or sets the consumer's employment details (Encrypted)
        /// </summary>
        public string ConsumerFinanceEmployment { get; set; }

        /// <summary>
        /// Gets or sets the consumer's bank details (Encrypted)
        /// </summary>
        public string ConsumerFinanceBank { get; set; }

        /// <summary>
        /// Gets or sets the consumer's income and expenditure details (Encrypted)
        /// </summary>
        public string ConsumerFinanceIncomeAndExpenditure { get; set; }

        /// <summary>
        /// Gets or sets the value whether or not the consumer has passed the affordability check
        /// </summary>
        public bool? AffordabilityCheckPassed { get; set; }

        /// <summary>
        /// Gets or sets the value whether or not the consumer has passed the CallValidate check
        /// </summary>
        public bool? CallValidatePassed { get; set; }

        /// <summary>
        /// Gets or sets the value whether or not the consumer has passed the CallReport check
        /// </summary>
        public bool? CallReportPassed { get; set; }

        /// <summary>
        /// Gets or sets the CallValidate result identifier (From CallCredit)
        /// </summary>
        public int? CallValidateResultId { get; set; }

        /// <summary>
        /// Gets or sets the CallValidate result identifier (From CallCredit)
        /// </summary>
        public int? CallReportResultId { get; set; }

        /// <summary>
        /// Gets or sets the DocuSign envelope identifier (From DocuSign)
        /// </summary>
        public string DocuSignEnvelopeId { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the customer completed the signup process
        /// </summary>
        public DateTime? SignupCompletedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the order identifier
        /// </summary>
        public int? OrderId { get; set; }

        /// <summary>
        /// Gets the value whether all the checks performed are passed
        /// </summary>
        public virtual bool AllChecksPassed
        {
            get { return (AffordabilityCheckPassed ?? false) && (CallReportPassed ?? false) && (CallValidatePassed ?? false); }
        }

        /// <summary>
        /// Gets or sets the order items for financing
        /// </summary>
        public virtual ICollection<ConsumerFinanceOrderItem> OrderItems
        {
            get { return _orderItems ?? (_orderItems = new List<ConsumerFinanceOrderItem>()); }
            protected set { _orderItems = value; }
        }
    }
}