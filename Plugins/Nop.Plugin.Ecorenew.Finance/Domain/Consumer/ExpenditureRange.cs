﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    /// <summary>
    /// Represents an expediture range
    /// </summary>
    public class ExpenditureRange : BaseEntity
    {
        /// <summary>
        /// Gets or sets the range description
        /// </summary>
        public string RangeDescription { get; set; }

        /// <summary>
        /// Gets or sets the mid point value
        /// </summary>
        public decimal MidPointValue { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the entity is active
        /// </summary>
        public bool IsActive { get; set; }
    }
}