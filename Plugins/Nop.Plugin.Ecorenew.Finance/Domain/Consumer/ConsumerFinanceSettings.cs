﻿using Nop.Core.Configuration;
using System;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Consumer
{
    public class ConsumerFinanceSettings : ISettings
    {
        /// <summary>
        /// Gets or sets the company to be provided to CallCredit's CallValidate API
        /// </summary>
        public string CallValidateCompany { get; set; }

        /// <summary>
        /// Gets or sets the username to be provided to CallCredit's CallValidate API
        /// </summary>
        public string CallValidateUsername { get; set; }

        /// <summary>
        /// Gets or sets the password to be provided to CallCredit's CallValidate API
        /// </summary>
        public string CallValidatePassword { get; set; }

        /// <summary>
        /// Gets or sets the application name to be provided to CallCredit's CallValidate API
        /// </summary>
        public string CallValidateApplication { get; set; }

        /// <summary>
        /// Gets or sets the URL for CallCredit's CallValidate Test API
        /// </summary>
        public string CallValidateTestApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the URL for CallCredit's CallValidate Live API
        /// </summary>
        public string CallValidateLiveApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether CallValidate Live API will be used or the CallValidate Test API
        /// </summary>
        public bool UseCallValidateLiveApi { get; set; }

        /// <summary>
        /// Gets or sets the company to be provided to CallCredit's CallReport API
        /// </summary>
        public string CallReportCompany { get; set; }

        /// <summary>
        /// Gets or sets the username to be provided to CallCredit's CallReport API
        /// </summary>
        public string CallReportUsername { get; set; }

        /// <summary>
        /// Gets or sets the password to be provided to CallCredit's CallReport API
        /// </summary>
        public string CallReportPassword { get; set; }

        /// <summary>
        /// Gets or sets the passing score for CallCredit's CallReport result
        /// </summary>
        public int CallReportPassingScore { get; set; }

        /// <summary>
        /// Gets or sets the URL for CallCredit's CallReport Test API
        /// </summary>
        public string CallReportTestApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the URL for CallCredit's CallReport Live API
        /// </summary>
        public string CallReportLiveApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether CallReport Live API will be used or the CallReport Test API
        /// </summary>
        public bool UseCallReportLiveApi { get; set; }

        /// <summary>
        /// Gets or sets the date and time (UTC) when the password for CallReport API was last changed
        /// </summary>
        public DateTime CallReportLastPasswordChangeDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the documentation fee (GBP)
        /// </summary>
        public decimal DocumentationFeeGbp { get; set; }

        /// <summary>
        /// Gets or sets the option to purchase fee (GBP)
        /// </summary>
        public decimal OptionToPurchaseFeeGbp { get; set; }

        /// <summary>
        /// Gets or sets the percentage rate or interest per annum
        /// </summary>
        public decimal PercentageRateOfInterestPerAnnum { get; set; }

        /// <summary>
        /// Gets or sets the APR percentage
        /// </summary>
        public decimal AprPercentage { get; set; }

        /// <summary>
        /// Gets or sets the initial payment (GBP) for products within minimum price range (GBP)
        /// </summary>
        public decimal InitialPaymentGbpForProductPriceGbp0 { get; set; }

        /// <summary>
        /// Gets or sets the initial payment (GBP) for products greater than or equal to 150 GBP
        /// </summary>
        public decimal InitialPaymentGbpForProductPriceGbp150 { get; set; }

        /// <summary>
        /// Gets or sets the initial payment (GBP) for products greater than or equal to 200 GBP
        /// </summary>
        public decimal InitialPaymentGbpForProductPriceGbp200 { get; set; }

        /// <summary>
        /// Gets or sets the initial payment (GBP) for products greater than or equal to 250 GBP
        /// </summary>
        public decimal InitialPaymentGbpForProductPriceGbp250 { get; set; }

        /// <summary>
        /// Gets or sets the initial payment (GBP) for products greater than or equal to 300 GBP
        /// </summary>
        public decimal InitialPaymentGbpForProductPriceGbp300 { get; set; }

        /// <summary>
        /// Gets or sets the initial payment (GBP) for products greater than or equal to 350 GBP
        /// </summary>
        public decimal InitialPaymentGbpForProductPriceGbp350 { get; set; }

        /// <summary>
        /// Gets or sets the initial payment (GBP) for products greater than or equal to 400 GBP
        /// </summary>
        public decimal InitialPaymentGbpForProductPriceGbp400 { get; set; }

        /// <summary>
        /// Gets or sets the initial payment (GBP) for products greater than or equal to 600 GBP
        /// </summary>
        public decimal InitialPaymentGbpForProductPriceGbp600 { get; set; }

        /// <summary>
        /// Gets or sets the initial payment (GBP) for products greater than or equal to 900 GBP
        /// </summary>
        public decimal InitialPaymentGbpForProductPriceGbp900 { get; set; }

        /// <summary>
        /// Gets or sets the path to the consumer finance document templates
        /// </summary>
        public string FinanceDocumentTemplatesPath { get; set; }

        /// <summary>
        /// Gets or sets the supplier company address
        /// </summary>
        public string SupplierCompanyAddress { get; set; }
    }
}