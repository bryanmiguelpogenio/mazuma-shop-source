﻿namespace Nop.Plugin.Ecorenew.Finance.Domain.Corporate
{
    /// <summary>
    /// Represents the bank details of the corporate company or group
    /// </summary>
    public class CorporateFinanceBankDetails
    {
        /// <summary>
        /// Gets or sets the sort code
        /// </summary>
        public string SortCode { get; set; }

        /// <summary>
        /// Gets or sets the account number
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// Gets or sets the account holder's name
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// Gets or sets the company name
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the trading name
        /// </summary>
        public string TradingName { get; set; }

        /// <summary>
        /// Gets or sets the number of years from the age of the bank account
        /// </summary>
        public int AccountAgeYears { get; set; }

        /// <summary>
        /// Gets or sets the number of months from the age of the bank account
        /// </summary>
        public int AccountAgeMonths { get; set; }
    }
}