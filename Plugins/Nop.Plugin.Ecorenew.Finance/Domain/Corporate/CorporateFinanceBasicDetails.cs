﻿using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using System;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Corporate
{
    /// <summary>
    /// Represents the basic information of the corporate company or group
    /// </summary>
    [Serializable]
    public class CorporateFinanceBasicDetails
    {
        /// <summary>
        /// Gets or sets the company name
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the trading name
        /// </summary>
        public string TradingName { get; set; }

        /// <summary>
        /// Gets or sets the registration number
        /// </summary>
        public string CompanyRegistrationNumber { get; set; }

        /// <summary>
        /// Gets or sets the VAT number
        /// </summary>
        public string VatNumber { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the corporate company or group has been established
        /// </summary>
        public DateTime DateEstablished { get; set; }

        /// <summary>
        /// Gets or sets the industry sector
        /// </summary>
        public string IndustrySector { get; set; }

        /// <summary>
        /// Gets or sets the company address
        /// </summary>
        public Address CompanyAddress { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the trading address is the same as the company address
        /// </summary>
        public bool TradingAddressSameAsCompanyAddress { get; set; }

        /// <summary>
        /// Gets or sets the trading address
        /// </summary>
        public Address TradingAddress { get; set; }
    }
}