﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Corporate
{
    /// <summary>
    /// Corporate finance settings
    /// </summary>
    public class CorporateFinanceSettings : ISettings
    {
        /// <summary>
        /// Gets the interest rate (%)
        /// </summary>
        public decimal InterestRate { get; set; }

        /// <summary>
        /// Gets the (ICT) processing cost in GBP
        /// </summary>
        public decimal ProcessingCostInGbp { get; set; }

        /// <summary>
        /// Gets or sets the path to the consumer finance document templates
        /// </summary>
        public string FinanceDocumentTemplatesPath { get; set; }
    }
}