﻿using Nop.Core;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Corporate
{
    /// <summary>
    /// Represents the corporate finance signup record
    /// </summary>
    public class CorporateFinanceSignup : BaseEntity
    {
        private ICollection<CorporateFinanceOrderItem> _orderItems;

        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the store identifier
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the basic details (encrypted) of the corporate company or group
        /// </summary>
        public string CorporateFinanceBasicDetails { get; set; }

        /// <summary>
        /// Gets or sets the details (encrypted) of the director of the corporate company or group
        /// </summary>
        public string CorporateFinanceDirectorDetails { get; set; }

        /// <summary>
        /// Gets or sets the bank details (encrypted) of the corporate corporate company or group
        /// </summary>
        public string CorporateFinanceBankDetails { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the signup record was created
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the signup record was last updated by the customer
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the signup process has been completed
        /// </summary>
        public DateTime? SignupCompletedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the application is approved.
        /// Null means that the approval process has yet to be done.
        /// </summary>
        public bool? IsApproved { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the customer who conducted the approval process
        /// </summary>
        public int? ApprovalProcessConductedByCustomerId { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the approval process was done
        /// </summary>
        public DateTime? DateOfApprovalUtc { get; set; }

        /// <summary>
        /// Gets or sets the DocuSign envelope identifier
        /// </summary>
        public string DocuSignEnvelopeId { get; set; }

        /// <summary>
        /// Gets or sets the order identifier
        /// </summary>
        public int? OrderId { get; set; }

        /// <summary>
        /// Gets or sets the order items for financing
        /// </summary>
        public virtual ICollection<CorporateFinanceOrderItem> OrderItems
        {
            get { return _orderItems ?? (_orderItems = new List<CorporateFinanceOrderItem>()); }
            protected set { _orderItems = value; }
        }
    }
}