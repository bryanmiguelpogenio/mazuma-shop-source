﻿using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using System;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Corporate
{
    /// <summary>
    /// Gets or sets the details of the director of the corporate company or group
    /// </summary>
    [Serializable]
    public class CorporateFinanceDirectorDetails
    {
        /// <summary>
        /// Gets or sets the title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the middle name
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the former name
        /// </summary>
        public string FormerName { get; set; }

        /// <summary>
        /// Gets or sets the date of birth
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether he/she is a permanent UK resident
        /// </summary>
        public bool IsUkResident { get; set; }

        /// <summary>
        /// Gets or sets the marital status identifier
        /// </summary>
        public int MaritalStatusId { get; set; }

        /// <summary>
        /// Gets or sets the number of dependents identifier
        /// </summary>
        public int NumberOfDependentsId { get; set; }

        /// <summary>
        /// Gets or sets the home owner status identifier
        /// </summary>
        public int HomeOwnerStatusId { get; set; }

        /// <summary>
        /// Gets or sets the email address
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the company telephone number
        /// </summary>
        public string CompanyTelephoneNumber { get; set; }

        /// <summary>
        /// Gets or sets th home number
        /// </summary>
        public string HomeNumber { get; set; }

        /// <summary>
        /// Gets or sets the mobile number
        /// </summary>
        public string MobileNumber { get; set; }

        /// <summary>
        /// Gets or sets the current address
        /// </summary>
        public Address CurrentAddress { get; set; }

        /// <summary>
        /// Gets or sets the previous address
        /// </summary>
        public Address PreviousAddress { get; set; }

        /// <summary>
        /// Gets or sets the password
        /// </summary>
        public string Password { get; set; }
    }
}