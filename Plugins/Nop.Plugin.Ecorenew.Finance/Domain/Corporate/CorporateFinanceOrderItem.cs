﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Corporate
{
    /// <summary>
    /// Represents an order item on financing
    /// </summary>
    public class CorporateFinanceOrderItem : BaseEntity
    {
        /// <summary>
        /// Gets or sets the corporate finance signup identifier
        /// </summary>
        public int CorporateFinanceSignupId { get; set; }

        /// <summary>
        /// Gets or sets the shopping cart item identifier
        /// </summary>
        public int? ShoppingCartItemId { get; set; }

        /// <summary>
        /// Gets or sets the order item identifier
        /// </summary>
        public int? OrderItemId { get; set; }

        /// <summary>
        /// Gets or sets the duration of agreement
        /// </summary>
        public int DurationOfAgreement { get; set; }

        /// <summary>
        /// Gets or sets the product identifier
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the product attributes XML
        /// </summary>
        public string ProductAttributesXml { get; set; }

        /// <summary>
        /// Gets or sets the product attributes desctription
        /// </summary>
        public string ProductAttributeDescription { get; set; }

        /// <summary>
        /// Gets or sets the SKU of the product
        /// </summary>
        public string ProductSku { get; set; }

        /// <summary>
        /// Gets or sets the currency code
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Gets or sets the product price (including TAX)
        /// </summary>
        public decimal ProductPriceInclTax { get; set; }

        /// <summary>
        /// Gets or sets the quantity of the product
        /// </summary>
        public int ProductQuantity { get; set; }

        /// <summary>
        /// Gets or sets the tax rate
        /// </summary>
        public decimal TaxRate { get; set; }

        /// <summary>
        /// Gets or sets the monthly payment amount
        /// </summary>
        public decimal MonthlyPayment { get; set; }

        /// <summary>
        /// Gets or sets the interest rate
        /// </summary>
        public decimal InterestRate { get; set; }

        /// <summary>
        /// Gets or sets the interest charges
        /// </summary>
        public decimal InterestCharges { get; set; }

        /// <summary>
        /// Gets or sets the total payable
        /// </summary>
        public decimal TotalPayable { get; set; }

        /// <summary>
        /// Gets or sets the RV
        /// </summary>
        public decimal Rv { get; set; }

        /// <summary>
        /// Gets or sets the processing cost
        /// </summary>
        public decimal ProcessingCost { get; set; }

        /// <summary>
        /// Gets or sets the Net RV
        /// </summary>
        public decimal NetRv { get; set; }

        /// <summary>
        /// Gets or sets the corporate finance signup
        /// </summary>
        public virtual CorporateFinanceSignup CorporateFinanceSignup { get; set; }
    }
}