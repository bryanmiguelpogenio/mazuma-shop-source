﻿namespace Nop.Plugin.Ecorenew.Finance.Domain.Corporate
{
    public enum CorporateFinanceSignupStatus
    {
        Unknown = 1,
        Approved = 2,
        Disapproved = 3
    }
}