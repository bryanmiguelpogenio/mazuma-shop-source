﻿namespace Nop.Plugin.Ecorenew.Finance.Domain.Corporate
{
    /// <summary>
    /// Represents message template system names for corporate finance
    /// </summary>
    public static class CorporateMessageTemplateSystemNames
    {
        /// <summary>
        /// Represents system name of notification about approved corporate finance registration
        /// </summary>
        public const string CorporateSignupApporvedCustomerNotification = "Ecorenew.Finance.Corporate.Approved.CustomerNotification";
    }
}