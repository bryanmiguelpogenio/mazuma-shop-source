﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Corporate
{
    public class SkuCorporateFinanceRv : BaseEntity
    {
        /// <summary>
        /// Gets or sets the SKU
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Gets or sets the RV in GBP
        /// </summary>
        public decimal RvInGbp { get; set; }
    }
}