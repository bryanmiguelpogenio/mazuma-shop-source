﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Corporate
{
    /// <summary>
    /// Represents the corporate finance option selected for the shopping cart item
    /// </summary>
    public class ShoppingCartItemCorporateFinanceOption : BaseEntity
    {
        /// <summary>
        /// Gets or sets the shopping cart item identifier
        /// </summary>
        public int ShoppingCartItemId { get; set; }

        /// <summary>
        /// Gets or sets the corporate finance option identifier
        /// </summary>
        public int CorporateFinanceOptionId { get; set; }

        /// <summary>
        /// Gets or sets the corporate finance option
        /// </summary>
        public virtual CorporateFinanceOption CorporateFinanceOption { get; set; }
    }
}