﻿using System;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Common
{
    [Serializable]
    public class Address
    {
        /// <summary>
        /// Gets or sets the house number
        /// </summary>
        public string HouseNumber { get; set; }

        /// <summary>
        /// Gets or sets the building number
        /// </summary>
        public string BuildingNumber { get; set; }

        /// <summary>
        /// Gets or sets the address 1
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the address 2
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the address 3
        /// </summary>
        public string Address3 { get; set; }

        /// <summary>
        /// Gets or sets the city / town
        /// </summary>
        public string CityTown { get; set; }

        /// <summary>
        /// Gets or sets the country identifier
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// Gets or sets the state / province / county identifier
        /// </summary>
        public int? StateProvinceId { get; set; }

        /// <summary>
        /// Gets or sets the postcode
        /// </summary>
        public string Postcode { get; set; }

        /// <summary>
        /// Gets or sets the length of stay (years)
        /// </summary>
        public int LengthOfStayYears { get; set; }

        /// <summary>
        /// Gets or sets the length of stay (months)
        /// </summary>
        public int LengthOfStayMonths { get; set; }
    }
}