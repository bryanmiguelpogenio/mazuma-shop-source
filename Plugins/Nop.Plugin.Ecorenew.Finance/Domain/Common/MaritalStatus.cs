﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Common
{
    /// <summary>
    /// Represents a marital status
    /// </summary>
    public class MaritalStatus : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }
    }
}