﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Common
{
    public class FinanceSettings : ISettings
    {
        public FinanceType FinanceType { get; set; }
    }
}