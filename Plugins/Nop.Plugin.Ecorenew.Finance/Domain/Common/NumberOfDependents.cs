﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Common
{
    /// <summary>
    /// Represents a number of dependants option
    /// </summary>
    public class NumberOfDependents : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }
    }
}