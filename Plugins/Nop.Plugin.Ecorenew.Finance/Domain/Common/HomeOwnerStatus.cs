﻿using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Domain.Common
{
    /// <summary>
    /// Represents a home owner status option
    /// </summary>
    public class HomeOwnerStatus : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }
    }
}