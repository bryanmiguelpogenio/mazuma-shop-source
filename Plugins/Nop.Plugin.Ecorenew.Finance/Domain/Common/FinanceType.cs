﻿namespace Nop.Plugin.Ecorenew.Finance.Domain.Common
{
    public enum FinanceType
    {
        Consumer = 1,
        Corporate = 2
    }
}