﻿$(document).ready(function () {
	var financeBreakdownXhr = null;

	function reloadFinanceValues() {
		$('#corporatefinance_productdetails_aslowas').hide();

		var productId = $('#corporatefinance_productdetails_productid').val();
		var financeBreakdownUrl = "/ProductDetailsCorporateFinanceBreakdown/GetFinanceBreakdown?productId=" + productId;

		if (financeBreakdownXhr != null) {
			financeBreakdownXhr.abort();
			financeBreakdownXhr = null
		}

		var modalContent = $('#corporatefinance_breakdownpopup .corporatefinance-modal-content .terms');
		modalContent.html('');

		financeBreakdownXhr = $.ajax({
			cache: false,
			url: financeBreakdownUrl,
			data: $('#product-details-form').serialize(),
			type: 'post',
			success: function (data) {
				$('#aslowas_price').text(data.LowestMonthlyPayment);
				$('#aslowas_month').text(data.LowestMonthlyPaymentMonths + " months");

				$('#finance-monthly-deposit').text(data.LowestMonthlyPaymentMonths);
				$('#finance-months').text(data.InitialPayment);
				$('#finance-monthly-payment').text(data.LowestMonthlyPayment);
				$('#finance-total-payable').text(data.LowestMonthlyPaymentTotalAmountPayable);
				$('.finance-apr').text(data.Apr);
				$('.finance-option-to-purchase-fee').text(data.OptionToPurchaseFee)

				for (var i = 0; i < data.FinanceValues.TermViews.length; i++) {
					modalContent.html(modalContent.html() + data.FinanceValues.TermViews[i]);
				}

				$('#corporatefinance_productdetails_aslowas').show();
			}
		});
	}

	reloadFinanceValues();

	$(".product-price").on('DOMSubtreeModified', function () {
		reloadFinanceValues();
	});

	function showBreakdownModal() {
		modal = document.getElementById('corporatefinance_breakdownpopup');
		modal.style.display = 'block';
	}

	function hideBreakdownModal() {
		modal = document.getElementById('corporatefinance_breakdownpopup');
		modal.style.display = "none";
	}

	$("#corporatefinance_productdetails_comparefinanceoptions").click(function () {
		showBreakdownModal();
	});

	$('.finance-corporate-compare-payment-options').click(function () {
		showBreakdownModal();
	});

	window.onclick = function (event) {
		modal = document.getElementById('corporatefinance_breakdownpopup');
		if (event.target == modal) {
			hideBreakdownModal();
		}
	}

	$('.corporatefinance-modal-close').click(function () {
		hideBreakdownModal();
	});
});