﻿$(document).ready(function () {
	$('#corporatefinance_signup_basicdetails_companyaddress .corporatefinance-signup-basicdetails-country').change(function () {
		countryIdChanged(this, '#corporatefinance_signup_basicdetails_companyaddress');
	});

	$('#corporatefinance_signup_basicdetails_tradingaddress .corporatefinance-signup-basicdetails-country').change(function () {
		countryIdChanged(this, '#corporatefinance_signup_basicdetails_tradingaddress');
	});

	function countryIdChanged(control, parentSelector) {
		var selectedCountryId = $(control).val();

		$.ajax({
			type: 'POST',
			url: '/ConsumerFinanceSignup/GetStateProvinces',
			dataType: 'json',
			data: {
				countryId: selectedCountryId
			},
			success: function (data) {
				setStateProvince(parentSelector + " .corporatefinance-signup-basicdetails-stateprovince", data);
			}
		});
	}

	function setStateProvince(control, data) {
		var select = $(control);

		select.empty();

		for (var i = 0; i < data.length; i++) {
			select.append('<option value="' + data[i].Id + '">' + data[i].Name + "</option>");
		}

		select.val("");
	}

	function evalTradingAddressDisplay() {
		if ($('#corporatefinance_signup_basicdetails_tradingaddress_sameas_companyaddress').prop('checked')) {
			$('#corporatefinance_signup_basicdetails_tradingaddress').hide();
		} else {
			$('#corporatefinance_signup_basicdetails_tradingaddress').show();
		}
	}

	// run on load
	evalTradingAddressDisplay();

	$('#corporatefinance_signup_basicdetails_tradingaddress_sameas_companyaddress').change(function () {
		evalTradingAddressDisplay();
	});
});