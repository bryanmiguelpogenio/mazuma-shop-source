﻿$(document).ready(function () {
	$('[data-toggle="popover"]').popover();

	$("#corporatefinance_signup_directordetails_mobile").mask("+00 000 000 0000");

	$('#corporatefinance_signup_directordetails_email').change(function () {
		var email = $(this).val();
		$.ajax({
			cache: false,
			url: '/ConsumerFinanceSignup/ValidateEmail',
			data: { emailAddress: email },
			type: 'post',
			success: function (data) {
				$('#corporatefinance_signup_directordetails_passwordrequired').val(data.passwordRequired);

				if (data.passwordRequired)
					$('.password-field-section').prop('style', '');
				else
					$('.password-field-section').prop('style', 'display: none');
			}
		});
	});

	$('#corporatefinance_signup_directordetails_homeaddress_current .corporatefinance-signup-directordetails-homeaddress-country').change(function () {
		countryIdChanged(this, '#corporatefinance_signup_directordetails_homeaddress_current');
	});

	$('#corporatefinance_signup_directordetails_homeaddress_previous .corporatefinance-signup-directordetails-homeaddress-country').change(function () {
		countryIdChanged(this, '#corporatefinance_signup_directordetails_homeaddress_previous');
	});

	function countryIdChanged(control, parentSelector) {
		var selectedCountryId = $(control).val();

		$.ajax({
			type: 'POST',
			url: '/ConsumerFinanceSignup/GetStateProvinces',
			dataType: 'json',
			data: {
				countryId: selectedCountryId
			},
			success: function (data) {
				setStateProvince(parentSelector + " .corporatefinance-signup-directordetails-homeaddress-stateprovince", data);
			}
		});
	}

	function setStateProvince(control, data) {
		var select = $(control);

		select.empty();
		
		for (var i = 0; i < data.length; i++) {
			select.append('<option value="' + data[i].Id + '">' + data[i].Name + "</option>");
		}

		select.val("");
	}
});