﻿$(document).ready(function () {
	$('#consumerfinance_paymentmode_selector').change(function () {
		var pageLoader = $('.page-loader');
		if (pageLoader)
			pageLoader.removeAttr('style');

		var selectedPaymentModeId = $(this).val();

		$.ajax({
			type: 'POST',
			url: '/ConsumerPaymentTermSelector/SelectedPaymentModeChanged',
			dataType: 'json',
			data: {
				paymentModeId: selectedPaymentModeId
			},
			success: function (data) {
				if (data.showFinanceOption) {
					$('#consumerfinance_paymentterm_financeoption').val($('#consumerfinance_paymentterm_financeoption option:first').val());
					setFinanceOption($('#consumerfinance_paymentterm_financeoption').val());

					$('#consumerfinance_paymentterm_financeoption_section').show();
					$('#consumerfinance_paymentterm_apply_section').show();
				}
				else {
					$('#consumerfinance_paymentterm_financeoption_section').hide();
					$('#consumerfinance_paymentterm_apply_section').hide();
					//window.location = window.location.href;

					$.ajax("EcorenewFinanceCommon/OrderSummaryTable").done(function (data) {
						$('.order-summary-content .table-wrapper .cart').replaceWith(data);
					});

					$.ajax("EcorenewFinanceCommon/OrderTotals").done(function (data) {
						$('.order-summary-content .totals .total-info').replaceWith(data);
					});

					$.ajax("EcorenewFinanceCommon/FlyoutShoppingCart").done(function (data) {
						$('#flyout-cart').replaceWith(data);
					});
				}
			}
		});
	});

	$('#consumerfinance_paymentterm_financeoption').change(function () {
		var selectedFinanceOptionId = $(this).val();
		setFinanceOption(selectedFinanceOptionId);
	});

	function setFinanceOption(selectedFinanceOptionId) {
		$.ajax({
			type: 'POST',
			url: '/ConsumerPaymentTermSelector/SelectedConsumerFinanceOptionChanged',
			dataType: 'json',
			data: {
				financeOptionId: selectedFinanceOptionId
			},
			success: function () {
				//window.location = window.location.href;
				$.ajax("EcorenewFinanceCommon/OrderSummaryTable").done(function (data) {
					$('.order-summary-content .table-wrapper .cart').replaceWith(data);
				});

				$.ajax("EcorenewFinanceCommon/OrderTotals").done(function (data) {
					$('.order-summary-content .totals .total-info').replaceWith(data);
				});

				$.ajax("EcorenewFinanceCommon/FlyoutShoppingCart").done(function (data) {
					$('#flyout-cart').replaceWith(data);
				});
			}
		});
	}

	$(document).ajaxStop(function () {
		var pageLoader = $('.page-loader');
		if (pageLoader)
			pageLoader.hide();
	});
});