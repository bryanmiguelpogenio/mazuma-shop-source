﻿$(document).ready(function () {
	var financeBreakdownXhr = null;

	function reloadFinanceValues() {
		$('#consumerfinance_productdetails_aslowas').hide();

		var productId = $('#consumerfinance_productdetails_productid').val();
		var financeBreakdownUrl = "/ProductDetailsConsumerFinanceBreakdown/GetFinanceBreakdown?productId=" + productId;

		if (financeBreakdownXhr != null) {
			financeBreakdownXhr.abort();
			financeBreakdownXhr = null
		}

		var modalContent = $('#consumerfinance_breakdownpopup .consumerfinance-modal-content .terms');
		modalContent.html('');

		financeBreakdownXhr = $.ajax({
			cache: false,
			url: financeBreakdownUrl,
			data: $('#product-details-form').serialize(),
			type: 'post',
			success: function (data) {
				$('#aslowas_price').text(data.LowestMonthlyPayment);
				$('#aslowas_month').text(data.LowestMonthlyPaymentMonths + " months");

				$('#finance-monthly-deposit').text(data.LowestMonthlyPaymentMonths);
				$('#finance-months').text(data.InitialPayment);
				$('#finance-monthly-payment').text(data.LowestMonthlyPayment);
				$('#finance-total-payable').text(data.LowestMonthlyPaymentTotalAmountPayable);
				$('.finance-apr').text(data.Apr);
				$('.finance-option-to-purchase-fee').text(data.OptionToPurchaseFee)

				for (var i = 0; i < data.FinanceValues.TermViews.length; i++) {
					modalContent.html(modalContent.html() + data.FinanceValues.TermViews[i]);
				}

				$('#consumerfinance_productdetails_aslowas').show();
			}
		});
	}

	reloadFinanceValues();

	$(".product-price").on('DOMSubtreeModified', function () {
		reloadFinanceValues();
	});

	function showBreakdownModal() {
		modal = document.getElementById('consumerfinance_breakdownpopup');
		modal.style.display = 'block';
	}

	function hideBreakdownModal() {
		modal = document.getElementById('consumerfinance_breakdownpopup');
		modal.style.display = "none";
	}

	$("#consumerfinance_productdetails_comparefinanceoptions").click(function () {
		showBreakdownModal();
	});

	$('.finance-consumer-compare-payment-options').click(function () {
		showBreakdownModal();
	});

	window.onclick = function (event) {
		modal = document.getElementById('consumerfinance_breakdownpopup');
		if (event.target == modal) {
			hideBreakdownModal();
		}
	}

	$('.consumerfinance-modal-close').click(function () {
		hideBreakdownModal();
	});
});