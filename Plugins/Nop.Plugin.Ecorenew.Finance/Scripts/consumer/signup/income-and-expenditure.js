﻿function expenditureCategorySelectValueChange(rangeSelectId, specificValueInputId, specificValueEnabledHiddenId) {
	if ($(rangeSelectId + ' option:last').is(':selected')) {
		$(specificValueEnabledHiddenId).val(true);
		$(specificValueInputId).show();
	} else {
		$(specificValueEnabledHiddenId).val(false);
		$(specificValueInputId).hide();
	}
}

$(document).ready(function () {
	$('[data-toggle="popover"]').popover();

	$('#consumerfinance_signup_monthlyincomeaftertax').mask('00000');
});