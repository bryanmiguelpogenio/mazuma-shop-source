﻿$(document).ready(function () {
	$('#before_you_begin').on('show.bs.collapse', function () {
		$('#before_you_beginIcon').removeClass('glyphicon-plus');
		$('#before_you_beginIcon').addClass('glyphicon-minus');
	});
	$('#before_you_begin').on('hide.bs.collapse', function () {
		$('#before_you_beginIcon').removeClass('glyphicon-minus');
		$('#before_you_beginIcon').addClass('glyphicon-plus');
	});

	$('#qualifications').on('show.bs.collapse', function () {
		$('#qualificationsIcon').removeClass('glyphicon-plus');
		$('#qualificationsIcon').addClass('glyphicon-minus');
	});
	$('#qualifications').on('hide.bs.collapse', function () {
		$('#qualificationsIcon').removeClass('glyphicon-minus');
		$('#qualificationsIcon').addClass('glyphicon-plus');
	});
});