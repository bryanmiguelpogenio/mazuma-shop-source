﻿$(document).ready(function () {

	evalEmploymentStatus();

	$("#consumerfinance_signup_employmentstatus").change(function () {
		evalEmploymentStatus();
	});

	setRequired();
});

function evalEmploymentStatus() {
	var e = $('#consumerfinance_signup_employmentstatus').val();

	if (e == "1") {
		$("#consumerfinance_signup_employment_employed").show();
		$("#consumerfinance_signup_employment_selfemployed").hide();
		$("#consumerfinance_signup_employment_otherstatus").hide();
	}
	else if (e == "2") {
		$("#consumerfinance_signup_employment_employed").hide();
		$("#consumerfinance_signup_employment_selfemployed").show();
		$("#consumerfinance_signup_employment_otherstatus").hide();
	}
	else if (e == "3") {
		$("#consumerfinance_signup_employment_employed").hide();
		$("#consumerfinance_signup_employment_selfemployed").hide();
		$("#consumerfinance_signup_employment_otherstatus").show();
	}
	else {
		$("#consumerfinance_signup_employment_employed").hide();
		$("#consumerfinance_signup_employment_selfemployed").hide();
		$("#consumerfinance_signup_employment_otherstatus").hide();
	}
}