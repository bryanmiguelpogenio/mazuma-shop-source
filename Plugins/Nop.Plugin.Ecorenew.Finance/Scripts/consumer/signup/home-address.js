﻿$(document).ready(function () {

	$("#consumerfinance_signup_homeaddress_previous").hide();

	var currentYears = $('#consumerfinance_signup_homeaddress_current .consumerfinance-signup-homeaddress-years');
	var currentMonths = $('#consumerfinance_signup_homeaddress_current .consumerfinance-signup-homeaddress-months');
	if (currentYears.val() < 3 && (currentYears.val() != 0 || currentMonths.val() != 0)) {
		$("#consumerfinance_signup_homeaddress_previous").show();
	}

	currentYears.change(function () {
		if ($(this).val() < 3) {
			$("#consumerfinance_signup_homeaddress_previous").show();
		} else {
			$("#consumerfinance_signup_homeaddress_previous").hide();
		}
	});

	currentMonths.change(function () {
		if ($('#current-years').val() < 3) {
			$("#consumerfinance_signup_homeaddress_previous").show();
		} else {
			$("#consumerfinance_signup_homeaddress_previous").hide();
		}
	});

	$('#consumerfinance_signup_homeaddress_current .consumerfinance-signup-homeaddress-country').change(function () {
		countryIdChanged(this, '#consumerfinance_signup_homeaddress_current');
	});

	$('#consumerfinance_signup_homeaddress_previous .consumerfinance-signup-homeaddress-country').change(function () {
		countryIdChanged(this, '#consumerfinance_signup_homeaddress_previous');
	});

	function countryIdChanged(control, parentSelector) {
		var selectedCountryId = $(control).val();

		$.ajax({
			type: 'POST',
			url: '/ConsumerFinanceSignup/GetStateProvinces',
			dataType: 'json',
			data: {
				countryId: selectedCountryId
			},
			success: function (data) {
				setStateProvince(parentSelector + " .consumerfinance-signup-homeaddress-stateprovince", data);
			}
		});
	}

	function setStateProvince(control, data) {
		var select = $(control);

		select.empty();
		
		for (var i = 0; i < data.length; i++) {
			select.append('<option value="' + data[i].Id + '">' + data[i].Name + "</option>");
		}

		select.val("");
	}
});