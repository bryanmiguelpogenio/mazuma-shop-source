﻿$(document).ready(function () {
	$('[data-toggle="popover"]').popover();

	$("#consumerfinance_signup_personalinformation_mobile").mask("+00 000 000 0000");

	$('#consumerfinance_signup_personalinformation_email').change(function () {
		var email = $(this).val();
		$.ajax({
			cache: false,
			url: '/ConsumerFinanceSignup/ValidateEmail',
			data: { emailAddress: email },
			type: 'post',
			success: function (data) {
				$('#consumerfinance_signup_personalinformation_passwordrequired').val(data.passwordRequired);

				if (data.passwordRequired)
					$('.password-field-section').prop('style', '');
				else
					$('.password-field-section').prop('style', 'display: none');
			}
		});
	});
});