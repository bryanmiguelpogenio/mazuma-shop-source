﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework.Mvc.Routing;

namespace Nop.Plugin.Ecorenew.Finance
{
    public class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Ecorenew.Finance.Consumer.Signup", "ConsumerFinanceSignup/{action}",
                new { controller = "ConsumerFinanceSignup", action = "Index" });

            routeBuilder.MapRoute("Ecorenew.Finance.Corporate.Signup", "CorporateFinanceSignup/{action}",
                new { controller = "CorporateFinanceSignup", action = "Index" });

            routeBuilder.MapRoute("Ecorenew.Finance.ShoppingCart", "FinanceShoppingCart/{action}",
                new { controller = "EcorenewFinanceShoppingCart" });
        }
        public int Priority
        {
            get
            {
                return -1;
            }
        }
    }
}