﻿using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Nop.Core.Domain.Orders;
using Nop.Core.Events;
using Nop.Services.Events;
using System.Linq;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;

namespace Nop.Plugin.Ecorenew.Finance.Events
{
    public class ShoppingCartItemInsertedEventConsumer : IConsumer<EntityInserted<ShoppingCartItem>>
    {
        #region Fields

        private readonly IConsumerFinanceService _consumerFinanceService;
        private readonly ICorporateFinanceService _corporateFinanceService;
        private readonly FinanceSettings _financeSettings;

        #endregion

        #region Ctor

        public ShoppingCartItemInsertedEventConsumer(IConsumerFinanceService consumerFinanceService,
            ICorporateFinanceService corporateFinanceService,
            FinanceSettings financeSettings)
        {
            this._consumerFinanceService = consumerFinanceService;
            this._corporateFinanceService = corporateFinanceService;
            this._financeSettings = financeSettings;
        }

        #endregion

        #region Methods

        public void HandleEvent(EntityInserted<ShoppingCartItem> eventMessage)
        {
            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        var shoppingCartItemConsumerFinanceOptions = _consumerFinanceService.GetCustomerShoppingCartItemConsumerFinanceOptions();
                        if (shoppingCartItemConsumerFinanceOptions.Any())
                        {
                            _consumerFinanceService.CreateShoppingCartItemFinanceOption(eventMessage.Entity.Id, shoppingCartItemConsumerFinanceOptions.First().ConsumerFinanceOptionId);
                        }
                        break;
                    case FinanceType.Corporate:
                        var shoppingCartItemCorporateFinanceOptions = _corporateFinanceService.GetCustomerShoppingCartItemCorporateFinanceOptions();
                        if (shoppingCartItemCorporateFinanceOptions.Any())
                        {
                            _corporateFinanceService.CreateShoppingCartItemFinanceOption(eventMessage.Entity.Id, shoppingCartItemCorporateFinanceOptions.First().CorporateFinanceOptionId);
                        }
                        break;
                }
            }
        }

        #endregion
    }
}