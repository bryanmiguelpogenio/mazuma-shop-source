﻿using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Nop.Core.Domain.Orders;
using Nop.Core.Events;
using Nop.Services.Events;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;

namespace Nop.Plugin.Ecorenew.Finance.Events
{
    public class ShoppingCartItemDeletedEventConsumer : IConsumer<EntityDeleted<ShoppingCartItem>>
    {
        #region Fields

        private readonly IConsumerFinanceService _consumerFinanceService;
        private readonly ICorporateFinanceService _corporateFinanceService;
        private readonly FinanceSettings _financeSettings;

        #endregion

        #region Ctor

        public ShoppingCartItemDeletedEventConsumer(IConsumerFinanceService consumerFinanceService,
            ICorporateFinanceService corporateFinanceService,
            FinanceSettings financeSettings)
        {
            this._consumerFinanceService = consumerFinanceService;
            this._corporateFinanceService = corporateFinanceService;
            this._financeSettings = financeSettings;
        }

        #endregion

        #region Methods

        public void HandleEvent(EntityDeleted<ShoppingCartItem> eventMessage)
        {
            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        _consumerFinanceService.DeleteShoppingCartItemFinanceOption(eventMessage.Entity.Id);
                        break;
                    case FinanceType.Corporate:
                        _corporateFinanceService.DeleteShoppingCartItemFinanceOption(eventMessage.Entity.Id);
                        break;
                }
            }
        }

        #endregion
    }
}