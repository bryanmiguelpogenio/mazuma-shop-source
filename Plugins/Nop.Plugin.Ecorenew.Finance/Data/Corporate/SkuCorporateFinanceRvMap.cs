﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;

namespace Nop.Plugin.Ecorenew.Finance.Data.Corporate
{
    public class SkuCorporateFinanceRvMap : NopEntityTypeConfiguration<SkuCorporateFinanceRv>
    {
        public SkuCorporateFinanceRvMap()
        {
            this.ToTable("Ecorenew_SkuCorporateFinanceRv");
            this.HasKey(x => x.Id);
        }
    }
}