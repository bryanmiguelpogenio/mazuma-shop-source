﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;

namespace Nop.Plugin.Ecorenew.Finance.Data.Corporate
{
    public class CorporateFinanceOptionMap : NopEntityTypeConfiguration<CorporateFinanceOption>
    {
        public CorporateFinanceOptionMap()
        {
            this.ToTable("Ecorenew_CorporateFinance_FinanceOption");
            this.HasKey(c => c.Id);

            this.Property(c => c.Description).IsRequired().HasMaxLength(100);
        }
    }
}