﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;

namespace Nop.Plugin.Ecorenew.Finance.Data.Corporate
{
    public class CorporateFinanceSignupMap : NopEntityTypeConfiguration<CorporateFinanceSignup>
    {
        public CorporateFinanceSignupMap()
        {
            this.ToTable("Ecorenew_CorporateFinance_Signup");
            this.HasKey(c => c.Id);

            this.Property(c => c.CorporateFinanceBasicDetails).IsOptional();
            this.Property(c => c.CorporateFinanceDirectorDetails).IsOptional();
            this.Property(c => c.CorporateFinanceBankDetails).IsOptional();
        }
    }
}