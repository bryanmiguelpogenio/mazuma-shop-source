﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;

namespace Nop.Plugin.Ecorenew.Finance.Data.Corporate
{
    public class ShoppingCartItemCorporateFinanceOptionMap : NopEntityTypeConfiguration<ShoppingCartItemCorporateFinanceOption>
    {
        public ShoppingCartItemCorporateFinanceOptionMap()
        {
            this.ToTable("Ecorenew_ShoppingCartItemCorporateFinanceOption");
            this.HasKey(s => s.Id);

            this.HasRequired(s => s.CorporateFinanceOption)
                .WithMany()
                .HasForeignKey(s => s.CorporateFinanceOptionId)
                .WillCascadeOnDelete(false);
        }
    }
}