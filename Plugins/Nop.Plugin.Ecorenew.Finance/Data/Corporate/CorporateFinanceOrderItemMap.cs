﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;

namespace Nop.Plugin.Ecorenew.Finance.Data.Corporate
{
    public class CorporateFinanceOrderItemMap : NopEntityTypeConfiguration<CorporateFinanceOrderItem>
    {
        public CorporateFinanceOrderItemMap()
        {
            this.ToTable("Ecorenew_CorporateFinance_OrderItem");
            this.HasKey(oi => oi.Id);

            this.Property(oi => oi.CurrencyCode).IsRequired().HasMaxLength(5);
            this.Property(oi => oi.ShoppingCartItemId).IsOptional();
            this.Property(oi => oi.OrderItemId).IsOptional();

            this.HasRequired(oi => oi.CorporateFinanceSignup)
                .WithMany(s => s.OrderItems)
                .HasForeignKey(oi => oi.CorporateFinanceSignupId);
        }
    }
}