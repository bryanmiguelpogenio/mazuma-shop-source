﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;

namespace Nop.Plugin.Ecorenew.Finance.Data.Consumer
{
    public class AnnualGrossIncomeMap : NopEntityTypeConfiguration<AnnualGrossIncome>
    {
        public AnnualGrossIncomeMap()
        {
            this.ToTable("Ecorenew_AnnualGrossIncome");
            this.HasKey(a => a.Id);

            this.Property(a => a.Name).IsRequired().HasMaxLength(200);
        }
    }
}