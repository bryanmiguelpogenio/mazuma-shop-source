﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;

namespace Nop.Plugin.Ecorenew.Finance.Data.Consumer
{
    public class ExpenditureCategoryMap : NopEntityTypeConfiguration<ExpenditureCategory>
    {
        public ExpenditureCategoryMap()
        {
            this.ToTable("Ecorenew_ExpenditureCategory");
            this.HasKey(e => e.Id);

            this.Property(e => e.LocaleStringResourceName).IsRequired().HasMaxLength(200);
        }
    }
}