﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;

namespace Nop.Plugin.Ecorenew.Finance.Data.Consumer
{
    public class EmploymentStatusMap : NopEntityTypeConfiguration<EmploymentStatus>
    {
        public EmploymentStatusMap()
        {
            this.ToTable("Ecorenew_EmploymentStatus");
            this.HasKey(e => e.Id);

            this.Property(e => e.Name).IsRequired().HasMaxLength(100);
        }
    }
}