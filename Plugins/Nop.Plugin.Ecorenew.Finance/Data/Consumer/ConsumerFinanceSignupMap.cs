﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;

namespace Nop.Plugin.Ecorenew.Finance.Data.Consumer
{
    public class ConsumerFinanceSignupMap : NopEntityTypeConfiguration<ConsumerFinanceSignup>
    {
        public ConsumerFinanceSignupMap()
        {
            this.ToTable("Ecorenew_ConsumerFinance_Signup");
            this.HasKey(c => c.Id);

            this.Ignore(c => c.AllChecksPassed);

            this.Property(c => c.ConsumerFinancePersonalInformation).IsOptional();
            this.Property(c => c.ConsumerFinanceHomeAddress).IsOptional();
            this.Property(c => c.ConsumerFinanceEmployment).IsOptional();
            this.Property(c => c.ConsumerFinanceBank).IsOptional();
            this.Property(c => c.ConsumerFinanceIncomeAndExpenditure).IsOptional();
            this.Property(c => c.AffordabilityCheckPassed).IsOptional();
            this.Property(c => c.CallValidatePassed).IsOptional();
            this.Property(c => c.CallReportPassed).IsOptional();
            this.Property(c => c.CallValidateResultId).IsOptional();
            this.Property(c => c.CallReportResultId).IsOptional();
            this.Property(c => c.DocuSignEnvelopeId).IsOptional();
            this.Property(c => c.SignupCompletedOnUtc).IsOptional();
            this.Property(c => c.OrderId).IsOptional();
        }
    }
}