﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;

namespace Nop.Plugin.Ecorenew.Finance.Data.Consumer
{
    public class ExpenditureCategoryRangeMap : NopEntityTypeConfiguration<ExpenditureCategoryRange>
    {
        public ExpenditureCategoryRangeMap()
        {
            this.ToTable("Ecorenew_ExpenditureCategoryRange");
            this.HasKey(ecr => ecr.Id);

            this.HasRequired(ecr => ecr.ExpenditureCategory)
                .WithMany(ec => ec.AvailableRanges)
                .HasForeignKey(ecr => ecr.ExpenditureCategoryId)
                .WillCascadeOnDelete(false);

            this.HasRequired(ecr => ecr.ExpenditureRange)
                .WithMany()
                .HasForeignKey(ecr => ecr.ExpenditureRangeId)
                .WillCascadeOnDelete(false);
        }
    }
}