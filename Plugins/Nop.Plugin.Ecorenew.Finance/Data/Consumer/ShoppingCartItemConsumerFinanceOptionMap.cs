﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;

namespace Nop.Plugin.Ecorenew.Finance.Data.Consumer
{
    public class ShoppingCartItemConsumerFinanceOptionMap : NopEntityTypeConfiguration<ShoppingCartItemConsumerFinanceOption>
    {
        public ShoppingCartItemConsumerFinanceOptionMap()
        {
            this.ToTable("Ecorenew_ShoppingCartItemConsumerFinanceOption");
            this.HasKey(s => s.Id);

            this.HasRequired(s => s.ConsumerFinanceOption)
                .WithMany()
                .HasForeignKey(s => s.ConsumerFinanceOptionId)
                .WillCascadeOnDelete(false);
        }
    }
}