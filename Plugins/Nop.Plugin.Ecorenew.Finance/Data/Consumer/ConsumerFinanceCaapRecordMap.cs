﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;

namespace Nop.Plugin.Ecorenew.Finance.Data.Consumer
{
    public class ConsumerFinanceCaapRecordMap : NopEntityTypeConfiguration<ConsumerFinanceCaapRecord>
    {
        public ConsumerFinanceCaapRecordMap()
        {
            this.ToTable("Ecorenew_ConsumerFinance_CaapRecord");
            this.HasKey(c => c.Id);
        }
    }
}