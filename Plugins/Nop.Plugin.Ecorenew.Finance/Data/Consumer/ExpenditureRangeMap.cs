﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;

namespace Nop.Plugin.Ecorenew.Finance.Data.Consumer
{
    public class ExpenditureRangeMap : NopEntityTypeConfiguration<ExpenditureRange>
    {
        public ExpenditureRangeMap()
        {
            this.ToTable("Ecorenew_ExpenditureRange");
            this.HasKey(e => e.Id);

            this.Property(e => e.RangeDescription).IsRequired().HasMaxLength(200);
        }
    }
}