﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;

namespace Nop.Plugin.Ecorenew.Finance.Data.Consumer
{
    public class ConsumerFinanceOrderItemMap : NopEntityTypeConfiguration<ConsumerFinanceOrderItem>
    {
        public ConsumerFinanceOrderItemMap()
        {
            this.ToTable("Ecorenew_ConsumerFinance_OrderItem");
            this.HasKey(oi => oi.Id);

            this.Property(oi => oi.CurrencyCode).IsRequired().HasMaxLength(5);
            this.Property(oi => oi.ShoppingCartItemId).IsOptional();
            this.Property(oi => oi.OrderItemId).IsOptional();

            this.HasRequired(oi => oi.ConsumerFinanceSignup)
                .WithMany(s => s.OrderItems)
                .HasForeignKey(oi => oi.ConsumerFinanceSignupId);

            this.HasOptional(oi => oi.ConsumerFinanceCaapRecord)
                .WithMany(c => c.OrderItems)
                .HasForeignKey(oi => oi.ConsumerFinanceCaapRecordId)
                .WillCascadeOnDelete(false);
        }
    }
}