﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;

namespace Nop.Plugin.Ecorenew.Finance.Data.Consumer
{
    public class ConsumerFinanceOptionMap : NopEntityTypeConfiguration<ConsumerFinanceOption>
    {
        public ConsumerFinanceOptionMap()
        {
            this.ToTable("Ecorenew_ConsumerFinance_FinanceOption");
            this.HasKey(c => c.Id);

            this.Property(c => c.Description).IsRequired().HasMaxLength(100);
        }
    }
}