﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;

namespace Nop.Plugin.Ecorenew.Finance.Data.Common
{
    public class NumberOfDependentsMap : NopEntityTypeConfiguration<NumberOfDependents>
    {
        public NumberOfDependentsMap()
        {
            this.ToTable("Ecorenew_NumberOfDependents");
            this.HasKey(n => n.Id);

            this.Property(n => n.Name).IsRequired().HasMaxLength(10);
        }
    }
}