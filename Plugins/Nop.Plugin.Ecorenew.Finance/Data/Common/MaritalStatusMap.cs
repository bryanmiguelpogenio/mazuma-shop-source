﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;

namespace Nop.Plugin.Ecorenew.Finance.Data.Common
{
    public class MaritalStatusMap : NopEntityTypeConfiguration<MaritalStatus>
    {
        public MaritalStatusMap()
        {
            this.ToTable("Ecorenew_MaritalStatus");
            this.HasKey(m => m.Id);

            this.Property(m => m.Name).IsRequired().HasMaxLength(100);
        }
    }
}