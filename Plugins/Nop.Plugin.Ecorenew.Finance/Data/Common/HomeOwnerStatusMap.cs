﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;

namespace Nop.Plugin.Ecorenew.Finance.Data.Common
{
    public class HomeOwnerStatusMap : NopEntityTypeConfiguration<HomeOwnerStatus>
    {
        public HomeOwnerStatusMap()
        {
            this.ToTable("Ecorenew_HomeOwnerStatus");
            this.HasKey(h => h.Id);

            this.Property(h => h.Name).IsRequired().HasMaxLength(200);
        }
    }
}