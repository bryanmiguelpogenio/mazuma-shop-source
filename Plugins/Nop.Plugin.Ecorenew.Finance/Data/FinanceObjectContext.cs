﻿using Ecorenew.Common.CallCredit.Data;
using Ecorenew.Common.CallCredit.Domain;
using Nop.Plugin.Ecorenew.Finance.Data.Common;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Plugin.Ecorenew.Finance.Data.Consumer;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;
using Nop.Core;
using Nop.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Nop.Plugin.Ecorenew.Finance.Data.Corporate;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;

namespace Nop.Plugin.Ecorenew.Finance.Data
{
    /// <summary>
    /// Object context
    /// </summary>
    public class FinanceObjectContext : DbContext, IDbContext
    {
        #region Ctor

        public FinanceObjectContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            //((IObjectContextAdapter) this).ObjectContext.ContextOptions.LazyLoadingEnabled = true;
        }

        #endregion

        #region Utilities

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AnnualGrossIncomeMap());
            modelBuilder.Configurations.Add(new CallReportResultMap());
            modelBuilder.Configurations.Add(new CallValidateResultMap());
            modelBuilder.Configurations.Add(new ConsumerFinanceCaapRecordMap());
            modelBuilder.Configurations.Add(new ConsumerFinanceOptionMap());
            modelBuilder.Configurations.Add(new ConsumerFinanceOrderItemMap());
            modelBuilder.Configurations.Add(new ConsumerFinanceSignupMap());
            modelBuilder.Configurations.Add(new EmploymentStatusMap());
            modelBuilder.Configurations.Add(new ExpenditureCategoryMap());
            modelBuilder.Configurations.Add(new ExpenditureRangeMap());
            modelBuilder.Configurations.Add(new ExpenditureCategoryRangeMap());
            modelBuilder.Configurations.Add(new HomeOwnerStatusMap());
            modelBuilder.Configurations.Add(new MaritalStatusMap());
            modelBuilder.Configurations.Add(new NumberOfDependentsMap());
            modelBuilder.Configurations.Add(new ShoppingCartItemConsumerFinanceOptionMap());

            modelBuilder.Configurations.Add(new CorporateFinanceOptionMap());
            modelBuilder.Configurations.Add(new CorporateFinanceOrderItemMap());
            modelBuilder.Configurations.Add(new CorporateFinanceSignupMap());
            modelBuilder.Configurations.Add(new ShoppingCartItemCorporateFinanceOptionMap());

            modelBuilder.Configurations.Add(new SkuCorporateFinanceRvMap());

            //disable EdmMetadata generation
            //modelBuilder.Conventions.Remove<IncludeMetadataConvention>();
            base.OnModelCreating(modelBuilder);
        }

        #endregion

        #region Methods

        public string CreateDatabaseScript()
        {
            return ((IObjectContextAdapter)this).ObjectContext.CreateDatabaseScript();
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        /// <summary>
        /// Install
        /// </summary>
        public void Install()
        {
            using (var transaction = Database.BeginTransaction())
            {
                try
                {
                    //create the tables
                    var dbScript = CreateDatabaseScript();
                    Database.ExecuteSqlCommand(dbScript);

                    Set<MaritalStatus>().Add(new MaritalStatus { Name = "Single" });
                    Set<MaritalStatus>().Add(new MaritalStatus { Name = "Married" });
                    Set<MaritalStatus>().Add(new MaritalStatus { Name = "Living Together" });
                    Set<MaritalStatus>().Add(new MaritalStatus { Name = "Divorced" });
                    Set<MaritalStatus>().Add(new MaritalStatus { Name = "Common Law / Partner" });
                    Set<MaritalStatus>().Add(new MaritalStatus { Name = "Separated" });
                    Set<MaritalStatus>().Add(new MaritalStatus { Name = "Widowed" });
                    Set<MaritalStatus>().Add(new MaritalStatus { Name = "Other" });

                    Set<NumberOfDependents>().Add(new NumberOfDependents { Name = "0" });
                    Set<NumberOfDependents>().Add(new NumberOfDependents { Name = "1" });
                    Set<NumberOfDependents>().Add(new NumberOfDependents { Name = "2" });
                    Set<NumberOfDependents>().Add(new NumberOfDependents { Name = "3" });
                    Set<NumberOfDependents>().Add(new NumberOfDependents { Name = "4" });
                    Set<NumberOfDependents>().Add(new NumberOfDependents { Name = "5" });
                    Set<NumberOfDependents>().Add(new NumberOfDependents { Name = "6" });
                    Set<NumberOfDependents>().Add(new NumberOfDependents { Name = "6 or more" });

                    Set<HomeOwnerStatus>().Add(new HomeOwnerStatus { Name = "Home owner" });
                    Set<HomeOwnerStatus>().Add(new HomeOwnerStatus { Name = "Tenant" });
                    Set<HomeOwnerStatus>().Add(new HomeOwnerStatus { Name = "Living with parents" });
                    Set<HomeOwnerStatus>().Add(new HomeOwnerStatus { Name = "Living with friends" });

                    Set<EmploymentStatus>().Add(new EmploymentStatus { Name = "Employed" });
                    Set<EmploymentStatus>().Add(new EmploymentStatus { Name = "Self employed" });
                    Set<EmploymentStatus>().Add(new EmploymentStatus { Name = "Homemaker / Retired / Other" });

                    Set<AnnualGrossIncome>().Add(new AnnualGrossIncome { Name = "Up to £4,999" });
                    Set<AnnualGrossIncome>().Add(new AnnualGrossIncome { Name = "Up to £9,999" });
                    Set<AnnualGrossIncome>().Add(new AnnualGrossIncome { Name = "Up to £14,999" });
                    Set<AnnualGrossIncome>().Add(new AnnualGrossIncome { Name = "Up to £19,999" });
                    Set<AnnualGrossIncome>().Add(new AnnualGrossIncome { Name = "Up to £24,999" });
                    Set<AnnualGrossIncome>().Add(new AnnualGrossIncome { Name = "Up to £29,999" });
                    Set<AnnualGrossIncome>().Add(new AnnualGrossIncome { Name = "Up to £34,999" });
                    Set<AnnualGrossIncome>().Add(new AnnualGrossIncome { Name = "Up to £39,999" });
                    Set<AnnualGrossIncome>().Add(new AnnualGrossIncome { Name = "Up to £44,999" });
                    Set<AnnualGrossIncome>().Add(new AnnualGrossIncome { Name = "Up to £49,999" });
                    Set<AnnualGrossIncome>().Add(new AnnualGrossIncome { Name = "More than £50,000" });

                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 0", MidPointValue = 0 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 1 - 50", MidPointValue = 25 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 51 - 100", MidPointValue = 75 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 101 - 150", MidPointValue = 125 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 151 - 200", MidPointValue = 175 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 201 - 250", MidPointValue = 225 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 251 - 300", MidPointValue = 275 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 301 - 350", MidPointValue = 325 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 351 - 400", MidPointValue = 375 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 401 - 450", MidPointValue = 425 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 451 - 500", MidPointValue = 475 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = " > £ 500", MidPointValue = 500 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 1 - 100", MidPointValue = 50 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 101 - 200", MidPointValue = 150 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 201 - 300", MidPointValue = 250 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 301 - 400", MidPointValue = 350 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 401 - 500", MidPointValue = 450 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 501 - 600", MidPointValue = 550 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 601 - 700", MidPointValue = 650 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 701 - 800", MidPointValue = 750 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 801 - 900", MidPointValue = 850 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = "£ 901 - 1000", MidPointValue = 950 });
                    Set<ExpenditureRange>().Add(new ExpenditureRange { IsActive = true, RangeDescription = " > £ 1000", MidPointValue = 1000 });

                    Set<ExpenditureCategory>().Add(new ExpenditureCategory
                    {
                        LocaleStringResourceName = "Ecorenew.Finance.ExpenditureCategory.RentMortgage",
                        Description = "Net rent, mortgage payments, contribution to shared housing, etc.",
                        DisplayOrderNumber = 1,
                        IsActive = true
                    });

                    SaveChanges();

                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 1, ExpenditureRangeId = 1 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 1, ExpenditureRangeId = 13 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 1, ExpenditureRangeId = 14 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 1, ExpenditureRangeId = 15 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 1, ExpenditureRangeId = 16 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 1, ExpenditureRangeId = 17 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 1, ExpenditureRangeId = 18 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 1, ExpenditureRangeId = 19 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 1, ExpenditureRangeId = 20 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 1, ExpenditureRangeId = 21 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 1, ExpenditureRangeId = 22 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 1, ExpenditureRangeId = 23 });

                    Set<ExpenditureCategory>().Add(new ExpenditureCategory
                    {
                        LocaleStringResourceName = "Ecorenew.Finance.ExpenditureCategory.MonthlyCreditCommitments",
                        Description = "Credit card minimum payments, car lease and loans, personal loans, payday loans, etc.",
                        DisplayOrderNumber = 2,
                        IsActive = true
                    });

                    SaveChanges();

                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 2, ExpenditureRangeId = 1 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 2, ExpenditureRangeId = 2 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 2, ExpenditureRangeId = 3 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 2, ExpenditureRangeId = 4 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 2, ExpenditureRangeId = 5 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 2, ExpenditureRangeId = 6 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 2, ExpenditureRangeId = 7 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 2, ExpenditureRangeId = 8 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 2, ExpenditureRangeId = 9 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 2, ExpenditureRangeId = 10 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 2, ExpenditureRangeId = 11 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 2, ExpenditureRangeId = 12 });

                    Set<ExpenditureCategory>().Add(new ExpenditureCategory
                    {
                        LocaleStringResourceName = "Ecorenew.Finance.ExpenditureCategory.UtilitiesBills",
                        Description = "Gas, electricity, telephone, cable tv, internet, leasehold service, etc.",
                        DisplayOrderNumber = 3,
                        IsActive = true
                    });

                    SaveChanges();

                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 3, ExpenditureRangeId = 1 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 3, ExpenditureRangeId = 2 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 3, ExpenditureRangeId = 3 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 3, ExpenditureRangeId = 4 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 3, ExpenditureRangeId = 5 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 3, ExpenditureRangeId = 6 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 3, ExpenditureRangeId = 7 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 3, ExpenditureRangeId = 8 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 3, ExpenditureRangeId = 9 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 3, ExpenditureRangeId = 10 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 3, ExpenditureRangeId = 11 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 3, ExpenditureRangeId = 12 });

                    Set<ExpenditureCategory>().Add(new ExpenditureCategory
                    {
                        LocaleStringResourceName = "Ecorenew.Finance.ExpenditureCategory.Transportation",
                        Description = "Public transport, petrol, car insurance, etc.",
                        DisplayOrderNumber = 4,
                        IsActive = true
                    });

                    SaveChanges();

                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 4, ExpenditureRangeId = 1 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 4, ExpenditureRangeId = 2 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 4, ExpenditureRangeId = 3 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 4, ExpenditureRangeId = 4 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 4, ExpenditureRangeId = 5 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 4, ExpenditureRangeId = 6 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 4, ExpenditureRangeId = 7 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 4, ExpenditureRangeId = 8 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 4, ExpenditureRangeId = 9 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 4, ExpenditureRangeId = 10 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 4, ExpenditureRangeId = 11 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 4, ExpenditureRangeId = 12 });

                    Set<ExpenditureCategory>().Add(new ExpenditureCategory
                    {
                        LocaleStringResourceName = "Ecorenew.Finance.ExpenditureCategory.Food",
                        Description = "Essential groceries, lunch at work, non-alcoholic, beverages, etc.",
                        DisplayOrderNumber = 5,
                        IsActive = true
                    });

                    SaveChanges();

                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 5, ExpenditureRangeId = 1 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 5, ExpenditureRangeId = 2 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 5, ExpenditureRangeId = 3 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 5, ExpenditureRangeId = 4 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 5, ExpenditureRangeId = 5 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 5, ExpenditureRangeId = 6 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 5, ExpenditureRangeId = 7 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 5, ExpenditureRangeId = 8 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 5, ExpenditureRangeId = 9 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 5, ExpenditureRangeId = 10 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 5, ExpenditureRangeId = 11 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 5, ExpenditureRangeId = 12 });

                    Set<ExpenditureCategory>().Add(new ExpenditureCategory
                    {
                        LocaleStringResourceName = "Ecorenew.Finance.ExpenditureCategory.Others",
                        Description = "All recurring medical bills, educational costs, childcare costs and leisure costs, etc.",
                        DisplayOrderNumber = 6,
                        IsActive = true
                    });

                    SaveChanges();

                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 6, ExpenditureRangeId = 1 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 6, ExpenditureRangeId = 2 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 6, ExpenditureRangeId = 3 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 6, ExpenditureRangeId = 4 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 6, ExpenditureRangeId = 5 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 6, ExpenditureRangeId = 6 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 6, ExpenditureRangeId = 7 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 6, ExpenditureRangeId = 8 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 6, ExpenditureRangeId = 9 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 6, ExpenditureRangeId = 10 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 6, ExpenditureRangeId = 11 });
                    Set<ExpenditureCategoryRange>().Add(new ExpenditureCategoryRange { ExpenditureCategoryId = 6, ExpenditureRangeId = 12 });

                    SaveChanges();

                    // Install consumer finance options
                    Set<ConsumerFinanceOption>().Add(new ConsumerFinanceOption { DurationInMonths = 12, Description = "12 Months", AgreementRate = 92.26M, IsActive = true });
                    Set<ConsumerFinanceOption>().Add(new ConsumerFinanceOption { DurationInMonths = 18, Description = "18 Months", AgreementRate = 64.39M, IsActive = true });
                    Set<ConsumerFinanceOption>().Add(new ConsumerFinanceOption { DurationInMonths = 24, Description = "24 Months", AgreementRate = 50.51M, IsActive = true });

                    // Install corporate finance options
                    Set<CorporateFinanceOption>().Add(new CorporateFinanceOption { DurationInMonths = 24, Description = "24 Months", AgreementRate = 50.51M, IsActive = true });

                    SaveChanges();

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        /// <summary>
        /// Uninstall
        /// </summary>
        public void Uninstall()
        {
            this.DropPluginTable(this.GetTableName<CallReportResult>());
            this.DropPluginTable(this.GetTableName<CallValidateResult>());
            this.DropPluginTable(this.GetTableName<ShoppingCartItemConsumerFinanceOption>());
            this.DropPluginTable(this.GetTableName<ConsumerFinanceOption>());
            this.DropPluginTable(this.GetTableName<ConsumerFinanceOrderItem>());
            this.DropPluginTable(this.GetTableName<ConsumerFinanceCaapRecord>());
            this.DropPluginTable(this.GetTableName<ConsumerFinanceSignup>());
            this.DropPluginTable(this.GetTableName<ExpenditureCategoryRange>());
            this.DropPluginTable(this.GetTableName<ExpenditureCategory>());
            this.DropPluginTable(this.GetTableName<ExpenditureRange>());
            this.DropPluginTable(this.GetTableName<AnnualGrossIncome>());
            this.DropPluginTable(this.GetTableName<EmploymentStatus>());
            this.DropPluginTable(this.GetTableName<HomeOwnerStatus>());
            this.DropPluginTable(this.GetTableName<MaritalStatus>());
            this.DropPluginTable(this.GetTableName<NumberOfDependents>());

            this.DropPluginTable(this.GetTableName<ShoppingCartItemCorporateFinanceOption>());
            this.DropPluginTable(this.GetTableName<CorporateFinanceOption>());
            this.DropPluginTable(this.GetTableName<CorporateFinanceOrderItem>());
            this.DropPluginTable(this.GetTableName<CorporateFinanceSignup>());

            this.DropPluginTable(this.GetTableName<SkuCorporateFinanceRv>());
        }

        /// <summary>
        /// Execute stores procedure and load a list of entities at the end
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="commandText">Command text</param>
        /// <param name="parameters">Parameters</param>
        /// <returns>Entities</returns>
        public IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters) where TEntity : BaseEntity, new()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a raw SQL query that will return elements of the given generic type.  The type can be any type that has properties that match the names of the columns returned from the query, or can be a simple primitive type. The type does not have to be an entity type. The results of this query are never tracked by the context even if the type of object returned is an entity type.
        /// </summary>
        /// <typeparam name="TElement">The type of object returned by the query.</typeparam>
        /// <param name="sql">The SQL query string.</param>
        /// <param name="parameters">The parameters to apply to the SQL query string.</param>
        /// <returns>Result</returns>
        public IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Executes the given DDL/DML command against the database.
        /// </summary>
        /// <param name="sql">The command string</param>
        /// <param name="doNotEnsureTransaction">false - the transaction creation is not ensured; true - the transaction creation is ensured.</param>
        /// <param name="timeout">Timeout value, in seconds. A null value indicates that the default value of the underlying provider will be used</param>
        /// <param name="parameters">The parameters to apply to the command string.</param>
        /// <returns>The result returned by the database after executing the command.</returns>
        public int ExecuteSqlCommand(string sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Detach an entity
        /// </summary>
        /// <param name="entity">Entity</param>
        public void Detach(object entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            ((IObjectContextAdapter)this).ObjectContext.Detach(entity);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether proxy creation setting is enabled (used in EF)
        /// </summary>
        public virtual bool ProxyCreationEnabled
        {
            get
            {
                return this.Configuration.ProxyCreationEnabled;
            }
            set
            {
                this.Configuration.ProxyCreationEnabled = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether auto detect changes setting is enabled (used in EF)
        /// </summary>
        public virtual bool AutoDetectChangesEnabled
        {
            get
            {
                return this.Configuration.AutoDetectChangesEnabled;
            }
            set
            {
                this.Configuration.AutoDetectChangesEnabled = value;
            }
        }

        #endregion
    }
}
