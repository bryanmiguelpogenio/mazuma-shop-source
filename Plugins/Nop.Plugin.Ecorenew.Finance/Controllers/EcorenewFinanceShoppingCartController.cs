﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Shipping.Date;
using Nop.Services.Tax;
using Nop.Web.Controllers;
using Nop.Web.Factories;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security.Captcha;

namespace Nop.Plugin.Ecorenew.Finance.Controllers
{
    public class EcorenewFinanceShoppingCartController : ShoppingCartController
    {
        #region Fields

        private readonly IConsumerFinanceService _consumerFinanceService;
        private readonly ICorporateFinanceService _corporateFinanceService;
        private readonly FinanceSettings _financeSettings;

        #endregion

        #region Ctor


        public EcorenewFinanceShoppingCartController(IShoppingCartModelFactory shoppingCartModelFactory, 
            IProductModelFactory productModelFactory, 
            IProductService productService, 
            IStoreContext storeContext, 
            IWorkContext workContext, 
            IShoppingCartService shoppingCartService, 
            IPictureService pictureService, 
            ILocalizationService localizationService, 
            IProductAttributeService productAttributeService, 
            IProductAttributeParser productAttributeParser, 
            ITaxService taxService, 
            ICurrencyService currencyService, 
            IPriceCalculationService priceCalculationService, 
            IPriceFormatter priceFormatter, 
            ICheckoutAttributeParser checkoutAttributeParser, 
            IDiscountService discountService, 
            ICustomerService customerService, 
            IGiftCardService giftCardService, 
            IDateRangeService dateRangeService, 
            ICheckoutAttributeService checkoutAttributeService, 
            IWorkflowMessageService workflowMessageService, 
            IPermissionService permissionService, 
            IDownloadService downloadService, 
            IStaticCacheManager cacheManager, 
            IWebHelper webHelper, 
            ICustomerActivityService customerActivityService, 
            IGenericAttributeService genericAttributeService,
            IConsumerFinanceService consumerFinanceService,
            ICorporateFinanceService corporateFinanceService,
            IOrderTotalCalculationService orderTotalCalculationService, 
            MediaSettings mediaSettings, 
            ShoppingCartSettings shoppingCartSettings, 
            OrderSettings orderSettings, 
            CaptchaSettings captchaSettings, 
            CustomerSettings customerSettings,
            FinanceSettings financeSettings) 
            : 
            base(shoppingCartModelFactory, 
                productModelFactory, 
                productService, 
                storeContext, 
                workContext, 
                shoppingCartService, 
                pictureService, 
                localizationService, 
                productAttributeService, 
                productAttributeParser, 
                taxService, 
                currencyService, 
                priceCalculationService, 
                priceFormatter, 
                checkoutAttributeParser, 
                discountService, 
                customerService, 
                giftCardService, 
                dateRangeService, 
                checkoutAttributeService, 
                workflowMessageService, 
                permissionService, 
                downloadService, 
                cacheManager, 
                webHelper, 
                customerActivityService, 
                genericAttributeService, 
                orderTotalCalculationService, 
                mediaSettings, 
                shoppingCartSettings, 
                orderSettings, 
                captchaSettings, 
                customerSettings)
        {
            this._consumerFinanceService = consumerFinanceService;
            this._corporateFinanceService = corporateFinanceService;
            this._financeSettings = financeSettings;
        }

        //public EcorenewFinanceShoppingCartController(IShoppingCartModelFactory shoppingCartModelFactory,
        //    IProductModelFactory productModelFactory,
        //    IProductService productService, 
        //    IStoreContext storeContext, 
        //    IWorkContext workContext, 
        //    IShoppingCartService shoppingCartService, 
        //    IPictureService pictureService, 
        //    ILocalizationService localizationService, 
        //    IProductAttributeService productAttributeService, 
        //    IProductAttributeParser productAttributeParser, 
        //    ITaxService taxService, 
        //    ICurrencyService currencyService, 
        //    IPriceCalculationService priceCalculationService, 
        //    IPriceFormatter priceFormatter, 
        //    ICheckoutAttributeParser checkoutAttributeParser, 
        //    IDiscountService discountService, 
        //    ICustomerService customerService, 
        //    IGiftCardService giftCardService, 
        //    IDateRangeService dateRangeService, 
        //    ICheckoutAttributeService checkoutAttributeService, 
        //    IWorkflowMessageService workflowMessageService, 
        //    IPermissionService permissionService, 
        //    IDownloadService downloadService, 
        //    IStaticCacheManager cacheManager, 
        //    IWebHelper webHelper, 
        //    ICustomerActivityService customerActivityService, 
        //    IGenericAttributeService genericAttributeService, 
        //    IConsumerFinanceService consumerFinanceService,
        //    ICorporateFinanceService corporateFinanceService,
        //    MediaSettings mediaSettings, 
        //    ShoppingCartSettings shoppingCartSettings, 
        //    OrderSettings orderSettings, 
        //    CaptchaSettings captchaSettings, 
        //    CustomerSettings customerSettings,
        //    FinanceSettings financeSettings)
        //    : base(shoppingCartModelFactory,
        //          productModelFactory,
        //          productService, 
        //          storeContext, 
        //          workContext, 
        //          shoppingCartService, 
        //          pictureService, 
        //          localizationService, 
        //          productAttributeService, 
        //          productAttributeParser, 
        //          taxService, 
        //          currencyService, 
        //          priceCalculationService, 
        //          priceFormatter, 
        //          checkoutAttributeParser, 
        //          discountService, 
        //          customerService, 
        //          giftCardService, 
        //          dateRangeService, 
        //          checkoutAttributeService, 
        //          workflowMessageService, 
        //          permissionService, 
        //          downloadService, 
        //          cacheManager, 
        //          webHelper, 
        //          customerActivityService, 
        //          genericAttributeService, 
        //          mediaSettings, 
        //          shoppingCartSettings, 
        //          orderSettings, 
        //          captchaSettings, 
        //          customerSettings)
        //{
        //}

        #endregion

        #region Methods

        [HttpPost, ActionName("Cart")]
        [FormValueRequired("updatecart")]
        public override IActionResult UpdateCart(IFormCollection form)
        {
            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        if (!_consumerFinanceService.CustomerCanUpdateShoppingCart())
                        {
                            ErrorNotification("You are not allowed to update your shopping cart. Please complete your previous order first."); // TODO localization and add additional info on error message
                            return RedirectToRoute("ShoppingCart");
                        }
                        break;
                    case FinanceType.Corporate:
                        if (!_corporateFinanceService.CustomerCanUpdateShoppingCart())
                        {
                            ErrorNotification("You are not allowed to update your shopping cart. Please complete your previous order first."); // TODO localization and add additional info on error message
                            return RedirectToRoute("ShoppingCart");
                        }
                        break;
                }
            }

            return base.UpdateCart(form);
        }
        
        #endregion
    }
}