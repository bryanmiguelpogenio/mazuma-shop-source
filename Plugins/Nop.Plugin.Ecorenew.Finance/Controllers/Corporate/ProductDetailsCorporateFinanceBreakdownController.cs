﻿using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Ecorenew.Finance.Models.Corporate.Components;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Themes;
using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;

namespace Nop.Plugin.Ecorenew.Finance.Controllers.Corporate
{
    public class ProductDetailsCorporateFinanceBreakdownController : BaseCorporateFinanceController
    {
        #region Fields

        private readonly ICorporateFinanceCalculationService _corporateFinanceCalculationService;
        private readonly IDownloadService _downloadService;
        private readonly ILocalizationService _localizationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductService _productService;
        private readonly IThemeContext _themeContext;

        #endregion

        #region Ctor

        public ProductDetailsCorporateFinanceBreakdownController(ICorporateFinanceCalculationService corporateFinanceCalculationService,
            IDownloadService downloadService,
            ILocalizationService localizationService,
            IPriceFormatter priceFormatter,
            IProductAttributeParser productAttributeParser,
            IProductAttributeService productAttributeService,
            IProductService productService,
            IThemeContext themeContext)
        {
            this._corporateFinanceCalculationService = corporateFinanceCalculationService;
            this._downloadService = downloadService;
            this._localizationService = localizationService;
            this._priceFormatter = priceFormatter;
            this._productAttributeParser = productAttributeParser;
            this._productAttributeService = productAttributeService;
            this._productService = productService;
            this._themeContext = themeContext;
        }

        #endregion

        #region Utilities

        /// <summary>
	    /// Parse product attributes on the add product to order details page
	    /// </summary>
	    /// <param name="product">Product</param>
	    /// <param name="form">Form</param>
        /// <param name="errors">Errors</param>
        /// <returns>Parsed attributes</returns>
        protected virtual string ParseProductAttributes(Product product, IFormCollection form, List<string> errors)
        {
            var attributesXml = string.Empty;

            var productAttributes = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);
            foreach (var attribute in productAttributes)
            {
                var controlId = $"product_attribute_{attribute.Id}";
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                    case AttributeControlType.ColorSquares:
                    case AttributeControlType.ImageSquares:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!StringValues.IsNullOrEmpty(ctrlAttributes))
                            {
                                var selectedAttributeId = int.Parse(ctrlAttributes);
                                if (selectedAttributeId > 0)
                                {
                                    //get quantity entered by customer
                                    var quantity = 1;
                                    var quantityStr = form[$"product_attribute_{attribute.Id}_{selectedAttributeId}_qty"];
                                    if (!StringValues.IsNullOrEmpty(quantityStr) && (!int.TryParse(quantityStr, out quantity) || quantity < 1))
                                        errors.Add(_localizationService.GetResource("ShoppingCart.QuantityShouldPositive"));

                                    attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                        attribute, selectedAttributeId.ToString(), quantity > 1 ? (int?)quantity : null);
                                }
                            }
                        }
                        break;
                    case AttributeControlType.Checkboxes:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!StringValues.IsNullOrEmpty(ctrlAttributes))
                            {
                                foreach (var item in ctrlAttributes.ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    var selectedAttributeId = int.Parse(item);
                                    if (selectedAttributeId > 0)
                                    {
                                        //get quantity entered by customer
                                        var quantity = 1;
                                        var quantityStr = form[$"product_attribute_{attribute.Id}_{item}_qty"];
                                        if (!StringValues.IsNullOrEmpty(quantityStr) && (!int.TryParse(quantityStr, out quantity) || quantity < 1))
                                            errors.Add(_localizationService.GetResource("ShoppingCart.QuantityShouldPositive"));

                                        attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                            attribute, selectedAttributeId.ToString(), quantity > 1 ? (int?)quantity : null);
                                    }
                                }
                            }
                        }
                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        {
                            //load read-only (already server-side selected) values
                            var attributeValues = _productAttributeService.GetProductAttributeValues(attribute.Id);
                            foreach (var selectedAttributeId in attributeValues
                                .Where(v => v.IsPreSelected)
                                .Select(v => v.Id)
                                .ToList())
                            {
                                //get quantity entered by customer
                                var quantity = 1;
                                var quantityStr = form[$"product_attribute_{attribute.Id}_{selectedAttributeId}_qty"];
                                if (!StringValues.IsNullOrEmpty(quantityStr) && (!int.TryParse(quantityStr, out quantity) || quantity < 1))
                                    errors.Add(_localizationService.GetResource("ShoppingCart.QuantityShouldPositive"));

                                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                    attribute, selectedAttributeId.ToString(), quantity > 1 ? (int?)quantity : null);
                            }
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!StringValues.IsNullOrEmpty(ctrlAttributes))
                            {
                                var enteredText = ctrlAttributes.ToString().Trim();
                                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                    attribute, enteredText);
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                        {
                            var day = form[controlId + "_day"];
                            var month = form[controlId + "_month"];
                            var year = form[controlId + "_year"];
                            DateTime? selectedDate = null;
                            try
                            {
                                selectedDate = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(day));
                            }
                            catch { }
                            if (selectedDate.HasValue)
                            {
                                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                    attribute, selectedDate.Value.ToString("D"));
                            }
                        }
                        break;
                    case AttributeControlType.FileUpload:
                        {
                            Guid.TryParse(form[controlId], out Guid downloadGuid);
                            var download = _downloadService.GetDownloadByGuid(downloadGuid);
                            if (download != null)
                            {
                                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                        attribute, download.DownloadGuid.ToString());
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            //validate conditional attributes (if specified)
            foreach (var attribute in productAttributes)
            {
                var conditionMet = _productAttributeParser.IsConditionMet(attribute, attributesXml);
                if (conditionMet.HasValue && !conditionMet.Value)
                {
                    attributesXml = _productAttributeParser.RemoveProductAttribute(attributesXml, attribute);
                }
            }

            return attributesXml;
        }

        #endregion

        #region Methods

        public virtual IActionResult GetFinanceBreakdown(int productId, IFormCollection form)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                return new NullJsonResult();

            var errors = new List<string>();
            var attributeXml = ParseProductAttributes(product, form, errors);

            var financeValues = _corporateFinanceCalculationService.ComputeProductCorporateFinanceValues(product, attributeXml);
            var lowestMonthlyPaymentTerm = financeValues.TermsBreakdown.FirstOrDefault(t => t.MonthlyPayment == financeValues.TermsBreakdown.Min(t2 => t2.MonthlyPayment));

            return Json(new ProductDetailsCorporateFinanceBreakdownModel
            {
                LowestMonthlyPayment = _priceFormatter.FormatPrice(lowestMonthlyPaymentTerm.MonthlyPayment),
                LowestMonthlyPaymentMonths = lowestMonthlyPaymentTerm.DurationInMonths.ToString(),
                LowestMonthlyPaymentTotal = _priceFormatter.FormatPrice(lowestMonthlyPaymentTerm.TotalPayable),
                FinanceValues = new CorporateFinanceBreakdownPopupComponentModel
                {
                    Terms = financeValues.TermsBreakdown.OrderBy(t => t.DurationInMonths).Select(t => new CorporateFinanceBreakdownTerm
                    {
                        MonthlyPayment = _priceFormatter.FormatPrice(t.MonthlyPayment),
                        NumberOfMonths = t.DurationInMonths,
                        PhoneCost = _priceFormatter.FormatPrice(financeValues.Price),
                        Title = $"{t.DurationInMonths} Months", // TODO localization
                        TotalPayable = _priceFormatter.FormatPrice(t.TotalPayable),
                        InterestCharges = _priceFormatter.FormatPrice(t.InterestCharges)
                    }).ToList(),
                    TermViews = financeValues.TermsBreakdown.OrderBy(t => t.DurationInMonths).Select(t => RenderFinanceTermViewToString(new CorporateFinanceBreakdownTerm
                    {
                        MonthlyPayment = _priceFormatter.FormatPrice(t.MonthlyPayment),
                        NumberOfMonths = t.DurationInMonths,
                        PhoneCost = _priceFormatter.FormatPrice(financeValues.Price),
                        Title = $"{t.DurationInMonths} Months", // TODO localization
                        TotalPayable = _priceFormatter.FormatPrice(t.TotalPayable),
                        InterestCharges = _priceFormatter.FormatPrice(t.InterestCharges)
                    })).ToList()
                }
            });
        }

        public virtual string RenderFinanceTermViewToString(CorporateFinanceBreakdownTerm financeTerm)
        {
            var themeViewName = $"{PLUGIN_URL}/Themes/{_themeContext.WorkingThemeName}/Views/Corporate/Components/CorporateFinanceBreakdownPopup/_TermBreakdown.cshtml";

            if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                return RenderPartialViewToString(themeViewName, financeTerm);

            return RenderPartialViewToString($"{PLUGIN_URL}/Views/Corporate/Components/CorporateFinanceBreakdownPopup/_TermBreakdown.cshtml", financeTerm);
        }

        #endregion
    }
}