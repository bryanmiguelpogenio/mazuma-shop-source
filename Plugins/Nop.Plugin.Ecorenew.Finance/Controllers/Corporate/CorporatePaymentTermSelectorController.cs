﻿using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Services.Logging;
using System;
using Nop.Plugin.Ecorenew.Finance.Models.Corporate.Components;

namespace Nop.Plugin.Ecorenew.Finance.Controllers.Corporate
{
    public class CorporatePaymentTermSelectorController : BaseCorporateFinanceController
    {
        #region Fields

        private readonly ICorporateFinanceService _corporateFinanceService;
        private readonly ILogger _logger;

        #endregion

        #region Ctor

        public CorporatePaymentTermSelectorController(ICorporateFinanceService corporateFinanceService,
            ILogger logger)
        {
            this._corporateFinanceService = corporateFinanceService;
            this._logger = logger;
        }

        #endregion

        #region Methods

        [HttpPost]
        public virtual IActionResult SelectedPaymentModeChanged(string paymentModeId)
        {
            if (!_corporateFinanceService.CustomerCanUpdateShoppingCart())
            {
                throw new NopException("You are not allowed to update your shopping cart. Please complete your previous order first."); // TODO localization and add additional info on error message
            }

            if (string.IsNullOrWhiteSpace(paymentModeId) || !Int32.TryParse(paymentModeId, out int pmId))
                throw new NopException("Invalid payment mode.");

            _corporateFinanceService.ClearCustomerShoppingCartItemFinanceOptions();

            return Json(new { showFinanceOption = pmId == (int)CorporatePaymentMode.PayMonthly });
        }

        [HttpPost]
        public virtual IActionResult SelectedCorporateFinanceOptionChanged(string financeOptionId)
        {
            if (!_corporateFinanceService.CustomerCanUpdateShoppingCart())
            {
                throw new NopException("You are not allowed to update your shopping cart. Please complete your previous order first."); // TODO localization and add additional info on error message
            }

            if (string.IsNullOrWhiteSpace(financeOptionId) || !Int32.TryParse(financeOptionId, out int foId))
                throw new NopException("Invalid finance option."); // TODO localization

            try
            {
                _corporateFinanceService.MapCustomerShoppingCartItemsToFinanceOption(foId);

                return Json(null);
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while updating shopping cart item finance options.", ex); // TODO locatlzation
                throw new NopException("We encountered a problem with your request. Please try again later."); // TODO localization
            }
        }

        #endregion
    }
}