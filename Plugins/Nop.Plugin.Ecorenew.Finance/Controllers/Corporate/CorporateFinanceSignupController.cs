﻿using Ecorenew.Common.DocuSign.Services;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Ecorenew.Finance.Extensions;
using Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using System;
using System.Linq;

namespace Nop.Plugin.Ecorenew.Finance.Controllers.Corporate
{
    public class CorporateFinanceSignupController : BaseCorporateFinanceController
    {
        #region Fields

        private readonly ICorporateFinanceService _corporateFinanceService;
        private readonly ICountryService _countryService;
        private readonly ICustomerService _customerService;
        private readonly IDocuSignService _docuSignService;
        private readonly ILogger _logger;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public CorporateFinanceSignupController(ICorporateFinanceService corporateFinanceService,
            ICountryService countryService,
            ICustomerService customerService,
            IDocuSignService docuSignService,
            ILogger logger,
            IStateProvinceService stateProvinceService,
            IStoreContext storeContext,
            IWorkContext workContext)
        {
            this._corporateFinanceService = corporateFinanceService;
            this._countryService = countryService;
            this._customerService = customerService;
            this._docuSignService = docuSignService;
            this._logger = logger;
            this._stateProvinceService = stateProvinceService;
            this._storeContext = storeContext;
            this._workContext = workContext;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void PrepareBasicDetailsModel(BasicDetailsModel model, bool loadDefaultCustomerInfo = false)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            // Date Established - Day
            model.DateEstablishedDaySelectList.Clear();
            model.DateEstablishedDaySelectList.Add(new SelectListItem { Text = "Please Select...", Value = "" });
            for (int i = 1; i <= 31; i++)
            {
                var item = new SelectListItem { Value = i.ToString(), Text = i.ToString().PadLeft(2, '0') };
                model.DateEstablishedDaySelectList.Add(item);
            }

            // Date Established - Month
            model.DateEstablishedMonthSelectList.Clear();
            model.DateEstablishedMonthSelectList.Add(new SelectListItem { Text = "Please Select...", Value = "" });
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem { Value = i.ToString(), Text = new DateTime(1900, i, 1).ToString("MMMM") + " (" + i.ToString().PadLeft(2, '0') + ")" };
                model.DateEstablishedMonthSelectList.Add(item);
            }

            // Date Established - Year
            model.DateEstablishedYearSelectList.Clear();
            model.DateEstablishedYearSelectList.Add(new SelectListItem { Text = "Please Select...", Value = "" });
            for (int i = DateTime.UtcNow.Year - 18; i >= 1910; i--)
            {
                var item = new SelectListItem { Value = i.ToString(), Text = i.ToString() };
                model.DateEstablishedYearSelectList.Add(item);
            }

            // countries
            var countries = _countryService.GetAllCountries();

            model.CompanyAddress.CountrySelectList.Clear();
            model.CompanyAddress.CountrySelectList.Add(new SelectListItem { Text = "Select current country", Value = "" });
            model.CompanyAddress.CountrySelectList.AddRange(countries.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }));
            model.CompanyAddress.CountrySelectList.ForEach(x => x.Selected = x.Value == model.CompanyAddress.CountryId?.ToString());

            model.TradingAddress.CountrySelectList.Clear();
            model.TradingAddress.CountrySelectList.Add(new SelectListItem { Text = "Select previous country", Value = "" });
            model.TradingAddress.CountrySelectList.AddRange(countries.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }));
            model.TradingAddress.CountrySelectList.ForEach(x => x.Selected = x.Value == model.TradingAddress.CountryId?.ToString());

            model.CompanyAddress.StateProvinceSelectList.Clear();
            model.CompanyAddress.StateProvinceSelectList.Add(new SelectListItem { Text = "Please select...", Value = "" });

            if (model.CompanyAddress.CountryId != null)
            {
                model.CompanyAddress.StateProvinceSelectList.AddRange(_stateProvinceService.GetStateProvincesByCountryId(model.CompanyAddress.CountryId.Value)
                    .Select(x => new SelectListItem
                    {
                        Text = x.GetLocalized(s => s.Name),
                        Value = x.Id.ToString(),
                        Selected = model.CompanyAddress.StateProvinceId.HasValue && model.CompanyAddress.StateProvinceId.Value == x.Id
                    }));
            }

            model.TradingAddress.StateProvinceSelectList.Clear();
            model.TradingAddress.StateProvinceSelectList.Add(new SelectListItem { Text = "Please select..", Value = "" });

            if (model.TradingAddress.CountryId != null)
            {
                model.TradingAddress.StateProvinceSelectList.AddRange(_stateProvinceService.GetStateProvincesByCountryId(model.TradingAddress.CountryId.Value)
                    .Select(x => new SelectListItem
                    {
                        Text = x.GetLocalized(s => s.Name),
                        Value = x.Id.ToString(),
                        Selected = model.TradingAddress.StateProvinceId.HasValue && model.TradingAddress.StateProvinceId.Value == x.Id
                    }));
            }
        }

        [NonAction]
        protected virtual void PrepareDirectorDetailsModel(DirectorDetailsModel model, bool loadDefaultCustomerInfo = false)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            // Titles
            model.TitleSelectList.Clear();
            model.TitleSelectList.Add(new SelectListItem { Text = "Select Title", Value = "" });
            model.TitleSelectList.Add(new SelectListItem { Text = "Mr", Value = "Mr" });
            model.TitleSelectList.Add(new SelectListItem { Text = "Mrs", Value = "Mrs" });
            model.TitleSelectList.Add(new SelectListItem { Text = "Miss", Value = "Miss" });
            model.TitleSelectList.Add(new SelectListItem { Text = "Ms", Value = "Ms" });
            model.TitleSelectList.ForEach(t => t.Selected = t.Value == model.Title);

            // Birth - Day
            model.DateOfBirthDaySelectList.Clear();
            model.DateOfBirthDaySelectList.Add(new SelectListItem { Text = "Please Select...", Value = "" });
            for (int i = 1; i <= 31; i++)
            {
                var item = new SelectListItem { Value = i.ToString(), Text = i.ToString().PadLeft(2, '0') };
                model.DateOfBirthDaySelectList.Add(item);
            }

            // Birth - Month
            model.DateOfBirthMonthSelectList.Clear();
            model.DateOfBirthMonthSelectList.Add(new SelectListItem { Text = "Please Select...", Value = "" });
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem { Value = i.ToString(), Text = new DateTime(1900, i, 1).ToString("MMMM") + " (" + i.ToString().PadLeft(2, '0') + ")" };
                model.DateOfBirthMonthSelectList.Add(item);
            }

            // Birth - Year
            model.DateOfBirthYearSelectList.Clear();
            model.DateOfBirthYearSelectList.Add(new SelectListItem { Text = "Please Select...", Value = "" });
            for (int i = DateTime.UtcNow.Year - 18; i >= 1910; i--)
            {
                var item = new SelectListItem { Value = i.ToString(), Text = i.ToString() };
                model.DateOfBirthYearSelectList.Add(item);
            }

            // Marital statuses
            model.MaritalStatusSelectList.Clear();
            model.MaritalStatusSelectList.Add(new SelectListItem { Text = "Select Marital Status", Value = "" });
            model.MaritalStatusSelectList.AddRange(
                _corporateFinanceService.GetAllMaritalStatuses().Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() })
            );
            model.MaritalStatusSelectList.ForEach(t => t.Selected = t.Value == model.MaritalStatusId?.ToString());

            // Number of dependents
            model.NumberOfDependentsSelectList.Clear();
            model.NumberOfDependentsSelectList.Add(new SelectListItem { Text = "Select Number of dependents", Value = "" });
            model.NumberOfDependentsSelectList.AddRange(
                _corporateFinanceService.GetAllNumberOfDependents().Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() })
            );
            model.NumberOfDependentsSelectList.ForEach(t => t.Selected = t.Value == model.NumberOfDependentsId?.ToString());

            // home owner statuses
            model.HomeOwnerStatusSelectList.Clear();
            model.HomeOwnerStatusSelectList.Add(new SelectListItem { Text = "Select home owner status", Value = "" });
            model.HomeOwnerStatusSelectList.AddRange(
                _corporateFinanceService.GetAllHomeOwnerStatuses().Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() })
            );
            model.HomeOwnerStatusSelectList.ForEach(x => x.Selected = x.Value == model.HomeOwnerStatusId?.ToString());

            // countries
            var countries = _countryService.GetAllCountries();

            model.CurrentAddress.CountrySelectList.Clear();
            model.CurrentAddress.CountrySelectList.Add(new SelectListItem { Text = "Select current country", Value = "" });
            model.CurrentAddress.CountrySelectList.AddRange(countries.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }));
            model.CurrentAddress.CountrySelectList.ForEach(x => x.Selected = x.Value == model.CurrentAddress.CountryId?.ToString());

            model.PreviousAddress.CountrySelectList.Clear();
            model.PreviousAddress.CountrySelectList.Add(new SelectListItem { Text = "Select previous country", Value = "" });
            model.PreviousAddress.CountrySelectList.AddRange(countries.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }));
            model.PreviousAddress.CountrySelectList.ForEach(x => x.Selected = x.Value == model.PreviousAddress.CountryId?.ToString());

            if (loadDefaultCustomerInfo)
            {
                var currentCustomer = _workContext.CurrentCustomer;

                if (currentCustomer.IsRegistered() && currentCustomer.BillingAddress != null && currentCustomer.BillingAddress.CountryId.HasValue && countries.Select(x => x.Id).Contains(currentCustomer.BillingAddress.CountryId.Value))
                {
                    model.FirstName = currentCustomer.BillingAddress.FirstName;
                    model.LastName = currentCustomer.BillingAddress.LastName;
                    model.CurrentAddress.Address1 = currentCustomer.BillingAddress.Address1;
                    model.CurrentAddress.Address2 = currentCustomer.BillingAddress.Address2;
                    model.CurrentAddress.CityTown = currentCustomer.BillingAddress.City;
                    model.CurrentAddress.CountryId = currentCustomer.BillingAddress.CountryId;
                    model.CurrentAddress.Postcode = currentCustomer.BillingAddress.ZipPostalCode;
                }
                else if (countries.Count == 1)
                {
                    model.CurrentAddress.CountryId = countries.First().Id;
                }
            }

            model.CurrentAddress.StateProvinceSelectList.Clear();
            model.CurrentAddress.StateProvinceSelectList.Add(new SelectListItem { Text = "Please select...", Value = "" });

            if (model.CurrentAddress.CountryId != null)
            {
                model.CurrentAddress.StateProvinceSelectList.AddRange(_stateProvinceService.GetStateProvincesByCountryId(model.CurrentAddress.CountryId.Value)
                    .Select(x => new SelectListItem
                    {
                        Text = x.GetLocalized(s => s.Name),
                        Value = x.Id.ToString(),
                        Selected = model.CurrentAddress.StateProvinceId.HasValue && model.CurrentAddress.StateProvinceId.Value == x.Id
                    }));
            }

            model.PreviousAddress.StateProvinceSelectList.Clear();
            model.PreviousAddress.StateProvinceSelectList.Add(new SelectListItem { Text = "Please select..", Value = "" });

            if (model.PreviousAddress.CountryId != null)
            {
                model.PreviousAddress.StateProvinceSelectList.AddRange(_stateProvinceService.GetStateProvincesByCountryId(model.PreviousAddress.CountryId.Value)
                    .Select(x => new SelectListItem
                    {
                        Text = x.GetLocalized(s => s.Name),
                        Value = x.Id.ToString(),
                        Selected = model.PreviousAddress.StateProvinceId.HasValue && model.PreviousAddress.StateProvinceId.Value == x.Id
                    }));
            }
        }

        [NonAction]
        protected virtual void PrepareBankModel(BankModel model, CorporateFinanceBasicDetails basicDetails = null, CorporateFinanceDirectorDetails directorDetails = null)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            if (basicDetails != null)
            {
                if (string.IsNullOrWhiteSpace(model.CompanyName))
                    model.CompanyName = basicDetails.CompanyName;
                if (string.IsNullOrWhiteSpace(model.TradingName))
                    model.TradingName = basicDetails.TradingName;
            }

            if (directorDetails != null)
            {
                if (string.IsNullOrWhiteSpace(model.AccountName))
                    model.AccountName = directorDetails.FirstName + " " + directorDetails.LastName;
            }
        }

        #endregion

        #region Methods

        #region Index

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading corporate finance signup index page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        [HttpPost]
        [ValidateHoneypot]
        [PublicAntiForgery]
        public virtual IActionResult Index(string dummy)
        {
            try
            {
                if (!_corporateFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                return RedirectToAction("BasicDetails");
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading corporate finance signup index page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #region Basic Details

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult BasicDetails()
        {
            try
            {
                if (!_corporateFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                BasicDetailsModel model = null;

                // Load the saved basic details in case the customer goes back to this step
                var signup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();
                if (signup != null && !string.IsNullOrWhiteSpace(signup.CorporateFinanceBasicDetails))
                {
                    var basicDetails = _corporateFinanceService.GetSignupBasicDetails(signup);
                    if (basicDetails != null)
                        model = basicDetails.ToModel();
                }

                bool loadDefaultCustomerInfo = false;
                if (model == null)
                {
                    loadDefaultCustomerInfo = true;
                    model = new BasicDetailsModel();
                }

                PrepareBasicDetailsModel(model, loadDefaultCustomerInfo);

                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading corporate finance signup basic details page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        [HttpPost]
        [ValidateHoneypot]
        [PublicAntiForgery]
        public virtual IActionResult BasicDetails(BasicDetailsModel model)
        {
            try
            {
                if (!_corporateFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                if (!ModelState.IsValid)
                {
                    PrepareBasicDetailsModel(model);
                    return View(model);
                }

                // Create a new signup record if the current customer doesn't have an existing signup record 
                // or if the customer's latest signup has already been completed
                // else, just save the basic details
                var signup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();
                if (signup == null || signup.SignupCompletedOnUtc.HasValue)
                {
                    // Create new signup record
                    signup = _corporateFinanceService.CreateNewCorporateFinanceSignup();
                }

                _corporateFinanceService.SetSignupBasicDetails(signup.Id, model.ToEntity());

                return RedirectToAction("DirectorDetails");
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while saving corporate finance signup basic details", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #region Email Validation

        [HttpPost]
        public virtual IActionResult ValidateEmail(string emailAddress)
        {
            if (string.IsNullOrEmpty(emailAddress))
                throw new ArgumentNullException("emailAddress");

            // check if valid email address
            if (!_workContext.CurrentCustomer.IsRegistered() && CommonHelper.IsValidEmail(emailAddress))
            {
                var customer = _customerService.GetCustomerByEmail(emailAddress);

                if (customer == null || !customer.IsRegistered())
                {
                    return Json(new { passwordRequired = true });
                }
            }

            return Json(new { passwordRequired = false });
        }

        #endregion

        #region Director Details

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult DirectorDetails()
        {
            try
            {
                if (!_corporateFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if basic details is okay
                if (_corporateFinanceService.GetSignupBasicDetails(signup) == null)
                    return RedirectToAction("BasicDetails");

                DirectorDetailsModel model = null;

                // Load the saved director details in case the customer goes back to this step
                if (!string.IsNullOrWhiteSpace(signup.CorporateFinanceDirectorDetails))
                {
                    var directorDetails = _corporateFinanceService.GetSignupDirectorDetails(signup);
                    if (directorDetails != null)
                        model = directorDetails.ToModel();
                }

                bool loadDefaultCustomerInfo = false;
                if (model == null)
                {
                    loadDefaultCustomerInfo = true;
                    model = new DirectorDetailsModel();
                }

                PrepareDirectorDetailsModel(model, loadDefaultCustomerInfo);

                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading corporate finance signup director details page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        [HttpPost]
        [ValidateHoneypot]
        [PublicAntiForgery]
        [ParameterBasedOnFormName("prev", "goToPreviousStep")]
        public virtual IActionResult DirectorDetails(DirectorDetailsModel model, bool goToPreviousStep)
        {
            try
            {
                if (goToPreviousStep)
                    return RedirectToAction("BasicDetails");

                if (!_corporateFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if basic details is okay
                if (_corporateFinanceService.GetSignupBasicDetails(signup) == null)
                    return RedirectToAction("BasicDetails");

                if (!ModelState.IsValid)
                {
                    PrepareDirectorDetailsModel(model);
                    return View(model);
                }

                // Check if the customer is registered, request the customer to login using their registered account
                var customer = _customerService.GetCustomerByEmail(model.Email);
                if (customer != null && customer.Id != _workContext.CurrentCustomer.Id)
                {
                    WarningNotification("Please log in using your account.");

                    PrepareDirectorDetailsModel(model);
                    return View(model);
                }

                _corporateFinanceService.SetSignupDirectorDetails(signup.Id, model.ToEntity());

                return RedirectToAction("Bank");
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while saving corporate finance signup director details", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #region State / Provinces (County)

        [HttpPost]
        public ActionResult GetStateProvinces(int countryId)
        {
            var result = _stateProvinceService.GetStateProvincesByCountryId(countryId)
                .ToList()
                .Select(x => new
                {
                    Id = x.Id.ToString(),
                    Name = x.Name
                })
                .ToList();

            result.Insert(0, new { Id = "", Name = "Please select..." });

            return Json(result.ToArray());
        }

        #endregion

        #region Bank

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Bank()
        {
            try
            {
                if (!_corporateFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if basic details is okay
                var basicDetails = _corporateFinanceService.GetSignupBasicDetails(signup);
                if (basicDetails == null)
                    return RedirectToAction("BasicDetails");

                // Check if director details is okay
                var directorDetails = _corporateFinanceService.GetSignupDirectorDetails(signup);
                if (directorDetails == null)
                    return RedirectToAction("DirectorDetails");

                BankModel model = null;

                // Load the saved bank information in case the customer goes back to this step
                if (!string.IsNullOrWhiteSpace(signup.CorporateFinanceBankDetails))
                {
                    var bank = _corporateFinanceService.GetSignupBankDetails(signup);
                    if (bank != null)
                        model = bank.ToModel();
                }

                model = model ?? new BankModel();

                PrepareBankModel(model, basicDetails, directorDetails);

                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading corporate finance signup bank page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        [HttpPost]
        [ValidateHoneypot]
        [PublicAntiForgery]
        [ParameterBasedOnFormName("prev", "goToPreviousStep")]
        public virtual IActionResult Bank(BankModel model, bool goToPreviousStep)
        {
            try
            {
                if (goToPreviousStep)
                    return RedirectToAction("IncomeAndExpenditure");

                if (!_corporateFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if basic details is okay
                if (_corporateFinanceService.GetSignupBasicDetails(signup) == null)
                    return RedirectToAction("BasicDetails");

                // Check if director details is okay
                if (_corporateFinanceService.GetSignupDirectorDetails(signup) == null)
                    return RedirectToAction("DirectorDetails");

                if (!ModelState.IsValid)
                {
                    PrepareBankModel(model);
                    return View(model);
                }

                _corporateFinanceService.SetSignupBankDetails(signup.Id, model.ToEntity());

                return RedirectToAction("Authorization");
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while saving corporate finance signup bank", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #region Authorization

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Authorization()
        {
            try
            {
                if (!_corporateFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if basic details is okay
                if (_corporateFinanceService.GetSignupBasicDetails(signup) == null)
                    return RedirectToAction("BasicDetails");

                // Check if director details is okay
                if (_corporateFinanceService.GetSignupDirectorDetails(signup) == null)
                    return RedirectToAction("DirectorDetails");

                // Check if bank is okay
                if (_corporateFinanceService.GetSignupBankDetails(signup) == null)
                    return RedirectToAction("Bank");

                AuthorizationModel model = new AuthorizationModel();
                
                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading corporate finance signup authorization page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #region Signup Result

        [HttpPost]
        [ValidateHoneypot]
        [PublicAntiForgery]
        [ParameterBasedOnFormName("prev", "goToPreviousStep")]
        public virtual IActionResult SignupResult(AuthorizationModel model, bool goToPreviousStep)
        {
            try
            {
                if (goToPreviousStep)
                    return RedirectToAction("Bank");

                if (!_corporateFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if basic details is okay
                if (_corporateFinanceService.GetSignupBasicDetails(signup) == null)
                    return RedirectToAction("BasicDetails");

                // Check if director details is okay
                if (_corporateFinanceService.GetSignupDirectorDetails(signup) == null)
                    return RedirectToAction("DirectorDetails");

                // Check if bank is okay
                if (_corporateFinanceService.GetSignupBankDetails(signup) == null)
                    return RedirectToAction("Bank");

                if (!ModelState.IsValid)
                {
                    return RedirectToAction("Authorization");
                }

                var completedSignup = _corporateFinanceService.CompleteCorporateFinanceSignup();

                return View(); // TODO model
            }
            catch (NopException ex)
            {
                ErrorNotification(ex.Message);
                return RedirectToAction("Authorization");
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while saving corporate finance signup", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToAction("Authorization");
            }
        }

        #endregion

        #endregion
    }
}