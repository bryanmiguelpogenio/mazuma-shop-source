﻿using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Ecorenew.Finance.Models.Consumer.Components;
using Nop.Services.Logging;
using System;

namespace Nop.Plugin.Ecorenew.Finance.Controllers.Consumer
{
    public class ConsumerPaymentTermSelectorController : BaseConsumerFinanceController
    {
        #region Fields

        private readonly IConsumerFinanceService _consumerFinanceService;
        private readonly ILogger _logger;

        #endregion

        #region Ctor

        public ConsumerPaymentTermSelectorController(IConsumerFinanceService consumerFinanceService,
            ILogger logger)
        {
            this._consumerFinanceService = consumerFinanceService;
            this._logger = logger;
        }

        #endregion

        #region Methods

        [HttpPost]
        public virtual IActionResult SelectedPaymentModeChanged(string paymentModeId)
        {
            if (!_consumerFinanceService.CustomerCanUpdateShoppingCart())
            {
                throw new NopException("You are not allowed to update your shopping cart. Please complete your previous order first."); // TODO localization and add additional info on error message
            }

            if (string.IsNullOrWhiteSpace(paymentModeId) || !Int32.TryParse(paymentModeId, out int pmId))
                throw new NopException("Invalid payment mode.");

            _consumerFinanceService.ClearCustomerShoppingCartItemFinanceOptions();

            return Json(new { showFinanceOption = pmId == (int)ConsumerPaymentMode.PayMonthly });
        }

        [HttpPost]
        public virtual IActionResult SelectedConsumerFinanceOptionChanged(string financeOptionId)
        {
            if (!_consumerFinanceService.CustomerCanUpdateShoppingCart())
            {
                throw new NopException("You are not allowed to update your shopping cart. Please complete your previous order first."); // TODO localization and add additional info on error message
            }

            if (string.IsNullOrWhiteSpace(financeOptionId) || !Int32.TryParse(financeOptionId, out int foId))
                throw new NopException("Invalid finance option."); // TODO localization

            try
            {
                _consumerFinanceService.MapCustomerShoppingCartItemsToFinanceOption(foId);

                return Json(null);
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while updating shopping cart item finance options.", ex); // TODO locatlzation
                throw new NopException("We encountered a problem with your request. Please try again later."); // TODO localization
            }
        }

        #endregion
    }
}