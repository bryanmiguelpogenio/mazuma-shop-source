﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Themes;
using System;

namespace Nop.Plugin.Ecorenew.Finance.Controllers.Consumer
{
    public abstract class BaseConsumerFinanceController : BasePluginController
    {
        protected const string PLUGIN_URL = "~/Plugins/Ecorenew.Finance";

        /// <summary>
        /// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object that renders a view to the response.
        /// </summary>
        /// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object for the response.</returns>
        public override ViewResult View()
        {
            var controllerName = ControllerContext.ActionDescriptor.ControllerName;
            var actionName = ControllerContext.ActionDescriptor.ActionName;

            var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

            var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/{controllerName}/{actionName}.cshtml";

            if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                return base.View(themeViewName);

            return base.View($"{PLUGIN_URL}/Views/Consumer/{controllerName}/{actionName}.cshtml");
        }

        /// <summary>
        /// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object by specifying a <paramref name="viewName" />.
        /// </summary>
        /// <param name="viewName">The name of the view that is rendered to the response.</param>
        /// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object for the response.</returns>
        public override ViewResult View(string viewName)
        {
            if (viewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return base.View(viewName);
            }
            else
            {
                var controllerName = ControllerContext.ActionDescriptor.ControllerName;

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/{controllerName}/{viewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return base.View(themeViewName);

                return base.View($"{PLUGIN_URL}/Views/Consumer/{controllerName}/{viewName}.cshtml");
            }
        }

        /// <summary>
        /// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object by specifying a <paramref name="model" />
        /// to be rendered by the view.
        /// </summary>
        /// <param name="model">The model that is rendered by the view.</param>
        /// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object for the response.</returns>
        public override ViewResult View(object model)
        {
            var controllerName = ControllerContext.ActionDescriptor.ControllerName;
            var actionName = ControllerContext.ActionDescriptor.ActionName;

            var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

            var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/{controllerName}/{actionName}.cshtml";

            if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                return base.View(themeViewName, model);

            return base.View($"{PLUGIN_URL}/Views/Consumer/{controllerName}/{actionName}.cshtml", model);
        }

        /// <summary>
        /// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object by specifying a <paramref name="viewName" />
        /// and the <paramref name="model" /> to be rendered by the view.
        /// </summary>
        /// <param name="viewName">The name of the view that is rendered to the response.</param>
        /// <param name="model">The model that is rendered by the view.</param>
        /// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.ViewResult" /> object for the response.</returns>
        public override ViewResult View(string viewName, object model)
        {
            if (viewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return base.View(viewName, model);
            }
            else
            {
                var controllerName = ControllerContext.ActionDescriptor.ControllerName;

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themeViewName = $"{PLUGIN_URL}/Themes/{themeName}/Views/Consumer/{controllerName}/{viewName}.cshtml";

                if (System.IO.File.Exists(CommonHelper.MapPath(themeViewName)))
                    return base.View(themeViewName, model);

                return base.View($"{PLUGIN_URL}/Views/Consumer/{controllerName}/{viewName}.cshtml", model);
            }
        }

        /// <summary>
        /// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object that renders a partial view to the response.
        /// </summary>
        /// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object for the response.</returns>
        public override PartialViewResult PartialView()
        {
            var controllerName = ControllerContext.ActionDescriptor.ControllerName;
            var actionName = ControllerContext.ActionDescriptor.ActionName;

            var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

            var themePartialViewName = $"{PLUGIN_URL}/Themes/{themeName}/PartialViews/Consumer/{controllerName}/{actionName}.cshtml";

            if (System.IO.File.Exists(themePartialViewName))
                return base.PartialView(themePartialViewName);

            return base.PartialView($"{PLUGIN_URL}/PartialViews/Consumer/{controllerName}/{actionName}.cshtml");
        }

        /// <summary>
        /// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object by specifying a <paramref name="viewName" />.
        /// </summary>
        /// <param name="viewName">The name of the view that is rendered to the response.</param>
        /// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object for the response.</returns>
        public override PartialViewResult PartialView(string viewName)
        {
            if (viewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return base.PartialView(viewName);
            }
            else
            {
                var controllerName = ControllerContext.ActionDescriptor.ControllerName;

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themePartialViewName = $"{PLUGIN_URL}/Themes/{themeName}/PartialViews/Consumer/{controllerName}/{viewName}.cshtml";

                if (System.IO.File.Exists(themePartialViewName))
                    return base.PartialView(themePartialViewName);

                return base.PartialView($"{PLUGIN_URL}/PartialViews/Consumer/{controllerName}/{viewName}.cshtml");
            }
        }

        /// <summary>
        /// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object by specifying a <paramref name="model" />
        /// to be rendered by the partial view.
        /// </summary>
        /// <param name="model">The model that is rendered by the partial view.</param>
        /// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object for the response.</returns>
        public override PartialViewResult PartialView(object model)
        {
            var controllerName = ControllerContext.ActionDescriptor.ControllerName;
            var actionName = ControllerContext.ActionDescriptor.ActionName;

            var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

            var themePartialViewName = $"{PLUGIN_URL}/Themes/{themeName}/PartialViews/Consumer/{controllerName}/{actionName}.cshtml";

            if (System.IO.File.Exists(themePartialViewName))
                return base.PartialView(themePartialViewName, model);

            return base.PartialView($"{PLUGIN_URL}/PartialViews/Consumer/{controllerName}/{actionName}.cshtml", model);
        }

        /// <summary>
        /// Creates a <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object by specifying a <paramref name="viewName" />
        /// and the <paramref name="model" /> to be rendered by the partial view.
        /// </summary>
        /// <param name="viewName">The name of the partial view that is rendered to the response.</param>
        /// <param name="model">The model that is rendered by the partial view.</param>
        /// <returns>The created <see cref="T:Microsoft.AspNetCore.Mvc.PartialViewResult" /> object for the response.</returns>
        public override PartialViewResult PartialView(string viewName, object model)
        {
            if (viewName.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                return base.PartialView(viewName, model);
            }
            else
            {
                var controllerName = ControllerContext.ActionDescriptor.ControllerName;

                var themeName = EngineContext.Current.Resolve<IThemeContext>().WorkingThemeName;

                var themePartialViewName = $"{PLUGIN_URL}/Themes/{themeName}/PartialViews/Consumer/{controllerName}/{viewName}.cshtml";

                if (System.IO.File.Exists(themePartialViewName))
                    return base.PartialView(themePartialViewName, model);

                return base.PartialView($"{PLUGIN_URL}/PartialViews/Consumer/{controllerName}/{viewName}.cshtml", model);
            }
        }
    }
}