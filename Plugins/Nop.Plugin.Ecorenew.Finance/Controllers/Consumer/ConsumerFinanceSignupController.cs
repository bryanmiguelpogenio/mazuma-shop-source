﻿using Ecorenew.Common.DocuSign.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;
using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Nop.Plugin.Ecorenew.Finance.Extensions;
using Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using System;
using System.Linq;

namespace Nop.Plugin.Ecorenew.Finance.Controllers.Consumer
{
    public class ConsumerFinanceSignupController : BaseConsumerFinanceController
    {
        #region Fields

        private readonly IConsumerFinanceService _consumerFinanceService;
        private readonly ICountryService _countryService;
        private readonly ICustomerService _customerService;
        private readonly IDocuSignService _docuSignService;
        private readonly ILogger _logger;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public ConsumerFinanceSignupController(IConsumerFinanceService consumerFinanceService,
            ICountryService countryService,
            ICustomerService customerService,
            IDocuSignService docuSignService,
            ILogger logger,
            IStateProvinceService stateProvinceService,
            IStoreContext storeContext,
            IWorkContext workContext)
        {
            this._consumerFinanceService = consumerFinanceService;
            this._countryService = countryService;
            this._customerService = customerService;
            this._docuSignService = docuSignService;
            this._logger = logger;
            this._stateProvinceService = stateProvinceService;
            this._storeContext = storeContext;
            this._workContext = workContext;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void PreparePersonalInformationModel(PersonalInformationModel model, bool loadDefaultCustomerInfo = false)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            // Titles
            model.TitleSelectList.Clear();
            model.TitleSelectList.Add(new SelectListItem { Text = "Select Title", Value = "" });
            model.TitleSelectList.Add(new SelectListItem { Text = "Mr", Value = "Mr" });
            model.TitleSelectList.Add(new SelectListItem { Text = "Mrs", Value = "Mrs" });
            model.TitleSelectList.Add(new SelectListItem { Text = "Miss", Value = "Miss" });
            model.TitleSelectList.Add(new SelectListItem { Text = "Ms", Value = "Ms" });
            model.TitleSelectList.ForEach(t => t.Selected = t.Value == model.Title);

            // Birth - Day
            model.DateOfBirthDaySelectList.Clear();
            model.DateOfBirthDaySelectList.Add(new SelectListItem { Text = "Please Select...", Value = "" });
            for (int i = 1; i <= 31; i++)
            {
                var item = new SelectListItem { Value = i.ToString(), Text = i.ToString().PadLeft(2, '0') };
                model.DateOfBirthDaySelectList.Add(item);
            }

            // Birth - Month
            model.DateOfBirthMonthSelectList.Clear();
            model.DateOfBirthMonthSelectList.Add(new SelectListItem { Text = "Please Select...", Value = "" });
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem { Value = i.ToString(), Text = new DateTime(1900, i, 1).ToString("MMMM") + " (" + i.ToString().PadLeft(2, '0') + ")" };
                model.DateOfBirthMonthSelectList.Add(item);
            }

            // Birth - Year
            model.DateOfBirthYearSelectList.Clear();
            model.DateOfBirthYearSelectList.Add(new SelectListItem { Text = "Please Select...", Value = "" });
            for (int i = DateTime.UtcNow.Year - 18; i >= 1910; i--)
            {
                var item = new SelectListItem { Value = i.ToString(), Text = i.ToString() };
                model.DateOfBirthYearSelectList.Add(item);
            }

            // Marital statuses
            model.MaritalStatusSelectList.Clear();
            model.MaritalStatusSelectList.Add(new SelectListItem { Text = "Select Marital Status", Value = "" });
            model.MaritalStatusSelectList.AddRange(
                _consumerFinanceService.GetAllMaritalStatuses().Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() })
            );
            model.MaritalStatusSelectList.ForEach(t => t.Selected = t.Value == model.MaritalStatusId?.ToString());

            // Number of dependents
            model.NumberOfDependentsSelectList.Clear();
            model.NumberOfDependentsSelectList.Add(new SelectListItem { Text = "Select Number of dependents", Value = "" });
            model.NumberOfDependentsSelectList.AddRange(
                _consumerFinanceService.GetAllNumberOfDependents().Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() })
            );
            model.NumberOfDependentsSelectList.ForEach(t => t.Selected = t.Value == model.NumberOfDependentsId?.ToString());

            // Pre-load customer attributes if not yet provided
            var currentCustomer = _workContext.CurrentCustomer;

            if (loadDefaultCustomerInfo && currentCustomer.IsRegistered())
            {
                model.Email = currentCustomer.Email;

                if (currentCustomer.BillingAddress != null)
                {
                    model.FirstName = currentCustomer.BillingAddress.FirstName;
                    model.LastName = currentCustomer.BillingAddress.LastName;
                }
            }
        }

        [NonAction]
        protected virtual void PrepareHomeAddressModel(HomeAddressModel model, bool loadDefaultCustomerInfo = false)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            // home owner statuses
            model.HomeOwnerStatusSelectList.Clear();
            model.HomeOwnerStatusSelectList.Add(new SelectListItem { Text = "Select home owner status", Value = "" });
            model.HomeOwnerStatusSelectList.AddRange(
                _consumerFinanceService.GetAllHomeOwnerStatuses().Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() })
            );
            model.HomeOwnerStatusSelectList.ForEach(x => x.Selected = x.Value == model.HomeOwnerStatusId?.ToString());

            // countries
            var countries = _countryService.GetAllCountries();

            model.Current.CountrySelectList.Clear();
            model.Current.CountrySelectList.Add(new SelectListItem { Text = "Select current country", Value = "" });
            model.Current.CountrySelectList.AddRange(countries.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }));
            model.Current.CountrySelectList.ForEach(x => x.Selected = x.Value == model.Current.CountryId?.ToString());

            model.Previous.CountrySelectList.Clear();
            model.Previous.CountrySelectList.Add(new SelectListItem { Text = "Select previous country", Value = "" });
            model.Previous.CountrySelectList.AddRange(countries.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }));
            model.Previous.CountrySelectList.ForEach(x => x.Selected = x.Value == model.Previous.CountryId?.ToString());

            if (loadDefaultCustomerInfo)
            {
                var currentCustomer = _workContext.CurrentCustomer;

                if (currentCustomer.IsRegistered() && currentCustomer.BillingAddress != null && currentCustomer.BillingAddress.CountryId.HasValue && countries.Select(x => x.Id).Contains(currentCustomer.BillingAddress.CountryId.Value))
                {
                    model.Current.Address1 = currentCustomer.BillingAddress.Address1;
                    model.Current.Address2 = currentCustomer.BillingAddress.Address2;
                    model.Current.CityTown = currentCustomer.BillingAddress.City;
                    model.Current.CountryId = currentCustomer.BillingAddress.CountryId;
                    model.Current.Postcode = currentCustomer.BillingAddress.ZipPostalCode;
                }
                else if (countries.Count == 1)
                {
                    model.Current.CountryId = countries.First().Id;
                }
            }

            model.Current.StateProvinceSelectList.Clear();
            model.Current.StateProvinceSelectList.Add(new SelectListItem { Text = "Please select...", Value = "" });

            if (model.Current.CountryId != null)
            {
                model.Current.StateProvinceSelectList.AddRange(_stateProvinceService.GetStateProvincesByCountryId(model.Current.CountryId.Value)
                    .Select(x => new SelectListItem
                    {
                        Text = x.GetLocalized(s => s.Name),
                        Value = x.Id.ToString(),
                        Selected = model.Current.StateProvinceId.HasValue && model.Current.StateProvinceId.Value == x.Id
                    }));
            }

            model.Previous.StateProvinceSelectList.Clear();
            model.Previous.StateProvinceSelectList.Add(new SelectListItem { Text = "Please select..", Value = "" });

            if (model.Previous.CountryId != null)
            {
                model.Previous.StateProvinceSelectList.AddRange(_stateProvinceService.GetStateProvincesByCountryId(model.Previous.CountryId.Value)
                    .Select(x => new SelectListItem
                    {
                        Text = x.GetLocalized(s => s.Name),
                        Value = x.Id.ToString(),
                        Selected = model.Previous.StateProvinceId.HasValue && model.Previous.StateProvinceId.Value == x.Id
                    }));
            }
        }

        [NonAction]
        protected virtual void PrepareEmploymentModel(EmploymentModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            // Employment status
            model.EmploymentStatusList.Clear();
            model.EmploymentStatusList.Add(new SelectListItem { Text = "Select employment status", Value = "" });
            model.EmploymentStatusList.AddRange(
                _consumerFinanceService.GetAllEmploymentStatuses().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name })
            );
            model.EmploymentStatusList.ForEach(x => x.Selected = x.Value == model.EmploymentStatusId?.ToString());

            // Annual gross income
            model.AnnualGrossIncomeList.Clear();
            model.AnnualGrossIncomeList.Add(new SelectListItem { Text = "Select annual gross income", Value = "" });
            model.AnnualGrossIncomeList.AddRange(
                _consumerFinanceService.GetAllAnnualGrossIncome().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name })
            );
            model.AnnualGrossIncomeList.ForEach(x => x.Selected = x.Value == model.AnnualGrossIncomeId?.ToString());
        }

        [NonAction]
        protected virtual void PrepareIncomeAndExpenditureModel(IncomeAndExpenditureModel model, ConsumerFinanceSignup signup = null)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            ConsumerFinanceIncomeAndExpenditure entityFromDb = null;

            if (signup != null && !string.IsNullOrWhiteSpace(signup.ConsumerFinanceIncomeAndExpenditure))
                entityFromDb = _consumerFinanceService.GetSignupIncomeAndExpenditure(signup);

            if (entityFromDb != null)
                model = entityFromDb.ToModel(model);

            // Expenditure categories
            int? tempRangeId = null;
            var expenditureCategories = _consumerFinanceService.GetAllConsumerExpenditureCategories()
                .ToList()
                .Select(x => new IncomeAndExpenditureModel.ExpenditureCategoryRangeModel
                {
                    ExpenditureCategoryId = x.Id,
                    ExpenditureCategoryLocaleStringResourceName = x.LocaleStringResourceName,
                    ExpenditureCategoryDescription = x.Description,
                    ExpenditureRangeId = (tempRangeId = (
                                entityFromDb != null ?
                                entityFromDb.ConsumerExpenditures.SingleOrDefault(eer => eer.ExpenditureCategoryId == x.Id)?.ExpenditureRangeId :
                                model.ExpenditureCategoryRangeList.SingleOrDefault(mcr => mcr.ExpenditureCategoryId == x.Id)?.ExpenditureRangeId
                    )),
                    ExpenditureSelectList = x.AvailableRanges.Select(xr => new SelectListItem
                    {
                        Text = xr.ExpenditureRange.RangeDescription,
                        Value = xr.ExpenditureRangeId.ToString(),
                        Selected = xr.ExpenditureRangeId == tempRangeId
                    }).ToList(),
                    SpecificValueEnabled = tempRangeId.HasValue && tempRangeId.Value == x.AvailableRanges.Max(xr => xr.Id),
                    SpecificValue = (
                                entityFromDb != null ?
                                entityFromDb.ConsumerExpenditures.SingleOrDefault(eer => eer.ExpenditureCategoryId == x.Id)?.ConsumerExpenditureSpecificValueInGbp :
                                model.ExpenditureCategoryRangeList.SingleOrDefault(mcr => mcr.ExpenditureCategoryId == x.Id)?.SpecificValue)
                })
                .ToList();

            expenditureCategories.ForEach(c => c.ExpenditureSelectList.Insert(0, new SelectListItem { Text = "Please select...", Value = "" }));

            model.ExpenditureCategoryRangeList = expenditureCategories;
        }

        [NonAction]
        protected virtual void PrepareBankModel(BankModel model, ConsumerFinancePersonalInformation personalInformation = null)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            if (personalInformation != null)
            {
                model.AccountName = personalInformation.FirstName + " " + personalInformation.LastName;
            }
        }

        #endregion

        #region Methods

        #region Index

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading consumer finance signup index page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        [HttpPost]
        [ValidateHoneypot]
        [PublicAntiForgery]
        public virtual IActionResult Index(string dummy)
        {
            try
            {
                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                return RedirectToAction("PersonalInformation");
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading consumer finance signup index page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #region Personal Information

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult PersonalInformation()
        {
            try
            {
                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                PersonalInformationModel model = null;

                // Load the saved personal information in case the customer goes back to this step
                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup != null && !string.IsNullOrWhiteSpace(signup.ConsumerFinancePersonalInformation))
                {
                    var personalInformation = _consumerFinanceService.GetSignupPersonalInformation(signup);
                    if (personalInformation != null)
                        model = personalInformation.ToModel();
                }

                bool loadDefaultCustomerInfo = false;
                if (model == null)
                {
                    loadDefaultCustomerInfo = true;
                    model = new PersonalInformationModel();
                }

                PreparePersonalInformationModel(model, loadDefaultCustomerInfo);

                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading consumer finance signup personal information page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        [HttpPost]
        [ValidateHoneypot]
        [PublicAntiForgery]
        public virtual IActionResult PersonalInformation(PersonalInformationModel model)
        {
            try
            {
                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                if (!ModelState.IsValid)
                {
                    PreparePersonalInformationModel(model);
                    return View(model);
                }

                // Check if the customer is registered, request the customer to login using their registered account
                var customer = _customerService.GetCustomerByEmail(model.Email);
                if (customer != null && customer.Id != _workContext.CurrentCustomer.Id)
                {
                    WarningNotification("Please log in using your account.");

                    PreparePersonalInformationModel(model);
                    return View(model);
                }

                // Create a new signup record if the current customer doesn't have an existing signup record 
                // or if the customer's latest signup has already been completed
                // else, just save the personal information
                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup == null || signup.SignupCompletedOnUtc.HasValue)
                {
                    // Create new signup record
                    signup = _consumerFinanceService.CreateNewConsumerFinanceSignup();
                }

                _consumerFinanceService.SetSignupPersonalInformation(signup.Id, model.ToEntity());

                return RedirectToAction("HomeAddress");
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while saving consumer finance signup personal information", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #region Email Validation

        [HttpPost]
        public virtual IActionResult ValidateEmail(string emailAddress)
        {
            if (string.IsNullOrEmpty(emailAddress))
                throw new ArgumentNullException("emailAddress");

            // check if valid email address
            if (!_workContext.CurrentCustomer.IsRegistered() && CommonHelper.IsValidEmail(emailAddress))
            {
                var customer = _customerService.GetCustomerByEmail(emailAddress);

                if (customer == null || !customer.IsRegistered())
                {
                    return Json(new { passwordRequired = true });
                }
            }

            return Json(new { passwordRequired = false });
        }

        #endregion

        #region Home Address

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult HomeAddress()
        {
            try
            {
                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if personal information is okay
                if (_consumerFinanceService.GetSignupPersonalInformation(signup) == null)
                    return RedirectToAction("PersonalInformation");

                HomeAddressModel model = null;

                // Load the saved home address information in case the customer goes back to this step
                if (!string.IsNullOrWhiteSpace(signup.ConsumerFinanceHomeAddress))
                {
                    var homeAddress = _consumerFinanceService.GetSignupHomeAddress(signup);
                    if (homeAddress != null)
                        model = homeAddress.ToModel();
                }

                bool loadDefaultCustomerInfo = false;
                if (model == null)
                {
                    loadDefaultCustomerInfo = true;
                    model = new HomeAddressModel();
                }

                PrepareHomeAddressModel(model, loadDefaultCustomerInfo);

                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading consumer finance signup home address page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        [HttpPost]
        [ValidateHoneypot]
        [PublicAntiForgery]
        [ParameterBasedOnFormName("prev", "goToPreviousStep")]
        public virtual IActionResult HomeAddress(HomeAddressModel model, bool goToPreviousStep)
        {
            try
            {
                if (goToPreviousStep)
                    return RedirectToAction("PersonalInformation");

                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if personal information is okay
                if (_consumerFinanceService.GetSignupPersonalInformation(signup) == null)
                    return RedirectToAction("PersonalInformation");

                if (!ModelState.IsValid)
                {
                    PrepareHomeAddressModel(model);
                    return View(model);
                }

                _consumerFinanceService.SetSignupHomeAddress(signup.Id, model.ToEntity());

                return RedirectToAction("Employment");
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while saving consumer finance signup home address", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #region State / Provinces (County)

        [HttpPost]
        public ActionResult GetStateProvinces(int countryId)
        {
            var result = _stateProvinceService.GetStateProvincesByCountryId(countryId)
                .ToList()
                .Select(x => new
                {
                    Id = x.Id.ToString(),
                    Name = x.Name
                })
                .ToList();

            result.Insert(0, new { Id = "", Name = "Please select..." });

            return Json(result.ToArray());
        }

        #endregion

        #region Employment

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Employment()
        {
            try
            {
                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if personal information is okay
                if (_consumerFinanceService.GetSignupPersonalInformation(signup) == null)
                    return RedirectToAction("PersonalInformation");

                // Check if home address is okay
                if (_consumerFinanceService.GetSignupHomeAddress(signup) == null)
                    return RedirectToAction("HomeAddress");

                EmploymentModel model = null;

                // Load the saved employment information in case the customer goes back to this step
                if (!string.IsNullOrWhiteSpace(signup.ConsumerFinanceEmployment))
                {
                    var employment = _consumerFinanceService.GetSignupEmployment(signup);                    
                    if (employment != null )
                        model = employment.ToModel();
                }

                model = model ?? new EmploymentModel();

                PrepareEmploymentModel(model);

                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading consumer finance signup employment page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        [HttpPost]
        [ValidateHoneypot]
        [PublicAntiForgery]
        [ParameterBasedOnFormName("prev", "goToPreviousStep")]
        public virtual IActionResult Employment(EmploymentModel model, bool goToPreviousStep)
        {
            try
            {
                if (goToPreviousStep)
                    return RedirectToAction("HomeAddress");

                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if personal information is okay
                if (_consumerFinanceService.GetSignupPersonalInformation(signup) == null)
                    return RedirectToAction("PersonalInformation");

                // Check if home address is okay
                if (_consumerFinanceService.GetSignupHomeAddress(signup) == null)
                    return RedirectToAction("HomeAddress");

                if (!ModelState.IsValid)
                {
                    PrepareEmploymentModel(model);
                    return View(model);
                }

                _consumerFinanceService.SetSignupEmployment(signup.Id, model.ToEntity());

                return RedirectToAction("IncomeAndExpenditure");
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while saving consumer finance signup employment", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #region IncomeAndExpenditure

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult IncomeAndExpenditure()
        {
            try
            {
                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if personal information is okay
                if (_consumerFinanceService.GetSignupPersonalInformation(signup) == null)
                    return RedirectToAction("PersonalInformation");

                // Check if home address is okay
                if (_consumerFinanceService.GetSignupHomeAddress(signup) == null)
                    return RedirectToAction("HomeAddress");

                // Check if employment is okay
                if (_consumerFinanceService.GetSignupEmployment(signup) == null)
                    return RedirectToAction("Employment");

                IncomeAndExpenditureModel model = null;

                // Load the saved income and expenditure information in case the customer goes back to this step
                if (!string.IsNullOrWhiteSpace(signup.ConsumerFinanceIncomeAndExpenditure))
                {
                    var incomeAndExpenditure = _consumerFinanceService.GetSignupIncomeAndExpenditure(signup);
                    if (incomeAndExpenditure != null)
                        model = incomeAndExpenditure.ToModel();
                }

                model = model ?? new IncomeAndExpenditureModel();

                PrepareIncomeAndExpenditureModel(model, signup);

                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading consumer finance signup income and expenditure page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        [HttpPost]
        [ValidateHoneypot]
        [PublicAntiForgery]
        [ParameterBasedOnFormName("prev", "goToPreviousStep")]
        public virtual IActionResult IncomeAndExpenditure(IncomeAndExpenditureModel model, bool goToPreviousStep)
        {
            try
            {
                if (goToPreviousStep)
                    return RedirectToAction("Employment");

                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if personal information is okay
                if (_consumerFinanceService.GetSignupPersonalInformation(signup) == null)
                    return RedirectToAction("PersonalInformation");

                // Check if home address is okay
                if (_consumerFinanceService.GetSignupHomeAddress(signup) == null)
                    return RedirectToAction("HomeAddress");

                // Check if employment is okay
                if (_consumerFinanceService.GetSignupEmployment(signup) == null)
                    return RedirectToAction("Employment");

                if (!ModelState.IsValid)
                {
                    PrepareIncomeAndExpenditureModel(model);
                    return View(model);
                }

                _consumerFinanceService.SetSignupIncomeAndExpenditure(signup.Id, model.ToEntity());

                return RedirectToAction("Bank");
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while saving consumer finance signup income and expenditure", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #region Bank

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Bank()
        {
            try
            {
                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if personal information is okay
                var personalInformation = _consumerFinanceService.GetSignupPersonalInformation(signup);
                if (personalInformation == null)
                    return RedirectToAction("PersonalInformation");

                // Check if home address is okay
                if (_consumerFinanceService.GetSignupHomeAddress(signup) == null)
                    return RedirectToAction("HomeAddress");

                // Check if employment is okay
                if (_consumerFinanceService.GetSignupEmployment(signup) == null)
                    return RedirectToAction("Employment");

                // Check if income and expenditure is okay
                if (_consumerFinanceService.GetSignupIncomeAndExpenditure(signup) == null)
                    return RedirectToAction("IncomeAndExpenditure");

                BankModel model = null;

                // Load the saved bank information in case the customer goes back to this step
                if (!string.IsNullOrWhiteSpace(signup.ConsumerFinanceBank))
                {
                    var bank = _consumerFinanceService.GetSignupBank(signup);
                    if (bank != null)
                        model = bank.ToModel();
                }

                model = model ?? new BankModel();

                PrepareBankModel(model, personalInformation);

                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading consumer finance signup bank page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        [HttpPost]
        [ValidateHoneypot]
        [PublicAntiForgery]
        [ParameterBasedOnFormName("prev", "goToPreviousStep")]
        public virtual IActionResult Bank(BankModel model, bool goToPreviousStep)
        {
            try
            {
                if (goToPreviousStep)
                    return RedirectToAction("IncomeAndExpenditure");

                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if personal information is okay
                if (_consumerFinanceService.GetSignupPersonalInformation(signup) == null)
                    return RedirectToAction("PersonalInformation");

                // Check if home address is okay
                if (_consumerFinanceService.GetSignupHomeAddress(signup) == null)
                    return RedirectToAction("HomeAddress");

                // Check if employment is okay
                if (_consumerFinanceService.GetSignupEmployment(signup) == null)
                    return RedirectToAction("Employment");

                // Check if income and expenditure is okay
                if (_consumerFinanceService.GetSignupIncomeAndExpenditure(signup) == null)
                    return RedirectToAction("IncomeAndExpenditure");

                if (!ModelState.IsValid)
                {
                    PrepareBankModel(model);
                    return View(model);
                }

                _consumerFinanceService.SetSignupBank(signup.Id, model.ToEntity());

                return RedirectToAction("Authorization");
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while saving consumer finance signup bank", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #region Authorization

        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Authorization()
        {
            try
            {
                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if personal information is okay
                if (_consumerFinanceService.GetSignupPersonalInformation(signup) == null)
                    return RedirectToAction("PersonalInformation");

                // Check if home address is okay
                if (_consumerFinanceService.GetSignupHomeAddress(signup) == null)
                    return RedirectToAction("HomeAddress");

                // Check if employment is okay
                if (_consumerFinanceService.GetSignupEmployment(signup) == null)
                    return RedirectToAction("Employment");

                // Check if income and expenditure is okay
                if (_consumerFinanceService.GetSignupIncomeAndExpenditure(signup) == null)
                    return RedirectToAction("IncomeAndExpenditure");

                // Check if bank is okay
                if (_consumerFinanceService.GetSignupBank(signup) == null)
                    return RedirectToAction("Bank");

                AuthorizationModel model = new AuthorizationModel();
                
                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while loading consumer finance signup authorization page", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #region Signup Result

        [HttpPost]
        [ValidateHoneypot]
        [PublicAntiForgery]
        [ParameterBasedOnFormName("prev", "goToPreviousStep")]
        public virtual IActionResult SignupResult(AuthorizationModel model, bool goToPreviousStep)
        {
            try
            {
                if (goToPreviousStep)
                    return RedirectToAction("Bank");

                if (!_consumerFinanceService.CustomerCanSignup(out string notAllowedMessage))
                {
                    ErrorNotification(notAllowedMessage);
                    return RedirectToRoute("HomePage");
                }

                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup == null)
                    return RedirectToAction("Index");

                // Check if the latest signup has been completed.
                // If the previous signup has been completed, redirect to Index to create a new one since the customer is allowed to signup again
                if (signup.SignupCompletedOnUtc.HasValue)
                    return RedirectToAction("Index");

                // Check if personal information is okay
                if (_consumerFinanceService.GetSignupPersonalInformation(signup) == null)
                    return RedirectToAction("PersonalInformation");

                // Check if home address is okay
                if (_consumerFinanceService.GetSignupHomeAddress(signup) == null)
                    return RedirectToAction("HomeAddress");

                // Check if employment is okay
                if (_consumerFinanceService.GetSignupEmployment(signup) == null)
                    return RedirectToAction("Employment");

                // Check if income and expenditure is okay
                if (_consumerFinanceService.GetSignupIncomeAndExpenditure(signup) == null)
                    return RedirectToAction("IncomeAndExpenditure");

                // Check if bank is okay
                if (_consumerFinanceService.GetSignupBank(signup) == null)
                    return RedirectToAction("Bank");

                if (!ModelState.IsValid)
                {
                    return RedirectToAction("Authorization");
                }

                var completedSignup = _consumerFinanceService.CompleteConsumerFinanceSignup();

                if (completedSignup.SignupCompletedOnUtc.HasValue)
                {
                    if (completedSignup.AllChecksPassed)
                    {
                        var personalInformation = _consumerFinanceService.GetSignupPersonalInformation(completedSignup);

                        string docuSignToken = _docuSignService.RequestRecipientToken(
                            completedSignup.DocuSignEnvelopeId,
                            completedSignup.CustomerId.ToString(),
                            String.Join(" ", personalInformation.FirstName, personalInformation.LastName),
                            personalInformation.Email,
                            _storeContext.CurrentStore.Id);

                        return View("SignupPassed", docuSignToken);
                    }
                    else
                    {
                        return View("SignupFailed");
                    }
                }
                else if (!completedSignup.CallReportPassed.HasValue)
                {
                    // TODO CallReport picklist
                    return View("CallReportPicklist");
                }

                // TODO Now what?
                return View("");
            }
            catch (NopException ex)
            {
                ErrorNotification(ex.Message);
                return RedirectToAction("Authorization");
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected error occurred while saving consumer finance signup", ex);
                ErrorNotification("We encountered a problem with your application. Please try again later.");
                return RedirectToRoute("HomePage");
            }
        }

        #endregion

        #endregion
    }
}