﻿using System.Collections.Generic;
using System.Linq;
using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.Vendors;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Shipping.Date;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Factories;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Models.Catalog;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;

namespace Nop.Plugin.Ecorenew.Finance.Factories
{
    public class FinanceProductModelFactory : ProductModelFactory
    {
        #region Fields

        private readonly IPriceFormatter _priceFormatter;
        private readonly IConsumerFinanceCalculationService _consumerFinanceCalculationService;
        private readonly ICorporateFinanceCalculationService _corporateFinanceCalculationService;
        private readonly FinanceSettings _financeSettings;

        #endregion

        #region Ctor

        public FinanceProductModelFactory(ISpecificationAttributeService specificationAttributeService, 
            ICategoryService categoryService, 
            IManufacturerService manufacturerService, 
            IProductService productService, 
            IVendorService vendorService, 
            IProductTemplateService productTemplateService, 
            IProductAttributeService productAttributeService, 
            IWorkContext workContext, 
            IStoreContext storeContext, 
            ITaxService taxService, 
            ICurrencyService currencyService, 
            IPictureService pictureService, 
            ILocalizationService localizationService, 
            IMeasureService measureService, 
            IPriceCalculationService priceCalculationService, 
            IPriceFormatter priceFormatter, 
            IWebHelper webHelper, 
            IDateTimeHelper dateTimeHelper, 
            IProductTagService productTagService, 
            IAclService aclService, 
            IStoreMappingService storeMappingService, 
            IPermissionService permissionService, 
            IDownloadService downloadService, 
            IProductAttributeParser productAttributeParser, 
            IDateRangeService dateRangeService, 
            IConsumerFinanceCalculationService consumerFinanceCalculationService,
            ICorporateFinanceCalculationService corporateFinanceCalculationService,
            MediaSettings mediaSettings, 
            CatalogSettings catalogSettings, 
            VendorSettings vendorSettings, 
            CustomerSettings customerSettings, 
            CaptchaSettings captchaSettings, 
            OrderSettings orderSettings, 
            SeoSettings seoSettings, 
            FinanceSettings financeSettings,
            IStaticCacheManager cacheManager) 
            : base(specificationAttributeService, 
                  categoryService, 
                  manufacturerService, 
                  productService, 
                  vendorService, 
                  productTemplateService, 
                  productAttributeService, 
                  workContext, 
                  storeContext, 
                  taxService, 
                  currencyService, 
                  pictureService, 
                  localizationService, 
                  measureService, 
                  priceCalculationService, 
                  priceFormatter, 
                  webHelper, 
                  dateTimeHelper, 
                  productTagService, 
                  aclService, 
                  storeMappingService, 
                  permissionService, 
                  downloadService, 
                  productAttributeParser, 
                  dateRangeService, 
                  mediaSettings, 
                  catalogSettings, 
                  vendorSettings, 
                  customerSettings, 
                  captchaSettings, 
                  orderSettings, 
                  seoSettings, 
                  cacheManager)
        {
            this._priceFormatter = priceFormatter;
            this._consumerFinanceCalculationService = consumerFinanceCalculationService;
            this._corporateFinanceCalculationService = corporateFinanceCalculationService;
            this._financeSettings = financeSettings;
        }

        #endregion

        #region Utilities

        protected virtual void PrepareConsumerProductOverviewModels(IEnumerable<Product> products, IEnumerable<ProductOverviewModel> models)
        {
            foreach (var model in models)
            {
                var financeValues = _consumerFinanceCalculationService.ComputeProductConsumerFinanceValues(products.First(p => p.Id == model.Id), null);
                if (financeValues != null && financeValues.TermsBreakdown != null && financeValues.TermsBreakdown.Count > 0)
                {
                    var lowestMonthly = financeValues.TermsBreakdown.Min(t => t.MonthlyPayment);

                    if (lowestMonthly > 0)
                        model.ProductPrice.Price = $"From {_priceFormatter.FormatPrice(lowestMonthly)} per month, plus initial payment";
                }
            }
        }

        protected virtual void PrepareCorporateProductOverviewModels(IEnumerable<Product> products, IEnumerable<ProductOverviewModel> models)
        {
            foreach (var model in models)
            {
                var financeValues = _corporateFinanceCalculationService.ComputeProductCorporateFinanceValues(products.First(p => p.Id == model.Id), null);
                var lowestMonthly = financeValues.TermsBreakdown.Min(t => t.MonthlyPayment);

                if (lowestMonthly > 0)
                    model.ProductPrice.Price = $"From {_priceFormatter.FormatPrice(lowestMonthly)} per month";
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare the product overview models
        /// </summary>
        /// <param name="products">Collection of products</param>
        /// <param name="preparePriceModel">Whether to prepare the price model</param>
        /// <param name="preparePictureModel">Whether to prepare the picture model</param>
        /// <param name="productThumbPictureSize">Product thumb picture size (longest side); pass null to use the default value of media settings</param>
        /// <param name="prepareSpecificationAttributes">Whether to prepare the specification attribute models</param>
        /// <param name="forceRedirectionAfterAddingToCart">Whether to force redirection after adding to cart</param>
        /// <returns>Collection of product overview model</returns>
        public override IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(IEnumerable<Product> products, bool preparePriceModel = true, bool preparePictureModel = true, int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false, bool forceRedirectionAfterAddingToCart = false)
        {
            var models = base.PrepareProductOverviewModels(products, preparePriceModel, preparePictureModel, productThumbPictureSize, prepareSpecificationAttributes, forceRedirectionAfterAddingToCart);

            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        PrepareConsumerProductOverviewModels(products, models);
                        break;
                    case FinanceType.Corporate:
                        PrepareCorporateProductOverviewModels(products, models);
                        break;
                }
            }

            return models;
        }

        #endregion
    }
}