﻿using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Web.Factories;
using Nop.Web.Models.Order;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;

namespace Nop.Plugin.Ecorenew.Finance.Factories
{
    public class FinanceOrderModelFactory : OrderModelFactory
    {
        #region Fields

        private readonly IWorkContext _workContext;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IConsumerFinanceService _consumerFinanceService;
        private readonly ICorporateFinanceService _corporateFinanceService;
        private readonly FinanceSettings _financeSettings;

        #endregion

        #region Ctor

        public FinanceOrderModelFactory(IAddressModelFactory addressModelFactory, 
            IOrderService orderService, 
            IWorkContext workContext, 
            ICurrencyService currencyService, 
            IPriceFormatter priceFormatter, 
            IOrderProcessingService orderProcessingService, 
            IDateTimeHelper dateTimeHelper, 
            IPaymentService paymentService, 
            ILocalizationService localizationService, 
            IShippingService shippingService, 
            ICountryService countryService, 
            IProductAttributeParser productAttributeParser, 
            IDownloadService downloadService, 
            IStoreContext storeContext, 
            IOrderTotalCalculationService orderTotalCalculationService, 
            IRewardPointService rewardPointService, 
            IConsumerFinanceService consumerFinanceService,
            ICorporateFinanceService corporateFinanceService,
            CatalogSettings catalogSettings, 
            OrderSettings orderSettings, 
            TaxSettings taxSettings, 
            ShippingSettings shippingSettings, 
            AddressSettings addressSettings, 
            RewardPointsSettings rewardPointsSettings, 
            PdfSettings pdfSettings,
            FinanceSettings financeSettings)
            : base(addressModelFactory, 
                  orderService, 
                  workContext, 
                  currencyService, 
                  priceFormatter, 
                  orderProcessingService, 
                  dateTimeHelper, 
                  paymentService, 
                  localizationService, 
                  shippingService, 
                  countryService, 
                  productAttributeParser, 
                  downloadService, 
                  storeContext, 
                  orderTotalCalculationService, 
                  rewardPointService, 
                  catalogSettings, 
                  orderSettings, 
                  taxSettings, 
                  shippingSettings, 
                  addressSettings, 
                  rewardPointsSettings, 
                  pdfSettings)
        {
            this._workContext = workContext;
            this._currencyService = currencyService;
            this._priceFormatter = priceFormatter;
            this._consumerFinanceService = consumerFinanceService;
            this._corporateFinanceService = corporateFinanceService;
            this._financeSettings = financeSettings;
        }

        #endregion

        #region Utilities

        protected virtual void PrepareConsumerOrderDetailsModel(Order order, OrderDetailsModel model)
        {
            decimal monthlySubTotal = 0;
            var orderCurrency = _currencyService.GetCurrencyByCode(order.CustomerCurrencyCode);

            foreach (var item in model.Items)
            {
                var consumerFinanceOrderItem = _consumerFinanceService.GetConsumerFinanceOrderItemByOrderItemId(item.Id);
                if (consumerFinanceOrderItem != null)
                {
                    var orderItemCurrency = _currencyService.GetCurrencyByCode(consumerFinanceOrderItem.CurrencyCode);
                    var monthlyPaymentValue = _currencyService.ConvertCurrency(consumerFinanceOrderItem.MonthlyPayment, orderItemCurrency, orderCurrency);
                    var monthlyPaymentSubTotalValue = monthlyPaymentValue * consumerFinanceOrderItem.ProductQuantity;
                    monthlySubTotal += monthlyPaymentSubTotalValue;

                    var monthlyPayment = _priceFormatter.FormatPrice(monthlyPaymentValue, true, orderCurrency, _workContext.WorkingLanguage, true);
                    var monthlyPaymentSubTotal = _priceFormatter.FormatPrice(consumerFinanceOrderItem.MonthlyPayment * consumerFinanceOrderItem.ProductQuantity, true, orderItemCurrency, _workContext.WorkingLanguage, true);

                    item.UnitPrice += $" Initial Payment ({monthlyPayment} Monthly)"; // TODO localization
                    item.SubTotal += $" Initial Payment ({monthlyPaymentSubTotal} Monthly)"; // TODO localization
                }
            }

            if (monthlySubTotal > 0)
            {
                model.OrderSubtotal += $" Initial Payment ({_priceFormatter.FormatPrice(monthlySubTotal, true, orderCurrency, _workContext.WorkingLanguage, true)} Monthly)"; // TODO localization
            }
        }

        protected virtual void PrepareCorporateOrderDetailsModel(Order order, OrderDetailsModel model)
        {
            decimal monthlySubTotal = 0;
            var orderCurrency = _currencyService.GetCurrencyByCode(order.CustomerCurrencyCode);

            foreach (var item in model.Items)
            {
                var corporateFinanceOrderItem = _corporateFinanceService.GetCorporateFinanceOrderItemByOrderItemId(item.Id);
                if (corporateFinanceOrderItem != null)
                {
                    var orderItemCurrency = _currencyService.GetCurrencyByCode(corporateFinanceOrderItem.CurrencyCode);
                    var monthlyPaymentValue = _currencyService.ConvertCurrency(corporateFinanceOrderItem.MonthlyPayment, orderItemCurrency, orderCurrency);
                    var monthlyPaymentSubTotalValue = monthlyPaymentValue * corporateFinanceOrderItem.ProductQuantity;
                    monthlySubTotal += monthlyPaymentSubTotalValue;
                    
                    item.UnitPrice += " Monthly"; // TODO localization
                    item.SubTotal += " Monthly"; // TODO localization
                }
            }

            if (monthlySubTotal > 0)
            {
                model.OrderSubtotal += " Monthly"; // TODO localization
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare the order details model
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Order details model</returns>
        public override OrderDetailsModel PrepareOrderDetailsModel(Order order)
        {
            var model = base.PrepareOrderDetailsModel(order);

            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        PrepareConsumerOrderDetailsModel(order, model);
                        break;
                    case FinanceType.Corporate:
                        PrepareCorporateOrderDetailsModel(order, model);
                        break;
                }
            }

            return model;
        }

        #endregion
    }
}