﻿using System.Collections.Generic;
using System.Linq;
using Ecorenew.Common.DocuSign.Services;
using Ecorenew.Common.DocuSignWeb;
using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Microsoft.AspNetCore.Http;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Core.Plugins;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Web.Factories;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Models.ShoppingCart;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;

namespace Nop.Plugin.Ecorenew.Finance.Factories
{
    public class FinanceShoppingCartModelFactory : ShoppingCartModelFactory
    {
        #region Fields

        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IPluginFinder _pluginFinder;
        private readonly IConsumerFinanceService _consumerFinanceService;
        private readonly IConsumerFinanceCalculationService _consumerFinanceCalculationService;
        private readonly ICorporateFinanceService _corporateFinanceService;
        private readonly ICorporateFinanceCalculationService _corporateFinanceCalculationService;
        private readonly IDocuSignService _docuSignService;
        private readonly FinanceSettings _financeSettings;

        #endregion

        #region Ctor

        public FinanceShoppingCartModelFactory(IAddressModelFactory addressModelFactory,
            IStoreContext storeContext,
            IWorkContext workContext,
            IShoppingCartService shoppingCartService,
            IPictureService pictureService,
            ILocalizationService localizationService,
            IProductService productService,
            IProductAttributeFormatter productAttributeFormatter,
            IProductAttributeParser productAttributeParser,
            ITaxService taxService,
            ICurrencyService currencyService,
            IPriceCalculationService priceCalculationService,
            IPriceFormatter priceFormatter,
            ICheckoutAttributeParser checkoutAttributeParser,
            ICheckoutAttributeFormatter checkoutAttributeFormatter,
            IOrderProcessingService orderProcessingService,
            IDiscountService discountService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IShippingService shippingService,
            IOrderTotalCalculationService orderTotalCalculationService,
            ICheckoutAttributeService checkoutAttributeService,
            IPaymentService paymentService,
            IPermissionService permissionService,
            IDownloadService downloadService,
            IStaticCacheManager cacheManager,
            IWebHelper webHelper,
            IGenericAttributeService genericAttributeService,
            IHttpContextAccessor httpContextAccessor,
            IPluginFinder pluginFinder,
            IConsumerFinanceService consumerFinanceService,
            IConsumerFinanceCalculationService consumerFinanceCalculationService,
            ICorporateFinanceService corporateFinanceService,
            ICorporateFinanceCalculationService corporateFinanceCalculationService,
            IDocuSignService docuSignService,
            MediaSettings mediaSettings,
            ShoppingCartSettings shoppingCartSettings,
            CatalogSettings catalogSettings,
            CommonSettings commonSettings,
            OrderSettings orderSettings,
            ShippingSettings shippingSettings,
            TaxSettings taxSettings,
            CaptchaSettings captchaSettings,
            AddressSettings addressSettings,
            RewardPointsSettings rewardPointsSettings,
            CustomerSettings customerSettings,
            FinanceSettings financeSettings)
            : base(addressModelFactory,
                  storeContext,
                  workContext,
                  shoppingCartService,
                  pictureService,
                  localizationService,
                  productService,
                  productAttributeFormatter,
                  productAttributeParser,
                  taxService,
                  currencyService,
                  priceCalculationService,
                  priceFormatter,
                  checkoutAttributeParser,
                  checkoutAttributeFormatter,
                  orderProcessingService,
                  discountService,
                  countryService,
                  stateProvinceService,
                  shippingService,
                  orderTotalCalculationService,
                  checkoutAttributeService,
                  paymentService,
                  permissionService,
                  downloadService,
                  cacheManager,
                  webHelper,
                  genericAttributeService,
                  httpContextAccessor,
                  mediaSettings,
                  shoppingCartSettings,
                  catalogSettings,
                  commonSettings,
                  orderSettings,
                  shippingSettings,
                  taxSettings,
                  captchaSettings,
                  addressSettings,
                  rewardPointsSettings,
                  customerSettings)
        {
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._priceFormatter = priceFormatter;
            this._pluginFinder = pluginFinder;
            this._consumerFinanceService = consumerFinanceService;
            this._consumerFinanceCalculationService = consumerFinanceCalculationService;
            this._corporateFinanceService = corporateFinanceService;
            this._corporateFinanceCalculationService = corporateFinanceCalculationService;
            this._docuSignService = docuSignService;
            this._financeSettings = financeSettings;
        }

        #endregion

        #region Utilities

        protected virtual void PrepareConsumerOrderTotalsModel(OrderTotalsModel model)
        {
            var shoppingCartItemFinanceOptions = _consumerFinanceService.GetCustomerShoppingCartItemConsumerFinanceOptions().ToList();
            if (shoppingCartItemFinanceOptions.Any())
            {
                model.SubTotal += " Initial Payment"; // TODO localization

                decimal monthlySubTotal = 0;

                foreach (var shoppingCartItemFinanceOption in shoppingCartItemFinanceOptions)
                {
                    var shoppingCartItem = _workContext.CurrentCustomer.ShoppingCartItems.Single(s => s.Id == shoppingCartItemFinanceOption.ShoppingCartItemId);
                    var financeValues = _consumerFinanceCalculationService.ComputeProductConsumerFinanceValues(shoppingCartItem.Product, shoppingCartItem.AttributesXml);
                    var selectedTerm = financeValues.TermsBreakdown.FirstOrDefault(s => s.DurationInMonths == shoppingCartItemFinanceOption.ConsumerFinanceOption.DurationInMonths);
                    monthlySubTotal += selectedTerm.MonthlyPayment * shoppingCartItem.Quantity;
                }

                if (monthlySubTotal > 0)
                    model.SubTotal += $" ({_priceFormatter.FormatPrice(RoundingHelper.RoundPrice(monthlySubTotal))} Monthly)"; // TODO localization
            }
        }

        protected virtual void PrepareCorporateOrderTotalsModel(OrderTotalsModel model)
        {
            var shoppingCartItemFinanceOptions = _corporateFinanceService.GetCustomerShoppingCartItemCorporateFinanceOptions().ToList();
            if (shoppingCartItemFinanceOptions.Any())
            {
                decimal monthlySubTotal = 0;

                foreach (var shoppingCartItemFinanceOption in shoppingCartItemFinanceOptions)
                {
                    var shoppingCartItem = _workContext.CurrentCustomer.ShoppingCartItems.Single(s => s.Id == shoppingCartItemFinanceOption.ShoppingCartItemId);
                    var financeValues = _corporateFinanceCalculationService.ComputeProductCorporateFinanceValues(shoppingCartItem.Product, shoppingCartItem.AttributesXml);
                    var selectedTerm = financeValues.TermsBreakdown.FirstOrDefault(s => s.DurationInMonths == shoppingCartItemFinanceOption.CorporateFinanceOption.DurationInMonths);

                    monthlySubTotal += selectedTerm.MonthlyPayment * shoppingCartItem.Quantity;
                }

                if (monthlySubTotal > 0)
                    model.SubTotal += " Monthly"; // TODO localization
            }
        }

        protected virtual void PrepareConsumerMiniShoppingCartModel(MiniShoppingCartModel model)
        {
            var shoppingCartItemFinanceOptions = _consumerFinanceService.GetCustomerShoppingCartItemConsumerFinanceOptions();
            if (shoppingCartItemFinanceOptions.Any())
            {
                foreach (var item in model.Items)
                {
                    item.UnitPrice += " Initial Payment"; // TODO localization

                    var itemFinanceOption = shoppingCartItemFinanceOptions.FirstOrDefault(s => s.ShoppingCartItemId == item.Id);
                    if (itemFinanceOption != null)
                    {
                        var shoppingCartItem = _workContext.CurrentCustomer.ShoppingCartItems.Single(s => s.Id == item.Id);
                        var financeValues = _consumerFinanceCalculationService.ComputeProductConsumerFinanceValues(shoppingCartItem.Product, shoppingCartItem.AttributesXml);
                        var selectedTerm = financeValues.TermsBreakdown.FirstOrDefault(s => s.DurationInMonths == itemFinanceOption.ConsumerFinanceOption.DurationInMonths);
                        string monthlyPayment = _priceFormatter.FormatPrice(RoundingHelper.RoundPrice(selectedTerm.MonthlyPayment * shoppingCartItem.Quantity));

                        item.UnitPrice += $" ({monthlyPayment} Monthly)"; // TODO localization
                    }
                }
            }
        }

        protected virtual void PrepareCorporateMiniShoppingCartModel(MiniShoppingCartModel model)
        {
            var shoppingCartItemFinanceOptions = _corporateFinanceService.GetCustomerShoppingCartItemCorporateFinanceOptions();
            if (shoppingCartItemFinanceOptions.Any())
            {
                foreach (var item in model.Items)
                {
                    var itemFinanceOption = shoppingCartItemFinanceOptions.FirstOrDefault(s => s.ShoppingCartItemId == item.Id);
                    if (itemFinanceOption != null)
                    {
                        item.UnitPrice += " Monthly"; // TODO localization
                    }
                }
            }
        }

        protected virtual void PrepareConsumerShoppingCartModel(ShoppingCartModel model, ShoppingCartModel newModel)
        {
            var shoppingCartItemFinanceOptions = _consumerFinanceService.GetCustomerShoppingCartItemConsumerFinanceOptions();
            if (shoppingCartItemFinanceOptions.Any())
            {
                foreach (var item in newModel.Items)
                {
                    item.UnitPrice += " Initial Payment"; // TODO localization

                    var itemFinanceOption = shoppingCartItemFinanceOptions.FirstOrDefault(s => s.ShoppingCartItemId == item.Id);
                    if (itemFinanceOption != null)
                    {
                        var shoppingCartItem = _workContext.CurrentCustomer.ShoppingCartItems.Single(s => s.Id == item.Id);
                        var financeValues = _consumerFinanceCalculationService.ComputeProductConsumerFinanceValues(shoppingCartItem.Product, shoppingCartItem.AttributesXml);
                        var selectedTerm = financeValues.TermsBreakdown.FirstOrDefault(s => s.DurationInMonths == itemFinanceOption.ConsumerFinanceOption.DurationInMonths);
                        string monthlyPayment = _priceFormatter.FormatPrice(RoundingHelper.RoundPrice(selectedTerm.MonthlyPayment));
                        string monthlyPaymentSubTotal = _priceFormatter.FormatPrice(RoundingHelper.RoundPrice(selectedTerm.MonthlyPayment * shoppingCartItem.Quantity));

                        item.UnitPrice += $" ({monthlyPayment} Monthly)"; // TODO localization
                        item.SubTotal += $" ({monthlyPaymentSubTotal} Monthly)"; // TODO localization
                    }
                }

                model.HideCheckoutButton = true;

                var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();
                if (signup != null && signup.SignupCompletedOnUtc.HasValue && signup.AllChecksPassed && _docuSignService.RequestEnvelopeStatus(signup.DocuSignEnvelopeId).Status == EnvelopeStatusCode.Completed && !signup.OrderId.HasValue)
                    model.HideCheckoutButton = false;
            }
        }

        protected virtual void PrepareCorporateShoppingCartModel(ShoppingCartModel model, ShoppingCartModel newModel)
        {
            var shoppingCartItemFinanceOptions = _corporateFinanceService.GetCustomerShoppingCartItemCorporateFinanceOptions();
            if (shoppingCartItemFinanceOptions.Any())
            {
                foreach (var item in newModel.Items)
                {
                    var itemFinanceOption = shoppingCartItemFinanceOptions.FirstOrDefault(s => s.ShoppingCartItemId == item.Id);
                    if (itemFinanceOption != null)
                    {
                        item.UnitPrice += " Monthly"; // TODO localization
                        item.SubTotal += " Monthly"; // TODO localization
                    }
                }

                model.HideCheckoutButton = true;

                var signup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();
                if (signup != null && signup.SignupCompletedOnUtc.HasValue && !string.IsNullOrWhiteSpace(signup.DocuSignEnvelopeId) && _docuSignService.RequestEnvelopeStatus(signup.DocuSignEnvelopeId).Status == EnvelopeStatusCode.Completed && !signup.OrderId.HasValue)
                    model.HideCheckoutButton = false;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare the order totals model
        /// </summary>
        /// <param name="cart">List of the shopping cart item</param>
        /// <param name="isEditable">Whether model is editable</param>
        /// <returns>Order totals model</returns>
        public override OrderTotalsModel PrepareOrderTotalsModel(IList<ShoppingCartItem> cart, bool isEditable)
        {
            var model = base.PrepareOrderTotalsModel(cart, isEditable);

            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        PrepareConsumerOrderTotalsModel(model);
                        break;
                    case FinanceType.Corporate:
                        PrepareCorporateOrderTotalsModel(model);
                        break;
                }
            }

            return model;
        }

        /// <summary>
        /// Prepare the mini shopping cart model
        /// </summary>
        /// <returns>Mini shopping cart model</returns>
        public override MiniShoppingCartModel PrepareMiniShoppingCartModel()
        {
            var model = base.PrepareMiniShoppingCartModel();

            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        PrepareConsumerMiniShoppingCartModel(model);
                        break;
                    case FinanceType.Corporate:
                        PrepareCorporateMiniShoppingCartModel(model);
                        break;
                }
            }
            
            return model;
        }

        /// <summary>
        /// Prepare the shopping cart model
        /// </summary>
        /// <param name="model">Shopping cart model</param>
        /// <param name="cart">List of the shopping cart item</param>
        /// <param name="isEditable">Whether model is editable</param>
        /// <param name="validateCheckoutAttributes">Whether to validate checkout attributes</param>
        /// <param name="prepareAndDisplayOrderReviewData">Whether to prepare and display order review data</param>
        /// <returns>Shopping cart model</returns>
        public override ShoppingCartModel PrepareShoppingCartModel(ShoppingCartModel model, IList<ShoppingCartItem> cart, bool isEditable = true, bool validateCheckoutAttributes = false, bool prepareAndDisplayOrderReviewData = false,
            bool insuranceChecked = false)
        {
            var newModel = base.PrepareShoppingCartModel(model, cart, isEditable, validateCheckoutAttributes, prepareAndDisplayOrderReviewData);

            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        PrepareConsumerShoppingCartModel(model, newModel);
                        break;
                    case FinanceType.Corporate:
                        PrepareCorporateShoppingCartModel(model, newModel);
                        break;
                }
            }

            return newModel;
        }

        #endregion
    }
}