﻿using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Web.Factories;
using Nop.Web.Models.Checkout;
using Nop.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Plugin.Ecorenew.Finance.Services.Corporate;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;

namespace Nop.Plugin.Ecorenew.Finance.Factories
{
    public class FinanceCheckoutModelFactory : CheckoutModelFactory
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IConsumerFinanceService _consumerFinanceService;
        private readonly ICorporateFinanceService _corporateFinanceService;
        private readonly FinanceSettings _financeSettings;

        #endregion

        #region Ctor

        public FinanceCheckoutModelFactory(IAddressModelFactory addressModelFactory, 
            IWorkContext workContext, 
            IStoreContext storeContext, 
            IStoreMappingService storeMappingService, 
            ILocalizationService localizationService, 
            ITaxService taxService, 
            ICurrencyService currencyService, 
            IPriceFormatter priceFormatter, 
            IOrderProcessingService orderProcessingService, 
            IProductAttributeParser productAttributeParser, 
            IProductService productService, 
            IGenericAttributeService genericAttributeService, 
            ICountryService countryService, 
            IStateProvinceService stateProvinceService, 
            IShippingService shippingService, 
            IPaymentService paymentService, 
            IOrderTotalCalculationService orderTotalCalculationService, 
            IRewardPointService rewardPointService, 
            IWebHelper webHelper, 
            IConsumerFinanceService consumerFinanceService,
            ICorporateFinanceService corporateFinanceService,
            CommonSettings commonSettings, 
            OrderSettings orderSettings, 
            RewardPointsSettings rewardPointsSettings, 
            PaymentSettings paymentSettings, 
            ShippingSettings shippingSettings, 
            AddressSettings addressSettings,
            FinanceSettings financeSettings)
            : base(addressModelFactory, 
                  workContext, 
                  storeContext, 
                  storeMappingService, 
                  localizationService, 
                  taxService, 
                  currencyService, 
                  priceFormatter, 
                  orderProcessingService, 
                  productAttributeParser, 
                  productService, 
                  genericAttributeService, 
                  countryService, 
                  stateProvinceService, 
                  shippingService, 
                  paymentService, 
                  orderTotalCalculationService, 
                  rewardPointService, 
                  webHelper, 
                  commonSettings, 
                  orderSettings, 
                  rewardPointsSettings, 
                  paymentSettings, 
                  shippingSettings, 
                  addressSettings)
        {
            this._localizationService = localizationService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._consumerFinanceService = consumerFinanceService;
            this._corporateFinanceService = corporateFinanceService;
            this._financeSettings = financeSettings;
        }

        #endregion

        #region Utilities

        protected virtual void PrepareConsumerBillingAddressModel(CheckoutBillingAddressModel model)
        {
            var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();

            if (signup != null)
            {
                var personalInformation = _consumerFinanceService.GetSignupPersonalInformation(signup);
                var homeAddress = _consumerFinanceService.GetSignupHomeAddress(signup);
                var employment = _consumerFinanceService.GetSignupEmployment(signup);

                model.BillingNewAddress.Address1 = (homeAddress.CurrentAddress.BuildingNumber + " " + homeAddress.CurrentAddress.Address1).Trim();
                model.BillingNewAddress.Address2 = string.IsNullOrWhiteSpace(homeAddress.CurrentAddress.Address2) ? null : homeAddress.CurrentAddress.Address2.Trim();
                model.BillingNewAddress.City = homeAddress.CurrentAddress.CityTown.Trim();
                model.BillingNewAddress.Company = employment.EmployedEmployerName.Trim();
                model.BillingNewAddress.CountryId = homeAddress.CurrentAddress.CountryId;

                foreach (var country in model.BillingNewAddress.AvailableCountries)
                {
                    if (country.Value == homeAddress.CurrentAddress.CountryId.ToString())
                        country.Selected = true;
                }

                model.BillingNewAddress.CountryName = _countryService.GetCountryById(homeAddress.CurrentAddress.CountryId).Name;

                if (model.BillingNewAddress.StateProvinceEnabled)
                {
                    model.BillingNewAddress.AvailableStates.Clear();

                    var states = _stateProvinceService
                        .GetStateProvincesByCountryId(model.BillingNewAddress.CountryId.HasValue ? model.BillingNewAddress.CountryId.Value : 0)
                        .ToList();

                    if (states.Any())
                    {
                        model.BillingNewAddress.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Address.SelectState"), Value = "0" });

                        foreach (var s in states)
                        {
                            model.BillingNewAddress.AvailableStates.Add(new SelectListItem
                            {
                                Text = s.GetLocalized(x => x.Name),
                                Value = s.Id.ToString(),
                                Selected = homeAddress.CurrentAddress.StateProvinceId.HasValue && homeAddress.CurrentAddress.StateProvinceId.Value == s.Id
                            });
                        }
                    }
                    else
                    {
                        bool anyCountrySelected = model.BillingNewAddress.AvailableCountries.Any(x => x.Selected);
                        model.BillingNewAddress.AvailableStates.Add(new SelectListItem
                        {
                            Text = _localizationService.GetResource(anyCountrySelected ? "Address.OtherNonUS" : "Address.SelectState"),
                            Value = "0"
                        });
                    }

                    var selectedState = model.BillingNewAddress.AvailableStates.FirstOrDefault(x => x.Selected);

                    if (selectedState != null)
                    {
                        model.BillingNewAddress.StateProvinceId = Convert.ToInt32(selectedState.Value);
                        model.BillingNewAddress.StateProvinceName = selectedState.Text;
                    }
                }

                model.BillingNewAddress.Email = personalInformation.Email.Trim();
                model.BillingNewAddress.FirstName = personalInformation.FirstName.Trim();
                model.BillingNewAddress.LastName = personalInformation.LastName.Trim();
                model.BillingNewAddress.PhoneNumber = (personalInformation.MobilePhoneNumber ?? "").Trim();
                model.BillingNewAddress.ZipPostalCode = homeAddress.CurrentAddress.Postcode.Trim();

                // check if available addresses is equal to the finance application address
                AddressModel equivalentAddress = null;

                foreach (var existingAddress in model.ExistingAddresses)
                {
                    if (existingAddress.Address1 == model.BillingNewAddress.Address1
                        && existingAddress.Address2 == model.BillingNewAddress.Address2
                        && existingAddress.City == model.BillingNewAddress.City
                        && existingAddress.CountryId == model.BillingNewAddress.CountryId
                        && (!model.BillingNewAddress.StateProvinceEnabled ? true : (existingAddress.StateProvinceId == model.BillingNewAddress.StateProvinceId))
                        && existingAddress.Email == model.BillingNewAddress.Email
                        && existingAddress.FirstName == model.BillingNewAddress.FirstName
                        && existingAddress.LastName == model.BillingNewAddress.LastName
                        && existingAddress.PhoneNumber == model.BillingNewAddress.PhoneNumber
                        && existingAddress.ZipPostalCode == model.BillingNewAddress.ZipPostalCode)
                    {
                        equivalentAddress = existingAddress;
                        break;
                    }
                }

                if (equivalentAddress != null)
                {
                    model.ExistingAddresses.Remove(equivalentAddress);
                    model.ExistingAddresses.Insert(0, equivalentAddress);
                    model.NewAddressPreselected = false;
                }
                else
                {
                    model.NewAddressPreselected = true;
                }
            }
        }

        protected virtual void PrepareCorporateBillingAddressModel(CheckoutBillingAddressModel model)
        {
            var signup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();

            if (signup != null)
            {
                var directorDetails = _corporateFinanceService.GetSignupDirectorDetails(signup);
                var basicDetails = _corporateFinanceService.GetSignupBasicDetails(signup);

                model.BillingNewAddress.Address1 = (basicDetails.CompanyAddress.BuildingNumber + " " + basicDetails.CompanyAddress.Address1).Trim();
                model.BillingNewAddress.Address2 = string.IsNullOrWhiteSpace(basicDetails.CompanyAddress.Address2) ? null : basicDetails.CompanyAddress.Address2.Trim();
                model.BillingNewAddress.City = basicDetails.CompanyAddress.CityTown.Trim();
                model.BillingNewAddress.Company = basicDetails.CompanyName.Trim();
                model.BillingNewAddress.CountryId = basicDetails.CompanyAddress.CountryId;

                foreach (var country in model.BillingNewAddress.AvailableCountries)
                {
                    if (country.Value == basicDetails.CompanyAddress.CountryId.ToString())
                        country.Selected = true;
                }

                model.BillingNewAddress.CountryName = _countryService.GetCountryById(basicDetails.CompanyAddress.CountryId).Name;

                if (model.BillingNewAddress.StateProvinceEnabled)
                {
                    model.BillingNewAddress.AvailableStates.Clear();

                    var states = _stateProvinceService
                        .GetStateProvincesByCountryId(model.BillingNewAddress.CountryId ?? 0)
                        .ToList();

                    if (states.Any())
                    {
                        model.BillingNewAddress.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Address.SelectState"), Value = "0" });

                        foreach (var s in states)
                        {
                            model.BillingNewAddress.AvailableStates.Add(new SelectListItem
                            {
                                Text = s.GetLocalized(x => x.Name),
                                Value = s.Id.ToString(),
                                Selected = basicDetails.CompanyAddress.StateProvinceId.HasValue && basicDetails.CompanyAddress.StateProvinceId.Value == s.Id
                            });
                        }
                    }
                    else
                    {
                        bool anyCountrySelected = model.BillingNewAddress.AvailableCountries.Any(x => x.Selected);
                        model.BillingNewAddress.AvailableStates.Add(new SelectListItem
                        {
                            Text = _localizationService.GetResource(anyCountrySelected ? "Address.OtherNonUS" : "Address.SelectState"),
                            Value = "0"
                        });
                    }

                    var selectedState = model.BillingNewAddress.AvailableStates.FirstOrDefault(x => x.Selected);

                    if (selectedState != null)
                    {
                        model.BillingNewAddress.StateProvinceId = Convert.ToInt32(selectedState.Value);
                        model.BillingNewAddress.StateProvinceName = selectedState.Text;
                    }
                }

                model.BillingNewAddress.Email = directorDetails.EmailAddress.Trim();
                model.BillingNewAddress.FirstName = directorDetails.FirstName.Trim();
                model.BillingNewAddress.LastName = directorDetails.LastName.Trim();
                model.BillingNewAddress.PhoneNumber = (directorDetails.MobileNumber ?? "").Trim();
                model.BillingNewAddress.ZipPostalCode = basicDetails.CompanyAddress.Postcode.Trim();

                // check if available addresses is equal to the finance application address
                AddressModel equivalentAddress = null;

                foreach (var existingAddress in model.ExistingAddresses)
                {
                    if (existingAddress.Address1 == model.BillingNewAddress.Address1
                        && existingAddress.Address2 == model.BillingNewAddress.Address2
                        && existingAddress.City == model.BillingNewAddress.City
                        && existingAddress.CountryId == model.BillingNewAddress.CountryId
                        && (!model.BillingNewAddress.StateProvinceEnabled ? true : (existingAddress.StateProvinceId == model.BillingNewAddress.StateProvinceId))
                        && existingAddress.Email == model.BillingNewAddress.Email
                        && existingAddress.FirstName == model.BillingNewAddress.FirstName
                        && existingAddress.LastName == model.BillingNewAddress.LastName
                        && existingAddress.PhoneNumber == model.BillingNewAddress.PhoneNumber
                        && existingAddress.ZipPostalCode == model.BillingNewAddress.ZipPostalCode)
                    {
                        equivalentAddress = existingAddress;
                        break;
                    }
                }

                if (equivalentAddress != null)
                {
                    model.ExistingAddresses.Remove(equivalentAddress);
                    model.ExistingAddresses.Insert(0, equivalentAddress);
                    model.NewAddressPreselected = false;
                }
                else
                {
                    model.NewAddressPreselected = true;
                }
            }
        }

        protected virtual void PrepareConsumerShippingAddressModel(CheckoutShippingAddressModel model)
        {
            var signup = _consumerFinanceService.GetLatestConsumerSignupOfCustomer();

            if (signup != null)
            {
                var personalInformation = _consumerFinanceService.GetSignupPersonalInformation(signup);
                var homeAddress = _consumerFinanceService.GetSignupHomeAddress(signup);
                var employment = _consumerFinanceService.GetSignupEmployment(signup);

                model.ShippingNewAddress.Address1 = (homeAddress.CurrentAddress.BuildingNumber + " " + homeAddress.CurrentAddress.Address1).Trim();
                model.ShippingNewAddress.Address2 = string.IsNullOrWhiteSpace(homeAddress.CurrentAddress.Address2) ? null : homeAddress.CurrentAddress.Address2.Trim();
                model.ShippingNewAddress.City = homeAddress.CurrentAddress.CityTown.Trim();
                model.ShippingNewAddress.Company = employment.EmployedEmployerName.Trim();
                model.ShippingNewAddress.CountryId = homeAddress.CurrentAddress.CountryId;

                foreach (var country in model.ShippingNewAddress.AvailableCountries)
                {
                    if (country.Value == homeAddress.CurrentAddress.CountryId.ToString())
                        country.Selected = true;
                }

                model.ShippingNewAddress.CountryName = _countryService.GetCountryById(homeAddress.CurrentAddress.CountryId).Name;

                if (model.ShippingNewAddress.StateProvinceEnabled)
                {
                    model.ShippingNewAddress.AvailableStates.Clear();

                    var states = _stateProvinceService
                        .GetStateProvincesByCountryId(model.ShippingNewAddress.CountryId.HasValue ? model.ShippingNewAddress.CountryId.Value : 0)
                        .ToList();
                    if (states.Any())
                    {
                        model.ShippingNewAddress.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Address.SelectState"), Value = "0" });

                        foreach (var s in states)
                        {
                            model.ShippingNewAddress.AvailableStates.Add(new SelectListItem
                            {
                                Text = s.GetLocalized(x => x.Name),
                                Value = s.Id.ToString(),
                                Selected = homeAddress.CurrentAddress.StateProvinceId.HasValue && homeAddress.CurrentAddress.StateProvinceId.Value == s.Id
                            });
                        }
                    }
                    else
                    {
                        bool anyCountrySelected = model.ShippingNewAddress.AvailableCountries.Any(x => x.Selected);
                        model.ShippingNewAddress.AvailableStates.Add(new SelectListItem
                        {
                            Text = _localizationService.GetResource(anyCountrySelected ? "Address.OtherNonUS" : "Address.SelectState"),
                            Value = "0"
                        });
                    }

                    var selectedState = model.ShippingNewAddress.AvailableStates.FirstOrDefault(x => x.Selected);

                    if (selectedState != null)
                    {
                        model.ShippingNewAddress.StateProvinceId = Convert.ToInt32(selectedState.Value);
                        model.ShippingNewAddress.StateProvinceName = selectedState.Text;
                    }
                }

                model.ShippingNewAddress.Email = personalInformation.Email.Trim();
                model.ShippingNewAddress.FirstName = personalInformation.FirstName.Trim();
                model.ShippingNewAddress.LastName = personalInformation.LastName.Trim();
                model.ShippingNewAddress.PhoneNumber = (personalInformation.MobilePhoneNumber ?? "").Trim();
                model.ShippingNewAddress.ZipPostalCode = homeAddress.CurrentAddress.Postcode.Trim();

                // check if available addresses is equal to the finance application address
                AddressModel equivalentAddress = null;

                foreach (var existingAddress in model.ExistingAddresses)
                {
                    if (existingAddress.Address1 == model.ShippingNewAddress.Address1
                        && existingAddress.Address2 == model.ShippingNewAddress.Address2
                        && existingAddress.City == model.ShippingNewAddress.City
                        && existingAddress.CountryId == model.ShippingNewAddress.CountryId
                        && (!model.ShippingNewAddress.StateProvinceEnabled ? true : (existingAddress.StateProvinceId == model.ShippingNewAddress.StateProvinceId))
                        && existingAddress.Email == model.ShippingNewAddress.Email
                        && existingAddress.FirstName == model.ShippingNewAddress.FirstName
                        && existingAddress.LastName == model.ShippingNewAddress.LastName
                        && existingAddress.PhoneNumber == model.ShippingNewAddress.PhoneNumber
                        && existingAddress.ZipPostalCode == model.ShippingNewAddress.ZipPostalCode)
                    {
                        equivalentAddress = existingAddress;
                        break;
                    }
                }

                model.ExistingAddresses.Clear();

                if (equivalentAddress != null)
                {
                    model.ExistingAddresses.Add(equivalentAddress);
                    model.NewAddressPreselected = false;
                }
                else
                {
                    model.NewAddressPreselected = true;
                }
            }
        }

        protected virtual void PrepareCorporateShippingAddressModel(CheckoutShippingAddressModel model)
        {
            var signup = _corporateFinanceService.GetLatestCorporateSignupOfCustomer();

            if (signup != null)
            {
                var directorDetails = _corporateFinanceService.GetSignupDirectorDetails(signup);
                var basicDetails = _corporateFinanceService.GetSignupBasicDetails(signup);

                model.ShippingNewAddress.Address1 = (basicDetails.CompanyAddress.BuildingNumber + " " + basicDetails.CompanyAddress.Address1).Trim();
                model.ShippingNewAddress.Address2 = string.IsNullOrWhiteSpace(basicDetails.CompanyAddress.Address2) ? null : basicDetails.CompanyAddress.Address2.Trim();
                model.ShippingNewAddress.City = basicDetails.CompanyAddress.CityTown.Trim();
                model.ShippingNewAddress.Company = basicDetails.CompanyName.Trim();
                model.ShippingNewAddress.CountryId = basicDetails.CompanyAddress.CountryId;

                foreach (var country in model.ShippingNewAddress.AvailableCountries)
                {
                    if (country.Value == basicDetails.CompanyAddress.CountryId.ToString())
                        country.Selected = true;
                }

                model.ShippingNewAddress.CountryName = _countryService.GetCountryById(basicDetails.CompanyAddress.CountryId).Name;

                if (model.ShippingNewAddress.StateProvinceEnabled)
                {
                    model.ShippingNewAddress.AvailableStates.Clear();

                    var states = _stateProvinceService
                        .GetStateProvincesByCountryId(model.ShippingNewAddress.CountryId ?? 0)
                        .ToList();
                    if (states.Any())
                    {
                        model.ShippingNewAddress.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Address.SelectState"), Value = "0" });

                        foreach (var s in states)
                        {
                            model.ShippingNewAddress.AvailableStates.Add(new SelectListItem
                            {
                                Text = s.GetLocalized(x => x.Name),
                                Value = s.Id.ToString(),
                                Selected = basicDetails.CompanyAddress.StateProvinceId.HasValue && basicDetails.CompanyAddress.StateProvinceId.Value == s.Id
                            });
                        }
                    }
                    else
                    {
                        bool anyCountrySelected = model.ShippingNewAddress.AvailableCountries.Any(x => x.Selected);
                        model.ShippingNewAddress.AvailableStates.Add(new SelectListItem
                        {
                            Text = _localizationService.GetResource(anyCountrySelected ? "Address.OtherNonUS" : "Address.SelectState"),
                            Value = "0"
                        });
                    }

                    var selectedState = model.ShippingNewAddress.AvailableStates.FirstOrDefault(x => x.Selected);

                    if (selectedState != null)
                    {
                        model.ShippingNewAddress.StateProvinceId = Convert.ToInt32(selectedState.Value);
                        model.ShippingNewAddress.StateProvinceName = selectedState.Text;
                    }
                }

                model.ShippingNewAddress.Email = directorDetails.EmailAddress.Trim();
                model.ShippingNewAddress.FirstName = directorDetails.FirstName.Trim();
                model.ShippingNewAddress.LastName = directorDetails.LastName.Trim();
                model.ShippingNewAddress.PhoneNumber = (directorDetails.MobileNumber ?? "").Trim();
                model.ShippingNewAddress.ZipPostalCode = basicDetails.CompanyAddress.Postcode.Trim();

                // check if available addresses is equal to the finance application address
                AddressModel equivalentAddress = null;

                foreach (var existingAddress in model.ExistingAddresses)
                {
                    if (existingAddress.Address1 == model.ShippingNewAddress.Address1
                        && existingAddress.Address2 == model.ShippingNewAddress.Address2
                        && existingAddress.City == model.ShippingNewAddress.City
                        && existingAddress.CountryId == model.ShippingNewAddress.CountryId
                        && (!model.ShippingNewAddress.StateProvinceEnabled ? true : (existingAddress.StateProvinceId == model.ShippingNewAddress.StateProvinceId))
                        && existingAddress.Email == model.ShippingNewAddress.Email
                        && existingAddress.FirstName == model.ShippingNewAddress.FirstName
                        && existingAddress.LastName == model.ShippingNewAddress.LastName
                        && existingAddress.PhoneNumber == model.ShippingNewAddress.PhoneNumber
                        && existingAddress.ZipPostalCode == model.ShippingNewAddress.ZipPostalCode)
                    {
                        equivalentAddress = existingAddress;
                        break;
                    }
                }

                model.ExistingAddresses.Clear();

                if (equivalentAddress != null)
                {
                    model.ExistingAddresses.Add(equivalentAddress);
                    model.NewAddressPreselected = false;
                }
                else
                {
                    model.NewAddressPreselected = true;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare billing address model
        /// </summary>
        /// <param name="cart">Cart</param>
        /// <param name="selectedCountryId">Selected country identifier</param>
        /// <param name="prePopulateNewAddressWithCustomerFields">Pre populate new address with customer fields</param>
        /// <param name="overrideAttributesXml">Override attributes xml</param>
        /// <returns>Billing address model</returns>
        public override CheckoutBillingAddressModel PrepareBillingAddressModel(IList<ShoppingCartItem> cart, int? selectedCountryId = null, bool prePopulateNewAddressWithCustomerFields = false, string overrideAttributesXml = "")
        {
            var model = base.PrepareBillingAddressModel(cart, selectedCountryId, prePopulateNewAddressWithCustomerFields, overrideAttributesXml);

            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        PrepareConsumerBillingAddressModel(model);
                        break;
                    case FinanceType.Corporate:
                        PrepareCorporateBillingAddressModel(model);
                        break;
                }
            }

            return model;
        }

        /// <summary>
        /// Prepare shipping address model
        /// </summary>
        /// <param name="selectedCountryId">Selected country identifier</param>
        /// <param name="prePopulateNewAddressWithCustomerFields">Pre populate new address with customer fields</param>
        /// <param name="overrideAttributesXml">Override attributes xml</param>
        /// <returns>Shipping address model</returns>
        public override CheckoutShippingAddressModel PrepareShippingAddressModel(int? selectedCountryId = null, bool prePopulateNewAddressWithCustomerFields = false, string overrideAttributesXml = "")
        {
            var model = base.PrepareShippingAddressModel(selectedCountryId, prePopulateNewAddressWithCustomerFields, overrideAttributesXml);

            if (FinancePlugin.IsInstalledAndEnabled())
            {
                switch (_financeSettings.FinanceType)
                {
                    case FinanceType.Consumer:
                        PrepareConsumerShippingAddressModel(model);
                        break;
                    case FinanceType.Corporate:
                        PrepareCorporateShippingAddressModel(model);
                        break;
                }
            }

            return model;
        }

        #endregion
    }
}