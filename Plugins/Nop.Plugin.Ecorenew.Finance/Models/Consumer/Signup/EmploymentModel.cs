﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Ecorenew.Finance.Validators.Consumer.Signup;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup
{
    [Validator(typeof(EmploymentModelValidator))]
    public class EmploymentModel : SignupPageModel
    {
        public EmploymentModel() : base("employment", "Ecorenew.Finance.Consumer.Signup.Employment.Title")
        {
            EmploymentStatusList = new List<SelectListItem>();
            AnnualGrossIncomeList = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.EmploymentStatus")]
        public int? EmploymentStatusId { get; set; }
        public List<SelectListItem> EmploymentStatusList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.AnnualGrossIncome")]
        public int? AnnualGrossIncomeId { get; set; }
        public List<SelectListItem> AnnualGrossIncomeList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.EmployedEmployerName")]
        public string EmployedEmployerName { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.EmployedPostCode")]
        public string EmployedPostCode { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.EmployedYearsEmployed")]
        public int EmployedYearsEmployed { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.EmployedMonthsEmployed")]
        public int EmployedMonthsEmployed { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.EmployedJobTitle")]
        public string EmployedJobTitle { get; set; }


        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.SelfEmployedTradingAs")]
        public string SelfEmployedTradingAs { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.SelfEmployedNatureOfBusiness")]
        public string SelfEmployedNatureOfBusiness { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.SelfEmployedYearsLength")]
        public int SelfEmployedYearsLength { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.SelfEmployedMonthsLength")]
        public int SelfEmployedMonthsLength { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.SelfEmployedPostCode")]
        public string SelfEmployedPostCode { get; set; }


        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.OtherYearsLength")]
        public int OtherYearsLength { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Employment.OtherMonthsLength")]
        public int OtherMonthsLength { get; set; }
    }
}