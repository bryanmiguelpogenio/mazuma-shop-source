﻿using FluentValidation.Attributes;
using Nop.Plugin.Ecorenew.Finance.Validators.Consumer.Signup;

namespace Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup
{
    [Validator(typeof(AuthorizationModelValidator))]
    public class AuthorizationModel : SignupPageModel
    {
        public AuthorizationModel() : base("authorization", "Ecorenew.Finance.Consumer.Signup.Authorization.Title")
        {
        }

        public bool Authorized { get; set; }
    }
}