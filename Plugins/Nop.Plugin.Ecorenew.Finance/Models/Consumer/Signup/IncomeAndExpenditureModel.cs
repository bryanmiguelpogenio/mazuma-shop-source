﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Ecorenew.Finance.Validators.Consumer.Signup;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup
{
    [Validator(typeof(IncomeAndExpenditureModelValidator))]
    public class IncomeAndExpenditureModel : SignupPageModel
    {
        public IncomeAndExpenditureModel() : base("income_and_expenditure", "Ecorenew.Finance.Consumer.Signup.IncomeAndExpenditure.Title")
        {
            this.ExpenditureCategoryRangeList = new List<ExpenditureCategoryRangeModel>();
        }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.IncomeAndExpenditure.IncomeAmount")]
        public decimal? MonthlyIncomeAmountAfterTax { get; set; }

        public List<ExpenditureCategoryRangeModel> ExpenditureCategoryRangeList { get; set; }

        public bool AbilityToPay { get; set; }
        public bool AboveIsCorrect { get; set; }

        #region Nested Classes

        [Validator(typeof(ExpenditureCategoryRangeModelValidator))]
        public class ExpenditureCategoryRangeModel
        {
            public ExpenditureCategoryRangeModel()
            {
                this.ExpenditureSelectList = new List<SelectListItem>();
            }

            public ExpenditureCategoryRangeModel(int expenditureCategoryId, string expenditureCategoryLocaleStringResourceName, string expenditureCategoryeDescription)
            {
                this.ExpenditureSelectList = new List<SelectListItem>();
                this.ExpenditureCategoryId = expenditureCategoryId;
                this.ExpenditureCategoryLocaleStringResourceName = expenditureCategoryLocaleStringResourceName;
                this.ExpenditureCategoryDescription = expenditureCategoryeDescription;
            }

            public int ExpenditureCategoryId { get; set; }
            public string ExpenditureCategoryLocaleStringResourceName { get; set; }
            public string ExpenditureCategoryDescription { get; set; }

            public int? ExpenditureRangeId { get; set; }
            public List<SelectListItem> ExpenditureSelectList { get; set; }

            public bool SpecificValueEnabled { get; set; }
            public decimal? SpecificValue { get; set; }
        }

        #endregion
    }
}