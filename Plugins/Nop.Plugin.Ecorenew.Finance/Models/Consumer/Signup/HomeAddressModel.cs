﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Ecorenew.Finance.Models.Common;
using Nop.Plugin.Ecorenew.Finance.Validators.Consumer.Signup;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup
{
    [Validator(typeof(HomeAddressModelValidator))]
    public class HomeAddressModel : SignupPageModel
    {
        public HomeAddressModel() : base("home_address", "Ecorenew.Finance.Consumer.Signup.HomeAddress.Title")
        {
            this.HomeOwnerStatusSelectList = new List<SelectListItem>();
            this.Current = new AddressModel();
            this.Previous = new AddressModel();
        }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.HomeAddress.IsPermanentUkResident")]
        public bool IsUkResident { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.HomeAddress.HomeOwnerStatus")]
        public int? HomeOwnerStatusId { get; set; }
        public List<SelectListItem> HomeOwnerStatusSelectList { get; set; }

        public AddressModel Current { get; set; }
        public AddressModel Previous { get; set; }
    }
}