﻿using FluentValidation.Attributes;
using Nop.Plugin.Ecorenew.Finance.Validators.Consumer.Signup;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup
{
    [Validator(typeof(BankModelValidator))]
    public class BankModel : SignupPageModel
    {
        public BankModel() : base("bank", "Ecorenew.Finance.Consumer.Signup.Bank.Title")
        {
        }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Bank.AccountHolder")]
        public string AccountName { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Bank.Sortcode")]
        public string SortCode { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Bank.AccountNumber")]
        public string AccountNumber { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.Bank.AgeOfAccount")]
        public int AccountAgeYears { get; set; }
        public int AccountAgeMonths { get; set; }
    }
}