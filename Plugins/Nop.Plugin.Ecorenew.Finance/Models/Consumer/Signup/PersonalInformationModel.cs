﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Ecorenew.Finance.Validators.Consumer.Signup;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup
{
    [Validator(typeof(PersonalInformationModelValidator))]
    public class PersonalInformationModel : SignupPageModel
    {
        public PersonalInformationModel() : base("personal_information", "Ecorenew.Finance.Consumer.Signup.PersonalInformation.Title")
        {
            this.TitleSelectList = new List<SelectListItem>();
            this.MaritalStatusSelectList = new List<SelectListItem>();
            this.NumberOfDependentsSelectList = new List<SelectListItem>();
            this.DateOfBirthDaySelectList = new List<SelectListItem>();
            this.DateOfBirthMonthSelectList = new List<SelectListItem>();
            this.DateOfBirthYearSelectList = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.Title")]
        public string Title { get; set; }
        public List<SelectListItem> TitleSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.FirstName")]
        public string FirstName { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.MiddleName")]
        public string MiddleName { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.LastName")]
        public string LastName { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.Name")]
        public string FullName { get { return $"{FirstName} {MiddleName} {LastName}"; }}

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.MaritalStatus")]
        public int? MaritalStatusId { get; set; }
        public List<SelectListItem> MaritalStatusSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.NumberOfDependents")]
        public int? NumberOfDependentsId { get; set; }
        public List<SelectListItem> NumberOfDependentsSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.DateOfBirth")]
        public int? DateOfBirthDay { get; set; }
        public List<SelectListItem> DateOfBirthDaySelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.DateOfBirth")]
        public int? DateOfBirthMonth { get; set; }
        public List<SelectListItem> DateOfBirthMonthSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.DateOfBirth")]
        public int? DateOfBirthYear { get; set; }
        public List<SelectListItem> DateOfBirthYearSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.Email")]
        public string Email { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.ConfirmEmail")]
        public string ConfirmEmail { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Consumer.Signup.PersonalDetails.MobileNumber")]
        public string MobilePhoneNumber { get; set; }

        public bool PasswordRequired { get; set; }

        [DataType(DataType.Password)]
        [NoTrim]
        [NopResourceDisplayName("Account.Fields.Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [NoTrim]
        [NopResourceDisplayName("Account.Fields.ConfirmPassword")]
        public string ConfirmPassword { get; set; }
    }
}