﻿using System;

namespace Nop.Plugin.Ecorenew.Finance.Models.Consumer.Components
{
    [Serializable]
    public class ConsumerFinanceBreakdownTerm
    {
        public string Title { get; set; }
        public int NumberOfMonths { get; set; }
        public string MonthlyPayment { get; set; }
        public string PhoneCost { get; set; }
        public string InitialPayment { get; set; }
        public string TotalChargeForCredit { get; set; }
        public string TotalAmountPayable { get; set; }
        public string Apr { get; set; }
    }
}