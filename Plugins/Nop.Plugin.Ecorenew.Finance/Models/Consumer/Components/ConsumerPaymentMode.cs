﻿namespace Nop.Plugin.Ecorenew.Finance.Models.Consumer.Components
{
    public enum ConsumerPaymentMode
    {
        PayNow = 1,
        PayMonthly = 2
    }
}