﻿using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Models.Consumer.Components
{
    [Serializable]
    public class ConsumerFinanceBreakdownPopupComponentModel
    {
        public ConsumerFinanceBreakdownPopupComponentModel()
        {
            this.Terms = new List<ConsumerFinanceBreakdownTerm>();
            this.TermViews = new List<string>();
        }

        public List<ConsumerFinanceBreakdownTerm> Terms { get; set; }
        public List<string> TermViews { get; set; }
    }
}