﻿using System;

namespace Nop.Plugin.Ecorenew.Finance.Models.Consumer.Components
{
    [Serializable]
    public class ProductDetailsConsumerFinanceBreakdownModel
    {
        public string Apr { get; set; }
        public string InitialPayment { get; set; }
        public string LowestMonthlyPayment { get; set; }
        public string LowestMonthlyPaymentMonths { get; set; }
        public string LowestMonthlyPaymentTotalAmountPayable { get; set; }
        public string OptionToPurchaseFee { get; set; }

        public ConsumerFinanceBreakdownPopupComponentModel FinanceValues { get; set; }
    }
}