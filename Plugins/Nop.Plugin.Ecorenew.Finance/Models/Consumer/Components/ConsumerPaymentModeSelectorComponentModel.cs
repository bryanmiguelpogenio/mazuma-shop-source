﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Models.Consumer.Components
{
    public class ConsumerPaymentModeSelectorComponentModel
    {
        public ConsumerPaymentModeSelectorComponentModel()
        {
            this.PaymentModes = new List<SelectListItem>();
            this.ConsumerFinanceOptions = new List<SelectListItem>();
        }

        public int? SelectedPaymentModeId { get; set; }
        public List<SelectListItem> PaymentModes { get; set; }

        public int? SelectedConsumerFinanceOptionId { get; set; }
        public List<SelectListItem> ConsumerFinanceOptions { get; set; }

        public bool ReadOnly { get; set; }
        public bool ShowSignupLink { get; set; }
        public string DocuSignToken { get; set; }
    }
}