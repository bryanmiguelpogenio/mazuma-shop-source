﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Models.Common
{
    public class AddressModel
    {
        public AddressModel()
        {
            CountrySelectList = new List<SelectListItem>();
            StateProvinceSelectList = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Ecorenew.Finance.Common.Address.BuildingNumber")]
        public string BuildingNumber { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Common.Address.Address1")]
        public string Address1 { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Common.Address.Address2")]
        public string Address2 { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Common.Address.CityTown")]
        public string CityTown { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Common.Address.Country")]
        public int? CountryId { get; set; }
        public List<SelectListItem> CountrySelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Common.Address.County")]
        public int? StateProvinceId { get; set; }
        public bool StateProvinceRequired { get; set; }
        public List<SelectListItem> StateProvinceSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Common.Address.PostCode")]
        public string Postcode { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Common.Address.LengthOfStay")]
        public int LengthOfStayYears { get; set; }
        public int LengthOfStayMonths { get; set; }
    }
}
