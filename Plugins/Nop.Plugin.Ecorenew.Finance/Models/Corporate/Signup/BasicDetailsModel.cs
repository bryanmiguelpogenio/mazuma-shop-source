﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Ecorenew.Finance.Models.Common;
using Nop.Plugin.Ecorenew.Finance.Validators.Corporate.Signup;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup
{
    [Validator(typeof(BasicDetailsModelValidator))]
    public class BasicDetailsModel : SignupPageModel
    {
        public BasicDetailsModel() : base("basic_details", "Ecorenew.Finance.Corporate.Signup.BasicDetails.Title")
        {
            this.DateEstablishedDaySelectList = new List<SelectListItem>();
            this.DateEstablishedMonthSelectList = new List<SelectListItem>();
            this.DateEstablishedYearSelectList = new List<SelectListItem>();
            this.CompanyAddress = new AddressModel();
            this.TradingAddress = new AddressModel();
        }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.BasicDetails.CompanyName")]
        public string CompanyName { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.BasicDetails.TradingAs")]
        public string TradingAs { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.BasicDetails.CompanyRegistrationNumber")]
        public string CompanyRegistrationNumber { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.BasicDetails.VatNumber")]
        public string VatNumber { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.BasicDetails.DateEstablished")]
        public int? DateEstablishedDay { get; set; }
        public List<SelectListItem> DateEstablishedDaySelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.BasicDetails.DateEstablished")]
        public int? DateEstablishedMonth { get; set; }
        public List<SelectListItem> DateEstablishedMonthSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.BasicDetails.DateEstablished")]
        public int? DateEstablishedYear { get; set; }
        public List<SelectListItem> DateEstablishedYearSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.BasicDetails.IndustrySector")]
        public string IndustrySector { get; set; }

        public AddressModel CompanyAddress { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.BasicDetails.TadingAddressSameAsCompanyAddress")]
        public bool TradingAddressSameAsCompanyAddress { get; set; }

        public AddressModel TradingAddress { get; set; }
    }
}