﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Ecorenew.Finance.Models.Common;
using Nop.Plugin.Ecorenew.Finance.Validators.Corporate.Signup;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup
{
    [Validator(typeof(DirectorDetailsModelValidator))]
    public class DirectorDetailsModel : SignupPageModel
    {
        public DirectorDetailsModel() : base("director_details", "Ecorenew.Finance.Corporate.Signup.DirectorDetails.Title")
        {
            this.TitleSelectList = new List<SelectListItem>();
            this.MaritalStatusSelectList = new List<SelectListItem>();
            this.NumberOfDependentsSelectList = new List<SelectListItem>();
            this.HomeOwnerStatusSelectList = new List<SelectListItem>();
            this.DateOfBirthDaySelectList = new List<SelectListItem>();
            this.DateOfBirthMonthSelectList = new List<SelectListItem>();
            this.DateOfBirthYearSelectList = new List<SelectListItem>();
            this.CurrentAddress = new AddressModel();
            this.PreviousAddress = new AddressModel();
        }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.Title")]
        public string Title { get; set; }
        public List<SelectListItem> TitleSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.FirstName")]
        public string FirstName { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.MiddleName")]
        public string MiddleName { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.LastName")]
        public string LastName { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.Name")]
        public string FullName { get { return $"{FirstName} {MiddleName} {LastName}"; } }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.DateOfBirth")]
        public int? DateOfBirthDay { get; set; }
        public List<SelectListItem> DateOfBirthDaySelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.DateOfBirth")]
        public int? DateOfBirthMonth { get; set; }
        public List<SelectListItem> DateOfBirthMonthSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.DateOfBirth")]
        public int? DateOfBirthYear { get; set; }
        public List<SelectListItem> DateOfBirthYearSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.IsPermanentUkResident")]
        public bool IsUkResident { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.MaritalStatus")]
        public int? MaritalStatusId { get; set; }
        public List<SelectListItem> MaritalStatusSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.NumberOfDependents")]
        public int? NumberOfDependentsId { get; set; }
        public List<SelectListItem> NumberOfDependentsSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.HomeOwnerStatus")]
        public int? HomeOwnerStatusId { get; set; }
        public List<SelectListItem> HomeOwnerStatusSelectList { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.Email")]
        public string Email { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.ConfirmEmail")]
        public string ConfirmEmail { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.DirectorDetails.MobileNumber")]
        public string MobilePhoneNumber { get; set; }

        public bool PasswordRequired { get; set; }

        [DataType(DataType.Password)]
        [NoTrim]
        [NopResourceDisplayName("Account.Fields.Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [NoTrim]
        [NopResourceDisplayName("Account.Fields.ConfirmPassword")]
        public string ConfirmPassword { get; set; }

        public AddressModel CurrentAddress { get; set; }

        public AddressModel PreviousAddress { get; set; }
    }
}