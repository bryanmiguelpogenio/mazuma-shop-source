﻿using FluentValidation.Attributes;
using Nop.Plugin.Ecorenew.Finance.Validators.Corporate.Signup;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup
{
    [Validator(typeof(BankModelValidator))]
    public class BankModel : SignupPageModel
    {
        public BankModel() : base("bank", "Ecorenew.Finance.Corporate.Signup.Bank.Title")
        {
        }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.Bank.AccountHolder")]
        public string AccountName { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.Bank.CompanyName")]
        public string CompanyName { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.Bank.TradingName")]
        public string TradingName { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.Bank.Sortcode")]
        public string SortCode { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.Bank.AccountNumber")]
        public string AccountNumber { get; set; }

        [NopResourceDisplayName("Ecorenew.Finance.Corporate.Signup.Bank.AgeOfAccount")]
        public int AccountAgeYears { get; set; }
        public int AccountAgeMonths { get; set; }
    }
}