﻿using FluentValidation.Attributes;
using Nop.Plugin.Ecorenew.Finance.Validators.Corporate.Signup;

namespace Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup
{
    [Validator(typeof(AuthorizationModelValidator))]
    public class AuthorizationModel : SignupPageModel
    {
        public AuthorizationModel() : base("authorization", "Ecorenew.Finance.Corporate.Signup.Authorization.Title")
        {
        }

        public bool Authorized { get; set; }
    }
}