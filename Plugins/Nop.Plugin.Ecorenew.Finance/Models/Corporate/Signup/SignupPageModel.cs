﻿namespace Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup
{
    public abstract class SignupPageModel
    {
        protected SignupPageModel(string pageId, string pageHeaderStringResourceName)
        {
            this.PageId = pageId;
            this.PageHeaderStringResourceName = pageHeaderStringResourceName;
        }

        public string PageId { get; protected set; }
        public string PageHeaderStringResourceName { get; protected set; }
    }
}