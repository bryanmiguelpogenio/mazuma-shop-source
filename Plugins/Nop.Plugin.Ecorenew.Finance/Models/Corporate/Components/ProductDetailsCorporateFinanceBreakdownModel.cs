﻿using System;

namespace Nop.Plugin.Ecorenew.Finance.Models.Corporate.Components
{
    [Serializable]
    public class ProductDetailsCorporateFinanceBreakdownModel
    {
        public string LowestMonthlyPayment { get; set; }
        public string LowestMonthlyPaymentMonths { get; set; }
        public string LowestMonthlyPaymentTotal { get; set; }

        public CorporateFinanceBreakdownPopupComponentModel FinanceValues { get; set; }
    }
}