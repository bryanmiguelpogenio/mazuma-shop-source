﻿using System;

namespace Nop.Plugin.Ecorenew.Finance.Models.Corporate.Components
{
    [Serializable]
    public class CorporateFinanceBreakdownTerm
    {
        public string Title { get; set; }
        public string PhoneCost { get; set; }
        public int NumberOfMonths { get; set; }
        public string MonthlyPayment { get; set; }
        public string InterestCharges { get; set; }
        public string TotalPayable { get; set; }
    }
}