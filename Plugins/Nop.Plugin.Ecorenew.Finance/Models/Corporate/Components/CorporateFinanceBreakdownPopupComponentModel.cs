﻿using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Models.Corporate.Components
{
    [Serializable]
    public class CorporateFinanceBreakdownPopupComponentModel
    {
        public CorporateFinanceBreakdownPopupComponentModel()
        {
            this.Terms = new List<CorporateFinanceBreakdownTerm>();
            this.TermViews = new List<string>();
        }

        public List<CorporateFinanceBreakdownTerm> Terms { get; set; }
        public List<string> TermViews { get; set; }
    }
}