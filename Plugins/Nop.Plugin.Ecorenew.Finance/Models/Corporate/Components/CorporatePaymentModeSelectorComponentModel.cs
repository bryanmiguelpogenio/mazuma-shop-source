﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.Finance.Models.Corporate.Components
{
    public class CorporatePaymentModeSelectorComponentModel
    {
        public CorporatePaymentModeSelectorComponentModel()
        {
            this.PaymentModes = new List<SelectListItem>();
            this.CorporateFinanceOptions = new List<SelectListItem>();
        }

        public int? SelectedPaymentModeId { get; set; }
        public List<SelectListItem> PaymentModes { get; set; }

        public int? SelectedCorporateFinanceOptionId { get; set; }
        public List<SelectListItem> CorporateFinanceOptions { get; set; }

        public bool ReadOnly { get; set; }
        public bool ShowSignupLink { get; set; }
        public string DocuSignToken { get; set; }
    }
}