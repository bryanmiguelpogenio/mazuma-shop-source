﻿namespace Nop.Plugin.Ecorenew.Finance.Models.Corporate.Components
{
    public enum CorporatePaymentMode
    {
        PayNow = 1,
        PayMonthly = 2
    }
}