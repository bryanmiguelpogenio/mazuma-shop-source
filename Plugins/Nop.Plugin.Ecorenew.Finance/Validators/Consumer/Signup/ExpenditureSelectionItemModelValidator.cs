﻿using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using FluentValidation;
using Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Ecorenew.Finance.Validators.Consumer.Signup
{
    public class ExpenditureCategoryRangeModelValidator : BaseNopValidator<IncomeAndExpenditureModel.ExpenditureCategoryRangeModel>
    {
        private readonly IConsumerFinanceService _consumerFinanceService;

        public ExpenditureCategoryRangeModelValidator(IConsumerFinanceService consumerFinanceService)
        {
            this._consumerFinanceService = consumerFinanceService;

            RuleFor(x => x.ExpenditureRangeId)
                .NotEmpty()
                .WithMessage("Please select an option.");

            RuleFor(x => x.SpecificValue)
                .NotEmpty()
                .WithMessage("Please input exact amount.")
                .When(x => x.SpecificValueEnabled);

            RuleFor(x => x.SpecificValue)
                .Must(SpecificValueGreaterThanRangeValue)
                .WithMessage(x =>
                {
                    var midPointValue = _consumerFinanceService.GetConsumerExpenditureRangeById(x.ExpenditureRangeId.Value).MidPointValue;
                    return $"Please input amount larger than {midPointValue}.";
                })
                .When(x => x.SpecificValueEnabled);
        }

        public bool SpecificValueGreaterThanRangeValue(IncomeAndExpenditureModel.ExpenditureCategoryRangeModel model, decimal? value)
        {
            if (!model.ExpenditureRangeId.HasValue)
                return false;

            var range = _consumerFinanceService.GetConsumerExpenditureRangeById(model.ExpenditureRangeId.Value);

            if (range != null && model.SpecificValue.HasValue && model.SpecificValue.Value > range.MidPointValue)
                return true;

            return false;
        }
    }
}
