﻿using FluentValidation;
using Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Ecorenew.Finance.Validators.Consumer.Signup
{
    public class EmploymentModelValidator : BaseNopValidator<EmploymentModel>
    {
        public EmploymentModelValidator()
        {
            RuleFor(x => x.EmploymentStatusId)
                .NotEmpty()
                .WithMessage("Please select employment status.");

            RuleFor(x => x.AnnualGrossIncomeId)
                .NotEmpty()
                .WithMessage("Please select annual gross income.");

            When(x => x.EmploymentStatusId == 1, () =>
            {
                RuleFor(x => x.EmployedEmployerName)
                    .NotEmpty()
                    .WithMessage("Please enter employer name.");

                RuleFor(x => x.EmployedYearsEmployed)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Please enter a positive number.");

                RuleFor(x => x.EmployedMonthsEmployed)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Please enter a positive number.");

                RuleFor(x => x.EmployedMonthsEmployed)
                    .LessThan(12)
                    .WithMessage("Max is 11.");

                When(x => x.EmployedYearsEmployed == 0, () =>
                {
                    RuleFor(x => x.EmployedMonthsEmployed)
                        .GreaterThan(0)
                        .WithMessage("Invalid values for time employed.");
                });
            });

            When(x => x.EmploymentStatusId == 2, () =>
            {
                RuleFor(x => x.SelfEmployedNatureOfBusiness)
                    .NotEmpty()
                    .WithMessage("Please enter nature of business.");

                RuleFor(x => x.SelfEmployedTradingAs)
                    .NotEmpty()
                    .WithMessage("Please enter trading as.");

                RuleFor(x => x.SelfEmployedYearsLength)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Please enter a positive number.");

                RuleFor(x => x.SelfEmployedMonthsLength)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Please enter a positive number.");

                RuleFor(x => x.SelfEmployedMonthsLength)
                    .LessThan(12)
                    .WithMessage("Max is 11.");

                When(x => x.SelfEmployedYearsLength == 0, () =>
                {
                    RuleFor(x => x.SelfEmployedMonthsLength)
                        .GreaterThan(0)
                        .WithMessage("Invalid values.");
                });
            });

            When(x => x.EmploymentStatusId == 3, () =>
            {
                RuleFor(x => x.OtherYearsLength)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Please enter a positive number.");

                RuleFor(x => x.OtherMonthsLength)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Please enter a positive number.");

                RuleFor(x => x.OtherMonthsLength)
                    .LessThan(12)
                    .WithMessage("Max is 11.");

                When(x => x.OtherYearsLength == 0, () =>
                {
                    RuleFor(x => x.OtherMonthsLength)
                        .GreaterThan(0)
                        .WithMessage("Invalid values.");
                });
            });
        }
    }
}