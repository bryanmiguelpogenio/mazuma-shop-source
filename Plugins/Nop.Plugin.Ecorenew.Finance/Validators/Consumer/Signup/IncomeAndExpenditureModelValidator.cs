﻿using Nop.Plugin.Ecorenew.Finance.Services.Consumer;
using FluentValidation;
using Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Ecorenew.Finance.Validators.Consumer.Signup
{
    public class IncomeAndExpenditureModelValidator : BaseNopValidator<IncomeAndExpenditureModel>
    {
        public IncomeAndExpenditureModelValidator(IConsumerFinanceService consumerFinanceService)
        {
            RuleFor(x => x.MonthlyIncomeAmountAfterTax)
                .NotEmpty()
                .WithMessage("Please enter monthly income after tax.");

            RuleForEach(x => x.ExpenditureCategoryRangeList)
                .SetValidator(new ExpenditureCategoryRangeModelValidator(consumerFinanceService));

            RuleFor(x => x.AbilityToPay)
                .Equal(true)
                .WithMessage("Please confirm.");

            RuleFor(x => x.AboveIsCorrect)
                .Equal(true)
                .WithMessage("Please confirm.");
        }
    }
}