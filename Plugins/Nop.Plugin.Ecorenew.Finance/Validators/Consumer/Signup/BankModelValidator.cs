﻿using FluentValidation;
using Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Ecorenew.Finance.Validators.Consumer.Signup
{
    public class BankModelValidator : BaseNopValidator<BankModel>
    {
        public BankModelValidator()
        {
            RuleFor(x => x.AccountName)
                .NotEmpty()
                .WithMessage("Please enter account name.");

            RuleFor(x => x.AccountNumber)
                .NotEmpty()
                .WithMessage("Please enter account number.");

            RuleFor(x => x.SortCode)
                .NotEmpty()
                .WithMessage("Please enter sort code.");

            RuleFor(x => x.AccountNumber)
                .Matches("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]")
                .WithMessage("Invalid account number");

            RuleFor(x => x.SortCode)
                .Matches("[0-9][0-9]-[0-9][0-9]-[0-9][0-9]")
                .WithMessage("Invalid sort code");

            RuleFor(x => x.AccountAgeYears)
                .GreaterThanOrEqualTo(0)
                .WithMessage("Please enter a positive number.");

            RuleFor(x => x.AccountAgeMonths)
                .GreaterThanOrEqualTo(0)
                .WithMessage("Please enter a positive number.");

            RuleFor(x => x.AccountAgeMonths)
                .LessThan(12)
                .WithMessage("Max is 11.");

            When(x => x.AccountAgeYears == 0, () =>
            {
                RuleFor(x => x.AccountAgeMonths)
                    .GreaterThan(0)
                    .WithMessage("Invalid values.");
            });
        }
    }
}