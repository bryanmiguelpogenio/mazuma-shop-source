﻿using FluentValidation;
using Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Ecorenew.Finance.Validators.Consumer.Signup
{
    public class HomeAddressModelValidator : BaseNopValidator<HomeAddressModel>
    {
        public HomeAddressModelValidator()
        {
            RuleFor(x => x.IsUkResident)
                .Equal(true)
                .WithMessage("You must be a permantent UK resident to apply.");

            RuleFor(x => x.HomeOwnerStatusId)
                .NotEmpty()
                .WithMessage("Please select home owner status.");

            RuleFor(x => x.Current.Address1)
                .NotEmpty()
                .WithMessage("Please enter current address 1.");

            RuleFor(x => x.Current.CountryId)
                .NotEmpty()
                .WithMessage("Please select current country.");

            RuleFor(x => x.Current.StateProvinceId)
                .NotEmpty()
                .When(x => x.Current.StateProvinceRequired)
                .WithMessage("Please select county.");

            RuleFor(x => x.Current.Postcode)
                .NotEmpty()
                .WithMessage("Please enter current postcode.");

            RuleFor(x => x.Current.LengthOfStayYears)
                .GreaterThanOrEqualTo(0)
                .WithMessage("Please enter a positive number.");

            RuleFor(x => x.Current.LengthOfStayMonths)
                .GreaterThanOrEqualTo(0)
                .WithMessage("Please enter a positive number.");

            RuleFor(x => x.Current.LengthOfStayMonths)
                .LessThan(12)
                .WithMessage("Max is 11.");

            When(x => x.Current.LengthOfStayYears == 0, () =>
            {
                RuleFor(x => x.Current.LengthOfStayMonths)
                    .GreaterThan(0)
                    .WithMessage("Invalid length of stay.");
            });

            When(x => x.Current.LengthOfStayYears < 3, () =>
            {
                RuleFor(x => x.Previous.Address1)
                    .NotEmpty()
                    .WithMessage("Please enter previous address 1.");

                RuleFor(x => x.Previous.CountryId)
                    .NotEmpty()
                    .WithMessage("Please select previous country.");

                RuleFor(x => x.Previous.StateProvinceId)
                    .NotEmpty()
                    .When(x => x.Previous.StateProvinceRequired)
                    .WithMessage("Please select previous county.");

                RuleFor(x => x.Previous.Postcode)
                    .NotEmpty()
                    .WithMessage("Please enter previous postcode.");

                RuleFor(x => x.Previous.LengthOfStayYears)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Please enter a positive number.");

                RuleFor(x => x.Previous.LengthOfStayMonths)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Please enter a positive number.");

                RuleFor(x => x.Previous.LengthOfStayMonths)
                    .LessThan(12)
                    .WithMessage("Max is 11.");

                When(x => x.Previous.LengthOfStayYears == 0, () =>
                {
                    RuleFor(x => x.Previous.LengthOfStayMonths)
                        .GreaterThan(0)
                        .WithMessage("Invalid length of stay.");
                });
            });
        }
    }
}