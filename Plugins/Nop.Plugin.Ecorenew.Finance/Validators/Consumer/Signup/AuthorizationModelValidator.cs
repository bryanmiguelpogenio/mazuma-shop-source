﻿using FluentValidation;
using Nop.Plugin.Ecorenew.Finance.Models.Consumer.Signup;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Ecorenew.Finance.Validators.Consumer.Signup
{
    public class AuthorizationModelValidator : BaseNopValidator<AuthorizationModel>
    {
        public AuthorizationModelValidator()
        {
            RuleFor(x => x.Authorized)
                .Equal(true)
                .WithMessage("You need to confirm authorisation.");
        }
    }
}