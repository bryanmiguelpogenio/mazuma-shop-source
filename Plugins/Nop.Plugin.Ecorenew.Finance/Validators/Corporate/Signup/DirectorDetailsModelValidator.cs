﻿using FluentValidation;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using System;

namespace Nop.Plugin.Ecorenew.Finance.Validators.Corporate.Signup
{
    public class DirectorDetailsModelValidator : BaseNopValidator<DirectorDetailsModel>
    {
        public DirectorDetailsModelValidator(CustomerSettings customerSettings,
            ILocalizationService localizationService)
        {
            RuleFor(x => x.Title)
                .NotEmpty()
                .WithMessage("Please select title.");

            RuleFor(x => x.FirstName)
                .NotEmpty()
                .WithMessage("Please enter first name.");

            RuleFor(x => x.LastName)
                .NotEmpty()
                .WithMessage("Please enter last name.");

            RuleFor(x => x.DateOfBirthDay)
                .NotEmpty()
                .WithMessage("Please select day.");

            RuleFor(x => x.DateOfBirthMonth)
                .NotEmpty()
                .WithMessage("Please select month.");

            RuleFor(x => x.DateOfBirthYear)
                .NotEmpty()
                .WithMessage("Please select year.");

            RuleFor(x => x.DateOfBirthDay)
                .Must((pi, day) =>
                {
                    // Parse date of birth. If succeeded, return true.
                    try { var dateOfBirth = new DateTime(pi.DateOfBirthYear.Value, pi.DateOfBirthMonth.Value, pi.DateOfBirthDay.Value); return true; }
                    catch { return false; }
                })
                .WithMessage("Invalid date of birth");

            RuleFor(x => x.DateOfBirthDay)
                .Must((pi, day) =>
                {
                    if ((pi.DateOfBirthYear > DateTime.UtcNow.Year - 18)
                    || (pi.DateOfBirthYear == (DateTime.UtcNow.Year - 18) && pi.DateOfBirthMonth > DateTime.UtcNow.Month)
                    || (pi.DateOfBirthYear == (DateTime.UtcNow.Year - 18) && pi.DateOfBirthMonth == DateTime.UtcNow.Month && pi.DateOfBirthDay > DateTime.UtcNow.Day))
                    {
                        return false;
                    }

                    return true;
                })
                .WithMessage("Must be at least 18 years old.");

            RuleFor(x => x.MaritalStatusId)
                .NotEmpty()
                .WithMessage("Please select marital status.");

            RuleFor(x => x.NumberOfDependentsId)
                .NotEmpty()
                .WithMessage("Please select number of dependents.");

            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Please enter email address.");

            RuleFor(x => x.Email)
                .EmailAddress()
                .WithMessage(localizationService.GetResource("Common.WrongEmail"));

            RuleFor(x => x.Password).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Password.Required")).When(x => x.PasswordRequired);
            RuleFor(x => x.Password).Length(customerSettings.PasswordMinLength, 999).WithMessage(string.Format(localizationService.GetResource("Account.Fields.Password.LengthValidation"), customerSettings.PasswordMinLength)).When(x => x.PasswordRequired);

            RuleFor(x => x.Password)
                .Matches(@"[a-z]+")
                .WithMessage("Password should contain At least one lower case letter")
                .When(x => x.PasswordRequired);

            RuleFor(x => x.Password)
                .Matches(@"[A-Z]+")
                .WithMessage("Password should contain At least one upper case letter")
                .When(x => x.PasswordRequired);

            RuleFor(x => x.Password)
                .Matches(@"[0-9]+")
                .WithMessage("Password should contain At least one numeric value")
                .When(x => x.PasswordRequired);

            RuleFor(x => x.ConfirmPassword).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.ConfirmPassword.Required")).When(x => x.PasswordRequired);
            RuleFor(x => x.ConfirmPassword).Equal(x => x.Password).WithMessage(localizationService.GetResource("Account.Fields.Password.EnteredPasswordsDoNotMatch")).When(x => x.PasswordRequired);

            RuleFor(x => x.ConfirmEmail)
                .NotEmpty()
                .WithMessage("Please confirm email address.");

            RuleFor(x => x.ConfirmEmail)
                .Equal(x => x.Email)
                .WithMessage("Email addresses does not match.");

            RuleFor(x => x.MobilePhoneNumber)
                .NotEmpty()
                .WithMessage("Please enter home phone number.");

            RuleFor(x => x.IsUkResident)
                .Equal(true)
                .WithMessage("You must be a permantent UK resident to apply.");

            RuleFor(x => x.HomeOwnerStatusId)
                .NotEmpty()
                .WithMessage("Please select home owner status.");

            RuleFor(x => x.CurrentAddress.Address1)
                .NotEmpty()
                .WithMessage("Please enter current address 1.");

            RuleFor(x => x.CurrentAddress.CountryId)
                .NotEmpty()
                .WithMessage("Please select current country.");

            RuleFor(x => x.CurrentAddress.StateProvinceId)
                .NotEmpty()
                .When(x => x.CurrentAddress.StateProvinceRequired)
                .WithMessage("Please select county.");

            RuleFor(x => x.CurrentAddress.Postcode)
                .NotEmpty()
                .WithMessage("Please enter current postcode.");

            RuleFor(x => x.CurrentAddress.LengthOfStayYears)
                .GreaterThanOrEqualTo(0)
                .WithMessage("Please enter a positive number.");

            RuleFor(x => x.CurrentAddress.LengthOfStayMonths)
                .GreaterThanOrEqualTo(0)
                .WithMessage("Please enter a positive number.");

            RuleFor(x => x.CurrentAddress.LengthOfStayMonths)
                .LessThan(12)
                .WithMessage("Max is 11.");
        }
    }
}