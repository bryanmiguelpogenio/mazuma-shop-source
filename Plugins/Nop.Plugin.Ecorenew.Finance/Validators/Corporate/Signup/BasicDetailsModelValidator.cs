﻿using FluentValidation;
using Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup;
using Nop.Web.Framework.Validators;
using System;

namespace Nop.Plugin.Ecorenew.Finance.Validators.Corporate.Signup
{
    public class BasicDetailsModelValidator : BaseNopValidator<BasicDetailsModel>
    {
        public BasicDetailsModelValidator()
        {
            RuleFor(x => x.CompanyName)
                .NotEmpty()
                .WithMessage("Please input company name");

            RuleFor(x => x.TradingAs)
                .NotEmpty()
                .WithMessage("Please input trading as");

            RuleFor(x => x.CompanyRegistrationNumber)
                .NotEmpty()
                .WithMessage("Please input company registration number");

            RuleFor(x => x.VatNumber)
                .NotEmpty()
                .WithMessage("Please input VAT number");

            RuleFor(x => x.DateEstablishedDay)
                .NotEmpty()
                .WithMessage("Please select day.");

            RuleFor(x => x.DateEstablishedMonth)
                .NotEmpty()
                .WithMessage("Please select month.");

            RuleFor(x => x.DateEstablishedYear)
                .NotEmpty()
                .WithMessage("Please select year.");

            RuleFor(x => x.DateEstablishedDay)
                .Must((pi, day) =>
                {
                    // Parse date of birth. If succeeded, return true.
                    try { var dateOfBirth = new DateTime(pi.DateEstablishedYear.Value, pi.DateEstablishedMonth.Value, pi.DateEstablishedDay.Value); return true; }
                    catch { return false; }
                })
                .WithMessage("Invalid date of birth");

            RuleFor(x => x.IndustrySector)
                .NotEmpty()
                .WithMessage("Please input industry sector");

            // Company Address
            RuleFor(x => x.CompanyAddress.Address1)
                .NotEmpty()
                .WithMessage("Please enter address 1.");

            RuleFor(x => x.CompanyAddress.CountryId)
                .NotEmpty()
                .WithMessage("Please select country.");

            RuleFor(x => x.CompanyAddress.StateProvinceId)
                .NotEmpty()
                .When(x => x.CompanyAddress.StateProvinceRequired)
                .WithMessage("Please select county.");

            RuleFor(x => x.CompanyAddress.Postcode)
                .NotEmpty()
                .WithMessage("Please enter postcode.");

            RuleFor(x => x.CompanyAddress.LengthOfStayYears)
                .GreaterThanOrEqualTo(0)
                .WithMessage("Please enter a positive number.");

            RuleFor(x => x.CompanyAddress.LengthOfStayMonths)
                .GreaterThanOrEqualTo(0)
                .WithMessage("Please enter a positive number.");

            RuleFor(x => x.CompanyAddress.LengthOfStayMonths)
                .LessThan(12)
                .WithMessage("Max is 11.");

            When(x => x.CompanyAddress.LengthOfStayYears == 0, () =>
            {
                RuleFor(x => x.CompanyAddress.LengthOfStayMonths)
                    .GreaterThan(0)
                    .WithMessage("Invalid time at address.");
            });

            // Trading Address
            When(x => !x.TradingAddressSameAsCompanyAddress, () =>
            {
                RuleFor(x => x.TradingAddress.Address1)
                .NotEmpty()
                .WithMessage("Please enter address 1.");

                RuleFor(x => x.TradingAddress.CountryId)
                    .NotEmpty()
                    .WithMessage("Please select country.");

                RuleFor(x => x.TradingAddress.StateProvinceId)
                    .NotEmpty()
                    .When(x => x.TradingAddress.StateProvinceRequired)
                    .WithMessage("Please select county.");

                RuleFor(x => x.TradingAddress.Postcode)
                    .NotEmpty()
                    .WithMessage("Please enter postcode.");

                RuleFor(x => x.TradingAddress.LengthOfStayYears)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Please enter a positive number.");

                RuleFor(x => x.TradingAddress.LengthOfStayMonths)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Please enter a positive number.");

                RuleFor(x => x.TradingAddress.LengthOfStayMonths)
                    .LessThan(12)
                    .WithMessage("Max is 11.");

                When(x => x.TradingAddress.LengthOfStayYears == 0, () =>
                {
                    RuleFor(x => x.TradingAddress.LengthOfStayMonths)
                        .GreaterThan(0)
                        .WithMessage("Invalid time at address.");
                });
            });
        }
    }
}