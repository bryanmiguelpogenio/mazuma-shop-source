﻿using FluentValidation;
using Nop.Plugin.Ecorenew.Finance.Models.Corporate.Signup;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Ecorenew.Finance.Validators.Corporate.Signup
{
    public class AuthorizationModelValidator : BaseNopValidator<AuthorizationModel>
    {
        public AuthorizationModelValidator()
        {
            RuleFor(x => x.Authorized)
                .Equal(true)
                .WithMessage("You need to confirm authorisation.");
        }
    }
}