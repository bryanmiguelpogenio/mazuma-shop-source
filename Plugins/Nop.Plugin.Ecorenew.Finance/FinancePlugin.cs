﻿using System;
using System.Collections.Generic;
using Ecorenew.Common.DocuSign.Domain;
using Nop.Plugin.Ecorenew.Finance.Domain.Consumer;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Core.Plugins;
using Nop.Plugin.Ecorenew.Finance.Data;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Plugin.Ecorenew.Finance.Domain.Common;
using Nop.Web.Framework.Menu;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework;
using Nop.Plugin.Ecorenew.Finance.Domain.Corporate;
using Nop.Services.Messages;
using Nop.Core.Domain.Messages;

namespace Nop.Plugin.Ecorenew.Finance
{
    public class FinancePlugin : BasePlugin, IWidgetPlugin, IAdminMenuPlugin
    {
        #region Fields

        private readonly FinanceObjectContext _financeObjectContext;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly ConsumerFinanceSettings _consumerFinanceSettings;
        private readonly CorporateFinanceSettings _corporateFinanceSettings;
        private readonly DocuSignSettings _docuSignSettings;
        private readonly FinanceSettings _financeSettings;

        #endregion

        #region Ctor

        public FinancePlugin(FinanceObjectContext financeObjectContext,
            IMessageTemplateService messageTemplateService,
            ISettingService settingService,
            IWebHelper webHelper,
            ConsumerFinanceSettings consumerFinanceSettings,
            CorporateFinanceSettings corporateFinanceSettings,
            DocuSignSettings docuSignSettings,
            FinanceSettings financeSettings)
        {
            this._financeObjectContext = financeObjectContext;
            this._messageTemplateService = messageTemplateService;
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._consumerFinanceSettings = consumerFinanceSettings;
            this._corporateFinanceSettings = corporateFinanceSettings;
            this._docuSignSettings = docuSignSettings;
            this._financeSettings = financeSettings;
        }

        #endregion

        #region Utilities

        protected virtual void InstallDocuSignSettings()
        {
            _docuSignSettings.AccountId = "e485abec-8671-4683-a009-f109e8bdba80";
            _docuSignSettings.IntegratorKey = "48f7d126-7257-4fc6-b1ec-3bcd1de5f43e";
            _docuSignSettings.Password = "p@ssw0rd";
            _docuSignSettings.UserName = "928f06cc-0ed4-4b29-81f9-54759cb8e99b";

            _settingService.SaveSetting(_docuSignSettings);
        }

        protected virtual void InstallGeneralFinanceSettings()
        {
            _financeSettings.FinanceType = FinanceType.Consumer;
            _settingService.SaveSetting(_financeSettings);
        }

        protected virtual void InstallConsumerFinanceSettings()
        {
            _consumerFinanceSettings.CallReportCompany = "Ecorenew CR CTEST";
            _consumerFinanceSettings.CallReportUsername = "Ecorenew CR API CTEST";
            _consumerFinanceSettings.CallReportPassword = "Y53EW78NJ24LQ96B";
            _consumerFinanceSettings.CallReportLastPasswordChangeDateUtc = new DateTime(2018, 2, 23, 0, 14, 55);
            _consumerFinanceSettings.CallReportPassingScore = 240;
            _consumerFinanceSettings.CallReportTestApiUrl = "https://ct.callcreditsecure.co.uk/Services/CallReport/CallReport7.asmx";
            _consumerFinanceSettings.CallReportLiveApiUrl = "https://www.callcreditsecure.co.uk/Services/CallReport/CallReport7.asmx";
            _consumerFinanceSettings.UseCallReportLiveApi = false;

            _consumerFinanceSettings.CallValidateCompany = "Ecorenew CVAL CTEST";
            _consumerFinanceSettings.CallValidateUsername = "api.ctest@ecorenewfinance.com";
            _consumerFinanceSettings.CallValidatePassword = "684M4EFJa";
            _consumerFinanceSettings.CallValidateApplication = "V516-API-TEST";
            _consumerFinanceSettings.CallValidateTestApiUrl = "https://ct.callcreditsecure.co.uk/callvalidateapi/incomingserver.php";
            _consumerFinanceSettings.CallValidateLiveApiUrl = "https://www.callcreditsecure.co.uk/callvalidateapi/incomingserver.php";
            _consumerFinanceSettings.UseCallValidateLiveApi = false;

            _consumerFinanceSettings.AprPercentage = 21;
            _consumerFinanceSettings.DocumentationFeeGbp = 0;
            _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp0 = 15;
            _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp150 = 20;
            _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp200 = 25;
            _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp250 = 25;
            _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp300 = 30;
            _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp350 = 35;
            _consumerFinanceSettings.InitialPaymentGbpForProductPriceGbp400 = 40;
            _consumerFinanceSettings.OptionToPurchaseFeeGbp = 36;
            _consumerFinanceSettings.PercentageRateOfInterestPerAnnum = 19.2M;

            _consumerFinanceSettings.FinanceDocumentTemplatesPath = "~/Plugins/Ecorenew.Finance/DocumentTemplates/";
            _consumerFinanceSettings.SupplierCompanyAddress = ""; // TODO Supplier company address

            _settingService.SaveSetting(_consumerFinanceSettings);
        }

        protected virtual void InstallCorporateFinanceSettings()
        {
            _corporateFinanceSettings.InterestRate = 7;
            _corporateFinanceSettings.ProcessingCostInGbp = 8;
            _corporateFinanceSettings.FinanceDocumentTemplatesPath = "~/Plugins/Ecorenew.Finance/DocumentTemplates/";
            _settingService.SaveSetting(_corporateFinanceSettings);
        }

        protected virtual void InstallLocaleResources()
        {
            this.AddOrUpdatePluginLocaleResource("RequiredFieldTip", "* indicates required field");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Common.Address.Address1", "Address 1");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Common.Address.Address2", "Address 2");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Common.Address.BuildingNumber", "Building number or name");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Common.Address.CityTown", "City / Town");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Common.Address.Country", "Country");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Common.Address.County", "County");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Common.Address.LengthOfStay", "Length of stay");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Common.Address.MonthsOfStay", "Months");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Common.Address.PostCode", "Post code");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Common.Address.YearsOfStay", "Years");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.DropDownDefaultValue", "Please select");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.FinanceApplicationForm.Title", "Consumer Credit");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.FinanceOptionsBottomText", "<p>&nbsp;</p> <p>     The monthly cost is based on an initial payment of <span id=\"finance-monthly-deposit\"></span>, payable by card      at the time of order, followed by <span class=\"finance-months\"></span> monthly payments of <span id=\"finance-monthly-payment\"></span>.     The total amount payable is <span id=\"finance-total-payable\"></span>. APR <span class=\"finance-apr\"></span>% </p> <br /> <p>     Please note that if you decide to exercise your option to keep the phone at the end of the financing term, an option-to-purchase fee will be payable. </p> <br /> <p>     * To view a detailed breakdown of finance terms for the selected device, click <a href=\"#\" class=\"finance-consumer-compare-payment-options\">here</a>. </p>");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.ApplyForFinance", "Apply for finance");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Authorization.BeforeYouClickSubmitMessage", "Before you press Submit, please be aware that in order to assess your application, credit and fraud databases will be searched and a record of this search will be placed on your credit file.");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Authorization.IAcceptTheTermsAndConditions", "Yes, I understand that my credit file will be searched and I authorise you to do so.");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Authorization.TermsAndConditions", "<p>I confirm that the information provided by me in this application, and any other information provided by me to EcoRenew Finance Limited, is true and correct.</p> <p>In considering whether to enter into a Credit Agreement, I understand that EcoRenew Finance will attempt to verify the information provided by me and will search my credit record maintained by the credit reference agencies.</p> <p>The search will be recorded on my credit record, along with details of this application and this will be seen by other organisations that make searches. This and other information about me, and of those with whom I am linked financially, may be used to makes credit decisions about me and about those with whom I am financially linked..</p>");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Authorization.TermsAndConditions.Before", "<p>You’re nearly at the end. Just a couple more steps to go.</p><p>Read the authority statement below. If you’re happy with it, tick the box to acknowledge and then click Submit.</p>");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Authorization.Title", "Authorisation");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Bank.AccountHolder", "Name of account holder");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Bank.AccountNumber", "Account number");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Bank.AgeOfAccount", "How long have you had this account?");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Bank.AgeOfAccount.Months", "Months");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Bank.AgeOfAccount.Years", "Years");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Bank.Sortcode", "Sort code");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Bank.Title", "Bank");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Continue", "Continue");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.AnnualGrossIncome", "Annual gross income");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.EmployedEmployerName", "Employer");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.EmployedJobTitle", "Job title");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.EmployedMonthsEmployed", "Months");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.EmployedPostCode", "Post code");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.EmployedYearsEmployed", "Years");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.EmploymentStatus", "Employment status");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.LengthOtherEmploymentStatus", "How long have you been doing this?");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.LengthSelfEmployed", "How long have you been doing this?");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.OtherMonthsLength", "Months");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.OtherYearsLength", "Years");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.SelfEmployedMonthsLength", "Months");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.SelfEmployedNatureOfBusiness", "Nature of business");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.SelfEmployedPostCode", "Post code");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.SelfEmployedTradingAs", "Trading as");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.SelfEmployedYearsLength", "Years");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.TimeEmployedTitle", "Time employed");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Employment.Title", "Employment");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.HomeAddress.CurrentAddressTitle", "Current Address");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.HomeAddress.HomeOwnerStatus", "Home owner status");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.HomeAddress.IsPermanentUkResident", "Are you a UK resident?");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.HomeAddress.PreviousAddressTitle", "Previous Address");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.HomeAddress.PreviousHint", "As you’ve been at your current address for less than 3 years, we’ll need to know your previous address. Please complete the section below.");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.HomeAddress.Title", "Home Address");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.IncomeAndExpenditure.AbilityToPay", "I confirm that I have considered potential future income and outgoings in determining my ability to  pay.");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.IncomeAndExpenditure.AboveIsCorrect", "I confirm that all the income and expense information entered above is correct.");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.IncomeAndExpenditure.IncomeAmount", "Monthly Income Amount (after tax)");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.IncomeAndExpenditure.MonthlyIncome.Tooltip", "<small><p>Your main source of income after tax (if applicable.)</p><p>If you are not paid monthly (e.g. weekly) then please enter the equivalent monthly amount.</p><p>If you have chosen ‘Retired’ as your Employment Type, please enter the amount received from any pension or other income.</p></small>");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.IncomeAndExpenditure.PleaseEnterAccurateMonthlyExpenses", "Please enter an accurate estimate of your monthly expenses.");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.IncomeAndExpenditure.Title", "Income and Expenditure");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Index.Heading", "Welcome to the EcoRenew Finance Application");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Index.TermsAndConditions", "<p>Finance is provided by EcoRenew Finance Limited. Registered in England and Wales 07466832. Registered Office:  Unit C1, The Maltings, Station Road, Sawbridgeworth, Hertfordshire CM21 9JX. </p>  <p>EcoRenew Finance Limited is authorised and regulated by the UK Financial Conduct Authority. Reference Number: 728034.  Data Protection Licence Number: ZA136818. All finance terms and offers are subject to status after credit and affordability checks. All finance terms require an initial payment to be paid via debit or credit card. </p>");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Index.TermsAndConditions.Heading", "Terms and Conditions");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.ConfirmEmail", "Confirm email");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.ContactDetailsTitle", "Contact Details");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.DateOfBirth", "Date of birth");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.Email", "Email");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.FirstName", "First name");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.LastName", "Last name");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.MaritalStatus", "Marital status");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.MiddleName", "Middle name");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.MobileNumber", "Mobile number");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.Name", "Name");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.NumberOfDependents", "Number of dependents");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.Password.Notice", "It looks like you don’t have an account with us yet. You’ll need to register in order to track your order/s. Please input a password for your account.");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.Password.Tooltip", "<small> <p><b>Password criteria:</b>     <ul>         <li>             - Should have at least 6 characters         </li>         <li>             - Should contain At least one lower case letter         </li>         <li>             - Should contain At least one upper case letter         </li>         <li>             - Should contain At least one numeric value         </li></ul> </p> </small>");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalDetails.Title", "Title");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.PersonalInformation.Title", "Personal Information");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Previous", "Previous");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Proceed", "Proceed");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Consumer.Signup.Submit", "Submit");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.FinanceApplicationForm.Title", "Corporate Credit");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Authorization.BeforeYouClickSubmitMessage", "Before you press Submit, please be aware that in order to assess your application, credit and fraud databases will be searched and a record of this search will be placed on your credit file.");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Authorization.IAcceptTheTermsAndConditions", "Yes, I understand that my credit file will be searched and I authorise you to do so.");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Authorization.TermsAndConditions", "<p>I confirm that the information provided by me in this application, and any other information provided by me to EcoRenew Finance Limited, is true and correct.</p> <p>In considering whether to enter into a Credit Agreement, I understand that EcoRenew Finance will attempt to verify the information provided by me and will search my credit record maintained by the credit reference agencies.</p> <p>The search will be recorded on my credit record, along with details of this application and this will be seen by other organisations that make searches. This and other information about me, and of those with whom I am linked financially, may be used to makes credit decisions about me and about those with whom I am financially linked..</p>");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Authorization.TermsAndConditions.Before", "<p>You’re nearly at the end. Just a couple more steps to go.</p><p>Read the authority statement below. If you’re happy with it, tick the box to acknowledge and then click Submit.</p>");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Authorization.Title", "Authorisation");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Bank.AccountHolder", "Name of account holder");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Bank.CompanyName", "Company name");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Bank.TradingName", "Trading as");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Bank.AccountNumber", "Account number");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Bank.AgeOfAccount", "How long have you had this account?");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Bank.AgeOfAccount.Months", "Months");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Bank.AgeOfAccount.Years", "Years");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Bank.Sortcode", "Sort code");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Bank.Title", "Bank");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.BasicDetails.CompanyAddressTitle", "Registered address");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.BasicDetails.CompanyName", "Company name");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.BasicDetails.CompanyRegistrationNumber", "Registration number");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.BasicDetails.DateEstablished", "Date established");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.BasicDetails.IndustrySector", "Industry sector");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.BasicDetails.Title", "Basic Details");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.BasicDetails.TadingAddressSameAsCompanyAddress", "Same as company address");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.BasicDetails.TradingAddressTitle", "Trading address");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.BasicDetails.TradingAs", "Trading as");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.BasicDetails.VatNumber", "VAT number");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Continue", "Continue");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.ConfirmEmail", "Confirm email");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.ContactDetailsTitle", "Contact Details");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.DateOfBirth", "Date of birth");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.Email", "Email");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.FirstName", "First name");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.HomeAddress.CurrentAddressTitle", "Current Address");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.HomeAddress.PreviousAddressTitle", "Previous Address");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.HomeAddress.PreviousHint", "As you’ve been at your current address for less than 3 years, we’ll need to know your previous address. Please complete the section below.");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.HomeOwnerStatus", "Home owner status");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.IsPermanentUkResident", "Are you a UK resident?");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.LastName", "Last name");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.MaritalStatus", "Marital status");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.MiddleName", "Middle name");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.MobileNumber", "Mobile number");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.Name", "Name");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.NumberOfDependents", "Number of dependents");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.Password.Notice", "It looks like you don’t have an account with us yet. You’ll need to register in order to track your order/s. Please input a password for your account.");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.Password.Tooltip", "<small> <p><b>Password criteria:</b>     <ul>         <li>             - Should have at least 6 characters         </li>         <li>             - Should contain At least one lower case letter         </li>         <li>             - Should contain At least one upper case letter         </li>         <li>             - Should contain At least one numeric value         </li></ul> </p> </small>");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.PersonalInfomationTitle", "Personal Information");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.DirectorDetails.Title", "Director Details");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Index.Heading", "Welcome to the EcoRenew Finance Application");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Index.TermsAndConditions", "<p>Finance is provided by EcoRenew Finance Limited. Registered in England and Wales 07466832. Registered Office:  Unit C1, The Maltings, Station Road, Sawbridgeworth, Hertfordshire CM21 9JX. </p>  <p>EcoRenew Finance Limited is authorised and regulated by the UK Financial Conduct Authority. Reference Number: 728034.  Data Protection Licence Number: ZA136818. All finance terms and offers are subject to status after credit and affordability checks. All finance terms require an initial payment to be paid via debit or credit card. </p>");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Index.TermsAndConditions.Heading", "Terms and Conditions");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Previous", "Previous");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Proceed", "Proceed");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Corporate.Signup.Submit", "Submit");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.ExpenditureCategory.Food", "Food:");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.ExpenditureCategory.MonthlyCreditCommitments", "Monthly credit commitments:");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.ExpenditureCategory.Others", "Others:");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.ExpenditureCategory.RentMortgage", "Rent mortgage:");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.ExpenditureCategory.Transportation", "Transportation:");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.ExpenditureCategory.UtilitiesBills", "Utilities bills:");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.ShoppingCart.SelectFinanceOption", "Months to pay");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.ShoppingCart.SelectPaymentMode", "Select payment mode");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.Applications.ApprovalStatus", "Approval status");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Settings.FinanceType", "Finance type");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.Applications.Title", "B2B Applications");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.Applications.NoApprovalStatusesAvailable", "No approval statuses available");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.Applications.Fields.ApprovalStatus", "Approval status");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.Applications.Fields.StoreName", "Store");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.Applications.Fields.CompanyName", "Company name");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.Applications.Fields.CreatedOn", "Created on");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.Applications.Fields.ViewUpdate", "View / Update");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Settings.Title", "Finance Settings");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Settings.GeneralSettings.TabTitle", "General Settings");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Settings.ConsumerFinanceSettings.TabTitle", "B2C Settings");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Settings.CorporateFinanceSettings.TabTitle", "B2B Settings");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdate.Title", "View / Update B2B Application");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdate.Approve", "Approve");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdate.Disapprove", "Disapprove");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdate.BasicDetails.TabTitle", "Basic Details");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdate.DirectorDetails.TabTitle", "Director Details");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdate.BankDetails.TabTitle", "Bank Details");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdate.OrderedItems.TabTitle", "Ordered Items");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdateApplication.Fields.ProductSku", "SKU");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdateApplication.Fields.ProductName", "Product");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdateApplications.Fields.PaymentTerm", "Payment Term");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdateApplications.Fields.Quantity", "Quantity");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdateApplications.Fields.PaymentPerUnit", "Payment Per Unit");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdateApplications.Fields.PaymentSubTotal", "Payment Sub Total");
            this.AddOrUpdatePluginLocaleResource("Ecorenew.Finance.Admin.Corporate.ViewUpdateApplications.Fields.TotalPayable", "Total Payable");
        }

        protected virtual void InstallCorporateMessageTemplates()
        {
            if (_messageTemplateService.GetMessageTemplateByName(CorporateMessageTemplateSystemNames.CorporateSignupApporvedCustomerNotification, 0) == null)
            {
                _messageTemplateService.InsertMessageTemplate(new MessageTemplate
                {
                    Name = CorporateMessageTemplateSystemNames.CorporateSignupApporvedCustomerNotification,
                    Subject = "%Store.Name% - Your application has been approved",
                    Body = "<h3>Congratulations! Your request for finance has been approved!</h3><p>Hi %Customer.FullName%,<p><p>....</p><p>Click the link below to proceed with your application:</p><a href=\"%Signup.AgreementLink%\">%Signup.AgreementLink%</a>",
                    IsActive = true,
                    EmailAccountId = 1
                });
            }
        }

        #endregion

        #region Methods

        public override void Install()
        {
            _financeObjectContext.Install();

            InstallDocuSignSettings();

            InstallGeneralFinanceSettings();
            InstallConsumerFinanceSettings();
            InstallCorporateFinanceSettings();
            
            InstallLocaleResources();

            InstallCorporateMessageTemplates();

            base.Install();
        }

        public override void Uninstall()
        {
#if DEBUG
            _financeObjectContext.Uninstall();
            _settingService.DeleteSetting<FinanceSettings>();
            _settingService.DeleteSetting<ConsumerFinanceSettings>();
            _settingService.DeleteSetting<CorporateFinanceSettings>();
#endif

            base.Uninstall();
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/EcorenewFinanceAdmin/Settings";
        }

        public void GetPublicViewComponent(string widgetZone, out string viewComponentName)
        {
            switch (widgetZone)
            {
                case "ecorenew_finance_productdetails_financebreakdown":
                    viewComponentName = _financeSettings.FinanceType == FinanceType.Consumer ? "ProductDetailsConsumerFinanceBreakdown" : "ProductDetailsCorporateFinanceBreakdown";
                    break;
                case "ecorenew_finance_productdetails_financebreakdown_info":
                    viewComponentName = _financeSettings.FinanceType == FinanceType.Consumer ? "ProductDetailsConsumerFinanceBreakdownInfo" : "EcorenewFinanceEmpty";
                    break;
                case "body_end_html_tag_before":
                    viewComponentName = _financeSettings.FinanceType == FinanceType.Consumer ? "ConsumerFinanceBreakdownPopup" : "CorporateFinanceBreakdownPopup";
                    break;
                case "order_summary_cart_footer":
                    viewComponentName = _financeSettings.FinanceType == FinanceType.Consumer ? "ConsumerPaymentTermSelector" : "CorporatePaymentTermSelector";
                    break;
                case "orderdetails_page_top":
                    viewComponentName = _financeSettings.FinanceType == FinanceType.Consumer ? "ConsumerFinanceAgreementDocuments" : "CorporateFinanceAgreementDocuments";
                    break;
                default:
                    viewComponentName = "EcorenewFinanceEmpty";
                    break;
            }
        }

        public IList<string> GetWidgetZones()
        {
            return new List<string>()
            {
                "ecorenew_finance_productdetails_financebreakdown",
                "ecorenew_finance_productdetails_financebreakdown_info",
                "body_end_html_tag_before",
                "order_summary_cart_footer",
                "orderdetails_page_top"
            };
        }

        public static bool IsInstalledAndEnabled(int storeId = 0)
        {
            var pluginFinder = EngineContext.Current.Resolve<IPluginFinder>();
            var storeContext = EngineContext.Current.Resolve<IStoreContext>();

            storeId = storeId > 0 ? storeId : storeContext.CurrentStore.Id;

            var financePlugin = pluginFinder.GetPluginDescriptorBySystemName("Ecorenew.Finance");
            return (financePlugin != null && financePlugin.Installed && pluginFinder.AuthenticateStore(financePlugin, storeId));
        }

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var mainNode = new SiteMapNode()
            {
                SystemName = "Ecorenew",
                Title = "EcoRenew Group",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
                IconClass = "fa-leaf"
            };

            var financeNode = new SiteMapNode()
            {
                SystemName = "Ecorenew.Finance",
                Title = "Finance",
                Visible = true,
                IconClass = "fa-usd"
            };

            mainNode.ChildNodes.Add(financeNode);

            var financeSettingsNode = new SiteMapNode()
            {
                SystemName = "Ecorenew.Finance.Settings",
                Title = "Settings",
                Visible = true,
                ControllerName = "EcorenewFinanceAdmin",
                ActionName = "Settings",
                RouteValues = new RouteValueDictionary() { { "area", AreaNames.Admin } },
                IconClass = "fa-genderless"
            };

            financeNode.ChildNodes.Add(financeSettingsNode);

            var financeCorporateNode = new SiteMapNode()
            {
                SystemName = "Ecorenew.Finance.Corporate.Applications",
                Title = "B2B Applications",
                Visible = true,
                ControllerName = "EcorenewFinanceAdmin",
                ActionName = "CorporateApplications",
                RouteValues = new RouteValueDictionary() { { "area", AreaNames.Admin } },
                IconClass = "fa-genderless"
            };

            financeNode.ChildNodes.Add(financeCorporateNode);

            rootNode.ChildNodes.Add(mainNode);
        }

        #endregion
    }
}