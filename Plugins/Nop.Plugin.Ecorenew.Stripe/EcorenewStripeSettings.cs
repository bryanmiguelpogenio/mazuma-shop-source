﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Ecorenew.Stripe
{
    public class EcorenewStripeSettings : ISettings
    {
        public string SecretApiKey { get; set; }
        public string PublicApiKey { get; set; }
        public bool AuthorizeOnly { get; set; }
        public bool CanRePostPayment { get; set; }
        public bool ConfirmPayment { get; set; }
    }
}