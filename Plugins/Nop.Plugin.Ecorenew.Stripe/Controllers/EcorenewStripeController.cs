﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Ecorenew.Stripe.Models;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;

using StripeCustomerService = Stripe.CustomerService;

namespace Nop.Plugin.Ecorenew.Stripe.Controllers
{
    public class EcorenewStripeController : BasePaymentController
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly ICustomerService _customerService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreService _storeService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly ICurrencyService _currencyService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly CurrencySettings _currencySettings;
        private readonly string[] _zeroDecimalCurrencyCodes = { "bif", "clp", "djf", "gnf", "jpy", "kmf", "krw", "mga", "pyg", "rwf", "ugx", "vnd", "vuv", "xaf", "xof", "xpf" };
        #endregion

        #region Ctor

        public EcorenewStripeController(ILocalizationService localizationService,
            ICustomerService customerService,
            IPermissionService permissionService,
            ISettingService settingService,
            IStoreService storeService,
            IStoreContext storeContext,
            IWorkContext workContext,
            ICurrencyService currencyService,
            IOrderTotalCalculationService orderTotalCalculationService,
            CurrencySettings currencySettings)
        {
            this._localizationService = localizationService;
            this._customerService = customerService;
            this._permissionService = permissionService;
            this._settingService = settingService;
            this._storeService = storeService;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._currencyService = currencyService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._currencySettings = currencySettings;
        }

        #endregion

        #region Utilities

        protected virtual int ConvertToStripeAmount(decimal amount, string currencyCode)
        {
            decimal result = RoundingHelper.RoundPrice(amount);

            if (!_zeroDecimalCurrencyCodes.Contains(currencyCode.ToLower()))
                result *= 100;

            return Convert.ToInt32(result);
        }

        #endregion

        #region Methods

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var ecorenewStripeSettings = _settingService.LoadSetting<EcorenewStripeSettings>(storeScope);

            var model = new ConfigurationModel
            {
                AuthorizeOnly = ecorenewStripeSettings.AuthorizeOnly,
                PublicApiKey = ecorenewStripeSettings.PublicApiKey,
                SecretApiKey = ecorenewStripeSettings.SecretApiKey,
                CanRePostPayment = ecorenewStripeSettings.CanRePostPayment,
                ConfirmPayment = ecorenewStripeSettings.ConfirmPayment,
                ActiveStoreScopeConfiguration = storeScope
            };

            if (storeScope > 0)
            {
                model.AuthorizeOnly_OverrideForStore = _settingService.SettingExists(ecorenewStripeSettings, x => x.AuthorizeOnly, storeScope);
                model.PublicApiKey_OverrideForStore = _settingService.SettingExists(ecorenewStripeSettings, x => x.PublicApiKey, storeScope);
                model.SecretApiKey_OverrideForStore = _settingService.SettingExists(ecorenewStripeSettings, x => x.SecretApiKey, storeScope);
                model.CanRePostPayment_OverrideForStore = _settingService.SettingExists(ecorenewStripeSettings, x => x.CanRePostPayment, storeScope);
                model.ConfirmPayment_OverrideForStore = _settingService.SettingExists(ecorenewStripeSettings, x => x.ConfirmPayment, storeScope);
            }

            return View("~/Plugins/Ecorenew.Stripe/Views/Configure.cshtml", model);
        }

        [HttpPost, ActionName("Configure")]
        [FormValueRequired("save")]
        [AuthorizeAdmin]
        [AdminAntiForgery]
        [Area(AreaNames.Admin)]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var ecorenewStripeSettings = _settingService.LoadSetting<EcorenewStripeSettings>(storeScope);

            //save settings
            ecorenewStripeSettings.AuthorizeOnly = model.AuthorizeOnly;
            ecorenewStripeSettings.PublicApiKey = model.PublicApiKey;
            ecorenewStripeSettings.SecretApiKey = model.SecretApiKey;
            ecorenewStripeSettings.CanRePostPayment = model.CanRePostPayment;
            ecorenewStripeSettings.ConfirmPayment = model.ConfirmPayment;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(ecorenewStripeSettings, x => x.AuthorizeOnly, model.AuthorizeOnly_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(ecorenewStripeSettings, x => x.PublicApiKey, model.PublicApiKey_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(ecorenewStripeSettings, x => x.SecretApiKey, model.SecretApiKey_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(ecorenewStripeSettings, x => x.CanRePostPayment, model.CanRePostPayment_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(ecorenewStripeSettings, x => x.ConfirmPayment, model.ConfirmPayment_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        [HttpPost]
        public JsonResult CreatePaymentIntent()
        {
            // load settings for a chosen store scope
            var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var ecorenewStripeSettings = _settingService.LoadSetting<EcorenewStripeSettings>(storeScope);

            // Set your secret key: remember to change this to your live secret key in production
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            StripeConfiguration.SetApiKey(ecorenewStripeSettings.SecretApiKey);

            #region Amount

            var currency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);

            var cart = _workContext.CurrentCustomer.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id).ToList();

            if (!cart.Any())
                throw new NopException("Cart is empty");

            //order total (and applied discounts, gift cards, reward points)
            var orderTotal = _orderTotalCalculationService.GetShoppingCartTotal(cart, out decimal orderDiscountAmount, out List<DiscountForCaching> orderAppliedDiscounts, out List<AppliedGiftCard> appliedGiftCards, out int redeemedRewardPoints, out decimal redeemedRewardPointsAmount);
            if (!orderTotal.HasValue)
                throw new NopException("Order total couldn't be calculated");

            var amount = ConvertToStripeAmount(orderTotal.Value, currency.CurrencyCode);

            #endregion

            #region Customer

            var customer = _customerService.GetCustomerById(_workContext.CurrentCustomer.Id);
            if (customer == null)
                throw new Exception("Customer cannot be loaded");

            var customerService = new StripeCustomerService();
            var stripeCustomer = new Customer();

            if (customer.Email == null)
            {
                var customerCreateOptions = new CustomerCreateOptions
                {
                    Name = "Guest",
                    Description = $"Customer account for Customer #: {customer.Id}",
                };
                stripeCustomer = customerService.Create(customerCreateOptions);
            }
            else
            {
                // Check if email address already has a customer account.
                var customerListOptions = new CustomerListOptions
                {
                    Email = customer.Email
                };
                var customers = customerService.List(customerListOptions);

                if (customers.Count() < 0)
                {
                    // Create customer account for the email in stripe.
                    var customerCreateOptions = new CustomerCreateOptions
                    {
                        Name = customer.GetFullName(),
                        Email = customer.Email,
                        Description = $"Customer account for Customer #: {customer.Id}",
                    };
                    stripeCustomer = customerService.Create(customerCreateOptions);
                }
                else
                    stripeCustomer = customers.FirstOrDefault();
            }

            #endregion

            #region Payment Intent

            var paymentIntentService = new PaymentIntentService();
            var paymentIntentCreateOptions = new PaymentIntentCreateOptions
            {
                Amount = amount,
                Currency = currency.CurrencyCode.ToLower(),
                CustomerId = stripeCustomer.Id
            };

            #endregion

            return Json(new { intent = paymentIntentService.Create(paymentIntentCreateOptions) });
        }
        #endregion
    }
}