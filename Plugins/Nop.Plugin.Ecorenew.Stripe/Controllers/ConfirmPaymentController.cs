﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Orders;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;

using StripeCustomerService = Stripe.CustomerService;

namespace Nop.Plugin.Ecorenew.Stripe.Controllers
{
    public class ConfirmPaymentRequest
    {
        [JsonProperty("payment_method_id")]
        public string PaymentMethodId { get; set; }

        [JsonProperty("payment_intent_id")]
        public string PaymentIntentId { get; set; }

        [JsonProperty("secret_api_key")]
        public string SecretApiKey { get; set; }

        [JsonProperty("customer_id")]
        public string CustomerId { get; set; }

        [JsonProperty("mazuma_customer_id")]
        public string MazumaCustomerId { get; set; }
    }

    [Route("/ajax/confirm_payment")]
    public class ConfirmPaymentController : Controller
    {
        #region Fields

        private readonly IWorkContext _workContext;
        private readonly ICurrencyService _currencyService;
        private readonly IStoreContext _storeContext;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ICustomerService _customerService;
        private readonly CurrencySettings _currencySettings;
        private readonly EcorenewStripeSettings _ecorenewStripeSettings;
        private readonly string[] _zeroDecimalCurrencyCodes = { "bif", "clp", "djf", "gnf", "jpy", "kmf", "krw", "mga", "pyg", "rwf", "ugx", "vnd", "vuv", "xaf", "xof", "xpf" };

        #endregion

        #region Ctor

        public ConfirmPaymentController(
            IWorkContext workContext,
            ICurrencyService currencyService,
            IStoreContext storeContext,
            IOrderTotalCalculationService orderTotalCalculationService,
            ICustomerService customerService,
            CurrencySettings currencySettings,
            EcorenewStripeSettings ecorenewStripeSettings)
        {
            this._workContext = workContext;
            this._currencyService = currencyService;
            this._storeContext = storeContext;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._customerService = customerService;
            this._currencySettings = currencySettings;
            this._ecorenewStripeSettings = ecorenewStripeSettings;
        }

        #endregion

        #region Utilities

        protected virtual int ConvertToStripeAmount(decimal amount, string currencyCode)
        {
            decimal result = RoundingHelper.RoundPrice(amount);

            if (!_zeroDecimalCurrencyCodes.Contains(currencyCode.ToLower()))
                result *= 100;

            return Convert.ToInt32(result);
        }

        #endregion

        #region Methods

        public IActionResult Index([FromBody] ConfirmPaymentRequest request)
        {
            StripeConfiguration.SetApiKey(_ecorenewStripeSettings.SecretApiKey);

            var paymentIntentService = new PaymentIntentService();
            PaymentIntent paymentIntent = null;

            try
            {
                var currency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
                //var cart = _workContext.CurrentCustomer.ShoppingCartItems
                //    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                //    .LimitPerStore(_storeContext.CurrentStore.Id)
                //    .ToList();

                var customerId = 0;
                int.TryParse(request.MazumaCustomerId, out customerId);
                var cart = _customerService.GetCustomerById(customerId).ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();

                if (!cart.Any())
                    return Json(new { error = "Cart is empty" });

                // Order total (and applied discounts, gift cards, reward points)
                var orderTotal = _orderTotalCalculationService.GetShoppingCartTotal(cart, out decimal orderDiscountAmount, out List<DiscountForCaching> orderAppliedDiscounts, out List<AppliedGiftCard> appliedGiftCards, out int redeemedRewardPoints, out decimal redeemedRewardPointsAmount);
                if (!orderTotal.HasValue)
                    return Json(new { error = "Order total couldn't be calculated" });

                if (request.CustomerId == null)
                {
                    var customer = _customerService.GetCustomerById(customerId);
                    if (customer == null)
                        return Json(new { error = "Customer cannot be loaded" });

                    var customerService = new StripeCustomerService();

                    var stripeCustomer = new Customer();
                    if (customer.Email == null)
                    {
                        var customerCreateOptions = new CustomerCreateOptions
                        {
                            Name = "Guest",
                            Description = $"Customer account for Customer #: {customer.Id}",
                        };
                        stripeCustomer = customerService.Create(customerCreateOptions);
                    }
                    else
                    {
                        // Check if email address already has a customer account.
                        var customerListOptions = new CustomerListOptions
                        {
                            Email = customer.Email
                        };
                        var customers = customerService.List(customerListOptions);

                        if (!customers.Any())
                        {
                            // Create customer account for the email in stripe.
                            var customerCreateOptions = new CustomerCreateOptions
                            {
                                Name = customer.GetFullName(),
                                Email = customer.Email,
                                Description = $"Customer account for Customer #: {customer.Id}",
                            };
                            stripeCustomer = customerService.Create(customerCreateOptions);
                        }
                        else
                            stripeCustomer = customers.FirstOrDefault();
                    }

                    request.CustomerId = stripeCustomer.Id;
                }

                if (request.PaymentMethodId != null)
                {
                    // Create the PaymentIntent
                    var createOptions = new PaymentIntentCreateOptions
                    {
                        PaymentMethodId = request.PaymentMethodId,
                        Amount = ConvertToStripeAmount(orderTotal.Value, currency.CurrencyCode),
                        Currency = currency.CurrencyCode.ToLower(),
                        ConfirmationMethod = "manual",
                        Confirm = _ecorenewStripeSettings.ConfirmPayment,
                        CustomerId = request.CustomerId
                    };
                    paymentIntent = paymentIntentService.Create(createOptions);
                }

                if (request.PaymentIntentId != null)
                {
                    var confirmOptions = new PaymentIntentConfirmOptions { };
                    paymentIntent = paymentIntentService.Confirm(
                        request.PaymentIntentId,
                        confirmOptions
                    );
                }
            }
            catch (StripeException e)
            {
                return Json(new { error = e.StripeError.Message });
            }

            return generatePaymentResponse(paymentIntent);
        }

        private IActionResult generatePaymentResponse(PaymentIntent intent)
        {
            if (intent.Status == "requires_action" &&
                intent.NextAction.Type == "use_stripe_sdk")
            {
                // Tell the client to handle the action
                return Json(new
                {
                    requires_action = true,
                    payment_intent_client_secret = intent.ClientSecret,
                    customer_id = intent.CustomerId
                });
            }
            else if (intent.Status == "succeeded")
            {
                // The payment didn’t need any additional actions and completed!
                // Handle post-payment fulfillment
                return Json(new
                {
                    success = true,
                    payment_intent_id = intent.Id,
                    customer_id = intent.CustomerId
                });
            }
            else
            {
                // Invalid status
                return Json(new { error = "Invalid PaymentIntent status" });
            }
        }

        #endregion
    }
}