﻿using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Ecorenew.Stripe.Models
{
    public class ConfigurationModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.Stripe.Fields.SecretApiKey")]
        public string SecretApiKey { get; set; }
        public bool SecretApiKey_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.Stripe.Fields.PublicApiKey")]
        public string PublicApiKey { get; set; }
        public bool PublicApiKey_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.Stripe.Fields.AuthorizeOnly")]
        public bool AuthorizeOnly { get; set; }
        public bool AuthorizeOnly_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.Stripe.Fields.CanRePostPayment")]
        public bool CanRePostPayment { get; set; }
        public bool CanRePostPayment_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.Stripe.Fields.ConfirmPayment")]
        public bool ConfirmPayment { get; set; }
        public bool ConfirmPayment_OverrideForStore { get; set; }
    }
}