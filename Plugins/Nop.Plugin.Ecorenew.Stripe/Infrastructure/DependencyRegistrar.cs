﻿using Autofac;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Services.Payments;

namespace Nop.Plugin.Ecorenew.Stripe.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            // overridden services
            builder.RegisterType<Services.StripePaymentService>()
                .As<IPaymentService>()
                .InstancePerLifetimeScope();
        }
        public int Order
        {
            get { return 1; }
        }
    }
}
