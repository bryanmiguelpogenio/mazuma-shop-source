﻿
using Stripe;
using StripeCustomerService = Stripe.CustomerService;

namespace Nop.Plugin.Ecorenew.Stripe
{
    public class EcorenewStripeApi
    {
        public StripeList<Customer> GetStripeCustomers(string emailAddress, string secretApiKey)
        {
            var customerListOptions = new CustomerListOptions
            {
                Email = emailAddress
            };

            var service = new StripeCustomerService();
            return service.List(customerListOptions, new RequestOptions { ApiKey = secretApiKey });
        }

        public Customer CreateCustomer(string emailAddress, string secretApiKey)
        {
            var customerCreateOptions = new CustomerCreateOptions
            {
                Email = emailAddress,
                Description = $"Customer account for {emailAddress}"
            };

            var service = new StripeCustomerService();
            return service.Create(customerCreateOptions, new RequestOptions { ApiKey = secretApiKey });
        }

        public PaymentIntent CreatePaymentIntent(int amount, string currencyCode, string secretApiKey, bool confirm, string customerId)
        {
            var service = new PaymentIntentService();
            var options = new PaymentIntentCreateOptions
            {
                Amount = amount,
                Currency = currencyCode.ToLower(),
                //CustomerId = customerId
            };

            return service.Create(options, new RequestOptions { ApiKey = secretApiKey });
        }
    }
}