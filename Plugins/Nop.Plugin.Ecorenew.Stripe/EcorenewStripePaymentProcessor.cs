﻿using Microsoft.AspNetCore.Http;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
//using Nop.Core.Domain.Customers;

namespace Nop.Plugin.Ecorenew.Stripe
{
    public class EcorenewStripePaymentProcessor : BasePlugin, IPaymentMethod
    {
        private readonly ICurrencyService _currencyService;
        private readonly ICustomerService _customerService;
        private readonly ILocalizationService _localizationService;
        private readonly ILogger _logger;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly IOrderService _orderService;
        private readonly CurrencySettings _currencySettings;
        private readonly EcorenewStripeSettings _ecorenewStripeSettings;

        private readonly string[] _zeroDecimalCurrencyCodes = { "bif", "clp", "djf", "gnf", "jpy", "kmf", "krw", "mga", "pyg", "rwf", "ugx", "vnd", "vuv", "xaf", "xof", "xpf" };

        #region Ctor

        public EcorenewStripePaymentProcessor(ICurrencyService currencyService,
            ICustomerService customerService,
            ILocalizationService localizationService,
            ILogger logger,
            ISettingService settingService,
            IStoreContext storeContext,
            IWebHelper webHelper,
            IWorkContext workContext,
            IOrderService orderService,
            CurrencySettings currencySettings,
            EcorenewStripeSettings ecorenewStripeSettings)
        {
            this._currencyService = currencyService;
            this._customerService = customerService;
            this._localizationService = localizationService;
            this._logger = logger;
            this._settingService = settingService;
            this._storeContext = storeContext;
            this._webHelper = webHelper;
            this._workContext = workContext;
            this._orderService = orderService;
            this._currencySettings = currencySettings;
            this._ecorenewStripeSettings = ecorenewStripeSettings;
        }

        #endregion

        #region Utilities

        protected virtual int ConvertToStripeAmount(decimal amount, string currencyCode)
        {
            decimal result = RoundingHelper.RoundPrice(amount);

            if (!_zeroDecimalCurrencyCodes.Contains(currencyCode.ToLower()))
                result *= 100;

            return Convert.ToInt32(result);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();

            try
            {
                string paymentIntentToken = processPaymentRequest.CustomValues["paymenttoken"].ToString();

                _logger.InsertLog(Core.Domain.Logging.LogLevel.Information,
                    "Stripe: Process Payment",
                    $"Processing Payment for payment intent: {paymentIntentToken} started.");

                StripeConfiguration.SetApiKey(_ecorenewStripeSettings.SecretApiKey);

                var service = new PaymentIntentService();
                var paymentIntent = service.Get(paymentIntentToken);

                //if (_ecorenewStripeSettings.AuthorizeOnly)
                //{
                //    result.AuthorizationTransactionId = paymentIntent.Id;
                //    result.AuthorizationTransactionResult = paymentIntent.Status;

                //    // always set the status to paid since payment is being made through Payment Intent.
                //    result.NewPaymentStatus = PaymentStatus.Authorized;
                //}
                //else
                //{
                //    result.CaptureTransactionId = paymentIntent.Id;
                //    result.CaptureTransactionResult = paymentIntent.Status;
                //    result.NewPaymentStatus = PaymentStatus.Paid;
                //}

                result.CaptureTransactionId = paymentIntent.Id;
                result.CaptureTransactionResult = paymentIntent.Status;

                // always set the status to paid since payment is being made through Payment Intent.
                result.NewPaymentStatus = PaymentStatus.Paid;

                _logger.InsertLog(Core.Domain.Logging.LogLevel.Information,
                    "Stripe: Process Payment",
                    $"Processing Payment for payment intent: {paymentIntentToken} ended.");
            }
            catch (Exception ex)
            {
                _logger.InsertLog(Core.Domain.Logging.LogLevel.Error, "Stripe: Process Payment", ex.Message);
                result.AddError(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            try
            {
                // This function is for authorized payments only.
                _logger.InsertLog(Core.Domain.Logging.LogLevel.Information,
                    "Stripe: Post Process Payment",
                    $"Authorize. Post Process Payment for {postProcessPaymentRequest.Order.CustomOrderNumber} started.");

                StripeConfiguration.SetApiKey(_ecorenewStripeSettings.SecretApiKey);

                var options = new PaymentIntentUpdateOptions()
                {
                    Metadata = new Dictionary<string, string>
                    {
                        { "order_number", postProcessPaymentRequest.Order.CustomOrderNumber }
                    }
                };

                var service = new PaymentIntentService();
                service.Update(postProcessPaymentRequest.Order.CaptureTransactionId, options);

                //postProcessPaymentRequest.Order.PaymentStatus = PaymentStatus.Paid;
                //_orderService.UpdateOrder(postProcessPaymentRequest.Order);

                _logger.InsertLog(Core.Domain.Logging.LogLevel.Information,
                    "Stripe: Post Process Payment",
                    $"Post Process Payment for {postProcessPaymentRequest.Order.CustomOrderNumber} ended.");
            }
            catch (Exception ex)
            {
                _logger.InsertLog(Core.Domain.Logging.LogLevel.Error, "Stripe: Post Process Payment", ex.Message);
            }

        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shopping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            return String.IsNullOrWhiteSpace(_ecorenewStripeSettings.PublicApiKey) || String.IsNullOrWhiteSpace(_ecorenewStripeSettings.SecretApiKey);
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <param name="cart">Shopping cart</param>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            return 0;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();

            try
            {
                StripeConfiguration.SetApiKey(_ecorenewStripeSettings.SecretApiKey);

                var paymentIntentService = new PaymentIntentService();
                var paymentIntent = paymentIntentService.Get(refundPaymentRequest.Order.CaptureTransactionId);

                var refundCreateOptions = new RefundCreateOptions
                {
                    ChargeId = paymentIntent.Charges.Data[0].Id
                };

                if (refundPaymentRequest.IsPartialRefund)
                {
                    var currency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
                    refundCreateOptions.Amount = ConvertToStripeAmount(refundPaymentRequest.AmountToRefund, currency.CurrencyCode);
                }

                var refundService = new RefundService();
                var refund = refundService.Create(refundCreateOptions);

                if (refund.Status != "succeeded")
                {
                    // TODO apply more appropriate error message
                    result.AddError("Payment declined");
                }

                result.NewPaymentStatus = refundPaymentRequest.IsPartialRefund ? PaymentStatus.PartiallyRefunded : PaymentStatus.Refunded;
            }
            catch (Exception ex)
            {
                _logger.InsertLog(Core.Domain.Logging.LogLevel.Error, "Stripe: Refund", ex.Message);

                result.AddError(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();

            try
            {
                var service = new RefundService();
                var refund = service.Create(new RefundCreateOptions { ChargeId = voidPaymentRequest.Order.AuthorizationTransactionId },
                    new RequestOptions() { ApiKey = _ecorenewStripeSettings.SecretApiKey });

                if (refund.Status != "succeeded")
                {
                    // TODO apply more appropriate error message
                    result.AddError("Payment declined");
                }

                result.NewPaymentStatus = PaymentStatus.Voided;
            }
            catch (Exception ex)
            {
                _logger.Error("Payment error", ex);

                result.AddError(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Core.Domain.Orders.Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            //it's not a redirection payment method. So we always return false
            return _ecorenewStripeSettings.CanRePostPayment;
        }

        /// <summary>
        /// Validate payment form
        /// </summary>
        /// <param name="form">The parsed form values</param>
        /// <returns>List of validating errors</returns>
        public IList<string> ValidatePaymentForm(IFormCollection form)
        {
            // Validation will go through the javascript provided by Stripe.js
            return new List<string>();
        }

        /// <summary>
        /// Get payment information
        /// </summary>
        /// <param name="form">The parsed form values</param>
        /// <returns>Payment info holder</returns>
        public ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            return new ProcessPaymentRequest
            {
                CustomValues = new Dictionary<string, object> {
                    { "paymenttoken", form["paymenttoken"].ToString() }
                }
            };
        }

        /// <summary>
        /// Gets a configuration page URL
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/EcorenewStripe/Configure";
        }

        /// <summary>
        /// Gets a view component for displaying plugin in public store ("payment info" checkout step)
        /// </summary>
        /// <param name="viewComponentName">View component name</param>
        public void GetPublicViewComponent(out string viewComponentName)
        {
            viewComponentName = "EcorenewStripe";
        }

        /// <summary>
        /// Install the plugin
        /// </summary>
        public override void Install()
        {
            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.SecretApiKey", "Secret API Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.SecretApiKey.Hint", "Secret API Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.PublicApiKey", "Public API Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.PublicApiKey.Hint", "Public API Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.AuthorizeOnly", "Authorize Only");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.AuthorizeOnly.Hint", "Check to authorize before capture");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.CanRePostPayment", "Can Re-Post Payment");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.ConfirmPayment", "Confirm Payment");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.Stripe.PaymentMethodDescription", "Use Credit Card (Stripe)");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.Stripe.Errors.InvalidCard", "Invalid card");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.Stripe.Errors.ExpiredCard", "Card expired");

            base.Install();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        public override void Uninstall()
        {
            _settingService.DeleteSetting<EcorenewStripeSettings>();

            //locales
            this.DeletePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.SecretApiKey");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.SecretApiKey.Hint");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.PublicApiKey");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.PublicApiKey.Hint");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.AuthorizeOnly");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.AuthorizeOnly.Hint");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.CanRePostPayment");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.Stripe.Fields.ConfirmPayment");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.Stripe.PaymentMethodDescription");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.Stripe.Errors.InvalidCard");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.Stripe.Errors.ExpiredCard");

            base.Uninstall();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get { return RecurringPaymentType.NotSupported; }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType
        {
            get { return PaymentMethodType.Standard; }
        }

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a payment method description that will be displayed on checkout pages in the public store
        /// </summary>
        public string PaymentMethodDescription
        {
            //return description of this payment method to be display on "payment method" checkout step. good practice is to make it localizable
            //for example, for a redirection payment method, description may be like this: "You will be redirected to PayPal site to complete the payment"
            get { return _localizationService.GetResource("Plugins.Ecorenew.Stripe.PaymentMethodDescription"); }
        }

        #endregion
    }
}