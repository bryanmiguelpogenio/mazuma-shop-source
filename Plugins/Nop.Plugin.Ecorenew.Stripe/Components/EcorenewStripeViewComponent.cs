﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Ecorenew.Stripe.Models;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Orders;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Ecorenew.Stripe.Components
{
    [ViewComponent(Name = "EcorenewStripe")]
    public class EcorenewStripeViewComponent : NopViewComponent
    {
        #region Fields

        private readonly ICurrencyService _currencyService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly CurrencySettings _currencySettings;
        private readonly EcorenewStripeSettings _ecorenewStripeSettings;

        private readonly string[] _zeroDecimalCurrencyCodes = { "bif", "clp", "djf", "gnf", "jpy", "kmf", "krw", "mga", "pyg", "rwf", "ugx", "vnd", "vuv", "xaf", "xof", "xpf" };

        #endregion

        #region Ctor

        public EcorenewStripeViewComponent(ICurrencyService currencyService,
            IOrderTotalCalculationService orderTotalCalculationService,
            IStoreContext storeContext,
            IWorkContext workContext,
            CurrencySettings currencySettings,
            EcorenewStripeSettings ecorenewStripeSettings)
        {
            this._currencyService = currencyService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._currencySettings = currencySettings;
            this._ecorenewStripeSettings = ecorenewStripeSettings;
        }

        #endregion

        #region Utilities

        protected virtual int ConvertToStripeAmount(decimal amount, string currencyCode)
        {
            decimal result = RoundingHelper.RoundPrice(amount);

            if (!_zeroDecimalCurrencyCodes.Contains(currencyCode.ToLower()))
                result *= 100;

            return Convert.ToInt32(result);
        }

        #endregion

        #region Methods

        public IViewComponentResult Invoke()
        {
            var model = new PaymentInfoModel();

            // years
            for (var i = 0; i < 15; i++)
            {
                var year = (DateTime.Now.Year + i).ToString();
                model.ExpireYears.Add(new SelectListItem { Text = year, Value = year, });
            }

            // months
            for (var i = 1; i <= 12; i++)
            {
                model.ExpireMonths.Add(new SelectListItem { Text = i.ToString("D2"), Value = i.ToString(), });
            }

            var currency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);

            model.CurrencyCode = currency.CurrencyCode.ToLower();

            var cart = _workContext.CurrentCustomer.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id).ToList();

            if (!cart.Any())
                throw new NopException("Cart is empty");

            //order total (and applied discounts, gift cards, reward points)
            var orderTotal = _orderTotalCalculationService.GetShoppingCartTotal(cart, out decimal orderDiscountAmount, out List<DiscountForCaching> orderAppliedDiscounts, out List<AppliedGiftCard> appliedGiftCards, out int redeemedRewardPoints, out decimal redeemedRewardPointsAmount);
            if (!orderTotal.HasValue)
                throw new NopException("Order total couldn't be calculated");

            model.Amount = ConvertToStripeAmount(orderTotal.Value, currency.CurrencyCode);

            return View("~/Plugins/Ecorenew.Stripe/Views/PaymentInfo.cshtml", model);
        }

        #endregion
    }
}