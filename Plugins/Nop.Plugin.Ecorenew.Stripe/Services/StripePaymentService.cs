﻿using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Services.Configuration;
using Nop.Services.Logging;
using Nop.Services.Payments;
using Nop.Services.Stores;

namespace Nop.Plugin.Ecorenew.Stripe.Services
{
    public class StripePaymentService : PaymentService
    {
        #region Fields

        private readonly ILogger _logger;
        private readonly IPluginFinder _pluginFinder;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly EcorenewStripeSettings _ecorenewStripeSettings;

        #endregion

        #region Ctor

        public StripePaymentService(PaymentSettings paymentSettings,
            ILogger logger,
            IPluginFinder pluginFinder,
            ISettingService settingService,
            IStoreContext storeContext,
            IWorkContext workContext,
            ShoppingCartSettings shoppingCartSettings,
            EcorenewStripeSettings ecorenewStripeSettings)
            : base(paymentSettings,
                  pluginFinder,
                  settingService,
                  shoppingCartSettings)
        {
            this._logger = logger;
            this._pluginFinder = pluginFinder;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._ecorenewStripeSettings = ecorenewStripeSettings;
        }

        #endregion

        #region Utilities

        public void InsertOverrideInformationLog(string customOrderNumber)
        {
            _logger.InsertLog(Core.Domain.Logging.LogLevel.Information,
                "Stripe: Post Process Payment",
                $"Can't override method for {customOrderNumber} returning to default Post Process Payment method.");
        }

        #endregion

        #region Methods

        public override void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            _logger.InsertLog(Core.Domain.Logging.LogLevel.Information,
                "Stripe: Post Process Payment",
                $"Override post process payment for {postProcessPaymentRequest.Order.CustomOrderNumber}.");

            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(EcorenewStripePaymentConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
            {
                InsertOverrideInformationLog(postProcessPaymentRequest.Order.CaptureTransactionId);
                base.PostProcessPayment(postProcessPaymentRequest);
            }

            if (postProcessPaymentRequest.Order.PaymentMethodSystemName != EcorenewStripePaymentConstants.PluginSystemName)
            {
                InsertOverrideInformationLog(postProcessPaymentRequest.Order.CaptureTransactionId);
                base.PostProcessPayment(postProcessPaymentRequest);
            }

            // check if the setting for payment can override post payment is enabled
            if (!_ecorenewStripeSettings.CanRePostPayment)
            {
                InsertOverrideInformationLog(postProcessPaymentRequest.Order.CaptureTransactionId);
                base.PostProcessPayment(postProcessPaymentRequest);
            }

            var paymentMethod = LoadPaymentMethodBySystemName(postProcessPaymentRequest.Order.PaymentMethodSystemName);
            if (paymentMethod == null)
                throw new NopException("Payment method couldn't be loaded");

            paymentMethod.PostProcessPayment(postProcessPaymentRequest);

        }

        #endregion
    }
}
