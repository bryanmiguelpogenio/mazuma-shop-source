﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Tax;
using Nop.Core.Html;
using Nop.Core.Plugins;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Stores;

namespace Nop.Plugin.Ecorenew.CustomPdfService.Services
{
    public class CustomPdfService : PdfService
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IPaymentService _paymentService;
        private readonly IPluginFinder _pluginFinder;
        private readonly IStoreContext _storeContext;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly AddressSettings _addressSettings;
        private readonly CustomPdfServiceSettings _customPdfServiceSettings;

        #endregion

        #region Ctor

        public CustomPdfService(ILocalizationService localizationService, 
            ILanguageService languageService, 
            IWorkContext workContext, 
            IOrderService orderService, 
            IPaymentService paymentService, 
            IDateTimeHelper dateTimeHelper, 
            IPriceFormatter priceFormatter, 
            ICurrencyService currencyService, 
            IMeasureService measureService, 
            IPictureService pictureService,
            IPluginFinder pluginFinder,
            IProductService productService, 
            IProductAttributeParser productAttributeParser, 
            IStoreService storeService, 
            IStoreContext storeContext, 
            ISettingService settingService, 
            IAddressAttributeFormatter addressAttributeFormatter, 
            CatalogSettings catalogSettings, 
            CurrencySettings currencySettings, 
            MeasureSettings measureSettings, 
            PdfSettings pdfSettings, 
            TaxSettings taxSettings, 
            AddressSettings addressSettings,
            CustomPdfServiceSettings customPdfServiceSettings) 
            : 
            base(localizationService,
                languageService,
                workContext,
                orderService, 
                paymentService, 
                dateTimeHelper,
                priceFormatter,
                currencyService, 
                measureService,
                pictureService,
                productService,
                productAttributeParser,
                storeService,
                storeContext,
                settingService,
                addressAttributeFormatter, 
                catalogSettings, 
                currencySettings, 
                measureSettings, 
                pdfSettings, 
                taxSettings, 
                addressSettings)
        {
            this._localizationService = localizationService;
            this._paymentService = paymentService;
            this._pluginFinder = pluginFinder;
            this._storeContext = storeContext;
            this._addressAttributeFormatter = addressAttributeFormatter;
            this._addressSettings = addressSettings;
            this._customPdfServiceSettings = customPdfServiceSettings;
        }

        #endregion

        #region Methods

        protected override void PrintBillingInfo(int vendorId, Language lang, Font titleFont, Order order, Font font, PdfPTable addressTable)
        {
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(CustomPdfServiceConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
                base.PrintBillingInfo(vendorId, lang, titleFont, order, font, addressTable);

            // check if the setting for custom export manager is enabled
            if (!_customPdfServiceSettings.CustomPdfServiceEnabled)
                base.PrintBillingInfo(vendorId, lang, titleFont, order, font, addressTable);

            const string indent = "   ";
            var billingAddress = new PdfPTable(1) { RunDirection = GetDirection(lang) };
            billingAddress.DefaultCell.Border = Rectangle.NO_BORDER;

            billingAddress.AddCell(GetParagraph("PDFInvoice.BillingInformation", lang, titleFont));

            if (_addressSettings.CompanyEnabled && !string.IsNullOrEmpty(order.BillingAddress.Company))
                billingAddress.AddCell(GetParagraph("PDFInvoice.Company", indent, lang, font, order.BillingAddress.Company));

            billingAddress.AddCell(GetParagraph("PDFInvoice.Name", indent, lang, font, order.BillingAddress.FirstName + " " + order.BillingAddress.LastName));
            if (_addressSettings.PhoneEnabled)
                billingAddress.AddCell(GetParagraph("PDFInvoice.Phone", indent, lang, font, order.BillingAddress.PhoneNumber));
            if (_addressSettings.FaxEnabled && !string.IsNullOrEmpty(order.BillingAddress.FaxNumber))
                billingAddress.AddCell(GetParagraph("PDFInvoice.Fax", indent, lang, font, order.BillingAddress.FaxNumber));
            if (_addressSettings.StreetAddressEnabled)
                billingAddress.AddCell(GetParagraph("PDFInvoice.Address", indent, lang, font, order.BillingAddress.Address1));
            if (_addressSettings.StreetAddress2Enabled && !string.IsNullOrEmpty(order.BillingAddress.Address2))
                billingAddress.AddCell(GetParagraph("PDFInvoice.Address2", indent, lang, font, order.BillingAddress.Address2));
            if (_addressSettings.CityEnabled || _addressSettings.StateProvinceEnabled || _addressSettings.ZipPostalCodeEnabled)
                billingAddress.AddCell(new Paragraph(
                    $"{indent}{order.BillingAddress.City}, {(order.BillingAddress.StateProvince != null ? order.BillingAddress.StateProvince.GetLocalized(x => x.Name, lang.Id) : "")} {order.BillingAddress.ZipPostalCode}",
                    font));
            if (_addressSettings.CountryEnabled && order.BillingAddress.Country != null)
                billingAddress.AddCell(new Paragraph(indent + order.BillingAddress.Country.GetLocalized(x => x.Name, lang.Id),
                    font));

            //VAT number
            if (!string.IsNullOrEmpty(order.VatNumber))
                billingAddress.AddCell(GetParagraph("PDFInvoice.VATNumber", indent, lang, font, order.VatNumber));

            //custom attributes
            var customBillingAddressAttributes =
                _addressAttributeFormatter.FormatAttributes(order.BillingAddress.CustomAttributes);
            if (!string.IsNullOrEmpty(customBillingAddressAttributes))
            {
                //TODO: we should add padding to each line (in case if we have several custom address attributes)
                billingAddress.AddCell(
                    new Paragraph(indent + HtmlHelper.ConvertHtmlToPlainText(customBillingAddressAttributes, true, true), font));
            }

            //vendors payment details
            if (vendorId == 0)
            {
                //payment method
                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
                var paymentMethodStr = paymentMethod != null
                    ? paymentMethod.GetLocalizedFriendlyName(_localizationService, lang.Id)
                    : order.PaymentMethodSystemName;
                if (!string.IsNullOrEmpty(paymentMethodStr))
                {
                    billingAddress.AddCell(new Paragraph(" "));
                    billingAddress.AddCell(GetParagraph("PDFInvoice.PaymentMethod", indent, lang, font, paymentMethodStr));
                    billingAddress.AddCell(new Paragraph());
                }

                if (_customPdfServiceSettings.CustomPdfServiceDisplayCustomValuesEnabled)
                {
                    //custom values
                    var customValues = order.DeserializeCustomValues();
                    if (customValues != null)
                    {
                        foreach (var item in customValues)
                        {
                            billingAddress.AddCell(new Paragraph(" "));
                            billingAddress.AddCell(new Paragraph(indent + item.Key + ": " + item.Value, font));
                            billingAddress.AddCell(new Paragraph());
                        }
                    }
                }
            }
            addressTable.AddCell(billingAddress);
        }

        #endregion
    }
}
