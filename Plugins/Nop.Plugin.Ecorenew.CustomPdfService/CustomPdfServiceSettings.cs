﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Ecorenew.CustomPdfService
{
    public class CustomPdfServiceSettings : ISettings
    {
        /// <summary>
        /// Gets or sets the value indicating whether the custom PDF service is enabled
        /// </summary>
        public bool CustomPdfServiceEnabled { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether to display custom values is enabled
        /// </summary>
        public bool CustomPdfServiceDisplayCustomValuesEnabled { get; set; }
    }
}
