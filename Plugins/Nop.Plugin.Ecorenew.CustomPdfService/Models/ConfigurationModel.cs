﻿using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Ecorenew.CustomPdfService.Models
{
    public class ConfigurationModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.CustomPdfService.Fields.CustomPdfServiceEnabled")]
        public bool CustomPdfServiceEnabled { get; set; }
        public bool CustomPdfServiceEnabled_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.CustomPdfService.Fields.CustomPdfServiceDisplayCustomValuesEnabled")]
        public bool CustomPdfServiceDisplayCustomValuesEnabled { get; set; }
        public bool CustomPdfServiceDisplayCustomValuesEnabled_OverrideForStore { get; set; }
    }
}
