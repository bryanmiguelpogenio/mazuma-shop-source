﻿using Microsoft.AspNetCore.Routing;
using Nop.Core;
using Nop.Core.Plugins;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Web.Framework;
using Nop.Web.Framework.Menu;
using System.Linq;

namespace Nop.Plugin.Ecorenew.CustomPdfService
{
    public class CustomPdfServicePlugin : BasePlugin, IMiscPlugin, IAdminMenuPlugin
    {
        #region Fields

        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        #endregion

        #region Ctor

        public CustomPdfServicePlugin(ISettingService settingService,
            IWebHelper webHelper)
        {
            this._settingService = settingService;
            this._webHelper = webHelper;
        }

        #endregion

        #region Methods

        public override void Install()
        {
            var settings = new CustomPdfServiceSettings
            {
                CustomPdfServiceEnabled = false,
                CustomPdfServiceDisplayCustomValuesEnabled = false
            };
            _settingService.SaveSetting(settings);

            // locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomPdfService.Title", "Custom PDF Service");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomPdfService.Fields.CustomPdfServiceEnabled", "Enable Event");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomPdfService.Fields.CustomPdfServiceDisplayCustomValuesEnabled", "Display Custom Values");

            base.Install();
        }

        public override void Uninstall()
        {
            // settings
            _settingService.DeleteSetting<CustomPdfServiceSettings>();

            // locales
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomPdfService.Title");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomPdfService.Fields.CustomPdfServiceEnabled");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomPdfService.Fields.CustomPdfServiceDisplayCustomValuesEnabled");

            base.Uninstall();
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/CustomPdfService/Configure";
        }

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var mainNode = new SiteMapNode()
            {
                SystemName = "Ecorenew",
                Title = "EcoRenew Group",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
                IconClass = "fa-leaf"
            };

            var customerReminderNode = new SiteMapNode()
            {
                SystemName = "Ecorenew.CustomPdfService",
                Title = "Custom PDF Service",
                Visible = true,
                IconClass = "fa-genderless",
                RouteValues = new RouteValueDictionary() { { "area", AreaNames.Admin } },
                ControllerName = "CustomPdfService",
                ActionName = "Configure"
            };

            mainNode.ChildNodes.Add(customerReminderNode);

            var ecorenewNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Ecorenew");
            if (ecorenewNode != null)
                ecorenewNode.ChildNodes.Add(customerReminderNode);
            else
                rootNode.ChildNodes.Add(mainNode);
        }

        #endregion
    }
}
