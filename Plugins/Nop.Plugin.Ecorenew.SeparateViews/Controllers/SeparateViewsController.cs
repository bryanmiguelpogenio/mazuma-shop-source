﻿using Microsoft.AspNetCore.Mvc;

namespace Nop.Plugin.Ecorenew.SeparateViews.Controllers
{
    public class SeparateViewsController : BaseController
    {
        public IActionResult Widgets(string widgetZone)
        {
            return View(model: widgetZone);
        }
    }
}