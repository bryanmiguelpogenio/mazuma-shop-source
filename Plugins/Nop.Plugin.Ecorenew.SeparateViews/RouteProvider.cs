﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework.Mvc.Routing;

namespace Nop.Plugin.Ecorenew.SeparateViews
{
    public class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Ecorenew.SeparateViews.Widgets", "separate-views/widgets/{widgetZone}",
                new { controller = "SeparateViews", action = "Widgets" });

            routeBuilder.MapRoute("Ecorenew.SeparateViews.Widgets.PriceFeed", "pricefeed",
                new { controller = "SeparateViews", action = "Widgets", widgetZone = "home_page_top" });
        }
        public int Priority
        {
            get
            {
                return -1;
            }
        }
    }
}