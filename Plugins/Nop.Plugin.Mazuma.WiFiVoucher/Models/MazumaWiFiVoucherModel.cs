﻿using Nop.Web.Framework.Mvc.Models;

namespace Nop.Plugin.Mazuma.WiFiVoucher.Models
{
    public class MazumaWiFiVoucherModel : BaseNopEntityModel
    {
        public string VoucherSerialNumber { get; set; }
        public string Pin { get; set; }
        public decimal Balance { get; set; }
        public string Status { get; set; }
        public int? OrderId { get; set; }
    }
}