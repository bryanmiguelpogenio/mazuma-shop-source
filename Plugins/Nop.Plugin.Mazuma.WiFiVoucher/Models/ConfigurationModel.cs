﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;

namespace Nop.Plugin.Mazuma.WiFiVoucher.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Mazuma.WiFiVoucher.Fields.MazumaWiFiVoucherEnabled")]
        public bool MazumaWiFiVoucherEnabled { get; set; }
        public bool MazumaWiFiVoucherEnabled_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Mazuma.WiFiVoucher.Fields.MessageTemplateSystemName")]
        public string MessageTemplateSystemName { get; set; }
        public bool MessageTemplateSystemName_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Mazuma.WiFiVoucher.Fields.VoucherSerialNumber")]
        public string VoucherSerialNumberFilter { get; set; }
    }
}