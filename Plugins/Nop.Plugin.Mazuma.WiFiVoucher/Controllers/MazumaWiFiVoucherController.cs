﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Mazuma.WiFiVoucher.Domain;
using Nop.Plugin.Mazuma.WiFiVoucher.Models;
using Nop.Plugin.Mazuma.WiFiVoucher.Services;
using Nop.Services.Configuration;
using Nop.Services.ExportImport;
using Nop.Services.ExportImport.Help;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Nop.Plugin.Mazuma.WiFiVoucher.Controllers
{
    [AuthorizeAdmin]
    [Area(AreaNames.Admin)]
    public class MazumaWiFiVoucherController : BasePluginController
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly ILogger _logger;
        private readonly IMazumaWiFiVoucherService _mazumaWiFiVoucherService;
        private readonly ISettingService _settingService;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public MazumaWiFiVoucherController(ILocalizationService localizationService,
            IPermissionService permissionService,
            ILogger logger,
            IMazumaWiFiVoucherService mazumaWiFiVoucherService,
            ISettingService settingService,
            IStoreService storeService,
            IWorkContext workContext)
        {
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._logger = logger;
            this._mazumaWiFiVoucherService = mazumaWiFiVoucherService;
            this._settingService = settingService;
            this._storeService = storeService;
            this._workContext = workContext;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void ImportFromXlsx(Stream stream)
        {
            using (var xlPackage = new ExcelPackage(stream))
            {
                // get the first worksheet in the workbook
                var worksheet = xlPackage.Workbook.Worksheets.FirstOrDefault();
                if (worksheet == null)
                    throw new NopException("No worksheet found");

                //the columns
                var properties = ImportManager.GetPropertiesByExcelCells<MazumaWiFiVoucher>(worksheet);
                var manager = new PropertyManager<MazumaWiFiVoucher>(properties);

                var iRow = 2;

                var batch = new MazumaWiFiVoucherUploadBatch
                {
                    UploadedByUserId = _workContext.CurrentCustomer.Id,
                    UploadedOnUtc = DateTime.UtcNow
                };

                List<MazumaWiFiVoucher> existingVouchers = new List<MazumaWiFiVoucher>();

                while (true)
                {
                    var allColumnsAreEmpty = manager.GetProperties
                        .Select(property => worksheet.Cells[iRow, property.PropertyOrderPosition])
                        .All(cell => cell == null || cell.Value == null || string.IsNullOrEmpty(cell.Value.ToString()));

                    if (allColumnsAreEmpty)
                        break;

                    manager.ReadFromXlsx(worksheet, iRow);

                    // check if voucher is existing
                    var voucher = _mazumaWiFiVoucherService.GetMazumaWiFiVoucherBySerialNumber(manager.GetProperty("VoucherSerialNumber").StringValue);
                    if (voucher == null)
                    {
                        voucher = new MazumaWiFiVoucher();
                        
                        foreach (var property in manager.GetProperties)
                        {
                            switch (property.PropertyName)
                            {
                                case "VoucherSerialNumber":
                                    voucher.VoucherSerialNumber = property.StringValue;
                                    break;
                                case "Pin":
                                    voucher.Pin = property.StringValue;
                                    break;
                                case "Balance":
                                    voucher.Balance = property.IntValue;
                                    break;
                                case "Status":
                                    voucher.Status = property.StringValue;
                                    break;
                            }
                        }

                        batch.Vouchers.Add(voucher);
                    }
                    else
                    {
                        existingVouchers.Add(voucher);
                    }

                    iRow++;
                }

                if (existingVouchers.Any())
                {
                    _logger.Warning($"There are {existingVouchers.Count} items already existing in the vouchers list.", new Exception(string.Join(Environment.NewLine, existingVouchers.Select(x => x.VoucherSerialNumber))));
                    throw new NopException($"There are {existingVouchers.Count} items already existing in the vouchers list. Please see Log for more details.");
                }
                else
                {
                    _mazumaWiFiVoucherService.UploadMazumaWiFiVoucherBatch(batch);
                }
            }
        }

        #endregion

        #region Methods

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMessageTemplates))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var settings = _settingService.LoadSetting<MazumaWiFiVoucherPluginSettings>(storeScope);

            var model = new ConfigurationModel
            {
                ActiveStoreScopeConfiguration = storeScope,
                MazumaWiFiVoucherEnabled = settings.MazumaWiFiVoucherEnabled,
                MessageTemplateSystemName = settings.MessageTemplateSystemName
            };

            if (storeScope > 0)
            {
                model.MazumaWiFiVoucherEnabled_OverrideForStore = _settingService.SettingExists(settings, x => x.MazumaWiFiVoucherEnabled, storeScope);
                model.MessageTemplateSystemName_OverrideForStore = _settingService.SettingExists(settings, x => x.MessageTemplateSystemName, storeScope);
            }

            return View("~/Plugins/Mazuma.WiFiVoucher/Views/Configure.cshtml", model);
        }

        [HttpPost]
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageMessageTemplates))
                return AccessDeniedView();

            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var settings = _settingService.LoadSetting<MazumaWiFiVoucherPluginSettings>(storeScope);

            //save settings
            settings.MessageTemplateSystemName = model.MessageTemplateSystemName;
            settings.MazumaWiFiVoucherEnabled = model.MazumaWiFiVoucherEnabled;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(settings, x => x.MazumaWiFiVoucherEnabled, model.MazumaWiFiVoucherEnabled_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(settings, x => x.MessageTemplateSystemName, model.MessageTemplateSystemName_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        [HttpPost]
        public virtual IActionResult GetMazumaWiFiVoucher(DataSourceRequest command, ConfigurationModel model)
        {
            var vouchers = _mazumaWiFiVoucherService.GetMazumaWiFiVouchers(model.VoucherSerialNumberFilter, command.Page - 1, command.PageSize);
            var source = vouchers.Select(x => new MazumaWiFiVoucherModel
            {
                Balance = x.Balance,
                Id = x.Id,
                OrderId = x.OrderId,
                Pin = x.Pin,
                Status = x.Status,
                VoucherSerialNumber = x.VoucherSerialNumber
            });

            var gridModel = new DataSourceResult
            {
                Data = source,
                Total = vouchers.TotalCount
            };

            return Json(gridModel);
        }

        [HttpPost]
        public virtual IActionResult UpdateMazumaWiFiVoucher(MazumaWiFiVoucherModel model)
        {
            if (model.VoucherSerialNumber != null)
                model.VoucherSerialNumber = model.VoucherSerialNumber.Trim();
            if (model.Pin != null)
                model.Pin = model.Pin.Trim();
            if (model.Status != null)
                model.Status = model.Status.Trim();

            if (!ModelState.IsValid)
            {
                return Json(new DataSourceResult { Errors = ModelState.SerializeErrors() });
            }

            var voucher = _mazumaWiFiVoucherService.GetMazumaWiFiVoucherById(model.Id);
            if (voucher == null)
                return Content("No voucher could be loaded with the specified ID");

            voucher.VoucherSerialNumber = model.VoucherSerialNumber;
            voucher.Pin = model.Pin;
            voucher.Balance = model.Balance;
            voucher.Status = model.Status;

            _mazumaWiFiVoucherService.UpdateMazumaWiFiVoucher(voucher);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult AddMazumaWiFiVoucher(MazumaWiFiVoucherModel model)
        {
            if (model.VoucherSerialNumber != null)
                model.VoucherSerialNumber = model.VoucherSerialNumber.Trim();
            if (model.Pin != null)
                model.Pin = model.Pin.Trim();
            if (model.Status != null)
                model.Status = model.Status.Trim();

            if (!ModelState.IsValid)
            {
                return Json(new DataSourceResult { Errors = ModelState.SerializeErrors() });
            }

            var voucher = new MazumaWiFiVoucher
            {
                Balance = model.Balance,
                Pin = model.Pin,
                Status = model.Status,
                VoucherSerialNumber = model.VoucherSerialNumber,
                MazumaWiFiVoucherUploadBatch = new MazumaWiFiVoucherUploadBatch
                {
                    UploadedByUserId = _workContext.CurrentCustomer.Id,
                    UploadedOnUtc = DateTime.UtcNow
                }
            };

            _mazumaWiFiVoucherService.CreateMazumaWiFiVoucher(voucher);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult DeleteMazumaWiFiVoucher(int id)
        {
            var voucher = _mazumaWiFiVoucherService.GetMazumaWiFiVoucherById(id);
            if (voucher == null)
                throw new ArgumentException("No voucher found with the specified id");

            _mazumaWiFiVoucherService.DeleteMazumaWiFiVoucher(voucher);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult ImportExcel(IFormFile importexcelfile)
        {
            try
            {
                if (importexcelfile != null && importexcelfile.Length > 0)
                {
                    ImportFromXlsx(importexcelfile.OpenReadStream());
                }
                else
                {
                    ErrorNotification(_localizationService.GetResource("Admin.Common.UploadFile"));
                    return RedirectToAction("List");
                }

                SuccessNotification("Success");
                return RedirectToAction("Configure");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Configure");
            }
        }

        #endregion
    }
}