﻿using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Mazuma.WiFiVoucher.Data;
using Nop.Plugin.Mazuma.WiFiVoucher.Domain;
using Nop.Plugin.Mazuma.WiFiVoucher.Services;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Mazuma.WiFiVoucher.Infrastructure
{
    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        private const string DBCONTEXT_NAME = "nop_object_context_mazuma_wifivoucher";

        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            //data context
            this.RegisterPluginDataContext<MazumaWiFiVoucherPluginObjectContext>(builder, DBCONTEXT_NAME);

            //override required repository with our custom context
            builder.RegisterType<EfRepository<MazumaWiFiVoucherUploadBatch>>()
                .As<IRepository<MazumaWiFiVoucherUploadBatch>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(DBCONTEXT_NAME))
                .InstancePerLifetimeScope();
            builder.RegisterType<EfRepository<MazumaWiFiVoucher>>()
                .As<IRepository<MazumaWiFiVoucher>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(DBCONTEXT_NAME))
                .InstancePerLifetimeScope();
            
            // services
            builder.RegisterType<MazumaWiFiVoucherService>()
                .As<IMazumaWiFiVoucherService>()
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}