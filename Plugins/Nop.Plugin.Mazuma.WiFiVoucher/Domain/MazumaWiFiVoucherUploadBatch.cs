﻿using Nop.Core;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Mazuma.WiFiVoucher.Domain
{
    /// <summary>
    /// Represents the batch of vouchers uploaded
    /// </summary>
    public class MazumaWiFiVoucherUploadBatch : BaseEntity
    {
        private ICollection<MazumaWiFiVoucher> _vouchers;

        /// <summary>
        /// Gets or sets the identifier of the user who uploaded the batch
        /// </summary>
        public int UploadedByUserId { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the batch was uploaded
        /// </summary>
        public DateTime UploadedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the vouchers associated with the batch
        /// </summary>
        public ICollection<MazumaWiFiVoucher> Vouchers
        {
            get { return _vouchers ?? (_vouchers = new List<MazumaWiFiVoucher>()); }
            protected set { _vouchers = value; }
        }
    }
}