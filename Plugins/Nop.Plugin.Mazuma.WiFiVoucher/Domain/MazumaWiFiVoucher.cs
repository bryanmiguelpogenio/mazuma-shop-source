﻿using Nop.Core;
using System;

namespace Nop.Plugin.Mazuma.WiFiVoucher.Domain
{
    /// <summary>
    /// Represents the Mazuma WiFi voucher
    /// </summary>
    public class MazumaWiFiVoucher : BaseEntity
    {
        /// <summary>
        /// Gets or sets the voucher serial number
        /// </summary>
        public string VoucherSerialNumber { get; set; }

        /// <summary>
        /// Gets or sets the pin
        /// </summary>
        public string Pin { get; set; }

        /// <summary>
        /// Gets or sets the balance
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// Gets or sets the status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the order assigned to the voucher code
        /// </summary>
        public int? OrderId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the batch where the voucher info is included
        /// </summary>
        public int MazumaWiFiVoucherUploadBatchId { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the entity has been synchronized with the logistics system
        /// </summary>
        public DateTime? SynchronizedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the batch where the voucher info in included
        /// </summary>
        public MazumaWiFiVoucherUploadBatch MazumaWiFiVoucherUploadBatch { get; set; }
    }
}