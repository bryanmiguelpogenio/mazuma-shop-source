﻿using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Core.Plugins;
using Nop.Plugin.Mazuma.WiFiVoucher.Data;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;

namespace Nop.Plugin.Mazuma.WiFiVoucher
{
    public class MazumaWiFiVoucherPlugin : BasePlugin, IMiscPlugin
    {
        #region Fields

        private readonly MazumaWiFiVoucherPluginObjectContext _objectContext;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;

        #endregion

        #region Ctor

        public MazumaWiFiVoucherPlugin(MazumaWiFiVoucherPluginObjectContext objectContext,
            ISettingService settingService,
            IWebHelper webHelper)
        {
            this._objectContext = objectContext;
            this._settingService = settingService;
            this._webHelper = webHelper;
        }

        #endregion

        #region Methods

        public override void Install()
        {
            //settings
            var settings = new MazumaWiFiVoucherPluginSettings
            {
                MazumaWiFiVoucherEnabled = false,
                MessageTemplateSystemName = "Mazuma.WiFiVoucherMessage"
            };
            _settingService.SaveSetting(settings);

            _objectContext.Install();

            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.MazumaWiFiVoucherEnabled", "Enabled");
            this.AddOrUpdatePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.MessageTemplateSystemName", "Message template system name");
            this.AddOrUpdatePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Configure.General", "General Settings");
            this.AddOrUpdatePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Configure.Vouchers", "Vouchers");
            this.AddOrUpdatePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.VoucherSerialNumber", "Voucher Serial Number");
            this.AddOrUpdatePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.Pin", "Pin");
            this.AddOrUpdatePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.Balance", "Balance");
            this.AddOrUpdatePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.Status", "Status");
            this.AddOrUpdatePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.OrderId", "Order ID");

            base.Install();
        }

        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<MazumaWiFiVoucherPluginSettings>();

            _objectContext.Uninstall();

            //locales
            this.DeletePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.MazumaWiFiVoucherEnabled");
            this.DeletePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.MessageTemplateSystemName");
            this.DeletePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Configure.General");
            this.DeletePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Configure.Vouchers");
            this.DeletePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.VoucherSerialNumber");
            this.DeletePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.Pin");
            this.DeletePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.Balance");
            this.DeletePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.Status");
            this.DeletePluginLocaleResource("Plugins.Mazuma.WiFiVoucher.Fields.OrderId");

            base.Uninstall();
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/MazumaWiFiVoucher/Configure";
        }

        public static bool IsInstalledAndEnabled(int storeId = 0)
        {
            var pluginFinder = EngineContext.Current.Resolve<IPluginFinder>();
            var storeContext = EngineContext.Current.Resolve<IStoreContext>();

            storeId = storeId > 0 ? storeId : storeContext.CurrentStore.Id;

            var plugin = pluginFinder.GetPluginDescriptorBySystemName("Mazuma.WiFiVoucher");
            return (plugin != null && plugin.Installed && pluginFinder.AuthenticateStore(plugin, storeId));
        }

        #endregion
    }
}