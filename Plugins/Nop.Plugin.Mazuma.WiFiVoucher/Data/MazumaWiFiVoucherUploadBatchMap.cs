﻿using Nop.Data.Mapping;
using Nop.Plugin.Mazuma.WiFiVoucher.Domain;

namespace Nop.Plugin.Mazuma.WiFiVoucher.Data
{
    public class MazumaWiFiVoucherUploadBatchMap : NopEntityTypeConfiguration<MazumaWiFiVoucherUploadBatch>
    {
        public MazumaWiFiVoucherUploadBatchMap()
        {
            this.ToTable("MazumaWiFiVoucherUploadBatch");
            this.HasKey(x => x.Id);
        }
    }
}