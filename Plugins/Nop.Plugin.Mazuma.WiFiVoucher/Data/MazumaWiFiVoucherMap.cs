﻿using Nop.Data.Mapping;
using Nop.Plugin.Mazuma.WiFiVoucher.Domain;

namespace Nop.Plugin.Mazuma.WiFiVoucher.Data
{
    public class MazumaWiFiVoucherMap : NopEntityTypeConfiguration<MazumaWiFiVoucher>
    {
        public MazumaWiFiVoucherMap()
        {
            this.ToTable("MazumaWiFiVoucher");
            this.HasKey(x => x.Id);

            this.Property(x => x.VoucherSerialNumber).IsRequired().HasMaxLength(20);
            this.Property(x => x.Pin).IsRequired().HasMaxLength(20);
            this.Property(x => x.Balance).IsRequired().HasPrecision(18, 4);
            this.Property(x => x.Status).IsRequired().HasMaxLength(20);
            this.Property(x => x.OrderId).IsOptional();
            this.Property(x => x.SynchronizedOnUtc).IsOptional();

            this.HasRequired(x => x.MazumaWiFiVoucherUploadBatch)
                .WithMany(ub => ub.Vouchers)
                .HasForeignKey(x => x.MazumaWiFiVoucherUploadBatchId);
        }
    }
}