﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Core.Plugins;
using Nop.Plugin.Mazuma.WiFiVoucher.Domain;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Mazuma.WiFiVoucher.Services
{
    public class OrderPaidEventConsumer : IConsumer<OrderPaidEvent>
    {
        #region Fields

        private readonly IRepository<MazumaWiFiVoucher> _mazumaWiFiVoucherRepository;
        private readonly IEmailAccountService _emailAccountService;
        private readonly ILanguageService _languageService;
        private readonly ILogger _logger;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly IStoreService _storeService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IPluginFinder _pluginFinder;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly MazumaWiFiVoucherPluginSettings _mazumaWiFiVoucherPluginSettings;

        #endregion

        #region Ctor

        public OrderPaidEventConsumer(IRepository<MazumaWiFiVoucher> mazumaWiFiVoucherRepository,
            IEmailAccountService emailAccountService,
            ILanguageService languageService,
            ILogger logger,
            IMessageTemplateService messageTemplateService,
            IMessageTokenProvider messageTokenProvider,
            IStoreService storeService,
            IStoreContext storeContext,
            IWorkContext workContext,
            IWorkflowMessageService workflowMessageService,
            IPluginFinder pluginFinder,
            EmailAccountSettings emailAccountSettings,
            MazumaWiFiVoucherPluginSettings mazumaWiFiVoucherPluginSettings)
        {
            this._mazumaWiFiVoucherRepository = mazumaWiFiVoucherRepository;
            this._emailAccountService = emailAccountService;
            this._languageService = languageService;
            this._logger = logger;
            this._messageTemplateService = messageTemplateService;
            this._messageTokenProvider = messageTokenProvider;
            this._storeService = storeService;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._workflowMessageService = workflowMessageService;
            this._pluginFinder = pluginFinder;
            this._emailAccountSettings = emailAccountSettings;
            this._mazumaWiFiVoucherPluginSettings = mazumaWiFiVoucherPluginSettings;
        }

        #endregion

        #region Utilities

        protected virtual int SendEmailNotification(Order order, MazumaWiFiVoucher voucher)
        {
            var customer = order.Customer;
            var store = _storeService.GetStoreById(order.StoreId);
            var languageId = order.CustomerLanguageId;

            languageId = EnsureLanguageIsActive(languageId, store.Id);

            var messageTemplate = GetActiveMessageTemplate(_mazumaWiFiVoucherPluginSettings.MessageTemplateSystemName, store.Id);
            if (messageTemplate == null)
            {
                _logger.Error($"Mazuma WiFi voucher message template not found. Message template system name: {_mazumaWiFiVoucherPluginSettings.MessageTemplateSystemName}. Order ID: {order.Id}", customer: customer);
                return 0;
            }

            //email account
            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

            //tokens
            var tokens = new List<Token>
            {
                new Token("Mazuma.WiFiVoucher.SerialNumber", voucher.VoucherSerialNumber),
                new Token("Mazuma.WiFiVoucher.Pin", voucher.Pin)
            };
            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            _messageTokenProvider.AddCustomerTokens(tokens, customer);

            var toEmail = order.BillingAddress.Email;
            var toName = $"{order.BillingAddress.FirstName} {order.BillingAddress.LastName}";

            return _workflowMessageService.SendNotification(messageTemplate, emailAccount, languageId, tokens, toEmail, toName);
        }

        /// <summary>
        /// Ensure language is active
        /// </summary>
        /// <param name="languageId">Language identifier</param>
        /// <param name="storeId">Store identifier</param>
        /// <returns>Return a value language identifier</returns>
        protected virtual int EnsureLanguageIsActive(int languageId, int storeId)
        {
            //load language by specified ID
            var language = _languageService.GetLanguageById(languageId);

            if (language == null || !language.Published)
            {
                //load any language from the specified store
                language = _languageService.GetAllLanguages(storeId: storeId).FirstOrDefault();
            }
            if (language == null || !language.Published)
            {
                //load any language
                language = _languageService.GetAllLanguages().FirstOrDefault();
            }

            if (language == null)
                throw new Exception("No active language could be loaded");
            return language.Id;
        }

        /// <summary>
        /// Get message template
        /// </summary>
        /// <param name="messageTemplateName">Message template name</param>
        /// <param name="storeId">Store identifier</param>
        /// <returns>Message template</returns>
        protected virtual MessageTemplate GetActiveMessageTemplate(string messageTemplateName, int storeId)
        {
            var messageTemplate = _messageTemplateService.GetMessageTemplateByName(messageTemplateName, storeId);

            //no template found
            if (messageTemplate == null)
                return null;

            //ensure it's active
            var isActive = messageTemplate.IsActive;
            if (!isActive)
                return null;

            return messageTemplate;
        }

        /// <summary>
        /// Get EmailAccount to use with a message templates
        /// </summary>
        /// <param name="messageTemplate">Message template</param>
        /// <param name="languageId">Language identifier</param>
        /// <returns>EmailAccount</returns>
        protected virtual EmailAccount GetEmailAccountOfMessageTemplate(MessageTemplate messageTemplate, int languageId)
        {
            var emailAccountId = messageTemplate.GetLocalized(mt => mt.EmailAccountId, languageId);
            //some 0 validation (for localizable "Email account" dropdownlist which saves 0 if "Standard" value is chosen)
            if (emailAccountId == 0)
                emailAccountId = messageTemplate.EmailAccountId;

            var emailAccount = _emailAccountService.GetEmailAccountById(emailAccountId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            return emailAccount;
        }

        #endregion

        #region Methods

        public void HandleEvent(OrderPaidEvent eventMessage)
        {
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(MazumaWiFiVoucherConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
                return;

            // check if the setting for custom export manager is enabled
            if (!_mazumaWiFiVoucherPluginSettings.MazumaWiFiVoucherEnabled)
                return;

            if (eventMessage?.Order == null)
                return;

            //check whether the plugin is installed and is active
            if (!MazumaWiFiVoucherPlugin.IsInstalledAndEnabled())
                return;

            // check if there are voucher codes already assigned to the same order ID
            if (_mazumaWiFiVoucherRepository.TableNoTracking.FirstOrDefault(x => x.OrderId == eventMessage.Order.Id) != null)
                return;

            // check if there are any available voucher codes
            var voucher = _mazumaWiFiVoucherRepository.Table.FirstOrDefault(x => !x.OrderId.HasValue);
            if (voucher != null)
            {
                // set the order id to the voucher
                voucher.OrderId = eventMessage.Order.Id;
                _mazumaWiFiVoucherRepository.Update(voucher);

                // send email notification
                SendEmailNotification(eventMessage.Order, voucher);
            }
        }

        #endregion
    }
}