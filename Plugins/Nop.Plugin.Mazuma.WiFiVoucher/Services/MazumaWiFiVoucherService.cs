﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Plugin.Mazuma.WiFiVoucher.Domain;
using System;
using System.Linq;

namespace Nop.Plugin.Mazuma.WiFiVoucher.Services
{
    public class MazumaWiFiVoucherService : IMazumaWiFiVoucherService
    {
        #region Fields

        private readonly IRepository<MazumaWiFiVoucher> _mazumaWiFiVoucherRepository;
        private readonly IRepository<MazumaWiFiVoucherUploadBatch> _mazumaWiFiUploadBatchRepository;

        #endregion

        #region Ctor

        public MazumaWiFiVoucherService(IRepository<MazumaWiFiVoucher> mazumaWiFiVoucherRepository,
            IRepository<MazumaWiFiVoucherUploadBatch> mazumaWiFiUploadBatchRepository)
        {
            this._mazumaWiFiVoucherRepository = mazumaWiFiVoucherRepository;
            this._mazumaWiFiUploadBatchRepository = mazumaWiFiUploadBatchRepository;
        }

        #endregion

        #region Methods

        public virtual IPagedList<MazumaWiFiVoucher> GetMazumaWiFiVouchers(string serialNumber = null, int pageIndex = 0, int pageSize = Int32.MaxValue)
        {
            var vouchers = _mazumaWiFiVoucherRepository.Table;

            if (!string.IsNullOrWhiteSpace(serialNumber))
                vouchers = vouchers.Where(x => x.VoucherSerialNumber.ToLower().Contains(serialNumber.ToLower()));

            vouchers = vouchers.OrderByDescending(x => x.VoucherSerialNumber);

            return new PagedList<MazumaWiFiVoucher>(vouchers, pageIndex, pageSize);
        }

        public virtual MazumaWiFiVoucher GetMazumaWiFiVoucherById(int id)
        {
            return _mazumaWiFiVoucherRepository.GetById(id);
        }

        public MazumaWiFiVoucher GetMazumaWiFiVoucherBySerialNumber(string serialNumber)
        {
            return _mazumaWiFiVoucherRepository.Table.FirstOrDefault(x => x.VoucherSerialNumber == serialNumber);
        }

        public void CreateMazumaWiFiVoucher(MazumaWiFiVoucher mazumaWiFiVoucher)
        {
            if (mazumaWiFiVoucher == null)
                throw new ArgumentNullException(nameof(mazumaWiFiVoucher));

            if (_mazumaWiFiVoucherRepository.TableNoTracking.Any(x => x.VoucherSerialNumber == mazumaWiFiVoucher.VoucherSerialNumber))
                throw new NopException($"Mazuma WiFi voucher with serial number {mazumaWiFiVoucher.VoucherSerialNumber} already exists");

            _mazumaWiFiVoucherRepository.Insert(mazumaWiFiVoucher);
        }

        public void UpdateMazumaWiFiVoucher(MazumaWiFiVoucher mazumaWiFiVoucher)
        {
            if (mazumaWiFiVoucher == null)
                throw new ArgumentNullException(nameof(mazumaWiFiVoucher));

            _mazumaWiFiVoucherRepository.Update(mazumaWiFiVoucher);
        }

        public void DeleteMazumaWiFiVoucher(MazumaWiFiVoucher mazumaWiFiVoucher)
        {
            if (mazumaWiFiVoucher == null)
                throw new ArgumentNullException(nameof(mazumaWiFiVoucher));

            _mazumaWiFiVoucherRepository.Delete(mazumaWiFiVoucher);
        }

        public void UploadMazumaWiFiVoucherBatch(MazumaWiFiVoucherUploadBatch mazumaWiFiVoucherUploadBatch)
        {
            if (mazumaWiFiVoucherUploadBatch == null)
                throw new ArgumentNullException(nameof(mazumaWiFiVoucherUploadBatch));

            if (!mazumaWiFiVoucherUploadBatch.Vouchers.Any())
                throw new NopException("Voucher batch is empty.");

            _mazumaWiFiUploadBatchRepository.Insert(mazumaWiFiVoucherUploadBatch);
        }

        #endregion
    }
}