﻿using Nop.Core;
using Nop.Plugin.Mazuma.WiFiVoucher.Domain;
using System;

namespace Nop.Plugin.Mazuma.WiFiVoucher.Services
{
    public interface IMazumaWiFiVoucherService
    {
        IPagedList<MazumaWiFiVoucher> GetMazumaWiFiVouchers(string serialNumber = null, int pageIndex = 0, int pageSize = Int32.MaxValue);
        MazumaWiFiVoucher GetMazumaWiFiVoucherById(int id);
        MazumaWiFiVoucher GetMazumaWiFiVoucherBySerialNumber(string serialNumber);
        void CreateMazumaWiFiVoucher(MazumaWiFiVoucher mazumaWiFiVoucher);
        void UpdateMazumaWiFiVoucher(MazumaWiFiVoucher mazumaWiFiVoucher);
        void DeleteMazumaWiFiVoucher(MazumaWiFiVoucher mazumaWiFiVoucher);
        void UploadMazumaWiFiVoucherBatch(MazumaWiFiVoucherUploadBatch mazumaWiFiVoucherUploadBatch);
    }
}