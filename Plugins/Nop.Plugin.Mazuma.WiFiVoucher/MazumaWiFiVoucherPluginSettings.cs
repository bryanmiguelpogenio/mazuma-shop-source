﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Mazuma.WiFiVoucher
{
    public class MazumaWiFiVoucherPluginSettings : ISettings
    {
        public bool MazumaWiFiVoucherEnabled { get; set; }
        public string MessageTemplateSystemName { get; set; }
    }
}