﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework.Mvc.Routing;

namespace Nop.Plugin.Payments.BrainTreePayPal
{
    public class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            //Submit PayPal Express Checkout button
            routeBuilder.MapRoute("Plugin.Payments.BrainTreePayPal.ProcessPayload",
                 "PaymentBrainTreePayPal/ProcessPayload",
                 new { controller = "PaymentBrainTreePayPal", action = "ProcessPayload" });

            // set shipping method
            routeBuilder.MapRoute("Plugin.Payments.BrainTreePayPal.SetShippingMethod",
                 "PaymentBrainTreePayPal/SetShippingMethod",
                 new { controller = "PaymentBrainTreePayPal", action = "SetShippingMethod" });

            // Confirm order
            routeBuilder.MapRoute("Plugin.Payments.BrainTreePayPal.Confirm",
                 "PaymentBrainTreePayPal/Confirm",
                 new { controller = "PaymentBrainTreePayPal", action = "Confirm" });
        }

        public int Priority
        {
            get { return 0; }
        }
    }
}