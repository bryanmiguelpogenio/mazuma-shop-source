﻿using Braintree;
using Nop.Core.Infrastructure.Mapper;
using Nop.Plugin.Payments.BrainTreePayPal.Domain;

namespace Nop.Plugin.Payments.BrainTreePayPal.Extensions
{
    public static class MappingExtensions
    {
        public static TDestination MapTo<TSource, TDestination>(this TSource source)
        {
            return AutoMapperConfiguration.Mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination)
        {
            return AutoMapperConfiguration.Mapper.Map(source, destination);
        }
        
        public static PayPalDetails ToModel(this BrainTreePayPalPaymentPayPalDetails entity)
        {
            return entity.MapTo<BrainTreePayPalPaymentPayPalDetails, PayPalDetails>();
        }

        public static BrainTreePayPalPaymentPayPalDetails ToEntity(this PayPalDetails model)
        {
            return model.MapTo<PayPalDetails, BrainTreePayPalPaymentPayPalDetails>();
        }

        public static BrainTreePayPalPaymentPayPalDetails ToEntity(this PayPalDetails model, BrainTreePayPalPaymentPayPalDetails destination)
        {
            return model.MapTo(destination);
        }
    }
}