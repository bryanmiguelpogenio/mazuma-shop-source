﻿using Microsoft.AspNetCore.Mvc.Controllers;
using Nop.Core.Domain.Payments;
using Nop.Services.Events;
using Nop.Services.Payments;
using Nop.Web.Framework.Events;
using Nop.Web.Framework.UI;

namespace Nop.Plugin.Payments.BrainTreePayPal.Services
{
    public class EventConsumer : IConsumer<PageRenderingEvent>
    {
        #region Fields

        private readonly IPaymentService _paymentService;
        private readonly BrainTreePayPalPaymentSettings _brainTreePayPalPaymentSettings;
        private readonly PaymentSettings _paymentSettings;

        #endregion

        #region Ctor

        public EventConsumer(IPaymentService paymentService,
            BrainTreePayPalPaymentSettings brainTreePayPalPaymentSettings,
            PaymentSettings paymentSettings)
        {
            this._paymentService = paymentService;
            this._brainTreePayPalPaymentSettings = brainTreePayPalPaymentSettings;
            this._paymentSettings = paymentSettings;
        }

        #endregion

        #region Methods

        public void HandleEvent(PageRenderingEvent eventMessage)
        {
            //if (eventMessage?.Helper?.ViewContext?.ActionDescriptor == null)
            //    return;

            ////check whether the plugin is installed and is active
            //var brainTreePayPalPaymentMethod = _paymentService.LoadPaymentMethodBySystemName("Payments.BrainTreePayPal");
            //if (!(brainTreePayPalPaymentMethod?.PluginDescriptor?.Installed ?? false) || !brainTreePayPalPaymentMethod.IsPaymentMethodActive(_paymentSettings))
            //    return;

            ////add js sсript to one page checkout
            //if (eventMessage.Helper.ViewContext.ActionDescriptor is ControllerActionDescriptor actionDescriptor &&
            //    actionDescriptor.ControllerName == "Checkout" && actionDescriptor.ActionName == "OnePageCheckout")
            //{
            //    eventMessage.Helper.AddScriptParts(ResourceLocation.Footer, "https://www.paypalobjects.com/api/checkout.min.js", excludeFromBundle: true);
            //    eventMessage.Helper.AddScriptParts(ResourceLocation.Footer, "https://js.braintreegateway.com/web/3.37.0/js/client.min.js", excludeFromBundle: true);
            //    eventMessage.Helper.AddScriptParts(ResourceLocation.Footer, "https://js.braintreegateway.com/web/3.37.0/js/paypal-checkout.min.js", excludeFromBundle: true);

            //    if (_brainTreePayPalPaymentSettings.EnablePayPalKount)
            //        eventMessage.Helper.AddScriptParts(ResourceLocation.Footer, "https://js.braintreegateway.com/web/3.37.0/js/data-collector.min.js", excludeFromBundle: true);
            //}
        }

        #endregion
    }
}