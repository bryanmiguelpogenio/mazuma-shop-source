﻿using Nop.Plugin.Payments.BrainTreePayPal.Domain;
using Nop.Plugin.Payments.BrainTreePayPal.Models;

namespace Nop.Plugin.Payments.BrainTreePayPal.Services
{
    public interface IBrainTreePayPalService
    {
        string GetAccessToken(bool forceGetNew = false);
        string GetProductFromPriceStatement(int productId, bool forceGetNew = false);
        CalculatedFinancingOptionsResult GetFinancingCalculatorValues(decimal price, string currencyCode);
        OrderPayPalCreditFinancingDetails GetOrderPayPalCreditFinancingDetails(int orderId);
    }
}