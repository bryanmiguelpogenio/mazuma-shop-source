﻿using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Plugin.Payments.BrainTree;
using Nop.Plugin.Payments.BrainTreePayPal.Domain;
using Nop.Plugin.Payments.BrainTreePayPal.Models;
using Nop.Services.Catalog;
using System;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace Nop.Plugin.Payments.BrainTreePayPal.Services
{
    public class BrainTreePayPalService : IBrainTreePayPalService
    {
        #region Constants

        private const string PAYPAL_ACCESS_TOKEN_KEY = "paypal_access_token";
        private const string PAYPAL_CREDIT_FROM_STATEMENT_KEY_FORMAT = "paypal_credit_from_statement_{0}";
        private const string PAYPAL_FINANCING_CALCULATOR_VALUES_KEY_FORMAT = "paypal_financing_calculator_values_{0}_{1}";

        #endregion

        #region Fields

        private readonly IRepository<OrderPayPalCreditFinancingDetails> _orderPayPalCreditFinancingDetailsRepository;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IProductService _productService;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IWorkContext _workContext;
        private readonly BrainTreePaymentSettings _brainTreePaymentSettings;
        private readonly BrainTreePayPalPaymentSettings _brainTreePayPalPaymentSettings;

        #endregion

        #region Ctor

        public BrainTreePayPalService(IRepository<OrderPayPalCreditFinancingDetails> orderPayPalCreditFinancingDetailsRepository,
            IPriceCalculationService priceCalculationService,
            IPriceFormatter priceFormatter,
            IProductService productService,
            IStaticCacheManager cacheManager,
            IWorkContext workContext,
            BrainTreePaymentSettings brainTreePaymentSettings,
            BrainTreePayPalPaymentSettings brainTreePayPalPaymentSettings)
        {
            this._orderPayPalCreditFinancingDetailsRepository = orderPayPalCreditFinancingDetailsRepository;
            this._priceCalculationService = priceCalculationService;
            this._priceFormatter = priceFormatter;
            this._productService = productService;
            this._cacheManager = cacheManager;
            this._workContext = workContext;
            this._brainTreePaymentSettings = brainTreePaymentSettings;
            this._brainTreePayPalPaymentSettings = brainTreePayPalPaymentSettings;
        }

        #endregion

        #region Utilities

        protected virtual CalculatedFinancingOptionsResult GetCalculatedFinancingOptionsResult(decimal price, string currencyCode)
        {
            var accessToken = GetAccessToken();

            using (var client = new WebClient())
            {
                client.BaseAddress = _brainTreePaymentSettings.UseSandBox ? "https://api.sandbox.paypal.com" : "https://api.paypal.com";
                client.Headers[HttpRequestHeader.Authorization] = $"Bearer {accessToken}";
                client.Headers[HttpRequestHeader.ContentType] = "application/json";

                var options = new PayPalCreditFinancingOptionRequestParam
                {
                    FinancingCountryCode = "GB",
                    TransactionAmount = new PayPalCreditFinancingOptionRequestParamTransactionAmount
                    {
                        CurrencyCode = currencyCode,
                        Value = price
                    }
                };

                var optionsJson = JsonConvert.SerializeObject(options, Formatting.Indented);
                var optionsBytes = Encoding.Default.GetBytes(optionsJson);

                var responsebytes = client.UploadData("/v1/credit/calculated-financing-options", "POST", optionsBytes);
                string responsebody = Encoding.UTF8.GetString(responsebytes);

                return JsonConvert.DeserializeObject<CalculatedFinancingOptionsResult>(responsebody);
            }
        }

        #endregion

        #region Methods

        public virtual string GetAccessToken(bool forceGetNew = false)
        {
            Func<string> accessToken = () =>
            {
                using (var client = new WebClient())
                {
                    string credentials = Convert.ToBase64String(
                        Encoding.ASCII.GetBytes(
                            _brainTreePayPalPaymentSettings.PayPalRestApiClientId
                            + ":"
                            + _brainTreePayPalPaymentSettings.PayPalRestApiSecret));
                    client.Headers[HttpRequestHeader.Authorization] = string.Format(
                        "Basic {0}", credentials);

                    client.BaseAddress = _brainTreePaymentSettings.UseSandBox ? "https://api.sandbox.paypal.com" : "https://api.paypal.com";

                    var parameters = new NameValueCollection
                    {
                        { "grant_type", "client_credentials" }
                    };

                    var responsebytes = client.UploadValues("/v1/oauth2/token", "POST", parameters);
                    string responsebody = Encoding.UTF8.GetString(responsebytes);

                    return JsonConvert.DeserializeObject<PayPalAuthTokenResult>(responsebody).AccessToken;
                }
            };

            if (forceGetNew || !_brainTreePayPalPaymentSettings.CacheAccessToken)
                _cacheManager.Remove(PAYPAL_ACCESS_TOKEN_KEY);

            if (_brainTreePayPalPaymentSettings.CacheAccessToken)
                return _cacheManager.Get(PAYPAL_ACCESS_TOKEN_KEY, accessToken);
            else
                return accessToken();
        }

        public virtual string GetProductFromPriceStatement(int productId, bool forceGetNew = false)
        {
            var product = _productService.GetProductById(productId);
            if (product == null)
                throw new NopException($"Product does not exist. ID {productId}");

            var price = _priceCalculationService.GetFinalPrice(product, _workContext.CurrentCustomer);

            if (price < 99)
                return null;

            Func<string> fromPriceStatement = () =>
            {
                CalculatedFinancingOptionsResult result = GetCalculatedFinancingOptionsResult(price, _workContext.WorkingCurrency.CurrencyCode);

                try
                {
                    var minMonthly = result.FinancingOptions.Min(fo => fo.QualifyingFinancingOptions.Where(x => x.CreditFinancing.CreditType == "INST").Min(qfo => qfo.MonthlyPayment.Value));
                    return minMonthly > 0 ? _priceFormatter.FormatPrice(minMonthly) : "";
                }
                catch
                {
                    return null;
                }
            };

            string key = string.Format(PAYPAL_CREDIT_FROM_STATEMENT_KEY_FORMAT, productId);

            if (forceGetNew || !_brainTreePayPalPaymentSettings.CacheFromPriceStatement)
                _cacheManager.Remove(key);

            if (_brainTreePayPalPaymentSettings.CacheFromPriceStatement)
                return _cacheManager.Get(key, fromPriceStatement);
            else
                return fromPriceStatement();
        }

        public virtual CalculatedFinancingOptionsResult GetFinancingCalculatorValues(decimal price, string currencyCode)
        {
            string key = string.Format(PAYPAL_FINANCING_CALCULATOR_VALUES_KEY_FORMAT, currencyCode, price);

            if (!_brainTreePayPalPaymentSettings.CacheFinancingCalculatorValues)
                _cacheManager.Remove(key);

            if (_brainTreePayPalPaymentSettings.CacheFinancingCalculatorValues)
            {
                return _cacheManager.Get(key, () =>
                {
                    return GetCalculatedFinancingOptionsResult(price, currencyCode);
                });
            }
            else
            {
                return GetCalculatedFinancingOptionsResult(price, currencyCode);
            }
        }

        public virtual OrderPayPalCreditFinancingDetails GetOrderPayPalCreditFinancingDetails(int orderId)
        {
            return _orderPayPalCreditFinancingDetailsRepository.Table.FirstOrDefault(x => x.OrderId == orderId);
        }

        #endregion
    }
}