﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Http.Extensions;
using Nop.Plugin.Payments.BrainTree;
using Nop.Plugin.Payments.BrainTreePayPal.Domain;
using Nop.Plugin.Payments.BrainTreePayPal.Models;
using Nop.Plugin.Payments.BrainTreePayPal.Services;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Payments.BrainTreePayPal.Controllers
{
    public class PaymentBrainTreePayPalController : BasePaymentController
    {
        #region Fields

        private readonly IRepository<OrderPayPalCreditFinancingDetails> _orderPayPalCreditFinancingDetailsRepository;
        private readonly IBrainTreePayPalService _brainTreePayPalService;
        private readonly ICountryService _countryService;
        private readonly ICustomerService _customerService;
        private readonly ICurrencyService _currencyService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly ILogger _logger;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IWorkContext _workContext;
        private readonly IShippingService _shippingService;
        private readonly IStoreContext _storeContext;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IStoreService _storeService;
        private readonly ITaxService _taxService;
        private readonly IPaymentService _paymentService;
        private readonly IPermissionService _permissionService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IWebHelper _webHelper;
        private readonly BrainTreePayPalPaymentSettings _brainTreePayPalPaymentSettings;
        private readonly CommonSettings _commonSettings;
        private readonly OrderSettings _orderSettings;
        private readonly ShippingSettings _shippingSettings;

        #endregion

        #region Ctor

        public PaymentBrainTreePayPalController(IRepository<OrderPayPalCreditFinancingDetails> orderPayPalCreditFinancingDetailsRepository,
            IBrainTreePayPalService brainTreePayPalService,
            ICountryService countryService,
            ICustomerService customerService,
            ICurrencyService currencyService,
            IGenericAttributeService genericAttributeService,
            ISettingService settingService,
            ILocalizationService localizationService, 
            ILogger logger,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IOrderTotalCalculationService orderTotalCalculationService,
            IWorkContext workContext, 
            IShippingService shippingService,
            IStoreContext storeContext,
            IStateProvinceService stateProvinceService,
            IStoreService storeService,
            ITaxService taxService,
            IPaymentService paymentService,
            IPermissionService permissionService,
            IPriceFormatter priceFormatter,
            IWebHelper webHelper,
            BrainTreePayPalPaymentSettings brainTreePayPalPaymentSettings,
            CommonSettings commonSettings,
            OrderSettings orderSettings,
            ShippingSettings shippingSettings)
        {
            this._orderPayPalCreditFinancingDetailsRepository = orderPayPalCreditFinancingDetailsRepository;
            this._brainTreePayPalService = brainTreePayPalService;
            this._countryService = countryService;
            this._customerService = customerService;
            this._currencyService = currencyService;
            this._genericAttributeService = genericAttributeService;
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._logger = logger;
            this._orderProcessingService = orderProcessingService;
            this._orderService = orderService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._workContext = workContext;
            this._shippingService = shippingService;
            this._storeContext = storeContext;
            this._stateProvinceService = stateProvinceService;
            this._storeService = storeService;
            this._taxService = taxService;
            this._paymentService = paymentService;
            this._permissionService = permissionService;
            this._priceFormatter = priceFormatter;
            this._webHelper = webHelper;
            this._brainTreePayPalPaymentSettings = brainTreePayPalPaymentSettings;
            this._commonSettings = commonSettings;
            this._orderSettings = orderSettings;
            this._shippingSettings = shippingSettings;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual IList<ShoppingCartItem> GetCart()
        {
            return _workContext.CurrentCustomer.ShoppingCartItems
                .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                .ToList();
        }

        [NonAction]
        protected virtual bool IsAllowedToCheckout()
        {
            return !(_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed);
        }

        [NonAction]
        protected virtual CheckoutConfirmModel PrepareConfirmOrderModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutConfirmModel
            {
                TermsOfServiceOnOrderConfirmPage = _orderSettings.TermsOfServiceOnOrderConfirmPage,
                TermsOfServicePopup = _commonSettings.PopupForTermsOfServiceLinks
            };

            //min order amount validation
            bool minOrderTotalAmountOk = _orderProcessingService.ValidateMinOrderTotalAmount(cart);
            if (!minOrderTotalAmountOk)
            {
                decimal minOrderTotalAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_orderSettings.MinOrderTotalAmount, _workContext.WorkingCurrency);
                model.MinOrderTotalWarning = string.Format(_localizationService.GetResource("Checkout.MinOrderTotalAmount"), _priceFormatter.FormatPrice(minOrderTotalAmount, true, false));
            }
            return model;
        }

        [NonAction]
        protected virtual bool IsMinimumOrderPlacementIntervalValid(Customer customer)
        {
            //prevent 2 orders being placed within an X seconds time frame
            if (_orderSettings.MinimumOrderPlacementInterval == 0)
                return true;

            var lastOrder = _orderService.SearchOrders(_storeContext.CurrentStore.Id,
                customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                .FirstOrDefault();
            if (lastOrder == null)
                return true;

            var interval = DateTime.UtcNow - lastOrder.CreatedOnUtc;
            return interval.TotalSeconds > _orderSettings.MinimumOrderPlacementInterval;
        }

        [NonAction]
        protected virtual CheckoutPlaceOrderModel PlaceOrder(string insurenceShoppingCartItems = "")
        {
            var model = new CheckoutPlaceOrderModel();
            try
            {
                var nonce = HttpContext.Session.Get<string>(BrainTreePayPalPaymentSystemNames.PAYMENT_METHOD_NONCE);
                if (string.IsNullOrWhiteSpace(nonce))
                {
                    model.RedirectToCart = true;
                    return model;
                }

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                //place order
                var processPaymentRequest = new ProcessPaymentRequest();
                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = BrainTreePayPalPaymentSystemNames.PAYMENT_METHOD_SYSTEM_NAME;
                if (!string.IsNullOrEmpty(insurenceShoppingCartItems))
                {
                    processPaymentRequest.InsurenceShoppingCartIds = insurenceShoppingCartItems.Split(',').ToList();
                }
                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);

                if (placeOrderResult.Success)
                {
                    // we need to check if credit financing was appied
                    var selectedFinancingOption = HttpContext.Session.Get<PayPalCreditFinancingOffered>(BrainTreePayPalPaymentSystemNames.SELECTED_FINANCING_OPTION);
                    if (selectedFinancingOption != null)
                    {
                        var financingOption = new OrderPayPalCreditFinancingDetails
                        {
                            CartAmountImmutable = selectedFinancingOption.cartAmountImmutable,
                            MonthlyPaymentCurrency = selectedFinancingOption.monthlyPayment.currency,
                            MonthlyPaymentValue = selectedFinancingOption.monthlyPayment.value,
                            OrderId = placeOrderResult.PlacedOrder.Id,
                            PayerAcceptance = selectedFinancingOption.payerAcceptance,
                            Term = selectedFinancingOption.term,
                            TotalCostCurrency = selectedFinancingOption.totalCost.currency,
                            TotalCostValue = selectedFinancingOption.totalCost.value,
                            TotalInterestCurrency = selectedFinancingOption.totalInterest.currency,
                            TotalInterestValue = selectedFinancingOption.totalInterest.value
                        };

                        _orderPayPalCreditFinancingDetailsRepository.Insert(financingOption);

                        HttpContext.Session.Remove(BrainTreePayPalPaymentSystemNames.SELECTED_FINANCING_OPTION);
                    }

                    HttpContext.Session.Remove(BrainTreePayPalPaymentSystemNames.PAYMENT_METHOD_NONCE);
                    HttpContext.Session.Remove(BrainTreePayPalPaymentSystemNames.DEVICE_DATA);

                    var postProcessPaymentRequest = new PostProcessPaymentRequest
                    {
                        Order = placeOrderResult.PlacedOrder
                    };
                    _paymentService.PostProcessPayment(postProcessPaymentRequest);

                    if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                    {
                        //redirection or POST has been done in PostProcessPayment
                        model.IsRedirected = true;
                        return model;
                    }
                    model.CompletedId = placeOrderResult.PlacedOrder.Id;
                    return model;
                }
                foreach (var error in placeOrderResult.Errors)
                    model.Warnings.Add(error);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc);
                model.Warnings.Add(exc.Message);
            }
            return model;
        }

        [NonAction]
        protected virtual CheckoutShippingMethodModel PrepareShippingMethodModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutShippingMethodModel();

            var getShippingOptionResponse = _shippingService.GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress);
            if (getShippingOptionResponse.Success)
            {
                //performance optimization. cache returned shipping options.
                //we'll use them later (after a customer has selected an option).
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                                                       SystemCustomerAttributeNames.OfferedShippingOptions,
                                                       getShippingOptionResponse.ShippingOptions,
                                                       _storeContext.CurrentStore.Id);

                foreach (var shippingOption in getShippingOptionResponse.ShippingOptions)
                {
                    var soModel = new CheckoutShippingMethodModel.ShippingMethodModel()
                    {
                        Name = shippingOption.Name,
                        Description = shippingOption.Description,
                        ShippingRateComputationMethodSystemName = shippingOption.ShippingRateComputationMethodSystemName,
                        ShippingOption = shippingOption
                    };

                    //adjust rate
                    List<DiscountForCaching> appliedDiscounts = null;
                    var shippingTotal = _orderTotalCalculationService.AdjustShippingRate(
                        shippingOption.Rate, cart, out appliedDiscounts);

                    decimal rateBase = _taxService.GetShippingPrice(shippingTotal, _workContext.CurrentCustomer);
                    decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                    soModel.Fee = _priceFormatter.FormatShippingPrice(rate, true);

                    model.ShippingMethods.Add(soModel);
                }

                //find a selected (previously) shipping method
                var selectedShippingOption = _workContext.CurrentCustomer.GetAttribute<ShippingOption>(SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                if (selectedShippingOption != null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.ToList()
                                                      .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedShippingOption.Name, StringComparison.InvariantCultureIgnoreCase) &&
                                                                  !String.IsNullOrEmpty(so.ShippingRateComputationMethodSystemName) && so.ShippingRateComputationMethodSystemName.Equals(selectedShippingOption.ShippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase));
                    if (shippingOptionToSelect != null)
                        shippingOptionToSelect.Selected = true;
                }
                //if no option has been selected, let's do it for the first one
                if (model.ShippingMethods.FirstOrDefault(so => so.Selected) == null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.FirstOrDefault();
                    if (shippingOptionToSelect != null)
                        shippingOptionToSelect.Selected = true;
                }
            }
            else
                foreach (var error in getShippingOptionResponse.Errors)
                    model.Warnings.Add(error);

            return model;
        }

        [NonAction]
        protected virtual void SetShippingMethodToNull()
        {
            _genericAttributeService.SaveAttribute<ShippingOption>(_workContext.CurrentCustomer,
                SystemCustomerAttributeNames.SelectedShippingOption, null, _storeContext.CurrentStore.Id);
        }

        [NonAction]
        protected virtual bool SetShippingMethod(IList<ShoppingCartItem> cart, string shippingoption)
        {
            //parse selected method 
            if (String.IsNullOrEmpty(shippingoption))
                return false;
            var splittedOption = shippingoption.Split(new string[] { "___" }, StringSplitOptions.RemoveEmptyEntries);
            if (splittedOption.Length != 2)
                return false;
            string selectedName = splittedOption[0];
            string shippingRateComputationMethodSystemName = splittedOption[1];

            //find it
            //performance optimization. try cache first
            var shippingOptions = _workContext.CurrentCustomer.GetAttribute<List<ShippingOption>>(SystemCustomerAttributeNames.OfferedShippingOptions, _storeContext.CurrentStore.Id);
            if (shippingOptions == null || shippingOptions.Count == 0)
            {
                //not found? let's load them using shipping service
                shippingOptions = _shippingService
                    .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress,
                    allowedShippingRateComputationMethodSystemName: shippingRateComputationMethodSystemName)
                    .ShippingOptions
                    .ToList();
            }
            else
            {
                //loaded cached results. let's filter result by a chosen shipping rate computation method
                shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                                                 .ToList();
            }

            var shippingOption = shippingOptions
                .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
            if (shippingOption == null)
                return false;

            //save
            _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, _storeContext.CurrentStore.Id);

            return true;
        }

        #endregion

        #region Methods

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var brainTreePaymentSettings = _settingService.LoadSetting<BrainTreePaymentSettings>(storeScope);
            var brainTreePayPalPaymentSettings = _settingService.LoadSetting<BrainTreePayPalPaymentSettings>(storeScope);

            var model = new ConfigurationModel
            {
                ActiveStoreScopeConfiguration = storeScope,
                UseSandBox = brainTreePaymentSettings.UseSandBox,
                PublicKey = brainTreePaymentSettings.PublicKey,
                PrivateKey = brainTreePaymentSettings.PrivateKey,
                MerchantId = brainTreePaymentSettings.MerchantId,
                MerchantAccountId = brainTreePaymentSettings.MerchantAccountId,
                AdditionalFee = brainTreePaymentSettings.AdditionalFee,
                AdditionalFeePercentage = brainTreePaymentSettings.AdditionalFeePercentage,
                PayPalButtonSize = brainTreePayPalPaymentSettings.PayPalButtonSize,
                PayPalFlow = brainTreePayPalPaymentSettings.PayPalFlow,
                PayPalIntent = brainTreePayPalPaymentSettings.PayPalIntent,
                EnablePayPalCreditButton = brainTreePayPalPaymentSettings.EnablePayPalCreditButton,
                PayPalCreditButtonSize = brainTreePayPalPaymentSettings.PayPalCreditButtonSize,
                PayPalCreditFlow = brainTreePayPalPaymentSettings.PayPalCreditFlow,
                PayPalCreditIntent = brainTreePayPalPaymentSettings.PayPalCreditIntent,
                EnablePayPalKount = brainTreePayPalPaymentSettings.EnablePayPalKount,
                AllowedShippingTwoLetterCountryCodes = brainTreePayPalPaymentSettings.AllowedShippingTwoLetterCountryCodes,
                PayPalRestApiClientId = brainTreePayPalPaymentSettings.PayPalRestApiClientId,
                PayPalRestApiSecret = brainTreePayPalPaymentSettings.PayPalRestApiSecret,
                CacheAccessToken = brainTreePayPalPaymentSettings.CacheAccessToken,
                CacheFromPriceStatement = brainTreePayPalPaymentSettings.CacheFromPriceStatement,
                CacheFinancingCalculatorValues = brainTreePayPalPaymentSettings.CacheFinancingCalculatorValues
            };

            if (storeScope > 0)
            {
                model.UseSandbox_OverrideForStore = _settingService.SettingExists(brainTreePaymentSettings, x => x.UseSandBox, storeScope);
                model.PublicKey_OverrideForStore = _settingService.SettingExists(brainTreePaymentSettings, x => x.PublicKey, storeScope);
                model.PrivateKey_OverrideForStore = _settingService.SettingExists(brainTreePaymentSettings, x => x.PrivateKey, storeScope);
                model.MerchantId_OverrideForStore = _settingService.SettingExists(brainTreePaymentSettings, x => x.MerchantId, storeScope);
                model.MerchantAccountId_OverrideForStore = _settingService.SettingExists(brainTreePaymentSettings, x => x.MerchantAccountId, storeScope);
                model.AdditionalFee_OverrideForStore = _settingService.SettingExists(brainTreePaymentSettings, x => x.AdditionalFee, storeScope);
                model.AdditionalFeePercentage_OverrideForStore = _settingService.SettingExists(brainTreePaymentSettings, x => x.AdditionalFeePercentage, storeScope);
                model.PayPalButtonSize_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.PayPalButtonSize, storeScope);
                model.PayPalFlow_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.PayPalFlow, storeScope);
                model.PayPalIntent_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.PayPalIntent, storeScope);
                model.EnablePayPalCreditButton_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.EnablePayPalCreditButton, storeScope);
                model.PayPalCreditButtonSize_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.PayPalCreditButtonSize, storeScope);
                model.PayPalCreditFlow_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.PayPalCreditFlow, storeScope);
                model.PayPalCreditIntent_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.PayPalCreditIntent, storeScope);
                model.EnablePayPalKount_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.EnablePayPalKount, storeScope);
                model.AllowedShippingTwoLetterCountryCodes_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.AllowedShippingTwoLetterCountryCodes, storeScope);
                model.PayPalRestApiClientId_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.PayPalRestApiClientId, storeScope);
                model.PayPalRestApiSecret_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.PayPalRestApiSecret, storeScope);
                model.CacheAccessToken_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.CacheAccessToken, storeScope);
                model.CacheFromPriceStatement_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.CacheFromPriceStatement, storeScope);
                model.CacheFinancingCalculatorValues_OverrideForStore = _settingService.SettingExists(brainTreePayPalPaymentSettings, x => x.CacheFinancingCalculatorValues, storeScope);
            }

            return View("~/Plugins/Payments.BrainTreePayPal/Views/Configure.cshtml", model);
        }

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var brainTreePaymentSettings = _settingService.LoadSetting<BrainTreePaymentSettings>(storeScope);
            var brainTreePayPalPaymentSettings = _settingService.LoadSetting<BrainTreePayPalPaymentSettings>(storeScope);

            //save settings
            brainTreePaymentSettings.UseSandBox = model.UseSandBox;
            brainTreePaymentSettings.PublicKey = model.PublicKey;
            brainTreePaymentSettings.PrivateKey = model.PrivateKey;
            brainTreePaymentSettings.MerchantId = model.MerchantId;
            brainTreePaymentSettings.MerchantAccountId = model.MerchantAccountId;
            brainTreePaymentSettings.AdditionalFee = model.AdditionalFee;
            brainTreePaymentSettings.AdditionalFeePercentage = model.AdditionalFeePercentage;
            brainTreePayPalPaymentSettings.PayPalButtonSize = model.PayPalButtonSize;
            brainTreePayPalPaymentSettings.PayPalFlow = model.PayPalFlow;
            brainTreePayPalPaymentSettings.PayPalIntent = model.PayPalIntent;
            brainTreePayPalPaymentSettings.EnablePayPalCreditButton = model.EnablePayPalCreditButton;
            brainTreePayPalPaymentSettings.PayPalCreditButtonSize = model.PayPalCreditButtonSize;
            brainTreePayPalPaymentSettings.PayPalCreditFlow = model.PayPalCreditFlow;
            brainTreePayPalPaymentSettings.PayPalCreditIntent = model.PayPalCreditIntent;
            brainTreePayPalPaymentSettings.EnablePayPalKount = model.EnablePayPalKount;
            brainTreePayPalPaymentSettings.AllowedShippingTwoLetterCountryCodes = model.AllowedShippingTwoLetterCountryCodes;
            brainTreePayPalPaymentSettings.PayPalRestApiClientId = model.PayPalRestApiClientId;
            brainTreePayPalPaymentSettings.PayPalRestApiSecret = model.PayPalRestApiSecret;
            brainTreePayPalPaymentSettings.CacheAccessToken = model.CacheAccessToken;
            brainTreePayPalPaymentSettings.CacheFromPriceStatement = model.CacheFromPriceStatement;
            brainTreePayPalPaymentSettings.CacheFinancingCalculatorValues = model.CacheFinancingCalculatorValues;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(brainTreePaymentSettings, x => x.UseSandBox, model.UseSandbox_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePaymentSettings, x => x.PublicKey, model.PublicKey_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePaymentSettings, x => x.PrivateKey, model.PrivateKey_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePaymentSettings, x => x.MerchantId, model.MerchantId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePaymentSettings, x => x.MerchantAccountId, model.MerchantAccountId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePaymentSettings, x => x.AdditionalFee, model.AdditionalFee_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePaymentSettings, x => x.AdditionalFeePercentage, model.AdditionalFeePercentage_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.PayPalButtonSize, model.PayPalButtonSize_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.PayPalFlow, model.PayPalFlow_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.PayPalIntent, model.PayPalIntent_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.EnablePayPalCreditButton, model.EnablePayPalCreditButton_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.PayPalCreditButtonSize, model.PayPalCreditButtonSize_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.PayPalCreditFlow, model.PayPalCreditFlow_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.PayPalCreditIntent, model.PayPalCreditIntent_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.EnablePayPalKount, model.EnablePayPalKount_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.AllowedShippingTwoLetterCountryCodes, model.AllowedShippingTwoLetterCountryCodes_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.PayPalRestApiClientId, model.PayPalRestApiClientId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.PayPalRestApiSecret, model.PayPalRestApiSecret_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.CacheAccessToken, model.CacheAccessToken_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.CacheFromPriceStatement, model.CacheFromPriceStatement_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(brainTreePayPalPaymentSettings, x => x.CacheFinancingCalculatorValues, model.CacheFinancingCalculatorValues_OverrideForStore, storeScope, false);


            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        [HttpPost]
        public IActionResult ProcessPayload(PayPalPayload payload, string deviceData)
        {
            try
            {
                if (payload == null || payload.details == null || payload.details.shippingAddress == null)
                    throw new NopException("Invalid payload");

                // create new address if payload shipping address is not existing
                var currentCustomer = _customerService.GetCustomerById(_workContext.CurrentCustomer.Id);
                var payloadDetails = payload.details;
                var payloadBillingAddress = payload.details.billingAddress;
                var payloadShippingAddress = payload.details.shippingAddress;

                // check if the shipping address country is allowed
                List<string> allowedCountryCodes = new List<string>();
                
                if (!String.IsNullOrWhiteSpace(_brainTreePayPalPaymentSettings.AllowedShippingTwoLetterCountryCodes))
                    allowedCountryCodes.AddRange(_brainTreePayPalPaymentSettings.AllowedShippingTwoLetterCountryCodes.ToLower().Replace(" ", "").Split(','));

                if (!allowedCountryCodes.Contains(payloadShippingAddress.countryCode.ToLower()))
                    return Json(new { success = false, message = "Sorry, we are currently not offering shipment to your country." });

                Address billingAddress = null;
                Address shippingAddress = null;

                if (payloadBillingAddress != null)
                {
                    // get/update billing and shipping address
                    int? payloadBillingStateProvinceId = null;
                    var payloadBillingStateProvince = _stateProvinceService.GetStateProvinceByAbbreviation(payloadBillingAddress.state);
                    if (payloadBillingStateProvince != null)
                        payloadBillingStateProvinceId = payloadBillingStateProvince.Id;
                    int? payloadBillingCountryId = null;
                    var payloadBillingCountry = _countryService.GetCountryByTwoLetterIsoCode(payloadBillingAddress.countryCode);
                    if (payloadBillingCountry != null)
                        payloadBillingCountryId = payloadBillingCountry.Id;

                    billingAddress = currentCustomer.Addresses.ToList().FindAddress(
                        payloadDetails.firstName, payloadDetails.lastName, payloadDetails.phone,
                        payloadDetails.email, string.Empty, string.Empty, payloadBillingAddress.line1, payloadBillingAddress.line2, payloadBillingAddress.city,
                        payloadBillingStateProvinceId, payloadBillingAddress.postalCode, payloadBillingCountryId,
                        //TODO process custom attributes
                        null);

                    if (billingAddress == null)
                    {
                        billingAddress = new Address()
                        {
                            FirstName = payloadDetails.firstName,
                            LastName = payloadDetails.lastName,
                            PhoneNumber = payloadDetails.phone,
                            Email = payloadDetails.email,
                            FaxNumber = string.Empty,
                            Company = string.Empty,
                            Address1 = payloadBillingAddress.line1,
                            Address2 = payloadBillingAddress.line2,
                            City = payloadBillingAddress.city,
                            StateProvinceId = payloadBillingStateProvinceId,
                            ZipPostalCode = payloadBillingAddress.postalCode,
                            CountryId = payloadBillingCountryId,
                            CreatedOnUtc = DateTime.UtcNow,
                        };
                        currentCustomer.Addresses.Add(billingAddress);
                    }
                }

                // get/update billing and shipping address
                int? payloadShippingStateProvinceId = null;
                var payloadShippingStateProvince = _stateProvinceService.GetStateProvinceByAbbreviation(payloadShippingAddress.state);
                if (payloadShippingStateProvince != null)
                    payloadShippingStateProvinceId = payloadShippingStateProvince.Id;
                int? payloadShippingCountryId = null;
                var payloadShippingCountry = _countryService.GetCountryByTwoLetterIsoCode(payloadShippingAddress.countryCode);
                if (payloadShippingCountry != null)
                    payloadShippingCountryId = payloadShippingCountry.Id;

                shippingAddress = currentCustomer.Addresses.ToList().FindAddress(
                    payloadDetails.firstName, payloadDetails.lastName, payloadDetails.phone,
                    payloadDetails.email, string.Empty, string.Empty, payloadShippingAddress.line1, payloadShippingAddress.line2, payloadShippingAddress.city,
                    payloadShippingStateProvinceId, payloadShippingAddress.postalCode, payloadShippingCountryId,
                    //TODO process custom attributes
                    null);

                if (shippingAddress == null)
                {
                    shippingAddress = new Address()
                    {
                        FirstName = payloadDetails.firstName,
                        LastName = payloadDetails.lastName,
                        PhoneNumber = payloadDetails.phone,
                        Email = payloadDetails.email,
                        FaxNumber = string.Empty,
                        Company = string.Empty,
                        Address1 = payloadShippingAddress.line1,
                        Address2 = payloadShippingAddress.line2,
                        City = payloadShippingAddress.city,
                        StateProvinceId = payloadShippingStateProvinceId,
                        ZipPostalCode = payloadShippingAddress.postalCode,
                        CountryId = payloadShippingCountryId,
                        CreatedOnUtc = DateTime.UtcNow,
                    };
                    currentCustomer.Addresses.Add(shippingAddress);
                }

                //set default addresses
                currentCustomer.BillingAddress = billingAddress ?? shippingAddress;
                currentCustomer.ShippingAddress = shippingAddress;

                _customerService.UpdateCustomer(currentCustomer);

                HttpContext.Session.Set(BrainTreePayPalPaymentSystemNames.PAYMENT_METHOD_NONCE, payload.nonce);
                HttpContext.Session.Set(BrainTreePayPalPaymentSystemNames.DEVICE_DATA, deviceData);
                
                if (payload.creditFinancingOffered != null)
                {
                    HttpContext.Session.Set(BrainTreePayPalPaymentSystemNames.SELECTED_FINANCING_OPTION, payload.creditFinancingOffered);
                }
                else
                {
                    HttpContext.Session.Remove(BrainTreePayPalPaymentSystemNames.SELECTED_FINANCING_OPTION);
                }

                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    BrainTreePayPalPaymentSystemNames.PAYMENT_METHOD_SYSTEM_NAME,
                    _storeContext.CurrentStore.Id);

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                _logger.Error("Unexpected exception occurred while processing PayPal payload.", ex);
                throw;
            }
        }

        [HttpGet]
        public IActionResult SetShippingMethod()
        {
            var cart = GetCart();

            if (!cart.RequiresShipping())
                return RedirectToAction("Confirm");

            var model = PrepareShippingMethodModel(cart);

            if (_shippingSettings.BypassShippingMethodSelectionIfOnlyOne &&
                model.ShippingMethods.Count == 1)
            {
                //if we have only one shipping method, then a customer doesn't have to choose a shipping method
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.SelectedShippingOption,
                    model.ShippingMethods.First().ShippingOption,
                    _storeContext.CurrentStore.Id);

                return RedirectToAction("Confirm");
            }

            return View("~/Plugins/Payments.BrainTreePayPal/Views/SetShippingMethod.cshtml", model);
        }

        [HttpPost, ActionName("SetShippingMethod")]
        public IActionResult SetShippingMethod(string shippingoption)
        {
            //validation
            var cart = GetCart();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (!IsAllowedToCheckout())
                return new UnauthorizedResult();

            if (!cart.RequiresShipping())
            {
                SetShippingMethodToNull();
                return RedirectToAction("Confirm");
            }

            var success = SetShippingMethod(cart, shippingoption);

            return RedirectToAction(success ? "Confirm" : "SetShippingMethod");
        }

        public IActionResult Confirm()
        {
            //validation
            var cart = GetCart();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (!IsAllowedToCheckout())
                return new UnauthorizedResult();

            //model
            var model = PrepareConfirmOrderModel(cart);

            return View("~/Plugins/Payments.BrainTreePayPal/Views/Confirm.cshtml", model);
        }

        [HttpPost, ActionName("Confirm")]
        public IActionResult ConfirmOrder()
        {
            //validation
            var cart = GetCart();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            if (!IsAllowedToCheckout())
                return new UnauthorizedResult();
            //load insurance 
            var insurenceShoppingCartItems = _workContext.CurrentCustomer.GetAttribute<string>(
                SystemCustomerAttributeNames.SelectedShoppingCartItemWithInsurence,
                _genericAttributeService, _storeContext.CurrentStore.Id);
            if (string.IsNullOrEmpty(insurenceShoppingCartItems))
            {
                insurenceShoppingCartItems = string.Empty;
            }
            
            //model
            var checkoutPlaceOrderModel = PlaceOrder(insurenceShoppingCartItems);
            if (checkoutPlaceOrderModel.RedirectToCart)
                return RedirectToRoute("ShoppingCart");

            if (checkoutPlaceOrderModel.IsRedirected)
                return Content("Redirected");

            if (!string.IsNullOrEmpty(insurenceShoppingCartItems))
            {
                return RedirectToRoute("InsuranceDirectDebitInfo", new { orderId = checkoutPlaceOrderModel.CompletedId });
            }

            if (checkoutPlaceOrderModel.CompletedId.HasValue)
                return RedirectToRoute("CheckoutCompleted", new { orderId = checkoutPlaceOrderModel.CompletedId });

            //If we got this far, something failed, redisplay form
            return View("~/Plugins/Payments.BrainTreePayPal/Views/Confirm.cshtml", checkoutPlaceOrderModel);
        }

        [HttpPost]
        public IActionResult GetFinancingCalculatorValues(decimal price)
        {
            var result = _brainTreePayPalService.GetFinancingCalculatorValues(price, _workContext.WorkingCurrency.CurrencyCode);
            
            return Json(result);
        }

        #endregion
    }
}