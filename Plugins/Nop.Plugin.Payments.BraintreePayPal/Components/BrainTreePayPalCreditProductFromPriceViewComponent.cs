﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Plugin.Payments.BrainTree;
using Nop.Plugin.Payments.BrainTreePayPal.Models;
using Nop.Plugin.Payments.BrainTreePayPal.Services;
using Nop.Services.Catalog;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace Nop.Plugin.Payments.BrainTreePayPal.Components
{
    [ViewComponent(Name = "BrainTreePayPalCreditProductFromPrice")]
    public class BrainTreePayPalCreditProductFromPriceViewComponent : NopViewComponent
    {
        #region Fields

        private readonly IBrainTreePayPalService _brainTreePayPalService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IProductService _productService;
        private readonly IWorkContext _workContext;
        private readonly BrainTreePaymentSettings _brainTreePaymentSettings;
        private readonly BrainTreePayPalPaymentSettings _brainTreePayPalPaymentSettings;

        #endregion

        #region Ctor

        public BrainTreePayPalCreditProductFromPriceViewComponent(IBrainTreePayPalService brainTreePayPalService,
            IPriceCalculationService priceCalculationService,
            IPriceFormatter priceFormatter,
            IProductService productService,
            IWorkContext workContext,
            BrainTreePaymentSettings brainTreePaymentSettings,
            BrainTreePayPalPaymentSettings brainTreePayPalCreditPaymentSettings)
        {
            this._brainTreePayPalService = brainTreePayPalService;
            this._priceCalculationService = priceCalculationService;
            this._priceFormatter = priceFormatter;
            this._productService = productService;
            this._workContext = workContext;
            this._brainTreePaymentSettings = brainTreePaymentSettings;
            this._brainTreePayPalPaymentSettings = brainTreePayPalCreditPaymentSettings;
        }

        #endregion

        #region Methods

        // Additional Data = Product Id
        public IViewComponentResult Invoke(int additionalData)
        {
            //var product = _productService.GetProductById(additionalData);
            //if (product == null)
            //    throw new NopException($"Product does not exist. ID {additionalData}");

            //var price = _priceCalculationService.GetFinalPrice(product, _workContext.CurrentCustomer);

            //if (price < 99)
            //    return View("~/Plugins/Payments.BrainTreePayPal/Views/NullPaymentInfo.cshtml");

            //PayPalAuthTokenResult tokenResult = null;

            //using (var client = new WebClient())
            //{
            //    client.BaseAddress = _brainTreePaymentSettings.UseSandBox ? "https://api.sandbox.paypal.com" : "https://api.paypal.com";

            //    string credentials = Convert.ToBase64String(
            //        Encoding.ASCII.GetBytes(
            //            _brainTreePayPalPaymentSettings.PayPalRestApiClientId
            //            + ":"
            //            + _brainTreePayPalPaymentSettings.PayPalRestApiSecret));
            //    client.Headers[HttpRequestHeader.Authorization] = string.Format(
            //        "Basic {0}", credentials);

            //    var parameters = new NameValueCollection
            //    {
            //        { "grant_type", "client_credentials" }
            //    };

            //    var responsebytes = client.UploadValues("/v1/oauth2/token", "POST", parameters);
            //    string responsebody = Encoding.UTF8.GetString(responsebytes);

            //    tokenResult = JsonConvert.DeserializeObject<PayPalAuthTokenResult>(responsebody);
            //}

            //CalculatedFinancingOptionsResult result = null;

            //using (var client = new WebClient())
            //{
            //    client.BaseAddress = _brainTreePaymentSettings.UseSandBox ? "https://api.sandbox.paypal.com" : "https://api.paypal.com";
            //    client.Headers[HttpRequestHeader.Authorization] = $"Bearer {tokenResult.AccessToken}";
            //    client.Headers[HttpRequestHeader.ContentType] = "application/json";

            //    var options = new PayPalCreditFinancingOptionRequestParam
            //    {
            //        FinancingCountryCode = "GB",
            //        TransactionAmount = new PayPalCreditFinancingOptionRequestParamTransactionAmount
            //        {
            //            CurrencyCode = _workContext.WorkingCurrency.CurrencyCode,
            //            Value = price
            //        }
            //    };

            //    var optionsJson = JsonConvert.SerializeObject(options, Formatting.Indented);
            //    var optionsBytes = Encoding.Default.GetBytes(optionsJson);

            //    var responsebytes = client.UploadData("/v1/credit/calculated-financing-options", "POST", optionsBytes);
            //    string responsebody = Encoding.UTF8.GetString(responsebytes);

            //    result = JsonConvert.DeserializeObject<CalculatedFinancingOptionsResult>(responsebody);
            //}

            //var model = new FromPriceModel
            //{
            //    FormattedFromPrice = ""
            //};

            //try
            //{
            //    var minMonthly = result.FinancingOptions.Min(fo => fo.QualifyingFinancingOptions.Where(x => x.CreditFinancing.CreditType == "INST").Min(qfo => qfo.MonthlyPayment.Value));
            //    model.FormattedFromPrice = minMonthly > 0 ? _priceFormatter.FormatPrice(minMonthly) : "";
            //}
            //catch
            //{
            //    // do nothing
            //}

            var model = new FromPriceModel
            {
                FormattedFromPrice = _brainTreePayPalService.GetProductFromPriceStatement(additionalData)
            };

            if (string.IsNullOrWhiteSpace(model.FormattedFromPrice))
                return View("~/Plugins/Payments.BrainTreePayPal/Views/NullPaymentInfo.cshtml");

            return View("~/Plugins/Payments.BrainTreePayPal/Views/FromPrice.cshtml", model);
        }

        #endregion
    }
}