﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.BrainTree;
using Nop.Plugin.Payments.BrainTreePayPal.Models;
using Nop.Plugin.Payments.BrainTreePayPal.Services;
using Nop.Services.Discounts;
using Nop.Services.Orders;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace Nop.Plugin.Payments.BrainTreePayPal.Components
{
    [ViewComponent(Name = "BrainTreePayPalCreditFinancingCalculator")]
    public class BrainTreePayPalCreditFinancingCalculatorViewComponent : NopViewComponent
    {
        #region Fields

        private readonly IBrainTreePayPalService _brainTreePayPalService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly BrainTreePaymentSettings _brainTreePaymentSettings;
        private readonly BrainTreePayPalPaymentSettings _brainTreePayPalPaymentSettings;

        #endregion

        #region Ctor

        public BrainTreePayPalCreditFinancingCalculatorViewComponent(IBrainTreePayPalService brainTreePayPalService,
            IOrderTotalCalculationService orderTotalCalculationService,
            IStoreContext storeContext,
            IWorkContext workContext,
            BrainTreePaymentSettings brainTreePaymentSettings,
            BrainTreePayPalPaymentSettings brainTreePayPalCreditPaymentSettings)
        {
            this._brainTreePayPalService = brainTreePayPalService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._brainTreePaymentSettings = brainTreePaymentSettings;
            this._brainTreePayPalPaymentSettings = brainTreePayPalCreditPaymentSettings;
        }

        #endregion

        #region Methods

        // Additional Data = Product Price
        public IViewComponentResult Invoke(decimal additionalData)
        {
            if (additionalData < 99)
                return View("~/Plugins/Payments.BrainTreePayPal/Views/NullPaymentInfo.cshtml");

            //// get the paypal token
            //PayPalAuthTokenResult result = null;

            //using (var client = new WebClient())
            //{
            //    string credentials = Convert.ToBase64String(
            //        Encoding.ASCII.GetBytes(
            //            _brainTreePayPalPaymentSettings.PayPalRestApiClientId
            //            + ":"
            //            + _brainTreePayPalPaymentSettings.PayPalRestApiSecret));
            //    client.Headers[HttpRequestHeader.Authorization] = string.Format(
            //        "Basic {0}", credentials);

            //    client.BaseAddress = _brainTreePaymentSettings.UseSandBox ? "https://api.sandbox.paypal.com" : "https://api.paypal.com";

            //    var parameters = new NameValueCollection
            //    {
            //        { "grant_type", "client_credentials" }
            //    };

            //    var responsebytes = client.UploadValues("/v1/oauth2/token", "POST", parameters);
            //    string responsebody = Encoding.UTF8.GetString(responsebytes);

            //    result = JsonConvert.DeserializeObject<PayPalAuthTokenResult>(responsebody);
            //}

            var accessToken = _brainTreePayPalService.GetAccessToken();

            var model = new FinancingCalculatorModel
            {
                InitialValue = additionalData,
                Token = accessToken
            };

            return View("~/Plugins/Payments.BrainTreePayPal/Views/FinancingCalculator.cshtml", model);
        }

        #endregion
    }
}