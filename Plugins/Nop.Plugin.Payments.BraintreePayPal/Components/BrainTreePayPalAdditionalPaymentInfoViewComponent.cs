﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Payments.BrainTreePayPal.Models;
using Nop.Plugin.Payments.BrainTreePayPal.Services;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Orders;
using Nop.Web.Framework.Components;
using System;

namespace Nop.Plugin.Payments.BrainTreePayPal.Components
{
    [ViewComponent(Name = "BrainTreePayPalAdditionalPaymentInfo")]
    public class BrainTreePayPalAdditionalPaymentInfoViewComponent : NopViewComponent
    {
        #region Fields

        private readonly IBrainTreePayPalService _brainTreePayPalService;
        private readonly ICurrencyService _currencyService;
        private readonly IOrderService _orderService;
        private readonly IPriceFormatter _priceFormatter;

        #endregion

        #region Ctor

        public BrainTreePayPalAdditionalPaymentInfoViewComponent(IBrainTreePayPalService brainTreePayPalService,
            ICurrencyService currencyService,
            IOrderService orderService,
            IPriceFormatter priceFormatter)
        {
            this._brainTreePayPalService = brainTreePayPalService;
            this._currencyService = currencyService;
            this._orderService = orderService;
            this._priceFormatter = priceFormatter;
        }

        #endregion

        #region Methods

        // Additional Data = Order ID
        public IViewComponentResult Invoke(int additionalData)
        {
            var model = new AdditionalPaymentInfoModel();

            // get order data
            var order = _orderService.GetOrderById(additionalData);
            if (order != null && order.PaymentMethodSystemName.ToLower() == BrainTreePayPalPaymentSystemNames.PAYMENT_METHOD_SYSTEM_NAME.ToLower())
            {
                model.IsPayPal = true;

                var orderFinancingDetails = _brainTreePayPalService.GetOrderPayPalCreditFinancingDetails(additionalData);
                if (orderFinancingDetails != null)
                {
                    model.IsPayPalCredit = true;
                    model.PayPalCreditTerm = orderFinancingDetails.Term.ToString() + " Months";

                    var totalCostCurrency = _currencyService.GetCurrencyByCode(orderFinancingDetails.TotalCostCurrency);
                    var totalInterestCurrency = _currencyService.GetCurrencyByCode(orderFinancingDetails.TotalInterestCurrency);
                    var monthlyPaymentCurrency = _currencyService.GetCurrencyByCode(orderFinancingDetails.MonthlyPaymentCurrency);

                    model.PayPalCreditInterest = _priceFormatter.FormatPrice(Convert.ToDecimal(orderFinancingDetails.TotalInterestValue), true, totalInterestCurrency);
                    model.PayPalCreditMonthlyPayment = _priceFormatter.FormatPrice(Convert.ToDecimal(orderFinancingDetails.MonthlyPaymentValue), true, monthlyPaymentCurrency);
                    model.PayPalCreditTotal = _priceFormatter.FormatPrice(Convert.ToDecimal(orderFinancingDetails.TotalCostValue), true, totalCostCurrency);
                }
            }

            return View("~/Plugins/Payments.BrainTreePayPal/Views/AdditionalPaymentInfo.cshtml", model);
        }

        #endregion
    }
}