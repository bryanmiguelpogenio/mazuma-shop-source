﻿using Braintree;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.BrainTree;
using Nop.Plugin.Payments.BrainTreePayPal.Models;
using Nop.Plugin.Payments.BrainTreePayPal.Services;
using Nop.Services.Catalog;
using Nop.Services.Discounts;
using Nop.Services.Orders;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using Environment = Braintree.Environment;

namespace Nop.Plugin.Payments.BrainTreePayPal.Components
{
    [ViewComponent(Name = "PaymentBrainTreePayPal")]
    public class PaymentBrainTreePayPalViewComponent : NopViewComponent
    {
        #region Fields

        private readonly IBrainTreePayPalService _brainTreePayPalService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly BrainTreePaymentSettings _brainTreePaymentSettings;
        private readonly BrainTreePayPalPaymentSettings _brainTreePayPalPaymentSettings;

        #endregion

        #region Ctor

        public PaymentBrainTreePayPalViewComponent(IBrainTreePayPalService brainTreePayPalService,
            IOrderTotalCalculationService orderTotalCalculationService,
            IPriceFormatter priceFormatter,
            IStoreContext storeContext,
            IWorkContext workContext,
            BrainTreePaymentSettings brainTreePaymentSettings,
            BrainTreePayPalPaymentSettings brainTreePayPalPaymentSettings)
        {
            this._brainTreePayPalService = brainTreePayPalService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._priceFormatter = priceFormatter;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._brainTreePaymentSettings = brainTreePaymentSettings;
            this._brainTreePayPalPaymentSettings = brainTreePayPalPaymentSettings;
        }

        #endregion

        public IViewComponentResult Invoke()
        {
            try
            {
                //get settings
                var useSandBox = _brainTreePaymentSettings.UseSandBox;
                var merchantId = _brainTreePaymentSettings.MerchantId;
                var publicKey = _brainTreePaymentSettings.PublicKey;
                var privateKey = _brainTreePaymentSettings.PrivateKey;

                //new gateway
                var gateway = new BraintreeGateway
                {
                    Environment = useSandBox ? Environment.SANDBOX : Environment.PRODUCTION,
                    MerchantId = merchantId,
                    PublicKey = publicKey,
                    PrivateKey = privateKey
                };

                var model = new PaymentInfoModel
                {
                    AuthorizationCode = gateway.ClientToken.generate(new ClientTokenRequest()),
                    CurrencyCode = _workContext.WorkingCurrency.CurrencyCode,
                    Locale = _workContext.WorkingCurrency.DisplayLocale,
                    EnablePayPalCreditButton = _brainTreePayPalPaymentSettings.EnablePayPalCreditButton,
                    EnablePayPalKount = _brainTreePayPalPaymentSettings.EnablePayPalKount,
                    PayPalButtonSize = _brainTreePayPalPaymentSettings.PayPalButtonSize,
                    PayPalCreditButtonSize = _brainTreePayPalPaymentSettings.PayPalCreditButtonSize,
                    PayPalCreditFlow = _brainTreePayPalPaymentSettings.PayPalCreditFlow,
                    PayPalCreditIntent = _brainTreePayPalPaymentSettings.PayPalCreditIntent,
                    PayPalFlow = _brainTreePayPalPaymentSettings.PayPalFlow,
                    PayPalIntent = _brainTreePayPalPaymentSettings.PayPalIntent,
                    UseSandBox = _brainTreePaymentSettings.UseSandBox
                };

                var cart = _workContext.CurrentCustomer.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .LimitPerStore(_storeContext.CurrentStore.Id).ToList();

                if (!cart.Any())
                    throw new NopException("Cart is empty");

                //order total (and applied discounts, gift cards, reward points)
                var orderTotal = _orderTotalCalculationService.GetShoppingCartTotal(cart, out decimal orderDiscountAmount, out List<DiscountForCaching> orderAppliedDiscounts, out List<AppliedGiftCard> appliedGiftCards, out int redeemedRewardPoints, out decimal redeemedRewardPointsAmount);
                if (!orderTotal.HasValue)
                    throw new NopException("Order total couldn't be calculated");

                model.Amount = orderTotal.Value;

                // get the paypal token
                //PayPalAuthTokenResult tokenResult = null;

                //using (var client = new WebClient())
                //{
                //    client.BaseAddress = _brainTreePaymentSettings.UseSandBox ? "https://api.sandbox.paypal.com" : "https://api.paypal.com";

                //    string credentials = Convert.ToBase64String(
                //        Encoding.ASCII.GetBytes(
                //            _brainTreePayPalPaymentSettings.PayPalRestApiClientId
                //            + ":"
                //            + _brainTreePayPalPaymentSettings.PayPalRestApiSecret));
                //    client.Headers[HttpRequestHeader.Authorization] = string.Format(
                //        "Basic {0}", credentials);

                //    var parameters = new NameValueCollection
                //    {
                //        { "grant_type", "client_credentials" }
                //    };

                //    var responsebytes = client.UploadValues("/v1/oauth2/token", "POST", parameters);
                //    string responsebody = Encoding.UTF8.GetString(responsebytes);

                //    tokenResult = JsonConvert.DeserializeObject<PayPalAuthTokenResult>(responsebody);
                //}

                var accessToken = _brainTreePayPalService.GetAccessToken();

                if (model.EnablePayPalCreditButton)
                {
                    if (orderTotal.Value < 99)
                    {
                        model.EnablePayPalCreditButton = false;
                    }
                    else
                    {
                        CalculatedFinancingOptionsResult result = null;

                        using (var client = new WebClient())
                        {
                            client.BaseAddress = _brainTreePaymentSettings.UseSandBox ? "https://api.sandbox.paypal.com" : "https://api.paypal.com";
                            client.Headers[HttpRequestHeader.Authorization] = $"Bearer {accessToken}";
                            client.Headers[HttpRequestHeader.ContentType] = "application/json";

                            var options = new PayPalCreditFinancingOptionRequestParam
                            {
                                FinancingCountryCode = "GB",
                                TransactionAmount = new PayPalCreditFinancingOptionRequestParamTransactionAmount
                                {
                                    CurrencyCode = _workContext.WorkingCurrency.CurrencyCode,
                                    Value = model.Amount
                                }
                            };

                            var optionsJson = JsonConvert.SerializeObject(options, Formatting.Indented);
                            var optionsBytes = Encoding.Default.GetBytes(optionsJson);

                            var responsebytes = client.UploadData("/v1/credit/calculated-financing-options", "POST", optionsBytes);
                            string responsebody = Encoding.UTF8.GetString(responsebytes);

                            result = JsonConvert.DeserializeObject<CalculatedFinancingOptionsResult>(responsebody);
                        }

                        try
                        {
                            var qualifyingOptions = result.FinancingOptions.First().QualifyingFinancingOptions.Where(x => x.CreditFinancing.CreditType == "INST");
                            var maxNumberOfMonths = qualifyingOptions.Max(qfo => qfo.CreditFinancing.Term);
                            var financingOption = qualifyingOptions.First(qfo => qfo.CreditFinancing.Term == maxNumberOfMonths);

                            model.PayPalCreditNumberOfMonths = maxNumberOfMonths;
                            model.PayPalCreditMonthlyPayment = _priceFormatter.FormatPrice(financingOption.MonthlyPayment.Value);
                            model.PayPalCreditInstallmentRate = financingOption.CreditFinancing.Apr + " %";
                            model.PayPalCreditCostOfPurchase = _priceFormatter.FormatPrice(model.Amount);
                            model.PayPalCreditTotalIncludingInterest = _priceFormatter.FormatPrice(financingOption.TotalCost.Value);
                        }
                        catch
                        {
                            // do nothing
                        }
                    }
                }

                return View("~/Plugins/Payments.BrainTreePayPal/Views/PaymentInfo.cshtml", model);
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }
    }
}