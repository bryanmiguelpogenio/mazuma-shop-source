﻿using Nop.Web.Framework.Mvc.Models;
using System.Collections.Generic;

namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class CheckoutConfirmModel : BaseNopModel
    {
        public CheckoutConfirmModel()
        {
            Warnings = new List<string>();
        }

        public bool TermsOfServiceOnOrderConfirmPage { get; set; }
        public bool TermsOfServicePopup { get; set; }
        public string MinOrderTotalWarning { get; set; }

        public IList<string> Warnings { get; set; }
    }

    public class CheckoutPlaceOrderModel : CheckoutConfirmModel
    {
        public bool RedirectToCart { get; set; }

        public bool IsRedirected { get; set; }

        public int? CompletedId { get; set; }
    }
}