﻿namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class PayPalCreditFinancingOfferedMonthlyPayment
    {
        public string value { get; set; }
        public string currency { get; set; }
    }
}