﻿namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class TransactionLogModel
    {
        public string TransactionId { get; set; }
    }
}