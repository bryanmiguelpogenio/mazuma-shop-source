﻿using Newtonsoft.Json;
using System;

namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    [Serializable]
    public class CalculatedFinancingOptionsResult
    {
        [JsonProperty(PropertyName = "configuration_owner_account")]
        public string ConfigurationOwnerAccount { get; set; }

        [JsonProperty(PropertyName = "financing_fee_charged_to_separate_account")]
        public bool FinancingFeeChargedToSeparateAccount { get; set; }

        [JsonProperty(PropertyName = "financing_options")]
        public CalculatedFinancingOptionRoot[] FinancingOptions { get; set; }
    }

    [Serializable]
    public class CalculatedFinancingOptionRoot
    {
        [JsonProperty(PropertyName = "product")]
        public string Product { get; set; }

        [JsonProperty(PropertyName = "qualifying_financing_options")]
        public CalculatedFinancingOption[] QualifyingFinancingOptions { get; set; }

        [JsonProperty(PropertyName = "non_qualifying_financing_options")]
        public CalculatedFinancingOption[] NonQualifyingFinancingOptions { get; set; }
    }

    [Serializable]
    public class CalculatedFinancingOption
    {
        [JsonProperty(PropertyName = "credit_financing")]
        public CalculatedFinancingOptionCreditFinancing CreditFinancing { get; set; }

        [JsonProperty(PropertyName = "min_amount")]
        public ValueCurrency MinAmount { get; set; }

        [JsonProperty(PropertyName = "monthly_percentage_rate")]
        public string MonthlyPercentageRate { get; set; }

        [JsonProperty(PropertyName = "monthly_payment")]
        public ValueCurrency MonthlyPayment { get; set; }

        [JsonProperty(PropertyName = "total_interest")]
        public ValueCurrency TotalInterest { get; set; }

        [JsonProperty(PropertyName = "total_cost")]
        public ValueCurrency TotalCost { get; set; }

        [JsonProperty(PropertyName = "paypal_subsidy")]
        public bool PayPalSubsidy { get; set; }

        [JsonProperty(PropertyName = "estimated_installments")]
        public CalculatedFinancingOptionEstimatedInstallment[] EstimatedInstallments { get; set; }
    }

    [Serializable]
    public class CalculatedFinancingOptionCreditFinancing
    {
        [JsonProperty(PropertyName = "financing_code")]
        public string FinancingCode { get; set; }

        [JsonProperty(PropertyName = "product")]
        public string Product { get; set; }

        [JsonProperty(PropertyName = "apr")]
        public string Apr { get; set; }

        [JsonProperty(PropertyName = "nominal_rate")]
        public string NominalRate { get; set; }

        [JsonProperty(PropertyName = "term")]
        public int Term { get; set; }

        [JsonProperty(PropertyName = "interval")]
        public string Interval { get; set; }

        [JsonProperty(PropertyName = "country_code")]
        public string CountryCode { get; set; }

        [JsonProperty(PropertyName = "credit_type")]
        public string CreditType { get; set; }

        [JsonProperty(PropertyName = "vendor_financing_id")]
        public string VendorFinancingId { get; set; }

        [JsonProperty(PropertyName = "enabled")]
        public bool Enabled { get; set; }

        [JsonProperty(PropertyName = "links")]
        public CalculatedFinancingOptionCreditFinancingLink[] Links { get; set; }
    }

    [Serializable]
    public class CalculatedFinancingOptionCreditFinancingLink
    {
        [JsonProperty(PropertyName = "href")]
        public string Href { get; set; }

        [JsonProperty(PropertyName = "rel")]
        public string Rel { get; set; }

        [JsonProperty(PropertyName = "method")]
        public string Method { get; set; }
    }

    [Serializable]
    public class ValueCurrency
    {
        [JsonProperty(PropertyName = "currency_code")]
        public string CurrencyCode { get; set; }

        [JsonProperty(PropertyName = "value")]
        public decimal Value { get; set; }
    }

    [Serializable]
    public class CalculatedFinancingOptionEstimatedInstallment
    {
        [JsonProperty(PropertyName = "payment_number")]
        public int PaymentNumber { get; set; }

        [JsonProperty(PropertyName = "total_payment")]
        public ValueCurrency TotalPayment { get; set; }

        [JsonProperty(PropertyName = "principal")]
        public ValueCurrency Principal { get; set; }

        [JsonProperty(PropertyName = "interest")]
        public ValueCurrency Interest { get; set; }
    }
}
