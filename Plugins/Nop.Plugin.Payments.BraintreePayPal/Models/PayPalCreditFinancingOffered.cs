﻿namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class PayPalCreditFinancingOffered
    {
        public PayPalCreditFinancingOfferedTotalCost totalCost { get; set; }
        public int term { get; set; }
        public PayPalCreditFinancingOfferedMonthlyPayment monthlyPayment { get; set; }
        public PayPalCreditFinancingOfferedTotalInterest totalInterest { get; set; }
        public bool payerAcceptance { get; set; }
        public bool cartAmountImmutable { get; set; }
    }
}