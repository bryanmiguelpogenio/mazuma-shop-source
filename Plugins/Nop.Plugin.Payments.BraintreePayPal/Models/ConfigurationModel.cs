﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;

namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.UseSandbox")]
        public bool UseSandBox { get; set; }
        public bool UseSandbox_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.MerchantId")]
        public string MerchantId { get; set; }
        public bool MerchantId_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.MerchantAccountId")]
        public string MerchantAccountId { get; set; }
        public bool MerchantAccountId_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.PublicKey")]
        public string PublicKey { get; set; }
        public bool PublicKey_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.PrivateKey")]
        public string PrivateKey { get; set; }
        public bool PrivateKey_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.AdditionalFee")]
        public decimal AdditionalFee { get; set; }
        public bool AdditionalFee_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.AdditionalFeePercentage")]
        public bool AdditionalFeePercentage { get; set; }
        public bool AdditionalFeePercentage_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.PayPalButtonSize")]
        public string PayPalButtonSize { get; set; }
        public bool PayPalButtonSize_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.PayPalFlow")]
        public string PayPalFlow { get; set; }
        public bool PayPalFlow_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.PayPalIntent")]
        public string PayPalIntent { get; set; }
        public bool PayPalIntent_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.EnablePayPalCreditButton")]
        public bool EnablePayPalCreditButton { get; set; }
        public bool EnablePayPalCreditButton_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditButtonSize")]
        public string PayPalCreditButtonSize { get; set; }
        public bool PayPalCreditButtonSize_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditFlow")]
        public string PayPalCreditFlow { get; set; }
        public bool PayPalCreditFlow_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditIntent")]
        public string PayPalCreditIntent { get; set; }
        public bool PayPalCreditIntent_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.EnablePayPalKount")]
        public bool EnablePayPalKount { get; set; }
        public bool EnablePayPalKount_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.AllowedShippingTwoLetterCountryCodes")]
        public string AllowedShippingTwoLetterCountryCodes { get; set; }
        public bool AllowedShippingTwoLetterCountryCodes_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPalCredit.Fields.PayPalRestApiClientId")]
        public string PayPalRestApiClientId { get; set; }
        public bool PayPalRestApiClientId_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPalCredit.Fields.PayPalRestApiSecret")]
        public string PayPalRestApiSecret { get; set; }
        public bool PayPalRestApiSecret_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.CacheAccessToken")]
        public bool CacheAccessToken { get; set; }
        public bool CacheAccessToken_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.CacheFromPriceStatement")]
        public bool CacheFromPriceStatement { get; set; }
        public bool CacheFromPriceStatement_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.CacheFinancingCalculatorValues")]
        public bool CacheFinancingCalculatorValues { get; set; }
        public bool CacheFinancingCalculatorValues_OverrideForStore { get; set; }
    }
}