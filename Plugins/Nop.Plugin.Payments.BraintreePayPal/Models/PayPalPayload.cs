﻿namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class PayPalPayload
    {
        public string nonce { get; set; }
        public PayPalPayloadDetails details { get; set; }
        public PayPalCreditFinancingOffered creditFinancingOffered { get; set; }
    }
}