﻿namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class PayPalPayloadDetails
    {
        public string email { get; set; }
        public string payerId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string countryCode { get; set; }
        public string phone { get; set; }
        public PayPalAddress shippingAddress { get; set; }
        public PayPalAddress billingAddress { get; set; }
    }
}