﻿using Newtonsoft.Json;
using System;

namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    [Serializable]
    public class PayPalCreditFinancingOptionRequestParam
    {
        [JsonProperty(PropertyName = "financing_country_code")]
        public string FinancingCountryCode { get; set; }

        [JsonProperty(PropertyName = "transaction_amount")]
        public PayPalCreditFinancingOptionRequestParamTransactionAmount TransactionAmount { get; set; }
    }

    [Serializable]
    public class PayPalCreditFinancingOptionRequestParamTransactionAmount
    {
        [JsonProperty(PropertyName = "value")]
        public decimal Value { get; set; }

        [JsonProperty(PropertyName = "currency_code")]
        public string CurrencyCode { get; set; }
    }
}