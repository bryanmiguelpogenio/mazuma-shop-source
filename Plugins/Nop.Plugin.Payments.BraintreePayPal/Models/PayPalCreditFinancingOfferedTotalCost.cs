﻿namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class PayPalCreditFinancingOfferedTotalCost
    {
        public string value { get; set; }
        public string currency { get; set; }
    }
}