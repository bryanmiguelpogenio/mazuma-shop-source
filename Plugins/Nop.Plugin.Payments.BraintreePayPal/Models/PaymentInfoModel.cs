﻿using Nop.Web.Framework.Mvc.Models;

namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class PaymentInfoModel : BaseNopModel
    {
        public bool UseSandBox { get; set; }
        public string AuthorizationCode { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public string Locale { get; set; }

        public string PayPalButtonSize { get; set; }
        public string PayPalFlow { get; set; }
        public string PayPalIntent { get; set; }

        public bool EnablePayPalCreditButton { get; set; }
        public string PayPalCreditButtonSize { get; set; }
        public string PayPalCreditFlow { get; set; }
        public string PayPalCreditIntent { get; set; }

        public bool EnablePayPalKount { get; set; }

        public string PayPalCreditMonthlyPayment { get; set; }
        public int PayPalCreditNumberOfMonths { get; set; }
        public string PayPalCreditInstallmentRate { get; set; }
        public string PayPalCreditCostOfPurchase { get; set; }
        public string PayPalCreditTotalIncludingInterest { get; set; }
    }
}