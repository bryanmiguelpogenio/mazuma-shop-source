﻿namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class PayPalCreditFinancingOfferedTotalInterest
    {
        public string value { get; set; }
        public string currency { get; set; }
    }
}