﻿namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class PayPalAddress
    {
        public string recipientName { get; set; }
        public string line1 { get; set; }
        public string line2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string countryCode { get; set; }
    }
}