﻿using Nop.Web.Framework.Mvc.Models;

namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class FinancingCalculatorModel : BaseNopModel
    {
        public string Token { get; set; }
        public decimal InitialValue { get; set; }
    }
}