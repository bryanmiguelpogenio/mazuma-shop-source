﻿using Nop.Web.Framework.Mvc.Models;

namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class FromPriceModel : BaseNopModel
    {
        public string FormattedFromPrice { get; set; }
    }
}