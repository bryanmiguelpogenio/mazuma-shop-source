﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;

namespace Nop.Plugin.Payments.BrainTreePayPal.Models
{
    public class AdditionalPaymentInfoModel : BaseNopModel
    {
        /// <summary>
        /// Gets or set the value indicating whether the order is paid through the BrainTree PayPal plugin
        /// </summary>
        public bool IsPayPal { get; set; }

        /// <summary>
        /// Gets or set the value indicating whether the order is subject to financing with PayPal Credit
        /// </summary>
        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.IsPayPalCredit")]
        public bool IsPayPalCredit { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditTerm")]
        public string PayPalCreditTerm { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditMonthlyPayment")]
        public string PayPalCreditMonthlyPayment { get; set; }
        
        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditInterest")]
        public string PayPalCreditInterest { get; set; }

        [NopResourceDisplayName("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditTotal")]
        public string PayPalCreditTotal { get; set; }
    }
}