﻿namespace Nop.Plugin.Payments.BrainTreePayPal
{
    public static class BrainTreePayPalPaymentSystemNames
    {
        public const string PAYMENT_METHOD_SYSTEM_NAME = "Payments.BrainTreePayPal";
        public const string PAYMENT_METHOD_NONCE = "payment_method_nonce";
        public const string DEVICE_DATA = "device_data";
        public const string SELECTED_FINANCING_OPTION = "selected_financing_option";
    }
}