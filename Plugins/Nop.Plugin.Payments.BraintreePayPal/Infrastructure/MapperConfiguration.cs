﻿using AutoMapper;
using Braintree;
using Nop.Core.Infrastructure.Mapper;
using Nop.Plugin.Payments.BrainTreePayPal.Domain;

namespace Nop.Plugin.Payments.BrainTreePayPal.Infrastructure
{
    public class MapperConfiguration : Profile, IMapperProfile
    {
        public MapperConfiguration()
        {
            CreateMap<PayPalDetails, BrainTreePayPalPaymentPayPalDetails>()
                .ForMember(dest => dest.TransactionType, mo => mo.Ignore())
                .ForMember(dest => dest.TransactionTypeId, mo => mo.Ignore())
                .ForMember(dest => dest.CustomerId, mo => mo.Ignore())
                .ForMember(dest => dest.BrainTreeTransactionId, mo => mo.Ignore())
                .ForMember(dest => dest.CreatedOnUtc, mo => mo.Ignore())
                .ForMember(dest => dest.LogLevel, mo => mo.Ignore())
                .ForMember(dest => dest.LogLevelId, mo => mo.Ignore());
        }

        public int Order => 0;
    }
}