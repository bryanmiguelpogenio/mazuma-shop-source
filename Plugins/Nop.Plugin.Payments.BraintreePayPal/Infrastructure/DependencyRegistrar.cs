﻿using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Payments.BrainTreePayPal.Data;
using Nop.Plugin.Payments.BrainTreePayPal.Domain;
using Nop.Plugin.Payments.BrainTreePayPal.Services;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Payments.BrainTreePayPal.Infrastructure
{
    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            //data context
            this.RegisterPluginDataContext<BrainTreePayPalPaymentObjectContext>(builder, "nop_object_context_braintree_paypal");

            //override required repository with our custom context
            builder.RegisterType<EfRepository<BrainTreePayPalPaymentPayPalDetails>>()
                .As<IRepository<BrainTreePayPalPaymentPayPalDetails>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_braintree_paypal"))
                .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<OrderPayPalCreditFinancingDetails>>()
                .As<IRepository<OrderPayPalCreditFinancingDetails>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_braintree_paypal"))
                .InstancePerLifetimeScope();

            //services
            builder.RegisterType<BrainTreePayPalService>()
                .As<IBrainTreePayPalService>()
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}