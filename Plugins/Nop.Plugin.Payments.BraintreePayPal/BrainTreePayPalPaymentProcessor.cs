﻿using System;
using System.Collections.Generic;
using Braintree;
using Microsoft.AspNetCore.Http;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.BrainTreePayPal.Controllers;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Environment = Braintree.Environment;
using Nop.Services.Localization;
using Nop.Plugin.Payments.BrainTree;
using Nop.Core.Http.Extensions;
using Nop.Core.Data;
using Nop.Plugin.Payments.BrainTreePayPal.Domain;
using Nop.Plugin.Payments.BrainTreePayPal.Extensions;
using Nop.Plugin.Payments.BrainTreePayPal.Data;
using Nop.Services.Cms;

namespace Nop.Plugin.Payments.BrainTreePayPal
{
    public class BrainTreePayPalPaymentProcessor : BasePlugin, IPaymentMethod, IWidgetPlugin
    {
        #region Constants

        /// <summary>
        /// nopCommerce partner code
        /// </summary>
        private const string BN_CODE = "nopCommerce_SP";

        #endregion

        #region Fields

        private readonly BrainTreePayPalPaymentObjectContext _objectContext;
        private readonly IRepository<BrainTreePayPalPaymentPayPalDetails> _payPalDetailsRepository;
        private readonly ICustomerService _customerService;
        private readonly ISession _session;
        private readonly ISettingService _settingService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly BrainTreePaymentSettings _brainTreePaymentSettings;
        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;
        private readonly ILogger _logger;

        #endregion

        #region Ctor

        public BrainTreePayPalPaymentProcessor(BrainTreePayPalPaymentObjectContext objectContext,
            IRepository<BrainTreePayPalPaymentPayPalDetails> payPalDetailsRepository,
            ICustomerService customerService,
            IHttpContextAccessor httpContextAccessor,
            ISettingService settingService,
            IOrderTotalCalculationService orderTotalCalculationService,
            BrainTreePaymentSettings brainTreePaymentSettings,
            ILocalizationService localizationService,
            IWebHelper webHelper,
            ILogger logger)
        {
            this._objectContext = objectContext;
            this._payPalDetailsRepository = payPalDetailsRepository;
            this._customerService = customerService;
            this._session = httpContextAccessor.HttpContext.Session;
            this._settingService = settingService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._brainTreePaymentSettings = brainTreePaymentSettings;
            this._localizationService = localizationService;
            this._webHelper = webHelper;
            this._logger = logger;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var processPaymentResult = new ProcessPaymentResult();

            var paymentMethodNonce = _session.Get<string>(BrainTreePayPalPaymentSystemNames.PAYMENT_METHOD_NONCE);

            if (string.IsNullOrWhiteSpace(paymentMethodNonce))
            {
                processPaymentResult.AddError("Error processing payment. Please re-enter your payment details.");
                return processPaymentResult;
            }

            //get customer
            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);

            //get settings
            var useSandBox = _brainTreePaymentSettings.UseSandBox;
            var merchantId = _brainTreePaymentSettings.MerchantId;
            var merchantAccountId = _brainTreePaymentSettings.MerchantAccountId;
            var publicKey = _brainTreePaymentSettings.PublicKey;
            var privateKey = _brainTreePaymentSettings.PrivateKey;

            //new gateway
            var gateway = new BraintreeGateway
            {
                Environment = useSandBox ? Environment.SANDBOX : Environment.PRODUCTION,
                MerchantId = merchantId,
                PublicKey = publicKey,
                PrivateKey = privateKey
            };

            //new transaction request
            var transactionRequest = new TransactionRequest
            {
                Amount = processPaymentRequest.OrderTotal,
                Channel = BN_CODE,
                OrderId = processPaymentRequest.OrderGuid.ToString(),
                PaymentMethodNonce = paymentMethodNonce,
                DeviceData = _session.Get<string>(BrainTreePayPalPaymentSystemNames.DEVICE_DATA)
            };

            if (!string.IsNullOrWhiteSpace(merchantAccountId))
                transactionRequest.MerchantAccountId = merchantAccountId;

            // billing address request
            var billingAddressRequest = new AddressRequest
            {
                CountryCodeAlpha2 = customer.BillingAddress.Country?.TwoLetterIsoCode,
                CountryName = customer.BillingAddress.Country?.Name,
                Locality = customer.BillingAddress.City,
                Region = customer.BillingAddress.StateProvince?.Name,
                FirstName = customer.BillingAddress.FirstName,
                LastName = customer.BillingAddress.LastName,
                StreetAddress = customer.BillingAddress.Address1,
                PostalCode = customer.BillingAddress.ZipPostalCode
            };
            transactionRequest.BillingAddress = billingAddressRequest;

            // shipping address request
            var shippingAddressRequest = new AddressRequest
            {
                CountryCodeAlpha2 = customer.BillingAddress.Country?.TwoLetterIsoCode,
                CountryName = customer.BillingAddress.Country?.Name,
                Locality = customer.BillingAddress.City,
                Region = customer.BillingAddress.StateProvince?.Name,
                FirstName = customer.BillingAddress.FirstName,
                LastName = customer.BillingAddress.LastName,
                StreetAddress = customer.BillingAddress.Address1,
                PostalCode = customer.BillingAddress.ZipPostalCode
            };
            transactionRequest.BillingAddress = shippingAddressRequest;

            //transaction options request
            var transactionOptionsRequest = new TransactionOptionsRequest
            {
                SubmitForSettlement = true
            };
            transactionRequest.Options = transactionOptionsRequest;

            //sending a request
            var result = gateway.Transaction.Sale(transactionRequest);

            //result
            if (result.IsSuccess())
            {
                processPaymentResult.NewPaymentStatus = PaymentStatus.Paid;
                processPaymentResult.AuthorizationTransactionId = result.Target.Id;

                // clear nonce and device data
                _session.Remove(BrainTreePayPalPaymentSystemNames.PAYMENT_METHOD_NONCE);
                _session.Remove(BrainTreePayPalPaymentSystemNames.DEVICE_DATA);
                
                // record paypal details
                if (result.Target != null && result.Target.PayPalDetails != null)
                {
                    var payPalDetails = result.Target.PayPalDetails.ToEntity();
                    payPalDetails.TransactionType = BrainTreePayPalPaymentTransactionType.Payment;
                    payPalDetails.CustomerId = customer.Id;
                    payPalDetails.BrainTreeTransactionId = result.Target.Id;
                    payPalDetails.LogLevel = Core.Domain.Logging.LogLevel.Information;
                    payPalDetails.CreatedOnUtc = DateTime.UtcNow;

                    _payPalDetailsRepository.Insert(payPalDetails);
                }
            }
            else
            {
                processPaymentResult.AddError("Failed to process payment. Please try again.");

                // log error
                _logger.Error($"Failed to process BrainTree PayPal payment. Transaction ID: {result.Target?.Id}. Debug ID: {result.Target?.PayPalDetails?.DebugId}. Message: {result.Message}");

                // record paypal details
                if (result.Target != null && result.Target.PayPalDetails != null)
                {
                    var payPalDetails = result.Target.PayPalDetails.ToEntity();
                    payPalDetails.TransactionType = BrainTreePayPalPaymentTransactionType.Payment;
                    payPalDetails.CustomerId = customer.Id;
                    payPalDetails.BrainTreeTransactionId = result.Target.Id;
                    payPalDetails.LogLevel = Core.Domain.Logging.LogLevel.Error;
                    payPalDetails.CreatedOnUtc = DateTime.UtcNow;

                    _payPalDetailsRepository.Insert(payPalDetails);
                }
            }

            return processPaymentResult;
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            //nothing
        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            //you can put any logic here
            //for example, hide this payment method if all products in the cart are downloadable
            //or hide this payment method if current customer is from certain country
            return false;
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            var result = this.CalculateAdditionalFee(_orderTotalCalculationService, cart,
                _brainTreePaymentSettings.AdditionalFee, _brainTreePaymentSettings.AdditionalFeePercentage);
            return result;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            result.AddError("Capture method not supported");
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();

            try
            {
                string transactionId = refundPaymentRequest.Order.AuthorizationTransactionId;

                if (string.IsNullOrWhiteSpace(transactionId))
                {
                    result.AddError("No transaction ID recorded for this order.");
                    return result;
                }

                //get settings
                var useSandBox = _brainTreePaymentSettings.UseSandBox;
                var merchantId = _brainTreePaymentSettings.MerchantId;
                var publicKey = _brainTreePaymentSettings.PublicKey;
                var privateKey = _brainTreePaymentSettings.PrivateKey;

                //new gateway
                var gateway = new BraintreeGateway
                {
                    Environment = useSandBox ? Environment.SANDBOX : Environment.PRODUCTION,
                    MerchantId = merchantId,
                    PublicKey = publicKey,
                    PrivateKey = privateKey
                };

                Result<Transaction> transactionResult = 
                    refundPaymentRequest.IsPartialRefund
                        ? gateway.Transaction.Refund(transactionId, refundPaymentRequest.AmountToRefund)
                        : gateway.Transaction.Refund(transactionId);

                if (transactionResult.IsSuccess())
                {
                    result.NewPaymentStatus = refundPaymentRequest.IsPartialRefund ? PaymentStatus.PartiallyRefunded : PaymentStatus.Refunded;

                    // record paypal details
                    if (transactionResult.Target != null && transactionResult.Target.PayPalDetails != null)
                    {
                        var payPalDetails = transactionResult.Target.PayPalDetails.ToEntity();
                        payPalDetails.TransactionType = refundPaymentRequest.IsPartialRefund ? BrainTreePayPalPaymentTransactionType.PartialRefund : BrainTreePayPalPaymentTransactionType.Refund;
                        payPalDetails.BrainTreeTransactionId = transactionResult.Target.Id;
                        payPalDetails.LogLevel = Core.Domain.Logging.LogLevel.Information;
                        payPalDetails.CreatedOnUtc = DateTime.UtcNow;

                        _payPalDetailsRepository.Insert(payPalDetails);
                    }
                }
                else
                {
                    result.AddError(transactionResult.Message);

                    // log error
                    _logger.Error($"Failed to process BrainTree PayPal payment. Transaction ID: {transactionId}. Debug ID: {transactionResult.Target?.PayPalDetails?.DebugId}. Message: {transactionResult.Message}");

                    // record paypal details
                    if (transactionResult.Target != null && transactionResult.Target.PayPalDetails != null)
                    {
                        var payPalDetails = transactionResult.Target.PayPalDetails.ToEntity();
                        payPalDetails.TransactionType = refundPaymentRequest.IsPartialRefund ? BrainTreePayPalPaymentTransactionType.PartialRefund : BrainTreePayPalPaymentTransactionType.Refund;
                        payPalDetails.BrainTreeTransactionId = transactionResult.Target.Id;
                        payPalDetails.LogLevel = Core.Domain.Logging.LogLevel.Error;
                        payPalDetails.CreatedOnUtc = DateTime.UtcNow;

                        _payPalDetailsRepository.Insert(payPalDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("BrainTree PayPal refund error", ex);

                result.AddError(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();

            try
            {
                string transactionId = voidPaymentRequest.Order.AuthorizationTransactionId;

                if (string.IsNullOrWhiteSpace(transactionId))
                {
                    result.AddError("No transaction ID recorded for this order.");
                    return result;
                }

                //get settings
                var useSandBox = _brainTreePaymentSettings.UseSandBox;
                var merchantId = _brainTreePaymentSettings.MerchantId;
                var publicKey = _brainTreePaymentSettings.PublicKey;
                var privateKey = _brainTreePaymentSettings.PrivateKey;

                //new gateway
                var gateway = new BraintreeGateway
                {
                    Environment = useSandBox ? Environment.SANDBOX : Environment.PRODUCTION,
                    MerchantId = merchantId,
                    PublicKey = publicKey,
                    PrivateKey = privateKey
                };

                Result<Transaction> transactionResult = gateway.Transaction.Void(transactionId);

                if (transactionResult.IsSuccess())
                {
                    result.NewPaymentStatus = PaymentStatus.Voided;

                    // record paypal details
                    if (transactionResult.Target != null && transactionResult.Target.PayPalDetails != null)
                    {
                        var payPalDetails = transactionResult.Target.PayPalDetails.ToEntity();
                        payPalDetails.TransactionType = BrainTreePayPalPaymentTransactionType.Void;
                        payPalDetails.BrainTreeTransactionId = transactionResult.Target.Id;
                        payPalDetails.LogLevel = Core.Domain.Logging.LogLevel.Information;
                        payPalDetails.CreatedOnUtc = DateTime.UtcNow;

                        _payPalDetailsRepository.Insert(payPalDetails);
                    }
                }
                else
                {
                    result.AddError(transactionResult.Message);

                    // log error
                    _logger.Error($"Failed to process BrainTree PayPal payment. Transaction ID: {transactionId}. Debug ID: {transactionResult.Target?.PayPalDetails?.DebugId}. Message: {transactionResult.Message}");

                    // record paypal details
                    if (transactionResult.Target != null && transactionResult.Target.PayPalDetails != null)
                    {
                        var payPalDetails = transactionResult.Target.PayPalDetails.ToEntity();
                        payPalDetails.TransactionType = BrainTreePayPalPaymentTransactionType.Void;
                        payPalDetails.BrainTreeTransactionId = transactionResult.Target.Id;
                        payPalDetails.LogLevel = Core.Domain.Logging.LogLevel.Error;
                        payPalDetails.CreatedOnUtc = DateTime.UtcNow;

                        _payPalDetailsRepository.Insert(payPalDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Payment error", ex);

                result.AddError(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            var result = new CancelRecurringPaymentResult();
            result.AddError("Recurring payment not supported");
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            //it's not a redirection payment method. So we always return false
            return false;
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/PaymentBrainTreePayPal/Configure";
        }
        
        public IList<string> ValidatePaymentForm(IFormCollection form)
        {
            var warnings = new List<string>();

            return warnings;
        }

        public ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            var paymentInfo = new ProcessPaymentRequest();

            return paymentInfo;
        }

        public void GetPublicViewComponent(out string viewComponentName)
        {
            viewComponentName = "PaymentBrainTreePayPal";
        }

        public IList<string> GetWidgetZones()
        {
            return new List<string>
            {
                "braintree_paypal_credit_financing_calculator",
                "braintree_paypal_credit_fromprice",
                "admin_order_details_info_bottom"
            };
        }

        public void GetPublicViewComponent(string widgetZone, out string viewComponentName)
        {
            switch (widgetZone)
            {
                case "braintree_paypal_credit_financing_calculator":
                    viewComponentName = "BrainTreePayPalCreditFinancingCalculator";
                    break;
                case "braintree_paypal_credit_fromprice":
                    viewComponentName = "BrainTreePayPalCreditProductFromPrice";
                    break;
                case "admin_order_details_info_bottom":
                    viewComponentName = "BrainTreePayPalAdditionalPaymentInfo";
                    break;
                default:
                    viewComponentName = "";
                    break;
            }
        }

        public Type GetControllerType()
        {
            return typeof(PaymentBrainTreePayPalController);
        }

        public override void Install()
        {
            //settings
            var settings = new BrainTreePayPalPaymentSettings
            {
                EnablePayPalCreditButton = false,
                EnablePayPalKount = true,
                PayPalButtonSize = "responsive",
                PayPalCreditButtonSize = "responsive",
                PayPalCreditFlow = "checkout",
                PayPalCreditIntent = "sale",
                PayPalFlow = "checkout",
                PayPalIntent = "sale"
            };
            _settingService.SaveSetting(settings);

            _objectContext.Install();

            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.UseSandbox", "Use Sandbox");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.UseSandbox.Hint", "Check to enable Sandbox (testing environment).");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.MerchantId", "Merchant ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.MerchantId.Hint", "Enter Merchant ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.MerchantAccountId", "Merchant Account ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.MerchantAccountId.Hint", "Enter Merchant Account ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PublicKey", "Public Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PublicKey.Hint", "Enter Public key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PrivateKey", "Private Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PrivateKey.Hint", "Enter Private key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.AdditionalFee", "Additional fee");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.AdditionalFee.Hint", "Enter additional fee to charge your customers.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.AdditionalFeePercentage", "Additional fee. Use percentage");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.AdditionalFeePercentage.Hint", "Determines whether to apply a percentage additional fee to the order total. If not enabled, a fixed value is used.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.PaymentMethodDescription", "Pay by PayPal or PayPal Credit");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalButtonSize", "PayPal button size");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalButtonSize.Hint", "Enter PayPal button size");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalFlow", "PayPal flow");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalFlow.Hint", "Enter PayPal flow");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalIntent", "PayPal intent");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalIntent.Hint", "Enter PayPal intent");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.EnablePayPalCreditButton", "Enable PayPal Credit button");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.EnablePayPalCreditButton.Hint", "Check to enable PayPal Credit button");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditButtonSize", "PayPal Credit button size");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditButtonSize.Hint", "Enter PayPal Credit button size");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditFlow", "PayPal Credit flow");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditFlow.Hint", "Enter PayPal Credit flow");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditIntent", "PayPal Credit intent");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditIntent.Hint", "Enter PayPal Credit intent");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.EnablePayPalKount", "Enable PayPal Kount");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.EnablePayPalKount.Hint", "Check to enable PayPal Kount");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.CacheAccessToken", "Enable access token caching");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.CacheAccessToken.Hint", "Check to enable access token caching");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.CacheFromPriceStatement", "Enable \"From Price\" statement caching");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.CacheFromPriceStatement.Hint", "Check to enable \"From Price\" statement caching");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.CacheFinancingCalculatorValues", "Enable financing calculator values caching");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.CacheFinancingCalculatorValues.Hint", "Check to enable financing calculator values caching");

            base.Install();
        }

        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<BrainTreePayPalPaymentSettings>();

            _objectContext.Uninstall();

            //locales
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.UseSandbox");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.UseSandbox.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.MerchantId");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.MerchantId.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.MerchantAccountId");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.MerchantAccountId.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PublicKey");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PublicKey.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PrivateKey");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PrivateKey.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.AdditionalFee");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.AdditionalFee.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.AdditionalFeePercentage");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.AdditionalFeePercentage.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.PaymentMethodDescription");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalButtonSize");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalButtonSize.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalFlow");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalFlow.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalIntent");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalIntent.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.EnablePayPalCreditButton");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.EnablePayPalCreditButton.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditButtonSize");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditButtonSize.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditFlow");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditFlow.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditIntent");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.PayPalCreditIntent.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.EnablePayPalKount");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.EnablePayPalKount.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.CacheAccessToken");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.CacheAccessToken.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.CacheFromPriceStatement");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.CacheFromPriceStatement.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.CacheFinancingCalculatorValues");
            this.DeletePluginLocaleResource("Plugins.Payments.BrainTreePayPal.Fields.CacheFinancingCalculatorValues.Hint");

            base.Uninstall();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get
            {
                return RecurringPaymentType.NotSupported;
            }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType
        {
            get
            {
                return PaymentMethodType.Button;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a payment method description that will be displayed on checkout pages in the public store
        /// </summary>
        public string PaymentMethodDescription
        {
            get { return _localizationService.GetResource("Plugins.Payments.BrainTreePayPal.PaymentMethodDescription"); }
        }

        #endregion

    }
}
