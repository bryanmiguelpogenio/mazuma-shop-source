﻿using Nop.Data.Mapping;
using Nop.Plugin.Payments.BrainTreePayPal.Domain;

namespace Nop.Plugin.Payments.BrainTreePayPal.Data
{
    public class OrderPayPalCreditFinancingDetailsMap : NopEntityTypeConfiguration<OrderPayPalCreditFinancingDetails>
    {
        public OrderPayPalCreditFinancingDetailsMap()
        {
            this.ToTable("BrainTree_PayPalCredit_OrderFinancingDetails");
            this.HasKey(x => x.Id);

            this.Property(x => x.MonthlyPaymentCurrency).HasMaxLength(20);
            this.Property(x => x.MonthlyPaymentValue).HasMaxLength(20);
            this.Property(x => x.TotalCostCurrency).HasMaxLength(20);
            this.Property(x => x.TotalCostValue).HasMaxLength(20);
            this.Property(x => x.TotalInterestCurrency).HasMaxLength(20);
            this.Property(x => x.TotalInterestValue).HasMaxLength(20);
        }
    }
}