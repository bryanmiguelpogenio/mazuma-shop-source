﻿using Nop.Data.Mapping;
using Nop.Plugin.Payments.BrainTreePayPal.Domain;

namespace Nop.Plugin.Payments.BrainTreePayPal.Data
{
    public class BrainTreePayPalPaymentPayPalDetailsMap : NopEntityTypeConfiguration<BrainTreePayPalPaymentPayPalDetails>
    {
        public BrainTreePayPalPaymentPayPalDetailsMap()
        {
            this.ToTable("BrainTree_PayPalDetails");
            this.HasKey(x => x.Id);

            this.Property(x => x.TransactionTypeId).IsRequired();
            this.Ignore(x => x.TransactionType);
            this.Property(x => x.CustomerId).IsOptional();
            this.Property(x => x.BrainTreeTransactionId).IsOptional();
            this.Property(x => x.TransactionFeeAmount).IsOptional();
            this.Property(x => x.RefundId).IsOptional();
            this.Property(x => x.CaptureId).IsOptional();
            this.Property(x => x.SellerProtectionStatus).IsOptional();
            this.Property(x => x.PayerStatus).IsOptional();
            this.Property(x => x.PayerLastName).IsOptional();
            this.Property(x => x.PayerFirstName).IsOptional();
            this.Property(x => x.PayerId).IsOptional();
            this.Property(x => x.CustomField).IsOptional();
            this.Property(x => x.PayeeEmail).IsOptional();
            this.Property(x => x.DebugId).IsOptional();
            this.Property(x => x.ImageUrl).IsOptional();
            this.Property(x => x.Token).IsOptional();
            this.Property(x => x.AuthorizationId).IsOptional();
            this.Property(x => x.PaymentId).IsOptional();
            this.Property(x => x.PayerEmail).IsOptional();
            this.Property(x => x.TransactionFeeCurrencyIsoCode).IsOptional();
            this.Property(x => x.Description).IsOptional();
            this.Property(x => x.CreatedOnUtc).IsRequired();
            this.Property(x => x.LogLevelId).IsRequired();
            this.Ignore(x => x.LogLevel);
        }
    }
}