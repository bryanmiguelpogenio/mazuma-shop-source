﻿namespace Nop.Plugin.Payments.BrainTreePayPal.Domain
{
    public enum BrainTreePayPalPaymentTransactionType
    {
        Payment = 1,
        Refund = 2,
        PartialRefund = 3,
        Void = 4
    }
}