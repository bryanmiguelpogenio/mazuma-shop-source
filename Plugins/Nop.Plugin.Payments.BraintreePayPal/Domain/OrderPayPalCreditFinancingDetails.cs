﻿using Nop.Core;

namespace Nop.Plugin.Payments.BrainTreePayPal.Domain
{
    public class OrderPayPalCreditFinancingDetails : BaseEntity
    {
        public int OrderId { get; set; }
        public string TotalCostValue { get; set; }
        public string TotalCostCurrency { get; set; }
        public int Term { get; set; }
        public string MonthlyPaymentValue { get; set; }
        public string MonthlyPaymentCurrency { get; set; }
        public string TotalInterestValue { get; set; }
        public string TotalInterestCurrency { get; set; }
        public bool PayerAcceptance { get; set; }
        public bool CartAmountImmutable { get; set; }
    }
}