﻿using Nop.Core;
using Nop.Core.Domain.Logging;
using System;

namespace Nop.Plugin.Payments.BrainTreePayPal.Domain
{
    public class BrainTreePayPalPaymentPayPalDetails : BaseEntity
    {
        public int TransactionTypeId { get; set; }
        public virtual BrainTreePayPalPaymentTransactionType TransactionType
        {
            get { return (BrainTreePayPalPaymentTransactionType)TransactionTypeId; }
            set { TransactionTypeId = (int)value; }
        }

        public int? CustomerId { get; set; }
        public string BrainTreeTransactionId { get; set; }
        public string TransactionFeeAmount { get; set; }
        public string RefundId { get; set; }
        public string CaptureId { get; set; }
        public string SellerProtectionStatus { get; set; }
        public string PayerStatus { get; set; }
        public string PayerLastName { get; set; }
        public string PayerFirstName { get; set; }
        public string PayerId { get; set; }
        public string CustomField { get; set; }
        public string PayeeEmail { get; set; }
        public string DebugId { get; set; }
        public string ImageUrl { get; set; }
        public string Token { get; set; }
        public string AuthorizationId { get; set; }
        public string PaymentId { get; set; }
        public string PayerEmail { get; set; }
        public string TransactionFeeCurrencyIsoCode { get; set; }
        public string Description { get; set; }

        public DateTime CreatedOnUtc { get; set; }
        public int LogLevelId { get; set; }

        public virtual LogLevel LogLevel
        {
            get { return (LogLevel)LogLevelId; }
            set { LogLevelId = (int)value; }
        }
    }
}