﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.BrainTreePayPal
{
    public class BrainTreePayPalPaymentSettings : ISettings
    {
        // Standard PayPal
        public string PayPalButtonSize { get; set; }
        public string PayPalFlow { get; set; }
        public string PayPalIntent { get; set; }

        // PayPal Credit
        public bool EnablePayPalCreditButton { get; set; }
        public string PayPalCreditButtonSize { get; set; }
        public string PayPalCreditFlow { get; set; }
        public string PayPalCreditIntent { get; set; }

        public string PayPalRestApiClientId { get; set; }
        public string PayPalRestApiSecret { get; set; }

        // Others
        public bool EnablePayPalKount { get; set; }

        public string AllowedShippingTwoLetterCountryCodes { get; set; }

        public bool CacheAccessToken { get; set; }
        public bool CacheFromPriceStatement { get; set; }
        public bool CacheFinancingCalculatorValues { get; set; }
    }
}