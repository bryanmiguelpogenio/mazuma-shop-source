﻿using Nop.Data.Mapping;
using Nop.Plugin.Ecorenew.CustomerReminder.Domain;

namespace Nop.Plugin.Ecorenew.CustomerReminder.Data
{
    public class CustomerReminderMessageRecordMap : NopEntityTypeConfiguration<CustomerReminderMessageRecord>
    {
        public CustomerReminderMessageRecordMap()
        {
            this.ToTable("CustomerReminder_MessageRecord");
            this.HasKey(x => x.Id);

            this.Property(x => x.EmailAddress).HasMaxLength(255).IsRequired();

            this.Ignore(x => x.CustomerReminderMessageType);
        }
    }
}
