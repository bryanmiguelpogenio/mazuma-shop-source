﻿namespace Nop.Plugin.Ecorenew.CustomerReminder
{
    public enum ConditionMetUnitOfMeasurement
    {
        Minutes = 0,
        Hours = 1,
        Days = 2
    }
}
