﻿using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Tax;

namespace Nop.Plugin.Ecorenew.CustomerReminder.Services
{
    public class CustomMessageTokenProvider : ICustomMessageTokenProvider
    {
        #region Fields
        
        private readonly IEventPublisher _eventPublisher;
        private readonly IPictureService _pictureService;
        private readonly IStoreService _storeService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ILocalizationService _localizationService;

        #endregion

        #region Ctor

        public CustomMessageTokenProvider(IEventPublisher eventPublisher,
            IPictureService pictureService,
            IStoreService storeService,
            IProductAttributeParser productAttributeParser,
            ILocalizationService localizationService)
        {
            this._eventPublisher = eventPublisher;
            this._pictureService = pictureService;
            this._storeService = storeService;
            this._productAttributeParser = productAttributeParser;
            this._localizationService = localizationService;
        }

        #endregion

        #region Methods

        public void AddShoppingCartItemsTokens(IList<Token> tokens, Customer customer, int storeId)
        {
            if (!customer.HasShoppingCartItems)
            {
                tokens.Add(new Token("ShoppingCartItems", ""));
            }
            else
            {
                var store = _storeService.GetStoreById(storeId);
                string storeUrl = store.SslEnabled ? store.SecureUrl : store.Url;

                var shoppingCartItems = customer.ShoppingCartItems
                        .Where(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart
                        && x.StoreId == store.Id);

                var shoppingCartItemHtmlRow = string.Empty;
                foreach (var sci in shoppingCartItems)
                {

                    string productUrl = storeUrl.TrimEnd('/') + "/" + sci.Product.GetSeName();
                    var productPicture = _pictureService.GetPicturesByProductId(sci.ProductId, 1).FirstOrDefault();
                    string productPictureUrl = _pictureService.GetPictureUrl(productPicture, 75, storeLocation: storeUrl);

                    //attributes
                    //we cannot inject IProductAttributeFormatter into constructor because it'll cause circular references.
                    //that's why we resolve it here this way
                    var productAttributeFormatter = EngineContext.Current.Resolve<ICustomProductAttributeFormatter>();
                    var attributes = productAttributeFormatter.FormatAttributes(sci.Product,
                        sci.AttributesXml,
                        sci.Customer,
                        renderPrices: false,
                        htmlEncode: false,
                        separator: string.Empty);

                    var priceFormatter = EngineContext.Current.Resolve<IPriceFormatter>();
                    var taxService = EngineContext.Current.Resolve<ITaxService>();
                    var priceCalculationService = EngineContext.Current.Resolve<IPriceCalculationService>();

                    var price = priceFormatter.FormatPrice(taxService.GetProductPrice(sci.Product, priceCalculationService.GetSubTotal(sci), out decimal taxRate));
                    var quantity = string.Format(_localizationService.GetResource("Plugins.Ecorenew.CustomerReminder.AbandonedCartTemplate.Paragraph"), $"Qty : {sci.Quantity}");

                    shoppingCartItemHtmlRow += string.Format(_localizationService.GetResource("Plugins.Ecorenew.CustomerReminder.AbandonedCartTemplate.Row"),
                            productPictureUrl, sci.Product.Name, attributes, quantity, price, productUrl);
                }

                string shoppingCartItemToken = string.Format(_localizationService.GetResource("Plugins.Ecorenew.CustomerReminder.AbandonedCartTemplate.Table"), shoppingCartItems.Count(), shoppingCartItemHtmlRow);

                tokens.Add(new Token("ShoppingCartItems", shoppingCartItemToken, true));
            }

            //event notification
            _eventPublisher.EntityTokensAdded(customer, tokens);
        }

        #endregion
    }
}
