﻿using Nop.Core.Domain.Customers;

namespace Nop.Plugin.Ecorenew.CustomerReminder.Services
{
    public interface ICustomWorkflowMessageService
    {
        int SendFirstAbandonedCartMessage(int storeId, Customer customer, int languageId);
    }
}