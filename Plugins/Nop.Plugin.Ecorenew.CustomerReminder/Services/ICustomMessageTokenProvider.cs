﻿using Nop.Core.Domain.Customers;
using Nop.Services.Messages;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.CustomerReminder.Services
{
    public interface ICustomMessageTokenProvider
    {
        void AddShoppingCartItemsTokens(IList<Token> tokens, Customer customer, int storeId);
    }
}