﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Messages;
using Nop.Core.Plugins;
using Nop.Plugin.Ecorenew.CustomerReminder.Domain;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.CustomerReminder.Services
{
    public class CustomWorkflowMessageService : WorkflowMessageService, ICustomWorkflowMessageService
    {
        #region Fields

        private readonly IRepository<CustomerReminderMessageRecord> _customerReminderMessageRecordRepository;
        private readonly CustomerReminderSettings _customerReminderSettings;
        private readonly IPluginFinder _pluginFinder;
        private readonly ILogger _logger;
        private readonly IStoreContext _storeContext;
        private readonly ICustomMessageTokenProvider _customMessageTokenProvider;
        private readonly IEventPublisher _eventPublisher;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly IStoreService _storeService;

        #endregion

        #region Ctor
        //public CustomWorkflowMessageService(CustomerReminderSettings customerReminderSettings,
        //    IPluginFinder pluginFinder,
        //    ILogger logger,
        //    IStoreContext storeContext,
        //    IMessageTemplateService messageTemplateService,
        //    IStoreService storeService)
        //{
        //    this._customerReminderSettings = customerReminderSettings;
        //    this._pluginFinder = pluginFinder;
        //    this._logger = logger;
        //    this._storeContext = storeContext;
        //    this._messageTemplateService = messageTemplateService;
        //    this._storeService = storeService;
        //}

        public CustomWorkflowMessageService(IRepository<CustomerReminderMessageRecord> customerReminderMessageRecordRepository,
            CustomerReminderSettings customerReminderSettings,
            IPluginFinder pluginFinder,
            ILogger logger,
            IMessageTemplateService messageTemplateService, 
            IQueuedEmailService queuedEmailService, 
            ILanguageService languageService, 
            ITokenizer tokenizer, 
            IEmailAccountService emailAccountService, 
            IMessageTokenProvider messageTokenProvider, 
            IStoreService storeService, 
            IStoreContext storeContext, 
            CommonSettings commonSettings, 
            EmailAccountSettings emailAccountSettings,
            ICustomMessageTokenProvider customMessageTokenProvider,
            IEventPublisher eventPublisher) 
            : 
            base(messageTemplateService, 
                queuedEmailService, 
                languageService, 
                tokenizer, 
                emailAccountService, 
                messageTokenProvider, 
                storeService, 
                storeContext, 
                commonSettings, 
                emailAccountSettings, 
                eventPublisher)
        {
            this._customerReminderMessageRecordRepository = customerReminderMessageRecordRepository;
            this._customerReminderSettings = customerReminderSettings;
            this._pluginFinder = pluginFinder;
            this._logger = logger;
            this._storeContext = storeContext;
            this._customMessageTokenProvider = customMessageTokenProvider;
            this._eventPublisher = eventPublisher;
            this._messageTemplateService = messageTemplateService;
            this._messageTokenProvider = messageTokenProvider;
            this._storeService = storeService;
        }

        #endregion

        #region Methods

        public int SendFirstAbandonedCartMessage(int storeId, Customer customer, int languageId)
        {
            // check if the plugin is installed and enabled for the current store
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(CustomerReminderConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
            {
                _logger.Error("Plugin Discount Coupon Generator is not yet installed.");
                return 0;
            }

            // check if the setting for customer registered event discount is enabled
            if (!_customerReminderSettings.AbandonedCartEventEnabled)
                return 0;

            var store = _storeService.GetStoreById(storeId);
            if (store == null)
                throw new Exception("Send abandoned cart message failed. Store with ID " + storeId + " not found");

            var messageTemplate = GetActiveMessageTemplate(CustomerReminderConstants.AbandonedCartMessage, store.Id);

            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            if (messageTemplate == null)
                return 0;

            //email account
            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

            // tokens
            var tokens = new List<Token>();

            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            _messageTokenProvider.AddCustomerTokens(tokens, customer);
            _customMessageTokenProvider.AddShoppingCartItemsTokens(tokens, customer, store.Id);

            //event notification
            _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

            int queuedEmailId = SendNotification(messageTemplate, emailAccount, languageId, tokens, customer.Email, customer.GetFullName());

            // add discount coupon message record
            var customerReminderMessageRecord = new CustomerReminderMessageRecord
            {
                StoreId = store.Id,
                EmailAddress = customer.Email,
                CustomerReminderMessageType = CustomerReminderMessageType.AbandonedCart,
                QueuedEmailId = queuedEmailId,
                CreatedOnUtc = DateTime.UtcNow
            };

            _customerReminderMessageRecordRepository.Insert(customerReminderMessageRecord);

            return queuedEmailId;
        }

        #endregion

    }
}
