﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Ecorenew.CustomerReminder
{
    public class CustomerReminderSettings : ISettings
    {

        /// <summary>
        /// Gets or sets the value indicating whether the customer registered event discount is enabled
        /// </summary>
        public bool AbandonedCartEventEnabled { get; set; }

        /// <summary>
        /// This setting specifies that the date should be later than the specified interval from now backwards. 
        /// Example: The product should have been added to the shopping cart not more than 60 days ago.
        /// </summary>
        public int AbandonedCartEventConditionMetLaterThan { get; set; }

        /// <summary>
        /// Unit of measurement for AbandonedCartEventConditionMetLaterThan
        /// </summary>
        public ConditionMetUnitOfMeasurement AbandonedCartEventConditionMetUomLaterThan { get; set; }

        /// <summary>
        /// This setting specifies that the date should be earlier than the specified interval from now backwards.
        /// Example: The product should have been added to the shopping cart at least 1 minute ago.
        /// </summary>
        public int AbandonedCartEventConditionMetEarlierThan { get; set; }

        /// <summary>
        /// Unit of measurement for AbandonedCartEventConditionMetEarlierThan
        /// </summary>
        public ConditionMetUnitOfMeasurement AbandonedCartEventConditionMetUomEarlierThan { get; set; }
    }
}
