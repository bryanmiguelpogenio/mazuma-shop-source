﻿using Nop.Core;
using System;

namespace Nop.Plugin.Ecorenew.CustomerReminder.Domain
{
    public class CustomerReminderMessageRecord : BaseEntity
    {
        public int StoreId { get; set; }

        public int CustomerReminderMessageTypeId { get; set; }

        public string EmailAddress { get; set; }

        public int QueuedEmailId { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public virtual CustomerReminderMessageType CustomerReminderMessageType
        {
            get { return (CustomerReminderMessageType)CustomerReminderMessageTypeId; }
            set { CustomerReminderMessageTypeId = (int)value; }
        }

    }
}
