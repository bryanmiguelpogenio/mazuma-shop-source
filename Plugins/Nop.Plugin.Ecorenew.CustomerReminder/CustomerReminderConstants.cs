﻿namespace Nop.Plugin.Ecorenew.CustomerReminder
{
    public class CustomerReminderConstants
    {
        public const string PluginSystemName = "Ecorenew.CustomerReminder";
        public const string AbandonedCartMessage = "CustomerReminder.AbandonedCart";

        public const string AbandonedCartEventScheduleTaskName = "Abandoned Cart Event (Customer Reminder)";
        public const string AbandonedCartEventScheduleTask = "Nop.Plugin.Ecorenew.CustomerReminder.Tasks.AbandonedCartMessageGeneratorTask";
    }
}
