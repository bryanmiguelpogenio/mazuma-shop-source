﻿using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Ecorenew.CustomerReminder.Data;
using Nop.Plugin.Ecorenew.CustomerReminder.Domain;
using Nop.Plugin.Ecorenew.CustomerReminder.Services;
using Nop.Services.Messages;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Ecorenew.CustomerReminder.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        #region Fields

        private const string DB_CONTEXT_NAME = "nop_object_context_eco_customer_reminder";

        #endregion


        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            // new services
            builder.RegisterType<CustomMessageTokenProvider>()
                .As<ICustomMessageTokenProvider>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CustomWorkflowMessageService>()
                .As<ICustomWorkflowMessageService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CustomProductAttributeFormatter>()
                .As<ICustomProductAttributeFormatter>()
                .InstancePerLifetimeScope();

            // overridden services
            builder.RegisterType<CustomWorkflowMessageService>()
                .As<IWorkflowMessageService>()
                .InstancePerLifetimeScope();

            //data context
            this.RegisterPluginDataContext<CustomerReminderObjectContext>(builder, DB_CONTEXT_NAME);

            //override required repository with our custom context
            builder.RegisterType<EfRepository<CustomerReminderMessageRecord>>()
                .As<IRepository<CustomerReminderMessageRecord>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(DB_CONTEXT_NAME))
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 9; }
        }
    }
}
