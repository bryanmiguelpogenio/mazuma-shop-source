﻿using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Ecorenew.CustomerReminder.Services;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Events;
using Nop.Services.Tasks;
using Nop.Services.Logging;
using Nop.Core.Plugins;
using Nop.Core;
using System.Linq;
using System;
using Nop.Plugin.Ecorenew.CustomerReminder.Domain;

namespace Nop.Plugin.Ecorenew.CustomerReminder.Tasks
{
    public class AbandonedCartMessageGeneratorTask : IScheduleTask
    {
        #region Fields

        private readonly IRepository<CustomerReminderMessageRecord> _customerReminderMessageRecordRepository;
        private readonly IRepository<ShoppingCartItem> _shoppingCartItemRepository;
        private readonly ICustomerService _customerService;
        private readonly ICustomWorkflowMessageService _customWorkflowMessageService;
        private readonly IDiscountService _discountService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILogger _logger;
        private readonly IPluginFinder _pluginFinder;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly CustomerReminderSettings _customerReminderSettings;

        #endregion

        #region Ctor

        public AbandonedCartMessageGeneratorTask(IRepository<CustomerReminderMessageRecord> customerReminderMessageRecordRepository,
            IRepository<ShoppingCartItem> shoppingCartItemRepository,
            ICustomerService customerService,
            ICustomWorkflowMessageService customWorkflowMessageService,
            IDiscountService discountService,
            IEventPublisher eventPublisher,
            ILogger logger,
            IPluginFinder pluginFinder,
            IStoreContext storeContext,
            IWorkContext workContext,
            CustomerReminderSettings customerReminderSettings)
        {
            this._customerReminderMessageRecordRepository = customerReminderMessageRecordRepository;
            this._shoppingCartItemRepository = shoppingCartItemRepository;
            this._customerService = customerService;
            this._customWorkflowMessageService = customWorkflowMessageService;
            this._discountService = discountService;
            this._eventPublisher = eventPublisher;
            this._logger = logger;
            this._pluginFinder = pluginFinder;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._customerReminderSettings = customerReminderSettings;
        }

        #endregion

        #region Methods

        public void Execute()
        {
            // check if the plugin is installed and enabled for the current store
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(CustomerReminderConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
            {
                _logger.Error("Plugin Discount Coupon Generator is not yet installed.");
                return;
            }

            if (!_customerReminderSettings.AbandonedCartEventEnabled)
                return;

            // get the list of registered customers who has placed an item at the shopping cart
            var registeredCustomerRole = _customerService.GetCustomerRoleBySystemName("Registered");

            var laterThanUom = _customerReminderSettings.AbandonedCartEventConditionMetUomLaterThan;
            var laterThanValue = _customerReminderSettings.AbandonedCartEventConditionMetLaterThan;
            var laterThanUtc = DateTime.UtcNow.Subtract(new TimeSpan(
                days: laterThanUom == ConditionMetUnitOfMeasurement.Days ? laterThanValue : 0,
                hours: laterThanUom == ConditionMetUnitOfMeasurement.Hours ? laterThanValue : 0,
                minutes: laterThanUom == ConditionMetUnitOfMeasurement.Minutes ? laterThanValue : 0,
                seconds: 0));

            var earlierThanUom = _customerReminderSettings.AbandonedCartEventConditionMetUomEarlierThan;
            var earlierThanValue = _customerReminderSettings.AbandonedCartEventConditionMetEarlierThan;
            var earlierThanUtc = DateTime.UtcNow.Subtract(new TimeSpan(
                days: earlierThanUom == ConditionMetUnitOfMeasurement.Days ? earlierThanValue : 0,
                hours: earlierThanUom == ConditionMetUnitOfMeasurement.Hours ? earlierThanValue : 0,
                minutes: earlierThanUom == ConditionMetUnitOfMeasurement.Minutes ? earlierThanValue : 0,
                seconds: 0));

            var query = _shoppingCartItemRepository.TableNoTracking
                .GroupBy(s => s.StoreId)
                .Select(gs => new
                {
                    StoreId = gs.Key,
                    Customers = gs.GroupBy(s => s.Customer)
                        .Where(g => g.Max(s => s.UpdatedOnUtc) > laterThanUtc
                            && g.Max(s => s.UpdatedOnUtc) < earlierThanUtc
                            && g.Key.Active
                            && !g.Key.Deleted
                            && g.Key.CustomerRoles.Where(x => x.Active).Select(x => x.Id).Contains(registeredCustomerRole.Id))
                })
                .ToList()
                .Select(x => new
                {
                    StoreId = x.StoreId,
                    Customers = x.Customers.ToList()
                        .Where(g => g.Max(s => s.UpdatedOnUtc) > (_customerReminderMessageRecordRepository
                                .TableNoTracking
                                .Where(d => d.EmailAddress == g.Key.Email)
                                .Max(d => (DateTime?)d.CreatedOnUtc) ?? new DateTime()))
                })
                .ToList();

            foreach (var item in query)
            {
                foreach (var customerShoppingCart in item.Customers)
                    _customWorkflowMessageService.SendFirstAbandonedCartMessage(item.StoreId, customerShoppingCart.Key, _workContext.WorkingLanguage.Id);
            }
        }

        #endregion
    }
}
