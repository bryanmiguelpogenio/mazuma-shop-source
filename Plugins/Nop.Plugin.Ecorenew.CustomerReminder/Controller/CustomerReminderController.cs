﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Ecorenew.CustomerReminder.Models;
using Nop.Services;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using System.Linq;

namespace Nop.Plugin.Ecorenew.CustomerReminder.Controller
{
    public class CustomerReminderController : BasePluginController
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public CustomerReminderController(ILocalizationService localizationService,
            IPermissionService permissionService,
            ISettingService settingService,
            IStoreService storeService,
            IWorkContext workContext)
        {
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._settingService = settingService;
            this._storeService = storeService;
            this._workContext = workContext;
        }

        #endregion

        #region Methods


        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDiscounts))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var customerReminderSettings = _settingService.LoadSetting<CustomerReminderSettings>(storeScope);


            var model = new ConfigurationModel
            {
                ActiveStoreScopeConfiguration = storeScope,

                AbandonedCartEventEnabled = customerReminderSettings.AbandonedCartEventEnabled,
                AbandonedCartEventConditionMetEarlierThan = customerReminderSettings.AbandonedCartEventConditionMetEarlierThan,
                AbandonedCartEventConditionMetUomEarlierThan = customerReminderSettings.AbandonedCartEventConditionMetUomEarlierThan,
                AbandonedCartEventConditionMetLaterThan = customerReminderSettings.AbandonedCartEventConditionMetLaterThan,
                AbandonedCartEventConditionMetUomLaterThan = customerReminderSettings.AbandonedCartEventConditionMetUomLaterThan,
            };

            if (storeScope > 0)
            {

                model.AbandonedCartEventEnabled_OverrideForStore = _settingService.SettingExists(customerReminderSettings, x => x.AbandonedCartEventEnabled, storeScope);
                model.AbandonedCartEventConditionMetEarlierThan_OverrideForStore = _settingService.SettingExists(customerReminderSettings, x => x.AbandonedCartEventConditionMetEarlierThan, storeScope);
                model.AbandonedCartEventConditionMetUomEarlierThan_OverrideForStore = _settingService.SettingExists(customerReminderSettings, x => x.AbandonedCartEventConditionMetUomEarlierThan, storeScope);
                model.AbandonedCartEventConditionMetLaterThan_OverrideForStore = _settingService.SettingExists(customerReminderSettings, x => x.AbandonedCartEventConditionMetLaterThan, storeScope);
                model.AbandonedCartEventConditionMetUomLaterThan_OverrideForStore = _settingService.SettingExists(customerReminderSettings, x => x.AbandonedCartEventConditionMetUomLaterThan, storeScope);
            }

            model.AbandonedCartEventConditionMetUomEarlierThanList = ConditionMetUnitOfMeasurement.Days.ToSelectList(false).ToList();
            foreach (var item in model.AbandonedCartEventConditionMetUomEarlierThanList.Where(x => x.Value == model.AbandonedCartEventConditionMetUomEarlierThan.ToString()))
                item.Selected = true;

            model.AbandonedCartEventConditionMetUomLaterThanList = ConditionMetUnitOfMeasurement.Days.ToSelectList(false).ToList();
            foreach (var item in model.AbandonedCartEventConditionMetUomLaterThanList.Where(x => x.Value == model.AbandonedCartEventConditionMetUomLaterThan.ToString()))
                item.Selected = true;

            return View("~/Plugins/Ecorenew.CustomerReminder/Views/Configure.cshtml", model);
        }

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var customerReminderSettings = _settingService.LoadSetting<CustomerReminderSettings>(storeScope);

            customerReminderSettings.AbandonedCartEventEnabled = model.AbandonedCartEventEnabled;

            customerReminderSettings.AbandonedCartEventConditionMetEarlierThan = model.AbandonedCartEventConditionMetEarlierThan;
            customerReminderSettings.AbandonedCartEventConditionMetUomEarlierThan = model.AbandonedCartEventConditionMetUomEarlierThan;
            customerReminderSettings.AbandonedCartEventConditionMetLaterThan = model.AbandonedCartEventConditionMetLaterThan;
            customerReminderSettings.AbandonedCartEventConditionMetUomLaterThan = model.AbandonedCartEventConditionMetUomLaterThan;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(customerReminderSettings, x => x.AbandonedCartEventEnabled, model.AbandonedCartEventEnabled_OverrideForStore, storeScope, false);

            _settingService.SaveSettingOverridablePerStore(customerReminderSettings, x => x.AbandonedCartEventConditionMetEarlierThan, model.AbandonedCartEventConditionMetEarlierThan_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(customerReminderSettings, x => x.AbandonedCartEventConditionMetUomEarlierThan, model.AbandonedCartEventConditionMetUomEarlierThan_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(customerReminderSettings, x => x.AbandonedCartEventConditionMetLaterThan, model.AbandonedCartEventConditionMetLaterThan_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(customerReminderSettings, x => x.AbandonedCartEventConditionMetUomLaterThan, model.AbandonedCartEventConditionMetUomLaterThan_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            model.AbandonedCartEventConditionMetUomEarlierThanList = ConditionMetUnitOfMeasurement.Days.ToSelectList(false).ToList();
            foreach (var item in model.AbandonedCartEventConditionMetUomEarlierThanList.Where(x => x.Value == model.AbandonedCartEventConditionMetUomEarlierThan.ToString()))
                item.Selected = true;

            model.AbandonedCartEventConditionMetUomLaterThanList = ConditionMetUnitOfMeasurement.Days.ToSelectList(false).ToList();
            foreach (var item in model.AbandonedCartEventConditionMetUomLaterThanList.Where(x => x.Value == model.AbandonedCartEventConditionMetUomLaterThan.ToString()))
                item.Selected = true;

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        #endregion

    }
}
