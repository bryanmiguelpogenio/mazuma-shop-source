﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.CustomerReminder.Models
{
    public class ConfigurationModel
    {
        public ConfigurationModel()
        {
            this.AbandonedCartEventConditionMetUomLaterThanList = new List<SelectListItem>();
            this.AbandonedCartEventConditionMetUomEarlierThanList = new List<SelectListItem>();
        }

        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.CustomerReminder.Fields.AbandonedCartEventEnabled")]
        public bool AbandonedCartEventEnabled { get; set; }
        public bool AbandonedCartEventEnabled_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.CustomerReminder.Fields.AbandonedCartEventConditionMetLaterThan")]
        public int AbandonedCartEventConditionMetLaterThan { get; set; }
        public bool AbandonedCartEventConditionMetLaterThan_OverrideForStore { get; set; }

        public ConditionMetUnitOfMeasurement AbandonedCartEventConditionMetUomLaterThan { get; set; }
        public bool AbandonedCartEventConditionMetUomLaterThan_OverrideForStore { get; set; }
        public IList<SelectListItem> AbandonedCartEventConditionMetUomLaterThanList { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.CustomerReminder.Fields.AbandonedCartEventConditionMetEarlierThan")]
        public int AbandonedCartEventConditionMetEarlierThan { get; set; }
        public bool AbandonedCartEventConditionMetEarlierThan_OverrideForStore { get; set; }

        public ConditionMetUnitOfMeasurement AbandonedCartEventConditionMetUomEarlierThan { get; set; }
        public bool AbandonedCartEventConditionMetUomEarlierThan_OverrideForStore { get; set; }
        public IList<SelectListItem> AbandonedCartEventConditionMetUomEarlierThanList { get; set; }
    }
}
