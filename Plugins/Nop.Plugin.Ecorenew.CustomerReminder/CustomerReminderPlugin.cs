﻿using Nop.Core.Plugins;
using Nop.Services.Common;
using Nop.Services.Messages;
using Nop.Web.Framework.Menu;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Services.Configuration;
using Nop.Core;
using Nop.Plugin.Ecorenew.CustomerReminder.Data;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework;
using System.Linq;
using Nop.Services.Localization;
using Nop.Core.Domain.Tasks;
using Nop.Core.Domain.Messages;

namespace Nop.Plugin.Ecorenew.CustomerReminder
{
    public class CustomerReminderPlugin : BasePlugin, IMiscPlugin, IAdminMenuPlugin
    {
        #region Fields


        private readonly IEmailAccountService _emailAccountService;
        private readonly ILogger _logger;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IScheduleTaskService _scheduleTaskService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly CustomerReminderObjectContext _objectContext;

        #endregion

        #region Ctor

        public CustomerReminderPlugin(IEmailAccountService emailAccountService,
            ILogger logger,
            IMessageTemplateService messageTemplateService,
            IScheduleTaskService scheduleTaskService,
            ISettingService settingService,
            IStoreContext storeContext,
            IWebHelper webHelper,
            IWorkContext workContext,
            CustomerReminderObjectContext objectContext)
        {
            this._emailAccountService = emailAccountService;
            this._logger = logger;
            this._messageTemplateService = messageTemplateService;
            this._scheduleTaskService = scheduleTaskService;
            this._settingService = settingService;
            this._storeContext = storeContext;
            this._webHelper = webHelper;
            this._workContext = workContext;
            this._objectContext = objectContext;
        }

        #endregion

        #region Methods

        public override void Install()
        {
            // settings
            var settings = new CustomerReminderSettings
            {
                AbandonedCartEventEnabled = false,
                AbandonedCartEventConditionMetEarlierThan = 1,
                AbandonedCartEventConditionMetUomEarlierThan = ConditionMetUnitOfMeasurement.Days,
                AbandonedCartEventConditionMetLaterThan = 2,
                AbandonedCartEventConditionMetUomLaterThan = ConditionMetUnitOfMeasurement.Days
            };
            _settingService.SaveSetting(settings);

            _objectContext.Install();

            // locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.AbandonedCart.Event.Title", "Abandoned Cart Event");

            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.Fields.AbandonedCartEventEnabled", "Enable Event");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.Fields.AbandonedCartEventConditionMetLaterThan", "Condition Met Later Than");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.Fields.AbandonedCartEventConditionMetEarlierThan", "Condition Met Earlier Than");

            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.AbandonedCartTemplate.Row", "Enter text here");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.AbandonedCartTemplate.Table", "Enter text here");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.AbandonedCartTemplate.Paragraph", "Enter text here");

            // install abandoned cart event schedule task
            if (_scheduleTaskService.GetTaskByType(CustomerReminderConstants.AbandonedCartEventScheduleTask) == null)
            {
                _scheduleTaskService.InsertTask(new ScheduleTask
                {
                    Enabled = true,
                    Seconds = 1800, // 30 mins?
                    Name = CustomerReminderConstants.AbandonedCartEventScheduleTaskName,
                    Type = CustomerReminderConstants.AbandonedCartEventScheduleTask,
                });
            }

            var emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            if (emailAccount != null)
            {
                _messageTemplateService.InsertMessageTemplate(new MessageTemplate
                {
                    EmailAccountId = emailAccount.Id,
                    IsActive = true,
                    Name = CustomerReminderConstants.AbandonedCartMessage,
                    Subject = CustomerReminderConstants.AbandonedCartMessage,
                    Body = "This is a test",
                    LimitedToStores = false
                });
            }

            base.Install();
        }


        public override void Uninstall()
        {
            // settings
            _settingService.DeleteSetting<CustomerReminderSettings>();

            _objectContext.Uninstall();

            // locales
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.AbandonedCart.Event.Title");

            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.Fields.AbandonedCartEventEnabled");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.Fields.AbandonedCartEventConditionMetLaterThan");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.Fields.AbandonedCartEventConditionMetEarlierThan");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.Fields.AbandonedCartEventDiscountCumulative");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.Fields.AbandonedCartEventDiscountShareable");

            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.AbandonedCartTemplate.Row");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.AbandonedCartTemplate.Table");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomerReminder.AbandonedCartTemplate.Paragraph");

            // remove scheduled task
            var task = _scheduleTaskService.GetTaskByType(CustomerReminderConstants.AbandonedCartEventScheduleTask);

            if (task != null)
                _scheduleTaskService.DeleteTask(task);
            base.Uninstall();
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/CustomerReminder/Configure";
        }

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var mainNode = new SiteMapNode()
            {
                SystemName = "Ecorenew",
                Title = "EcoRenew Group",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
                IconClass = "fa-leaf"
            };

            var customerReminderNode = new SiteMapNode()
            {
                SystemName = "Ecorenew.CustomerReminder",
                Title = "Customer Reminder",
                Visible = true,
                IconClass = "fa-genderless",
                RouteValues = new RouteValueDictionary() { { "area", AreaNames.Admin } },
                ControllerName = "CustomerReminder",
                ActionName = "Configure"
            };

            mainNode.ChildNodes.Add(customerReminderNode);

            var ecorenewNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Ecorenew");
            if (ecorenewNode != null)
                ecorenewNode.ChildNodes.Add(customerReminderNode);
            else
                rootNode.ChildNodes.Add(mainNode);
        }

        #endregion
    }
}
