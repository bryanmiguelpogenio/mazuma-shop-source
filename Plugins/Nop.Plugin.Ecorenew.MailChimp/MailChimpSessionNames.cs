﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Ecorenew.MailChimp
{
    public static class MailChimpSessionNames
    {
        public const string MAILCHIMP_EMAIL_ID = "mailchimp_email_id";
        public const string MAILCHIMP_CAMPAIGN_ID = "mailchimp_campaign_id";
    }
}
