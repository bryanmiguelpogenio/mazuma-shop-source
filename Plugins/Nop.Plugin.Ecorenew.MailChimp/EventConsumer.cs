﻿using Microsoft.AspNetCore.Http;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Ecorenew.MailChimp.Services;
using Nop.Services.Events;
using Nop.Core.Http.Extensions;
using System;
using System.Linq;
using MailChimp.Net.Core;
using Nop.Core.Domain.Directory;
using Nop.Services.Directory;
using System.Collections.Generic;
using Nop.Core.Domain.Messages;
using Nop.Services.Logging;
using Nop.Services.Catalog;
using Nop.Core.Domain.Catalog;
using mailchimp = MailChimp.Net.Models;

namespace Nop.Plugin.Ecorenew.MailChimp
{
    public class EventConsumer : IConsumer<OrderCancelledEvent>, IConsumer<OrderPaidEvent>,
        IConsumer<EmailSubscribedEvent>, IConsumer<EmailUnsubscribedEvent>
    {
        private readonly ISession _session;
        private readonly CurrencySettings _currencySettings;
        private readonly ICurrencyService _currencyService;
        private readonly IMailChimpService _mailChimpService;
        private readonly MailChimpSettings _mailChimpSettings;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly ILogger _logger;

        public EventConsumer(CurrencySettings currencySettings,
            ICurrencyService currencyService,
            IHttpContextAccessor httpContextAccessor,
            IMailChimpService mailChimpService,
            MailChimpSettings mailChimpSettings,
            IProductAttributeParser productAttributeParser,
            ILogger logger)
        {
            this._session = httpContextAccessor.HttpContext.Session;
            this._currencySettings = currencySettings;
            this._currencyService = currencyService;
            this._mailChimpService = mailChimpService;
            this._mailChimpSettings = mailChimpSettings;
            this._productAttributeParser = productAttributeParser;
            this._logger = logger;
        }

        public void HandleEvent(OrderCancelledEvent eventMessage)
        {
            try
            {
                if (_mailChimpSettings.EnableEcommerce)
                    ProcessOrderCancelled(eventMessage.Order);
            }
            catch (Exception ex)
            {
                _logger.Error($"MailChimp Cancel Order Event", ex);
            }
            
        }

        public void HandleEvent(OrderPaidEvent eventMessage)
        {
            try
            {
                if (_mailChimpSettings.EnableEcommerce)
                    ProcessOrderPaid(eventMessage.Order);
            }
            catch (Exception ex)
            {
                _logger.Error($"MailChimp Order Paid Event", ex);
            }
            
        }

        public void HandleEvent(EmailSubscribedEvent eventMessage)
        {
            try
            {
                ProcessSubscribedToNewsletter(eventMessage.Subscription);
            }
            catch (Exception ex)
            {
                _logger.Error($"MailChimp Email Subscribed Event", ex);
            }
            
        }

        public void HandleEvent(EmailUnsubscribedEvent eventMessage)
        {
            try
            {
                ProcessUnsubscribeToNewsletter(eventMessage.Subscription);
            }
            catch (Exception ex)
            {
                _logger.Error($"MailChimp Email Unsubscribed Event", ex);
            }
        }

        private void ProcessOrderPaid(Core.Domain.Orders.Order order)
        {
            var campaignId = _session.Get<string>(MailChimpSessionNames.MAILCHIMP_CAMPAIGN_ID);
            //var emailId = _session.Get<string>(MailChimpSessionNames.MAILCHIMP_EMAIL_ID);

            if (campaignId == null)
                return;

            var currencyCode = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId)?.CurrencyCode;

            foreach (var orderItem in order.OrderItems)
            {
                var productId = orderItem.ProductId.ToString();
                var title = orderItem.Product.Name;
                string sku = string.Empty;
                string productAttributeCombinationId = string.Empty;
                var isProductAttributeCombinationExisting = false;

                //var sku = orderItem.Product.ProductAttributeCombinations.FirstOrDefault(pac => pac.AttributesXml == orderItem.AttributesXml).Sku;
                //var productAttributeCombinationId = orderItem.Product.ProductAttributeCombinations.FirstOrDefault(pac => pac.AttributesXml == orderItem.AttributesXml).Id.ToString();

                var combination = _productAttributeParser.FindProductAttributeCombination(orderItem.Product, orderItem.AttributesXml);
                if (combination != null)
                {
                    sku = combination.Sku;
                    productAttributeCombinationId = combination.Id.ToString();
                    isProductAttributeCombinationExisting = true;
                }

                if (!isProductAttributeCombinationExisting)
                {
                    _logger.Warning($"MailChimp Order Paid Event: Product Attribute Combination doesnt exist.");
                    return;
                }

                var variants = new List<mailchimp.Variant>
                {
                    new mailchimp.Variant
                    {
                        Id = productAttributeCombinationId,
                        Title = title,
                        Sku = sku,
                        Visibility = "true"
                    }
                };

                if (!_mailChimpService.IsProductExistingAsync(productId))
                {
                    var products = new List<mailchimp.Product>
                    {
                        new mailchimp.Product
                        {
                            Id = orderItem.ProductId.ToString(),
                            Title = orderItem.Product.Name,
                            Variants = variants
                        }
                    };
                    _mailChimpService.AddProducts(products);

                    _logger.Information($"MailChimp Order Paid Event: Added product in MailChimp");
                }

                if (!_mailChimpService.IsProductVariantExisting(productId, productAttributeCombinationId))
                {
                    _mailChimpService.AddProductVariants(variants, productId);

                    _logger.Information($"MailChimp Order Paid Event: Added variant in MailChimp");
                }
            }

            var mailChimpOrder = new mailchimp.Order
            {
                Id = order.CustomOrderNumber,
                Customer = new mailchimp.Customer
                {
                    Id = order.BillingAddressId.ToString(),
                    EmailAddress = order.BillingAddress.Email,
                    OptInStatus = false
                },
                CampaignId = campaignId,
                CurrencyCode = (CurrencyCode)Enum.Parse(typeof(CurrencyCode), currencyCode, true),
                OrderTotal = order.OrderTotal,
                DiscountTotal = order.OrderDiscount,
                TaxTotal = order.OrderTax,
                ShippingTotal = order.OrderShippingExclTax,
                ProcessedAtForeign = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                UpdatedAtForeign = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                Lines = order.OrderItems.ToList().Select(oi => new mailchimp.Line
                {
                    Id = oi.Id.ToString(),
                    ProductId = oi.ProductId.ToString(),
                    ProductVariantId = _productAttributeParser.FindProductAttributeCombination(oi.Product, oi.AttributesXml).Id.ToString(),
                    Quantity = oi.Quantity,
                    Price = oi.UnitPriceExclTax,
                    Discount = oi.DiscountAmountExclTax
                }).ToList()
            };

 
            _mailChimpService.AddOrder(mailChimpOrder);

            _logger.Information($"MailChimp Order Paid Event: Added order in MailChimp");
        }

        private void ProcessOrderCancelled(Core.Domain.Orders.Order order)
        {
            if (_mailChimpService.IsOrderExisting(order.CustomOrderNumber))
            {
                var currencyCode = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId)?.CurrencyCode;

                var mailChimpOrder = new mailchimp.Order
                {
                    Id = order.CustomOrderNumber,
                    CancelledAtForeign = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                    CurrencyCode = (CurrencyCode)Enum.Parse(typeof(CurrencyCode), currencyCode, true),
                    OrderTotal = 0,
                    DiscountTotal = 0,
                    TaxTotal = 0,
                    ShippingTotal = 0,
                    Lines = order.OrderItems.ToList().Select(oi => new mailchimp.Line
                    {
                        Id = oi.Id.ToString(),
                        ProductId = oi.ProductId.ToString(),
                        ProductVariantId = _productAttributeParser.FindProductAttributeCombination(oi.Product, oi.AttributesXml).Id.ToString(),
                        Quantity = 0,
                        Price = 0,
                        Discount = 0
                    }).ToList()
                };
                _mailChimpService.CancelOrder(mailChimpOrder);

                _logger.Information($"MailChimp Order Cancelled Event: Order has been cancelled in MailChimp");
            }
        }

        private void ProcessSubscribedToNewsletter(NewsLetterSubscription subscription)
        {
            var email = subscription.Email;

            var member = _mailChimpService.GetMember(email);

            if (member == null)
            {
                member = new mailchimp.Member { EmailAddress = email };
                member = _mailChimpService.MapMember(member);
            }

            member.Status = mailchimp.Status.Subscribed;

            _mailChimpService.SubscribeOrUnsubscribe(member);

            _logger.Information($"MailChimp Email Subscribed Event: Subscriber added in MailChimp");
        }

        private void ProcessUnsubscribeToNewsletter(NewsLetterSubscription subscription)
        {
            var member = _mailChimpService.GetMember(subscription.Email);

            if (member != null)
            {
                member.Status = mailchimp.Status.Unsubscribed;
                _mailChimpService.SubscribeOrUnsubscribe(member);
                _logger.Information($"MailChimp Email Unsubscribed Event: Subscriber has been removed in MailChimp");
            }
        }
    }
}
