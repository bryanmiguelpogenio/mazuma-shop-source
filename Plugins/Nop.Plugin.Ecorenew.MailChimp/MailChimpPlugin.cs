﻿using Nop.Core;
using Nop.Core.Domain.Tasks;
using Nop.Core.Plugins;
using Nop.Services.Cms;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.MailChimp
{
    public class MailChimpPlugin : BasePlugin, IWidgetPlugin
    {
        private readonly MailChimpSettings _mailChimpSettings;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly IScheduleTaskService _scheduleTaskService;

        public MailChimpPlugin(MailChimpSettings mailChimpSettings,
            ISettingService settingService,
            IWebHelper webHelper,
            IScheduleTaskService scheduleTaskService)
        {
            this._mailChimpSettings = mailChimpSettings;
            this._settingService = settingService;
            this._webHelper = webHelper;
            this._scheduleTaskService = scheduleTaskService;
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/MailChimp/Configure";
        }

        public void GetPublicViewComponent(string widgetZone, out string viewComponentName)
        {
            viewComponentName = "EcorenewMailChimp";
        }

        public IList<string> GetWidgetZones()
        {
            return new List<string>() { "body_end_html_tag_before" };
        }

        public override void Install()
        {
            var settings = new MailChimpSettings();

            _settingService.SaveSetting(settings);

            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.MailChimp.ApiKey", "API Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.MailChimp.ListId", "List Id");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.MailChimp.StoreId", "Store Id");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.MailChimp.EnableEcommerce", "Enable Ecommerce");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.MailChimp.UpdateProducts", "Update Products");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.MailChimp.UpdateSubscriptions", "Update Subscriptions");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.MailChimp.Updating", "Batch operation has started. You can leave this page while batch operation is in progress.");

            base.Install();
        }

        public override void Uninstall()
        {
            _settingService.DeleteSetting<MailChimpSettings>();

            this.DeletePluginLocaleResource("Plugins.Ecorenew.MailChimp.ApiKey");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.MailChimp.ListId");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.MailChimp.StoreId");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.MailChimp.EnableEcommerce");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.MailChimp.Updating");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.MailChimp.UpdateProducts");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.MailChimp.UpdateSubscriptions");

            base.Uninstall();
        }

    }
}
