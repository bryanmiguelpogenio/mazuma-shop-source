﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Ecorenew.MailChimp.Models
{
    public class MailChimpCampaignModel
    {
        public string MailChimpCampaignId { get; set; }
        public string MailChimpEmailId { get; set; }
    }
}
