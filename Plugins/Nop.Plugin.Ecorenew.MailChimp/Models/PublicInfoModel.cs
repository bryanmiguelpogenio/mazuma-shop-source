﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Ecorenew.MailChimp.Models
{
    public class PublicInfoModel
    {
        public string QueryNameCampaignId { get; set; }
        public string QueryNameEmailId { get; set; }
    }
}
