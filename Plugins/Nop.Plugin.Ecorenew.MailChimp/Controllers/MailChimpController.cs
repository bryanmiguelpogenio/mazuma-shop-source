﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Ecorenew.MailChimp.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Core.Http.Extensions;
using System.Threading.Tasks;
using Nop.Plugin.Ecorenew.MailChimp.Services;
using mailchimp = MailChimp.Net.Models;

namespace Nop.Plugin.Ecorenew.MailChimp.Controllers
{
    public class MailChimpController : BasePluginController
    {
        private readonly ISession _session;
        private readonly IWorkContext _workContext;
        private readonly IStoreService _storeService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly IMailChimpService _mailChimpService;

        public MailChimpController(IHttpContextAccessor httpContextAccessor,
            IWorkContext workContext,
            IStoreService storeService,
            IPermissionService permissionService,
            ISettingService settingService,
            ILocalizationService localizationService,
            IMailChimpService mailChimpService)
        {
            this._session = httpContextAccessor.HttpContext.Session;
            this._workContext = workContext;
            this._storeService = storeService;
            this._permissionService = permissionService;
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._mailChimpService = mailChimpService;
        }

        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePlugins))
                return AccessDeniedView();

            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var settings = _settingService.LoadSetting<MailChimpSettings>(storeScope);

            var model = new ConfigurationModel
            {
                ApiKey = settings.ApiKey,
                StoreId = settings.StoreId,
                EnableEcommerce = settings.EnableEcommerce,
                ListId = settings.ListId
            };
            model.ActiveStoreScopeConfiguration = storeScope;

            if (storeScope > 0)
            {
                model.ApiKey_OverrideForStore = _settingService.SettingExists(settings, x => x.ApiKey, storeScope);
                model.StoreId_OverrideForStore = _settingService.SettingExists(settings, x => x.StoreId, storeScope);
                model.EnableEcommerce_OverrideForStore = _settingService.SettingExists(settings, x => x.EnableEcommerce, storeScope);
                model.ListId_OverrideForStore = _settingService.SettingExists(settings, x => x.ListId, storeScope);
            }

            return View("~/Plugins/Ecorenew.MailChimp/Views/Configure.cshtml", model);
        }

        [HttpPost, ActionName("Configure")]
        [FormValueRequired("save")]
        [Area(AreaNames.Admin)]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePlugins))
                return AccessDeniedView();

            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var settings = _settingService.LoadSetting<MailChimpSettings>(storeScope);

            if (string.IsNullOrEmpty(settings.ApiKey))
            {
                settings.ApiKey = model.ApiKey;
                _settingService.SaveSettingOverridablePerStore(settings, x => x.ApiKey, model.ApiKey_OverrideForStore, storeScope, false);
                _settingService.ClearCache();

                SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

                return Configure();
            }

            var addStoreResult = true;

            if (!_mailChimpService.IsStoreExisting(model.StoreId))
            {
                var store = new mailchimp.Store
                {
                    Id = model.StoreId,
                    ListId = model.ListId
                };

                addStoreResult = _mailChimpService.AddStore(store, storeScope);
            }

            if (!addStoreResult)
                model.StoreId = string.Empty;

            settings.ApiKey = model.ApiKey;
            settings.StoreId = model.StoreId;
            settings.EnableEcommerce = model.EnableEcommerce;
            settings.ListId = model.ListId;

            _settingService.SaveSettingOverridablePerStore(settings, x => x.ApiKey, model.ApiKey_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(settings, x => x.StoreId, model.StoreId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(settings, x => x.EnableEcommerce, model.EnableEcommerce_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(settings, x => x.ListId, model.ListId_OverrideForStore, storeScope, false);

            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        //[HttpPost]
        //public void SetMailChimpCampaign(MailChimpCampaignModel model)
        //{
        //    if (!string.IsNullOrEmpty(model.MailChimpCampaignId))
        //        _session.Set<string>(MailChimpSessionNames.MAILCHIMP_CAMPAIGN_ID, model.MailChimpCampaignId);

        //    if (!string.IsNullOrEmpty(model.MailChimpEmailId))
        //        _session.Set<string>(MailChimpSessionNames.MAILCHIMP_EMAIL_ID, model.MailChimpEmailId);
        //}

        [HttpPost, ActionName("Configure")]
        [FormValueRequired("updateProducts")]
        [Area(AreaNames.Admin)]
        public IActionResult UpdateProducts()
        {
            _mailChimpService.AddOrUpdateBatchProducts();

            SuccessNotification(_localizationService.GetResource("Plugins.Ecorenew.MailChimp.Updating"));

            return Configure();
        }

        [HttpPost, ActionName("Configure")]
        [FormValueRequired("updateSubscribers")]
        [Area(AreaNames.Admin)]
        public IActionResult UpdateSubscribers()
        {
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);

            _mailChimpService.AddOrUpdateBatchSubscribers(storeScope);

            SuccessNotification(_localizationService.GetResource("Plugins.Ecorenew.MailChimp.Updating"));

            return Configure();
        }

    }
}
