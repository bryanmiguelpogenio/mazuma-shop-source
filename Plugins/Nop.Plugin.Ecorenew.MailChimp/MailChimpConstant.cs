﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Ecorenew.MailChimp
{
    public static class MailChimpConstant
    {
        public const string HTTP_METHOD_POST = "POST";
        public const string HTTP_METHOD_PUT = "PUT";
        public const string HTTP_METHOD_PATCH = "PATCH";
        public const string MERGE_FIELD_FIRST_NAME = "FNAME";
        public const string MERGE_FIELD_LAST_NAME = "LNAME";
        public const int ITEM_LIMIT = 2000;
    }
}
