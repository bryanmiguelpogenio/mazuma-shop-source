﻿using MailChimp.Net.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nop.Plugin.Ecorenew.MailChimp.Services
{
    public interface IMailChimpService
    {
        bool IsOrderExisting(string customOrderNumber);

        bool IsProductExistingAsync(string productId);

        bool IsProductVariantExisting(string productId, string variantId);

        bool IsStoreExisting(string storeId);

        void AddOrder(Order order);

        void CancelOrder(Order order);

        void AddProducts(List<Product> products);

        void AddProductVariants(List<Variant> variants, string productId);

        bool AddStore(Store store, int storeId);

        void SubscribeOrUnsubscribe(Member member);

        Member MapMember(Member member);

        Member GetMember(string email);

        Task AddOrUpdateBatchProducts();

        Task AddOrUpdateBatchSubscribers(int storeId);
    }
}
