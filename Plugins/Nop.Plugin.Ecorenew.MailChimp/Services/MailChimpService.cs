﻿using MailChimp.Net;
using MailChimp.Net.Interfaces;
using MailChimp.Net.Models;
using MailChimp.Net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nop.Core.Domain.Directory;
using Nop.Services.Directory;
using Nop.Core.Data;
using Newtonsoft.Json;
using Nop.Services.Customers;
using Nop.Services.Common;
using Nop.Core.Domain.Customers;
using mailchimp = MailChimp.Net.Models;
using Nop.Services.Stores;
using Nop.Core;
using Nop.Services.Localization;
using Nop.Services.Helpers;

namespace Nop.Plugin.Ecorenew.MailChimp.Services
{
    public class MailChimpService : IMailChimpService
    {
        private readonly CurrencySettings _currencySettings;
        private readonly ICurrencyService _currencyService;
        private readonly MailChimpSettings _mailChimpSettings;
        private readonly IRepository<Core.Domain.Catalog.Product> _productRepository;
        private readonly IRepository<Core.Domain.Messages.NewsLetterSubscription> _subscriptionRepository;
        private readonly ICustomerService _customerService;
        private readonly IStoreService _storeService;
        private readonly IWebHelper _webHelper;
        private readonly ILanguageService _languageService;
        private readonly IDateTimeHelper _dateTimeHelper;

        public MailChimpService(CurrencySettings currencySettings,
            ICurrencyService currencyService,
            MailChimpSettings mailChimpSettings,
            IRepository<Core.Domain.Catalog.Product> productRepository,
            IRepository<Core.Domain.Messages.NewsLetterSubscription> subscriptionRepository,
            ICustomerService customerService,
            IStoreService storeService,
            IWebHelper webHelper,
            ILanguageService languageService,
            IDateTimeHelper dateTimeHelper)
        {
            this._currencySettings = currencySettings;
            this._currencyService = currencyService;
            this._mailChimpSettings = mailChimpSettings;
            this._productRepository = productRepository;
            this._subscriptionRepository = subscriptionRepository;
            this._customerService = customerService;
            this._storeService = storeService;
            this._webHelper = webHelper;
            this._languageService = languageService;
            this._dateTimeHelper = dateTimeHelper;
        }

        public bool IsOrderExisting(string customOrderNumber)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var order = new Order();

            try
            {
                order = Task.Run(async () => await manager.ECommerceStores
                .Orders(_mailChimpSettings.StoreId)
                .GetAsync(customOrderNumber)).Result;
            }
            catch (Exception)
            {
                return false;
            }

            return order != null;
        }

        public bool IsProductExistingAsync(string productId)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var product = new Product();

            try
            {
                product = Task.Run(async () => await manager.ECommerceStores.Products(_mailChimpSettings.StoreId).GetAsync(productId)).Result;
            }
            catch (Exception)
            {
                return false;
            }

            return product != null;
        }

        public bool IsProductVariantExisting(string productId, string variantId)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var variant = new Variant();

            try
            {
                variant = Task.Run(async () => await manager.ECommerceStores.Products(_mailChimpSettings.StoreId).Variances(productId).GetAsync(variantId)).Result;
            }
            catch (Exception)
            {
                return false;
            }

            return variant != null;
        }

        public bool IsStoreExisting(string storeId)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var variant = new Store();

            try
            {
                variant = Task.Run(async () => await manager.ECommerceStores.GetAsync(storeId)).Result;
            }
            catch (Exception)
            {
                return false;
            }

            return variant != null;
        }

        public void AddOrder(Order order)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var result = Task.Run(async () => await manager.ECommerceStores.Orders(_mailChimpSettings.StoreId).AddAsync(order)).Result;
        }

        public void CancelOrder(Order order)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var result = Task.Run(async () => await manager.ECommerceStores.Orders(_mailChimpSettings.StoreId).UpdateAsync(order.Id, order)).Result;
        }

        public void AddProducts(List<Product> products)
        {
            var result = Task.Run(async () => await AddProductsAsync(products)).Result;
        }

        public void AddProductVariants(List<Variant> variants, string productId)
        {
            var result = Task.Run(async () => await AddProductVariantsAsync(variants, productId)).Result;
        }

        public bool AddStore(Store store, int storeId)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var nopStore = _storeService.GetStoreById(storeId);

            if (nopStore == null)
                return false;

            var currencyCode = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId)?.CurrencyCode;

            store.Name = nopStore.Name;
            store.Domain = _webHelper.GetStoreLocation();
            store.CurrencyCode = (CurrencyCode)Enum.Parse(typeof(CurrencyCode), currencyCode, true);
            store.PrimaryLocale = (_languageService.GetLanguageById(nopStore.DefaultLanguageId) ?? _languageService.GetAllLanguages().FirstOrDefault())?.UniqueSeoCode;
            store.Phone = nopStore.CompanyPhoneNumber;
            store.Timezone = _dateTimeHelper.DefaultStoreTimeZone?.StandardName;

            var result = Task.Run(async () => await manager.ECommerceStores.AddAsync(store)).Result;

            return result != null;
        }

        public void SubscribeOrUnsubscribe(Member member)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var result = Task.Run(async () => await manager.Members.AddOrUpdateAsync(_mailChimpSettings.ListId, member)).Result;
        }

        public Member GetMember(string email)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);
            var member = new Member();

            try
            {
                member = Task.Run(async () => await manager.Members.GetAsync(_mailChimpSettings.ListId, manager.Members.Hash(email.ToLower()))).Result;
            }
            catch (Exception)
            {
                return null;
            }

            return member;
        }

        private async Task<List<Product>> AddProductsAsync(List<Product> products)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var mailChimpProducts = new List<Product>();

            foreach (var product in products)
            {
                var mailChimpProduct = await manager.ECommerceStores.Products(_mailChimpSettings.StoreId).AddAsync(product);
                mailChimpProducts.Add(mailChimpProduct);
            }

            return mailChimpProducts;
        }

        private async Task<List<Variant>> AddProductVariantsAsync(List<Variant> variants, string productId)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var mailChimpVariants = new List<Variant>();

            foreach (var variant in variants)
            {
                var mailChimpVariant = await manager
                    .ECommerceStores
                    .Products(_mailChimpSettings.StoreId)
                    .Variances(productId)
                    .AddAsync(variant);
                mailChimpVariants.Add(mailChimpVariant);
            }

            return mailChimpVariants;
        }

        public async Task AddOrUpdateBatchProducts()
        {
            var operations = new List<Operation>();
            operations.AddRange(GetAddProductsOperations());
            operations.AddRange(GetUpdateProductsOperations());

            await ExecuteBatchOperations(operations);
        }

        private IEnumerable<Operation> GetAddProductsOperations()
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var products = GetMapProducts();

            var operations = products.Select(x => new Operation
            {
                Method = MailChimpConstant.HTTP_METHOD_POST,
                Path = $"ecommerce/stores/{_mailChimpSettings.StoreId}/products",
                Body = JsonConvert.SerializeObject(x, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })
            });

            return operations;
        }

        private IEnumerable<Operation> GetUpdateProductsOperations()
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var products = GetMapProducts();

            var operations = products.Select(x => new Operation
            {
                Method = MailChimpConstant.HTTP_METHOD_PATCH,
                Path = $"ecommerce/stores/{_mailChimpSettings.StoreId}/products/{x.Id}",
                Body = JsonConvert.SerializeObject(x, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })
            });

            return operations;
        }

        private IEnumerable<Product> GetMapProducts()
        {
            var products = _productRepository
                .TableNoTracking
                .Where(p => p.Published 
                    && p.ProductAttributeCombinations.Any(pac => !string.IsNullOrEmpty(pac.Sku)))
                .Select(p => new Product
                {
                    Id = p.Id.ToString(),
                    Title = p.Name,
                    Variants = p.ProductAttributeCombinations
                        .Where(pac => !string.IsNullOrEmpty(pac.Sku))
                        .Select(pac => new Variant
                        {
                            Id = pac.Id.ToString(),
                            Title = p.Name,
                            Sku = pac.Sku,
                            Visibility = "true"
                        }).ToList()
                }).ToList();

            return products;
        }

        public async Task AddOrUpdateBatchSubscribers(int storeId)
        {
            var operations = new List<Operation>();
            operations.AddRange(GetCreateSubscribersOperations(storeId));
            operations.AddRange(GetUpdateSubscribersOperations(storeId));

            await ExecuteBatchOperations(operations);
        }

        private IEnumerable<Operation> GetCreateSubscribersOperations(int storeId)
        {
            var members = GetMapMembers(storeId);

            var operations = members.Select(x => new Operation
            {
                Method = MailChimpConstant.HTTP_METHOD_POST,
                Path = $"/lists/{_mailChimpSettings.ListId}/members/",
                Body = JsonConvert.SerializeObject(x, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })
            });

            return operations;
        }

        private IEnumerable<Operation> GetUpdateSubscribersOperations(int storeId)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var members = GetMapMembers(storeId, true);

            var operations = members.Select(x => new Operation
            {
                Method = MailChimpConstant.HTTP_METHOD_PUT,
                Path = $"/lists/{_mailChimpSettings.ListId}/members/{manager.Members.Hash(x.EmailAddress.ToLower())}",
                Body = JsonConvert.SerializeObject(x, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })
            });

            return operations;
        }

        private IEnumerable<Member> GetMapMembers(int storeId, bool includeInactive = false)
        {
            var nopMembers = _subscriptionRepository.TableNoTracking
                .Where(x => x.StoreId == storeId);

            if (!includeInactive)
                nopMembers = nopMembers.Where(x => x.Active);

            var members = nopMembers.Select(x => new Member
            {
                EmailAddress = x.Email,
                Status = x.Active ? Status.Subscribed : Status.Unsubscribed
            }).ToList();

            members.ForEach(x => MapMember(x));

            return members;
        }

        public Member MapMember(Member member)
        {
            var customer = _customerService.GetCustomerByEmail(member.EmailAddress);
            if (!customer?.IsGuest() ?? false)
            {
                var firstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                var lastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
                if (!string.IsNullOrEmpty(firstName) || !string.IsNullOrEmpty(lastName))
                {
                    member.MergeFields = new Dictionary<string, object>
                    {
                        [MailChimpConstant.MERGE_FIELD_FIRST_NAME] = firstName,
                        [MailChimpConstant.MERGE_FIELD_LAST_NAME] = lastName
                    };
                }
            }

            return member;
        }

        private async Task ExecuteBatchOperations(List<Operation> operations)
        {
            IMailChimpManager manager = new MailChimpManager(_mailChimpSettings.ApiKey);

            var batchNumber = operations.Count / MailChimpConstant.ITEM_LIMIT +
                    (operations.Count % MailChimpConstant.ITEM_LIMIT > 0 ? 1 : 0);

            for (int i = 0; i < batchNumber; i++)
            {
                var batchOperations = operations.Skip(i * MailChimpConstant.ITEM_LIMIT).Take(MailChimpConstant.ITEM_LIMIT);
                var batch = await manager.Batches.AddAsync(new BatchRequest { Operations = batchOperations });
            }
        }
    }
}
