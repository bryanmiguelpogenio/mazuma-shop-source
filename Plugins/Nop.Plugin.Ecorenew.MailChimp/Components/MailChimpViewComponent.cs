﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Ecorenew.MailChimp.Models;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Ecorenew.MailChimp.Components
{
    [ViewComponent(Name = "EcorenewMailChimp")]
    public class MailChimpViewComponent : NopViewComponent
    {
        public MailChimpViewComponent()
        {

        }

        public IViewComponentResult Invoke(string widgetZone, object additionalData)
        {
            var model = new PublicInfoModel
            {
                QueryNameCampaignId = "mc_cid",
                QueryNameEmailId =  "mc_eid"
            };

            return View("~/Plugins/Ecorenew.MailChimp/Views/PublicInfo.cshtml", model);
        }
    }
}
