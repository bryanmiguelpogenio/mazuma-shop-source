﻿using Nop.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Ecorenew.MailChimp
{
    public class MailChimpSettings : ISettings
    {
        public string ApiKey { get; set; }

        public string StoreId { get; set; }

        public bool EnableEcommerce { get; set; }

        public string ListId { get; set; }
    }
}
