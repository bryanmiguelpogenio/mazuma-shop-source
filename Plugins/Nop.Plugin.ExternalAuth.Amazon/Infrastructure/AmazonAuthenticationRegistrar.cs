﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Nop.Core.Infrastructure;
using Nop.Services.Authentication.External;

namespace Nop.Plugin.ExternalAuth.Amazon.Infrastructure
{
    /// <summary>
    /// Registration of Amazon authentication service (plugin)
    /// </summary>
    public class AmazonAuthenticationRegistrar : IExternalAuthenticationRegistrar
    {
        /// <summary>
        /// Configure
        /// </summary>
        /// <param name="builder">Authentication builder</param>
        public void Configure(AuthenticationBuilder builder)
        {
            builder.AddAmazon(AmazonAuthenticationDefaults.AuthenticationScheme, options =>
            {
                var settings = EngineContext.Current.Resolve<AmazonExternalAuthSettings>();

                options.ClientId = settings.ClientKeyIdentifier;
                options.ClientSecret = settings.ClientSecret;
                options.SaveTokens = true;
            });
        }
    }
}
