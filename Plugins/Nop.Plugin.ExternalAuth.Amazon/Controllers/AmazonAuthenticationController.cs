﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Security.OAuth.Amazon;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Nop.Core;
using Nop.Plugin.ExternalAuth.Amazon.Models;
using Nop.Services.Authentication.External;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.ExternalAuth.Amazon.Controllers
{
    public class AmazonAuthenticationController : BasePluginController
    {
        #region Fields

        private readonly AmazonExternalAuthSettings _amazonExternalAuthSettings;
        private readonly IExternalAuthenticationService _externalAuthenticationService;
        private readonly ILocalizationService _localizationService;
        private readonly IOptionsMonitorCache<AmazonAuthenticationOptions> _optionsCache;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor

        public AmazonAuthenticationController(AmazonExternalAuthSettings amazonExternalAuthSettings,
            IExternalAuthenticationService externalAuthenticationService,
            ILocalizationService localizationService,
            IOptionsMonitorCache<AmazonAuthenticationOptions> optionsCache,
            IPermissionService permissionService,
            ISettingService settingService)
        {
            this._amazonExternalAuthSettings = amazonExternalAuthSettings;
            this._externalAuthenticationService = externalAuthenticationService;
            this._localizationService = localizationService;
            this._optionsCache = optionsCache;
            this._permissionService = permissionService;
            this._settingService = settingService;
        }

        #endregion

        #region Methods

        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageExternalAuthenticationMethods))
                return AccessDeniedView();

            var model = new ConfigurationModel
            {
                ClientId = _amazonExternalAuthSettings.ClientKeyIdentifier,
                ClientSecret = _amazonExternalAuthSettings.ClientSecret,
                DefaultReturnUrl = _amazonExternalAuthSettings.DefaultReturnUrl
            };

            return View("~/Plugins/ExternalAuth.Amazon/Views/Configure.cshtml", model);
        }

        [HttpPost]
        [AdminAntiForgery]
        [AuthorizeAdmin]
        [Area(AreaNames.Admin)]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageExternalAuthenticationMethods))
                return AccessDeniedView();

            if (!ModelState.IsValid)
                return Configure();

            //save settings
            _amazonExternalAuthSettings.ClientKeyIdentifier = model.ClientId;
            _amazonExternalAuthSettings.ClientSecret = model.ClientSecret;
            _amazonExternalAuthSettings.DefaultReturnUrl = model.DefaultReturnUrl;
            _settingService.SaveSetting(_amazonExternalAuthSettings);

            //clear Amazon authentication options cache
            _optionsCache.TryRemove(AmazonAuthenticationDefaults.AuthenticationScheme);

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        public IActionResult Login(string returnUrl)
        {
            if (!_externalAuthenticationService.ExternalAuthenticationMethodIsAvailable(AmazonAuthenticationDefaults.ProviderSystemName))
                throw new NopException("Amazon authentication module cannot be loaded");

            if (string.IsNullOrEmpty(_amazonExternalAuthSettings.ClientKeyIdentifier) || string.IsNullOrEmpty(_amazonExternalAuthSettings.ClientSecret))
                throw new NopException("Amazon authentication module not configured");

            if (string.IsNullOrEmpty(returnUrl))
                returnUrl = _amazonExternalAuthSettings.DefaultReturnUrl;

            //configure login callback action
            var authenticationProperties = new AuthenticationProperties
            {
                RedirectUri = Url.Action("LoginCallback", "AmazonAuthentication", new { returnUrl = returnUrl })
            };

            return Challenge(authenticationProperties, AmazonAuthenticationDefaults.AuthenticationScheme);
        }

        public async Task<IActionResult> LoginCallback(string returnUrl)
        {
            //authenticate Amazon user
            var authenticateResult =  await this.HttpContext.AuthenticateAsync(AmazonAuthenticationDefaults.AuthenticationScheme);
            if (!authenticateResult.Succeeded || !authenticateResult.Principal.Claims.Any())
                return RedirectToRoute("Login");

            //create external authentication parameters
            var authenticationParameters = new ExternalAuthenticationParameters
            {
                ProviderSystemName = AmazonAuthenticationDefaults.ProviderSystemName,
                AccessToken = await this.HttpContext.GetTokenAsync(AmazonAuthenticationDefaults.AuthenticationScheme, "access_token"),
                Email = authenticateResult.Principal.FindFirst(claim => claim.Type == ClaimTypes.Email)?.Value,
                ExternalIdentifier = authenticateResult.Principal.FindFirst(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value,
                ExternalDisplayIdentifier = authenticateResult.Principal.FindFirst(claim => claim.Type == ClaimTypes.Name)?.Value,
                Claims = authenticateResult.Principal.Claims.Select(claim => new ExternalAuthenticationClaim(claim.Type, claim.Value)).ToList()
            };

            //authenticate Nop user
            return _externalAuthenticationService.Authenticate(authenticationParameters, returnUrl);
        }

        #endregion
    }
}