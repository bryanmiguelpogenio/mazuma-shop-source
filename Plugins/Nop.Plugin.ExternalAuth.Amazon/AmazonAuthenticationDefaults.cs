﻿
namespace Nop.Plugin.ExternalAuth.Amazon
{
    /// <summary>
    /// Default values used by the Amazon authentication middleware
    /// </summary>
    public static class AmazonAuthenticationDefaults
    {
        /// <summary>
        /// System name of the external authentication method
        /// </summary>
        public const string ProviderSystemName = "ExternalAuth.Amazon";

        /// <summary>
        /// Default value of the Scheme
        /// </summary>
        public const string AuthenticationScheme = "Amazon";
    }
}
