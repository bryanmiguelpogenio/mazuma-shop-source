﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.ExternalAuth.Amazon.Components
{
    [ViewComponent(Name = "AmazonAuthentication")]
    public class AmazonAuthenticationViewComponent : NopViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Plugins/ExternalAuth.Amazon/Views/PublicInfo.cshtml");
        }
    }
}