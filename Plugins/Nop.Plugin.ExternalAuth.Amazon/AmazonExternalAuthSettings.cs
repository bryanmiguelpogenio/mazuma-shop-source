using Nop.Core.Configuration;

namespace Nop.Plugin.ExternalAuth.Amazon
{
    /// <summary>
    /// Represents settings of the Amazon authentication method
    /// </summary>
    public class AmazonExternalAuthSettings : ISettings
    {
        /// <summary>
        /// Gets or sets OAuth2 client identifier
        /// </summary>
        public string ClientKeyIdentifier { get; set; }

        /// <summary>
        /// Gets or sets OAuth2 client secret
        /// </summary>
        public string ClientSecret { get; set; }

        /// <summary>
        /// Gets or sets Default returnUrl
        /// </summary>
        public string DefaultReturnUrl { get; set; }
    }
}
