using Nop.Core;
using Nop.Core.Plugins;
using Nop.Services.Authentication.External;
using Nop.Services.Configuration;
using Nop.Services.Localization;

namespace Nop.Plugin.ExternalAuth.Amazon
{
    /// <summary>
    /// Represents method for the authentication with Amazon account
    /// </summary>
    public class AmazonAuthenticationMethod : BasePlugin, IExternalAuthenticationMethod
    {
        #region Fields

        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;

        #endregion

        #region Ctor

        public AmazonAuthenticationMethod(ISettingService settingService,
            IWebHelper webHelper)
        {
            this._settingService = settingService;
            this._webHelper = webHelper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a configuration page URL
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/AmazonAuthentication/Configure";
        }

        /// <summary>
        /// Gets a view component for displaying plugin in public store
        /// </summary>
        /// <param name="viewComponentName">View component name</param>
        public void GetPublicViewComponent(out string viewComponentName)
        {
            viewComponentName = "AmazonAuthentication";
        }

        /// <summary>
        /// Install the plugin
        /// </summary>
        public override void Install()
        {
            //settings
            _settingService.SaveSetting(new AmazonExternalAuthSettings());

            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.ExternalAuth.Amazon.ClientKeyIdentifier", "App ID/API Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.ExternalAuth.Amazon.ClientKeyIdentifier.Hint", "Enter your app ID/API key here. You can find it on your Amazon application page.");
            this.AddOrUpdatePluginLocaleResource("Plugins.ExternalAuth.Amazon.ClientSecret", "App Secret");
            this.AddOrUpdatePluginLocaleResource("Plugins.ExternalAuth.Amazon.ClientSecret.Hint", "Enter your app secret here. You can find it on your Amazon application page.");

            base.Install();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<AmazonExternalAuthSettings>();

            //locales
            this.DeletePluginLocaleResource("Plugins.ExternalAuth.Amazon.ClientKeyIdentifier");
            this.DeletePluginLocaleResource("Plugins.ExternalAuth.Amazon.ClientKeyIdentifier.Hint");
            this.DeletePluginLocaleResource("Plugins.ExternalAuth.Amazon.ClientSecret");
            this.DeletePluginLocaleResource("Plugins.ExternalAuth.Amazon.ClientSecret.Hint");

            base.Uninstall();
        }

        #endregion
    }
}
