﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Ecorenew.CustomMessageTemplates
{
    public class CustomMessageTemplateSettings : ISettings
    {
        /// <summary>
        /// Gets or sets the value indicating whether the custom message templates is enabled
        /// </summary>
        public bool CustomMessageTemplatesEnabled { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the custom message token provider enabled is enabled
        /// </summary>
        //public bool CustomMessageTokenProviderEnabled { get; set; }
    }
}
