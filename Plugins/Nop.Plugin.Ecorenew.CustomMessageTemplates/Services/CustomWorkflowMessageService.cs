﻿using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Core.Plugins;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Ecorenew.CustomMessageTemplates.Services
{
    public class CustomWorkflowMessageService : WorkflowMessageService
    {
        #region Fields

        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly IPluginFinder _pluginFinder;
        private readonly IStoreService _storeService;
        private readonly IStoreContext _storeContext;
        private readonly IEventPublisher _eventPublisher;
        private readonly CustomMessageTemplateSettings _customMessageTemplateSettings;

        #endregion

        #region Ctor

        public CustomWorkflowMessageService(IMessageTemplateService messageTemplateService, 
            IQueuedEmailService queuedEmailService, 
            ILanguageService languageService, 
            ITokenizer tokenizer, 
            IEmailAccountService emailAccountService, 
            IMessageTokenProvider messageTokenProvider,
            IPluginFinder pluginFinder,
            IStoreService storeService, 
            IStoreContext storeContext, 
            CommonSettings commonSettings, 
            EmailAccountSettings emailAccountSettings, 
            IEventPublisher eventPublisher,
            CustomMessageTemplateSettings customMessageTemplateSettings) 
            : 
            base(messageTemplateService, 
                queuedEmailService, 
                languageService, 
                tokenizer, 
                emailAccountService, 
                messageTokenProvider, 
                storeService, 
                storeContext, 
                commonSettings, 
                emailAccountSettings, 
                eventPublisher)
        {
            this._messageTokenProvider = messageTokenProvider;
            this._pluginFinder = pluginFinder;
            this._storeService = storeService;
            this._storeContext = storeContext;
            this._eventPublisher = eventPublisher;
            this._customMessageTemplateSettings = customMessageTemplateSettings;
        }

        #endregion

        #region Methods

        public override int SendOrderPaidCustomerNotification(Order order, int languageId, string attachmentFilePath = null, string attachmentFileName = null)
        {
            // check if the plugin is installed and enabled for the current store
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(CustomMessageTemplatesConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
                return base.SendOrderPaidCustomerNotification(order, languageId, attachmentFilePath, attachmentFileName);

            // check if the setting for custom message templates enabled is enabled
            if (!_customMessageTemplateSettings.CustomMessageTemplatesEnabled)
                return base.SendOrderPaidCustomerNotification(order, languageId, attachmentFilePath, attachmentFileName);

            if (order == null)
                throw new ArgumentNullException(nameof(order));

            var store = _storeService.GetStoreById(order.StoreId) ?? _storeContext.CurrentStore;

            if (store == null)
                throw new Exception("Send Order paid customer notification failed. Store with ID " + _storeContext.CurrentStore.Id + " not found");

            languageId = EnsureLanguageIsActive(languageId, store.Id);

            var messageTemplate = GetActiveMessageTemplate(MessageTemplateSystemNames.OrderPaidCustomerNotification, store.Id);
            if (messageTemplate == null)
                return 0;

            //email account
            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

            //tokens
            var tokens = new List<Token>();
            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            _messageTokenProvider.AddOrderTokens(tokens, order, languageId);
            _messageTokenProvider.AddCustomerTokens(tokens, order.Customer);

            //event notification
            _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

            var toEmail = order.BillingAddress.Email;
            var toName = $"{order.BillingAddress.FirstName} {order.BillingAddress.LastName}";

            return SendNotification(messageTemplate, emailAccount, languageId, tokens, toEmail, toName,
                attachmentFilePath, attachmentFileName);
        }

        public override int SendOrderPlacedStoreOwnerNotification(Order order, int languageId)
        {
            // check if the plugin is installed and enabled for the current store
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(CustomMessageTemplatesConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
                return base.SendOrderPlacedStoreOwnerNotification(order, languageId);

            // check if the setting for custom message templates enabled is enabled
            if (!_customMessageTemplateSettings.CustomMessageTemplatesEnabled)
                return base.SendOrderPlacedStoreOwnerNotification(order, languageId);

            if (order == null)
                throw new ArgumentNullException(nameof(order));

            var store = _storeService.GetStoreById(order.StoreId) ?? _storeContext.CurrentStore;
            languageId = EnsureLanguageIsActive(languageId, store.Id);

            var messageTemplate = GetActiveMessageTemplate(MessageTemplateSystemNames.OrderPlacedStoreOwnerNotification, store.Id);
            if (messageTemplate == null)
                return 0;

            //email account
            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

            //tokens
            var tokens = new List<Token>();
            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            _messageTokenProvider.AddOrderTokens(tokens, order, languageId);
            _messageTokenProvider.AddCustomerTokens(tokens, order.Customer);

            //event notification
            _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

            var toEmail = emailAccount.Email;
            var toName = emailAccount.DisplayName;

            return SendNotification(messageTemplate, emailAccount, languageId, tokens, toEmail, toName);
        }
        #endregion
    }
}
