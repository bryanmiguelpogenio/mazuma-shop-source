﻿using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Ecorenew.CustomMessageTemplates.Models
{
    public class ConfigurationModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.CustomMessageTemplates.Fields.CustomMessageTemplatesEnabled")]
        public bool CustomMessageTemplateEnabled { get; set; }
        public bool CustomMessageTemplateEnabled_OverrideForStore { get; set; }
    }
}
