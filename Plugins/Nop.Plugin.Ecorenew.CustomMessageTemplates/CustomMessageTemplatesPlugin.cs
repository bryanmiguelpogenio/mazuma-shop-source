﻿using Microsoft.AspNetCore.Routing;
using Nop.Core;
using Nop.Core.Plugins;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Web.Framework;
using Nop.Web.Framework.Menu;
using System.Linq;

namespace Nop.Plugin.Ecorenew.CustomMessageTemplates
{
    public class CustomMessageTemplatesPlugin : BasePlugin, IMiscPlugin, IAdminMenuPlugin
    {
        #region Fields

        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;

        #endregion

        #region Ctor

        public CustomMessageTemplatesPlugin(ISettingService settingService,
            IWebHelper webHelper)
        {
            this._settingService = settingService;
            this._webHelper = webHelper;
        }

        #endregion

        #region Methods

        public override void Install()
        {
            var settings = new CustomMessageTemplateSettings
            {
                CustomMessageTemplatesEnabled = false
            };
            _settingService.SaveSetting(settings);

            // locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomMessageTemplates.Title", "Custom Message Templates");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomMessageTemplates.Fields.CustomMessageTemplatesEnabled", "Enable Event");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomMessageTemplates.OrderCustomerNotification.Insurance.Title", "Insurance: Accidental Damage, Theft & loss");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomMessageTemplates.OrderCustomerNotification.Insurance.Content1", "<strong>*No payment will be taken today</strong>");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomMessageTemplates.OrderCustomerNotification.Insurance.Content2", "A separate email will be sent from our Partner Company Tinhat with your insurance details.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomMessageTemplates.OrderCustomerNotification.Note", "<br /><p style=\"margin: 0; padding: 0; font-style: italic \">Mazuma Mobile Ltd is an Introducer Appointed Representative of Bastion Insurance Services Ltd who are authorised and regulated by FCA, Reg No. 650727</p>");

            base.Install();
        }

        public override void Uninstall()
        {
            // settings
            _settingService.DeleteSetting<CustomMessageTemplateSettings>();

            // locales
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomMessageTemplates.Title");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomMessageTemplates.Fields.CustomMessageTemplatesEnabled");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomMessageTemplates.OrderCustomerNotification.Insurance.Title");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomMessageTemplates.OrderCustomerNotification.Insurance.Content1");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomMessageTemplates.OrderCustomerNotification.Insurance.Content2");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomMessageTemplates.OrderCustomerNotification.Note");

            base.Uninstall();
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/CustomMessageTemplates/Configure";
        }

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var mainNode = new SiteMapNode()
            {
                SystemName = "Ecorenew",
                Title = "EcoRenew Group",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
                IconClass = "fa-leaf"
            };

            var customerReminderNode = new SiteMapNode()
            {
                SystemName = "Ecorenew.CustomMessageTemplates",
                Title = "Custom Message Templates",
                Visible = true,
                IconClass = "fa-genderless",
                RouteValues = new RouteValueDictionary() { { "area", AreaNames.Admin } },
                ControllerName = "CustomMessageTemplates",
                ActionName = "Configure"
            };

            mainNode.ChildNodes.Add(customerReminderNode);

            var ecorenewNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Ecorenew");
            if (ecorenewNode != null)
                ecorenewNode.ChildNodes.Add(customerReminderNode);
            else
                rootNode.ChildNodes.Add(mainNode);
        }

        #endregion
    }
}
