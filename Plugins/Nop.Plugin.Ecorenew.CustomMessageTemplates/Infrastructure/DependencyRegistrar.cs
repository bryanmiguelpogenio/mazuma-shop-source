﻿using Autofac;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Plugin.Ecorenew.CustomMessageTemplates.Services;
using Nop.Services.Messages;

namespace Nop.Plugin.Ecorenew.CustomMessageTemplates.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            // overridden services
            builder.RegisterType<CustomWorkflowMessageService>()
                .As<IWorkflowMessageService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CustomMessageTokenProvider>()
                .As<IMessageTokenProvider>()
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 9; }
        }
    }
}
