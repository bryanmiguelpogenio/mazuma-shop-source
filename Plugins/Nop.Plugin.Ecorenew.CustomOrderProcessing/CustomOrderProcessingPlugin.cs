﻿using Microsoft.AspNetCore.Routing;
using Nop.Core;
using Nop.Core.Plugins;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Web.Framework;
using Nop.Web.Framework.Menu;
using System.Linq;

namespace Nop.Plugin.Ecorenew.CustomOrderProcessing
{
    public class CustomOrderProcessingPlugin : BasePlugin, IMiscPlugin, IAdminMenuPlugin
    {
        #region Fields

        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;

        #endregion

        #region Ctor

        public CustomOrderProcessingPlugin(ISettingService settingService,
            IWebHelper webHelper)
        {
            this._settingService = settingService;
            this._webHelper = webHelper;
        }

        #endregion

        #region Methods

        public override void Install()
        {
            var settings = new CustomOrderProcessingSettings
            {
                CustomOrderProcessingEnabled = false
            };
            _settingService.SaveSetting(settings);

            // locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomOrderProcessing.Title", "Custom Order Processing");
            this.AddOrUpdatePluginLocaleResource("Plugins.Ecorenew.CustomOrderProcessing.Fields.CustomOrderProcessing", "Enable Event");

            base.Install();
        }

        public override void Uninstall()
        {
            // settings
            _settingService.DeleteSetting<CustomOrderProcessingSettings>();

            // locales
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomOrderProcessing.Title");
            this.DeletePluginLocaleResource("Plugins.Ecorenew.CustomOrderProcessing.Fields.CustomOrderProcessing");

            base.Uninstall();
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/CustomOrderProcessing/Configure";
        }

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var mainNode = new SiteMapNode()
            {
                SystemName = "Ecorenew",
                Title = "EcoRenew Group",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
                IconClass = "fa-leaf"
            };

            var customerReminderNode = new SiteMapNode()
            {
                SystemName = "Ecorenew.CustomOrderProcessing",
                Title = "Custom Order Processing",
                Visible = true,
                IconClass = "fa-genderless",
                RouteValues = new RouteValueDictionary() { { "area", AreaNames.Admin } },
                ControllerName = "CustomOrderProcessing",
                ActionName = "Configure"
            };

            mainNode.ChildNodes.Add(customerReminderNode);

            var ecorenewNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Ecorenew");
            if (ecorenewNode != null)
                ecorenewNode.ChildNodes.Add(customerReminderNode);
            else
                rootNode.ChildNodes.Add(mainNode);
        }

        #endregion
    }
}
