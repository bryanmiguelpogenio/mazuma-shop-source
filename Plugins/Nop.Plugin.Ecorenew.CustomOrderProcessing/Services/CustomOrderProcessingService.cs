﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Bank;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Core.Plugins;
using Nop.Services.Affiliates;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Events;
using Nop.Services.Insurance;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Ecorenew.CustomOrderProcessing.Services
{
    public class CustomOrderProcessingService : OrderProcessingService
    {
        #region Fields

        private readonly IInsuranceDeviceService _insuranceDeviceService;
        private readonly IOrderService _orderService;
        private readonly ILocalizationService _localizationService;
        private readonly IProductService _productService;
        private readonly ILogger _logger;
        private readonly IPluginFinder _pluginFinder;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IShippingService _shippingService;
        private readonly ITaxService _taxService;
        private readonly IStoreContext _storeContext;
        private readonly CustomOrderProcessingSettings _customOrderProcessingSettings;

        #endregion

        #region Ctor

        public CustomOrderProcessingService(IInsuranceDeviceService insuranceDeviceService,
            IOrderService orderService, 
            IWebHelper webHelper, 
            ILocalizationService localizationService, 
            ILanguageService languageService, 
            IProductService productService, 
            IPaymentService paymentService, 
            ILogger logger, 
            IOrderTotalCalculationService orderTotalCalculationService,
            IPluginFinder pluginFinder,
            IPriceCalculationService priceCalculationService, 
            IPriceFormatter priceFormatter, 
            IProductAttributeParser productAttributeParser, 
            IProductAttributeFormatter productAttributeFormatter, 
            IGiftCardService giftCardService, 
            IShoppingCartService shoppingCartService, 
            ICheckoutAttributeFormatter checkoutAttributeFormatter, 
            IShippingService shippingService, 
            IShipmentService shipmentService, 
            ITaxService taxService, 
            ICustomerService customerService, 
            IDiscountService discountService, 
            IEncryptionService encryptionService, 
            IWorkContext workContext, 
            IWorkflowMessageService workflowMessageService, 
            IVendorService vendorService, 
            ICustomerActivityService customerActivityService, 
            ICurrencyService currencyService, 
            IAffiliateService affiliateService, 
            IEventPublisher eventPublisher, 
            IPdfService pdfService, 
            IRewardPointService rewardPointService, 
            IGenericAttributeService genericAttributeService, 
            ICountryService countryService, 
            IStateProvinceService stateProvinceService,
            IStoreContext storeContext,
            ShippingSettings shippingSettings, 
            PaymentSettings paymentSettings, 
            RewardPointsSettings rewardPointsSettings, 
            OrderSettings orderSettings, 
            TaxSettings taxSettings, 
            LocalizationSettings localizationSettings, 
            CurrencySettings currencySettings, 
            ICustomNumberFormatter customNumberFormatter, 
            IRepository<TransferRecord> transferRecordRepository,
            CustomOrderProcessingSettings customOrderProcessingSettings) 
            : 
            base(orderService, 
                webHelper, 
                localizationService, 
                languageService, 
                productService, 
                paymentService, 
                logger, 
                orderTotalCalculationService, 
                priceCalculationService, 
                priceFormatter, 
                productAttributeParser, 
                productAttributeFormatter, 
                giftCardService, 
                shoppingCartService, 
                checkoutAttributeFormatter, 
                shippingService, 
                shipmentService, 
                taxService, 
                customerService, 
                discountService,
                encryptionService, 
                workContext, 
                workflowMessageService, 
                vendorService, 
                customerActivityService, 
                currencyService, 
                affiliateService, 
                eventPublisher, 
                pdfService, 
                rewardPointService, 
                genericAttributeService, 
                countryService, 
                stateProvinceService, 
                shippingSettings, 
                paymentSettings, 
                rewardPointsSettings, 
                orderSettings, 
                taxSettings, 
                localizationSettings, 
                currencySettings, 
                customNumberFormatter, 
                transferRecordRepository)
        {
            this._insuranceDeviceService = insuranceDeviceService;
            this._orderService = orderService;
            this._localizationService = localizationService;
            this._productService = productService;
            this._logger = logger;
            this._pluginFinder = pluginFinder;
            this._priceCalculationService = priceCalculationService;
            this._productAttributeFormatter = productAttributeFormatter;
            this._shoppingCartService = shoppingCartService;
            this._shippingService = shippingService;
            this._taxService = taxService;
            this._storeContext = storeContext;
            this._customOrderProcessingSettings = customOrderProcessingSettings;
        }

        #endregion

        #region Methods

        protected override void MoveShoppingCartItemsToOrderItems(PlaceOrderContainer details, Order order, IList<string> insuranceShoppingCartIds = null)
        {
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName(CustomOrderProcessingConstants.PluginSystemName);
            if (pluginDescriptor == null || !pluginDescriptor.Installed || _pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id) == false)
            {
                base.MoveShoppingCartItemsToOrderItems(details, order);
                return;
            }

            // check if the setting for custom order processing is enabled
            if (!_customOrderProcessingSettings.CustomOrderProcessingEnabled)
            {
                base.MoveShoppingCartItemsToOrderItems(details, order);
                return;
            }

            foreach (var sc in details.Cart)
            {
                //prices
                var scUnitPrice = _priceCalculationService.GetUnitPrice(sc);
                var scSubTotal = _priceCalculationService.GetSubTotal(sc, true, out decimal discountAmount,
                    out List<DiscountForCaching> scDiscounts, out int? _);
                var scUnitPriceInclTax =
                    _taxService.GetProductPrice(sc.Product, scUnitPrice, true, details.Customer, out decimal _);
                var scUnitPriceExclTax =
                    _taxService.GetProductPrice(sc.Product, scUnitPrice, false, details.Customer, out _);
                var scSubTotalInclTax =
                    _taxService.GetProductPrice(sc.Product, scSubTotal, true, details.Customer, out _);
                var scSubTotalExclTax =
                    _taxService.GetProductPrice(sc.Product, scSubTotal, false, details.Customer, out _);
                var discountAmountInclTax =
                    _taxService.GetProductPrice(sc.Product, discountAmount, true, details.Customer, out _);
                var discountAmountExclTax =
                    _taxService.GetProductPrice(sc.Product, discountAmount, false, details.Customer, out _);
                foreach (var disc in scDiscounts)
                    if (!details.AppliedDiscounts.ContainsDiscount(disc))
                        details.AppliedDiscounts.Add(disc);

                //attributes
                var attributeDescription =
                    _productAttributeFormatter.FormatAttributes(sc.Product, sc.AttributesXml, details.Customer);

                var itemWeight = _shippingService.GetShoppingCartItemWeight(sc);

                //get unitInsurancePrice
                decimal unitInsurancePrice = 0;
                if (insuranceShoppingCartIds != null && insuranceShoppingCartIds.Any(x => x == sc.Id.ToString()))
                {
                    var insuranceDevice = _insuranceDeviceService.GetInsuranceDeviceByAttributesXml(sc.AttributesXml);

                    if (insuranceDevice != null)
                    {
                        _logger.InsertLog(Core.Domain.Logging.LogLevel.Information,
                            "Order Processing: Insurance Value",
                            $"Insurance device value is {insuranceDevice.Value}");

                        var insuranceProduct = _productService.GetInsuranceProductByProduct(sc.Product, insuranceDevice.Value);
                        if (insuranceProduct != null)
                            unitInsurancePrice = insuranceProduct.Price;
                    }
                }

                //save order item
                var orderItem = new OrderItem
                {
                    OrderItemGuid = Guid.NewGuid(),
                    Order = order,
                    ProductId = sc.ProductId,
                    UnitPriceInclTax = scUnitPriceInclTax,
                    UnitPriceExclTax = scUnitPriceExclTax,
                    PriceInclTax = scSubTotalInclTax,
                    PriceExclTax = scSubTotalExclTax,
                    OriginalProductCost = _priceCalculationService.GetProductCost(sc.Product, sc.AttributesXml),
                    AttributeDescription = attributeDescription,
                    AttributesXml = sc.AttributesXml,
                    Quantity = sc.Quantity,
                    DiscountAmountInclTax = discountAmountInclTax,
                    DiscountAmountExclTax = discountAmountExclTax,
                    DownloadCount = 0,
                    IsDownloadActivated = false,
                    LicenseDownloadId = 0,
                    ItemWeight = itemWeight,
                    RentalStartDateUtc = sc.RentalStartDateUtc,
                    RentalEndDateUtc = sc.RentalEndDateUtc,
                    UnitInsurancePrice = unitInsurancePrice
                };
                order.OrderItems.Add(orderItem);
                _orderService.UpdateOrder(order);

                //gift cards
                AddGiftCards(sc.Product, sc.AttributesXml, sc.Quantity, orderItem, scUnitPriceExclTax);

                //inventory
                _productService.AdjustInventory(sc.Product, -sc.Quantity, sc.AttributesXml,
                    string.Format(_localizationService.GetResource("Admin.StockQuantityHistory.Messages.PlaceOrder"),
                        order.Id));
            }

            //clear shopping cart
            details.Cart.ToList().ForEach(sci => _shoppingCartService.DeleteShoppingCartItem(sci, false));
        }

        #endregion
    }
}
