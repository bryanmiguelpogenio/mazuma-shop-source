﻿using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Ecorenew.CustomOrderProcessing.Models
{
    public class ConfigurationModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Ecorenew.CustomExportManager.Fields.CustomExportManagerEnabled")]
        public bool CustomOrderProcessingEnabled { get; set; }
        public bool CustomOrderProcessingEnabled_OverrideForStore { get; set; }
    }
}
