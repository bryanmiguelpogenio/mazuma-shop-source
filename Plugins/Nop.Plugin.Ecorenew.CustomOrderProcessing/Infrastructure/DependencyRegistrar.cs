﻿using Autofac;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Plugin.Ecorenew.CustomOrderProcessing.Services;
using Nop.Services.Orders;

namespace Nop.Plugin.Ecorenew.CustomOrderProcessing.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            // overridden services
            builder.RegisterType<CustomOrderProcessingService>()
                .As<IOrderProcessingService>()
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 2; }
        }
    }
}
