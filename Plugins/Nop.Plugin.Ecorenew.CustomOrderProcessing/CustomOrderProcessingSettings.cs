﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Ecorenew.CustomOrderProcessing
{
    public class CustomOrderProcessingSettings : ISettings
    {
        /// <summary>
        /// Gets or sets the value indicating whether the custom order processing is enabled
        /// </summary>
        public bool CustomOrderProcessingEnabled { get; set; }
    }
}
