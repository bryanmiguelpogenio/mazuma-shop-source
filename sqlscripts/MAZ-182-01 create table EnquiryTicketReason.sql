

CREATE TABLE [dbo].[EnquiryTicketReason](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Reason] [nvarchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_EnquiryTicketReason] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[EnquiryTicketReason] ADD  CONSTRAINT [DF_EnquiryTicketReason_IsActive]  DEFAULT ((1)) FOR [IsActive]



