create table dbo.Site
(
Id int identity(1,1) primary key,
Name nvarchar(max) not null,
WebUrl nvarchar(max) not null,
DbConn nvarchar(max) not null,
DisplayOrder int default '0' not null,
MainBack int not null
);

--EntityData is only used to save the data which has no identity(such as Discount_AppliedToCategories)
create table dbo.OperateLog
(
Id int identity(1,1) primary key,
Action int not null,
TableName nvarchar(500) not null,
EntityId int not null
);

--create link sql

create table dbo.SyncGroupInfo
(
Id int identity(1,1) primary key,
GroupName nvarchar(max) not null,
TableNames nvarchar(max) not null,
DisplayOrder int default '0' not null,
RequireSync int default '0' not null
);

insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('CustomerRole','dbo.CustomerRole,dbo.AclRecord,dbo.PermissionRecord,dbo.PermissionRecord_Role_Mapping',1,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('BrainTree_PayPal','dbo.BrainTree_PayPalCredit_OrderFinancingDetails,dbo.BrainTree_PayPalDetails',2,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Campaign','dbo.Campaign',3,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Product','dbo.Category,dbo.CategoryTemplate,dbo.CrossSellProduct,dbo.Picture,dbo.PredefinedProductAttributeValue,dbo.Product,dbo.Product_Category_Mapping,dbo.Product_Manufacturer_Mapping,dbo.Product_Picture_Mapping,dbo.Product_ProductAttribute_Mapping,dbo.Product_ProductTag_Mapping,dbo.Product_SpecificationAttribute_Mapping,dbo.ProductAttribute,dbo.ProductAttributeCombination,dbo.ProductAttributeValue,dbo.ProductAvailabilityRange,dbo.ProductTag,dbo.ProductTemplate,dbo.ProductWarehouseInventory,dbo.RelatedProduct,dbo.SpecificationAttribute,dbo.SpecificationAttributeOption,dbo.TempSkuPrice,dbo.TempSpecAttrCustomValue,dbo.TierPrice',4,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('CheckoutAttribute','dbo.CheckoutAttribute,dbo.CheckoutAttributeValue',5,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Country','dbo.Country,dbo.StateProvince',6,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('CustomerAttribute','dbo.CustomerAttribute,dbo.CustomerAttributeValue',8,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('DeliveryDate','dbo.DeliveryDate',9,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Discount','dbo.Discount,dbo.Discount_AppliedToCategories,dbo.Discount_AppliedToManufacturers,dbo.Discount_AppliedToProducts,dbo.Discount_AppliedToProductSkus,dbo.DiscountRequirement',10,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Download','dbo.Download',11,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Ecorenew','dbo.Ecorenew_AnnualGrossIncome,dbo.Ecorenew_CallReportResult,dbo.Ecorenew_CallValidateResult,dbo.Ecorenew_ConsumerFinance_CaapRecord,dbo.Ecorenew_ConsumerFinance_FinanceOption,dbo.Ecorenew_ConsumerFinance_OrderItem,dbo.Ecorenew_ConsumerFinance_Signup,dbo.Ecorenew_CorporateFinance_FinanceOption,dbo.Ecorenew_CorporateFinance_OrderItem,dbo.Ecorenew_CorporateFinance_Signup,dbo.Ecorenew_EmploymentStatus,dbo.Ecorenew_ExpenditureCategory,dbo.Ecorenew_ExpenditureCategoryRange,dbo.Ecorenew_ExpenditureRange,dbo.Ecorenew_HomeOwnerStatus,dbo.Ecorenew_MaritalStatus,dbo.Ecorenew_NumberOfDependents,dbo.Ecorenew_ShoppingCartItemConsumerFinanceOption,dbo.Ecorenew_ShoppingCartItemCorporateFinanceOption,dbo.Ecorenew_SkuCorporateFinanceRv',12,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('EmailAccount','dbo.EmailAccount',13,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('FNS','dbo.FNS_eBayEPID,dbo.FNS_eBayShipment,dbo.FNS_FeedASIN,dbo.FNS_FeedCategory,dbo.FNS_FeedCondition,dbo.FNS_FeedEPID,dbo.FNS_FeedProduct,dbo.FNS_FeedRec,dbo.FNS_MetaTag,dbo.FNS_MetaTagCategory,dbo.FNS_MetaTagCondition,dbo.FNS_MetaTagManufacturer,dbo.FNS_MetaTagProduct,dbo.FNS_TrackingCode,dbo.FNS_Url404Error,dbo.FNS_UrlRewriteRecord',14,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('GiftCard','dbo.GiftCard',15,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('GoogleProduct','dbo.GoogleProduct',16,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Identity','dbo.IdentityClaims,dbo.IdentityResources',17,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Manufacturer','dbo.Manufacturer,dbo.ManufacturerTemplate',19,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('MazumaWiFiVoucher','dbo.MazumaWiFiVoucher,dbo.MazumaWiFiVoucherUploadBatch',20,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Measure','dbo.MeasureDimension,dbo.MeasureWeight,dbo.MessageTemplate',21,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('ReturnRequest','dbo.ReturnRequestAction,dbo.ReturnRequestReason',22,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('ScheduleTask','dbo.ScheduleTask',23,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Setting','dbo.Setting',24,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('SevenSpike','dbo.SS_A_Attachment,dbo.SS_A_AttachmentCategory,dbo.SS_A_AttachmentDownload,dbo.SS_A_FileExtensionImage,dbo.SS_AS_AnywhereSlider,dbo.SS_AS_SliderImage,dbo.SS_C_Condition,dbo.SS_C_ConditionGroup,dbo.SS_C_ConditionStatement,dbo.SS_C_EntityCondition,dbo.SS_C_ProductOverride,dbo.SS_CR_Reminder,dbo.SS_ES_EntitySetting,dbo.SS_HD_StaffToDepartment,dbo.SS_HD_TicketAttribute,dbo.SS_HD_TicketAttributeValue,dbo.SS_HD_TicketDepartment,dbo.SS_HD_TicketNote,dbo.SS_HD_TicketToAttributeValue,dbo.SS_HD_TicketToProduct,dbo.SS_HW_HtmlWidget,dbo.SS_JC_JCarousel,dbo.SS_KB_Article,dbo.SS_KB_ArticleComment,dbo.SS_KB_ArticleRevision,dbo.SS_KB_ArticleToCategory,dbo.SS_KB_Category,dbo.SS_KB_RelatedArticle,dbo.SS_MAP_EntityMapping,dbo.SS_MAP_EntityWidgetMapping,dbo.SS_MM_Menu,dbo.SS_MM_MenuItem,dbo.SS_PR_CategoryPageRibbon,dbo.SS_PR_ProductPageRibbon,dbo.SS_PR_ProductRibbon,dbo.SS_PR_RibbonPicture,dbo.SS_QT_Tab,dbo.SS_RB_Category,dbo.SS_RB_Post,dbo.SS_RB_RelatedBlog,dbo.SS_RB_RichBlogPostCategoryMapping,dbo.SS_S_Schedule,dbo.SS_SC_SaleCampaign,dbo.SS_SC_SaleCampaignFailedRecords,dbo.SS_SC_SaleCampaignHistory,dbo.SS_SF_NopSalesForceProduct,dbo.SS_SL_Shop,dbo.SS_SL_ShopImage,dbo.SS_SNF_SocialFeed,dbo.SS_SNF_SocialNetwork,dbo.SS_SOTD_SaleOfTheDayOffer,dbo.SS_SPC_ProductsGroup,dbo.SS_SPC_ProductsGroupItem,dbo.SS_SS_SeoTemplate',25,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Tax','dbo.TaxCategory,dbo.TaxRate',27,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Topic','dbo.Topic,dbo.TopicTemplate',28,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('UrlRecord','dbo.UrlRecord',29,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Warehourse','dbo.Warehouse',30,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('WebHooks','WebHooks.WebHooks,WebHooks.[__MigrationHistory]',31,0);
insert into dbo.SyncGroupInfo(GroupName,TableNames,DisplayOrder,RequireSync)
values('Synchronization','dbo.Site,dbo.SyncGroupInfo',32,1);



insert into dbo.LocaleStringResource(LanguageId,ResourceName,ResourceValue)
values(1,'Admin.System.SynchronizeDatabase','Synchronization');


insert into dbo.ScheduleTask(Name,Seconds,Type,Enabled,StopOnError)
values('Auto synchronize database',600,'Nop.Services.MultiDb.AutoSynchronizeDatabaseTask, Nop.Services',1,0);