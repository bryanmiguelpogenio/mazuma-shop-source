

INSERT INTO [dbo].[EnquiryTicketReason]([Reason],[IsActive]) VALUES ('Where is my order?', 1)
INSERT INTO [dbo].[EnquiryTicketReason]([Reason],[IsActive]) VALUES ('Failed Payment', 1)
INSERT INTO [dbo].[EnquiryTicketReason]([Reason],[IsActive]) VALUES ('My order has some quality issue', 1)
INSERT INTO [dbo].[EnquiryTicketReason]([Reason],[IsActive]) VALUES ('The tracking is correct, but the shipment has not yet arrived', 1)
INSERT INTO [dbo].[EnquiryTicketReason]([Reason],[IsActive]) VALUES ('Other order related issues', 1)


