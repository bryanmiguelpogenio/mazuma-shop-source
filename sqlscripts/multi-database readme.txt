1, Publish web site for each country.
1.1, We publish one sub-site for each country
1.2, We treat the English site as the main site, all other sub-sites' data will be synchronized from the main site's database
1.3, We publish one site as the main backend system, and the main backend system contains no database at all, it's dataSetting.json file only contains the database connection information for the English site.
2, Configurate the sub-site's database:
2.1, Execute the .sql script with correct information.
2.2, Add linked server for the main database
3, Change the datasettings.json file for the sub-site
4, Modify the data saved in Site table of main site's database
5, Configurate the sub-site in its administrator area such as store, language, currency, shipping method and so on. After these configuration, do not do anything in the administrator area any more, use the backend web site instead.
6, Change the database connection information from the drop down on the backend web site to view all sub-site's sell data.

7, Synchronize data from the main site's administrator area
7.1, System --> Synchronization, in this page, we can configurate the tables that should be synchronized, only the checked group's tables will be synchronized, the unchecked tables and those have not shown to user will be not synchronized.
7.2, The synchronization of Product group will take a long time to finish the work, the tables will be locked between the synchronization and any other operation such as searching product or checkout will be timeout. So if you have not modified the Product or Product related data, do not check the Product group. In our 
7.3, Users can synchronize data mannually by clicking the menu item below the "Clear cache" and "Restart application" when the backend site is connecting to the English site. And the synchronization operation will be executed by a schedule task named "Auto synchronize database".

(We don't synchronize these data: store, language, currency, customer, order, shipping method and some related data).