

CREATE TABLE [dbo].[EnquiryReplyRecord](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EnquiryRequestId] [int] NOT NULL,
	[IsFromClient] [bit] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[CreatorUserId] [int] NULL,
	[CreatedOnUtc] [datetime] NOT NULL,
 CONSTRAINT [PK_EnquiryReplyRecord] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



ALTER TABLE [dbo].[EnquiryReplyRecord]  WITH NOCHECK ADD  CONSTRAINT [FK_EnquiryReplyRecord_Customer] FOREIGN KEY([CreatorUserId])
REFERENCES [dbo].[Customer] ([Id])


ALTER TABLE [dbo].[EnquiryReplyRecord] NOCHECK CONSTRAINT [FK_EnquiryReplyRecord_Customer]


ALTER TABLE [dbo].[EnquiryReplyRecord]  WITH CHECK ADD  CONSTRAINT [FK_EnquiryReplyRecord_EnquiryRequest] FOREIGN KEY([EnquiryRequestId])
REFERENCES [dbo].[EnquiryRequest] ([Id])
ON DELETE CASCADE


ALTER TABLE [dbo].[EnquiryReplyRecord] CHECK CONSTRAINT [FK_EnquiryReplyRecord_EnquiryRequest]



