

CREATE TABLE [dbo].[EnquiryRequest](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RequestNumber] [nvarchar](20) NULL,
	[RequestStatusId] [int] NOT NULL,
	[CustomerId] [int] NULL,
	[OrderId] [int] NULL,
	[OrderNumber] [nvarchar](20) NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[UserEmail] [nvarchar](100) NOT NULL,
	[EnquiryTicketReasonId] [int] NOT NULL,
	[Enquiry] [nvarchar](2000) NOT NULL,
	[CreatedOnUtc] [datetime] NOT NULL,
	[LastUpdatedOnUtc] [datetime] NOT NULL,
 CONSTRAINT [PK_EnquiryRequest] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[EnquiryRequest]  WITH NOCHECK ADD  CONSTRAINT [FK_EnquiryRequest_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
ON DELETE CASCADE


ALTER TABLE [dbo].[EnquiryRequest] NOCHECK CONSTRAINT [FK_EnquiryRequest_Customer]


ALTER TABLE [dbo].[EnquiryRequest]  WITH CHECK ADD  CONSTRAINT [FK_EnquiryRequest_EnquiryTicketReason] FOREIGN KEY([EnquiryTicketReasonId])
REFERENCES [dbo].[EnquiryTicketReason] ([Id])
ON DELETE CASCADE


ALTER TABLE [dbo].[EnquiryRequest] CHECK CONSTRAINT [FK_EnquiryRequest_EnquiryTicketReason]


ALTER TABLE [dbo].[EnquiryRequest]  WITH NOCHECK ADD  CONSTRAINT [FK_EnquiryRequest_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])


ALTER TABLE [dbo].[EnquiryRequest] NOCHECK CONSTRAINT [FK_EnquiryRequest_Order]



