﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Enquiry;
using Nop.Core.Domain.Localization;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Enquiry;
using Nop.Services.Events;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Factories;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Models.EnquiryRequest;

namespace Nop.Web.Controllers
{
    [HttpsRequirement(SslRequirement.No)]
    public partial class EnquiryRequestController : BasePublicController
    {
        #region Fields
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly IProductService _productService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IWebHelper _webHelper;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IPermissionService _permissionService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IEventPublisher _eventPublisher;
        private readonly IManufacturerService _manufacturerService;
        private readonly ICurrencyService _currencyService;
        private readonly IEnquiryRequestService _enquiryRequestService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly LocalizationSettings _localizationSettings;
        private readonly CaptchaSettings _captchaSettings;
        private readonly CurrencySettings _currencySettings;
        private readonly CommonSettings _commonSettings;

        #endregion

        #region Ctor

        public EnquiryRequestController(ICatalogModelFactory catalogModelFactory,
            IProductService productService,
            IWorkContext workContext,
            IStoreContext storeContext,
            ILocalizationService localizationService,
            IWorkflowMessageService workflowMessageService,
            IWebHelper webHelper,
            ICustomerActivityService customerActivityService,
            IStoreMappingService storeMappingService,
            IPermissionService permissionService,
            IProductModelFactory productModelFactory,
            IEventPublisher eventPublisher,
            IManufacturerService manufacturerService,
            ICurrencyService currencyService,
            IEnquiryRequestService enquiryRequestService,
            IDateTimeHelper dateTimeHelper,
            LocalizationSettings localizationSettings,
            CaptchaSettings captchaSettings,
            CurrencySettings currencySettings,
            CommonSettings commonSettings)
        {
            _catalogModelFactory = catalogModelFactory;
            _productService = productService;
            _workContext = workContext;
            _storeContext = storeContext;
            _localizationService = localizationService;
            _workflowMessageService = workflowMessageService;
            _webHelper = webHelper;
            _customerActivityService = customerActivityService;
            _storeMappingService = storeMappingService;
            _permissionService = permissionService;
            _productModelFactory = productModelFactory;
            _eventPublisher = eventPublisher;
            _manufacturerService = manufacturerService;
            _currencyService = currencyService;
            _enquiryRequestService = enquiryRequestService;
            _dateTimeHelper = dateTimeHelper;

            _localizationSettings = localizationSettings;
            _captchaSettings = captchaSettings;
            _currencySettings = currencySettings;
            _commonSettings = commonSettings;
        }

        #endregion

        //Enquiry pequest history page
        [HttpsRequirement(SslRequirement.Yes)]
        [CheckAccessClosedStore(true)]
        public virtual IActionResult History()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var list = _enquiryRequestService.GetListByPage(_workContext.CurrentCustomer.Id);
            var model = new EnquiryRequestListModel();
            if (list == null || !list.Any())
                return View(model);
            model.Items = list.Select(x => new EnquiryRequestItemModel
            {
                Id = x.Id,
                Status = x.RequestStatus,
                RequestNumber = x.RequestNumber,
                TicketReason = x.EnquiryTicketReason.Reason,
                ShortEnquiry = string.Join("", x.Enquiry.Take(50)) + "...",
                CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc),
                LastUpdatedOn = _dateTimeHelper.ConvertToUserTime(x.LastUpdatedOnUtc, DateTimeKind.Utc)
            }).ToList();
            return View(model);
        }


        //Enquiry pequest detail page
        [HttpsRequirement(SslRequirement.Yes)]
        [CheckAccessClosedStore(true)]
        public virtual IActionResult Detail(int id)
        {
            if (id <= 0)
                return NotFound();
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var entity = _enquiryRequestService.GetById(id);
            if (entity == null)
                return NotFound();
            var model = new EnquiryRequestModel()
            {
                Id = entity.Id,
                RequestNumber = entity.RequestNumber,
                UserName = entity.UserName,
                UserEmail = entity.UserEmail,
                OrderNumber = entity.OrderNumber,
                TicketReason = entity.EnquiryTicketReason.Reason,
                Enquiry = entity.Enquiry,
                CreatedOn = _dateTimeHelper.ConvertToUserTime(entity.CreatedOnUtc, DateTimeKind.Utc),
                LastUpdatedOn = _dateTimeHelper.ConvertToUserTime(entity.LastUpdatedOnUtc, DateTimeKind.Utc),
                ReplyRecords = entity.EnquiryReplyRecords.Select(x => new EnquiryReplyRecordModel
                {
                    Id = x.Id,
                    Message = x.Message,
                    IsFromClient = x.IsFromClient,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc)
                }).ToList()
            };
            return View(model);
        }

        [HttpPost, ActionName("Detail")]
        [PublicAntiForgery]
        [ValidateCaptcha]
        //available even when a store is closed
        [CheckAccessClosedStore(true)]
        public virtual IActionResult ResubmitEnquiryRequest(EnquiryRequestModel model)
        {
            //validate CAPTCHA
            if (_captchaSettings.Enabled)
            {
                ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));
            }

            if (ModelState.IsValid)
            {
                var entity = new EnquiryReplyRecord()
                {
                    EnquiryRequestId = model.Id,
                    Message = model.ReplyMessage,
                    CreatedOnUtc = DateTime.UtcNow,
                    CreatorUserId = _workContext.CurrentCustomer.Id,
                    IsFromClient = true
                };
                //insert entity
                _enquiryRequestService.InsertReplyRecord(entity);

                var enquiryRequest = _enquiryRequestService.GetById(model.Id);

                //activity log
                _customerActivityService.InsertActivity("PublicStore.EnquiryRequest", _localizationService.GetResource("ActivityLog.PublicStore.EnquiryRequest.CustomerResubmit"), entity.Id, enquiryRequest.Id);
                
                //Sent email to store owners
                var ticketReason = _enquiryRequestService.GetAllEnquiryTicketReasons()
                    .Where(x => x.Id == enquiryRequest.EnquiryTicketReasonId)
                    .Select(x => x.Reason)
                    .FirstOrDefault();
                _workflowMessageService.SendEnquiryRequestCustomerRepliedMessage(_workContext.WorkingLanguage.Id, enquiryRequest, ticketReason);

                return RedirectToAction("Detail", new { id = model.Id });
            }

            return View(model);
        }


        //Enquiry pequest page
        [HttpsRequirement(SslRequirement.Yes)]
        //available even when a store is closed
        [CheckAccessClosedStore(true)]
        public virtual IActionResult Submit()
        {
            var model = new EnquiryRequestBaseModel()
            {
                UserEmail = _workContext.CurrentCustomer.Email,
                UserName = _workContext.CurrentCustomer.GetFullName(),
                EnquiryTicketReasons = _enquiryRequestService.GetAllEnquiryTicketReasons()
            };
            return View(model);
        }

        [HttpPost, ActionName("Submit")]
        [PublicAntiForgery]
        [ValidateCaptcha]
        //available even when a store is closed
        [CheckAccessClosedStore(true)]
        public virtual IActionResult SubmitEnquiryRequest(EnquiryRequestBaseModel model)
        {
            //validate CAPTCHA
            if (_captchaSettings.Enabled)
            {
                ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));
            }

            if (ModelState.IsValid)
            {
                var entity = new EnquiryRequest()
                {
                    OrderNumber = model.OrderNumber,
                    UserName = model.UserName,
                    UserEmail = model.UserEmail.Trim(),
                    EnquiryTicketReasonId = model.EnquiryTicketReasonId,
                    Enquiry = model.Enquiry
                };
                if(_workContext.CurrentCustomer.IsRegistered())
                {
                    entity.CustomerId = _workContext.CurrentCustomer.Id;
                }
                //insert entity
                _enquiryRequestService.Insert(entity);

                //activity log
                _customerActivityService.InsertActivity("PublicStore.EnquiryRequest", _localizationService.GetResource("ActivityLog.PublicStore.EnquiryRequest.Submit"), entity.Id);

                //Sent email to store owners
                var ticketReason = _enquiryRequestService.GetAllEnquiryTicketReasons()
                    .Where(x => x.Id == entity.EnquiryTicketReasonId)
                    .Select(x => x.Reason)
                    .FirstOrDefault();
                _workflowMessageService.SendEnquiryRequestMessage(_workContext.WorkingLanguage.Id, entity, ticketReason);

                model.SuccessfullySent = true;
                model.Result = _localizationService.GetResource("EnquiryRequest.YourEnquiryHasBeenSent");

                return View(model);
            }

            return View(model);
        }
    }
}
