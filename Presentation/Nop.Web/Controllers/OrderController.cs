﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Bank;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.Encryption;
using Nop.Services.Bank;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Web.Factories;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Order;

namespace Nop.Web.Controllers
{
    public partial class OrderController : BasePublicController
    {
        #region Fields
        private readonly LocalizationSettings _localizationSettings;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IOrderModelFactory _orderModelFactory;
        private readonly IOrderService _orderService;
        private readonly IShipmentService _shipmentService;
        private readonly IWorkContext _workContext;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IPaymentService _paymentService;
        private readonly ITransferRecordService _transferRecordService;
        private readonly IPdfService _pdfService;
        private readonly IWebHelper _webHelper;
        private readonly RewardPointsSettings _rewardPointsSettings;

        #endregion

		#region Ctor

        public OrderController(IOrderModelFactory orderModelFactory,
            IOrderService orderService, 
            IShipmentService shipmentService, 
            IWorkContext workContext,
            IOrderProcessingService orderProcessingService, 
            IPaymentService paymentService,
            ITransferRecordService transferRecordService,
            IPdfService pdfService, 
            IWebHelper webHelper, 
            IWorkflowMessageService workflowMessageService,
            RewardPointsSettings rewardPointsSettings,
            LocalizationSettings localizationSettings)
        {
            this._localizationSettings = localizationSettings;
            this._workflowMessageService = workflowMessageService;
            this._orderModelFactory = orderModelFactory;
            this._orderService = orderService;
            this._shipmentService = shipmentService;
            this._workContext = workContext;
            this._orderProcessingService = orderProcessingService;
            this._paymentService = paymentService;
            this._transferRecordService = transferRecordService;
            this._pdfService = pdfService;
            this._webHelper = webHelper;
            this._rewardPointsSettings = rewardPointsSettings;
        }

        #endregion

        #region Methods

        //My account / Orders
        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult CustomerOrders()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            var model = _orderModelFactory.PrepareCustomerOrderListModel();
            return View(model);
        }

        //My account / Orders / Cancel recurring order
        [HttpPost, ActionName("CustomerOrders")]
        [PublicAntiForgery]
        [FormValueRequired(FormValueRequirement.StartsWith, "cancelRecurringPayment")]
        public virtual IActionResult CancelRecurringPayment(IFormCollection form)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            //get recurring payment identifier
            var recurringPaymentId = 0;
            foreach (var formValue in form.Keys)
                if (formValue.StartsWith("cancelRecurringPayment", StringComparison.InvariantCultureIgnoreCase))
                    recurringPaymentId = Convert.ToInt32(formValue.Substring("cancelRecurringPayment".Length));

            var recurringPayment = _orderService.GetRecurringPaymentById(recurringPaymentId);
            if (recurringPayment == null)
            {
                return RedirectToRoute("CustomerOrders");
            }

            if (_orderProcessingService.CanCancelRecurringPayment(_workContext.CurrentCustomer, recurringPayment))
            {
                var errors = _orderProcessingService.CancelRecurringPayment(recurringPayment);

                var model = _orderModelFactory.PrepareCustomerOrderListModel();
                model.RecurringPaymentErrors = errors;

                return View(model);
            }

            return RedirectToRoute("CustomerOrders");
        }

        //My account / Orders / Retry last recurring order
        [HttpPost, ActionName("CustomerOrders")]
        [PublicAntiForgery]
        [FormValueRequired(FormValueRequirement.StartsWith, "retryLastPayment")]
        public virtual IActionResult RetryLastRecurringPayment(IFormCollection form)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            //get recurring payment identifier
            var recurringPaymentId = 0;
            if (!form.Keys.Any(formValue => formValue.StartsWith("retryLastPayment", StringComparison.InvariantCultureIgnoreCase) &&
                int.TryParse(formValue.Substring(formValue.IndexOf('_') + 1), out recurringPaymentId)))
            {
                return RedirectToRoute("CustomerOrders");
            }

            var recurringPayment = _orderService.GetRecurringPaymentById(recurringPaymentId);
            if (recurringPayment == null)
                return RedirectToRoute("CustomerOrders");

            if (!_orderProcessingService.CanRetryLastRecurringPayment(_workContext.CurrentCustomer, recurringPayment))
                return RedirectToRoute("CustomerOrders");

            var errors = _orderProcessingService.ProcessNextRecurringPayment(recurringPayment);
            var model = _orderModelFactory.PrepareCustomerOrderListModel();
            model.RecurringPaymentErrors = errors.ToList();

            return View(model);
        }

        //My account / Reward points
        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult CustomerRewardPoints(int? pageNumber)
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return Challenge();

            if (!_rewardPointsSettings.Enabled)
                return RedirectToRoute("CustomerInfo");

            var model = _orderModelFactory.PrepareCustomerRewardPoints(pageNumber);
            return View(model);
        }

        //My account / Order details page
        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult Details(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            var model = _orderModelFactory.PrepareOrderDetailsModel(order);
            return View(model);
        }

        //My account / Order details page / Print
        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult PrintOrderDetails(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            var model = _orderModelFactory.PrepareOrderDetailsModel(order);
            model.PrintMode = true;

            return View("Details", model);
        }

        //My account / Order details page / PDF invoice
        public virtual IActionResult GetPdfInvoice(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            var orders = new List<Order>();
            orders.Add(order);
            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, MimeTypes.ApplicationPdf, $"order_{order.Id}.pdf");
        }

        //My account / Order details page / re-order
        public virtual IActionResult ReOrder(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            _orderProcessingService.ReOrder(order);
            return RedirectToRoute("ShoppingCart");
        }

        //My account / Order details page / Complete payment
        [HttpPost, ActionName("Details")]
        [PublicAntiForgery]
        [FormValueRequired("repost-payment")]
        public virtual IActionResult RePostPayment(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            if (!_paymentService.CanRePostProcessPayment(order))
                return RedirectToRoute("OrderDetails", new { orderId = orderId });

            var postProcessPaymentRequest = new PostProcessPaymentRequest
            {
                Order = order
            };
            _paymentService.PostProcessPayment(postProcessPaymentRequest);

            if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
            {
                //redirection or POST has been done in PostProcessPayment
                return Content("Redirected");
            }

            //if no redirection has been done (to a third-party payment page)
            //theoretically it's not possible
            return RedirectToRoute("OrderDetails", new { orderId = orderId });
        }

        public virtual IActionResult BankTransferInfo(string token, string date)
        {
            if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(date))
            {
                return NotFound();
            }
            var encryptionProvider = new EncryptionProvider(EncryptionKeys.Key_BankTransfer);
            //Decrypt
            var guid = encryptionProvider.DecryptFromBase64(token);
            var deadline = encryptionProvider.DecryptFromBase64(date);
            var deadlineDate = DateTime.MinValue;
            DateTime.TryParse(deadline, out deadlineDate);
            if(deadlineDate.AddDays(2) < DateTime.Now)
            {
                return NotFound();
            }
            var orderGuid = Guid.Empty;
            Guid.TryParse(guid, out orderGuid);
            if (orderGuid == Guid.Empty)
            {
                return NotFound();
            }
            var order = _orderService.GetOrderByGuid(orderGuid);
            if (order == null || order.Deleted)
                return NotFound();

            var model = _orderModelFactory.PrepareOrderDetailsModel(order);
            return View(model);
        }

        //My account / Order details page / Complete payment
        [HttpPost, ActionName("Details")]
        [PublicAntiForgery]
        [FormValueRequired("transactionRefrenceNO")]
        public virtual JsonResult SubmitTransactionRefrenceNO(TransactionRefrenceModel model)
        {
            var order = _orderService.GetOrderById(model.OrderId);
            if (order == null || order.Deleted || order.CustomerId != _workContext.CurrentCustomer.Id)
                throw new Exception("Order is not exist！");
            if (order == null || order.Deleted || order.CustomerId != _workContext.CurrentCustomer.Id
                || order.PaymentMethodSystemName != SystemCustomerNames.PaymentMethod_BankTranfer
                || order.TransferRecords.Any()
                || order.PaymentStatusId != (int)PaymentStatus.Pending
                || order.OrderStatusId != (int)OrderStatus.Pending)
            {
                throw new Exception("Order is in processing！");
            }
            _transferRecordService.InsertTransferRecord(new Core.Domain.Bank.TransferRecord()
            {
                OrderId = model.OrderId,
                TransferStatusId = TransferStatus.Pending.GetHashCode(),
                FromAccount = model.BankName,
                FromCardCode = model.AccountNO,
                SerialNumber = model.TransactionRefrenceNO,
                TransferDate = model.DepositedOn,
                CreatedUTC = DateTime.UtcNow
            });
            //Add order note
            order.OrderNotes.Add(new OrderNote
            {
                Note = "Customer submited the bank transfer record. Account NO: " + model.AccountNO,
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            if (order.PaymentMethodSystemName.Equals(SystemCustomerNames.PaymentMethod_BankTranfer))
            {
                //send email notifications to store owners
                var orderPlacedStoreOwnerNotificationQueuedEmailId = _workflowMessageService.SendTransferOrderPlacedStoreOwnerNotification(order, _localizationSettings.DefaultAdminLanguageId);
            }

            return new JsonResult("Success");
        }

        //My account / Order details page / Shipment details page
        [HttpsRequirement(SslRequirement.Yes)]
        public virtual IActionResult ShipmentDetails(int shipmentId)
        {
            var shipment = _shipmentService.GetShipmentById(shipmentId);
            if (shipment == null)
                return Challenge();

            var order = shipment.Order;
            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return Challenge();

            var model = _orderModelFactory.PrepareShipmentDetailsModel(shipment);
            return View(model);
        }
        #endregion


        #region Order reviews

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult OrderReviews(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            if (order == null || order.Deleted || order.CustomerId != _workContext.CurrentCustomer.Id)
                return Challenge();

            OrderReviewsModel model = new OrderReviewsModel();

            var _orderModelFactory = EngineContext.Current.Resolve<IOrderModelFactory>();
            //default value
            _orderModelFactory.CustomerOrderCommentModel(orderId, model);

            return View(model);
        }

        [HttpPost]
        public virtual ActionResult OrderReviews(OrderReviewsModel model)
        {
            var order = _orderService.GetOrderById(model.OrderId);
            if (order == null || order.Deleted || order.CustomerId != _workContext.CurrentCustomer.Id)
                return Challenge();

            if (!_workContext.CurrentCustomer.IsRegistered())
                ModelState.AddModelError("Register", "Order can be reviewed only after register user！");

            if (model.Rating <= 0 || model.Rating > 5)
            {
                ModelState.AddModelError("Rating", "Please select rating！");
                _orderModelFactory.CustomerOrderCommentModel(model.OrderId, model);
                return View(model);
            }
            if (string.IsNullOrEmpty(model.ReviewText))
            {
                ModelState.AddModelError("ReviewText", "Please input review text！");
                _orderModelFactory.CustomerOrderCommentModel(model.OrderId, model);
                return View(model);
            }
            var _catalogSettings = EngineContext.Current.Resolve<CatalogSettings>();
            var _storeContext = EngineContext.Current.Resolve<IStoreContext>();
            var _productAttributeFormatter = EngineContext.Current.Resolve<IProductAttributeFormatter>();
            var _productService = EngineContext.Current.Resolve<IProductService>();
            foreach (var orderItem in order.OrderItems)
            {
                var product = orderItem.Product;
                //save review
                var rating = model.Rating;
                var isApproved = !_catalogSettings.ProductReviewsMustBeApproved;

                var productReview = new ProductReview
                {
                    OrderId = model.OrderId,
                    ProductId = product.Id,
                    CustomerId = _workContext.CurrentCustomer.Id,
                    ProductAttributeInfo = _productAttributeFormatter.FormatAttributes(orderItem.Product, orderItem.AttributesXml),
                    Title =model.Title,
                    ReviewText = model.ReviewText,
                    Rating = rating,
                    HelpfulYesTotal = 0,
                    HelpfulNoTotal = 0,
                    IsApproved = isApproved,
                    CreatedOnUtc = DateTime.UtcNow,
                    StoreId = _storeContext.CurrentStore.Id,
                };
                product.ProductReviews.Add(productReview);
                _productService.UpdateProduct(product);

                //update product totals
                _productService.UpdateProductReviewTotals(product);
              
            }
            return Redirect("/order/history");
        }
        #endregion

        #region Check Order Status

        public virtual IActionResult CheckOrderStatus()
        {
            var model = new CheckOrderStatusModel();
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult CheckOrderStatus(CheckOrderStatusModel model)
        {
            var order = _orderService.GetOrderByCustomOrderNumber(model.CustomOrderNumber);
            var localizationService = EngineContext.Current.Resolve<ILocalizationService>();
            if (order == null || order.Deleted)
            {
                ModelState.AddModelError("", localizationService.GetResource("Order.CheckOrderStatus.CustomOrderNumber.NotFound"));
            }
            else if (order.Customer.Email != model.Email)
            {
                ModelState.AddModelError("", localizationService.GetResource("Order.CheckOrderStatus.CustomOrderNumber.NotMatch"));
            }
            if(ModelState.IsValid)
            {
                return RedirectToAction("CheckOrderStatusDetails", new { orderGuid = order.OrderGuid });
            }
            else
            {
                return View(model);
            }
        }

        public virtual IActionResult CheckOrderStatusDetails(Guid orderGuid)
        {
            var order = _orderService.GetOrderByGuid(orderGuid);
            if (order == null || order.Deleted)
                return Challenge();

            var model = _orderModelFactory.PrepareOrderDetailsModel(order);
            return View(model);
        }

        #endregion

    }
}