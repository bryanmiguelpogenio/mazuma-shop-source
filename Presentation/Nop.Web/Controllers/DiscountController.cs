﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Localization;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Factories;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Models.Discounts;

namespace Nop.Web.Controllers
{
    [HttpsRequirement(SslRequirement.No)]
    public partial class DiscountController : BasePublicController
    {
        #region Fields
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly IProductService _productService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IWebHelper _webHelper;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IPermissionService _permissionService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IEventPublisher _eventPublisher;
        private readonly IManufacturerService _manufacturerService;
        private readonly ICurrencyService _currencyService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly CaptchaSettings _captchaSettings;
        private readonly CurrencySettings _currencySettings;

        #endregion

        #region Ctor

        public DiscountController(ICatalogModelFactory catalogModelFactory,
            IProductService productService,
            IWorkContext workContext,
            IStoreContext storeContext,
            ILocalizationService localizationService,
            IWorkflowMessageService workflowMessageService,
            IWebHelper webHelper,
            ICustomerActivityService customerActivityService,
            IStoreMappingService storeMappingService,
            IPermissionService permissionService,
            IProductModelFactory productModelFactory,
            IEventPublisher eventPublisher,
            IManufacturerService manufacturerService,
            ICurrencyService currencyService,

        LocalizationSettings localizationSettings,
            CaptchaSettings captchaSettings,
            CurrencySettings currencySettings)
        {
            this._catalogModelFactory = catalogModelFactory;
            this._productService = productService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._localizationService = localizationService;
            this._workflowMessageService = workflowMessageService;
            this._webHelper = webHelper;
            this._customerActivityService = customerActivityService;
            this._storeMappingService = storeMappingService;
            this._permissionService = permissionService;
            this._productModelFactory = productModelFactory;
            this._eventPublisher = eventPublisher;
            this._manufacturerService = manufacturerService;
            this._currencyService = currencyService;

            this._localizationSettings = localizationSettings;
            this._captchaSettings = captchaSettings;
            this._currencySettings = currencySettings;
        }
        
        #endregion
        
        #region Methods

        public virtual IActionResult Index()
        {
            var model = new DiscountIndexModel();
            var manufacturers = _manufacturerService.GetAllManufacturers();
            var priceRange = _productService.GetDiscountProductPriceRange();
            if(priceRange != null && priceRange.Any())
            {
                model.MinPrice = priceRange.First();
                model.MaxPrice = priceRange.Last();
            }
            var primaryCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            if(primaryCurrency != null)
            {
                //currency char
                var currencySymbol = "";
                if (!string.IsNullOrEmpty(primaryCurrency.DisplayLocale))
                    currencySymbol = new RegionInfo(primaryCurrency.DisplayLocale).CurrencySymbol;
                else
                    currencySymbol = primaryCurrency.CurrencyCode;
                model.CurrencySymbol = currencySymbol;
            }
            model.AvailableManufacturers = manufacturers.Select(x => new KeyValuePair<int, string>(x.Id, x.Name)).ToList();
            var categoryModel = _catalogModelFactory.PrepareCategoryNavigationModel(0, 0);
            if(categoryModel != null && categoryModel.Categories != null)
            {
                model.Categories = categoryModel.Categories;
            }
            return View(model);
        }

        [HttpPost]
        public virtual JsonResult OpcLoadProductsGridMethod(DiscountPagingFilteringModel command)
        {
            var products = _productService.GetPagedDiscountProducts(command.CategoryIds, command.ManufacturerIds, command.PriceFrom, command.PriceTo, command.PageIndex, command.PageSize,command.OrderBy);
            if (products.Any())
            {
                var model = new DicountProductsModel();
                model.PagingFilteringContext.LoadPagedList(products);
                model.Products = _productModelFactory.PrepareProductOverviewModels(products).ToList();
                return Json(new
                {
                    update_section = RenderPartialViewToString("ProductsGrid", model)
                });
            } else
            {
                return Json(new
                {
                    update_section = "<p style=\"margin-top:50px;text-align: center;\">NO RESULTS FOUND</p>"
                });
            }
        }

        #endregion
    }
}
