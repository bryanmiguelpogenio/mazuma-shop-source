﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Data;
using Nop.Services.MultiDb;
using Nop.Web.Areas.Admin.Extensions;
using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Areas.Admin.Models.MultiDb;
using Nop.Web.Framework.Components;

namespace Nop.Web.Areas.Admin.Components
{
    public class AdminSiteSelectorViewComponent : NopViewComponent
    {
        private readonly ISiteService _siteService;
        private readonly string _currentDbConn;

        public AdminSiteSelectorViewComponent(ISiteService siteService, DataSettings dataSettings)
        {
            _siteService = siteService;
            _currentDbConn = dataSettings.DataConnectionString;
        }

        public IViewComponentResult Invoke()
        {
            SiteSelectorModel model = new SiteSelectorModel();
            model.AvailableSites = _siteService.GetAllSites().Select(x=> {
                return new SiteModel() {
                    Id=x.Id,
                    Name=x.Name,
                    WebUrl=x.WebUrl,
                    DbConnectionString=x.DbConn,
                    DisplayOrder=x.DisplayOrder
                };
            }).ToList();
            if (model.AvailableSites != null && model.AvailableSites.Count > 0)
            {
                foreach (var site in model.AvailableSites)
                {
                    if (string.Equals(site.DbConnectionString, _currentDbConn, System.StringComparison.CurrentCultureIgnoreCase))
                    {
                        model.CurrentSite = site;
                        break;
                    }
                }
            }
            
            return View(model);
        }
    }
}
