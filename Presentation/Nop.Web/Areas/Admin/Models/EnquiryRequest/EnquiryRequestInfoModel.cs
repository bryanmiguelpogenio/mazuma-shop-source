﻿using FluentValidation.Attributes;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Models.EnquiryRequest;
using Nop.Web.Validators.EnquiryRequest;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.EnquiryRequest
{
    [Validator(typeof(EnquiryRequestInfoValidator))]
    public class EnquiryRequestInfoModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.EnquiryRequest.CustomerId")]
        public int? CustomerId { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.RequestNumber")]
        public string RequestNumber { get; set; }

        public int? OrderId { get; set; }
        [NopResourceDisplayName("Admin.EnquiryRequest.OrderNumber")]
        public string OrderNumber { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.UserName")]
        public string UserName { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.UserEmail")]
        public string UserEmail { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.TicketReason")]
        public string TicketReason { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.Enquiry")]
        public string Enquiry { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.CreatedOnUtc")]
        public DateTime CreatedOnUtc { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.ReplyRecords")]
        public IList<EnquiryReplyRecordModel> ReplyRecords { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.ReplyMessage")]
        public string ReplyMessage { get; set; }
    }
}
