﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Enquiry;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Areas.Admin.Models.EnquiryRequest
{
    public class EnquiryRequestFilterModel
    {
        public EnquiryRequestFilterModel() {
            AvailableRequestStatus = new List<SelectListItem>();
            EnquiryTicketReasons = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Admin.EnquiryRequest.CustomerId")]
        public int CustomerId { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.RequestNumber")]
        public string RequestNumber { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.OrderNumber")]
        public string OrderNumber { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.UserEmail")]
        public string UserEmail { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.RequestStatus")]
        public string RequestStatusId { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.TicketReason")]
        public int EnquiryTicketReasonId { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.CreatedFrom")]
        [UIHint("DateNullable")]
        public DateTime? CreatedFrom { get; set; }

        [NopResourceDisplayName("Admin.EnquiryRequest.CreatedTo")]
        [UIHint("DateNullable")]
        public DateTime? CreatedTo { get; set; }

        public IList<SelectListItem> AvailableRequestStatus { get; set; }
        public IList<SelectListItem> EnquiryTicketReasons { get; set; }
    }
}
