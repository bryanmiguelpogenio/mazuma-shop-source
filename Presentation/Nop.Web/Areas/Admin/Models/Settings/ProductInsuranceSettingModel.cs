﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;
using System;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Settings
{
    public partial class ProductInsuranceSettingModel:BaseNopEntityModel
    {
        public ProductInsuranceSettingModel() {
            this.AvailableProduct = new List<SelectListItem>();
        }
        
        [NopResourceDisplayName("Admin.Configuration.ProductInsuranceSetting.Fields.ProductId")]
        public int ProductId { get; set; }
        public IList<SelectListItem> AvailableProduct { get; set; }
        [NopResourceDisplayName("Admin.Configuration.ProductInsuranceSetting.Fields.ProductType")]
        public int ProductType { get; set; }
        public string ProductInsuranceType { get; set; }
        [NopResourceDisplayName("Admin.Configuration.ProductInsuranceSetting.Fields.RangeStart")]
        public decimal RangeStart { get; set; }
        [NopResourceDisplayName("Admin.Configuration.ProductInsuranceSetting.Fields.RangeEnd")]
        public decimal RangeEnd { get; set; }
        [NopResourceDisplayName("Admin.Configuration.ProductInsuranceSetting.Fields.IsActive")]
        public bool IsActive { get; set; }
        [NopResourceDisplayName("Admin.Configuration.ProductInsuranceSetting.Fields.ProductName")]
        public String ProductName { get; set; }
        [NopResourceDisplayName("Admin.Configuration.ProductInsuranceSetting.Fields.ProductPrice")]
        public decimal? ProductPrice { get; set; }
        //public virtual Product Product { get; set; }
    }
}
