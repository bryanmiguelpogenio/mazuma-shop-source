﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Settings
{
    public class ProductInsuranceSettingListModel
    {
        public ProductInsuranceSettingListModel()
        {
            AvailableProductType = new List<SelectListItem>();
        }
        [NopResourceDisplayName("Admin.Configuration.ProductInsuranceSetting.List.ProductType")]
        public int ProductType { get; set; }

        public IList<SelectListItem> AvailableProductType { get; set; }
    }
}
