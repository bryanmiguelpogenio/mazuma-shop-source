﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Bank
{
    public class TransferRecordListModel
    {
        public TransferRecordListModel() {
            AvailableTransferStatus = new List<SelectListItem>();
        }
        [NopResourceDisplayName("Admin.Bank.TransferRecord.List.OrderId")]
        public int OrderId { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.List.TransferStatusIds")]
        public int[] TransferStatusIds { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.List.FromAccount")]
        public string FromAccount { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.List.FromCardCode")]
        public string FromCardCode { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.List.SerialNumber")]
        public string SerialNumber { get; set; }

        public IList<SelectListItem> AvailableTransferStatus { get; set; }
    }
}
