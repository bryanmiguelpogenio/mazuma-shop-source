﻿using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Bank
{
    public class BankInfoListModel
    {
        [NopResourceDisplayName("Admin.Bank.BankInfo.List.SearchIBAN")]
        public string SearchIBAN { get; set; }
        [NopResourceDisplayName("Admin.Bank.BankInfo.List.SearchCompanyName")]
        public string SearchCompanyName { get; set; }
        [NopResourceDisplayName("Admin.Bank.BankInfo.List.SearchAddress")]
        public string SearchAddress { get; set; }
    }
}
