﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Bank;
using Nop.Web.Areas.Admin.Validators.Bank;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;
using System;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.Bank
{
    [Validator(typeof(TransferRecordValidator))]
    public partial class TransferRecordModel:BaseNopEntityModel
    {
        public TransferRecordModel()
        {
            AvailableTransferStatus = new List<SelectListItem>();
        }

        public IList<SelectListItem> AvailableTransferStatus { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.Fields.OrderId")]
        public int OrderId { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.Fields.FromCardCode")]
        public string FromCardCode { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.Fields.FromAccount")]
        public string FromAccount { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.Fields.Amount")]
        public decimal Amount { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.Fields.CreatedUTC")]
        public DateTime CreatedUTC { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.Fields.SerialNumber")]
        public string SerialNumber { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.Fields.TransferStatusId")]
        public int TransferStatusId { get; set; }
        public string TransferStatus { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.Fields.TransferDate")]
        public DateTime TransferDate { get; set; }

        [NopResourceDisplayName("Admin.Bank.TransferRecord.Fields.Remark")]
        public string Remark { get; set; }

    }
}
