﻿using FluentValidation.Attributes;
using Nop.Web.Areas.Admin.Validators.Bank;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;

namespace Nop.Web.Areas.Admin.Models.Bank
{
    [Validator(typeof(BankInfoValidator))]
    public partial class BankInfoModel:BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.Bank.BankInfo.Fields.IBAN")]
        public string IBAN { get; set; }

        [NopResourceDisplayName("Admin.Bank.BankInfo.Fields.CompanyName")]
        public string CompanyName { get; set; }

        [NopResourceDisplayName("Admin.Bank.BankInfo.Fields.Address")]
        public string Address { get; set; }

        [NopResourceDisplayName("Admin.Bank.BankInfo.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [NopResourceDisplayName("Admin.Bank.BankInfo.Fields.IsActive")]
        public bool IsActive { get; set; }

        public int StoreId { get; set; }
    }
}
