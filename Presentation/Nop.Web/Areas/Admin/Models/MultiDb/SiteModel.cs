﻿using Nop.Web.Framework.Mvc.Models;

namespace Nop.Web.Areas.Admin.Models.MultiDb
{
    public class SiteModel:BaseNopEntityModel
    {
        public string Name { get; set; }
        
        public string WebUrl { get; set; }
        
        public string DbConnectionString { get; set; }
        
        public int DisplayOrder { get; set; }
        
        public int MainBack { get; set; }
    }
}
