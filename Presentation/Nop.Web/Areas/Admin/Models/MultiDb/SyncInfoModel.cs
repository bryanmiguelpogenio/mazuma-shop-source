﻿using Nop.Web.Framework.Mvc.Models;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.MultiDb
{
    public class SyncGroupModel : BaseNopModel
    {
        public int Id { get; set; }
        public string GroupName { get; set; }

        public string TableNames { get; set; }

        public int DisplayOrder { get; set; }

        public int RequireSync { get; set; }
    }
    
    public class SyncInfoModel:BaseNopModel
    {
        public IList<SyncGroupModel> GroupInfos { get; set; }
    }
}
