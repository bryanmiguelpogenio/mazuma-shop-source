﻿using Nop.Web.Areas.Admin.Models.MultiDb;
using Nop.Web.Framework.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.Common
{
    public class SiteSelectorModel : BaseNopModel
    {
        public SiteSelectorModel()
        {
            AvailableSites = new List<SiteModel>();
        }

        public IList<SiteModel> AvailableSites { get; set; }

        public SiteModel CurrentSite { get; set; }
    }
}
