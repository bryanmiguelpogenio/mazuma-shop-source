﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;

namespace Nop.Web.Areas.Admin.Models.Insurance
{
    public partial class InsuranceSearchModel:BaseNopModel
    {
        [NopResourceDisplayName("Admin.Catalog.Insuarnce.List.SearchInsuarnceDeviceName")]
        public string SearchName { get; set; }

        public int ProductAttributeValueId { get; set; }

        public int SelectedInsuranceDeviceId { get; set; }
    }
}
