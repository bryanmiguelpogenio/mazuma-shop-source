﻿using Nop.Web.Framework.Mvc.Models;


namespace Nop.Web.Areas.Admin.Models.Insurance
{
    public partial class InsuranceDeviceModel : BaseNopEntityModel
    {
        public string Key { get; set; }
        public string Type { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Value { get; set; }
    }
}
