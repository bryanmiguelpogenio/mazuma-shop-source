﻿using FluentValidation;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Areas.Admin.Models.Bank;
using Nop.Core.Domain.Bank;

namespace Nop.Web.Areas.Admin.Validators.Bank
{
    public partial class TransferRecordValidator : BaseNopValidator<TransferRecordModel>
    {
        public TransferRecordValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.SerialNumber).NotEmpty().WithMessage(localizationService.GetResource("Admin.Bank.TransferRecord.Fields.SerialNumber.Required"));

            SetDatabaseValidationRules<TransferRecord>(dbContext);
        }
    }
}