﻿using FluentValidation;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Areas.Admin.Models.Bank;
using Nop.Core.Domain.Bank;

namespace Nop.Web.Areas.Admin.Validators.Bank
{
    public partial class BankInfoValidator : BaseNopValidator<BankInfoModel>
    {
        public BankInfoValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.IBAN).NotEmpty().WithMessage(localizationService.GetResource("Admin.Bank.BankInfo.Fields.IBAN.Required"));

            SetDatabaseValidationRules<BankInfo>(dbContext);
        }
    }
}