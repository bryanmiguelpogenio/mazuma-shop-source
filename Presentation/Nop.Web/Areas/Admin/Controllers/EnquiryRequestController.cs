﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Enquiry;
using Nop.Services;
using Nop.Services.Enquiry;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Models.EnquiryRequest;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Models.EnquiryRequest;
using System;
using System.Linq;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class EnquiryRequestController : BaseAdminController
    {
        #region Fields
        private readonly IEnquiryRequestService _enquiryRequestService;
        private readonly IPermissionService _permissionService;
        private readonly IStoreContext _storeContext;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IWorkContext _workContext;
        #endregion

        #region Ctor
        public EnquiryRequestController(IEnquiryRequestService enquiryRequestService,
            IPermissionService permissionService,
            IStoreContext storeContext,
            ILocalizationService localizationService,
            ICustomerActivityService customerActivityService,
            IWorkflowMessageService workflowMessageService,
            IDateTimeHelper dateTimeHelper,
            IWorkContext workContext)
        {
            _customerActivityService = customerActivityService;
            _localizationService = localizationService;
            _storeContext = storeContext;
            _permissionService = permissionService;
            _enquiryRequestService = enquiryRequestService;
            _workflowMessageService = workflowMessageService;
            _dateTimeHelper = dateTimeHelper;
            _workContext = workContext;
        }
        #endregion


        #region Methods
        //list
        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();
            var model = new EnquiryRequestFilterModel()
            {
                AvailableRequestStatus = EnquiryTicketStatus.Pending.ToSelectList(false).ToList()
            };
            model.AvailableRequestStatus.Insert(0, new SelectListItem
            { Text = _localizationService.GetResource("Admin.Common.All"), Value = " ", Selected = true });

            model.EnquiryTicketReasons = _enquiryRequestService.GetAllEnquiryTicketReasons()
                .Select(x=> new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Reason
                }).ToList();
            model.EnquiryTicketReasons.Insert(0, new SelectListItem
            { Text = _localizationService.GetResource("Admin.Common.All"), Value = " ", Selected = true });

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(EnquiryRequestFilterModel model, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var enquiryRequests = _enquiryRequestService.GetListByPage(
                customerId: model.CustomerId,
                requestNumber: model.RequestNumber,
                orderNumber: model.OrderNumber,
                userEmail: model.UserEmail,
                requestStatusId: model.RequestStatusId,
                enquiryTicketReasonId: model.EnquiryTicketReasonId,
                createdFromUtc: model.CreatedFrom,
                createdToUtc: model.CreatedTo,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize
                );
            var gridModel = new DataSourceResult
            {
                Data = enquiryRequests.Select(x => new EnquiryRequestItemModel
                {
                    Id = x.Id,
                    Status = x.RequestStatus,
                    UserEmail = x.UserEmail,
                    RequestNumber = x.RequestNumber,
                    OrderNumber = x.OrderNumber,
                    TicketReason = x.EnquiryTicketReason.Reason,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc),
                    LastUpdatedOn = _dateTimeHelper.ConvertToUserTime(x.LastUpdatedOnUtc, DateTimeKind.Utc)
                }).ToList(),
                Total = enquiryRequests.Count
            };
            return Json(gridModel);
        }

        //edit
        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var entity = _enquiryRequestService.GetById(id);
            //No entity found with the specified id
            if (entity == null)
                return RedirectToAction("List");

            var model = new EnquiryRequestInfoModel()
            {
                Id = entity.Id,
                CustomerId = entity.CustomerId,
                RequestNumber = entity.RequestNumber,
                UserName = entity.UserName,
                UserEmail = entity.UserEmail,
                OrderId = entity.Order == null ? null : entity.OrderId,
                OrderNumber = entity.OrderNumber,
                TicketReason = entity.EnquiryTicketReason.Reason,
                Enquiry = entity.Enquiry,
                CreatedOnUtc = _dateTimeHelper.ConvertToUserTime(entity.CreatedOnUtc, DateTimeKind.Utc),
                ReplyRecords = entity.EnquiryReplyRecords.Select(x => new EnquiryReplyRecordModel
                {
                    Id = x.Id,
                    Message = x.Message,
                    IsFromClient = x.IsFromClient,
                    CreatorEmail = x.CreatorUser.Email,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc)
                }).ToList()
            };
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(EnquiryRequestInfoModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var entity = new EnquiryReplyRecord()
                {
                    EnquiryRequestId = model.Id,
                    Message = model.ReplyMessage,
                    CreatedOnUtc = DateTime.UtcNow,
                    CreatorUserId = _workContext.CurrentCustomer.Id
                };
                _enquiryRequestService.InsertReplyRecord(entity);

                var enquiryRequest = _enquiryRequestService.GetById(model.Id);
                //Sent email to customer
                var ticketReason = _enquiryRequestService.GetAllEnquiryTicketReasons()
                    .Where(x => x.Id == enquiryRequest.EnquiryTicketReasonId)
                    .Select(x => x.Reason)
                    .FirstOrDefault();
                _workflowMessageService.SendEnquiryRequestRepliedMessage(_workContext.WorkingLanguage.Id, enquiryRequest, ticketReason);

                SuccessNotification(_localizationService.GetResource("Admin.EnquiryRequest.Updated"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = model.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //delete
        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                return AccessDeniedView();

            var entity = _enquiryRequestService.GetById(id);
            //No entity found with the specified id
            if (entity == null)
                return RedirectToAction("List");
            _enquiryRequestService.Delete(entity);
            
            SuccessNotification(_localizationService.GetResource("Admin.EnquiryRequest.Deleted"));
            return RedirectToAction("List");
        }

        #endregion
    }
}
