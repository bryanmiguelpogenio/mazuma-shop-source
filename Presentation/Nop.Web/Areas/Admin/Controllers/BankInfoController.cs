﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Services.Bank;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Extensions;
using Nop.Web.Areas.Admin.Models.Bank;
using Nop.Web.Framework.Extensions;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc.Filters;
using System.Linq;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class BankInfoController:BaseAdminController
    {
        #region Fields
        private readonly IBankInfoService _bankInfoService;
        private readonly ITransferRecordService _transferRecordService;
        private readonly IPermissionService _permissionService;
        private readonly IStoreContext _storeContext;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerActivityService _customerActivityService;
        #endregion

        #region Ctor
        public BankInfoController(IBankInfoService bankInfoService,
            IPermissionService permissionService,
            IStoreContext storeContext,
            ILocalizationService localizationService,
            ICustomerActivityService customerActivityService,
            ITransferRecordService transferRecordService)
        {
            this._customerActivityService = customerActivityService;
            this._localizationService = localizationService;
            this._storeContext = storeContext;
            this._permissionService = permissionService;
            this._bankInfoService = bankInfoService;
            this._transferRecordService = transferRecordService;
        }
        #endregion


        #region Methods      
        //list
        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();
            var model = new BankInfoListModel();
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(BankInfoListModel model, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            var bankInfos = _bankInfoService.GetAllBankInfo(
                storeId:_storeContext.CurrentStore.Id,
                IBAN: model.SearchIBAN,
                companyName:model.SearchCompanyName,
                address:model.SearchAddress);
            var gridModel = new DataSourceResult
            {
                Data = bankInfos.PagedForCommand(command).Select(x => {
                    return x.ToModel();
                }),
                Total = bankInfos.Count,
            };
            return Json(gridModel);
        }

        //create 
        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();
            var model = new BankInfoModel();
            model.StoreId = _storeContext.CurrentStore.Id;
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(BankInfoModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();
            if(ModelState.IsValid)
            {
                if (model.IsActive)
                {
                    var bankInfos = _bankInfoService.GetAllBankInfo(storeId: _storeContext.CurrentStore.Id);
                    foreach (var bf in bankInfos)
                    {
                        if (bf.IsActive)
                        {
                            bf.IsActive = false;
                            _bankInfoService.UpdateBankInfo(bf);
                        }
                    }
                }
                var bankInfo = model.ToEntity();
                bankInfo.StoreId = _storeContext.CurrentStore.Id;
                _bankInfoService.InsertBankInfo(bankInfo);

                SuccessNotification(_localizationService.GetResource("Admin.Bank.BankInfo.Added"));
                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();
                    return RedirectToAction("Edit", new { id = bankInfo.Id });
                }
                return RedirectToAction("List");
            }
            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //edit
        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            var bankInfo = _bankInfoService.GetBankInfoById(id);
            if (bankInfo== null)
                //No bankInfo  found with the specified id
                return RedirectToAction("List");

            var model = bankInfo.ToModel();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(BankInfoModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            var bankInfo = _bankInfoService.GetBankInfoById(model.Id);
            if (bankInfo == null)
                //No bankInfo found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                if (model.IsActive)
                {
                    var bankInfos = _bankInfoService.GetAllBankInfo(storeId: _storeContext.CurrentStore.Id);
                    foreach (var bf in bankInfos)
                    {
                        if (bf.IsActive)
                        {
                            bf.IsActive = false;
                            _bankInfoService.UpdateBankInfo(bf);
                        }
                    }
                }
                bankInfo.Address = model.Address;
                bankInfo.CompanyName = model.CompanyName;
                bankInfo.IBAN = model.IBAN;
                bankInfo.DisplayOrder = model.DisplayOrder;
                bankInfo.IsActive = model.IsActive;
                _bankInfoService.UpdateBankInfo(bankInfo);
                

                SuccessNotification(_localizationService.GetResource("Admin.Bank.BankInfo.Updated"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = bankInfo.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //delete
        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            var bankInfo = _bankInfoService.GetBankInfoById(id);
            if (bankInfo == null)
                //No discount found with the specified id
                return RedirectToAction("List");
            _bankInfoService.DeleteBankInfo(bankInfo);
            
            SuccessNotification(_localizationService.GetResource("Admin.Bank.BankInfo.Deleted"));
            return RedirectToAction("List");
        }

        #endregion
    }
}
