﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Insurance;
using Nop.Services.ExportImport;
using Nop.Services.Insurance;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Models.Insurance;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Extensions;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Linq;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class InsuranceDeviceController : BaseAdminController
    {
        #region Fields
        private readonly IImportManager _importManager;
        private readonly IPermissionService _permissionService;
        private readonly ILocalizationService _localizationService;
        private readonly IInsuranceDeviceService _insuranceService;
        private readonly IExportManager _exportManager;
        #endregion Fields

        #region Ctor

        public InsuranceDeviceController(IImportManager importManager,
            IPermissionService permissionService,
            ILocalizationService localizationService,
             IInsuranceDeviceService insuranceService,
             IExportManager exportManager)
        {
            this._exportManager = exportManager;
            this._insuranceService = insuranceService;
            this._localizationService = localizationService;
            this._importManager = importManager;
            this._permissionService = permissionService;
        }

        #endregion


        #region Methods      
        //list
        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();
            return View();
        }

        [HttpPost]
        public virtual IActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var insuranceDevices = _insuranceService.GetAllInsuranceDevice(
               command.Page - 1, command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = insuranceDevices,
                Total = insuranceDevices.TotalCount
            };

            return Json(gridModel);
        }

        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();
            return View();
        }

        [HttpPost]
        public virtual IActionResult Create(String model, String make, String type, int value)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();


            var insuracenDevice = new InsuranceDevice
            {
                Model = model,
                Value = value,
                Make = make,
                Type = type,
            };
            insuracenDevice.Key = insuracenDevice.Model?.Trim().Replace(" ", "_");
            insuracenDevice.Key = insuracenDevice.Make?.Trim().Replace(" ", "_") + "-" + insuracenDevice.Key;
            _insuranceService.InsertInsuranceDevice(insuracenDevice);

            SuccessNotification(_localizationService.GetResource("Admin.Catalog.InsuracenDevice.Added"));
            return RedirectToAction("List");
        }
        [HttpPost]
        public virtual IActionResult Update(int id, int value)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var insurancDevice = _insuranceService.GetInsuranceDeviceById(id);
            if (insurancDevice == null)
            {
                return Content("No insuranc device  found with the specified id");
            }


            insurancDevice.Value = value;
            _insuranceService.UpdateInsuranceDevice(insurancDevice);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var insurancDevice = _insuranceService.GetInsuranceDeviceById(id);
            if (insurancDevice == null)
            {
                return Content("No insuranc device  found with the specified id");
            }
            _insuranceService.DeleteInsuranceDevice(insurancDevice);

            return new NullJsonResult();
        }
        #endregion

        #region Import/Export

        [HttpPost, ActionName("List")]
        [FormValueRequired("exportexcel-all")]
        public virtual IActionResult ExportExcelAll()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var insuranceDevices = _insuranceService.GetAllInsuranceDevice();
            try
            {
                var bytes = _exportManager.ExportInsuranceDeviceToXlsx(insuranceDevices);

                return File(bytes, MimeTypes.TextXlsx, "insuranceDevices.xlsx");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }


        [HttpPost]
        public virtual IActionResult ImportExcel(IFormFile importexcelfile)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
            {
                return AccessDeniedView();
            }
            try
            {
                if (importexcelfile != null && importexcelfile.Length > 0)
                {
                    _importManager.ImportInsuranceDeviceFromXlsx(importexcelfile.OpenReadStream());
                }
                else
                {
                    ErrorNotification(_localizationService.GetResource("Admin.Common.UploadFile"));
                    return RedirectToAction("List");
                }
                SuccessNotification(_localizationService.GetResource("Admin.ImportInsuranceDevices.Imported"));
                return RedirectToAction("List");

            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }
        #endregion
    }
}
