﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Configuration;
using Nop.Services;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Models.Settings;
using Nop.Web.Framework.Extensions;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class ProductInsuranceSettingController : BaseAdminController
    {
        #region Fields
        private readonly IProductService _productService;
        private readonly IProductInsuranceSettingService _productInsuranceSettingService;
        private readonly IPermissionService _permissionService;
        private readonly IStoreContext _storeContext;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerActivityService _customerActivityService;
        #endregion

        #region Ctor
        public ProductInsuranceSettingController(IProductInsuranceSettingService productInsuranceSettingService,
            IPermissionService permissionService,
            IStoreContext storeContext,
            ILocalizationService localizationService,
            ICustomerActivityService customerActivityService, IProductService productService)
        {
            this._productService = productService;
            this._customerActivityService = customerActivityService;
            this._localizationService = localizationService;
            this._storeContext = storeContext;
            this._permissionService = permissionService;
            this._productInsuranceSettingService = productInsuranceSettingService;
        }
        #endregion
        #region Until


        protected virtual void PrepareProductInsuranceSettingModel(ProductInsuranceSettingModel model, ProductInsuranceSetting productInsuranceSetting)
        {
            if (productInsuranceSetting != null)
            {
                model.Id = productInsuranceSetting.Id;
                model.ProductType = productInsuranceSetting.ProductType;
                model.ProductInsuranceType = productInsuranceSetting.ProductInsuranceType.ToString();
                model.ProductId = productInsuranceSetting.ProductId;
                model.RangeEnd = productInsuranceSetting.RangeEnd;
                model.RangeStart = productInsuranceSetting.RangeStart;
                model.IsActive = productInsuranceSetting.IsActive;
            }

            var products = _productService.GetAllInsuranceProducts();
            model.AvailableProduct.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Configuration.SelectProductInsurance"), Value = "0" });
            if (products.Any())
            {
                foreach (var p in products)
                {
                    model.AvailableProduct.Add(new SelectListItem { Text = p.Name, Value = p.Id.ToString() });
                }
            }
        }
        #endregion

        #region Methods      
        //list
        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductInsuranceSetting))
                return AccessDeniedView();
            var model = new ProductInsuranceSettingListModel()
            {
                AvailableProductType = ProductInsuranceType.AllProduct.ToSelectList(false).ToList()
            };
            //model.AvailableProductType.Insert(0, new SelectListItem
            //{ Text = _localizationService.GetResource("Admin.Common.All"), Value = "0", Selected = true });
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(ProductInsuranceSettingListModel model, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductInsuranceSetting))
                return AccessDeniedView();

            var productInsuranceSettings = _productInsuranceSettingService.GetAllProductInsuranceSetting(
                productType:model.ProductType,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize
                );
            var gridModel = new DataSourceResult
            {
                Data = productInsuranceSettings.PagedForCommand(command).Select(x =>
                {
                    return new ProductInsuranceSettingModel
                    {
                        Id = x.Id,
                        ProductId = x.ProductId,
                        ProductType = x.ProductType,
                        ProductInsuranceType = x.ProductInsuranceType.ToString(),
                        RangeEnd = x.RangeEnd,
                        RangeStart = x.RangeStart,
                        ProductName = x.Product?.Name,
                        ProductPrice=x.Product?.Price,
                        IsActive = x.IsActive,
                    };
                }),
                Total = productInsuranceSettings.Count,
            };
            return Json(gridModel);
        }

        //create 
        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductInsuranceSetting))
                return AccessDeniedView();
            var model = new ProductInsuranceSettingModel();
            PrepareProductInsuranceSettingModel(model, null);
            model.IsActive = true;
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(ProductInsuranceSettingModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductInsuranceSetting))
                return AccessDeniedView();
            var product = _productService.GetProductById(model.ProductId);
            if (product == null)
            {
                ModelState.AddModelError("", "Please select Product!");
            }
            if (ModelState.IsValid)
            {
                var productInsuranceSetting = new ProductInsuranceSetting
                {
                    ProductId = model.ProductId,
                    IsActive = model.IsActive,
                    RangeEnd = model.RangeEnd,
                    RangeStart = model.RangeStart,
                    ProductType = model.ProductType,
                };
                _productInsuranceSettingService.InsertProductInsuranceSetting(productInsuranceSetting);

                SuccessNotification(_localizationService.GetResource("Admin.Configuration.ProductInsuranceSetting.Added"));
                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();
                    return RedirectToAction("Edit", new { id = productInsuranceSetting.Id });
                }
                return RedirectToAction("List");
            }
            //If we got this far, something failed, redisplay form
            PrepareProductInsuranceSettingModel(model, null);
            return View(model);
        }

        //edit
        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductInsuranceSetting))
                return AccessDeniedView();

            var productInsuranceSetting = _productInsuranceSettingService.GetProductInsuranceSettingById(id);
            if (productInsuranceSetting == null)
                //No productInsuranceSetting  found with the specified id
                return RedirectToAction("List");

            var model = new ProductInsuranceSettingModel();
            PrepareProductInsuranceSettingModel(model, productInsuranceSetting);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(ProductInsuranceSettingModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductInsuranceSetting))
                return AccessDeniedView();

            var productInsuranceSetting = _productInsuranceSettingService.GetProductInsuranceSettingById(model.Id);
            if (productInsuranceSetting == null)
                //No productInsuranceSetting found with the specified id
                return RedirectToAction("List");
            var product = _productService.GetProductById(model.ProductId);
            if (product == null)
            {
                ModelState.AddModelError("", "Please select Product!");
            }
            if (ModelState.IsValid)
            {
                
                productInsuranceSetting.IsActive = model.IsActive;
                productInsuranceSetting.ProductId = model.ProductId;
                productInsuranceSetting.RangeEnd = model.RangeEnd;
                productInsuranceSetting.RangeStart = model.RangeStart;
                productInsuranceSetting.ProductType = model.ProductType;
                _productInsuranceSettingService.UpdateProductInsuranceSetting(productInsuranceSetting);


                SuccessNotification(_localizationService.GetResource("Admin.Configuration.ProductInsuranceSetting.Updated"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = productInsuranceSetting.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            PrepareProductInsuranceSettingModel(model, productInsuranceSetting);
            return View(model);
        }

        //delete
        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProductInsuranceSetting))
                return AccessDeniedView();

            var productInsuranceSetting = _productInsuranceSettingService.GetProductInsuranceSettingById(id);
            if (productInsuranceSetting == null)
                //No discount found with the specified id
                return RedirectToAction("List");
            _productInsuranceSettingService.DeleteProductInsuranceSetting(productInsuranceSetting);

            SuccessNotification(_localizationService.GetResource("Admin.Configuration.ProductInsuranceSetting.Deleted"));
            return RedirectToAction("List");
        }

        #endregion
    }
}
