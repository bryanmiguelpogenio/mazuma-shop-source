﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Bank;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Services;
using Nop.Services.Bank;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Extensions;
using Nop.Web.Areas.Admin.Models.Bank;
using Nop.Web.Framework.Extensions;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Linq;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class TransferRecordController : BaseAdminController
    {
        #region   Fields
        private readonly ITransferRecordService _transferRecordService;
        private readonly IPermissionService _permissionService;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IOrderService _orderService;
        #endregion

        #region Ctor
        public TransferRecordController(
            IOrderService orderService,
            IDateTimeHelper dateTimeHelper,
            IPermissionService permissionService,
            ILocalizationService localizationService,
            ICustomerActivityService customerActivityService,
            ITransferRecordService transferRecordService)
        {
            this._orderService = orderService;
            this._dateTimeHelper = dateTimeHelper;
            this._customerActivityService = customerActivityService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._transferRecordService = transferRecordService;
        }
        #endregion

        #region Util

        protected virtual void LogEditTransferOrder(int transferId)
        {
            var transferRecord = _transferRecordService.GetTransferRecordById(transferId);

            var commonParams = new[] {
                transferRecord.Id.ToString(),
                transferRecord.TransferStatus.ToString(),
                transferRecord.Amount.ToString(),
                transferRecord.FromCardCode
            };
            _customerActivityService.InsertActivity("EditTransferOrder", _localizationService.GetResource("ActivityLog.EditTransferOrder"), commonParams);
            var order = _orderService.GetOrderById(transferRecord.OrderId);
            order.OrderNotes.Add(new OrderNote
            {
                Note = string.Format(_localizationService.GetResource("ActivityOrderNoteLog.EditTransferOrder"), commonParams),
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);
        }
        protected virtual void LogAddTransferOrder(int transferId)
        {
            var transferRecord = _transferRecordService.GetTransferRecordById(transferId);

            var commonParams = new[] {
                transferRecord.Id.ToString(),
                transferRecord.TransferStatus.ToString(),
                transferRecord.Amount.ToString(),
                transferRecord.FromCardCode
            };
            _customerActivityService.InsertActivity("AddTransferOrder", _localizationService.GetResource("ActivityLog.AddTransferOrder"), commonParams);
            var order = _orderService.GetOrderById(transferRecord.OrderId);
            order.OrderNotes.Add(new OrderNote
            {
                Note = string.Format(_localizationService.GetResource("ActivityOrderNoteLog.AddTransferOrder"), commonParams),
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);
        }

        #endregion

        #region  Methods
        //list
        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();
            var model = new TransferRecordListModel()
            {
                AvailableTransferStatus = TransferStatus.Pending.ToSelectList(false).ToList()
            };

            model.AvailableTransferStatus.Insert(0, new SelectListItem
            { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0", Selected = true });
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(TransferRecordListModel model, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var transferStatusIds = model.TransferStatusIds != null && !model.TransferStatusIds.Contains(0)
               ? model.TransferStatusIds.ToList()
               : null;

            var transferRecords = _transferRecordService.GetAllTransferRecord(
                orderId: model.OrderId,
                fromAccount: model.FromAccount,
                serialNumber: model.SerialNumber,
                tIds: transferStatusIds,
                fromCardCode: model.FromCardCode,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize
                );
            var gridModel = new DataSourceResult
            {
                Data = transferRecords.Select(x =>
                {
                    var transferRecordsModel = x.ToModel();
                    transferRecordsModel.CreatedUTC = _dateTimeHelper.ConvertToUserTime(x.CreatedUTC, DateTimeKind.Utc);
                    return transferRecordsModel;
                }),
                Total = transferRecords.Count
            };
            return Json(gridModel);
        }

        //create 
        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();
            var model = new TransferRecordModel();
            model.TransferStatusId = (int)TransferStatus.Pending;
            model.TransferStatus = TransferStatus.Pending.ToString();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(TransferRecordModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();
            var order = _orderService.GetOrderById(model.OrderId);
            if (order == null)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Admin.Bank.TransferRecord.Validator.OrderId"));
            }
            if (!(order.PaymentMethodSystemName.Equals(SystemCustomerNames.PaymentMethod_BankTranfer)))
            {
                ModelState.AddModelError("", _localizationService.GetResource("Admin.Bank.TransferRecord.Validator.PaymentMethod"));
            }
            if (model.Amount < order.OrderTotal)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Admin.Bank.TransferRecord.Validator.Amount"));
            }
            if (ModelState.IsValid)
            {
                var transferRecord = model.ToEntity();
                transferRecord.CreatedUTC = DateTime.UtcNow;
                transferRecord.TransferStatusId = model.TransferStatusId;
                _transferRecordService.InsertTransferRecord(transferRecord);

                LogAddTransferOrder(transferRecord.Id);

                SuccessNotification(_localizationService.GetResource("Admin.Bank.TransferRecord.Added"));
                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();
                    return RedirectToAction("Edit", new { id = transferRecord.Id });
                }
                return RedirectToAction("List");
            }
            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //edit
        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var transferRecord = _transferRecordService.GetTransferRecordById(id);
            if (transferRecord == null)
                //No transferRecord  found with the specified id
                return RedirectToAction("List");

            var model = transferRecord.ToModel();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(TransferRecordModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var transferRecord = _transferRecordService.GetTransferRecordById(model.Id);
            if (transferRecord == null)
                //No transferRecord found with the specified id
                return RedirectToAction("List");

            var order = _orderService.GetOrderById(transferRecord.OrderId);

            if (model.Amount < order.OrderTotal)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Admin.Bank.TransferRecord.Validator.Amount"));
            }

            if (ModelState.IsValid)
            {
                transferRecord.Amount = model.Amount;
                transferRecord.FromAccount = model.FromAccount;
                transferRecord.FromCardCode = model.FromCardCode;
                transferRecord.Remark = model.Remark;
                transferRecord.SerialNumber = model.SerialNumber;
                transferRecord.TransferStatusId = model.TransferStatusId;
                transferRecord.TransferDate = model.TransferDate;

                _transferRecordService.UpdateTransferRecord(transferRecord);

                LogEditTransferOrder(transferRecord.Id);

                SuccessNotification(_localizationService.GetResource("Admin.Bank.TransferRecord.Updated"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = transferRecord.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //delete
        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var transferRecord = _transferRecordService.GetTransferRecordById(id);
            if (transferRecord == null)
                //No discount found with the specified id
                return RedirectToAction("List");
            _transferRecordService.DeleteTransferRecord(transferRecord);

            _customerActivityService.InsertActivity("DeleteTransferOrder", _localizationService.GetResource("ActivityLog.DeleteTransferOrder"), transferRecord.Id);


            var order = _orderService.GetOrderById(transferRecord.OrderId);
            order.OrderNotes.Add(new OrderNote
            {
                Note = string.Format(_localizationService.GetResource("ActivityOrderNoteLog.DeleteTransferOrder"), transferRecord.Id),
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            SuccessNotification(_localizationService.GetResource("Admin.Bank.TransferRecord.Deleted"));
            return RedirectToAction("List");
        }
        #endregion
    }
}
