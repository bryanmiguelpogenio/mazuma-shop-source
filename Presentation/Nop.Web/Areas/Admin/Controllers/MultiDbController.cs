﻿using Microsoft.AspNetCore.Mvc;
using Nop.Services.MultiDb;
using Nop.Web.Framework;

namespace Nop.Web.Areas.Admin.Controllers
{
    [Area(AreaNames.Admin)]
    public partial class MultiDbController : Controller
    {
        #region Fields
        
        private readonly ISiteService _siteService;

        #endregion

        #region Ctor

        public MultiDbController(ISiteService siteService)
        {
            _siteService = siteService;
        }

        #endregion
        
                
        public IActionResult UpdateDatafromBackend()
        {
            _siteService.SyncDataFromMainBack();

            return Content("");
        }
        
        
    }
}