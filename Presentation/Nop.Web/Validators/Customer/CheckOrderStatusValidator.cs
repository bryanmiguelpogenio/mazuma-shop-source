﻿using System;
using System.Linq;
using FluentValidation;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Customer;
using Nop.Web.Models.Order;

namespace Nop.Web.Validators.Customer
{
    public partial class CheckOrderStatusValidator : BaseNopValidator<CheckOrderStatusModel>
    {
        public CheckOrderStatusValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Email.Required"));
            RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));
            RuleFor(x => x.CustomOrderNumber).NotEmpty().WithMessage(localizationService.GetResource("Order.CheckOrderStatus.CustomOrderNumber.Required"));
        }
    }
}