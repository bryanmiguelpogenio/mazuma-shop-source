﻿using FluentValidation;
using Nop.Core;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Checkout;

namespace Nop.Web.Validators.Checkout
{
    public partial class CheckoutPaymentInfoValidator : BaseNopValidator<CheckoutPaymentInfoModel>
    {
        public CheckoutPaymentInfoValidator(ILocalizationService localizationService,
          IStateProvinceService stateProvinceService,
          IWorkContext workContext)
        {
            RuleFor(x => x.AccountNumber).NotEmpty().WithMessage(localizationService.GetResource("PaymentInfo.AccountNumber.Required"));
            RuleFor(x => x.BankAccountName).NotEmpty().WithMessage(localizationService.GetResource("PaymentInfo.BankAccountName.Required"));
            RuleFor(x => x.SortCode).NotEmpty().WithMessage(localizationService.GetResource("PaymentInfo.SortCode.Required"));
        }
    }
}
