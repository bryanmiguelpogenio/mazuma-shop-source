﻿using FluentValidation;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Infrastructure;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Checkout;
using Nop.Web.Validators.Common;

namespace Nop.Web.Validators.Checkout
{
    public partial class CheckoutYourDetailValidator : BaseNopValidator<CheckoutYourDetailModel>
    {
        public CheckoutYourDetailValidator(ILocalizationService localizationService,
            IStateProvinceService stateProvinceService,
            AddressSettings addressSettings,
            IWorkContext workContext)
        {
            var addressValidator = (AddressValidator)EngineContext.Current.ResolveUnregistered(typeof(AddressValidator));
            RuleFor(x => x.BillingNewAddress).SetValidator(addressValidator);
            RuleFor(x => x.ShippingNewAddress).SetValidator(addressValidator).When(x=> x.ShipToSameBillingAddress);
            
            if (workContext.CurrentCustomer.IsGuest())
            {
                RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("YourDetail.Fields.Email.WrongEmail"));
                RuleFor(x => x.Email).EmailAddress().Must((x, context) =>
                {
                    if (x.IsCreateAccount)
                    {
                        if (string.IsNullOrEmpty(x.Email))
                        {
                            return false;
                        }
                    }
                    return true;
                }).WithMessage(localizationService.GetResource("YourDetail.Fields.Email.Required"));
                RuleFor(x => x.Password).Must((x, context) =>
                {
                    if (x.IsCreateAccount)
                    {
                        if (string.IsNullOrEmpty(x.Password))
                        {
                            return false;
                        }
                    }
                    return true;
                }).WithMessage(localizationService.GetResource("YourDetail.Fields.Password.Required"));
            }
        }
    }
}