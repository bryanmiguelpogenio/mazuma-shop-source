﻿using FluentValidation;
using Nop.Core.Domain.Common;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.EnquiryRequest;

namespace Nop.Web.Validators.EnquiryRequest
{
    public partial class EnquiryRequestValidator : BaseNopValidator<EnquiryRequestBaseModel>
    {
        public EnquiryRequestValidator(ILocalizationService localizationService, CommonSettings commonSettings)
        {
            RuleFor(x => x.UserEmail).NotEmpty().WithMessage(localizationService.GetResource("EnquiryRequest.UserEmail.Required"));
            RuleFor(x => x.UserEmail).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));
            RuleFor(x => x.UserName).NotEmpty().WithMessage(localizationService.GetResource("EnquiryRequest.UserName.Required"));
            RuleFor(x => x.Enquiry).NotEmpty().WithMessage(localizationService.GetResource("EnquiryRequest.Enquiry.Required"));
            RuleFor(x => x.Enquiry).Length(1, 2000).WithMessage(localizationService.GetResource("EnquiryRequest.Enquiry.LengthValidation"));
        }
    }
}