﻿using FluentValidation;
using Nop.Core.Domain.Common;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.EnquiryRequest;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Validators.EnquiryRequest
{
    public partial class EnquiryRequestInfoValidator : BaseNopValidator<EnquiryRequestInfoModel>
    {
        public EnquiryRequestInfoValidator(ILocalizationService localizationService, CommonSettings commonSettings)
        {
            RuleFor(x => x.ReplyMessage).NotEmpty().WithMessage(localizationService.GetResource("Admin.EnquiryReplyRecord.Message.Required"));
            RuleFor(x => x.ReplyMessage).Length(1, 2000).WithMessage(localizationService.GetResource("EnquiryRequest.Enquiry.LengthValidation"));
        }
    }
}