﻿using FluentValidation;
using Nop.Core.Domain.Common;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.EnquiryRequest;

namespace Nop.Web.Validators.EnquiryRequest
{
    public partial class EnquiryRequestModelValidator : BaseNopValidator<EnquiryRequestModel>
    {
        public EnquiryRequestModelValidator(ILocalizationService localizationService, CommonSettings commonSettings)
        {
            RuleFor(x => x.ReplyMessage).NotEmpty().WithMessage(localizationService.GetResource("EnquiryReplyRecord.Message.Required"));
            RuleFor(x => x.ReplyMessage).Length(1, 2000).WithMessage(localizationService.GetResource("EnquiryRequest.Enquiry.LengthValidation"));
        }
    }
}