﻿using System;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;

namespace Nop.Web.Models.EnquiryRequest
{
    public partial class EnquiryReplyRecordModel : BaseNopEntityModel
    {
        public int EnquiryRequestId { get; set; }

        [NopResourceDisplayName("Admin.EnquiryReplyRecord.Message")]
        public string Message { get; set; }

        public string CreatorEmail { get; set; }

        public bool IsFromClient { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}