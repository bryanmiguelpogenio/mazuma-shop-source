﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using Nop.Core.Domain.Enquiry;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Validators.EnquiryRequest;

namespace Nop.Web.Models.EnquiryRequest
{
    [Validator(typeof(EnquiryRequestValidator))]
    public partial class EnquiryRequestBaseModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("EnquiryRequest.OrderNumber")]
        public string OrderNumber { get; set; }

        [NopResourceDisplayName("EnquiryRequest.UserName")]
        public string UserName { get; set; }

        [DataType(DataType.EmailAddress)]
        [NopResourceDisplayName("EnquiryRequest.UserEmail")]
        public string UserEmail { get; set; }

        [NopResourceDisplayName("EnquiryRequest.EnquiryTicketReason")]
        public int EnquiryTicketReasonId { get; set; }

        [NopResourceDisplayName("EnquiryRequest.Enquiry")]
        public string Enquiry { get; set; }

        public IList<EnquiryTicketReason> EnquiryTicketReasons { get; set; }


        public bool SuccessfullySent { get; set; }
        public string Result { get; set; }
    }

    public partial class EnquiryRequestItemModel : BaseNopEntityModel
    {
        public string RequestNumber { get; set; }
        public string UserEmail { get; set; }
        public string OrderNumber { get; set; }
        public string TicketReason { get; set; }
        public string ShortEnquiry { get; set; }
        public EnquiryTicketStatus Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }
    }

    public partial class EnquiryRequestListModel : BaseNopEntityModel
    {
        public EnquiryRequestListModel()
        {
            Items = new List<EnquiryRequestItemModel>();
        }
        public IList<EnquiryRequestItemModel> Items { get; set; }
    }

    [Validator(typeof(EnquiryRequestModelValidator))]
    public partial class EnquiryRequestModel : EnquiryRequestBaseModel
    {
        public string RequestNumber { get; set; }
        public string TicketReason { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastUpdatedOn { get; set; }

        public IList<EnquiryReplyRecordModel> ReplyRecords { get; set; }

        [NopResourceDisplayName("EnquiryRequest.ReplyMessage")]
        public string ReplyMessage { get; set; }
    }
}