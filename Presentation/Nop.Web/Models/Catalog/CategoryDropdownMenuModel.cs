﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc.Models;

namespace Nop.Web.Models.Catalog
{
    public partial class CategoryDropdownMenuModel : BaseNopEntityModel
    {
        public IList<CategoryDropdownMenuItemModel> List { get; set; }
    }

    public partial class CategoryDropdownMenuItemModel : BaseNopEntityModel
    {
        public CategoryDropdownMenuItemModel()
        {
            SubCategories = new List<DropdownMenuItem>();
        }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public List<DropdownMenuItem> SubCategories { get; set; }
    }

    public class DropdownMenuItem
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}