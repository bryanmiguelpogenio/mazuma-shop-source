﻿using Nop.Core.Domain.Payments;
using System;

namespace Nop.Web.Models.Order
{
    public class TransactionRefrenceModel
    {
        public TransactionRefrenceModel()
        {
            SubmitEnable = true;
        }
        public int OrderId { get; set; }
        public PaymentStatus OrderPaymentStatus { get; set; }
        public string BankName { get; set; }
        public string AccountNO { get; set; }
        public string TransactionRefrenceNO { get; set; }
        public DateTime DepositedOn { get; set; }
        public bool SubmitEnable { get; set; }
    }
}