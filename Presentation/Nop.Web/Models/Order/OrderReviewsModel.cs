﻿using System.Collections.Generic;

namespace Nop.Web.Models.Order
{
    public class OrderReviewsModel
    {
        public int OrderId { get; set; }
        public List<ProductModel> ProductList { get; set; }
        public string ReviewText { get; set; }
        public string Title { get; set; }
        public int Rating { get; set; }
        public OrderReviewsModel()
        {
            ProductList = new List<ProductModel>();
        }
    }
    public class ProductModel
    {
        public int ProductId { get; set; }
        public string PictureUrl { get; set; }
    }
}