﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Models.Common;
using Nop.Web.Validators.Customer;

namespace Nop.Web.Models.Order
{
    [Validator(typeof(CheckOrderStatusValidator))]
    public partial class CheckOrderStatusModel : BaseNopModel
    {
        /// <summary>
        /// Customer Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// CustomOrderNumber
        /// </summary>
        [NopResourceDisplayName("Order.CheckOrderStatus.CustomOrderNumber")]
        public string CustomOrderNumber { get; set; }
    }
}