﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Framework.UI.Paging;
using Nop.Web.Models.Catalog;

namespace Nop.Web.Models.Discounts
{
    public partial class DicountProductsModel : BaseNopEntityModel
    {
        public DicountProductsModel()
        {
            Products = new List<ProductOverviewModel>();
            PagingFilteringContext = new DiscountPagingFilteringModel();
        }

        public DiscountPagingFilteringModel PagingFilteringContext { get; set; }

        public IList<ProductOverviewModel> Products { get; set; }

    }
}