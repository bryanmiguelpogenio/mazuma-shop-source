﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework.UI.Paging;

namespace Nop.Web.Models.Discounts
{
    public partial class DiscountPagingFilteringModel : BasePageableModel
    {
        /// <summary>
        /// Category IDs
        /// </summary>
        public IList<int> CategoryIds { get; set; }
        /// <summary>
        /// Manufacturer IDs
        /// </summary>
        public IList<int> ManufacturerIds { get; set; }

        /// <summary>
        /// Price - From 
        /// </summary>
        public decimal PriceFrom { get; set; }

        /// <summary>
        /// Price - To
        /// </summary>
        public decimal PriceTo { get; set; }

        /// <summary>
        /// OrderBy
        /// </summary>
        public int OrderBy { get; set; }
    }
}