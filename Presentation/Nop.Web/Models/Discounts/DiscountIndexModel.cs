﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Models.Catalog;

namespace Nop.Web.Models.Discounts
{
    public partial class DiscountIndexModel : BaseNopModel
    {
        public DiscountIndexModel()
        {
            AvailableManufacturers = new List<KeyValuePair<int, string>>();
            Categories = new List<CategorySimpleModel>();
        }
        public string CurrencySymbol { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public IList<KeyValuePair<int, string>> AvailableManufacturers { get; set; }
        public List<CategorySimpleModel> Categories { get; set; }
    }
}