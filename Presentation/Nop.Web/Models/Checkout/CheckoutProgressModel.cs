﻿using Nop.Web.Framework.Mvc.Models;

namespace Nop.Web.Models.Checkout
{
    public partial class CheckoutProgressModel : BaseNopModel
    {
        public CheckoutProgressStep CheckoutProgressStep { get; set; }
    }

    public enum CheckoutProgressStep
    {
        Cart = 1,
        Address = 2,
        Shipping = 3,
        Payment = 4,
        Confirm = 5,
        Complete = 6
    }
}