﻿using FluentValidation.Attributes;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Validators.Checkout;

namespace Nop.Web.Models.Checkout
{
    [Validator(typeof(CheckoutInsuranceDirectDebitValidator))]
    public partial  class CheckoutInsuranceDirectDebitModel: BaseNopModel
    {
        public int OrderId { get; set; }
        public bool NeedUseInsurance { get; set; }
        public string BankAccountName { get; set; }
        public string SortCode { get; set; }
        public string AccountNumber { get; set; }
    }
}
