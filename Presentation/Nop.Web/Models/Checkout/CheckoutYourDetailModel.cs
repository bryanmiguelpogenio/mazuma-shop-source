﻿using System.Collections.Generic;
using FluentValidation.Attributes;
using Microsoft.AspNetCore.Http;
using Nop.Core.Domain.Shipping;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Models.Common;
using Nop.Web.Validators.Checkout;

namespace Nop.Web.Models.Checkout
{
    [Validator(typeof(CheckoutYourDetailValidator))]
    public partial class CheckoutYourDetailModel : BaseNopModel
    {
        public CheckoutYourDetailModel()
        {
            ExistingAddresses = new List<AddressModel>();
            BillingNewAddress = new AddressModel();
            ShippingNewAddress = new AddressModel();

            ShippingMethods = new List<ShippingMethodModel>();
            Warnings = new List<string>();
        }

        //MVC is suppressing further validation if the IFormCollection is passed to a controller method. That's why we add to the model
        public IFormCollection Form { get; set; }

        public IList<AddressModel> ExistingAddresses { get; set; }

        public AddressModel BillingNewAddress { get; set; }
        
        public AddressModel ShippingNewAddress { get; set; }

        public int BillingAddressSelectedId { get; set; }
        public int ShippingAddressSelectedId { get; set; }

        public bool ShipToSameBillingAddress { get; set; }
        public bool ShipToSameBillingAddressAllowed { get; set; }

        /// <summary>
        /// Used on one-page checkout page
        /// </summary>
        //public bool NewAddressPreselected { get; set; }

        #region register info for guest
        public bool IsCreateAccount { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        #endregion
        // shipping settings:
        #region shipping settings
        public IList<CheckoutPickupPointModel> PickupPoints { get; set; }
        public bool AllowPickUpInStore { get; set; }
        public bool PickUpInStore { get; set; }
        public bool PickUpInStoreOnly { get; set; }
        public bool DisplayPickupPointsOnMap { get; set; }
        public string GoogleMapsApiKey { get; set; }
        #endregion


        #region shipping method

        public IList<ShippingMethodModel> ShippingMethods { get; set; }

        //public bool NotifyCustomerAboutShippingFromMultipleLocations { get; set; }

        public IList<string> Warnings { get; set; }

        #region Nested classes

        public partial class ShippingMethodModel : BaseNopModel
        {
            public string ShippingRateComputationMethodSystemName { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Fee { get; set; }
            public bool Selected { get; set; }

            public ShippingOption ShippingOption { get; set; }
        }
        #endregion
        #endregion
    }
}