﻿using FluentValidation.Attributes;
using Nop.Core.Domain.Bank;
using Nop.Web.Framework.Mvc.Models;
using Nop.Web.Validators.Checkout;

namespace Nop.Web.Models.Checkout
{
    [Validator(typeof(CheckoutPaymentInfoValidator))]
    public partial class CheckoutPaymentInfoModel : BaseNopModel
    {
        public string PaymentViewComponentName { get; set; }

        /// <summary>
        /// Used on one-page checkout page
        /// </summary>
        public bool DisplayOrderTotals { get; set; }

        //Only use for bank trasfer
        public BankInfo BankInfo { get; set; }

        //Only use for Insurance
        public bool NeedUseInsurance { get; set; }
        public string BankAccountName { get; set; }
        public string SortCode { get; set; }
        public string AccountNumber { get; set; }

    }
}