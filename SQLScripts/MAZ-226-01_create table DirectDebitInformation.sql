
CREATE TABLE [dbo].[DirectDebitInformation](
	[Id] [int] IDENTITY(1,1) NOT NULL primary key,
	[OrderId] [int]  NOT NULL,
	[BankAccountName] [nvarchar](200) NOT NULL,
	[SortCode] [nvarchar](200) NOT NULL,
	[AccountNumber] [nvarchar](200) NOT NULL,
	CONSTRAINT [FK_DirectDebitInformation_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id]),
) 
