--Create memory optimized table and indexes on the memory optimized table.
CREATE TABLE [dbo].InsuranceDevice
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[Key] NVARCHAR(Max) NOT  NULL,
	[Type] NVARCHAR(400)  NULL,
	[Make] NVARCHAR(400)   NULL,
	[Model] NVARCHAR(Max)  NULL,
	[Value] int NOT NULL,

 )
GO