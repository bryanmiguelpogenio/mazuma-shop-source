
INSERT INTO dbo.MessageTemplate
VALUES
(   'Service.EnquiryRequestReplied',  -- Name - nvarchar(200)
    NULL,  -- BccEmailAddresses - nvarchar(200)
    '%Store.Name%. Mazuma has replied your enquiry request',  -- Subject - nvarchar(1000)
    '<p>  %EnquiryRequest.Body%  </p>',  -- Body - nvarchar(max)
    0, -- IsActive - bit
    null,    -- DelayBeforeSend - int
    0,    -- DelayPeriodId - int
    0,    -- AttachedDownloadId - int
    2,    -- EmailAccountId - int
    0  -- LimitedToStores - bit
    )


	select * from MessageTemplate
	 where name='Service.EnquiryRequestReplied'

