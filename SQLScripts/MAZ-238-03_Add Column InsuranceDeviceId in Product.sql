
--外键需要为空才行
IF NOT EXISTS (SELECT  1 FROM sys.columns WHERE object_id = OBJECT_ID('[ProductAttributeCombination]') AND NAME = 'InsuranceDeviceId' ) 
    BEGIN
        ALTER TABLE [ProductAttributeCombination] ADD [InsuranceDeviceId] int NULL
    END
GO