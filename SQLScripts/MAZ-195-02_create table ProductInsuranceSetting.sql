/****** Object:  Table [dbo].[ProductInsuranceSetting]    Script Date: 2019/6/5 11:53:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProductInsuranceSetting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductType] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[RangeStart] [decimal](18, 0) NOT NULL,
	[RangeEnd] [decimal](18, 0) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ProductInsuranceSetting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProductInsuranceSetting]  WITH CHECK ADD  CONSTRAINT [FK_ProductInsuranceSetting_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO

ALTER TABLE [dbo].[ProductInsuranceSetting] CHECK CONSTRAINT [FK_ProductInsuranceSetting_Product]
GO