
  INSERT INTO dbo.ScheduleTask
  (
      Name,
      Seconds,
      Type,
      Enabled,
      StopOnError,
      LastStartUtc,
      LastEndUtc,
      LastSuccessUtc
  )
  VALUES
  (   'Upload Insurance To Ftp',       -- Name - nvarchar(max)
      86400,         -- Seconds - int
      'Nop.Services.Orders.UploadInsuranceToFtpTask, Nop.Services',       -- Type - nvarchar(max)
      0,      -- Enabled - bit
      0,      -- StopOnError - bit
      GETDATE(), -- LastStartUtc - datetime
      GETDATE(), -- LastEndUtc - datetime
      GETDATE()  -- LastSuccessUtc - datetime
      )