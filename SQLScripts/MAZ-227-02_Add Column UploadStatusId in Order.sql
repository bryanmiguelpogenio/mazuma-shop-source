IF NOT EXISTS (SELECT  1 FROM sys.columns WHERE object_id = OBJECT_ID('[Order]') AND NAME = 'UploadStatusId' ) 
    BEGIN
        ALTER TABLE [Order] ADD [UploadStatusId] int NULL
    END
GO
UPDATE [Order] SET [UploadStatusId] = 0 WHERE [UploadStatusId] IS NULL
GO
ALTER TABLE [Order] ALTER COLUMN [UploadStatusId] int NOT NULL

GO