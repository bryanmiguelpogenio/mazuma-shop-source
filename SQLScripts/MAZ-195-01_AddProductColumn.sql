-- InsuranceEnabled 启用保险产品
IF NOT EXISTS (SELECT  1 FROM sys.columns WHERE object_id = OBJECT_ID('[Product]') AND NAME = 'InsuranceEnabled' ) 
    BEGIN
        ALTER TABLE [Product] ADD [InsuranceEnabled] bit NULL
    END
GO
UPDATE [Product] SET [InsuranceEnabled] = 0 WHERE [InsuranceEnabled] IS NULL
GO

-- 是否是保险产品
IF NOT EXISTS (SELECT  1 FROM sys.columns WHERE object_id = OBJECT_ID('[Product]') AND NAME = 'IsInsurance' ) 
    BEGIN
        ALTER TABLE [Product] ADD [IsInsurance] bit NULL
    END
GO
UPDATE [Product] SET [IsInsurance] = 0 WHERE [IsInsurance] IS NULL
GO