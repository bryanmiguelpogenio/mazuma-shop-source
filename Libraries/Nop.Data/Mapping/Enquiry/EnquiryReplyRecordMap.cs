﻿using Nop.Core.Domain.Enquiry;

namespace Nop.Data.Mapping.Enquiry
{
    public partial class EnquiryReplyRecordMap : NopEntityTypeConfiguration<EnquiryReplyRecord>
    {
        public EnquiryReplyRecordMap()
        {
            this.ToTable("EnquiryReplyRecord");
            this.HasKey(t => t.Id);

            this.HasRequired(t => t.EnquiryRequest)
               .WithMany(t => t.EnquiryReplyRecords)
               .HasForeignKey(t => t.EnquiryRequestId);

            this.HasOptional(o => o.CreatorUser)
                .WithMany()
                .HasForeignKey(o => o.CreatorUserId);
        }
    }
}
