﻿using Nop.Core.Domain.Enquiry;

namespace Nop.Data.Mapping.Enquiry
{
    public partial class EnquiryTicketReasonMap : NopEntityTypeConfiguration<EnquiryTicketReason>
    {
        public EnquiryTicketReasonMap()
        {
            this.ToTable("EnquiryTicketReason");
            this.HasKey(t => t.Id);
            this.Property(t => t.Reason).HasMaxLength(200);
        }
    }
}
