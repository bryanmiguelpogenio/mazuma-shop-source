﻿using Nop.Core.Domain.Enquiry;

namespace Nop.Data.Mapping.Enquiry
{
    public partial class EnquiryRequestMap : NopEntityTypeConfiguration<EnquiryRequest>
    {
        public EnquiryRequestMap()
        {
            this.ToTable("EnquiryRequest");
            this.HasKey(t => t.Id);
            this.Property(t => t.RequestNumber).HasMaxLength(20);
            this.Property(t => t.OrderNumber).HasMaxLength(20);
            this.Property(t => t.UserName).HasMaxLength(100);
            this.Property(t => t.UserEmail).HasMaxLength(100);
            this.Property(t => t.Enquiry).HasMaxLength(2000);

            this.Ignore(t => t.RequestStatus);


            this.HasOptional(x => x.Order)
               .WithMany()
               .HasForeignKey(x => x.OrderId);

            this.HasOptional(x => x.Customer)
                .WithMany()
                .HasForeignKey(x => x.CustomerId);

            this.HasRequired(x => x.EnquiryTicketReason)
                .WithMany()
                .HasForeignKey(x => x.EnquiryTicketReasonId);
        }
    }
}
