﻿
using Nop.Core.Domain.Insurance;

namespace Nop.Data.Mapping.Insurance
{
    public partial class InsuranceDeviceMap:NopEntityTypeConfiguration<InsuranceDevice>
    {

        public InsuranceDeviceMap()
        {
            this.ToTable("InsuranceDevice");
            this.HasKey(i => i.Id);
            this.Property(i => i.Type).HasMaxLength(400);
            this.Property(i => i.Make).HasMaxLength(400);
        }
    }
}
