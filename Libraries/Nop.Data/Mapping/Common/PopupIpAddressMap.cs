using Nop.Core.Domain.Common;

namespace Nop.Data.Mapping.Common
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class PopupIpAddressMap : NopEntityTypeConfiguration<PopupIpAddress>
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public PopupIpAddressMap()
        {
            this.ToTable("PopupIpAddress");
            this.HasKey(ga => ga.Id);

            this.Property(ga => ga.IpAddress).IsRequired();
        }
    }
}