﻿using Nop.Core.Domain.MultiDb;

namespace Nop.Data.Mapping.MultiDb
{
    public class SyncGroupInfoMap: NopEntityTypeConfiguration<SyncGroupInfo>
    {
        public SyncGroupInfoMap()
        {
            ToTable("SyncGroupInfo");
            HasKey(s => s.Id);
            Property(s => s.GroupName).IsRequired();
            Property(s => s.TableNames).IsRequired();
            Property(s => s.DisplayOrder).IsRequired();
            Property(s => s.RequireSync).IsRequired();
        }
    }
}
