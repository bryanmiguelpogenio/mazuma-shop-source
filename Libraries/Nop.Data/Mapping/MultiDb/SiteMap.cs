﻿using Nop.Core.Domain.MultiDb;

namespace Nop.Data.Mapping.MultiDb
{
    public class SiteMap: NopEntityTypeConfiguration<Site>
    {
        public SiteMap()
        {
            ToTable("Site");
            HasKey(s => s.Id);
            Property(s => s.Name).IsRequired();
            Property(s => s.WebUrl).IsRequired();
            Property(s => s.DisplayOrder).IsRequired();
            Property(s => s.MainBack).IsRequired();
        }
    }
}
