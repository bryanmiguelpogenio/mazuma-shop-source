﻿using Nop.Core.Domain.MultiDb;

namespace Nop.Data.Mapping.MultiDb
{
    public class OpearteLogMap: NopEntityTypeConfiguration<OperateLog>
    {
        public OpearteLogMap()
        {
            ToTable("OperateLog");
            HasKey(s => s.Id);
            Property(s => s.Action).IsRequired();
            Property(s => s.TableName).IsRequired();
            Property(s => s.EntityId).IsRequired();
        }
    }
}
