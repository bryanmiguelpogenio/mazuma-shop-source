﻿namespace Nop.Data.Mapping.Orders
{
    public partial class OrderMap
    {
        protected override void PostInitialize()
        {
            this.Property(o => o.SynchronizedOnUtc).IsOptional();
            this.Property(o => o.EcorenewFinanceTypeId);
            this.Ignore(o => o.EcorenewFinanceType);
        }
    }
}