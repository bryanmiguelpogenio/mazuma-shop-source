﻿using Nop.Core.Domain.Orders;

namespace Nop.Data.Mapping.Orders
{
    public partial class DirectDebitInformationMap : NopEntityTypeConfiguration<DirectDebitInformation>
    {
        public DirectDebitInformationMap()
        {
            this.ToTable("DirectDebitInformation");
            this.HasKey(d => d.Id);
            this.Property(d => d.BankAccountName).HasMaxLength(200);
            this.Property(d => d.SortCode).HasMaxLength(200);
            this.Property(d => d.AccountNumber).HasMaxLength(200);

            this.HasRequired(t => t.Order)
             .WithMany(t => t.DirectDebits)
             .HasForeignKey(t => t.OrderId);
        }
    }
}
