﻿namespace Nop.Data.Mapping.Orders
{
    public partial class ReturnRequestMap
    {
        protected override void PostInitialize()
        {
            this.Property(r => r.SynchronizedOnUtc).IsOptional();
        }
    }
}