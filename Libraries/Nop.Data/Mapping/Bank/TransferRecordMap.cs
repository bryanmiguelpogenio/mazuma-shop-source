﻿using Nop.Core.Domain.Bank;

namespace Nop.Data.Mapping.Bank
{
    public partial class TransferRecordMap:NopEntityTypeConfiguration<TransferRecord>
    {
        public TransferRecordMap()
        {
            this.ToTable("TransferRecord");
            this.HasKey(t => t.Id);
            this.Property(t => t.SerialNumber).HasMaxLength(400);
            this.Property(t => t.ToAccount).HasMaxLength(400);
            this.Property(t => t.ToCardCode).HasMaxLength(400);
            this.Property(t => t.FromAccount).HasMaxLength(400);
            this.Property(t => t.FromCardCode).HasMaxLength(400);
            this.Property(t => t.Amount).HasPrecision(18, 4);

            this.Ignore(t => t.TransferStatus);

            this.HasRequired(t => t.Order)
               .WithMany(t=>t.TransferRecords)
               .HasForeignKey(t => t.OrderId);
        }
    }
}
