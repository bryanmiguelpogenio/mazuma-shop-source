﻿namespace Nop.Data.Mapping.Bank
{
    public partial class BankInfoMap:NopEntityTypeConfiguration<Core.Domain.Bank.BankInfo>
    {
        public BankInfoMap()
        {
            this.ToTable("BankInfo");
            this.HasKey(b => b.Id);
            this.Property(b => b.IBAN).HasMaxLength(400);
            this.Property(b => b.CompanyName).HasMaxLength(400);
            this.Property(b => b.Address).HasMaxLength(400);
        }
    }
}
