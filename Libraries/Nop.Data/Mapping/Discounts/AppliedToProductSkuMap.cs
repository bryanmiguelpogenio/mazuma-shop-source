using Nop.Core.Domain.Discounts;

namespace Nop.Data.Mapping.Discounts
{
    /// <summary>
    /// Mapping class
    /// </summary>
    public partial class AppliedToProductSkuMap : NopEntityTypeConfiguration<AppliedToProductSku>
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public AppliedToProductSkuMap()
        {
            this.ToTable("Discount_AppliedToProductSkus");
            this.HasKey(d => d.Id);

            this.HasRequired(duh => duh.Discount)
               .WithMany()
               .HasForeignKey(duh => duh.DiscountId);

            this.HasRequired(duh => duh.Product)
              .WithMany()
              .HasForeignKey(duh => duh.ProductId);
        }
    }
}