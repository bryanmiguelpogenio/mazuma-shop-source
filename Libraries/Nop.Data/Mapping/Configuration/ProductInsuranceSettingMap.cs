﻿using Nop.Core.Domain.Configuration;

namespace Nop.Data.Mapping.Configuration
{
    public partial class ProductInsuranceSettingMap:NopEntityTypeConfiguration<ProductInsuranceSetting>
    {
        public ProductInsuranceSettingMap() {
            this.ToTable("ProductInsuranceSetting");
            this.HasKey(p => p.Id);
            this.Property(p => p.RangeEnd).HasPrecision(18, 2);
            this.Property(p => p.RangeEnd).HasPrecision(18, 2);
            this.Ignore(p => p.ProductInsuranceType);
            this.HasRequired(p => p.Product)
                .WithMany(p => p.ProductInsuranceSettings)
                .HasForeignKey(p=>p.ProductId);
        }
    }
}
