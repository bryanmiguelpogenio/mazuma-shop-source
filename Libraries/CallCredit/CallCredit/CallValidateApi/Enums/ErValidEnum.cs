﻿namespace CallCredit.CallValidateApi.Enums
{
    public enum ErValidEnum
    {
        IndividualFoundOnCurrentAnnualElectoralRegister = 1,
        IndividualFoundOnPreviousAnnualElectoralRegister = 3,
        IndividualFoundOnCurrentRollingElectoralRegister = 4,
        IndividualFoundOnPreviousRollingElectoralRegister = 5
    }
}
