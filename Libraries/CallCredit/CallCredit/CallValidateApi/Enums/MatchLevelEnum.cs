﻿namespace CallCredit.CallValidateApi.Enums
{
    public enum MatchLevelEnum
    {
        IndividualReport,
        SurnameReport,
        AddressReport,
        NoMatchFound,
        Picklist
    }
}
