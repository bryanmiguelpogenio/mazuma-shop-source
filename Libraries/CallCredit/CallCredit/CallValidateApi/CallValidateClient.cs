﻿using CallCredit.CallValidateApi.RequestEntities;
using CallCredit.CallValidateApi.ResponseEntities;
using CallCredit.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace CallCredit.CallValidateApi
{
    public partial class CallValidateClient
    {
        #region Fields

        private CallValidateConfig _config;

        #endregion

        #region Constructors

        public CallValidateClient(CallValidateConfig config)
        {
            // TODO: validate config

            this._config = config;
        }

        #endregion

        #region Methods

        public Results PerformCheck(CallValidate request)
        {
            string mainXml = request.ToXmlString();

            mainXml = mainXml.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
            mainXml = mainXml.Replace(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "").Trim();

            WebRequest webRequest = WebRequest.Create(_config.UseLive ? _config.ApiLiveUrl : _config.ApiTestUrl);

            HttpWebRequest httpRequest = (HttpWebRequest)webRequest;
            httpRequest.Method = "POST";
            httpRequest.ContentType = "text/xml";

            // create stream and complete request
            using (Stream requestStream = httpRequest.GetRequestStream())
            {
                using (StreamWriter streamWriter = new StreamWriter(requestStream))
                {
                    streamWriter.Write(mainXml);
                    streamWriter.Close();
                }
            }

            var retval = new Results();

            // get the response
            using (WebResponse webResponse = httpRequest.GetResponse())
            {
                using (Stream responseStream = webResponse.GetResponseStream())
                {
                    using (StreamReader streamReader = new StreamReader(responseStream))
                    {
                        // read the response into an xml document
                        System.Xml.XmlDocument soapResonseXMLDocument = new System.Xml.XmlDocument();
                        soapResonseXMLDocument.LoadXml(streamReader.ReadToEnd());

                        // return only the xml representing the response details (inner request)
                        retval = soapResonseXMLDocument.GetElementsByTagName("Results")[0].ToObject<Results>();
                        retval.ResultXml = soapResonseXMLDocument.GetElementsByTagName("Results")[0].OuterXml;
                        retval.RequestXml = mainXml;
                    }
                }
            }

            return retval;
        }

        #endregion

        #region Utilities

        private CallValidate CreateNewCallValidateApiMain()
        {
            return new CallValidate(_config.Company, _config.Username, _config.Password, _config.ApplicationName);
        }

        #endregion
    }
}