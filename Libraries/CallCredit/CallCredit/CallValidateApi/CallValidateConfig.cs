﻿namespace CallCredit.CallValidateApi
{
    public partial class CallValidateConfig
    {
        public readonly string Company;
        public readonly string Username;
        public readonly string Password;
        public readonly string ApplicationName;
        public readonly string ApiTestUrl;
        public readonly string ApiLiveUrl;
        public readonly bool UseLive;

        public CallValidateConfig(string company, string username, string password, string applicationname, bool useLive = false, string apiTestUrl = null, string apiLiveUrl = null)
        {
            this.Company = company;
            this.Username = username;
            this.Password = password;
            this.ApplicationName = applicationname;
            this.ApiTestUrl = apiTestUrl ?? "https://ct.callcreditsecure.co.uk/callvalidateapi/incomingserver.php";
            //this.ApiTestUrl = apiTestUrl ?? "https://sftp.callcreditgroup.com/";
            this.ApiLiveUrl = apiLiveUrl ?? "https://www.callcreditsecure.co.uk/callvalidateapi/incomingserver.php";
            this.UseLive = useLive;
        }
    }
}
