﻿namespace CallCredit.CallValidateApi.Checks
{
    public abstract class CallValidateApiCheck
    {
        private CallValidateClient _client;

        protected CallValidateApiCheck(CallValidateClient client)
        {
            this._client = client;
        }

        // TODO: change return value to (response)
        protected virtual void Perform(object data)
        {
            ValidateParameters(data);

            // perform request
        }

        protected void ValidateParameters(object param)
        {

        }
    }
}