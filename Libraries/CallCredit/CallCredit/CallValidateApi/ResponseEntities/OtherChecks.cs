﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class OtherChecks
    {
        public string IdentityResult { get; set; }

        public int IdentityScore { get; set; }

        public int CreditScore { get; set; }

        public string ZodiacSign { get; set; }
    }
}
