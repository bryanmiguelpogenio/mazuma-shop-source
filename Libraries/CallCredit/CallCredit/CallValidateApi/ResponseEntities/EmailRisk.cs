﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class EmailRisk
    {
        public EmailRiskResult Result { get; set; }

        public EmailRiskEmail Email { get; set; }

        public EmailRiskDomain Domain { get; set; }
    }
}
