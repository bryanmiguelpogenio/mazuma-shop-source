﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class CallValidate3D
    {
        public int QuestionsRequested { get; set; }

        public int QuestionsReturned { get; set; }

        public Questions Questions { get; set; }
    }
}
