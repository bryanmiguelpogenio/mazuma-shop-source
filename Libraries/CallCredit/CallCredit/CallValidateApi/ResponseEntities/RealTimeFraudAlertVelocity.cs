﻿using System;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class RealTimeFraudAlertVelocity
    {
        [XmlAttributeAttribute()]
        public string Type { get; set; }

        public DateTime LastSeen { get; set; }

        public int TimesSeen { get; set; }

        public string Warning { get; set; }
    }
}
