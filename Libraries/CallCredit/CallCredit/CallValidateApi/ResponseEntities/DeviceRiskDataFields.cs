﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class DeviceRiskDataFields
    {
        [XmlElement(ElementName = "DataField")]
        public List<DeviceRiskDataField> DataField { get; set; }
    }
}
