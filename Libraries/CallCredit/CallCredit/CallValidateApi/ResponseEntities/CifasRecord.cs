﻿using System;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class CifasRecord
    {
        public string title { get; set; }
        
        public string firstname { get; set; }

        public string othername1 { get; set; }

        public string othername2 { get; set; }

        public string surname { get; set; }
        
        public DateTime birthdate { get; set; }

        public string flat { get; set; }

        public string housename { get; set; }
        
        public string housenumber { get; set; }
        
        public string street { get; set; }

        public string district { get; set; }

        public string country { get; set; }

        public string town { get; set; }
        
        public string postcode { get; set; }
        
        public string casereferenceno { get; set; }
        
        public string membernumber { get; set; }
        
        public string membername { get; set; }
        
        public string productcode { get; set; }
        
        public string productdesc { get; set; }
        
        public string frauddesc { get; set; }
        
        public DateTime inputdate { get; set; }
        
        public DateTime expirydate { get; set; }
    }
}
