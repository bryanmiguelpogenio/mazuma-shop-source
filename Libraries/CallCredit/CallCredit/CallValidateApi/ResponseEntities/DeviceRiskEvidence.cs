﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class DeviceRiskEvidence
    {
        [XmlElement(ElementName = "Evidence")]
        public List<DeviceRiskEvidenceEvidence> Evidence { get; set; }
    }
}
