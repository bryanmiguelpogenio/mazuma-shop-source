﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class DeviceRiskDataField
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
