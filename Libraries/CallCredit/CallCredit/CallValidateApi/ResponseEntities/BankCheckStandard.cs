﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class BankCheckStandard
    {
        public string Date_of_last_change { get; set; }

        public string BACS_Status { get; set; }
        
        public string Date_Closed_in_BACS_clearing { get; set; }
        
        public string Redirection_from_flag { get; set; }
        
        public string Redirected_to_Sort_Code { get; set; }
        
        public string BACS_Settlement_Bank { get; set; }
        
        public string Settlement_section { get; set; }
        
        public string Settlement_Subsection { get; set; }
        
        public string Handling_Bank { get; set; }
        
        public string Account_numbered_flag { get; set; }
        
        public string DDI_voucher_flag { get; set; }
        
        public string Transactions_disallowed_DR { get; set; }
        
        public string Transactions_disallowed_CR { get; set; }

        public string Transactions_disallowed_CU { get; set; }

        public string Transactions_disallowed_PR { get; set; }
        
        public string Transactions_disallowed_BS { get; set; }
        
        public string Transactions_disallowed_DV { get; set; }
        
        public string Transactions_disallowed_AU { get; set; }
        
        public string CHAPS_Return_Indicator { get; set; }
        
        public string SWIFT_data { get; set; }
        
        public string CHAPS_Effective_date_of_last_change { get; set; }
        
        public string Date_closed_in_CCCC_clearing { get; set; }
        
        public string CCCC_Settlement_bank { get; set; }
        
        public string Debit_agency_sort_code { get; set; }
        
        public string GB_Northern_Ireland_Indicator { get; set; }
        
        public string Branch_type_indicator { get; set; }
        
        public string Sort_Code_of_main_branch { get; set; }
        
        public string Major_Location_Name { get; set; }
        
        public string Minor_Location_Name { get; set; }
        
        public string Branch_Name_or_Place { get; set; }
        
        public string Branch_name_for_second_entry { get; set; }
        
        public string Full_Branch_title_part_1 { get; set; }
        
        public string Full_Branch_title_part_2 { get; set; }
        
        public string Full_Branch_title_part_3 { get; set; }
        
        public string Address_line_1 { get; set; }
        
        public string Address_line_2 { get; set; }
        
        public string Address_line_3 { get; set; }
        
        public string Address_line_4 { get; set; }
        
        public string Address_Town { get; set; }
        
        public string Address_County { get; set; }
        
        public string Post_Code_major_part { get; set; }
        
        public string Post_Code_minor_part { get; set; }
        
        public string Telephone_1_area { get; set; }
        
        public string Telephone_1_number { get; set; }
        
        public string Telephone_2_area { get; set; }
        
        public string Telephone_2_number { get; set; }
        
        public string Supervisory_Body { get; set; }
        
        public string National_Central_Bank_Country_Code { get; set; }
        
        public string Bank_Code_of_owning_bank { get; set; }
        
        public string Full_name_of_owning_bank_line_2 { get; set; }
        
        public string Full_name_of_owning_bank_line_1 { get; set; }
        
        public string Short_name_of_owning_bank { get; set; }
        
        public string Short_Branch_title { get; set; }
        
        public string Sub_branch_suffix { get; set; }
        
        public string BIC_branch { get; set; }
        
        public string BIC_bank { get; set; }
        
        public string sortcode { get; set; }
        
        public string Transposed_account_number { get; set; }
        
        public string Deleted_Date { get; set; }
        
        public string CCCC_Return_Indicator { get; set; }
        
        public string CCCC_Effective_date_of_last_change { get; set; }
        
        public string CCCC_Status { get; set; }
        
        public string CHAPSE_Return_indicator { get; set; }
        
        public string CHAPS_Euro_settlement_member { get; set; }
        
        public string CHAPS_Euro_routing_BIC_branch { get; set; }
        
        public string CHAPS_Euro_routing_BIC_bank { get; set; }
        
        public string Date_closed_in_CHAPS_Euro_clearing { get; set; }
        
        public string CHAPSE_Effective_date_of_last_change { get; set; }
        
        public string CHAPSE_Status { get; set; }
        
        public string CHAPS_routing_BIC_branch { get; set; }
        
        public string CHAPS_routing_BIC_bank { get; set; }
        
        public byte CHAPS_settlement_member { get; set; }
        
        public string Date_closed_in_CHAPS_clearing { get; set; }
        
        public string CHAPS_Status { get; set; }
        
        public string BACS_Date_of_last_change { get; set; }
        
        public string FPS_Status { get; set; }
        
        public string FPS_DateLastChanged { get; set; }
        
        public string FPS_DateClosed { get; set; }
        
        public string FPS_RedirectionFromFlag { get; set; }
        
        public string FPS_RedirectionToSortCode { get; set; }
        
        public string FPS_SettlementBankConnection { get; set; }
        
        public string FPS_SettlementBankCode { get; set; }
        
        public string FPS_HandlingBankConnection { get; set; }
        
        public string FPS_HandlingBankCode { get; set; }
        
        public string FPS_AccountNumbersFlag { get; set; }
        
        public string FPS_AgencyType { get; set; }
    }
}