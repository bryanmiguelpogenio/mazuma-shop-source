﻿using CallCredit.CallValidateApi.Enums;
using System;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class BankCheckEnhanced
    {
        public string Result { get; set; }

        public int Score { get; set; }

        public string AccountIssuer { get; set; }

        public string OtherAccountsFoundForIssuer { get; set; }

        public Ofa OFA { get; set; }

        public BankAccountPlus BankAccountPlus { get; set; }

        public DateTime AccountStartDate { get; set; }

        public string AccountRecencyWarning { get; set; }
    }
}
