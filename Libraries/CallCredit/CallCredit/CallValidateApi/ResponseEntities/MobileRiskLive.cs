﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class MobileRiskLive
    {
        public string CarrierConfidence { get; set; }

        public string NumberReachable { get; set; }

        public string NumberStatus { get; set; }

        public string Roaming { get; set; }

        public string RoamingCountry { get; set; }
    }
}
