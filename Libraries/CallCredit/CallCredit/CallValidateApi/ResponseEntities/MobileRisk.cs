﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class MobileRisk
    {
        public MobileRiskStandard Standard { get; set; }

        public MobileRiskLive Live { get; set; }

        public MobileRiskScore Score { get; set; }
    }
}
