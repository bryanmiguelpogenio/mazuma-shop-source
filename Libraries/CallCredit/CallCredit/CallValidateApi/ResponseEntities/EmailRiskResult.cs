﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class EmailRiskResult
    {
        public string Score { get; set; }

        public string RiskLevel { get; set; }

        public string Desciption { get; set; }

        public string DescriptionID { get; set; }
    }
}
