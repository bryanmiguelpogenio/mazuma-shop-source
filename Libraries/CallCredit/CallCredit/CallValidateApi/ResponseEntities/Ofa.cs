﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    /// <summary>
    /// Ofa (Ownership Fraud Alert)
    /// </summary>
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class Ofa
    {
        public string OFAType { get; set; }

        public string MismatchLevel { get; set; }

        public OfaName Name { get; set; }

        public OfaAddress Address { get; set; }

        public bool? JointAccount { get; set; }

        public bool? OFA_Match { get; set; }

        public string AccountMatchConfidence { get; set; }
    }
}
