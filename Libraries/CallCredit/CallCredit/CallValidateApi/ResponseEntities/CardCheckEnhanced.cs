﻿using CallCredit.CallValidateApi.Enums;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class CardCheckEnhanced
    {
        public string Result { get; set; }

        public int Score { get; set; }

        public string CardIssuer { get; set; }

        public string Country { get; set; }

        public string Scheme { get; set; }

        public string Type { get; set; }

        public string OtherAccountsFoundForIssuer { get; set; }

        public Ofa OFA { get; set; }
    }
}
