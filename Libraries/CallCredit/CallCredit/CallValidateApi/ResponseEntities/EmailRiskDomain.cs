﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class EmailRiskDomain
    {
        public string Status { get; set; }

        public string RegistrationDate { get; set; }
    }
}
