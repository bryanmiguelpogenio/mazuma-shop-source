﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class NcoaAlert
    {
        public bool? NCOAAlertFlag { get; set; }
    }
}
