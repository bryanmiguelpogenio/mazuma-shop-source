﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class MobileRiskStandard
    {
        public string Carrier { get; set; }

        public string LocationCity { get; set; }

        public string LocationCountry { get; set; }

        public string PhoneTypeCode { get; set; }

        public string PhoneTypeDesciption { get; set; }
    }
}
