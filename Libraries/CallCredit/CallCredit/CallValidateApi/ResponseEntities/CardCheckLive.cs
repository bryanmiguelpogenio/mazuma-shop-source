﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class CardCheckLive
    {
        public string AVS_result { get; set; }

        public string CV2_result { get; set; }

        public string CardLiveResult { get; set; }

        public string Country { get; set; }

        public string Scheme { get; set; }

        public string Issuer { get; set; }

        public string Type { get; set; }

        public string Diagnostics { get; set; }

        public string ExtendedAddressResult { get; set; }

        public string ExtendedPostcodeResult { get; set; }

        public string ExtendedCV2Result { get; set; }
    }
}
