﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class RealTimeFraudAlertInconsistencyGroup
    {
        [XmlAttributeAttribute()]
        public string Type { get; set; }

        [XmlElement(ElementName = "Inconsistency")]
        public List<RealTimeFraudAlertInconsistency> Inconsistency { get; set; }
    }
}
