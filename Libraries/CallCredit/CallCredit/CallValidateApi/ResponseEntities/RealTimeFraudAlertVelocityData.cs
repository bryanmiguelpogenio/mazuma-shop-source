﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class RealTimeFraudAlertVelocityData
    {
        [XmlElement(ElementName = "Velocity")]
        public List<RealTimeFraudAlertVelocity> Velocity { get; set; }
    }
}
