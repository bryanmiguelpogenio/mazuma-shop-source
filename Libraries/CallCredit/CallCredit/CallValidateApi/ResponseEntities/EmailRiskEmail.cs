﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class EmailRiskEmail
    {
        public string CreationDate { get; set; }

        public string FirstVerified { get; set; }

        public string Status { get; set; }
    }
}
