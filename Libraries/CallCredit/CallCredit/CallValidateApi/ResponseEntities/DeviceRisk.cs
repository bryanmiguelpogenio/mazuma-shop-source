﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class DeviceRisk
    {
        public string DataSourceName { get; set; }

        public string Recommendation { get; set; }

        public DeviceRiskDataFields DataFields { get; set; }

        public string Reason { get; set; }

        public int RulesMatched { get; set; }
    }
}
