﻿using CallCredit.CallValidateApi.Enums;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class IdentityCheck
    {
        public bool? addresslinkswarning { get; set; }
        
        public bool? addresspicklistfound { get; set; }
        
        public bool? addresswarning { get; set; }
        
        public string appverified { get; set; }
        
        public Cifas cifas { get; set; }

        public int confirmatorydobs { get; set; }

        public string currentaddressmatched { get; set; }

        public bool? deceasedwarning { get; set; }

        public bool? directorathome { get; set; }

        public bool? dvlawarning { get; set; }

        public string ervalid { get; set; }

        public string forwardingaddress { get; set; }

        public bool? fraudulentpassportwarning { get; set; }

        public bool? goneawaywarning { get; set; }

        public bool? grodeceased { get; set; }

        public bool? halomatch { get; set; }

        /// <remarks>
        /// Not currently populated
        /// </remarks>
        public object IDBasic { get; set; }

        public int levelofconfidencebai { get; set; }

        public int levelofconfidenceccjs { get; set; }

        public int levelofconfidencedob { get; set; }

        public int levelofconfidenceer { get; set; }

        public int levelofconfidencehalo { get; set; }

        public int levelofconfidenceinvestors { get; set; }

        public int levelofconfidenceshare { get; set; }

        public bool? lorwarning { get; set; }

        public string matchlevel { get; set; }

        public string namematched { get; set; }

        public bool? namepicklistfound { get; set; }

        public string nocs { get; set; }

        public int numactivecifasrecords { get; set; }

        public int numaddresslinks { get; set; }

        public int numbais { get; set; }

        public int numccjs { get; set; }

        public int numcorroborativechecks { get; set; }

        public int numcorroborativeidsconfirmed { get; set; }

        public int numcorroborativeotheridsconfirmed { get; set; }

        public int numinvestors { get; set; }

        public int numprimarychecks { get; set; }

        public int numprimaryotheridsconfirmed { get; set; }

        public int numsharerecords { get; set; }

        public bool? pafvalid { get; set; }

        public bool? passportwarning { get; set; }

        public bool? pepwarning { get; set; }

        public bool? readmatch { get; set; }

        public bool? sanctionswarning { get; set; }

        public bool? sdnwarning { get; set; }

        public bool? sharelorwarning { get; set; }

        public int totaldobs { get; set; }
    }
}
