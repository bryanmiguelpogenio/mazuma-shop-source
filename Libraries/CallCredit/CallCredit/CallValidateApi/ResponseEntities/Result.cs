﻿using System;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class Result
    {
        [XmlAttributeAttribute()]
        public string RID { get; set; }

        [XmlAttributeAttribute()]
        public string PID { get; set; }

        [XmlAttributeAttribute()]
        public string DateTime { get; set; }

        public Displays Displays { get; set; }
    }
}