﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [Serializable, XmlRoot("Results")]
    public partial class Results
    {
        public string RequestXml { get; set; }
        public string ResultXml { get; set; }

        [XmlAttributeAttribute()]
        public string APIVERSION { get; set; }

        public Result Result { get; set; }
        
        public Errors Errors { get; set; }
    }
}