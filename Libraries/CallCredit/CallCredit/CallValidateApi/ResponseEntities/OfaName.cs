﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class OfaName
    {
        public bool? NameMismatch { get; set; }

        public string NameData { get; set; }
    }
}
