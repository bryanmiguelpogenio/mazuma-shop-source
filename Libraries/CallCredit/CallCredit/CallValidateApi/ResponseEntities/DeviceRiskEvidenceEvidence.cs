﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class DeviceRiskEvidenceEvidence
    {
        public string Source { get; set; }

        public string Type { get; set; }
    }
}
