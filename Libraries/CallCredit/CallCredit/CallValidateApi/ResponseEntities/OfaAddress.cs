﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class OfaAddress
    {
        public bool? AddressMismatch { get; set; }

        public string PostcodeFound { get; set; }
    }
}
