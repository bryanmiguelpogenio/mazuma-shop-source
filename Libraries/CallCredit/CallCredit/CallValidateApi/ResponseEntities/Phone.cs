﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class Phone
    {
        public MobileRisk MobileRisk { get; set; }
    }
}
