﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class Question
    {
        [XmlAttributeAttribute()]
        public int priority { get; set; }

        [XmlAttributeAttribute()]
        public string category { get; set; }

        [XmlAttributeAttribute()]
        public string responses { get; set; }

        [XmlAttributeAttribute()]
        public int strength { get; set; }

        [XmlAttributeAttribute()]
        public int weight { get; set; }

        public string QuestionText { get; set; }

        public QuestionResponse Responses { get; set; }
    }
}
