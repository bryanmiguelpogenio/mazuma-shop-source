﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class QuestionResponse
    {
        /// <remarks>
        /// Note: Unable to determine if correct or not (should have attribute "correct")
        /// </remarks>
        [XmlElement(ElementName = "ResponseText")]
        public List<string> ResponseText { get; set; }
    }
}
