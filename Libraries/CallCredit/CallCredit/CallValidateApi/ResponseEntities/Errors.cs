﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class Errors
    {
        [XmlElement(ElementName = "Error")]
        public List<string> Error { get; set; }

        [XmlElement(ElementName = "FatalError")]
        public List<string> FatalError { get; set; }
    }
}