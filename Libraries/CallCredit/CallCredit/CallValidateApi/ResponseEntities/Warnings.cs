﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class Warnings
    {
        public bool? PrepayWarning { get; set; }

        public bool? NonGBRCardWarning { get; set; }

        public bool? NamePicklistWarning { get; set; }

        public bool? AddressPicklistWarning { get; set; }

        public bool? PAFNonValidWarning { get; set; }
        
        public bool? CardAccountClosedWarning { get; set; }
        
        public bool? BankAccountClosedWarning { get; set; }
    }
}
