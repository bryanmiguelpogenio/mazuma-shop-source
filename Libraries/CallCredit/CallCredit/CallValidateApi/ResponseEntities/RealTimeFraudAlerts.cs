﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class RealTimeFraudAlerts
    {
        public RealTimeFraudAlertSummary Summary { get; set; }

        public RealTimeFraudAlertVelocityData VelocityData { get; set; }

        public RealTimeFraudAlertInconsistencyData InconsistencyData { get; set; }
    }
}
