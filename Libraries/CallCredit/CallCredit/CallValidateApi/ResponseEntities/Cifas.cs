﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class Cifas
    {
        public CifasRecord cifasrecord { get; set; }
    }
}
