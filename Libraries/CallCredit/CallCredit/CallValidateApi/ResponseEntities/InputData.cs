﻿using CallCredit.CallValidateApi.RequestEntities;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class InputData
    {
        public IndividualDetails Individual { get; set; }

        public AddressDetails Address { get; set; }
    }
}
