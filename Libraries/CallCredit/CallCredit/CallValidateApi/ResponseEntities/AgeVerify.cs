﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class AgeVerify
    {
        public string Age { get; set; }

        public string DateOfBirth { get; set; }

        public string Over18 { get; set; }
    }
}
