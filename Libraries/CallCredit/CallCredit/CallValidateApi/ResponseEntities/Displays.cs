﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class Displays
    {
        public ChecksCompleted ChecksCompleted { get; set; }

        public InputData InputData { get; set; }

        public BankCheckStandard BankcheckStandard { get; set; }

        public BankCheckEnhanced BankcheckEnhanced { get; set; }

        public CardCheckEnhanced CardcheckEnhanced { get; set; }

        public CardCheckLive CardcheckLive { get; set; }

        public IdentityCheck IdentityCheck { get; set; }

        public NcoaAlert NCOAAlert { get; set; }

        public CallValidate3D CallValidate3D { get; set; }

        public DeliveryFraudCheck DeliveryfraudCheck { get; set; }

        public AgeVerify AgeVerify { get; set; }

        public OtherChecks OtherChecks { get; set; }

        public RealTimeFraudAlerts RealTimeFraudAlerts { get; set; }

        public DeviceRisk DeviceRisk { get; set; }

        public Phone Phone { get; set; }

        public Email Email { get; set; }

        public Warnings Warnings { get; set; }
    }
}
