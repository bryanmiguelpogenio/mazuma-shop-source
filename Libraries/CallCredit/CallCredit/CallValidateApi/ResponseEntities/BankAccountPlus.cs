﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class BankAccountPlus
    {
        public bool? BankAccountPlusFlag { get; set; }
    }
}
