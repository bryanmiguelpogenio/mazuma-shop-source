﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class RealTimeFraudAlertSummary
    {
        public int VelocityThreshold { get; set; }

        public string VelocityPeriod { get; set; }

        public int InconsistencyThreshold { get; set; }

        public string InconsistencyPeriod { get; set; }

        public int TotalVelociyWarnings { get; set; }

        public int TotalInconsistencyWarnings { get; set; }
    }
}
