﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class MobileRiskScore
    {
        public string RiskLevel { get; set; }

        public string RiskScore { get; set; }
    }
}
