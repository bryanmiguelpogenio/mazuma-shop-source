﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class Email
    {
        public EmailRisk EmailRisk { get; set; }
    }
}
