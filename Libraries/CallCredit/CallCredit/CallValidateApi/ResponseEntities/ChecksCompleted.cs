﻿using CallCredit.CallValidateApi.Enums;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class ChecksCompleted
    {
        public string BankStandard { get; set; }
        
        public string BankEnhanced { get; set; }
        
        public string CardLive { get; set; }
        
        public string CardEnhanced { get; set; }
        
        public string IDEnhanced { get; set; }
        
        public string NCOAAlert { get; set; }
        
        public string CallValidate3D { get; set; }
        
        public string TheAffordabilityReport { get; set; }
        
        public string DeliveryFraud { get; set; }
        
        public string CreditScore { get; set; }
        
        public string Zodiac { get; set; }
        
        public string BankAccountPlus { get; set; }
        
        public string BankOFA { get; set; }
        
        public string CardOFA { get; set; }
        
        public string RealTimeFraudAlerts { get; set; }
        
        public string DeviceRisk { get; set; }
        
        public string MobileRisk { get; set; }
        
        public string EmailRisk { get; set; }
    }
}
