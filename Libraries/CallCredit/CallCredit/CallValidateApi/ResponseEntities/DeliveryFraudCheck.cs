﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.ResponseEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class DeliveryFraudCheck
    {
        public int Current_Postcode_status { get; set; }

        public int Current_Fraud_indicator { get; set; }

        public string Previous_Postcode_status { get; set; }

        public int Previous_Fraud_indicator { get; set; }

        public bool? Previous_to_Current_Warning { get; set; }
    }
}
