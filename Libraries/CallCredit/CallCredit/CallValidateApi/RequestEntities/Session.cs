﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.RequestEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class Session
    {
        public Data data { get; set; }

        [XmlAttributeAttribute()]
        public string RID { get; set; }
    }
}
