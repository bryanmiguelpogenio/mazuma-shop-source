﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.RequestEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class Authentication
    {
        public string company { get; set; }
        
        public string username { get; set; }
        
        public string password { get; set; }
    }
}
