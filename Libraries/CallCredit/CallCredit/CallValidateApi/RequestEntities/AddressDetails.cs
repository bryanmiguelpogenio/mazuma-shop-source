﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.RequestEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class AddressDetails
    {
        #region Current

        public string Abodenumber { get; set; }
        
        public string Buildingnumber { get; set; }
        
        public string Buildingname { get; set; }
        
        public string Address1 { get; set; }
        
        public string Address2 { get; set; }
        
        public string Address3 { get; set; }
        
        public string Town { get; set; }
        
        public string Postcode { get; set; }

        #endregion

        #region Previous

        public string Previousabodenumber { get; set; }
        
        public string Previousbuildingnumber { get; set; }
        
        public string Previousbuildingname { get; set; }
        
        public string Previousaddress1 { get; set; }
        
        public string Previousaddress2 { get; set; }
        
        public string Previousaddress3 { get; set; }
        
        public string Previoustown { get; set; }
        
        public string Previouspostcode { get; set; }

        #endregion

        #region Delivery

        public string Deliveryabodenumber { get; set; }
        
        public string Deliverybuildingnumber { get; set; }
        
        public string Deliverybuildingname { get; set; }
        
        public string Deliveryaddress1 { get; set; }
        
        public string Deliveryaddress2 { get; set; }
        
        public string Deliveryaddress3 { get; set; }
        
        public string Deliverytown { get; set; }
        
        public string Deliverypostcode { get; set; }

        #endregion
    }
}