﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.RequestEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class Data
    {
        public ChecksRequired ChecksRequired { get; set; }
        
        public PersonalInformation Personalinformation { get; set; }
        
        public CardInformation Cardinformation { get; set; }
        
        public BankInformation Bankinformation { get; set; }
        
        public IncomeDetails IncomeDetails { get; set; }
        
        public DeviceDetails DeviceDetails { get; set; }
    }
}