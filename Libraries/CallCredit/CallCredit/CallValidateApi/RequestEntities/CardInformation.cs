﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.RequestEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class CardInformation
    {
        public string Cardnumber { get; set; }
        
        public string Cardexpirydate { get; set; }
        
        public string Cardstartdate { get; set; }
        
        public string CardCV2 { get; set; }
        
        public int Cardissuenumber { get; set; }
        
        public decimal Amount { get; set; }
    }
}