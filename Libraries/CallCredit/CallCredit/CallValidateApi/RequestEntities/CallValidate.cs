﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.RequestEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "", ElementName = "callvalidate", IsNullable = false)]
    public partial class CallValidate
    {
        public Authentication authentication { get; set; }
        
        public Sessions sessions { get; set; }
        
        public string application { get; set; }

        #region Constructors

        public CallValidate()
        {

        }

        public CallValidate(string company, string username, string password, string application)
        {
            this.authentication = new Authentication
            {
                company = company,
                username = username,
                password = password
            };

            this.application = application;
        }

        #endregion
    }
}