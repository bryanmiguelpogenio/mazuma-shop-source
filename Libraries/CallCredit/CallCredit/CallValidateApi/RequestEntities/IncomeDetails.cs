﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.RequestEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class IncomeDetails
    {
        public string netmonthlyincome { get; set; }
        
        public string annualgrossincome { get; set; }
    }
}
