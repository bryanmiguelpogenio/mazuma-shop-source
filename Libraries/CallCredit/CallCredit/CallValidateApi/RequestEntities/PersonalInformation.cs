﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.RequestEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class PersonalInformation
    {
        public IndividualDetails IndividualDetails { get; set; }
      
        public AddressDetails AddressDetails { get; set; }
    }
}
