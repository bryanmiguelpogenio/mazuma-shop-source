﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.RequestEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class DeviceDetails
    {
        public string DeviceRisk_AccountCode { get; set; }
        
        public string DeviceRisk_BlackBox { get; set; }
        
        public string DeviceRisk_EndUserIp { get; set; }
    }
}
