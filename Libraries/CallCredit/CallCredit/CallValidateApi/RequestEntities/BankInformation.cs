﻿using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.RequestEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class BankInformation
    {
        public string Bankaccountnumber { get; set; }

        public string Banksortcode { get; set; }
    }
}
