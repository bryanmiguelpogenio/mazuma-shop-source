﻿using System;
using System.Xml.Serialization;

namespace CallCredit.CallValidateApi.RequestEntities
{
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class IndividualDetails
    {
        public string Dateofbirth { get; set; }
        
        public string Title { get; set; }
        
        public string Firstname { get; set; }
        
        public string Othernames { get; set; }
        
        public string Surname { get; set; }
        
        public string Phonenumber { get; set; }
        
        public string Emailaddress { get; set; }
        
        public string Addresslessthan12months { get; set; }
        
        public string Passportline1 { get; set; }
        
        public string Passportline2 { get; set; }
        
        public string Drivinglicensenumber { get; set; }
        
        public string PassportExpiryDate { get; set; }
        
        public string IPAddress { get; set; }
    }
}