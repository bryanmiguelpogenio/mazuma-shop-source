﻿using System.Xml.Serialization;

namespace CallCredit.Extensions
{
    public static class ObjectExtensions
    {
        public static string ToXmlString(this object item)
        {
            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(item.GetType());
            serializer.Serialize(stringwriter, item);
            return stringwriter.ToString();
        }
    }
}
