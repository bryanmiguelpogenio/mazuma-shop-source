﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace CallCredit.Extensions
{
    public static class XmlExtensions
    {
        public static T ToObject<T>(this XmlNode xml)
        {
            var serializer = new XmlSerializer(typeof(T));

            using (var reader = new StringReader(xml.OuterXml))
            {
                return (T)serializer.Deserialize(reader);
            }
        }
    }
}
