﻿using Nop.Core;
using Nop.Core.Domain.Configuration;

namespace Nop.Services.Configuration
{
    public partial interface IProductInsuranceSettingService
    {
        IPagedList<ProductInsuranceSetting> GetAllProductInsuranceSetting(
            int productType = 0,
            int pageIndex = 0,
            int pageSize = int.MaxValue
           );
        ProductInsuranceSetting GetProductInsuranceSettingById(int id);
        void InsertProductInsuranceSetting(ProductInsuranceSetting productInsuranceSetting);
        void UpdateProductInsuranceSetting(ProductInsuranceSetting productInsuranceSetting);
        void DeleteProductInsuranceSetting(ProductInsuranceSetting productInsuranceSetting);
    }
}
