﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Configuration;
using Nop.Services.Events;

namespace Nop.Services.Configuration
{
    public partial class ProductInsuranceSettingService : IProductInsuranceSettingService
    {
        #region Fields
        private readonly IRepository<ProductInsuranceSetting> _productInsuranceSettingRepository;
        private readonly IEventPublisher _eventPublisher;
        #endregion

        #region Ctor
        public ProductInsuranceSettingService(IRepository<ProductInsuranceSetting> productInsuranceSettingRepository,
            IEventPublisher eventPublisher)
        {
            this._productInsuranceSettingRepository = productInsuranceSettingRepository;
            this._eventPublisher = eventPublisher;
        }
        #endregion

        #region Methods
        public void DeleteProductInsuranceSetting(ProductInsuranceSetting productInsuranceSetting)
        {
            if (productInsuranceSetting == null)
                throw new ArgumentNullException(nameof(productInsuranceSetting));

            _productInsuranceSettingRepository.Delete(productInsuranceSetting);

            //event notification
            _eventPublisher.EntityDeleted(productInsuranceSetting);
        }

        public ProductInsuranceSetting GetProductInsuranceSettingById(int productInsuranceSettingId)
        {
            if (productInsuranceSettingId == 0)
                return null;

            return _productInsuranceSettingRepository.GetById(productInsuranceSettingId);
        }

        public void InsertProductInsuranceSetting(ProductInsuranceSetting productInsuranceSetting)
        {
            if (productInsuranceSetting == null)
                throw new ArgumentNullException(nameof(productInsuranceSetting));

            _productInsuranceSettingRepository.Insert(productInsuranceSetting);
            _eventPublisher.EntityInserted(productInsuranceSetting);

        }

        public virtual IPagedList<ProductInsuranceSetting> GetAllProductInsuranceSetting(
            int productType = 0,
               int pageIndex = 0,
            int pageSize = int.MaxValue
            )
        {

            var query = _productInsuranceSettingRepository.Table;
            if (productType > 0)
            {
                query = query.Where(p => p.ProductType == productType);
            }
            query = query.OrderByDescending(b => b.Id);
            return new PagedList<ProductInsuranceSetting>(query, pageIndex, pageSize);
        }

        public void UpdateProductInsuranceSetting(ProductInsuranceSetting productInsuranceSetting)
        {
            if (productInsuranceSetting == null)
                throw new ArgumentNullException(nameof(productInsuranceSetting));

            _productInsuranceSettingRepository.Update(productInsuranceSetting);

            //event notification
            _eventPublisher.EntityUpdated(productInsuranceSetting);
        }
        #endregion    
    }
}
