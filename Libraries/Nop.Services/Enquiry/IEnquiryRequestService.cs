﻿using Nop.Core;
using Nop.Core.Domain.Enquiry;
using System;
using System.Collections.Generic;

namespace Nop.Services.Enquiry
{
    public partial interface IEnquiryRequestService
    {
        IPagedList<EnquiryRequest> GetListByPage(
            int customerId = 0,
            string requestNumber = "",
            string orderNumber = "",
            string userEmail = "",
            string requestStatusId = "",
            int enquiryTicketReasonId = 0,
            DateTime? createdFromUtc = null,
            DateTime? createdToUtc = null,
            int pageIndex = 0,
            int pageSize = int.MaxValue
            );
        EnquiryRequest GetById(int id);
        void Insert(EnquiryRequest input);
        void Update(EnquiryRequest input);
        void Delete(EnquiryRequest input);

        IList<EnquiryTicketReason> GetAllEnquiryTicketReasons();

        void InsertReplyRecord(EnquiryReplyRecord input);
    }
}
