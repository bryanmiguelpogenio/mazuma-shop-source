﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Enquiry;
using Nop.Core.Domain.Orders;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.Enquiry
{
    public partial class EnquiryRequestService : IEnquiryRequestService
    {
        private readonly IRepository<EnquiryRequest> _enquiryRequestRepository;
        private readonly IRepository<EnquiryTicketReason> _enquiryTicketReasonRepository;
        private readonly IRepository<EnquiryReplyRecord> _enquiryReplyRecordRepository;
        private readonly IRepository<Order> _orderRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;

        public EnquiryRequestService(IRepository<EnquiryRequest> enquiryRequestRepository,
            IRepository<EnquiryTicketReason> enquiryTicketReasonRepository,
            IRepository<EnquiryReplyRecord> enquiryReplyRecordRepository,
            IRepository<Order> orderRepository,
            IEventPublisher eventPublisher,
            ICacheManager cacheManager)
        {
            _enquiryRequestRepository = enquiryRequestRepository;
            _enquiryTicketReasonRepository = enquiryTicketReasonRepository;
            _enquiryReplyRecordRepository = enquiryReplyRecordRepository;
            _orderRepository = orderRepository;
            _eventPublisher = eventPublisher;
            _cacheManager = cacheManager;
        }


        public virtual IPagedList<EnquiryRequest> GetListByPage(
            int customerId = 0,
            string requestNumber = "",
            string orderNumber = "",
            string userEmail = "",
            string requestStatusId = "",
            int enquiryTicketReasonId = 0,
            DateTime? createdFromUtc = null,
            DateTime? createdToUtc = null,
            int pageIndex = 0,
            int pageSize = int.MaxValue
            )
        {
            var query = _enquiryRequestRepository.Table;
            if (customerId > 0)
                query = query.Where(b => b.CustomerId.HasValue && b.CustomerId.Value == customerId);
            if (!string.IsNullOrEmpty(requestNumber))
                query = query.Where(b => b.RequestNumber.Contains(requestNumber));
            if (!string.IsNullOrEmpty(orderNumber))
                query = query.Where(b => b.OrderNumber.Contains(orderNumber));
            if (!string.IsNullOrEmpty(userEmail))
                query = query.Where(b => b.UserEmail.Contains(userEmail));
            if (!string.IsNullOrEmpty(requestStatusId))
                query = query.Where(b => b.RequestStatusId.ToString() == requestStatusId);
            if (enquiryTicketReasonId > 0)
                query = query.Where(b => b.EnquiryTicketReasonId == enquiryTicketReasonId);
            if (createdFromUtc.HasValue)
            {
                var fromDate = createdFromUtc.Value.Date;
                query = query.Where(b => b.CreatedOnUtc >= fromDate);
            }
            if (createdToUtc.HasValue)
            {
                var todate = createdToUtc.Value.Date.AddDays(1);
                query = query.Where(b => b.CreatedOnUtc < todate);
            }

            query = query.OrderBy(x => x.RequestStatusId).ThenByDescending(b => b.LastUpdatedOnUtc);
            return new PagedList<EnquiryRequest>(query, pageIndex, pageSize);
        }



        public EnquiryRequest GetById(int id)
        {
            if (id == 0)
                return null;

            return _enquiryRequestRepository.GetById(id);
        }

        public void Insert(EnquiryRequest input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));
            if (!string.IsNullOrEmpty(input.OrderNumber))
            {
                var order = _orderRepository.Table.FirstOrDefault(x => x.CustomOrderNumber == input.OrderNumber);
                if (order != null)
                    input.OrderId = order.Id;
            }
            input.CreatedOnUtc = DateTime.UtcNow;
            input.LastUpdatedOnUtc = DateTime.UtcNow;

            _enquiryRequestRepository.Insert(input);
            input.RequestNumber = (1000 + input.Id).ToString();
            _enquiryRequestRepository.Update(input);
            _eventPublisher.EntityInserted(input);
        }

        public void Update(EnquiryRequest input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));
            input.LastUpdatedOnUtc = DateTime.UtcNow;

            _enquiryRequestRepository.Update(input);
            //event notification
            _eventPublisher.EntityUpdated(input);
        }


        public void Delete(EnquiryRequest input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            //delete entity
            _enquiryRequestRepository.Delete(input);

            //event notification
            _eventPublisher.EntityDeleted(input);
        }

        public IList<EnquiryTicketReason> GetAllEnquiryTicketReasons()
        {
            var cacheKey = "NOP_SERVICE_CacheKey_AllEnquiryTicketReasons";
            var model = _cacheManager.Get(cacheKey, () => _enquiryTicketReasonRepository.Table.Where(x => x.IsActive).ToList());

            return model;
        }


        public void InsertReplyRecord(EnquiryReplyRecord input)
        {
            if (input == null || input.EnquiryRequestId <= 0)
                throw new ArgumentNullException(nameof(input));
            var enquiryRequest = _enquiryRequestRepository.Table.FirstOrDefault(x => x.Id == input.EnquiryRequestId);
            if (enquiryRequest == null)
            {
                throw new ArgumentNullException(nameof(input));
            }
            _enquiryReplyRecordRepository.Insert(input);
            //update Enquiry Request
            enquiryRequest.RequestStatusId = input.IsFromClient ? EnquiryTicketStatus.Pending.GetHashCode() : EnquiryTicketStatus.Replied.GetHashCode();
            Update(enquiryRequest);

            _eventPublisher.EntityInserted(input);
        }
    }
}
