using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Common;

namespace Nop.Services.Common
{
    /// <summary>
    /// IP Address service interface
    /// </summary>
    public partial interface IPopupIpAddressService
    {
        /// <summary>
        /// Gets a popup
        /// </summary>
        /// <param name="popupId">popup identifier</param>
        /// <returns>A popup</returns>
        PopupIpAddress GetPopupById(int popupId);

        /// <summary>
        /// Get a popup by ip address and page name
        /// </summary>
        /// <param name="ipAddress">ip address</param>
        /// <param name="pages">page name</param>
        /// <returns></returns>
        PopupIpAddress GetPopupByPageIp(string ipAddress, int pages);

        /// <summary>
        /// Inserts a popup
        /// </summary>
        /// <param name="popup"></param>
        void InsertPopup(PopupIpAddress popup);

        /// <summary>
        /// Updates the popup
        /// </summary>
        /// <param name="popup"></param>
        void UpdatePopup(PopupIpAddress popup);
    }
}