﻿using System;
using System.Net;
using System.Text;
using Nop.Core;
using Nop.Services.Tasks;
using RestSharp;
using RestSharp.Authenticators;

namespace Nop.Services.Common
{
    /// <summary>
    /// Represents a task for keeping the site alive
    /// </summary>
    public partial class KeepAliveTask : IScheduleTask
    {
        private readonly IStoreContext _storeContext;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="storeContext">Store context</param>
        public KeepAliveTask(IStoreContext storeContext)
        {
            this._storeContext = storeContext;
        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public void Execute()
        {
            var url = _storeContext.CurrentStore.Url + "keepalive/index";
            using (var wc = new WebClient())
            {
                //String username = "ecorenew.developer";
                //String password = "Ec0R3NewDev101";

                //string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(username + ":" + password));
                //wc.Headers.Add("Authorization", "Basic " + credentials);

                wc.DownloadString(url);
            }

            //string username = "ecorenew.developer";
            //string password = "Ec0R3NewD3v101";

            //var client = new RestClient
            //{
            //    Authenticator = new HttpBasicAuthenticator(username, password)
            //};

            //var request = new RestRequest(url);

            //client.Get(request);
        }
    }
}
