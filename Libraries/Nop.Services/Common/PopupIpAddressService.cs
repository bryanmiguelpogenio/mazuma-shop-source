using System;
using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Services.Events;

namespace Nop.Services.Common
{
    /// <summary>
    /// PopupIpAddress service
    /// </summary>
    public partial class PopupIpAddressService : IPopupIpAddressService
    {
        #region Fields

        private readonly IRepository<PopupIpAddress> _popupIpAddressRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor
        
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="popupIpAddressRepository">popupIpAddress repository</param>
        /// <param name="eventPublisher">Event published</param>
        public PopupIpAddressService(IRepository<PopupIpAddress> popupIpAddressRepository,
            IEventPublisher eventPublisher)
        {
            this._popupIpAddressRepository = popupIpAddressRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a popup
        /// </summary>
        /// <param name="popupId">popup identifier</param>
        /// <returns>A popup</returns>
        public virtual PopupIpAddress GetPopupById(int popupId)
        {
            if (popupId == 0)
                return null;

            return _popupIpAddressRepository.GetById(popupId);
        }

        /// <summary>
        /// Get a popup by ip address and page name
        /// </summary>
        /// <param name="ipAddress">ip address</param>
        /// <param name="pages">page name</param>
        /// <returns></returns>
        public virtual PopupIpAddress GetPopupByPageIp(string ipAddress, int pages)
        {
            if (string.IsNullOrEmpty(ipAddress))
                return null;

            var query = from pia in _popupIpAddressRepository.Table
                        where pia.IpAddress == ipAddress &&
                        pia.Pages == pages
                        select pia;
            return query.FirstOrDefault();
        }

        /// <summary>
        /// Inserts a popup
        /// </summary>
        /// <param name="popup">popup</param>
        public virtual void InsertPopup(PopupIpAddress popup)
        {
            if (popup == null)
                throw new ArgumentNullException(nameof(popup));

            _popupIpAddressRepository.Insert(popup);
            
            //event notification
            _eventPublisher.EntityInserted(popup);
        }

        /// <summary>
        /// Updates the popup
        /// </summary>
        /// <param name="popup">popup</param>
        public virtual void UpdatePopup(PopupIpAddress popup)
        {
            if (popup == null)
                throw new ArgumentNullException(nameof(popup));

            _popupIpAddressRepository.Update(popup);

            //event notification
            _eventPublisher.EntityUpdated(popup);
        }
        
        #endregion
    }
}