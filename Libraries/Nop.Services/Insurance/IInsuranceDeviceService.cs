﻿using Nop.Core;
using Nop.Core.Domain.Insurance;

namespace Nop.Services.Insurance
{
    public partial interface IInsuranceDeviceService
    {
        IPagedList<InsuranceDevice> GetAllInsuranceDevice(int pageIndex = 0, int pageSize = int.MaxValue, string searchName = null);
        InsuranceDevice GetInsuranceDeviceById(int insuranceDeviceId);
        InsuranceDevice GetInsuranceDeviceByKey(string key);
        InsuranceDevice GetInsuranceDeviceByAttributesXml(string xml);
        void InsertInsuranceDevice(InsuranceDevice insuranceDevice);
        void UpdateInsuranceDevice(InsuranceDevice insuranceDevice);
        void DeleteInsuranceDevice(InsuranceDevice insuranceDevice);
    }
}
