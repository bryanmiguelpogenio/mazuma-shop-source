﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Insurance;
using Nop.Services.Catalog;
using Nop.Services.Events;

namespace Nop.Services.Insurance
{
    public partial class InsuranceDeviceService : IInsuranceDeviceService
    {

        #region Fields
        private readonly IRepository<InsuranceDevice> _insuranceDeviceRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly IProductAttributeParser _productAttributeParser;
        #endregion

        #region Ctor
        public InsuranceDeviceService(IRepository<InsuranceDevice> insuranceDeviceRepository,
            IEventPublisher eventPublisher,
            IProductAttributeParser productAttributeParser)
        {
            this._insuranceDeviceRepository = insuranceDeviceRepository;
            this._eventPublisher = eventPublisher;
            _productAttributeParser = productAttributeParser;
        }
        #endregion


        public void DeleteInsuranceDevice(InsuranceDevice insuranceDevice)
        {
            if (insuranceDevice == null)
                throw new ArgumentNullException(nameof(insuranceDevice));

            _insuranceDeviceRepository.Delete(insuranceDevice);

            //event notification
            _eventPublisher.EntityDeleted(insuranceDevice);
        }
        public virtual IPagedList<InsuranceDevice> GetAllInsuranceDevice(int pageIndex = 0, int pageSize = int.MaxValue, string searchName = null)
        {
            var query = _insuranceDeviceRepository.Table;
            if (!String.IsNullOrEmpty(searchName))
                query = query.Where(b => b.Model.Contains(searchName));
            query = query.OrderBy(i =>i.Id);
            var insuranceDevices = new PagedList<InsuranceDevice>(query, pageIndex, pageSize);

            return insuranceDevices;
        }
        public InsuranceDevice GetInsuranceDeviceById(int insuranceDeviceId)
        {
            if (insuranceDeviceId == 0)
                return null;

            return _insuranceDeviceRepository.GetById(insuranceDeviceId);
        }

        public InsuranceDevice GetInsuranceDeviceByKey(string key)
        {
            var query = _insuranceDeviceRepository.Table;
            if (string.IsNullOrEmpty(key))
                return null;

           query = query.Where(b => b.Key.Equals(key));
            return query.FirstOrDefault();
        }

        public InsuranceDevice GetInsuranceDeviceByAttributesXml(string attributesXml)
        {
            var attributeValues = _productAttributeParser.ParseProductAttributeValues(attributesXml);
            if (attributeValues.Any())
            {
                var attribute = attributeValues.Where(x => x.ProductAttributeMapping.ProductAttribute.Name.Contains("Storage")).FirstOrDefault();
                if (attribute != null)
                {
                    var insuranceDevice = GetInsuranceDeviceByKey(attribute.InsuranceDeviceKey);
                    return insuranceDevice;
                }

            }
            return null;
        }

        public void InsertInsuranceDevice(InsuranceDevice insuranceDevice)
        {
            if (insuranceDevice == null)
                throw new ArgumentNullException(nameof(insuranceDevice));

            _insuranceDeviceRepository.Insert(insuranceDevice);
            _eventPublisher.EntityInserted(insuranceDevice);
        }

        public void UpdateInsuranceDevice(InsuranceDevice insuranceDevice)
        {
            if (insuranceDevice == null)
                throw new ArgumentNullException(nameof(insuranceDevice));

            _insuranceDeviceRepository.Update(insuranceDevice);

            //event notification
            _eventPublisher.EntityUpdated(insuranceDevice);
        }
    }
}
