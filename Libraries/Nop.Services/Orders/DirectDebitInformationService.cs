﻿using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.Orders
{

    public partial class DirectDebitInformationService : IDirectDebitInformationService
    {

        #region Fields
        private readonly IRepository<DirectDebitInformation> _directDebitInformationRepository;
        private readonly IEventPublisher _eventPublisher;
        #endregion

        #region Ctor
        public DirectDebitInformationService(IRepository<DirectDebitInformation> directDebitInformationRepository,
            IEventPublisher eventPublisher)
        {
            this._eventPublisher = eventPublisher;
            this._directDebitInformationRepository = directDebitInformationRepository;
        }
        #endregion

        #region Method
        public virtual void DeleteDirectDebitInformation(DirectDebitInformation directDebitInformation)
        {
            if(directDebitInformation==null)
                throw new ArgumentNullException(nameof(directDebitInformation));
            _directDebitInformationRepository.Delete(directDebitInformation);

            //event notification
            _eventPublisher.EntityDeleted(directDebitInformation);

        }

        public virtual  DirectDebitInformation GetDirectDebitInformationById(int directDebitInformationId)
        {
            if (directDebitInformationId == 0)
                return null;
            return _directDebitInformationRepository.GetById(directDebitInformationId);
        }

        public virtual void InsertDirectDebitInformation(DirectDebitInformation directDebitInformation)
        {
            if (directDebitInformation == null)
                throw new ArgumentNullException(nameof(directDebitInformation));
            _directDebitInformationRepository.Insert(directDebitInformation);

            //event notification
            _eventPublisher.EntityInserted(directDebitInformation);
        }

        public virtual void UpdateDirectDebitInformation(DirectDebitInformation directDebitInformation)
        {
            if (directDebitInformation == null)
                throw new ArgumentNullException(nameof(directDebitInformation));
            _directDebitInformationRepository.Update(directDebitInformation);

            //event notification
            _eventPublisher.EntityUpdated(directDebitInformation);
        }

        public IList<DirectDebitInformation> GetAllDirectDebitInformation()
        {
            var query = _directDebitInformationRepository.Table;
            return query.ToList();
        }
        #endregion
    }
}
