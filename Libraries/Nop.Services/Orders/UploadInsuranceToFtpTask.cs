﻿using Nop.Core;
using Nop.Services.Configuration;
using Nop.Services.ExportImport;
using Nop.Services.Tasks;
using Renci.SshNet;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Nop.Services.Orders
{
    public partial class UploadInsuranceToFtpTask : IScheduleTask
    {
        private IExportManager _exportManager;
        private IOrderService _orderService;
        private ISettingService _settingService;
        private IDirectDebitInformationService _directDebitInformationService;

        public UploadInsuranceToFtpTask(IExportManager exportManager,
            IDirectDebitInformationService directDebitInformationService,
            IOrderService orderService,
            ISettingService settingService)
        {
            this._orderService = orderService;
            this._settingService = settingService;
            this._exportManager = exportManager;
            this._directDebitInformationService = directDebitInformationService;
        }

        public void Execute()
        {
            var insuranceFileName = _settingService.GetSettingByKey<string>("Insurance.FTPConfiguration.Filename");
            var fileName = string.Format(insuranceFileName, DateTime.Now.ToString("ddMMyyyy-HHmm"), CommonHelper.GenerateRandomDigitCode(4));

            var orders = _orderService.GetDirectDebitOrder().Where(x => x.PaymentStatus == Core.Domain.Payments.PaymentStatus.Paid);
            
            orders = orders.Where(x => x.Shipments.Any());
            orders = orders.Where(x => x.Shipments.FirstOrDefault().ShippedDateUtc.HasValue);

            var nowDay = DateTime.UtcNow;
            var yesterday = nowDay.AddDays(-1);
            var yesterdayFirst = new DateTime(yesterday.Year, yesterday.Month, yesterday.Day);
            var todayFirst = new DateTime(nowDay.Year, nowDay.Month, nowDay.Day);


            orders = orders.Where(x => DateTime.Compare(x.Shipments.FirstOrDefault().ShippedDateUtc.Value, yesterdayFirst) >= 0 && DateTime.Compare(x.Shipments.FirstOrDefault().ShippedDateUtc.Value, todayFirst) < 0);
            
            if (orders != null && orders.Count() > 0)
            {
                var result = _exportManager.ExportDirectDebitDataToTxt(orders.ToList());
                byte[] fileContents = Encoding.UTF8.GetBytes(result);

                var host = _settingService.GetSettingByKey<string>("Insurance.FTPConfiguration.Host");
                var username = _settingService.GetSettingByKey<string>("Insurance.FTPConfiguration.Username");
                var password = _settingService.GetSettingByKey<string>("Insurance.FTPConfiguration.Password");
                var destinationPath = _settingService.GetSettingByKey<string>("Insurance.FTPConfiguration.Destination");
                var port = _settingService.GetSettingByKey<int>("Insurance.FTPConfiguration.Port");

                UploadSFTPFile(host, username, password, fileContents, destinationPath, fileName, port);
            }
        }

        public static void UploadSFTPFile(string host, string username, string password, byte[] fileContents, string destinationpath, string filePath, int port)
        {
            using (SftpClient client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                client.WriteAllBytes(destinationpath + "/" + filePath, fileContents);
            }
        }

        /// <summary>
        /// Schedule Tasks
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="targetDir"></param>
        /// <param name="hostname">a value indicating the hostname of the ftp</param>
        /// <param name="username">a value indicating the username of the ftp</param>
        /// <param name="password">a value indicating the password of the ftp</param>
        public static void UploadFtpFile(byte[] fileContents, string fileName, string targetDir, string hostname, string username, string password)
        {
            //1. check target
            string URI = $"SFTP://{hostname}/{targetDir}/{fileName}";

            FtpWebRequest ftp = (FtpWebRequest)WebRequest.Create(URI);

            ftp.Credentials = new NetworkCredential(username, password);
            ftp.Method = WebRequestMethods.Ftp.UploadFile;
            ftp.UseBinary = true;
            ftp.UsePassive = true;
            ftp.ContentLength = fileContents.Length;

            try
            {
                using (Stream rs = ftp.GetRequestStream())
                {
                    rs.Write(fileContents, 0, fileContents.Length);
                    rs.Close();
                }
            }
            catch (Exception exc)
            {
                Debug.Write(exc.ToString());
            }
        }
    }
}
