﻿using Nop.Core.Domain.Orders;
using System.Collections.Generic;

namespace Nop.Services.Orders
{
    public partial interface IDirectDebitInformationService
    {
        IList<DirectDebitInformation> GetAllDirectDebitInformation();
        DirectDebitInformation GetDirectDebitInformationById(int directDebitInformationId);
        void InsertDirectDebitInformation(DirectDebitInformation directDebitInformation);
        void UpdateDirectDebitInformation(DirectDebitInformation directDebitInformation);
        void DeleteDirectDebitInformation(DirectDebitInformation directDebitInformation);
    }
}
