﻿using Nop.Core.Infrastructure;
using Nop.Services.Events;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using System;
using System.Net;

namespace Nop.Services.MultiDb
{
    public class AutoSynchronizeDatabaseTask:IScheduleTask
    {
        private ISiteService _siteService;
        private ILogger _logger;

        public AutoSynchronizeDatabaseTask(ISiteService siteService,ILogger logger)
        {
            _siteService = siteService;
            _logger = logger;
        }

        public void Execute()
        {
            //if (false==_siteService.IsCurrentSiteMainBack())
            //{
            //    return;
            //}

            //_logger.Information("Synchronize database begin");
            //var allSites = _siteService.GetAllSites();
            //if (allSites != null)
            //{
            //    string url = null;
            //    foreach (var site in allSites)
            //    {
            //        using (var wc = new WebClient())
            //        {
            //            url = site.WebUrl;
            //            if (false == url.EndsWith("/"))
            //            {
            //                url += "/";
            //            }
            //            url += "admin/multiDb/UpdateDatafromBackend";
            //            wc.DownloadStringAsync(new Uri(url));
            //        }
            //    }
            //}
            //_logger.Information("Synchronize database end.");

            if (_siteService.IsCurrentSiteMainBack())
            {
                return;
            }
            _logger.Information("Synchronize database begin");
            _siteService.SyncDataFromMainBack();
            _logger.Information("Synchronize database end");

        }
    }
}
