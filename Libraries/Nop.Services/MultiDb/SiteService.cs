﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Configuration;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.MultiDb;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Topics;
using Nop.Core.Infrastructure;
using Nop.Data;
using Nop.Services.Logging;

namespace Nop.Services.MultiDb
{
    public class SiteService : ISiteService
    {
        private readonly IRepository<SyncGroupInfo> _syncGroupInfoRepository;
        private readonly DataSettings _dataSettings;
        private readonly IDbContext _dbContext;
        private readonly ICacheManager _cacheManager;
        
        private static Dictionary<string, string> type2tablename = new Dictionary<string, string>();

        private const string IS_CURRENT_MAIN_BACK = "Mazuma.IS_CURRENT_MAIN_BACK";
        private const string ALL_SITE_CACHE_KEY = "Mazuma.Site.AllSites";
        private const string TABLE_NAME_CACHE_KEY = "Nop.Site.Tables.{0}";
        private const string TABLE_COLUMNS_CACHE_KEY = "Nop.Table.{0}.{1}.Columns";
        private static List<string> NoIdentityTables = new List<string>() {
                "dbo.Discount_AppliedToCategories",
                "dbo.Discount_AppliedToManufacturers",
                "dbo.Discount_AppliedToProducts",
                "dbo.Product_ProductTag_Mapping",
                "dbo.TempSkuPrice"
            };


        private void PrepareType2Tablename()
        {
            if (type2tablename.Count == 0)
            {
                type2tablename.Add(typeof(ManufacturerTemplate).FullName, "dbo.ManufacturerTemplate");
                type2tablename.Add(typeof(ProductAttribute).FullName, "dbo.ProductAttribute");
                type2tablename.Add(typeof(ProductAttributeValue).FullName, "dbo.ProductAttributeValue");
                type2tablename.Add(typeof(ProductAttributeMapping).FullName, "dbo.Product_ProductAttribute_Mapping");
                type2tablename.Add(typeof(PredefinedProductAttributeValue).FullName, "dbo.PredefinedProductAttributeValue");
                type2tablename.Add(typeof(ProductAttributeCombination).FullName, "dbo.ProductAttributeCombination");
                type2tablename.Add(typeof(CategoryTemplate).FullName, "dbo.CategoryTemplate");
                type2tablename.Add(typeof(StateProvince).FullName, "dbo.StateProvince");
                type2tablename.Add(typeof(Country).FullName, "dbo.Country");
                type2tablename.Add(typeof(Download).FullName, "dbo.Download");
                type2tablename.Add(typeof(CheckoutAttribute).FullName, "dbo.CheckoutAttribute");
                type2tablename.Add(typeof(CheckoutAttributeValue).FullName, "dbo.CheckoutAttributeValue");
                type2tablename.Add(typeof(MessageTemplate).FullName, "dbo.MessageTemplate");
                type2tablename.Add(typeof(Category).FullName, "dbo.Category");
                type2tablename.Add(typeof(CustomerRole).FullName, "dbo.CustomerRole");
                type2tablename.Add(typeof(ProductCategory).FullName, "dbo.Product_Category_Mapping");
                type2tablename.Add(typeof(TaxCategory).FullName, "dbo.TaxCategory");
                type2tablename.Add(typeof(ReturnRequestAction).FullName, "dbo.ReturnRequestAction");
                type2tablename.Add(typeof(ReturnRequestReason).FullName, "dbo.ReturnRequestReason");
                type2tablename.Add(typeof(AclRecord).FullName, "dbo.AclRecord");
                type2tablename.Add(typeof(Product).FullName, "dbo.Product");
                type2tablename.Add(typeof(RelatedProduct).FullName, "dbo.RelatedProduct");
                type2tablename.Add(typeof(CrossSellProduct).FullName, "dbo.CrossSellProduct");
                type2tablename.Add(typeof(ProductTemplate).FullName, "dbo.ProductTemplate");
                type2tablename.Add(typeof(TierPrice).FullName, "dbo.TierPrice");
                type2tablename.Add(typeof(ProductPicture).FullName, "dbo.Product_Picture_Mapping");
                type2tablename.Add(typeof(Campaign).FullName, "dbo.Campaign");
                type2tablename.Add(typeof(TopicTemplate).FullName, "dbo.TopicTemplate");
                type2tablename.Add(typeof(GiftCard).FullName, "dbo.GiftCard");
                type2tablename.Add(typeof(Manufacturer).FullName, "dbo.Manufacturer");
                type2tablename.Add(typeof(ProductManufacturer).FullName, "dbo.Product_Manufacturer_Mapping");
                type2tablename.Add(typeof(Discount).FullName, "dbo.Discount");
                type2tablename.Add(typeof(AppliedToProductSku).FullName, "dbo.Discount_AppliedToProductSkus");
                type2tablename.Add(typeof(Warehouse).FullName, "dbo.Warehouse");
                type2tablename.Add(typeof(ProductTag).FullName, "dbo.ProductTag");
                type2tablename.Add(typeof(EmailAccount).FullName, "dbo.EmailAccount");
                type2tablename.Add(typeof(DeliveryDate).FullName, "dbo.DeliveryDate");
                type2tablename.Add(typeof(ProductAvailabilityRange).FullName, "dbo.ProductAvailabilityRange");
                type2tablename.Add(typeof(CustomerAttribute).FullName, "dbo.CustomerAttribute");
                type2tablename.Add(typeof(CustomerAttributeValue).FullName, "dbo.CustomerAttributeValue");
                type2tablename.Add(typeof(SpecificationAttribute).FullName, "dbo.SpecificationAttribute");
                type2tablename.Add(typeof(SpecificationAttributeOption).FullName, "dbo.SpecificationAttributeOption");
                type2tablename.Add(typeof(ProductSpecificationAttribute).FullName, "dbo.Product_SpecificationAttribute_Mapping");
                type2tablename.Add(typeof(Topic).FullName, "dbo.Topic");
                type2tablename.Add(typeof(Setting).FullName, "dbo.Setting");
                type2tablename.Add(typeof(MeasureDimension).FullName, "dbo.MeasureDimension");
                type2tablename.Add(typeof(MeasureWeight).FullName, "dbo.MeasureWeight");
                type2tablename.Add(typeof(Picture).FullName, "dbo.Picture");
                type2tablename.Add(typeof(ProductWarehouseInventory).FullName, "dbo.ProductWarehouseInventory");
                type2tablename.Add(typeof(DiscountRequirement).FullName, "dbo.DiscountRequirement");
            }
        }

        public SiteService(
            IRepository<SyncGroupInfo> syncGroupInfoRepository,
            DataSettings dataSettings,
            IDbContext dbContext,
            ICacheManager cacheManager)
        {
            _syncGroupInfoRepository = syncGroupInfoRepository;
            _dataSettings = dataSettings;
            _dbContext = dbContext;
            _cacheManager = cacheManager;
        }
        

        public IList<Site> GetAllSites()
        {
            //return _siteRepository.Table.OrderBy(s => s.DisplayOrder).ToList();

            return _cacheManager.Get(ALL_SITE_CACHE_KEY, () => {
                string linkedSqlName = "";
                if (_dataSettings.RawDataSettings.ContainsKey("LinkedSqlName"))
                {
                    linkedSqlName = "[" + _dataSettings.RawDataSettings["LinkedSqlName"] + "].";
                }
                string mainDbName = "";
                if (_dataSettings.RawDataSettings.ContainsKey("MainDatabaseName"))
                {
                    mainDbName = "[" + _dataSettings.RawDataSettings["MainDatabaseName"] + "].";
                }
                string sql = "select * from " + linkedSqlName + mainDbName + "dbo.Site";
                IList<Site> result = _dbContext.SqlQuery<Site>(sql).ToList();
                return result;
            });
        }

        public Site GetSiteById(int siteId)
        {
            if (siteId <= 0)
            {
                return null;
            }

            //return _siteRepository.GetById(siteId);

            IList<Site> allSites = GetAllSites();
            return allSites.Where(x => x.Id == siteId).FirstOrDefault();
        }
        

        public bool IsCurrentSiteMainBack()
        {
            return _cacheManager.Get(IS_CURRENT_MAIN_BACK, () => {
                string currConn = _dataSettings.DataConnectionString;
                IList<Site> allSites = GetAllSites();
                Site site = allSites.Where(x => x.MainBack == 1).FirstOrDefault();
                if (site != null && string.Equals(site.DbConn, currConn, StringComparison.CurrentCultureIgnoreCase))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }
        
        private List<string> GetColumnNamesForTable(string tableName, bool containIdColumn = true)
        {
            List<string> columnNames = _cacheManager.Get(string.Format(TABLE_COLUMNS_CACHE_KEY, tableName,(containIdColumn?1:0)), () =>
            {
                string columnSql = "select Name from SysColumns where id=Object_Id('" + tableName + "')";
                List<string> tempColumnNames = _dbContext.SqlQuery<string>(columnSql).ToList();
                if (tempColumnNames != null && tempColumnNames.Count > 0)
                {
                    List<string> result = new List<string>();
                    foreach (string tcn in tempColumnNames)
                    {
                        if (false == containIdColumn && string.Equals(tcn, "Id", StringComparison.CurrentCultureIgnoreCase))
                        {
                            continue;
                        }
                        result.Add("[" + tcn + "]");
                    }
                    return result;
                }
                return new List<string>();
            });
            return columnNames;
        }
        
        private void SyncSingleTable(string tableName,int entityId, int action, string linkedSqlName, string mainDbName)
        {
            StringBuilder sql = new StringBuilder();
            if (action == 0)
            {
                List<string> columnNames = GetColumnNamesForTable(tableName);
                string columnNameStr = string.Join(",", columnNames);
                sql.Append("set identity_insert ").Append(tableName).Append(" on;");
                sql.Append("insert into ").Append(tableName).Append("(" +columnNameStr+") ");
                sql.Append(" select ").Append(columnNameStr).Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableName);
                sql.Append(" where Id= ").Append(entityId).Append(";");
                sql.Append("set identity_insert ").Append(tableName).Append(" off;");
            }
            else if (action == 1)
            {
                List<string> columnNames = GetColumnNamesForTable(tableName,false);
                sql.Append("update ").Append(tableName).Append(" set ");
                for (int i=0;i<columnNames.Count;i++)
                {
                    if (i > 0)
                    {
                        sql.Append(",");
                    }
                    sql.Append(columnNames[i]).Append(" = sourceTable.").Append(columnNames[i]);
                }
                sql.Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableName).Append(" sourceTable ");                
                sql.Append(" where sourceTable.Id= ").Append(entityId);
                sql.Append(" and ").Append(tableName).Append(".Id=").Append(entityId);
                sql.Append(";");
            }
            else if (action == 2)
            {
                sql.Append("delete from ").Append(tableName).Append(" where Id= ").Append(entityId).Append(";");
            }

            if (sql.Length > 0)
            {
                _dbContext.ExecuteSqlCommand(sql.ToString());
            }
        }

        private void SyncProductTable(int productId, int action, string linkedSqlName, string mainDbName)
        {
            //synchronize Product and Product_ProductTag_Mapping

            string tableNameProduct = "dbo.Product", tableNameMap = "dbo.Product_ProductTag_Mapping";
            List<string> columnNamesMap = GetColumnNamesForTable(tableNameMap);
            string columnNameStrMap = string.Join(",", columnNamesMap);
            StringBuilder sql = new StringBuilder();
            if (action == 0)
            {
                List<string> columnNamesProduct = GetColumnNamesForTable(tableNameProduct);
                string columnNameStrProduct = string.Join(",", columnNamesProduct);
                sql.Append("set identity_insert ").Append(tableNameProduct).Append(" on;");
                sql.Append("insert into ").Append(tableNameProduct).Append("(" + columnNameStrProduct + ") ");
                sql.Append(" select ").Append(columnNameStrProduct).Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableNameProduct);
                sql.Append(" where Id= ").Append(productId).Append(";");
                sql.Append("set identity_insert ").Append(tableNameProduct).Append(" off;");
                
                sql.Append("insert into ").Append(tableNameMap).Append("(" + columnNameStrMap + ") ");
                sql.Append(" select ").Append(columnNameStrMap).Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableNameMap);
                sql.Append(" where Product_Id= ").Append(productId).Append(";");
            }
            else if (action == 1)
            {
                List<string> columnNamesProduct = GetColumnNamesForTable(tableNameProduct,false);
                sql.Append("update ").Append(tableNameProduct).Append(" set ");
                for (int i = 0; i < columnNamesProduct.Count; i++)
                {
                    if (i > 0)
                    {
                        sql.Append(",");
                    }
                    sql.Append(columnNamesProduct[i]).Append(" = sourceTable.").Append(columnNamesProduct[i]);
                }
                sql.Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableNameProduct).Append(" sourceTable ");
                sql.Append(" where sourceTable.Id= ").Append(productId);
                sql.Append(" and ").Append(tableNameProduct).Append(".Id=").Append(productId);
                sql.Append(";");

                sql.Append("delete from ").Append(tableNameMap).Append(" where Product_Id= ").Append(productId).Append(";");
                sql.Append("insert into ").Append(tableNameMap).Append("(" + columnNameStrMap + ") ");
                sql.Append(" select ").Append(columnNameStrMap).Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableNameMap);
                sql.Append(" where Product_Id= ").Append(productId).Append(";");
            }
            else if (action == 2)
            {
                sql.Append("delete from ").Append(tableNameMap).Append(" where Product_Id= ").Append(productId).Append(";");
                sql.Append("delete from ").Append(tableNameProduct).Append(" where Id= ").Append(productId).Append(";");
            }

            if (sql.Length > 0)
            {
                _dbContext.ExecuteSqlCommand(sql.ToString());
            }
        }

        private void SyncDiscountTable(int discountId, int action, string linkedSqlName, string mainDbName)
        {
            //synchronize Discount, Discount_AppliedToCategories, Discount_AppliedToManufacturers, Discount_AppliedToProducts

            string tableNameDiscount = "dbo.Discount", tableNameCategory = "dbo.Discount_AppliedToCategories",tableNameManufacturer= "dbo.Discount_AppliedToManufacturers",tableNameProduct= "dbo.Discount_AppliedToProducts";
            List<string> columnNameCategory = GetColumnNamesForTable(tableNameCategory);
            List<string> columnNameManufacturer = GetColumnNamesForTable(tableNameManufacturer);
            List<string> columnNameProduct = GetColumnNamesForTable(tableNameProduct);
            string columnNameStrCategory = string.Join(",", columnNameCategory);
            string columnNameStrManufacturer = string.Join(",", columnNameManufacturer);
            string columnNameStrProduct = string.Join(",", columnNameProduct);
            StringBuilder sql = new StringBuilder();
            if (action == 0)
            {
                List<string> columnNameDiscount = GetColumnNamesForTable(tableNameDiscount);
                string columnNameStrDiscount = string.Join(",", columnNameDiscount);
                sql.Append("set identity_insert ").Append(tableNameDiscount).Append(" on;");
                sql.Append("insert into ").Append(tableNameDiscount).Append("(" + columnNameStrDiscount + ") ");
                sql.Append(" select ").Append(columnNameStrDiscount).Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableNameDiscount);
                sql.Append(" where Id= ").Append(discountId).Append(";");
                sql.Append("set identity_insert ").Append(tableNameDiscount).Append(" off;");

                sql.Append("insert into ").Append(tableNameCategory).Append("(" + columnNameStrCategory + ") ");
                sql.Append(" select ").Append(columnNameStrCategory).Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableNameCategory);
                sql.Append(" where Discount_Id= ").Append(discountId).Append(";");

                sql.Append("insert into ").Append(tableNameManufacturer).Append("(" + columnNameStrManufacturer + ") ");
                sql.Append(" select ").Append(columnNameStrManufacturer).Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableNameManufacturer);
                sql.Append(" where Discount_Id= ").Append(discountId).Append(";");

                sql.Append("insert into ").Append(tableNameProduct).Append("(" + columnNameStrProduct + ") ");
                sql.Append(" select ").Append(columnNameStrProduct).Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableNameProduct);
                sql.Append(" where Discount_Id= ").Append(discountId).Append(";");
            }
            else if (action == 1)
            {
                List<string> columnNameDiscount = GetColumnNamesForTable(tableNameDiscount,false);
                sql.Append("update ").Append(tableNameDiscount).Append(" set ");
                for (int i = 0; i < columnNameDiscount.Count; i++)
                {
                    if (i > 0)
                    {
                        sql.Append(",");
                    }
                    sql.Append(columnNameDiscount[i]).Append(" = sourceTable.").Append(columnNameDiscount[i]);
                }
                sql.Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableNameDiscount).Append(" sourceTable ");
                sql.Append(" where sourceTable.Id= ").Append(discountId);
                sql.Append(" and ").Append(tableNameDiscount).Append(".Id=").Append(discountId);
                sql.Append(";");

                sql.Append("delete from ").Append(tableNameCategory).Append(" where Discount_Id= ").Append(discountId).Append(";");
                sql.Append("insert into ").Append(tableNameCategory).Append("(" + columnNameStrCategory + ") ");
                sql.Append(" select ").Append(columnNameStrCategory).Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableNameCategory);
                sql.Append(" where Discount_Id= ").Append(discountId).Append(";");

                sql.Append("delete from ").Append(tableNameManufacturer).Append(" where Discount_Id= ").Append(discountId).Append(";");
                sql.Append("insert into ").Append(tableNameManufacturer).Append("(" + columnNameStrManufacturer + ") ");
                sql.Append(" select ").Append(columnNameStrManufacturer).Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableNameManufacturer);
                sql.Append(" where Discount_Id= ").Append(discountId).Append(";");

                sql.Append("delete from ").Append(tableNameProduct).Append(" where Discount_Id= ").Append(discountId).Append(";");
                sql.Append("insert into ").Append(tableNameProduct).Append("(" + columnNameStrProduct + ") ");
                sql.Append(" select ").Append(columnNameStrProduct).Append(" from ").Append(linkedSqlName).Append(mainDbName).Append(tableNameProduct);
                sql.Append(" where Discount_Id= ").Append(discountId).Append(";");
            }
            else if (action == 2)
            {
                sql.Append("delete from ").Append(tableNameDiscount).Append(" where Id= ").Append(discountId).Append(";");
            }

            if (sql.Length > 0)
            {
                _dbContext.ExecuteSqlCommand(sql.ToString());
            }
        }
        
        public void SyncDataFromMainBack()
        {
            ILogger logger = EngineContext.Current.Resolve<ILogger>();
            if (IsCurrentSiteMainBack())
            {
                return;
            }

            logger.Information("SyncDataFromMainBack start");

            if (type2tablename.Count == 0)
            {
                PrepareType2Tablename();
            }
            string linkedSqlName = "";
            if (_dataSettings.RawDataSettings.ContainsKey("LinkedSqlName"))
            {
                linkedSqlName = "[" + _dataSettings.RawDataSettings["LinkedSqlName"] + "].";
            }
            string mainDbName = "";
            if (_dataSettings.RawDataSettings.ContainsKey("MainDatabaseName"))
            {
                mainDbName = "[" + _dataSettings.RawDataSettings["MainDatabaseName"] + "].";
            }
            
            int lastId = 0;
            //var subLastOperation = _operateLogRepository.Table.OrderByDescending(x => x.Id).FirstOrDefault();
            //if (subLastOperation != null)
            //{
            //    lastId = subLastOperation.Id;
            //}

            lastId = _dbContext.SqlQuery<int>("select isnull(max(Id),0) from dbo.OperateLog").FirstOrDefault();

            var allOperations= _dbContext.SqlQuery<OperateLog>("select * from "+linkedSqlName+mainDbName+"dbo.OperateLog where Id>"+lastId+" order by Id").ToList();
            if (allOperations != null)
            {
                foreach (var log in allOperations)
                {
                    #region synchronize data of table

                    if ("dbo.ManufacturerTemplate".Equals(log.TableName)
                        || "dbo.ProductAttribute".Equals(log.TableName)
                        || "dbo.ProductAttributeValue".Equals(log.TableName)
                        || "dbo.Product_ProductAttribute_Mapping".Equals(log.TableName)
                        || "dbo.PredefinedProductAttributeValue".Equals(log.TableName)
                        || "dbo.ProductAttributeCombination".Equals(log.TableName)
                        || "dbo.CategoryTemplate".Equals(log.TableName)
                        || "dbo.StateProvince".Equals(log.TableName)
                        || "dbo.Country".Equals(log.TableName)
                        || "dbo.Download".Equals(log.TableName)
                        || "dbo.CheckoutAttribute".Equals(log.TableName)
                        || "dbo.CheckoutAttributeValue".Equals(log.TableName)
                        || "dbo.MessageTemplate".Equals(log.TableName)
                        || "dbo.Category".Equals(log.TableName)
                        || "dbo.CustomerRole".Equals(log.TableName)
                        || "dbo.Product_Category_Mapping".Equals(log.TableName)
                        || "dbo.TaxCategory".Equals(log.TableName)
                        || "dbo.ReturnRequestAction".Equals(log.TableName)
                        || "dbo.ReturnRequestReason".Equals(log.TableName)
                        || "dbo.AclRecord".Equals(log.TableName)
                        || "dbo.RelatedProduct".Equals(log.TableName)
                        || "dbo.CrossSellProduct".Equals(log.TableName)
                        || "dbo.ProductTemplate".Equals(log.TableName)
                        || "dbo.TierPrice".Equals(log.TableName)
                        || "dbo.Product_Picture_Mapping".Equals(log.TableName)
                        || "dbo.Campaign".Equals(log.TableName)
                        || "dbo.TopicTemplate".Equals(log.TableName)
                        || "dbo.Manufacturer".Equals(log.TableName)
                        || "dbo.Product_Manufacturer_Mapping".Equals(log.TableName)
                        || "dbo.Discount_AppliedToProductSkus".Equals(log.TableName)
                        || "dbo.Warehouse".Equals(log.TableName)
                        || "dbo.ProductTag".Equals(log.TableName)
                        || "dbo.EmailAccount".Equals(log.TableName)
                        || "dbo.DeliveryDate".Equals(log.TableName)
                        || "dbo.ProductAvailabilityRange".Equals(log.TableName)
                        || "dbo.CustomerAttribute".Equals(log.TableName)
                        || "dbo.CustomerAttributeValue".Equals(log.TableName)
                        || "dbo.SpecificationAttribute".Equals(log.TableName)
                        || "dbo.SpecificationAttributeOption".Equals(log.TableName)
                        || "dbo.Product_SpecificationAttribute_Mapping".Equals(log.TableName)
                        || "dbo.Topic".Equals(log.TableName)
                        || "dbo.Setting".Equals(log.TableName)
                        || "dbo.MeasureDimension".Equals(log.TableName)
                        || "dbo.MeasureWeight".Equals(log.TableName)
                        || "dbo.Picture".Equals(log.TableName)
                        || "dbo.ProductWarehouseInventory".Equals(log.TableName)
                        || "dbo.DiscountRequirement".Equals(log.TableName))
                    {
                        logger.Information("sync single table start");
                        SyncSingleTable(log.TableName, log.EntityId, log.Action, linkedSqlName, mainDbName);
                        logger.Information("sync single table end");
                    }
                    else if ("dbo.Product".Equals(log.TableName))
                    {
                        logger.Information("sync product table start");
                        SyncProductTable(log.EntityId, log.Action, linkedSqlName, mainDbName);
                        logger.Information("sync product table end");
                    }
                    else if ("dbo.Discount".Equals(log.TableName))
                    {
                        logger.Information("sync discount table start");
                        SyncDiscountTable(log.EntityId, log.Action, linkedSqlName, mainDbName);
                        logger.Information("sync discount table end");
                    }
                    else
                    {
                        logger.Information("unknown table: "+log.TableName);
                    }

                    #endregion
                    
                    _dbContext.ExecuteSqlCommand("insert into dbo.OperateLog(Action,EntityId,TableName) values(" + log.Action + "," + log.EntityId + ",'" + log.TableName + "')");
                }
            }


            logger.Information("SyncDataFromMainBack end");

        }
        

        public IList<SyncGroupInfo> GetAllSyncGroups()
        {
            return _syncGroupInfoRepository.Table.OrderBy(x=>x.DisplayOrder).ThenBy(x=>x.Id).ToList();
        }

        public void UpdateSyncGroupInfos(IList<SyncGroupInfo> list)
        {
            if (list != null)
            {
                _syncGroupInfoRepository.Update(list);
            }
        }

        public void AddOperationLog(int entityId, string entityTypeName, int action)
        {
            if (type2tablename.Count == 0)
            {
                PrepareType2Tablename();
            }
            if (false==type2tablename.ContainsKey(entityTypeName))
            {
                return;
            }
            string tableName = type2tablename[entityTypeName];

            string sql = "insert into dbo.OperateLog(Action,EntityId,TableName) values(" + action + "," + entityId + ",'" + tableName + "')";
            _dbContext.ExecuteSqlCommand(sql);
        }
    }
}
