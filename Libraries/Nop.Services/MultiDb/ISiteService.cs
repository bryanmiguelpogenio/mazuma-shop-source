﻿using Nop.Core;
using Nop.Core.Domain.MultiDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.MultiDb
{
    public interface ISiteService
    {
        IList<Site> GetAllSites();

        Site GetSiteById(int siteId);
        
        bool IsCurrentSiteMainBack();

        void SyncDataFromMainBack();

        IList<SyncGroupInfo> GetAllSyncGroups();

        void UpdateSyncGroupInfos(IList<SyncGroupInfo> list);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityTypeName"></param>
        /// <param name="action">0--insert,1--update,2--delete</param>
        void AddOperationLog(int entityId,string entityTypeName,int action);
    }
}
