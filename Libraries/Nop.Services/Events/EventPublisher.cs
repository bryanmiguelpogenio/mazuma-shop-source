﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Configuration;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Stores;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Topics;
using Nop.Core.Events;
using Nop.Core.Infrastructure;
using Nop.Core.Plugins;
using Nop.Services.Logging;
using Nop.Services.MultiDb;

namespace Nop.Services.Events
{
    /// <summary>
    /// Evnt publisher
    /// </summary>
    public class EventPublisher : IEventPublisher
    {
        private readonly ISubscriptionService _subscriptionService;
        private readonly ISiteService _siteService;
        

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="subscriptionService"></param>
        public EventPublisher(ISubscriptionService subscriptionService)
        {
            _subscriptionService = subscriptionService;
            _siteService = EngineContext.Current.Resolve<ISiteService>();
        }

        /// <summary>
        /// Publish to cunsumer
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="x">Event consumer</param>
        /// <param name="eventMessage">Event message</param>
        protected virtual void PublishToConsumer<T>(IConsumer<T> x, T eventMessage)
        {
            try
            {
                x.HandleEvent(eventMessage);
            }
            catch (Exception exc)
            {
                //log error
                var logger = EngineContext.Current.Resolve<ILogger>();
                //we put in to nested try-catch to prevent possible cyclic (if some error occurs)
                try
                {
                    logger.Error(exc.Message, exc);
                }
                catch (Exception)
                {
                    //do nothing
                }
            }
        }

        private static Type _typeInsert = typeof(EntityInserted<>);
        private static Type _typeUpdate = typeof(EntityUpdated<>);
        private static Type _typeDelete = typeof(EntityDeleted<>);



        private void AddOperateLog<T>(T em)
        {
            if (em == null)
            {
                return;
            }
            int action = -1;
            Type innerType = null;
            Type emType = em.GetType();
            if (false == emType.IsGenericType)
            {
                return;
            }
            var emGenericType = emType.GetGenericTypeDefinition();
            if (_typeInsert.Equals(emGenericType))
            {
                action = 0;
            }
            else if (_typeUpdate.Equals(emGenericType))
            {
                action = 1;
            }
            else if (_typeDelete.Equals(emGenericType))
            {
                action = 2;
            }
            var innerTypes = emType.GenericTypeArguments;
            if (innerTypes != null && innerTypes.Length>0)
            {
                innerType = innerTypes[0];
            }

            if (action > -1 && innerType!=null)
            {
                var entityField = emType.GetProperty("Entity");
                if (entityField == null)
                {
                    return;
                }
                BaseEntity baseEntity =  entityField.GetValue(em) as BaseEntity;
                if (baseEntity != null)
                {
                    _siteService.AddOperationLog(baseEntity.Id, innerType.FullName, action);
                }
            }
        }

        //private bool DoesRequireSynchronizeDatabase<T>(T eventMessage)
        //{
        //    if (eventMessage is EntityDeleted<ManufacturerTemplate> || eventMessage is EntityInserted<ManufacturerTemplate> || eventMessage is EntityUpdated<ManufacturerTemplate>
        //        || eventMessage is EntityDeleted<ProductAttribute> || eventMessage is EntityInserted<ProductAttribute> || eventMessage is EntityUpdated<ProductAttribute>
        //        || eventMessage is EntityDeleted<ProductAttributeMapping> || eventMessage is EntityInserted<ProductAttributeMapping> || eventMessage is EntityUpdated<ProductAttributeMapping>
        //        || eventMessage is EntityDeleted<ProductAttributeValue> || eventMessage is EntityInserted<ProductAttributeValue> || eventMessage is EntityUpdated<ProductAttributeValue>
        //        || eventMessage is EntityDeleted<PredefinedProductAttributeValue> || eventMessage is EntityInserted<PredefinedProductAttributeValue> || eventMessage is EntityUpdated<PredefinedProductAttributeValue>
        //        || eventMessage is EntityDeleted<ProductAttributeCombination> || eventMessage is EntityInserted<ProductAttributeCombination> || eventMessage is EntityUpdated<ProductAttributeCombination>
        //        || eventMessage is EntityDeleted<CategoryTemplate> || eventMessage is EntityInserted<CategoryTemplate> || eventMessage is EntityUpdated<CategoryTemplate>
        //        || eventMessage is EntityDeleted<StateProvince> || eventMessage is EntityInserted<StateProvince> || eventMessage is EntityUpdated<StateProvince>
        //        || eventMessage is EntityDeleted<Download> || eventMessage is EntityInserted<Download> || eventMessage is EntityUpdated<Download>
        //        || eventMessage is EntityDeleted<Country> || eventMessage is EntityInserted<Country> || eventMessage is EntityUpdated<Country>
        //        || eventMessage is EntityDeleted<CheckoutAttribute> || eventMessage is EntityInserted<CheckoutAttribute> || eventMessage is EntityUpdated<CheckoutAttribute>
        //        || eventMessage is EntityDeleted<MessageTemplate> || eventMessage is EntityInserted<MessageTemplate> || eventMessage is EntityUpdated<MessageTemplate>
        //        || eventMessage is EntityDeleted<CheckoutAttributeValue> || eventMessage is EntityInserted<CheckoutAttributeValue> || eventMessage is EntityUpdated<CheckoutAttributeValue>
        //        || eventMessage is EntityDeleted<Category> || eventMessage is EntityInserted<Category> || eventMessage is EntityUpdated<Category>
        //        || eventMessage is EntityDeleted<CustomerRole> || eventMessage is EntityInserted<CustomerRole> || eventMessage is EntityUpdated<CustomerRole>
        //        || eventMessage is EntityDeleted<ProductCategory> || eventMessage is EntityInserted<ProductCategory> || eventMessage is EntityUpdated<ProductCategory>
        //        || eventMessage is EntityDeleted<TaxCategory> || eventMessage is EntityInserted<TaxCategory> || eventMessage is EntityUpdated<TaxCategory>
        //        || eventMessage is EntityDeleted<ReturnRequestAction> || eventMessage is EntityInserted<ReturnRequestAction> || eventMessage is EntityUpdated<ReturnRequestAction>
        //        || eventMessage is EntityDeleted<ReturnRequestReason> || eventMessage is EntityInserted<ReturnRequestReason> || eventMessage is EntityUpdated<ReturnRequestReason>
        //        || eventMessage is EntityDeleted<AclRecord> || eventMessage is EntityInserted<AclRecord> || eventMessage is EntityUpdated<AclRecord>
        //        || eventMessage is EntityDeleted<Product> || eventMessage is EntityInserted<Product> || eventMessage is EntityUpdated<Product>
        //        || eventMessage is EntityDeleted<RelatedProduct> || eventMessage is EntityInserted<RelatedProduct> || eventMessage is EntityUpdated<RelatedProduct>
        //        || eventMessage is EntityDeleted<CrossSellProduct> || eventMessage is EntityInserted<CrossSellProduct> || eventMessage is EntityUpdated<CrossSellProduct>
        //        || eventMessage is EntityDeleted<ProductTemplate> || eventMessage is EntityInserted<ProductTemplate> || eventMessage is EntityUpdated<ProductTemplate>
        //        || eventMessage is EntityDeleted<TierPrice> || eventMessage is EntityInserted<TierPrice> || eventMessage is EntityUpdated<TierPrice>
        //        || eventMessage is EntityDeleted<ProductPicture> || eventMessage is EntityInserted<ProductPicture> || eventMessage is EntityUpdated<ProductPicture>
        //        || eventMessage is EntityDeleted<Campaign> || eventMessage is EntityInserted<Campaign> || eventMessage is EntityUpdated<Campaign>
        //        || eventMessage is EntityDeleted<TopicTemplate> || eventMessage is EntityInserted<TopicTemplate> || eventMessage is EntityUpdated<TopicTemplate>
        //        || eventMessage is EntityDeleted<GiftCard> || eventMessage is EntityInserted<GiftCard> || eventMessage is EntityUpdated<GiftCard>
        //        || eventMessage is EntityDeleted<Manufacturer> || eventMessage is EntityInserted<Manufacturer> || eventMessage is EntityUpdated<Manufacturer>
        //        || eventMessage is EntityDeleted<ProductManufacturer> || eventMessage is EntityInserted<ProductManufacturer> || eventMessage is EntityUpdated<ProductManufacturer>
        //        || eventMessage is EntityDeleted<Discount> || eventMessage is EntityInserted<Discount> || eventMessage is EntityUpdated<Discount>
        //        || eventMessage is EntityDeleted<AppliedToProductSku> || eventMessage is EntityInserted<AppliedToProductSku> || eventMessage is EntityUpdated<AppliedToProductSku>
        //        || eventMessage is EntityDeleted<Warehouse> || eventMessage is EntityInserted<Warehouse> || eventMessage is EntityUpdated<Warehouse>
        //        || eventMessage is EntityDeleted<ProductTag> || eventMessage is EntityInserted<ProductTag> || eventMessage is EntityUpdated<ProductTag>
        //        || eventMessage is EntityDeleted<EmailAccount> || eventMessage is EntityInserted<EmailAccount> || eventMessage is EntityUpdated<EmailAccount>
        //        || eventMessage is EntityDeleted<DeliveryDate> || eventMessage is EntityInserted<DeliveryDate> || eventMessage is EntityUpdated<DeliveryDate>
        //        || eventMessage is EntityDeleted<ProductAvailabilityRange> || eventMessage is EntityInserted<ProductAvailabilityRange> || eventMessage is EntityUpdated<ProductAvailabilityRange>
        //        || eventMessage is EntityDeleted<CustomerAttribute> || eventMessage is EntityInserted<CustomerAttribute> || eventMessage is EntityUpdated<CustomerAttribute>
        //        || eventMessage is EntityDeleted<CustomerAttributeValue> || eventMessage is EntityInserted<CustomerAttributeValue> || eventMessage is EntityUpdated<CustomerAttributeValue>
        //        || eventMessage is EntityDeleted<SpecificationAttribute> || eventMessage is EntityInserted<SpecificationAttribute> || eventMessage is EntityUpdated<SpecificationAttribute>
        //        || eventMessage is EntityDeleted<SpecificationAttributeOption> || eventMessage is EntityInserted<SpecificationAttributeOption> || eventMessage is EntityUpdated<SpecificationAttributeOption>
        //        || eventMessage is EntityDeleted<ProductSpecificationAttribute> || eventMessage is EntityInserted<ProductSpecificationAttribute> || eventMessage is EntityUpdated<ProductSpecificationAttribute>
        //        || eventMessage is EntityDeleted<Topic> || eventMessage is EntityInserted<Topic> || eventMessage is EntityUpdated<Topic>
        //        || eventMessage is EntityDeleted<Setting> || eventMessage is EntityInserted<Setting> || eventMessage is EntityUpdated<Setting>
        //        || eventMessage is EntityDeleted<MeasureDimension> || eventMessage is EntityInserted<MeasureDimension> || eventMessage is EntityUpdated<MeasureDimension>
        //        || eventMessage is EntityDeleted<MeasureWeight> || eventMessage is EntityInserted<MeasureWeight> || eventMessage is EntityUpdated<MeasureWeight>
        //        || eventMessage is EntityDeleted<Picture> || eventMessage is EntityInserted<Picture> || eventMessage is EntityUpdated<Picture>)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        /// <summary>
        /// Publish event
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="eventMessage">Event message</param>
        public virtual void Publish<T>(T eventMessage)
        {
            //get all event subscribers, excluding from not installed plugins
            var subscribers = _subscriptionService.GetSubscriptions<T>()
                .Where(subscriber => PluginManager.FindPlugin(subscriber.GetType())?.Installed ?? true).ToList();

            //publish event to subscribers
            subscribers.ForEach(subscriber => PublishToConsumer(subscriber, eventMessage));
            
            if (_siteService.IsCurrentSiteMainBack())
            {
                AddOperateLog(eventMessage);
            }
        }
    }
}
