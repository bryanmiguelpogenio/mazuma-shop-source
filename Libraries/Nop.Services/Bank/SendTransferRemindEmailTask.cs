﻿using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Tasks;
using System;

namespace Nop.Services.Bank
{
   public partial class SendTransferRemindEmailTask : IScheduleTask
    {
        private IOrderService _orderService;
        private IWorkflowMessageService _workflowMessageService;
        public SendTransferRemindEmailTask(IOrderService orderService,
            IWorkflowMessageService workflowMessageService)
        {
            this._workflowMessageService = workflowMessageService;
            this._orderService = orderService;
        }
        public void Execute()
        {
            var orders = _orderService.GetAllBankTransferOrder();
            foreach (var order in orders)
            {
                if(order.TransferRecords.Count==0)
                {
                    //test
                    //_workflowMessageService.SendOrderNeedPaidCustomerNotification(order, order.CustomerLanguageId);
                    // this task need run 1 hour once, otherwise this will excute more than one or not excute 
                    if (order.CreatedOnUtc.AddHours(24) <= DateTime.UtcNow && DateTime.UtcNow < order.CreatedOnUtc.AddHours(25))
                    {
                        _workflowMessageService.SendOrderNeedPaidCustomerNotification(order, order.CustomerLanguageId);
                    }
                }
               

            }
        }
    }
}
