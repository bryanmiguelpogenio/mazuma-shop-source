﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Bank;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.Bank
{
    public partial class BankInfoService : IBankInfoService
    {
        #region Fields
        private readonly IRepository<BankInfo> _bankInfoRepository;
        private readonly IEventPublisher _eventPublisher;
        #endregion

        #region Ctor
        public BankInfoService(IRepository<BankInfo> bankInfoRepository,
            IEventPublisher eventPublisher)
        {
            this._bankInfoRepository = bankInfoRepository;
            this._eventPublisher = eventPublisher;
        }
        #endregion

        #region Methods
        public void DeleteBankInfo(BankInfo bankinfo)
        {
            if (bankinfo == null)
                throw new ArgumentNullException(nameof(bankinfo));

            _bankInfoRepository.Delete(bankinfo);

            //event notification
            _eventPublisher.EntityDeleted(bankinfo);
        }

        public BankInfo GetBankInfoById(int bankId)
        {
            if (bankId == 0)
                return null;

            return _bankInfoRepository.GetById(bankId);
        }

        public BankInfo GetActiveBankInfoByStoreId(int storeId)
        {
            var bank = _bankInfoRepository.Table.Where(b => b.StoreId == storeId && b.IsActive)
                .OrderBy(b=> b.DisplayOrder).FirstOrDefault();

            return bank;
        }

        public void InsertBankInfo(BankInfo bankInfo)
        {
            if (bankInfo == null)
                throw new ArgumentNullException(nameof(bankInfo));

            _bankInfoRepository.Insert(bankInfo);
            _eventPublisher.EntityInserted(bankInfo);
            
        }

        public IList<BankInfo> GetAllBankInfo(int storeId,string IBAN="",string companyName="",string address="")
        {
            var query = _bankInfoRepository.Table;

            if (storeId > 0)
                query = query.Where(b => b.StoreId == storeId);
            if (!string.IsNullOrEmpty(IBAN))
                query = query.Where(b => b.IBAN.Contains(IBAN));
            if (!string.IsNullOrEmpty(companyName))
                query = query.Where(b => b.CompanyName.Contains(companyName));
            if (!string.IsNullOrEmpty(address))
                query = query.Where(b => b.Address.Contains(address));
            query = query.OrderBy(b=>b.DisplayOrder);

            return query.ToList();
        }

        public void UpdateBankInfo(BankInfo bankInfo)
        {
            if (bankInfo == null)
                throw new ArgumentNullException(nameof(bankInfo));

            _bankInfoRepository.Update(bankInfo);

            //event notification
            _eventPublisher.EntityUpdated(bankInfo);
        }
        #endregion
    }
}
