﻿using Nop.Services.Orders;
using Nop.Services.Tasks;
using System;

namespace Nop.Services.Bank
{
    public partial class CancelBankTransferTask : IScheduleTask
    {
        private IOrderProcessingService _orderProcessingService;
        private IOrderService _orderService;
        private ITransferRecordService _transferRecordService;


        public CancelBankTransferTask(IOrderService orderService,
            IOrderProcessingService orderProcessingService,
            ITransferRecordService transferRecordService) {
            this._transferRecordService = transferRecordService;
            this._orderProcessingService = orderProcessingService;
            this._orderService = orderService;
        }

        public void Execute()
        {
            var orders = _orderService.GetAllBankTransferOrder();
            foreach (var order in orders)
            {
                if(order.TransferRecords.Count==0)
                {
                    if (order.CreatedOnUtc.AddHours(48) <= DateTime.UtcNow)
                    {
                        if (_orderProcessingService.CanCancelOrder(order))
                        {
                            _orderProcessingService.CancelOrder(order, true);
                        }
                    }
                }
            }
        }
    }
}
