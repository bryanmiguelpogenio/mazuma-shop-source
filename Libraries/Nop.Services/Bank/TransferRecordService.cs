﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Bank;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.Bank
{
    public partial class TransferRecordService : ITransferRecordService
    {
        #region Fields
        private readonly IRepository<TransferRecord> _transferRecordRepository;
        private readonly IEventPublisher _eventPublisher;
        #endregion

        #region Ctor
        public TransferRecordService(IRepository<TransferRecord> transferRecordRepository,
            IEventPublisher eventPublisher)
        {
            this._transferRecordRepository = transferRecordRepository;
            this._eventPublisher = eventPublisher;
        }
        #endregion

        #region Methods
        public void DeleteTransferRecord(TransferRecord transferRecord)
        {
            if (transferRecord == null)
                throw new ArgumentNullException(nameof(transferRecord));

            transferRecord.Deleted = true;
            //delete product
            UpdateTransferRecord(transferRecord);

            //event notification
            _eventPublisher.EntityDeleted(transferRecord);
        }

        public TransferRecord GetTransferRecordById(int transferRecordId)
        {
            if (transferRecordId == 0)
                return null;

            return _transferRecordRepository.GetById(transferRecordId);
        }

        public void InsertTransferRecord(TransferRecord transferRecord)
        {
            if (transferRecord == null)
                throw new ArgumentNullException(nameof(transferRecord));

            _transferRecordRepository.Insert(transferRecord);
            _eventPublisher.EntityInserted(transferRecord);
            
        }

        public virtual IPagedList<TransferRecord> GetAllTransferRecord(
            int orderId=0,
            string fromAccount="",
            string serialNumber="",
            IList<int> tIds=null,
            string fromCardCode="",
            int pageIndex = 0,
            int pageSize = int.MaxValue
            )
        {
            var query = _transferRecordRepository.Table;
            query = query.Where(t => !t.Deleted);
            if (orderId > 0)
                query = query.Where(b => b.OrderId == orderId);
            if (!string.IsNullOrEmpty(serialNumber))
                query = query.Where(b => b.SerialNumber.Contains(serialNumber));
            if (!string.IsNullOrEmpty(fromAccount))
                query = query.Where(b => b.FromAccount.Contains(fromAccount));
            if (!string.IsNullOrEmpty(fromCardCode))
                query = query.Where(b => b.FromCardCode.Contains(fromCardCode));
            if (tIds != null && tIds.Any())
                query = query.Where(b => tIds.Contains(b.TransferStatusId));
            query = query.OrderByDescending(b => b.Id);
            return new PagedList<TransferRecord>(query, pageIndex, pageSize);
        }

        public void UpdateTransferRecord(TransferRecord transferRecord)
        {
            if (transferRecord == null)
                throw new ArgumentNullException(nameof(transferRecord));

            _transferRecordRepository.Update(transferRecord);

            //event notification
            _eventPublisher.EntityUpdated(transferRecord);
        } 
        #endregion
    }
}
