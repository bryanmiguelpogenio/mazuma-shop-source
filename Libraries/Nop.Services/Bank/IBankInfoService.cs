﻿using Nop.Core;
using Nop.Core.Domain.Bank;
using System.Collections.Generic;

namespace Nop.Services.Bank
{
    public partial interface IBankInfoService
    {
        IList<BankInfo> GetAllBankInfo(int storeId, string IBAN = "", string companyName = "", string address = "");
        BankInfo GetBankInfoById(int bankId);
        BankInfo GetActiveBankInfoByStoreId(int storeId);
        void InsertBankInfo(BankInfo backInfo);
        void UpdateBankInfo(BankInfo backInfo);
        void DeleteBankInfo(BankInfo bankinfo);
    }
}
