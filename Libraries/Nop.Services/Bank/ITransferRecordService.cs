﻿using Nop.Core;
using Nop.Core.Domain.Bank;
using System.Collections.Generic;

namespace Nop.Services.Bank
{
    public partial interface ITransferRecordService
    {
        IPagedList<TransferRecord> GetAllTransferRecord(
            int orderId = 0,
            string fromAccount = "",
            string serialNumber = "",
            IList<int> tIds = null,
            string fromCardCode = "",
            int pageIndex = 0,
            int pageSize = int.MaxValue
            );
        TransferRecord GetTransferRecordById(int transferRecordId);
        void InsertTransferRecord(TransferRecord transferRecord);
        void UpdateTransferRecord(TransferRecord transferRecord);
        void DeleteTransferRecord(TransferRecord transferRecord);
    }
}
