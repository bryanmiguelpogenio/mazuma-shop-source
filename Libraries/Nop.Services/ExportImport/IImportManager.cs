﻿using System.IO;

namespace Nop.Services.ExportImport
{
    /// <summary>
    /// Import manager interface
    /// </summary>
    public partial interface IImportManager
    {
        /// <summary>
        /// Import products from XLSX file
        /// </summary>
        /// <param name="stream">Stream</param>
        void ImportProductsFromXlsx(Stream stream);

        /// <summary>
        /// Import products attributes from XLSX file
        /// </summary>
        /// <param name="stream">Stream</param>
        void ImportProductAttributesFromXlsx(Stream stream);

        /// <summary>
        /// Import newsletter subscribers from TXT file
        /// </summary>
        /// <param name="stream">Stream</param>
        /// <returns>Number of imported subscribers</returns>
        int ImportNewsletterSubscribersFromTxt(Stream stream);

        /// <summary>
        /// Import states from TXT file
        /// </summary>
        /// <param name="stream">Stream</param>
        /// <returns>Number of imported states</returns>
        int ImportStatesFromTxt(Stream stream);

        /// <summary>
        /// Import manufacturers from XLSX file
        /// </summary>
        /// <param name="stream">Stream</param>
        void ImportManufacturersFromXlsx(Stream stream);

        /// <summary>
        /// Import categories from XLSX file
        /// </summary>
        /// <param name="stream">Stream</param>
        void ImportCategoriesFromXlsx(Stream stream);

        /// <summary>
        /// Import AppliedProductSku from XLSX file
        /// </summary>
        void ImportDiscountFromXlsx(Stream stream);

        /// <summary>
        /// Import DiscountProductSku from XLSX file
        /// </summary>
        void ImportDiscountProductSkuExcel(Stream stream);

        void ImportProductReviewsFromXlsx(Stream stream);

        /// <summary>
        /// Import Insurance Device From Xlsx file
        /// </summary>
        void ImportInsuranceDeviceFromXlsx(Stream stream);


    }
}
