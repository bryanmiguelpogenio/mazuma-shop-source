﻿using System;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Services.Orders;
using Nop.Core.Domain.Orders;

namespace Nop.Services.Messages
{
    /// <summary>
    /// Represents a task for sending queued message 
    /// </summary>
    public partial class SendEmailOrderReviewLinkTask : IScheduleTask
    {
        private readonly ILogger _logger;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IOrderService _orderService;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="queuedEmailService">Queued email service</param>
        /// <param name="emailSender">Email sender</param>
        /// <param name="logger">Logger</param>
        public SendEmailOrderReviewLinkTask(
            ILogger logger,
            IWorkflowMessageService workflowMessageService,
            IOrderService orderService)
        {
            this._logger = logger;
            this._workflowMessageService = workflowMessageService;
            this._orderService = orderService;
        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public virtual void Execute()
        {
            var orderList = _orderService.GetNotSendEmailOrders(DateTime.UtcNow);
            if(orderList != null && orderList.Count > 0)
            {
                foreach (var order in orderList)
                {
                    var orderReviewCustomerNotificationQueuedEmailId = _workflowMessageService.SendOrderReviewLinkCustomerNotification(order, order.CustomerLanguageId);
                    if (orderReviewCustomerNotificationQueuedEmailId > 0)
                    {
                        order.OrderNotes.Add(new OrderNote
                        {
                            Note = $"\"Order review link\" email (to customer) has been queued. Queued email identifier: {orderReviewCustomerNotificationQueuedEmailId}.",
                            DisplayToCustomer = false,
                            CreatedOnUtc = DateTime.UtcNow
                        });
                        order.IsSendEmail = true;
                        _orderService.UpdateOrder(order);
                    }
                }
            }
        }
    }
}
