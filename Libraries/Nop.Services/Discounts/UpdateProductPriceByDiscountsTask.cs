﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Discounts;
using Nop.Services.Catalog;
using Nop.Services.Discounts.Cache;
using Nop.Services.Tasks;
using RestSharp;
using RestSharp.Authenticators;
using Nop.Services.Logging;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain;

namespace Nop.Services.Discounts
{
    /// <summary>
    /// Represents a task for keeping the site alive
    /// </summary>
    public partial class UpdateProductPriceByDiscountsTask : IScheduleTask
    {
        private readonly IDiscountService _discountService;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IProductService _productService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly ILogger _logger;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="storeContext">Store context</param>
        public UpdateProductPriceByDiscountsTask(IDiscountService discountServic, 
            IStaticCacheManager cacheManager, 
            IProductService productService,
            ILogger logger,
            IProductAttributeService productAttributeService,
            StoreInformationSettings storeInformationSettings)
        {
            this._discountService = discountServic;
            this._cacheManager = cacheManager; 
            this._productService = productService;
            this._productAttributeService = productAttributeService;
            _storeInformationSettings = storeInformationSettings;
            this._logger = logger;
        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public void Execute()
        {
            var nowUtc = DateTime.UtcNow;

            //discount type    applied to product 
            UpdateProductPrice(nowUtc);
            //discount type    applied to applied to product attribute combination sku 
            UpdateAttributeCombinationPrice(nowUtc);
        }

        public void UpdateProductPrice(DateTime nowUtc)
        {
            //discount type    applied to product 
            var days = _storeInformationSettings.UpdatingPriceDays <= 0 ? 28 : _storeInformationSettings.UpdatingPriceDays;

            var discounts = _discountService.GetAllDiscounts(DiscountType.AssignedToSkus)
                .Where(d => d.StartDateUtc.HasValue
                         && d.StartDateUtc.Value.AddDays(days) <= nowUtc
                         && (!d.EndDateUtc.HasValue || d.EndDateUtc >= nowUtc))
                .ToList();
            //var key = DiscountEventConsumer.DISCOUNT_APPLIED_TO_PRODUCTS_TASK_KEY;
            //var discounts = _cacheManager.Get(key, () =>
            //{
            //    return _discountService.GetAllDiscounts(DiscountType.AssignedToSkus)
            //    .Where(d => d.StartDateUtc.HasValue
            //             && d.StartDateUtc.Value.AddDays(28) <= nowUtc
            //             && (!d.EndDateUtc.HasValue || d.EndDateUtc >= nowUtc))
            //    .ToList();
            //});
            foreach (var discount in discounts)
            {
                var products = discount.AppliedToProducts.Where(p => !p.Deleted).ToList();
                var discountAmount = decimal.Zero;
                foreach (var product in products)
                {
                    if (discount.UsePercentage)
                        discountAmount = product.Price * discount.DiscountPercentage / 100;
                    else
                        discountAmount = discount.DiscountAmount;
                    //update product price
                    product.Price -= discountAmount;
                    _productService.UpdateProduct(product);
                }
                //delete this discount
                _discountService.DeleteDiscount(discount);
            }
        }

        public void UpdateAttributeCombinationPrice(DateTime nowUtc)
        {
            //discount type    applied to applied to product attribute combination sku
            var days = _storeInformationSettings.UpdatingPriceDays <= 0 ? 28 : _storeInformationSettings.UpdatingPriceDays;

            var discounts = _discountService.GetAllDiscounts(DiscountType.AssignedToCustomSkus)
                .Where(d => d.StartDateUtc.HasValue
                         && d.StartDateUtc.Value.AddDays(days) <= nowUtc
                         && (!d.EndDateUtc.HasValue || d.EndDateUtc >= nowUtc))
                .ToList();
            //var key = DiscountEventConsumer.DISCOUNT_APPLIED_TO_ATTRIBUTE_SKU_TASK_KEY;
            //var discounts = _cacheManager.Get(key, () =>
            //{
            //    return _discountService.GetAllDiscounts(DiscountType.AssignedToCustomSkus)
            //    .Where(d => d.StartDateUtc.HasValue
            //             && d.StartDateUtc.Value.AddDays(28) <= nowUtc
            //             && (!d.EndDateUtc.HasValue || d.EndDateUtc >= nowUtc))
            //    .ToList();
            //});
            foreach (var discount in discounts)
            {
                var discountSkuMappings = discount.AppliedToProductSkus;
                var discountAmount = decimal.Zero;
                foreach (var discountSkuMapping in discountSkuMappings)
                {
                    var sku = discountSkuMapping.ProductSku;
                    var productAttributeCombination = _productAttributeService.GetProductAttributeCombinationBySku(sku);
                    if (productAttributeCombination.OverriddenPrice.HasValue)
                    {
                        if (discount.UsePercentage)
                            discountAmount = productAttributeCombination.OverriddenPrice.Value * discount.DiscountPercentage / 100;
                        else
                            discountAmount = discount.DiscountAmount;
                        //update product attribute combination override price
                        productAttributeCombination.OverriddenPrice -= discountAmount;
                        _productAttributeService.UpdateProductAttributeCombination(productAttributeCombination);
                    }
                }
                //delete this discount
                _discountService.DeleteDiscount(discount);
            }
        }
    }
}
