﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;
using Nop.Core.Plugins;
using Nop.Services.Catalog;
using Nop.Services.Catalog.Cache;
using Nop.Services.Customers;
using Nop.Services.Discounts.Cache;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Orders;

namespace Nop.Services.Discounts
{
    /// <summary>
    /// Discount service
    /// </summary>
    public partial class DiscountService : IDiscountService
    {
        #region Fields

        private readonly IRepository<Discount> _discountRepository;
        private readonly IRepository<DiscountRequirement> _discountRequirementRepository;
        private readonly IRepository<DiscountUsageHistory> _discountUsageHistoryRepository;
        private readonly IRepository<AppliedToProductSku> _appliedToProductSkuRepository;
        private readonly IRepository<ProductAttributeCombination> _productAttributeCombinationRepository;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IStoreContext _storeContext;
        private readonly ILocalizationService _localizationService;
        private readonly ICategoryService _categoryService;
        private readonly IPluginFinder _pluginFinder;
        private readonly IEventPublisher _eventPublisher;
        private readonly IManufacturerService _manufacturerService;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Static cache manager</param>
        /// <param name="discountRepository">Discount repository</param>
        /// <param name="discountRequirementRepository">Discount requirement repository</param>
        /// <param name="discountUsageHistoryRepository">Discount usage history repository</param>
        /// <param name="appliedToProductSkuRepository">Discount product sku history repository</param>
        /// <param name="storeContext">Store context</param>
        /// <param name="localizationService">Localization service</param>
        /// <param name="categoryService">Category service</param>
        /// <param name="pluginFinder">Plugin finder</param>
        /// <param name="eventPublisher">Event published</param>
        public DiscountService(IStaticCacheManager cacheManager,
            IRepository<Discount> discountRepository,
            IRepository<DiscountRequirement> discountRequirementRepository,
            IRepository<DiscountUsageHistory> discountUsageHistoryRepository,
            IRepository<AppliedToProductSku> appliedToProductSkuRepository,
            IRepository<ProductAttributeCombination> productAttributeCombinationRepository,
            IStoreContext storeContext,
            ILocalizationService localizationService,
            ICategoryService categoryService,
            IPluginFinder pluginFinder,
            IEventPublisher eventPublisher)
        {
            this._cacheManager = cacheManager;
            this._discountRepository = discountRepository;
            this._discountRequirementRepository = discountRequirementRepository;
            this._discountUsageHistoryRepository = discountUsageHistoryRepository;
            this._appliedToProductSkuRepository = appliedToProductSkuRepository;
            this._productAttributeCombinationRepository = productAttributeCombinationRepository;
            this._storeContext = storeContext;
            this._localizationService = localizationService;
            this._categoryService = categoryService;
            this._pluginFinder = pluginFinder;
            this._eventPublisher = eventPublisher;
            this._manufacturerService = EngineContext.Current.Resolve<IManufacturerService>();
        }

        #endregion

        #region Nested classes

        /// <summary>
        /// DiscountRequirement (for caching)
        /// </summary>
        [Serializable]
        public class DiscountRequirementForCaching
        {
            public DiscountRequirementForCaching()
            {
                ChildRequirements = new List<DiscountRequirementForCaching>();
            }

            public int Id { get; set; }
            public string SystemName { get; set; }
            public bool IsGroup { get; set; }
            public RequirementGroupInteractionType? InteractionType { get; set; }
            public IList<DiscountRequirementForCaching> ChildRequirements { get; set; }
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Get requirements for caching
        /// </summary>
        /// <param name="requirements">Collection of discount requirement</param>
        /// <returns>List of DiscountRequirementForCaching</returns>
        protected IList<DiscountRequirementForCaching> GetReqirementsForCaching(IEnumerable<DiscountRequirement> requirements)
        {
            var requirementForCaching = requirements.Select(requirement => new DiscountRequirementForCaching
            {
                Id = requirement.Id,
                IsGroup = requirement.IsGroup,
                SystemName = requirement.DiscountRequirementRuleSystemName,
                InteractionType = requirement.InteractionType,
                ChildRequirements = GetReqirementsForCaching(requirement.ChildRequirements)
            });
            
            return requirementForCaching.ToList();
        }

        /// <summary>
        /// Get discount validation result
        /// </summary>
        /// <param name="requirements">Collection of discount requirement</param>
        /// <param name="groupInteractionType">Interaction type within the group of requirements</param>
        /// <param name="customer">Customer</param>
        /// <param name="errors">Errors</param>
        /// <returns>True if result is valid; otherwise false</returns>
        protected bool GetValidationResult(IEnumerable<DiscountRequirementForCaching> requirements, 
            RequirementGroupInteractionType groupInteractionType, Customer customer, List<string> errors)
        {
            var result = false;

            foreach (var requirement in requirements)
            {
                if (requirement.IsGroup)
                {
                    //get child requirements for the group
                    var interactionType = requirement.InteractionType.HasValue 
                        ? requirement.InteractionType.Value : RequirementGroupInteractionType.And;
                    result = GetValidationResult(requirement.ChildRequirements, interactionType, customer, errors);
                }
                else
                {
                    //or try to get validation result for the requirement
                    var requirementRulePlugin = LoadDiscountRequirementRuleBySystemName(requirement.SystemName);
                    if (requirementRulePlugin == null)
                        continue;

                    if (!_pluginFinder.AuthorizedForUser(requirementRulePlugin.PluginDescriptor, customer))
                        continue;

                    if (!_pluginFinder.AuthenticateStore(requirementRulePlugin.PluginDescriptor, _storeContext.CurrentStore.Id))
                        continue;

                    var ruleResult = requirementRulePlugin.CheckRequirement(new DiscountRequirementValidationRequest
                    {
                        DiscountRequirementId = requirement.Id,
                        Customer = customer,
                        Store = _storeContext.CurrentStore
                    });

                    //add validation error
                    if (!ruleResult.IsValid)
                        errors.Add(ruleResult.UserError);

                    result = ruleResult.IsValid;
                }

                //all requirements must be met, so return false
                if (!result && groupInteractionType == RequirementGroupInteractionType.And)
                    return result;

                //any of requirements must be met, so return true
                if (result && groupInteractionType == RequirementGroupInteractionType.Or)
                    return result;
            }

            return result;
        }

        #endregion

        #region Methods

        #region Discounts

        /// <summary>
        /// Delete discount
        /// </summary>
        /// <param name="discount">Discount</param>
        public virtual void DeleteDiscount(Discount discount)
        {
            if (discount == null)
                throw new ArgumentNullException(nameof(discount));

            _discountRepository.Delete(discount);

            //event notification
            _eventPublisher.EntityDeleted(discount);
        }

        /// <summary>
        /// Gets a discount
        /// </summary>
        /// <param name="discountId">Discount identifier</param>
        /// <returns>Discount</returns>
        public virtual Discount GetDiscountById(int discountId)
        {
            if (discountId == 0)
                return null;

            return _discountRepository.GetById(discountId);
        }

        /// <summary>
        /// Gets all discounts
        /// </summary>
        /// <param name="discountType">Discount type; null to load all discount</param>
        /// <param name="couponCode">Coupon code to find (exact match)</param>
        /// <param name="discountName">Discount name</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Discounts</returns>
        public virtual IList<Discount> GetAllDiscounts(DiscountType? discountType = null,
            string couponCode = "", string discountName = "", bool showHidden = false)
        {
            var query = _discountRepository.Table;
            if (!showHidden)
            {
                //The function 'CurrentUtcDateTime' is not supported by SQL Server Compact. 
                //That's why we pass the date value
                var nowUtc = DateTime.UtcNow;
                query = query.Where(d =>
                    (!d.StartDateUtc.HasValue || d.StartDateUtc <= nowUtc)
                    && (!d.EndDateUtc.HasValue || d.EndDateUtc >= nowUtc));
            }
            if (!string.IsNullOrEmpty(couponCode))
            {
                query = query.Where(d => d.CouponCode == couponCode);
            }
            if (!string.IsNullOrEmpty(discountName))
            {
                query = query.Where(d => d.Name.Contains(discountName));
            }
            if (discountType.HasValue)
            {
                var discountTypeId = (int) discountType.Value;
                query = query.Where(d => d.DiscountTypeId == discountTypeId);
            }

            query = query.OrderBy(d => d.Name);

            var discounts = query.ToList();
            return discounts;
        }

        /// <summary>
        /// Inserts a discount
        /// </summary>
        /// <param name="discount">Discount</param>
        public virtual void InsertDiscount(Discount discount)
        {
            if (discount == null)
                throw new ArgumentNullException(nameof(discount));

            _discountRepository.Insert(discount);

            //event notification
            _eventPublisher.EntityInserted(discount);
        }

        /// <summary>
        /// Updates the discount
        /// </summary>
        /// <param name="discount">Discount</param>
        public virtual void UpdateDiscount(Discount discount)
        {
            if (discount == null)
                throw new ArgumentNullException(nameof(discount));

            _discountRepository.Update(discount);

            //event notification
            _eventPublisher.EntityUpdated(discount);
        }
        
        #endregion

        #region Discounts (caching)

        /// <summary>
        /// Gets all discounts (cachable models)
        /// </summary>
        /// <param name="discountType">Discount type; null to load all discount</param>
        /// <param name="couponCode">Coupon code to find (exact match)</param>
        /// <param name="discountName">Discount name</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Discounts</returns>
        public virtual IList<DiscountForCaching> GetAllDiscountsForCaching(DiscountType? discountType = null,
            string couponCode = "", string discountName = "", bool showHidden = false)
        {
            //we cache discounts between requests. Otherwise, they will be loaded for almost each HTTP request
            //we have to use the following workaround with cachable model (DiscountForCaching) because
            //Entity Framework doesn't support 2-level caching

            //we load all discounts, and filter them using "discountType" parameter later (in memory)
            //we do it because we know that this method is invoked several times per HTTP request with distinct "discountType" parameter
            //that's why let's access the database only once
            var key = string.Format(DiscountEventConsumer.DISCOUNT_ALL_KEY, showHidden, couponCode, discountName);
            var result = _cacheManager.Get(key, () =>
            {
                var discounts = GetAllDiscounts(null, couponCode, discountName, showHidden);
                return discounts.Select(d => d.MapDiscount()).ToList();
            });
            //we know that this method is usually inkoved multiple times
            //that's why we filter discounts by type on the application layer
            if (discountType.HasValue)
            {
                result = result.Where(d => d.DiscountType == discountType.Value).ToList();
            }
            return result;
        }

        /// <summary>
        /// Get category identifiers to which a discount is applied
        /// </summary>
        /// <param name="discount">Discount</param>
        /// <param name="customer">Customer</param>
        /// <returns>Category identifiers</returns>
        public virtual IList<int> GetAppliedCategoryIds(DiscountForCaching discount, Customer customer)
        {
            if (discount == null)
                throw new ArgumentNullException(nameof(discount));

            var discountId = discount.Id;
            var cacheKey = string.Format(DiscountEventConsumer.DISCOUNT_CATEGORY_IDS_MODEL_KEY,
                discountId,
                string.Join(",", customer.GetCustomerRoleIds()),
                _storeContext.CurrentStore.Id);
            var result = _cacheManager.Get(cacheKey, () =>
            {
                var ids = new List<int>();
                var rootCategoryIds = _discountRepository.Table.Where(x => x.Id == discountId)
                        .SelectMany(x => x.AppliedToCategories.Select(c => c.Id))
                        .ToList();
                foreach (var categoryId in rootCategoryIds)
                {
                    if (!ids.Contains(categoryId))
                        ids.Add(categoryId);
                    if (discount.AppliedToSubCategories)
                    {
                        //include subcategories
                        foreach (var childCategoryId in _categoryService
                            .GetAllCategoriesByParentCategoryId(categoryId, false, true)
                            .Select(x => x.Id))
                        {
                            if (!ids.Contains(childCategoryId))
                                ids.Add(childCategoryId);
                        }
                    }
                }
                return ids;
            });

            return result;
        }

        /// <summary>
        /// Get manufacturer identifiers to which a discount is applied
        /// </summary>
        /// <param name="discount">Discount</param>
        /// <param name="customer">Customer</param>
        /// <returns>Manufacturer identifiers</returns>
        public virtual IList<int> GetAppliedManufacturerIds(DiscountForCaching discount, Customer customer)
        {
            if (discount == null)
                throw new ArgumentNullException(nameof(discount));

            var discountId = discount.Id;
            var cacheKey = string.Format(DiscountEventConsumer.DISCOUNT_MANUFACTURER_IDS_MODEL_KEY,
                discountId,
                string.Join(",", customer.GetCustomerRoleIds()),
                _storeContext.CurrentStore.Id);
            var result = _cacheManager.Get(cacheKey, () =>
            {
                return _discountRepository.Table.Where(x => x.Id == discountId)
                    .SelectMany(x => x.AppliedToManufacturers.Select(c => c.Id))
                    .ToList();
            });

            return result;
        }

        #endregion

        #region Discount requirements

        /// <summary>
        /// Get all discount requirements
        /// </summary>
        /// <param name="discountId">Discount identifier</param>
        /// <param name="topLevelOnly">Whether to load top-level requirements only (without parent identifier)</param>
        /// <returns>Requirements</returns>
        public virtual IList<DiscountRequirement> GetAllDiscountRequirements(int discountId = 0, bool topLevelOnly = false)
        {
            var query = _discountRequirementRepository.Table;

            //filter by discount
            if (discountId > 0)
                query = query.Where(requirement => requirement.DiscountId == discountId);

            //filter by top-level
            if (topLevelOnly)
                query = query.Where(requirement => !requirement.ParentId.HasValue);

            query = query.OrderBy(requirement => requirement.Id);
            
            return query.ToList();
        }

        /// <summary>
        /// Delete discount requirement
        /// </summary>
        /// <param name="discountRequirement">Discount requirement</param>
        public virtual void DeleteDiscountRequirement(DiscountRequirement discountRequirement)
        {
            if (discountRequirement == null)
                throw new ArgumentNullException(nameof(discountRequirement));

            _discountRequirementRepository.Delete(discountRequirement);

            //event notification
            _eventPublisher.EntityDeleted(discountRequirement);
        }

        /// <summary>
        /// Load discount requirement rule by system name
        /// </summary>
        /// <param name="systemName">System name</param>
        /// <returns>Found discount requirement rule</returns>
        public virtual IDiscountRequirementRule LoadDiscountRequirementRuleBySystemName(string systemName)
        {
            var descriptor = _pluginFinder.GetPluginDescriptorBySystemName<IDiscountRequirementRule>(systemName);
            if (descriptor != null)
                return descriptor.Instance<IDiscountRequirementRule>();

            return null;
        }

        /// <summary>
        /// Load all discount requirement rules
        /// </summary>
        /// <param name="customer">Load records allowed only to a specified customer; pass null to ignore ACL permissions</param>
        /// <returns>Discount requirement rules</returns>
        public virtual IList<IDiscountRequirementRule> LoadAllDiscountRequirementRules(Customer customer = null)
        {
            return _pluginFinder.GetPlugins<IDiscountRequirementRule>(customer: customer).ToList();
        }
        
        #endregion

        #region Validation

        /// <summary>
        /// Validate discount
        /// </summary>
        /// <param name="discount">Discount</param>
        /// <param name="customer">Customer</param>
        /// <returns>Discount validation result</returns>
        public virtual DiscountValidationResult ValidateDiscount(Discount discount, Customer customer)
        {
            if (discount == null)
                throw new ArgumentNullException(nameof(discount));

            return ValidateDiscount(discount.MapDiscount(), customer);
        }

        /// <summary>
        /// Validate discount
        /// </summary>
        /// <param name="discount">Discount</param>
        /// <param name="customer">Customer</param>
        /// <param name="couponCodesToValidate">Coupon codes to validate</param>
        /// <returns>Discount validation result</returns>
        public virtual DiscountValidationResult ValidateDiscount(Discount discount, Customer customer, string[] couponCodesToValidate)
        {
            if (discount == null)
                throw new ArgumentNullException(nameof(discount));

            return ValidateDiscount(discount.MapDiscount(), customer, couponCodesToValidate);
        }

        /// <summary>
        /// Validate discount
        /// </summary>
        /// <param name="discount">Discount</param>
        /// <param name="customer">Customer</param>
        /// <returns>Discount validation result</returns>
        public virtual DiscountValidationResult ValidateDiscount(DiscountForCaching discount, Customer customer)
        {
            if (discount == null)
                throw new ArgumentNullException(nameof(discount));

            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            var couponCodesToValidate = customer.ParseAppliedDiscountCouponCodes();
            return ValidateDiscount(discount, customer, couponCodesToValidate);
        }

        /// <summary>
        /// Validate discount
        /// </summary>
        /// <param name="discount">Discount</param>
        /// <param name="customer">Customer</param>
        /// <param name="couponCodesToValidate">Coupon codes to validate</param>
        /// <returns>Discount validation result</returns>
        public virtual DiscountValidationResult ValidateDiscount(DiscountForCaching discount, Customer customer, string[] couponCodesToValidate)
        {
            if (discount == null)
                throw new ArgumentNullException(nameof(discount));

            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            //invalid by default
            var result = new DiscountValidationResult();

            //check coupon code
            if (discount.RequiresCouponCode)
            {
                if (string.IsNullOrEmpty(discount.CouponCode))
                    return result;

                if (couponCodesToValidate == null)
                    return result;

                if (!couponCodesToValidate.Any(x => x.Equals(discount.CouponCode, StringComparison.InvariantCultureIgnoreCase)))
                    return result;
            }

            //Do not allow discounts applied to order subtotal or total when a customer has gift cards in the cart.
            //Otherwise, this customer can purchase gift cards with discount and get more than paid ("free money").
            if (discount.DiscountType == DiscountType.AssignedToOrderSubTotal ||
                discount.DiscountType == DiscountType.AssignedToOrderTotal)
            {
                var cart = customer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .LimitPerStore(_storeContext.CurrentStore.Id)
                    .ToList();

                var hasGiftCards = cart.Any(x => x.Product.IsGiftCard);
                if (hasGiftCards)
                {
                    result.Errors = new List<string> { _localizationService.GetResource("ShoppingCart.Discount.CannotBeUsedWithGiftCards") };
                    return result;
                }
            }

            //check date range
            var now = DateTime.UtcNow;
            if (discount.StartDateUtc.HasValue)
            {
                var startDate = DateTime.SpecifyKind(discount.StartDateUtc.Value, DateTimeKind.Utc);
                if (startDate.CompareTo(now) > 0)
                {
                    result.Errors = new List<string> { _localizationService.GetResource("ShoppingCart.Discount.NotStartedYet") };
                    return result;
                }
            }
            if (discount.EndDateUtc.HasValue)
            {
                var endDate = DateTime.SpecifyKind(discount.EndDateUtc.Value, DateTimeKind.Utc);
                if (endDate.CompareTo(now) < 0)
                {
                    result.Errors = new List<string> { _localizationService.GetResource("ShoppingCart.Discount.Expired") };
                    return result;
                }
            }

            //discount limitation
            switch (discount.DiscountLimitation)
            {
                case DiscountLimitationType.NTimesOnly:
                    {
                        var usedTimes = GetAllDiscountUsageHistory(discount.Id, null, null, 0, 1).TotalCount;
                        if (usedTimes >= discount.LimitationTimes)
                            return result;
                    }
                    break;
                case DiscountLimitationType.NTimesPerCustomer:
                    {
                        if (customer.IsRegistered())
                        {
                            var usedTimes = GetAllDiscountUsageHistory(discount.Id, customer.Id, null, 0, 1).TotalCount;
                            if (usedTimes >= discount.LimitationTimes)
                            {
                                result.Errors = new List<string> { _localizationService.GetResource("ShoppingCart.Discount.CannotBeUsedAnymore") };
                                return result;
                            }
                        }
                    }
                    break;
                case DiscountLimitationType.Unlimited:
                default:
                    break;
            }

            //discount requirements
            var key = string.Format(DiscountEventConsumer.DISCOUNT_REQUIREMENT_MODEL_KEY, discount.Id);
            var requirementsForCaching = _cacheManager.Get(key, () =>
            {
                var requirements = GetAllDiscountRequirements(discount.Id, true);
                return GetReqirementsForCaching(requirements);
            });

            //get top-level group
            var topLevelGroup = requirementsForCaching.FirstOrDefault();
            if (topLevelGroup == null || (topLevelGroup.IsGroup && !topLevelGroup.ChildRequirements.Any()) || !topLevelGroup.InteractionType.HasValue)
            {
                //there are no requirements, so discount is valid
                result.IsValid = true;
                return result;
            }

            //requirements exist, let's check them
            var errors = new List<string>();
            result.IsValid = GetValidationResult(requirementsForCaching, topLevelGroup.InteractionType.Value, customer, errors);

            //set errors if result is not valid
            if (!result.IsValid)
                result.Errors = errors;

            return result;
        }

        /// <summary>
        /// Validate discount
        /// </summary>
        /// <param name="discount">Discount</param>
        /// <returns>Discount validation result</returns>
        public virtual DiscountValidationResult ValidateDiscount(DiscountForCaching discount)
        {
            if (discount == null)
                throw new ArgumentNullException(nameof(discount));

            //var key = string.Format(DiscountEventConsumer.DISCOUNT_REQUIREMENT_MODEL_KEY, discount.Id);
            //var requirementsForCaching = _cacheManager.Get(key, () =>
            //{
            //    var requirements = GetAllDiscountRequirements(discount.Id, true);
            //    return GetReqirementsForCaching(requirements);
            //});

            //invalid by default
            var result = new DiscountValidationResult();

            //check date range
            var now = DateTime.UtcNow;
            if (discount.StartDateUtc.HasValue)
            {
                var startDate = DateTime.SpecifyKind(discount.StartDateUtc.Value, DateTimeKind.Utc);
                if (startDate.CompareTo(now) > 0)
                {
                    result.Errors = new List<string> { _localizationService.GetResource("ShoppingCart.Discount.NotStartedYet") };
                    return result;
                }
            }
            if (discount.EndDateUtc.HasValue)
            {
                var endDate = DateTime.SpecifyKind(discount.EndDateUtc.Value, DateTimeKind.Utc);
                if (endDate.CompareTo(now) < 0)
                {
                    result.Errors = new List<string> { _localizationService.GetResource("ShoppingCart.Discount.Expired") };
                    return result;
                }
            }

            //discount limitation
            switch (discount.DiscountLimitation)
            {
                case DiscountLimitationType.NTimesOnly:
                    {
                        var usedTimes = GetAllDiscountUsageHistory(discount.Id, null, null, 0, 1).TotalCount;
                        if (usedTimes >= discount.LimitationTimes)
                            return result;
                    }
                    break;
                case DiscountLimitationType.NTimesPerCustomer:
                case DiscountLimitationType.Unlimited:
                default:
                    break;
            }
            result.IsValid = true;
            return result;
        }

        #endregion

        #region Discount usage history

        /// <summary>
        /// Gets a discount usage history record
        /// </summary>
        /// <param name="discountUsageHistoryId">Discount usage history record identifier</param>
        /// <returns>Discount usage history</returns>
        public virtual DiscountUsageHistory GetDiscountUsageHistoryById(int discountUsageHistoryId)
        {
            if (discountUsageHistoryId == 0)
                return null;

            return _discountUsageHistoryRepository.GetById(discountUsageHistoryId);
        }

        /// <summary>
        /// Gets all discount usage history records
        /// </summary>
        /// <param name="discountId">Discount identifier; null to load all records</param>
        /// <param name="customerId">Customer identifier; null to load all records</param>
        /// <param name="orderId">Order identifier; null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Discount usage history records</returns>
        public virtual IPagedList<DiscountUsageHistory> GetAllDiscountUsageHistory(int? discountId = null,
            int? customerId = null, int? orderId = null, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var discountUsageHistory = _discountUsageHistoryRepository.Table;

            //filter by discount
            if (discountId.HasValue && discountId.Value > 0)
                discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.DiscountId == discountId.Value);

            //filter by customer
            if (customerId.HasValue && customerId.Value > 0)
                discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.Order != null && historyRecord.Order.CustomerId == customerId.Value);

            //filter by order
            if (orderId.HasValue && orderId.Value > 0)
                discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.OrderId == orderId.Value);

            //ignore deleted orders
            discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.Order != null && !historyRecord.Order.Deleted);

            //order
            discountUsageHistory = discountUsageHistory.OrderByDescending(historyRecord => historyRecord.CreatedOnUtc).ThenBy(historyRecord => historyRecord.Id);

            return new PagedList<DiscountUsageHistory>(discountUsageHistory, pageIndex, pageSize);
        }

        /// <summary>
        /// Insert discount usage history record
        /// </summary>
        /// <param name="discountUsageHistory">Discount usage history record</param>
        public virtual void InsertDiscountUsageHistory(DiscountUsageHistory discountUsageHistory)
        {
            if (discountUsageHistory == null)
                throw new ArgumentNullException(nameof(discountUsageHistory));

            _discountUsageHistoryRepository.Insert(discountUsageHistory);
            
            //event notification
            _eventPublisher.EntityInserted(discountUsageHistory);
        }

        /// <summary>
        /// Update discount usage history record
        /// </summary>
        /// <param name="discountUsageHistory">Discount usage history record</param>
        public virtual void UpdateDiscountUsageHistory(DiscountUsageHistory discountUsageHistory)
        {
            if (discountUsageHistory == null)
                throw new ArgumentNullException(nameof(discountUsageHistory));

            _discountUsageHistoryRepository.Update(discountUsageHistory);
            
            //event notification
            _eventPublisher.EntityUpdated(discountUsageHistory);
        }

        /// <summary>
        /// Delete discount usage history record
        /// </summary>
        /// <param name="discountUsageHistory">Discount usage history record</param>
        public virtual void DeleteDiscountUsageHistory(DiscountUsageHistory discountUsageHistory)
        {
            if (discountUsageHistory == null)
                throw new ArgumentNullException(nameof(discountUsageHistory));

            _discountUsageHistoryRepository.Delete(discountUsageHistory);
            
            //event notification
            _eventPublisher.EntityDeleted(discountUsageHistory);
        }

        #endregion

        #region Discount Product Sku
        /// <summary>
        /// if only has discount of applied to product attribute combination sku
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public virtual bool OnlyHasAppliedToSkuDiscounts(Product product, Customer customer, string combinationSku = null)
        {
            if(product == null)
                return false;

            //discounts applied to products
            if (product.HasDiscountsApplied)
            {
                //we use this property ("HasDiscountsApplied") for performance optimization to avoid unnecessary database calls
                foreach (var discount in product.AppliedDiscounts)
                {
                    if (ValidateDiscount(discount, customer).IsValid &&
                        discount.DiscountType == DiscountType.AssignedToSkus)
                        return false;
                }
            }

            //discounts applied to categories
            foreach (var discount in GetAllDiscountsForCaching(DiscountType.AssignedToCategories))
            {
                //load identifier of categories with this discount applied to
                var discountCategoryIds = GetAppliedCategoryIds(discount, customer);

                //compare with categories of this product
                var productCategoryIds = new List<int>();
                if (discountCategoryIds.Any())
                {
                    //load identifier of categories of this product
                    var cacheKey = string.Format(PriceCacheEventConsumer.PRODUCT_CATEGORY_IDS_MODEL_KEY,
                        product.Id,
                        string.Join(",", customer.GetCustomerRoleIds()),
                        _storeContext.CurrentStore.Id);
                    productCategoryIds = _cacheManager.Get(cacheKey, () =>
                        _categoryService
                        .GetProductCategoriesByProductId(product.Id)
                        .Select(x => x.CategoryId)
                        .ToList());
                }

                foreach (var categoryId in productCategoryIds)
                {
                    if (discountCategoryIds.Contains(categoryId))
                    {
                        if (ValidateDiscount(discount, customer).IsValid)
                            return false;
                    }
                }
            }

            //discounts applied to manufacturers
            foreach (var discount in GetAllDiscountsForCaching(DiscountType.AssignedToManufacturers))
            {
                //load identifier of manufacturers with this discount applied to
                var discountManufacturerIds = GetAppliedManufacturerIds(discount, customer);

                //compare with manufacturers of this product
                var productManufacturerIds = new List<int>();
                if (discountManufacturerIds.Any())
                {
                    //load identifier of manufacturers of this product
                    var cacheKey = string.Format(PriceCacheEventConsumer.PRODUCT_MANUFACTURER_IDS_MODEL_KEY,
                        product.Id,
                        string.Join(",", customer.GetCustomerRoleIds()),
                        _storeContext.CurrentStore.Id);
                    productManufacturerIds = _cacheManager.Get(cacheKey, () =>
                        _manufacturerService
                        .GetProductManufacturersByProductId(product.Id)
                        .Select(x => x.ManufacturerId)
                        .ToList());
                }

                foreach (var manufacturerId in productManufacturerIds)
                {
                    if (discountManufacturerIds.Contains(manufacturerId))
                    {
                        if (ValidateDiscount(discount, customer).IsValid)
                            return false;
                    }
                }
            }

            //discounts applied to product attributes sku
            return HasAppliedToSkuDiscounts(product, combinationSku);
        }

        /// <summary>
        /// has discount of applied to product attribute combination sku
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public virtual bool HasAppliedToSkuDiscounts(Product product, string combinationSku = null)
        {
            if (product == null)
                return false;

            var pid = product.Id;
            var query = _discountRepository.Table.Where(x=> x.DiscountTypeId == (int)DiscountType.AssignedToCustomSkus);
            var apsQuery = _appliedToProductSkuRepository.Table;
            var nowUtc = DateTime.UtcNow;
            if (!string.IsNullOrEmpty(combinationSku))
            {
                apsQuery = from d in query
                        join aps in apsQuery on d.Id equals aps.DiscountId
                        where aps.ProductId == pid 
                            && aps.ProductSku == combinationSku
                            && (!d.StartDateUtc.HasValue || d.StartDateUtc <= nowUtc)
                            && (!d.EndDateUtc.HasValue || d.EndDateUtc >= nowUtc)
                           select aps;
            }
            else
            {
                apsQuery = from d in query
                           join aps in apsQuery on d.Id equals aps.DiscountId
                           where aps.ProductId == pid
                              && (!d.StartDateUtc.HasValue || d.StartDateUtc <= nowUtc)
                              && (!d.EndDateUtc.HasValue || d.EndDateUtc >= nowUtc)
                           select aps;
            }
            if (apsQuery.Any())
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// whether to show general discount ribbon
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public virtual bool ShowGeneralDiscountRibbon(Product product)
        {
            if (product == null)
                return false;

            var pid = product.Id;
            var query = _discountRepository.Table.Where(x => x.DiscountTypeId == (int)DiscountType.AssignedToCustomSkus);
            var apsQuery = _appliedToProductSkuRepository.Table;
            var nowUtc = DateTime.UtcNow;
            var result = from aps in apsQuery
                         join d in query on aps.DiscountId equals d.Id
                         where aps.ProductId == pid
                            && (!d.StartDateUtc.HasValue || d.StartDateUtc <= nowUtc)
                            && (!d.EndDateUtc.HasValue || d.EndDateUtc >= nowUtc)
                         select d;
            if (result.Any(d => d.ShowGeneralRibbon))
                return true;
            else
                return false;
        }

        /// <summary>
        /// get discount amount of product attribute combination skus
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public virtual decimal GetBestDiscountAmount(Product product)
        {
            if (product == null)
                return decimal.Zero;

            var pid = product.Id;
            var query = _discountRepository.Table.Where(x => x.DiscountTypeId == (int)DiscountType.AssignedToCustomSkus);
            var apsQuery = _appliedToProductSkuRepository.Table;
            var nowUtc = DateTime.UtcNow;
            var result = from aps in apsQuery
                         join d in query on aps.DiscountId equals d.Id
                         where aps.ProductId == pid
                            && (!d.StartDateUtc.HasValue || d.StartDateUtc <= nowUtc)
                            && (!d.EndDateUtc.HasValue || d.EndDateUtc >= nowUtc)
                         orderby d.DiscountAmount descending
                         select d;
            if (result.Any())
                return result.FirstOrDefault().DiscountAmount;
            else
                return decimal.Zero;
        }

        /// <summary>
        /// Get discount product sku record using discount amount descending
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public virtual AppliedToProductSku GetAppliedToProductByBestDiscount(Product product)
        {
            var nowUtc = DateTime.UtcNow;
            var apsQuery = _appliedToProductSkuRepository.Table;
            var query = _discountRepository.Table.Where(x => x.DiscountTypeId == (int)DiscountType.AssignedToCustomSkus);
            var comb = _productAttributeCombinationRepository.Table;
            var combQuery = from aps in apsQuery
                       join d in query on aps.DiscountId equals d.Id
                       join c in comb on aps.ProductSku equals c.Sku
                       where aps.ProductId == product.Id 
                           && (!d.StartDateUtc.HasValue || d.StartDateUtc <= nowUtc)
                           && (!d.EndDateUtc.HasValue || d.EndDateUtc >= nowUtc)
                       orderby d.DiscountAmount descending, c.StockQuantity descending
                       select c;
            var combStock = combQuery.Where(x => x.StockQuantity > 0);
            var sku = string.Empty;
            if(combStock.Count() > 0)
                sku = combStock.FirstOrDefault().Sku;
            else
            {
                if(combQuery.Count() > 0)
                    sku = combQuery.FirstOrDefault().Sku;
                else
                    return null;
            }
            var result = apsQuery.Where(x => x.ProductId == product.Id && x.ProductSku == sku);
            return result.FirstOrDefault();
        }

        /// <summary>
        /// Get discount product sku record
        /// </summary>
        /// <param name="discountId">Discount id</param>
        /// <param name="productId">Discount product id</param>
        /// <param name="productSku">Discount product sku record</param>
        public virtual AppliedToProductSku GetAppliedToProductSkuById(int discountId, int productId, string productSku = "")
        {
            if (String.IsNullOrEmpty(productSku))
                return null;

            var query = _appliedToProductSkuRepository.Table;
            var productSkus = from aps in _appliedToProductSkuRepository.Table
                              where aps.DiscountId == discountId && aps.ProductSku == productSku
                              && aps.ProductId == productId
                              select aps;

            return productSkus.FirstOrDefault();
        }

        /// <summary>
        /// Get discount product sku record using cache
        /// </summary>
        /// <param name="discountId"></param>
        /// <param name="productId"></param>
        /// <param name="productSku"></param>
        /// <returns></returns>
        public virtual AppliedToProductSku GetAppliedToProductSkuByProductSku(int discountId, int productId, string productSku = "")
        {
            if (String.IsNullOrEmpty(productSku))
                return null;
            var cacheKey = string.Format(DiscountEventConsumer.DISCOUNT_PRODUCT_ATTRIBUTE_SKU_MODEL_KEY,
                discountId,
                _storeContext.CurrentStore.Id);
            var result = _cacheManager.Get(cacheKey, () => {
                return _appliedToProductSkuRepository.Table
                            .Where(aps => aps.DiscountId == discountId
                            && aps.ProductSku == productSku 
                            && aps.ProductId == productId).FirstOrDefault();
            });
            return result;
        }

        /// <summary>
        /// Insert discount product sku record
        /// </summary>
        /// <param name="productSku">Discount  product sku record</param>
        public virtual void InsertDiscountProductSku(AppliedToProductSku productSku)
        {
            if (productSku == null)
                throw new ArgumentNullException(nameof(productSku));

            _appliedToProductSkuRepository.Insert(productSku);

            //event notification
            _eventPublisher.EntityInserted(productSku);
        }

        /// <summary>
        /// Delete discount product sku record
        /// </summary>
        /// <param name="productSku">Discount product sku record</param>
        public virtual void DeleteDiscountProductSku(AppliedToProductSku productSku)
        {
            if (productSku == null)
                throw new ArgumentNullException(nameof(productSku));

            _appliedToProductSkuRepository.Delete(productSku);

            //event notification
            _eventPublisher.EntityDeleted(productSku);
        }
        /// <summary>
        /// Delete discount product sku record
        /// </summary>
        /// <param name="productSku">Discount product sku record</param>

        public virtual void DeleteDiscountProductSkus(List<AppliedToProductSku> productSkus)
        {
            if (productSkus == null)
                throw new ArgumentNullException(nameof(productSkus));

            _appliedToProductSkuRepository.Delete(productSkus);

            //event notification
            //_eventPublisher.EntityDeleted(productSkus.ToString());
        }
        #endregion

        #endregion
    }
}
