﻿using Ecorenew.Common.DocuSignWeb;
using System.Collections.Generic;

namespace Ecorenew.Common.DocuSign.Services
{
    public interface IDocuSignService
    {
        EnvelopeStatus SendEnvelope(string subject, string emailBlurb, IEnumerable<Recipient> recipients, IEnumerable<Document> documents, IEnumerable<Tab> tabs);

        EnvelopeStatus SendEnvelopeEmbedded(string subject, string emailBlurb, IEnumerable<Recipient> recipients, IEnumerable<Document> documents, IEnumerable<Tab> tabs, string clientUserId);

        string RequestRecipientToken(string envelopeId, string clientUserId, string docusignUserName, string email, int storeId);

        EnvelopeStatus RequestEnvelopeStatus(string envelopeId);

        Envelope RequestEnvelope(string envelopeId);

        Recipient CreateDocuSignRecipient(string email, string userName, string Id, RecipientTypeCode type, ushort? routingOrder = null);

        Document CreateDocuSignDocument(string name, string id, byte[] pdfBytes);

        Tab CreateDocuSignTab(string documentId, string recipientId, TabTypeCode type, int pageNumber, decimal xPosition, decimal yPosition, bool isRequired, CustomTabType? customTabType = null);
    }
}