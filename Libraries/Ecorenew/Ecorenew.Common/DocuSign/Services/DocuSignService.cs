﻿using Ecorenew.Common.DocuSign.Domain;
using Ecorenew.Common.DocuSignWeb;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Ecorenew.Common.DocuSign.Services
{
    public class DocuSignService : IDocuSignService
    {
        #region Fields

        private readonly IStoreService _storeService;
        private readonly DocuSignSettings _docuSignSettings;

        #endregion

        #region Ctor

        public DocuSignService(IStoreService storeService,
            DocuSignSettings docuSignSettings)
        {
            this._storeService = storeService;
            this._docuSignSettings = docuSignSettings;
        }

        #endregion

        #region Utilities

        protected virtual string GetDocuSignAuthenticationString()
        {
            return "<DocuSignCredentials><Username>" + _docuSignSettings.UserName
                + "</Username><Password>" + _docuSignSettings.Password
                + "</Password><IntegratorKey>" + _docuSignSettings.IntegratorKey
                + "</IntegratorKey></DocuSignCredentials>";
        }

        protected virtual DSAPIServiceSoapClient GetDocuSignClient()
        {
            return new DSAPIServiceSoapClient(new BasicHttpBinding(BasicHttpSecurityMode.Transport), new EndpointAddress("https://demo.docusign.net/api/3.0/dsapi.asmx"));
        }

        protected virtual EnvelopeStatus SendEnvelope(string subject, string emailBlurb, IEnumerable<Recipient> recipients, IEnumerable<Document> documents, IEnumerable<Tab> tabs, string clientUserId)
        {
            string auth = GetDocuSignAuthenticationString();

            using (DSAPIServiceSoapClient client = GetDocuSignClient())
            {
                using (OperationContextScope scope = new System.ServiceModel.OperationContextScope(client.InnerChannel))
                {
                    HttpRequestMessageProperty httpRequestProperty = new HttpRequestMessageProperty();
                    httpRequestProperty.Headers.Add("X-DocuSign-Authentication", auth);
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                    // Create the envelope content
                    Envelope envelope = new Envelope();
                    envelope.Subject = subject;
                    envelope.EmailBlurb = emailBlurb;
                    envelope.AccountId = _docuSignSettings.AccountId;

                    envelope.Recipients = recipients.ToArray();
                    envelope.Documents = documents.ToArray();
                    envelope.Tabs = tabs.ToArray();

                    if (!string.IsNullOrWhiteSpace(clientUserId))
                    {
                        envelope.Recipients[0].CaptiveInfo = new RecipientCaptiveInfo();
                        envelope.Recipients[0].CaptiveInfo.ClientUserId = clientUserId;
                    }

                    // Create the envelope on the account
                    EnvelopeStatus status = client.CreateEnvelope(envelope);

                    // Send the envelope
                    return client.SendEnvelope(status.EnvelopeID, _docuSignSettings.AccountId);
                }
            }
        }

        #endregion

        #region Methods

        public EnvelopeStatus SendEnvelope(string subject, string emailBlurb, IEnumerable<Recipient> recipients, IEnumerable<Document> documents, IEnumerable<Tab> tabs)
        {
            return SendEnvelope(subject, emailBlurb, recipients, documents, tabs, null);
        }

        public EnvelopeStatus SendEnvelopeEmbedded(string subject, string emailBlurb, IEnumerable<Recipient> recipients, IEnumerable<Document> documents, IEnumerable<Tab> tabs, string clientUserId)
        {
            return SendEnvelope(subject, emailBlurb, recipients, documents, tabs, clientUserId);
        }

        public string RequestRecipientToken(string envelopeId, string clientUserId, string docusignUserName, string email, int storeId)
        {
            string auth = GetDocuSignAuthenticationString();

            using (DSAPIServiceSoapClient client = GetDocuSignClient())
            {
                using (OperationContextScope scope = new System.ServiceModel.OperationContextScope(client.InnerChannel))
                {
                    HttpRequestMessageProperty httpRequestProperty = new HttpRequestMessageProperty();
                    httpRequestProperty.Headers.Add("X-DocuSign-Authentication", auth);
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                    RequestRecipientTokenAuthenticationAssertion assertion = new RequestRecipientTokenAuthenticationAssertion();
                    assertion.AssertionID = new Guid().ToString();
                    assertion.AuthenticationInstant = DateTime.Now;
                    assertion.AuthenticationMethod = RequestRecipientTokenAuthenticationAssertionAuthenticationMethod.Password;

                    assertion.SecurityDomain = "Request Recipient Token Test";

                    // get store
                    var store = _storeService.GetStoreById(storeId);

                    if (store == null)
                    {
                        throw new Exception("Store not found.");
                    }

                    string baseUrl = ((store.SslEnabled && !string.IsNullOrWhiteSpace(store.SecureUrl)) ? store.SecureUrl : store.Url).TrimEnd('/');
                    string checkoutUrl = baseUrl + "/checkout";
                    string cartUrl = baseUrl + "/cart";

                    // construct the URLs based on username
                    RequestRecipientTokenClientURLs urls = new RequestRecipientTokenClientURLs();

                    urls.OnSigningComplete = checkoutUrl;
                    urls.OnViewingComplete = cartUrl;
                    urls.OnCancel = cartUrl;
                    urls.OnDecline = cartUrl;
                    urls.OnSessionTimeout = cartUrl;
                    urls.OnTTLExpired = cartUrl;
                    urls.OnIdCheckFailed = cartUrl;
                    urls.OnAccessCodeFailed = cartUrl;
                    urls.OnException = cartUrl;

                    // request the token for a specific recipient
                    String token = client.RequestRecipientToken(envelopeId,
                        clientUserId, docusignUserName,
                        email, assertion, urls);

                    return token;
                }
            }
        }

        public EnvelopeStatus RequestEnvelopeStatus(string envelopeId)
        {
            string auth = GetDocuSignAuthenticationString();

            DSAPIServiceSoapClient client = GetDocuSignClient();

            using (OperationContextScope scope = new System.ServiceModel.OperationContextScope(client.InnerChannel))
            {
                HttpRequestMessageProperty httpRequestProperty = new HttpRequestMessageProperty();
                httpRequestProperty.Headers.Add("X-DocuSign-Authentication", auth);
                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                return client.RequestStatus(envelopeId);
            }
        }

        public Envelope RequestEnvelope(string envelopeId)
        {
            string auth = GetDocuSignAuthenticationString();

            DSAPIServiceSoapClient client = GetDocuSignClient();

            using (OperationContextScope scope = new System.ServiceModel.OperationContextScope(client.InnerChannel))
            {
                HttpRequestMessageProperty httpRequestProperty = new HttpRequestMessageProperty();
                httpRequestProperty.Headers.Add("X-DocuSign-Authentication", auth);
                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                return client.RequestEnvelope(envelopeId, false);
            }
        }

        public Recipient CreateDocuSignRecipient(string email, string userName, string Id, RecipientTypeCode type, ushort? routingOrder = null)
        {
            var recipient = new Recipient();

            recipient.Email = email;
            recipient.UserName = userName;
            recipient.ID = Id;
            recipient.Type = type;

            if (routingOrder.HasValue)
            {
                recipient.RoutingOrder = routingOrder.Value;
                recipient.RoutingOrderSpecified = true;
            }

            return recipient;
        }

        public Document CreateDocuSignDocument(string name, string id, byte[] pdfBytes)
        {
            return new Document
            {
                Name = name,
                ID = id,
                PDFBytes = pdfBytes
            };
        }

        public Tab CreateDocuSignTab(string documentId, string recipientId, TabTypeCode type, int pageNumber, decimal xPosition, decimal yPosition, bool isRequired, CustomTabType? customTabType = null)
        {
            var tab = new Tab();

            tab.DocumentID = documentId;
            tab.RecipientID = recipientId;
            tab.Type = type;
            tab.PageNumber = pageNumber.ToString();
            tab.XPosition = xPosition.ToString();
            tab.YPosition = yPosition.ToString();
            tab.CustomTabRequired = isRequired;
            tab.CustomTabRequiredSpecified = true;

            if (customTabType.HasValue)
            {
                tab.CustomTabType = customTabType.Value;
                tab.CustomTabTypeSpecified = true;
            }

            return tab;
        }

        #endregion
    }
}