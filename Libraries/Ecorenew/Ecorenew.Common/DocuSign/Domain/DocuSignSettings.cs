﻿using Nop.Core.Configuration;

namespace Ecorenew.Common.DocuSign.Domain
{
    public class DocuSignSettings : ISettings
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string IntegratorKey { get; set; }
        public string AccountId { get; set; }
    }
}