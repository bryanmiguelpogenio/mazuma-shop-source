﻿using Nop.Core;
using System;

namespace Ecorenew.Common.CallCredit.Domain
{
    /// <summary>
    /// Represents a CallValidate result
    /// </summary>
    public class CallValidateResult : BaseEntity
    {
        /// <summary>
        /// Gets or sets the PID provided by CallCredit
        /// </summary>
        public string Pid { get; set; }

        /// <summary>
        /// Gets or sets the encrypted CallValidate result XML
        /// </summary>
        public string EncryptedResultXml { get; set; }

        /// <summary>
        /// Gets or sets the date and time in UTC when the entity has been created
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the creator's customer identifier
        /// </summary>
        public int CreatedByCustomerId { get; set; }
    }
}