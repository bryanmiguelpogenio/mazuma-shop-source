﻿using Nop.Core;
using System;

namespace Ecorenew.Common.CallCredit.Domain
{
    /// <summary>
    /// Represents a CallReport search result
    /// </summary>
    public class CallReportResult : BaseEntity
    {
        /// <summary>
        /// Gets or sets the search ID provided by CallCredit
        /// </summary>
        public string SearchId { get; set; }

        /// <summary>
        /// Gets or sets the encrypted result XML
        /// </summary>
        public string EncryptedResultXml { get; set; }

        /// <summary>
        /// Gets or sets the date and time
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the creator's customer identifier
        /// </summary>
        public int CreatedByCustomerId { get; set; }
    }
}