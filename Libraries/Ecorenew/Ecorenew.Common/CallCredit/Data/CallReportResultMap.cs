﻿using Ecorenew.Common.CallCredit.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Ecorenew.Common.CallCredit.Data
{
    public class CallReportResultMap : EntityTypeConfiguration<CallReportResult>
    {
        public CallReportResultMap()
        {
            this.ToTable("Ecorenew_CallReportResult");
            this.HasKey(cr => cr.Id);

            this.Property(cr => cr.SearchId).IsRequired().HasMaxLength(100);
            this.Property(cr => cr.EncryptedResultXml).IsRequired();
        }
    }
}