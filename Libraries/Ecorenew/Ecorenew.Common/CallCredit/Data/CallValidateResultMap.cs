﻿using Ecorenew.Common.CallCredit.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Ecorenew.Common.CallCredit.Data
{
    public class CallValidateResultMap : EntityTypeConfiguration<CallValidateResult>
    {
        public CallValidateResultMap()
        {
            this.ToTable("Ecorenew_CallValidateResult");
            this.HasKey(cv => cv.Id);

            this.Property(cv => cv.Pid).IsRequired().HasMaxLength(100);
            this.Property(cv => cv.EncryptedResultXml).IsRequired();
        }
    }
}