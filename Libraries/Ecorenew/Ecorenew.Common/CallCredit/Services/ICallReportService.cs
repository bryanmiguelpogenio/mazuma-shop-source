﻿using Callcredit.CallReport7;
using Ecorenew.Common.CallCredit.Domain;

namespace Ecorenew.Common.CallCredit.Services
{
    public interface ICallReportService
    {
        /// <summary>
        /// Execute CallReport search
        /// </summary>
        /// <param name="company">Company</param>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="searchDefinition">Search definition</param>
        /// <param name="apiTestUrl">CallReport API Test URL</param>
        /// <param name="apiLiveUrl">CallReport API Live URL</param>
        /// <param name="useLiveUrl">Use Live API URL?</param>
        CT_SearchResult CallReportSearch(
            string company, 
            string username, 
            string password, 
            CT_SearchDefinition searchDefinition, 
            string apiTestUrl, 
            string apiLiveUrl, 
            bool useLiveUrl);

        /// <summary>
        /// Gets a CallReportResult
        /// </summary>
        /// <param name="id">CallReportResult identifier</param>
        CallReportResult GetCallReportResultById(int id);

        /// <summary>
        /// Process CallReport picklist
        /// </summary>
        /// <param name="picklist">CallReport search result picklist</param>
        CallReportProcessPicklistsResult ProcessPicklist(CT_apipicklist picklist);

        /// <summary>
        /// Adds a new CallReportResult record to the database
        /// </summary>
        /// <param name="result">CallReport search result</param>
        /// <param name="customerId">Customer identifier</param>
        CallReportResult SaveCallReportResult(CT_SearchResult result, int customerId = 0);
    }
}