﻿using CallCredit.CallValidateApi.ResponseEntities;
using Ecorenew.Common.CallCredit.Domain;

namespace Ecorenew.Common.CallCredit.Services
{
    /// <summary>
    /// CallValidate service interface
    /// </summary>
    public interface ICallValidateService
    {
        /// <summary>
        /// Performs a CallValidate check and saves the result
        /// </summary>
        /// <param name="company">Company</param>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="application">Application</param>
        /// <param name="data">Data to check</param>
        /// <param name="apiTestUrl">API URL for testing</param>
        /// <param name="apiLiveUrl">API URL for live</param>
        /// <param name="useLiveUrl">Use live API URL</param>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="IdEnhancedCheck">Perform ID Enhanced check?</param>
        /// <param name="BankStandardCheck">Perform Bank Standard check?</param>
        /// <param name="BankEnhancedCheck">Perform Bank Enhanced check?</param>
        CallValidateResult PerformCallValidateChecks(
            string company, 
            string username, 
            string password, 
            string application,
            global::CallCredit.CallValidateApi.RequestEntities.Data data,
            string apiTestUrl,
            string apiLiveUrl,
            bool useLiveUrl,
            int customerId = 0,
            bool IdEnhancedCheck = true,
            bool BankStandardCheck = true,
            bool BankEnhancedCheck = true);

        /// <summary>
        /// Returns the results of the decrypted CallValidateResult result XML
        /// </summary>
        /// <param name="callValidateResultId">CallValidateResult identifier</param>
        Results GetCallValidateResultResults(int callValidateResultId);

        /// <summary>
        /// Returns the results of the decrypted CallValidateResult result XML
        /// </summary>
        /// <param name="callValidateResult">CallValidateResult</param>
        Results GetCallValidateResultResults(CallValidateResult callValidateResult);
    }
}