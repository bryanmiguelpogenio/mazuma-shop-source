﻿using Callcredit.CallReport7;
using System.Collections.Generic;

namespace Ecorenew.Common.CallCredit.Services
{
    /// <summary>
    /// Represents the result for processing the picklists
    /// </summary>
    public class CallReportProcessPicklistsResult
    {
        /// <summary>
        /// Gets or sets the value indicating whether the picklists has been regenerated
        /// </summary>
        public bool PicklistsRegenerated { get; set; }

        public CT_SearchDefinition CallReportSearchDefinition { get; set; }

        public CT_SearchResult CallReportApiResult { get; set; }

        public List<NameMatchSelection> NameMatchSelectionList { get; set; }

        public List<AddressMatchSelection> AddressMatchSelectionList { get; set; }
    }

    /// <summary>
    /// Represents a selection for name matches
    /// </summary>
    public class NameMatchSelection
    {
        /// <summary>
        /// Gets or sets the dictionary for the name matches
        /// </summary>
        public IDictionary<int, string> NameDictionary { get; set; }

        /// <summary>
        /// Gets or sets the applicant index
        /// </summary>
        public int ApplicantIndex { get; set; }

        /// <summary>
        /// Gets or sets the applicant address index
        /// </summary>
        public int ApplicantAddressIndex { get; set; }

        /// <summary>
        /// Gets or sets the applicant address fullmatch index
        /// </summary>
        public int ApplicantAddressFullMatchIndex { get; set; }

        /// <summary>
        /// Gets or sets the applicant address fullmatch name index
        /// </summary>
        public int ApplicantAddressFullMatchNameIndex { get; set; }
    }

    /// <summary>
    /// Represents a selection for address matches
    /// </summary>
    public class AddressMatchSelection
    {
        /// <summary>
        /// Gets or sets dictionary for the address matches
        /// </summary>
        public IDictionary<int, string> AddressFullMatchDictionary { get; set; }

        /// <summary>
        /// Gets or sets the applicant index
        /// </summary>
        public int ApplicantIndex { get; set; }

        /// <summary>
        /// Gets or sets the applicant address index
        /// </summary>
        public int ApplicantAddressIndex { get; set; }
    }
}