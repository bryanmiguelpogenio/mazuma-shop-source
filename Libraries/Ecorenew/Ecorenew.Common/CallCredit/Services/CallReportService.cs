﻿using Callcredit.CallReport7;
using CallCredit.Extensions;
using Ecorenew.Common.CallCredit.Domain;
using Nop.Core;
using Nop.Core.Data;
using Nop.Services.Security;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Services.Protocols;
using System.Xml;

namespace Ecorenew.Common.CallCredit.Services
{
    /// <summary>
    /// CallReport service
    /// </summary>
    public class CallReportService : ICallReportService
    {
        #region Fields

        private readonly IRepository<CallReportResult> _callReportResultRepository;
        private readonly IEncryptionService _encryptionService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public CallReportService(IRepository<CallReportResult> callReportResultRepository,
            IEncryptionService encryptionService,
            IWorkContext workContext)
        {
            this._callReportResultRepository = callReportResultRepository;
            this._encryptionService = encryptionService;
            this._workContext = workContext;
        }

        #endregion

        /// <summary>
        /// Execute CallReport search
        /// </summary>
        /// <param name="company">Company</param>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="searchDefinition">Search definition</param>
        /// <param name="apiTestUrl">CallReport API Test URL</param>
        /// <param name="apiLiveUrl">CallReport API Live URL</param>
        /// <param name="useLiveUrl">Use Live API URL?</param>
        public virtual CT_SearchResult CallReportSearch(
            string company,
            string username,
            string password,
            CT_SearchDefinition searchDefinition,
            string apiTestUrl,
            string apiLiveUrl,
            bool useLiveUrl)
        {
            // Create a new proxy object which represents the Callcredit API
            CallReport7 apiProxy = new CallReport7
            {
                Url = useLiveUrl ? apiLiveUrl : apiTestUrl,
                callcreditheadersValue = new callcreditheaders
                {
                    company = company,
                    username = username,
                    password = password
                }
            };

            CT_SearchResult apiresult = new CT_SearchResult();

            try
            {
                /* Issue the request to the Callcredit API. */
                return apiProxy.Search07a(searchDefinition);
            }
            catch (SoapException soapException)
            {
                StringBuilder errorMessage = new StringBuilder();

                errorMessage.AppendLine(soapException.Message);

                XmlNodeList errorList = soapException.Detail.SelectNodes("//Error");

                if (errorList != null && errorList.Count > 0)
                {
                    errorMessage.AppendLine(string.Empty);

                    foreach (XmlNode errorTextNode in errorList)
                    {
                        errorMessage.Append(errorTextNode.InnerText);
                        errorMessage.AppendLine(string.Empty);
                    }
                }

                throw new Exception(errorMessage.ToString());
            }
            catch (Exception unexpectedException)
            {
                throw unexpectedException;
            }
            finally
            {
                apiProxy.Dispose();
            }
        }

        /// <summary>
        /// Process CallReport picklist
        /// </summary>
        /// <param name="picklist">CallReport search result picklist</param>
        public virtual CallReportProcessPicklistsResult ProcessPicklist(CT_apipicklist picklist)
        {
            var result = new CallReportProcessPicklistsResult();

            // Check that the picklist instance exists and that picklist processing is allowed for this user.
            if (picklist != null && picklist.picklist != 0)
            {
                if (picklist.regeneratedSpecified && picklist.regenerated != 0)
                {
                    // Warn the inheritor that the picklist has been regenerated.
                    result.PicklistsRegenerated = true;
                }

                if (picklist.applicant != null)
                {
                    int applicantIndex = 0;

                    // Check each applicant in turn.
                    foreach (var applicant in picklist.applicant)
                    {
                        if (applicant.address != null)
                        {
                            int applicantAddressIndex = 0;

                            // Check for picklists which need processing (starting with address based picklist).
                            foreach (var address in applicant.address)
                            {
                                if (address.fullmatches != null)
                                {
                                    // Assume a picklist is needed for this address, until proven otherwise.
                                    bool addressPicklistRequired = true;

                                    if (address.fullmatches.reporttype == "4")
                                    {
                                        throw new Exception("Too many address matches have been found.");
                                    }

                                    if (address.fullmatches.matchstatus != null)
                                    {
                                        // Any matchstatus means that no picklist is needed for this address.
                                        addressPicklistRequired = false;
                                    }

                                    if (address.fullmatches.fullmatch != null)
                                    {
                                        int applicantAddressFullMatchIndex = 0;

                                        // Check each address match in turn.
                                        foreach (var fullmatch in address.fullmatches.fullmatch)
                                        {
                                            if (fullmatch.selectedSpecified && fullmatch.selected == 1)
                                            {
                                                // A selected fullmatch means that no picklist is needed for this address.
                                                addressPicklistRequired = false;

                                                if (fullmatch.name != null)
                                                {
                                                    int applicantAddressFullMatchNameIndex = 0;

                                                    // Check for name picklists which need processing.
                                                    foreach (var name in fullmatch.name)
                                                    {
                                                        if (name.namematches != null)
                                                        {
                                                            // Assume a picklist is needed for this name, until proven otherwise.
                                                            bool namePicklistRequired = true;

                                                            if (name.namematches.reporttype != null &&
                                                                name.namematches.reporttype == "4")
                                                            {
                                                                throw new Exception("Too many name matches have been found.");
                                                            }

                                                            if (name.namematches.matchstatus != null)
                                                            {
                                                                // Any matchstatus means that no picklist is needed for this name.
                                                                namePicklistRequired = false;
                                                            }

                                                            if (name.namematches.namematch != null)
                                                            {
                                                                // Check each name match in turn. 
                                                                foreach (var namematchRecord in name.namematches.namematch)
                                                                {
                                                                    if (namematchRecord.selectedSpecified && namematchRecord.selected == 1)
                                                                    {
                                                                        // A selected namematch means that no picklist is needed for this name.
                                                                        namePicklistRequired = false;
                                                                    }
                                                                }
                                                            }

                                                            if (namePicklistRequired)
                                                            {
                                                                // Warn the inheritor that this name needs resolving.
                                                                if (result.NameMatchSelectionList == null)
                                                                {
                                                                    result.NameMatchSelectionList = new List<NameMatchSelection>();
                                                                }

                                                                var selection = new NameMatchSelection
                                                                {
                                                                    ApplicantAddressFullMatchIndex = applicantAddressFullMatchIndex,
                                                                    ApplicantAddressFullMatchNameIndex = applicantAddressFullMatchNameIndex,
                                                                    ApplicantAddressIndex = applicantAddressIndex,
                                                                    ApplicantIndex = applicantIndex,
                                                                    NameDictionary = new Dictionary<int, string>()
                                                                };

                                                                for (int i = 0; i < name.namematches.namematch.Length; i++)
                                                                {
                                                                    var nameBuilder = new StringBuilder();

                                                                    var namematch = name.namematches.namematch[i];

                                                                    if (namematch.title != null && namematch.title.Length > 0) nameBuilder.AppendFormat("{0} ", namematch.title);
                                                                    if (namematch.forename != null && namematch.forename.Length > 0) nameBuilder.AppendFormat("{0} ", namematch.forename);
                                                                    if (namematch.othernames != null && namematch.othernames.Length > 0) nameBuilder.AppendFormat("{0} ", namematch.othernames);
                                                                    if (namematch.surname != null && namematch.surname.Length > 0) nameBuilder.AppendFormat("{0} ", namematch.surname);
                                                                    if (namematch.suffix != null && namematch.suffix.Length > 0) nameBuilder.AppendFormat("{0} ", namematch.suffix);

                                                                    selection.NameDictionary.Add(i, nameBuilder.ToString());
                                                                }

                                                                result.NameMatchSelectionList.Add(selection);
                                                            }
                                                        }

                                                        applicantAddressFullMatchNameIndex++;
                                                    }
                                                }
                                            }

                                            applicantAddressFullMatchIndex++;
                                        }
                                    }

                                    if (addressPicklistRequired)
                                    {
                                        // Warn the inheritor that this address needs resolving.
                                        if (result.AddressMatchSelectionList == null)
                                        {
                                            result.AddressMatchSelectionList = new List<AddressMatchSelection>();
                                        }

                                        var selection = new AddressMatchSelection
                                        {
                                            AddressFullMatchDictionary = new Dictionary<int, string>(),
                                            ApplicantAddressIndex = applicantAddressIndex,
                                            ApplicantIndex = applicantIndex
                                        };

                                        for (int i = 0; i < address.fullmatches.fullmatch.Length; i++)
                                        {
                                            var fullmatch = address.fullmatches.fullmatch[i];

                                            var addressBuilder = new StringBuilder();
                                            var addressmatch = fullmatch.addressmatch;
                                            if (addressmatch.abodeno != null && addressmatch.abodeno.Length > 0) addressBuilder.AppendFormat("{0} ", addressmatch.abodeno);
                                            if (addressmatch.buildingname != null && addressmatch.buildingname.Length > 0) addressBuilder.AppendFormat("{0} ", addressmatch.buildingname);
                                            if (addressmatch.buildingno != null && addressmatch.buildingno.Length > 0) addressBuilder.AppendFormat("{0} ", addressmatch.buildingno);
                                            if (addressmatch.street1 != null && addressmatch.street1.Length > 0) addressBuilder.AppendFormat("{0} ", addressmatch.street1);
                                            if (addressmatch.street2 != null && addressmatch.street2.Length > 0) addressBuilder.AppendFormat("{0} ", addressmatch.street2);
                                            if (addressmatch.posttown != null && addressmatch.posttown.Length > 0) addressBuilder.AppendFormat("{0} ", addressmatch.posttown);
                                            if (addressmatch.postcode != null && addressmatch.postcode.Length > 0) addressBuilder.AppendFormat("{0} ", addressmatch.postcode);

                                            selection.AddressFullMatchDictionary.Add(i, addressBuilder.ToString());
                                        }

                                        result.AddressMatchSelectionList.Add(selection);
                                    }
                                }

                                applicantAddressIndex++;
                            }
                        }

                        applicantIndex++;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Adds a new CallReportResult record to the database
        /// </summary>
        /// <param name="result">CallReport search result</param>
        /// <param name="customerId">Customer identifier</param>
        public virtual CallReportResult SaveCallReportResult(CT_SearchResult result, int customerId = 0)
        {
            // Convert API result to XML
            string callReportXmlString = result.ToXmlString();

            // Insert new CallReportResult record
            var callReportResult = new CallReportResult
            {
                CreatedByCustomerId = customerId > 0 ? customerId : _workContext.CurrentCustomer.Id,
                CreatedOnUtc = DateTime.UtcNow,
                EncryptedResultXml = _encryptionService.EncryptText(callReportXmlString),
                SearchId = result.creditreport.searchid
            };

            _callReportResultRepository.Insert(callReportResult);

            return callReportResult;
        }

        /// <summary>
        /// Gets a CallReportResult
        /// </summary>
        /// <param name="id">CallReportResult identifier</param>
        public virtual CallReportResult GetCallReportResultById(int id)
        {
            return _callReportResultRepository.GetById(id);
        }
    }
}