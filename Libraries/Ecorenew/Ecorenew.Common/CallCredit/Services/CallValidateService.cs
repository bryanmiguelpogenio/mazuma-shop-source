﻿using CallCredit.CallValidateApi;
using CallCredit.CallValidateApi.RequestEntities;
using CallCredit.CallValidateApi.ResponseEntities;
using Ecorenew.Common.CallCredit.Domain;
using Ecorenew.Common.Utilities;
using Nop.Core;
using Nop.Core.Data;
using Nop.Services.Security;
using System;

namespace Ecorenew.Common.CallCredit.Services
{
    /// <summary>
    /// CallValidate service
    /// </summary>
    public class CallValidateService : ICallValidateService
    {
        #region Fields

        private readonly IRepository<CallValidateResult> _callValidateResultRepository;
        private readonly IEncryptionService _encryptionService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public CallValidateService(IRepository<CallValidateResult> callValidateResultRepository,
            IEncryptionService encryptionService,
            IWorkContext workContext)
        {
            this._callValidateResultRepository = callValidateResultRepository;
            this._encryptionService = encryptionService;
            this._workContext = workContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Performs a CallValidate check and saves the result
        /// </summary>
        /// <param name="company">Company</param>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="application">Application</param>
        /// <param name="data">Data to check</param>
        /// <param name="apiTestUrl">API URL for testing</param>
        /// <param name="apiLiveUrl">API URL for live</param>
        /// <param name="useLiveUrl">Use live API URL</param>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="IdEnhancedCheck">Perform ID Enhanced check?</param>
        /// <param name="BankStandardCheck">Perform Bank Standard check?</param>
        /// <param name="BankEnhancedCheck">Perform Bank Enhanced check?</param>
        public virtual CallValidateResult PerformCallValidateChecks(
            string company,
            string username,
            string password,
            string application,
            global::CallCredit.CallValidateApi.RequestEntities.Data data,
            string apiTestUrl,
            string apiLiveUrl,
            bool useLiveUrl,
            int customerId = 0,
            bool IdEnhancedCheck = true,
            bool BankStandardCheck = true,
            bool BankEnhancedCheck = true)
        {
            if (string.IsNullOrEmpty(company))
                throw new ArgumentNullException("company");
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException("username");
            if (string.IsNullOrEmpty(password))
                throw new ArgumentNullException("password");
            if (string.IsNullOrEmpty(application))
                throw new ArgumentNullException("application");

            CallValidateConfig config = new CallValidateConfig(company, username, password, application, useLiveUrl, apiTestUrl, apiLiveUrl);
            CallValidateClient client = new CallValidateClient(config);

            data.ChecksRequired = new ChecksRequired
            {
                IDEnhanced = IdEnhancedCheck ? "yes" : "no",
                BankStandard = BankStandardCheck ? "yes" : "no",
                BankEnhanced = BankEnhancedCheck ? "yes" : "no"
            };

            CallValidate request = new CallValidate(config.Company, config.Username, config.Password, config.ApplicationName)
            {
                sessions = new Sessions
                {
                    session = new Session { data = data }
                }
            };

            var result = client.PerformCheck(request);

            // Format XML result
            string formattedXml = result.ResultXml.ToIndentedXmlString();

            // Insert new CallValidateResult record
            var callValidateResult = new CallValidateResult
            {
                CreatedByCustomerId = customerId > 0 ? customerId : _workContext.CurrentCustomer.Id,
                CreatedOnUtc = DateTime.UtcNow,
                Pid = result.Result.PID,
                EncryptedResultXml = _encryptionService.EncryptText(formattedXml)
            };

            _callValidateResultRepository.Insert(callValidateResult);

            return callValidateResult;
        }

        /// <summary>
        /// Returns the results of the decrypted CallValidateResult result XML
        /// </summary>
        /// <param name="callValidateResultId">CallValidateResult identifier</param>
        public virtual Results GetCallValidateResultResults(int callValidateResultId)
        {
            if (callValidateResultId <= 0)
                return null;

            var callValidateResult = _callValidateResultRepository.GetById(callValidateResultId);
            if (callValidateResult != null)
            {
                return GetCallValidateResultResults(callValidateResult);
            }

            return null;
        }

        /// <summary>
        /// Returns the results of the decrypted CallValidateResult result XML
        /// </summary>
        /// <param name="callValidateResult">CallValidateResult</param>
        public virtual Results GetCallValidateResultResults(CallValidateResult callValidateResult)
        {
            if (callValidateResult == null)
                throw new ArgumentNullException("callValidateResult");
            
            string decryptedResultXml = _encryptionService.DecryptText(callValidateResult.EncryptedResultXml);
            return decryptedResultXml.ConvertXmlString<Results>();
        }

        #endregion
    }
}