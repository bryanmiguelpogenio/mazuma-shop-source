﻿using System.Collections.Generic;

namespace Ecorenew.Common.Utilities
{
    public static class StringExtensions
    {
        // TODO rename method
        public static List<string> LimitString(this string text, int charLimit)
        {
            string[] words = text.Split(' ');

            List<string> sentences = new List<string>();

            string line = "";
            foreach (string word in words)
            {
                if ((line + word).Length > charLimit)
                {
                    sentences.Add(line);
                    line = "";
                }

                line += string.Format("{0} ", word);
            }

            if (line.Length > 0)
                sentences.Add(line);

            return sentences;
        }
    }
}