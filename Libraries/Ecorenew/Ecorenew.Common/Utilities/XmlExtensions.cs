﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Ecorenew.Common.Utilities
{
    /// <summary>
    /// Extension methods related to XML
    /// </summary>
    public static class XmlExtensions
    {
        /// <summary>
        /// Formats the XML string to indented formatting
        /// </summary>
        /// <param name="xmlString">XML string</param>
        public static string ToIndentedXmlString(this string xmlString)
        {
            string result = null;

            using (MemoryStream mStream = new MemoryStream())
            {
                using (XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode))
                {
                    XmlDocument document = new XmlDocument();

                    document.LoadXml(xmlString);

                    writer.Formatting = System.Xml.Formatting.Indented;

                    document.WriteContentTo(writer);
                    writer.Flush();
                    mStream.Flush();

                    mStream.Position = 0;

                    StreamReader sReader = new StreamReader(mStream);

                    result = sReader.ReadToEnd();

                    mStream.Close();
                    writer.Close();
                }
            }

            return result;
        }

        /// <summary>
        /// Serializes XML to specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlString">XML string to serialize</param>
        public static T ConvertXmlString<T>(this string xmlString)
        {
            T result = default(T);

            using (TextReader reader = new StringReader(xmlString))
            {
                try
                {
                    result = (T)new XmlSerializer(typeof(T)).Deserialize(reader);
                }
                catch (InvalidOperationException)
                {
                    // String passed is not XML, simply return defaultXmlClass
                }
            }

            return result;
        }
    }
}