﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Nop.Core.Infrastructure.Encryption
{
    public class EncryptionProvider
    {
        private readonly SymmetricAlgorithm _mCsp = new TripleDESCryptoServiceProvider();

        public EncryptionProvider(string key = "")
        {
            if (string.IsNullOrEmpty(key)) key = EncryptionKeys.Key;
            _mCsp.Key = Convert.FromBase64String(key);
            _mCsp.IV = Convert.FromBase64String(EncryptionKeys.Iv);
            _mCsp.Mode = CipherMode.ECB;
            _mCsp.Padding = PaddingMode.PKCS7;
        }

        #region Base64加解密

        /// <summary>
        /// EncryptToBase64
        /// </summary>
        /// <param name="data">Unencrypted text</param>
        /// <returns></returns>
        public string EncryptToBase64(string data)
        {
            if (data == null) return "";

            var ct = _mCsp.CreateEncryptor(_mCsp.Key, _mCsp.IV);
            var byt = Encoding.UTF8.GetBytes(data);
            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
            cs.Write(byt, 0, byt.Length);
            cs.FlushFinalBlock();
            cs.Close();
            return Convert.ToBase64String(ms.ToArray());
        }

        /// <summary>
        /// DecryptFromBase64
        /// </summary>
        /// <param name="data">Encrypted text</param>
        /// <returns></returns>
        public string DecryptFromBase64(string data)
        {
            if (data == null) return "";

            var ct = _mCsp.CreateDecryptor(_mCsp.Key, _mCsp.IV);
            var byt = Convert.FromBase64String(data);
            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
            cs.Write(byt, 0, byt.Length);
            cs.FlushFinalBlock();
            cs.Close();
            return Encoding.UTF8.GetString(ms.ToArray());
        }
        #endregion
    }
}
