namespace Nop.Core.Domain.Customers
{
    /// <summary>
    /// System customer attribute names
    /// </summary>
    public static partial class SystemCustomerAttributeNames
    {
        //Form fields
        /// <summary>
        /// FirstName
        /// </summary>
        public static string FirstName { get { return "FirstName"; } }
        /// <summary>
        /// LastName
        /// </summary>
        public static string LastName { get { return "LastName"; } }
        /// <summary>
        /// Gender
        /// </summary>
        public static string Gender { get { return "Gender"; } }
        /// <summary>
        /// DateOfBirth
        /// </summary>
        public static string DateOfBirth { get { return "DateOfBirth"; } }
        /// <summary>
        /// Company
        /// </summary>
        public static string Company { get { return "Company"; } }
        /// <summary>
        /// StreetAddress
        /// </summary>
        public static string StreetAddress { get { return "StreetAddress"; } }
        /// <summary>
        /// StreetAddress2
        /// </summary>
        public static string StreetAddress2 { get { return "StreetAddress2"; } }
        /// <summary>
        /// ZipPostalCode
        /// </summary>
        public static string ZipPostalCode { get { return "ZipPostalCode"; } }
        /// <summary>
        /// City
        /// </summary>
        public static string City { get { return "City"; } }
        /// <summary>
        /// CountryId
        /// </summary>
        public static string CountryId { get { return "CountryId"; } }
        /// <summary>
        /// StateProvinceId
        /// </summary>
        public static string StateProvinceId { get { return "StateProvinceId"; } }
        /// <summary>
        /// Phone
        /// </summary>
        public static string Phone { get { return "Phone"; } }
        /// <summary>
        /// Fax
        /// </summary>
        public static string Fax { get { return "Fax"; } }
        /// <summary>
        /// VatNumber
        /// </summary>
        public static string VatNumber { get { return "VatNumber"; } }
        /// <summary>
        /// VatNumberStatusId
        /// </summary>
        public static string VatNumberStatusId { get { return "VatNumberStatusId"; } }
        /// <summary>
        /// TimeZoneId
        /// </summary>
        public static string TimeZoneId { get { return "TimeZoneId"; } }
        /// <summary>
        /// CustomCustomerAttributes
        /// </summary>
        public static string CustomCustomerAttributes { get { return "CustomCustomerAttributes"; } }

        //Other attributes
        /// <summary>
        /// DiscountCouponCode
        /// </summary>
        public static string DiscountCouponCode { get { return "DiscountCouponCode"; } }
        /// <summary>
        /// GiftCardCouponCodes
        /// </summary>
        public static string GiftCardCouponCodes { get { return "GiftCardCouponCodes"; } }
        /// <summary>
        /// AvatarPictureId
        /// </summary>
        public static string AvatarPictureId { get { return "AvatarPictureId"; } }
        /// <summary>
        /// ForumPostCount
        /// </summary>
        public static string ForumPostCount { get { return "ForumPostCount"; } }
        /// <summary>
        /// Signature
        /// </summary>
        public static string Signature { get { return "Signature"; } }
        /// <summary>
        /// PasswordRecoveryToken
        /// </summary>
        public static string PasswordRecoveryToken { get { return "PasswordRecoveryToken"; } }
        /// <summary>
        /// PasswordRecoveryTokenDateGenerated
        /// </summary>
        public static string PasswordRecoveryTokenDateGenerated { get { return "PasswordRecoveryTokenDateGenerated"; } }
        /// <summary>
        /// AccountActivationToken
        /// </summary>
        public static string AccountActivationToken { get { return "AccountActivationToken"; } }
        /// <summary>
        /// EmailRevalidationToken
        /// </summary>
        public static string EmailRevalidationToken { get { return "EmailRevalidationToken"; } }
        /// <summary>
        /// LastVisitedPage
        /// </summary>
        public static string LastVisitedPage { get { return "LastVisitedPage"; } }
        /// <summary>
        /// ImpersonatedCustomerId
        /// </summary>
        public static string ImpersonatedCustomerId { get { return "ImpersonatedCustomerId"; } }
        /// <summary>
        /// AdminAreaStoreScopeConfiguration
        /// </summary>
        public static string AdminAreaStoreScopeConfiguration { get { return "AdminAreaStoreScopeConfiguration"; } }

        //depends on store
        /// <summary>
        /// CurrencyId
        /// </summary>
        public static string CurrencyId { get { return "CurrencyId"; } }
        /// <summary>
        /// LanguageId
        /// </summary>
        public static string LanguageId { get { return "LanguageId"; } }
        /// <summary>
        /// LanguageAutomaticallyDetected
        /// </summary>
        public static string LanguageAutomaticallyDetected { get { return "LanguageAutomaticallyDetected"; } }
        /// <summary>
        /// SelectedPaymentMethod
        /// </summary>
        public static string SelectedPaymentMethod { get { return "SelectedPaymentMethod"; } }
        /// <summary>
        /// SelectedShoppingCartItemWithInsurance
        /// </summary>
        public static string SelectedShoppingCartItemWithInsurence { get { return "SelectedShoppingCartItemWithInsurance"; } }

        /// <summary>
        /// SelectedShoppingCartItemQuantityWithInsurance
        /// </summary>
        //public static string SelectedShoppingCartItemQuantityWithInsurance { get { return "SelectedShoppingCartItemQuantityWithInsurance"; } }
        /// <summary>
        /// SelectedShippingOption
        /// </summary>
        public static string SelectedShippingOption { get { return "SelectedShippingOption"; } }
        /// <summary>
        /// SelectedPickupPoint
        /// </summary>
        public static string SelectedPickupPoint { get { return "SelectedPickupPoint"; } }
        /// <summary>
        /// CheckoutAttributes
        /// </summary>
        public static string CheckoutAttributes { get { return "CheckoutAttributes"; } }
        /// <summary>
        /// OfferedShippingOptions
        /// </summary>
        public static string OfferedShippingOptions { get { return "OfferedShippingOptions"; } }
        /// <summary>
        /// LastContinueShoppingPage
        /// </summary>
        public static string LastContinueShoppingPage { get { return "LastContinueShoppingPage"; } }
        /// <summary>
        /// NotifiedAboutNewPrivateMessages
        /// </summary>
        public static string NotifiedAboutNewPrivateMessages { get { return "NotifiedAboutNewPrivateMessages"; } }
        /// <summary>
        /// WorkingThemeName
        /// </summary>
        public static string WorkingThemeName { get { return "WorkingThemeName"; } }
        /// <summary>
        /// TaxDisplayTypeId
        /// </summary>
        public static string TaxDisplayTypeId { get { return "TaxDisplayTypeId"; } }
        /// <summary>
        /// UseRewardPointsDuringCheckout
        /// </summary>
        public static string UseRewardPointsDuringCheckout { get { return "UseRewardPointsDuringCheckout"; } }
        /// <summary>
        /// EuCookieLawAccepted
        /// </summary>
        public static string EuCookieLawAccepted { get { return "EuCookieLaw.Accepted"; } }

        /// <summary>
        /// PopupHomePageLastTime
        /// </summary>
        public static string PopupHomePageLastTimeUtc { get { return "Popup.Homepage.LastTimeUtc"; } }

        /// <summary>
        /// PopupHomePageTimes
        /// </summary>
        public static string PopupHomePageTimes { get { return "Popup.Homepage.Times"; } }

        /// <summary>
        /// PopupRegisterPageLastTimeUtc
        /// </summary>
        public static string PopupRegisterPageLastTimeUtc { get { return "Popup.RegisterPage.LastTimeUtc"; } }

        /// <summary>
        /// PopupRegisterPageTimes
        /// </summary>
        public static string PopupRegisterPageTimes { get { return "Popup.RegisterPage.Times"; } }

        /// <summary>
        /// PopupCategoryPageLastTimeUtc
        /// </summary>
        public static string PopupCategoryPageLastTimeUtc { get { return "Popup.CategoryPage.LastTimeUtc"; } }

        /// <summary>
        /// PopupCategoryPageTimes
        /// </summary>
        public static string PopupCategoryPageTimes { get { return "Popup.CategoryPage.Times"; } }

        /// <summary>
        /// PopupProductPageLastTimeUtc
        /// </summary>
        public static string PopupProductPageLastTimeUtc { get { return "Popup.ProductPage.LastTimeUtc"; } }

        /// <summary>
        /// PopupProductPageTimes
        /// </summary>
        public static string PopupProductPageTimes { get { return "Popup.ProductPage.Times"; } }
    }
}