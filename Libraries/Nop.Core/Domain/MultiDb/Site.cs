﻿namespace Nop.Core.Domain.MultiDb
{
    public class Site:BaseEntity
    {
        public string Name { get; set; }
        
        public string WebUrl { get; set; }
        
        public string DbConn { get; set; }
        
        public int DisplayOrder { get; set; }
        
        public int MainBack { get; set; }
    }
}
