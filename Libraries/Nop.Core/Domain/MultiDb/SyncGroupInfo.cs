﻿using System.Collections.Generic;

namespace Nop.Core.Domain.MultiDb
{
    public class SyncGroupInfo:BaseEntity
    {
        public string GroupName { get; set; }
        
        public int DisplayOrder { get; set; }
        public string TableNames { get; set; }

        public int RequireSync { get; set; }
    }
}
