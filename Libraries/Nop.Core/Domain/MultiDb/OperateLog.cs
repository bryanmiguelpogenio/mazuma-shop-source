﻿namespace Nop.Core.Domain.MultiDb
{
    public class OperateLog:BaseEntity
    {
        public int Action { get; set; }
        public string TableName { get; set; }
        
        public int EntityId { get; set; }
    }
}
