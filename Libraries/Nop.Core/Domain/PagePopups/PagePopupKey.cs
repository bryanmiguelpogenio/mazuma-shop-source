﻿using System.ComponentModel;

namespace Nop.Core.Domain.PagePopups
{
    /// <summary>
    /// Represents a PagePopup Keys enumeration
    /// </summary>
    public enum PagePopupKey
    {
        //Default = 1,

        HomePage = 2,

        RegisterPage = 3,

        CategoryPage = 4,

        ProductPage = 5
    }
}
