﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;

namespace Nop.Core.Domain.Enquiry
{
    public partial class EnquiryRequest : BaseEntity
    {
        private ICollection<EnquiryReplyRecord> _enquiryReplyRecords;

        public string RequestNumber { get; set; }
        public int RequestStatusId { get; set; }
        public int? CustomerId { get; set; }
        public int? OrderId { get; set; }
        public string OrderNumber { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public int EnquiryTicketReasonId { get; set; }
        public string Enquiry { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime LastUpdatedOnUtc { get; set; }

        /// <summary>
        /// Gets the Customer
        /// </summary>
        public virtual Customer Customer { get; set; }
        /// <summary>
        /// Gets the order
        /// </summary>
        public virtual Order Order { get; set; }
        /// <summary>
        /// Gets the EnquiryTicketReason
        /// </summary>
        public virtual EnquiryTicketReason EnquiryTicketReason { get; set; }

        public virtual ICollection<EnquiryReplyRecord> EnquiryReplyRecords
        {
            get { return _enquiryReplyRecords ?? (_enquiryReplyRecords = new List<EnquiryReplyRecord>()); }
            protected set { _enquiryReplyRecords = value; }
        }

        #region Custom properties
        public EnquiryTicketStatus RequestStatus
        {
            get
            {
                return (EnquiryTicketStatus)RequestStatusId;
            }
            set
            {
                RequestStatusId = (int)value;
            }
        }

        #endregion

    }
}
