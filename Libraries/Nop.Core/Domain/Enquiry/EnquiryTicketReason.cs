﻿namespace Nop.Core.Domain.Enquiry
{
    public partial class EnquiryTicketReason : BaseEntity
    {
        public string Reason { get; set; }
        public bool IsActive { get; set; }
    }
}
