﻿using Nop.Core.Domain.Customers;
using System;

namespace Nop.Core.Domain.Enquiry
{
    public partial class EnquiryReplyRecord : BaseEntity
    {
        public int EnquiryRequestId { get; set; }
        public string Message { get; set; }
        public int? CreatorUserId { get; set; }
        public bool IsFromClient { get; set; }
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets the EnquiryReques
        /// </summary>
        public virtual EnquiryRequest EnquiryRequest { get; set; }
        /// <summary>
        /// Gets the CreatorUser
        /// </summary>
        public virtual Customer CreatorUser { get; set; }
    }
}
