﻿namespace Nop.Core.Domain.Enquiry
{
    public enum EnquiryTicketStatus
    {
        Pending = 0,
        Replied = 10
    }
}
