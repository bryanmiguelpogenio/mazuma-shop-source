using System;

namespace Nop.Core.Domain.Common
{
    /// <summary>
    /// Represents a popup record
    /// </summary>
    public partial class PopupIpAddress : BaseEntity
    {
        /// <summary>
        /// Gets or sets the Ip Address
        /// </summary>
        public string IpAddress { get; set; }
        
        /// <summary>
        /// Gets or sets page name
        /// </summary>
        public int Pages { get; set; }

        /// <summary>
        /// Gets or sets times of popup
        /// </summary>
        public int PopupTimes { get; set; }

        /// <summary>
        /// Gets or sets UTC time of last popup
        /// </summary>
        public DateTime PopupLastTimeUtc { get; set; }
    }
}
