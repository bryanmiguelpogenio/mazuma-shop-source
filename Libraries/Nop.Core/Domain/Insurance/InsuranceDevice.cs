﻿namespace Nop.Core.Domain.Insurance
{
    public partial class InsuranceDevice:BaseEntity
    {
        public string Key { get; set; }
        public string Type { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Value { get; set; }
    }
}
