﻿using Nop.Core.Domain.Orders;
using System;

namespace Nop.Core.Domain.Bank
{
    public partial class TransferRecord:BaseEntity
    {
        public int OrderId { get; set; }
        public string FromAccount { get; set; }
        public string FromCardCode { get; set; }
        public string ToCardCode { get; set; }
        public string ToAccount { get; set; }
        public decimal Amount { get; set; }
        public string SerialNumber { get; set; }
        public int TransferStatusId { get; set; }
        public string Remark { get; set; }
        public DateTime TransferDate { get; set; }
        public DateTime CreatedUTC { get; set; }
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets the order
        /// </summary>
        public virtual Order Order { get; set; }

        #region Custom properties
        public TransferStatus TransferStatus
        {
            get
            {
                return (TransferStatus)TransferStatusId;
            }
            set
            {
                TransferStatusId = (int)value;
            }
        }

        #endregion

    }
}
