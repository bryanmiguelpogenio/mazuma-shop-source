﻿namespace Nop.Core.Domain.Bank
{
    public enum TransferStatus
    {
        Pending = 10,
        Passed = 20,
        Refused =30,
    }
}
