﻿namespace Nop.Core.Domain.Bank
{
    public partial class BankInfo:BaseEntity
    {
        public string IBAN { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsActive { get; set; }
        public int StoreId { get; set; }
    }
}
