﻿namespace Nop.Core.Domain.Orders
{
    public partial class DirectDebitInformation : BaseEntity
    {
        #region Properties

        public int OrderId { get; set; }

        /// <summary>
        /// Gets or sets the Bank Account Name
        /// </summary>
        public string BankAccountName { get; set; }

        /// <summary>
        /// Gets or sets the Sort Code
        /// </summary>
        public string SortCode { get; set; }

        /// <summary>
        /// Gets or sets the Account Number
        /// </summary>
        public string AccountNumber { get; set; }
        #endregion

        #region  Navigation properties

        public virtual Order Order { get; set; }
        #endregion
    }
}
