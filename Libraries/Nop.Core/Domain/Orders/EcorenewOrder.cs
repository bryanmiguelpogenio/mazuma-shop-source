﻿using System;

namespace Nop.Core.Domain.Orders
{
    public partial class Order
    {
        /// <summary>
        /// Gets or sets the date and time when the order data has been synchronized with the EcoRenew Logistics system
        /// </summary>
        public DateTime? SynchronizedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the EcoRenew finance type identifier
        /// </summary>
        public int EcorenewFinanceTypeId { get; set; }

        /// <summary>
        /// Gets or sets the EcoRenew finance type
        /// </summary>
        public virtual EcorenewFinanceType? EcorenewFinanceType
        {
            get { return EcorenewFinanceTypeId == 0 ? null : (EcorenewFinanceType?)EcorenewFinanceTypeId; }
            set { EcorenewFinanceTypeId = value == null ? 0 : (int)value; }
        }
    }
}