﻿using System;

namespace Nop.Core.Domain.Orders
{
    public partial class ReturnRequest
    {
        public DateTime? SynchronizedOnUtc { get; set; }
    }
}