﻿

namespace Nop.Core.Domain.Orders
{
    public enum UploadStatus
    {
        NotUploaded = 0,

        Uploaded=10,

        Error=20,
    }
}
