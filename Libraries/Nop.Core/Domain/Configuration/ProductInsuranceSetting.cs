﻿
using Nop.Core.Domain.Catalog;

namespace Nop.Core.Domain.Configuration
{
    public partial class ProductInsuranceSetting:BaseEntity
    {
        public int ProductId { get; set; }
        public int ProductType { get; set; }
        public decimal RangeStart { get; set; }
        public decimal RangeEnd { get; set; }
        public bool IsActive { get; set; }
        
        public virtual Product Product { get; set; }

        #region Custom properties
        public ProductInsuranceType ProductInsuranceType
        {
            get
            {
                return (ProductInsuranceType)ProductType;
            }
            set
            {
                ProductType = (int)value;
            }
        }
        #endregion
    }
}
